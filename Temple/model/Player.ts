﻿module Temple {

    export class Player extends Phaser.Sprite {

        private _gameControler: GameControler = GameControler.getInstance();
        private _obstacleControler: ObstacleControler = ObstacleControler.getInstance();
        private _player: Phaser.Sprite;
        private _stars: Phaser.Sprite;
        constructor(game: Phaser.Game, key: string) {

            super(game, 0, 0, "", 0);


            this._player = new Phaser.Sprite(this.game, -15, -135, key, 0);
            this._player.animations.add("stars", this.countFrames(11, 46), 24, false);
            this._player.animations.add("stand", this.countFrames(1, 9), 24, false);
            this._player.animations.add("left", this.countFrames(58, 73), 24, false);
            this._player.animations.add("right", this.countFrames(42, 57), 24, false);
            this._player.animations.add("up", this.countFrames(26, 41), 24, false);
            this._player.animations.add("down", this.countFrames(10, 25), 24, false);
            this._player.animations.add("diamond", this.countFrames(74, 84), 24, false);
            this._player.animations.add("tap", this.countFrames(85, 96), 24, false);
            this._player.animations.add("death", this.countFrames(109, 129), 24, false);

            this.addChild(this._player);

            this._stars = new Phaser.Sprite(this.game, 0, -60, "stars", 0);
            this._stars.animations.add("play");
            this._stars.visible = false;
            this.addChild(this._stars);
           

        }

        death() {
            console.log("kusi ta smierc");
            this._obstacleControler.enemyCollision = true;
            this.body.velocity.setTo(0, 0);
            this._player.animations.getAnimation("death").onComplete.add( this.kuniec, this);
            this._player.animations.play("death", 24, false);
        }

        kuniec() {
            console.log("koniec kusi ta smierc");
            this._gameControler.level.playAgainAfterDeath();
            this._obstacleControler.enemyCollision = false;

        }

        stand() {
           
            this._player.animations.getAnimation("stand").onComplete.add(function () {  }, this);
            this._player.animations.play("stand", 24, true);
            
        }

        left() {
            this._player.animations.getAnimation("left").onComplete.add(function () {  }, this);
            this._player.animations.play("left", 24, true);
        }

        right() {

            this._player.animations.getAnimation("right").onComplete.add(function () { }, this);
            this._player.animations.play("right", 24, true);

        }

        up() {
            this._player.animations.getAnimation("up").onComplete.add(function () {  }, this);
            this._player.animations.play("up", 24, true);

        }

        diamond() {
           
                this._player.animations.getAnimation("diamond").onComplete.add(function () {  }, this);
                this._player.animations.play("diamond", 24, false);

          
        }

        tap() {
            this._player.animations.getAnimation("tap").onComplete.add(function () { this.endstars(); }, this);
            this._player.animations.play("tap", 24, true);

        }


        down() {
            this._player.animations.getAnimation("down").onComplete.add(function () { }, this);
            this._player.animations.play("down", 24, true);

        }

        stars(num:number=3) {
            this._obstacleControler.checkCollisionObstale = false;
            this._obstacleControler.enemyCollision = true;
            this._stars.visible = true;
            this._stars.animations.play("play", 24, true);

            this.game.time.events.add(Phaser.Timer.SECOND * num, function () {

                this._stars.visible = false;
                this.endstars();
                this._obstacleControler.enemyCollision = false;


            }, this)

        }

        endstars() {

            this._obstacleControler.checkCollisionObstale = true;
            this._gameControler.gamePaused = false;
           // this.stand();
        }



        countFrames(start: number, end: number): Array<number> {
            var countArr: Array<number> = new Array();
            for (var i: number = start; i < end; i++) {

                countArr.push(i);


            }

            return countArr;


        }


    }

}


/*

1-10 stoi
11-46 gwiazdki
47-62 idzie w dol
63-78 idzie w gore
79-84 idzie w prawo
95-110 idzie w lewo
111-121 wierci diament
122-145 u gory napis Tap
146 -223 umiera

*/
 