﻿module Temple {

    export class Enemy extends Phaser.Sprite {
        private _gameControler: GameControler = GameControler.getInstance();
        private _nextMove: number = 0;
        private _moveRate: number = 3000;
        private Speed: number = 60;
        public stopEnemy: boolean = true;
        private startPosition: Phaser.Point;
        private outOfBounds: boolean = false;
        private _correctPosition: Phaser.Point;
        private _audio: Phaser.Sound;
        constructor(game:Phaser.Game,x:number,y:number,key:string) {

            super(game, x, y, key);

            this.startPosition = new Phaser.Point(x, y);
            this.frame = 0;
            this.checkWorldBounds = true;
            this.anchor.setTo(0.5, 0.5);

            if (key == "enemy2") {

                this._audio = this.game.add.audio("bat", 1, false);

            }
            else if (key == "enemy1") {

                this._audio = this.game.add.audio("handgoti", 1, false);
            }
           // this.outOfBoundsKill = true;
          //  this.animations.add("play",[0,1]);
            //this.animations.play("play", 24, true);
           // this.moveTo();
           
        }

        playAudio() {

            if (this._audio != null) {

                this._audio.play();

            }

        }

        moveStop() {

           

        }

        moveOn() {

            this.Speed = 0;
        }

        public set moveRate(moveSpeed: number) {

            this._moveRate = moveSpeed;

        }

        moveTo() {

            if (!this.stopEnemy) {
                
               // this.body.bounds.setTo(10, 10, 300, 300);
                //this.body.setBounds(100, 100, 550, 550);
             

                if (this.game.physics.arcade.distanceBetween(this, this._gameControler.player) < 800) {
                    if (this.game.time.now > this._nextMove) {
                        this._nextMove = this.game.time.now + this._moveRate;

                  

                        // bullet.reset(this.turret.x, this.turret.y);
                        var x: number = this._gameControler.player.world.x;
                        var y: number = this._gameControler.player.world.y;
                        if (x > 650) x = 650;
                        else if (x < 99) x =100;
                        if (y < 100) y = 100;
                        else if (y > 640) y = 400;
                    
                        this.game.physics.arcade.moveToObject(this, this._gameControler.player,60);
                    }
                }

                this.game.time.events.add(Phaser.Timer.SECOND * 3, function () {
                    this.outOfBounds = false;
                    this.moveTo();




                }, this)



            }

           // var point: Phaser.Point = new Phaser.Point(this._gameControler.player.x, this._gameControler.player.y);
          
              /*  var x = Math.floor(point.x);
                var y = Math.floor(point.y);


                x = x - (x % 50);
                y = y - (y % 50);



              //  var tween = this.game.add.tween(this).to({ x: x, y: y }, 250, Phaser.Easing.Quadratic.InOut, true);
              //  tween.onComplete.add(this.moveComplete, this);
            */
            /*
                var target = this._gameControler.player;
                var follower = this;
                if (follower.x == NaN) return;ss
                var distanceX: number = Math.floor( target.x - follower.x);
                var distanceY: number = Math.floor(target.y - follower.y);
                console.log("dystans", distanceX, distanceY);
                //get total distance as one number
               var distanceTotal:number = Math.floor( Math.sqrt(distanceX * distanceX + distanceY * distanceY));
               console.log("total", distanceTotal);
                //check if target is within agro range
               var turnRate:number = 0.3;
               var moveX: number = 0;
               var moveY: number = 0;
               var speed:number = 3;
               
                if (distanceTotal <= 200 && distanceTotal > 0 && distanceX > 0 && distanceY > 0 ) {
                    //calculate how much to move
                    var moveDistanceX: number = Math.floor(  distanceX / distanceTotal);
                    var moveDistanceY: number = Math.floor(  distanceY / distanceTotal);
                    //console.log("total GOWNO", distanceX , distanceTotal, 0/0);
                    //increase current speed
                    moveX += moveDistanceX;
                    moveY += moveDistanceY;
		
                    //get total move distance
                    var totalmove = Math.floor( Math.sqrt(moveX * moveX + moveY * moveY));
		
                    //apply easing
                    moveX = Math.floor( speed * moveX / totalmove);
                    moveY = Math.floor( speed * moveY / totalmove);
		
                    //move follower
                    console.log("movex", moveX, moveY);
                    follower.x += moveX;
                    follower.y += moveY;
		
                    //rotate follower toward target
                    follower.rotation = 180 * Math.atan2(moveY, moveX) / Math.PI;
                   
                }
            
            */
        }

        moveComplete() {

          
            this.game.time.events.add(Phaser.Timer.SECOND * 2, function () {

                this.moveTo();




            }, this)
        }

        makeStopEnemy() {

            this.stopEnemy = true;
          //  this.animations.stop("play");
            this.body.velocity.setTo(0, 0) 
        }

        makeStartEnemy() {

            this.stopEnemy = false;
        //    this.animations.play("play",24,true);
            this.moveTo();
        }

        restart() {

            this.body.velocity.setTo(0, 0);
            this.position = this.startPosition;
            this.stopEnemy = true;
        }


        update() {

           // console.log(this.game.world.bounds, new Phaser.Rectangle(this.x, this.y, 50, 50));
            if (!this.game.world.bounds.intersects(new Phaser.Rectangle(this.world.x, this.world.y, 200, 200), 1) && !this.outOfBounds) {

               
                console.log("blokun kurwe", this.game.world.bounds, new Phaser.Rectangle(this.world.x, this.world.y, 50, 50));
                this.body.velocity.setTo(0, 0);
                this.position.setTo(this._correctPosition.x, this._correctPosition.y);
                this.outOfBounds = true
                //this.moveTo();

            }
            else {
                this._correctPosition = new Phaser.Point(this.x, this.y);
            }

        }

    }

} 