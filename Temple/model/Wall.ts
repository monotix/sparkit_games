﻿module Temple {

    export class Wall extends Phaser.Sprite {

        private _wall: Phaser.Sprite;
        constructor(game: Phaser.Game, x: number, y: number) {

            super(game, x, y, "");

            var grp: Phaser.Graphics = new Phaser.Graphics(game, x, y);
            grp.beginFill(0x000000, 1);
            grp.drawRect(0, 0, 50, 50);
            grp.endFill();

           

            this._wall = new Phaser.Sprite(game, x, y, "");
            this._wall.anchor.setTo(0.5, 0.5);
            this._wall.addChild(grp);
            this.name = "wall";
            this._wall.addChild(grp);
            this.addChild(this._wall);
        }


    }

} 