﻿module Temple {

    export class Player extends Phaser.Sprite {

        private _gameControler: GameControler = GameControler.getInstance();
        constructor(game: Phaser.Game, key: string) {

            super(game, 0, 0, key, 0);

            this.animations.add("stars", this.countFrames(11, 46), 24, false);


        }



        public stars() {

            this.animations.getAnimation("stars").onComplete.add(function () { this.endstars(); }, this);
            this.animations.play("stars", 24, false);



        }

        endstars() {

            this._gameControler.gamePaused = false;
        }



        countFrames(start: number, end: number): Array<number> {
            var countArr: Array<number> = new Array();
            for (var i: number = start; i < end + 1; i++) {

                countArr.push(i);


            }

            return countArr;


        }


    }

}


/*

1-10 stoi
11-46 gwiazdki
47-62 idzie w dol
63-78 idzie w gore
79-84 idzie w prawo
95-110 idzie w lewo
111-121 wierci diament
122-145 u gory napis Tap
146 -223 umiera

*/
 