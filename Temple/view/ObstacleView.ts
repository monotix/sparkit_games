﻿module Temple {

    export class ObstacleView extends Phaser.Group {


        private _gameControler: GameControler = GameControler.getInstance();
        private _home: Phaser.Sprite;
        private _obstacleControler: ObstacleControler = ObstacleControler.getInstance();
        private _panleGameControler: PanelGameControler = PanelGameControler.getInstance();
        private _diamondControler: DiamondsControler = DiamondsControler.getInstance();
        private _hole: Phaser.Sprite;
        private _strikeStill: Phaser.Sprite;
        private enemArray: Array<Phaser.Sprite>;
        private _audioTrap: Phaser.Sound;
        constructor(game: Phaser.Game) {

            super(game);

            this.enableBody = true;
            this._home = new Phaser.Sprite(this.game, 0, 0, "home");
            this._home.name = "home";
           // this._home.body.setSize(50, 50, 0, 0);
            this.addObstacle(this._home, new Phaser.Point(50, 100));
            this._audioTrap = this.game.add.audio("trap", 1, false);
            this.addEnemis()

        }

        addEnemis() {

            var currentLevel = this._diamondControler.currentLevel
            this.enemArray = new Array();
            if (currentLevel == 0) {

                this._hole = new Phaser.Sprite(this.game, 0, 0, "hole");
                this._hole.animations.add("hole", this.countFrames(0, 57), 24, false);
                this._hole.name = "hole";
                this.addObstacle(this._hole, new Phaser.Point(300, 300));


                this._strikeStill = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                this._strikeStill.name = "strikeStill";
                this.addObstacle(this._strikeStill, new Phaser.Point(400, 300));

                this.enemArray.push(this._hole);
                this.enemArray.push(this._strikeStill);

             

            }
            else if (currentLevel == 1) {


                this._hole = new Phaser.Sprite(this.game, 0, 0, "hole");
                this._hole.animations.add("hole", this.countFrames(0, 57), 24, false);
                this._hole.name = "hole";
                this.addObstacle(this._hole, new Phaser.Point(300, 300));


                this._strikeStill = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                this._strikeStill.name = "strikeStill";
                this.addObstacle(this._strikeStill, new Phaser.Point(400, 300));

                var strikeStill = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                strikeStill.name = "strikeStill";
                this.addObstacle(strikeStill, new Phaser.Point(450, 300));

                this.enemArray.push(this._hole);
                this.enemArray.push(this._strikeStill);
                this.enemArray.push(strikeStill);

            }

            else if (currentLevel == 2) {


                this._hole = new Phaser.Sprite(this.game, 0, 0, "hole");
                this._hole.animations.add("hole", this.countFrames(0, 57), 24, false);
                this._hole.name = "hole";
                this.addObstacle(this._hole, new Phaser.Point(200, 250));


                this._strikeStill = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                this._strikeStill.name = "strikeStill";
                this.addObstacle(this._strikeStill, new Phaser.Point(350, 250));

                var strikeStill = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                strikeStill.name = "strikeStill";
                this.addObstacle(strikeStill, new Phaser.Point(450, 200));

                var hole = new Phaser.Sprite(this.game, 0, 0, "hole");
                hole.animations.add("hole", this.countFrames(0, 57), 24, false);
                hole.name = "hole";
                this.addObstacle(hole, new Phaser.Point(200, 200));

                this.enemArray.push(strikeStill);
                this.enemArray.push(hole);
                this.enemArray.push(this._hole);
                this.enemArray.push(this._strikeStill);

            }

            else if (currentLevel == 3) {


                this._hole = new Phaser.Sprite(this.game, 0, 0, "hole");
                this._hole.animations.add("hole", this.countFrames(0, 57), 24, false);
                this._hole.name = "hole";
                this.addObstacle(this._hole, new Phaser.Point(350, 100));


                this._strikeStill = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                this._strikeStill.name = "strikeStill";
                this.addObstacle(this._strikeStill, new Phaser.Point(400, 300));

                var strikeStill = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                strikeStill.name = "strikeStill";
                this.addObstacle(strikeStill, new Phaser.Point(450, 200));

                var hole = new Phaser.Sprite(this.game, 0, 0, "hole");
                hole.animations.add("hole", this.countFrames(0, 57), 24, false);
                hole.name = "hole";
                this.addObstacle(hole, new Phaser.Point(200, 250));

                var hole2 = new Phaser.Sprite(this.game, 0, 0, "hole");
                hole2.animations.add("hole", this.countFrames(0, 57), 24, false);
                hole2.name = "hole";
                this.addObstacle(hole2, new Phaser.Point(300, 250));

                this.enemArray.push(strikeStill);
                this.enemArray.push(hole);
                this.enemArray.push(hole2);
                this.enemArray.push(this._hole);
                this.enemArray.push(this._strikeStill);

            }
            else if (currentLevel == 4) {


                this._hole = new Phaser.Sprite(this.game, 0, 0, "hole");
                this._hole.animations.add("hole", this.countFrames(0, 57), 24, false);
                this._hole.name = "hole";
                this.addObstacle(this._hole, new Phaser.Point(300, 400));

                var hole2 = new Phaser.Sprite(this.game, 0, 0, "hole");
                hole2.animations.add("hole", this.countFrames(0, 57), 24, false);
                hole2.name = "hole";
                this.addObstacle(hole2, new Phaser.Point(650, 350));

                var hole = new Phaser.Sprite(this.game, 0, 0, "hole");
                hole.animations.add("hole", this.countFrames(0, 57), 24, false);
                hole.name = "hole";
                this.addObstacle(hole, new Phaser.Point(550, 250));

                this._strikeStill = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                this._strikeStill.name = "strikeStill";
                this.addObstacle(this._strikeStill, new Phaser.Point(200, 200));


                var strikeStill = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                strikeStill.name = "strikeStill";
                this.addObstacle(strikeStill, new Phaser.Point(450, 350));

                var strikeStill2 = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                strikeStill2.name = "strikeStill";
                this.addObstacle(strikeStill2, new Phaser.Point(500, 250));

                this.enemArray.push(strikeStill);
                this.enemArray.push(hole2);
                this.enemArray.push(hole);
                this.enemArray.push(this._hole);
                this.enemArray.push(this._strikeStill);

            }

            else if (currentLevel == 5) {


                this._hole = new Phaser.Sprite(this.game, 0, 0, "hole");
                this._hole.animations.add("hole", this.countFrames(0, 57), 24, false);
                this._hole.name = "hole";
                this.addObstacle(this._hole, new Phaser.Point(300, 400));

                var hole2 = new Phaser.Sprite(this.game, 0, 0, "hole");
                hole2.animations.add("hole", this.countFrames(0, 57), 24, false);
                hole2.name = "hole";
                this.addObstacle(hole2, new Phaser.Point(650, 350));

                var hole = new Phaser.Sprite(this.game, 0, 0, "hole");
                hole.animations.add("hole", this.countFrames(0, 57), 24, false);
                hole.name = "hole";
                this.addObstacle(hole, new Phaser.Point(550, 200));

                this._strikeStill = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                this._strikeStill.name = "strikeStill";
                this.addObstacle(this._strikeStill, new Phaser.Point(200, 150));


                var strikeStill = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                strikeStill.name = "strikeStill";
                this.addObstacle(strikeStill, new Phaser.Point(450, 350));

                var strikeStill2 = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                strikeStill2.name = "strikeStill";
                this.addObstacle(strikeStill2, new Phaser.Point(500, 200));

                this.enemArray.push(strikeStill);
                this.enemArray.push(hole2);
                this.enemArray.push(hole);
                this.enemArray.push(this._hole);
                this.enemArray.push(this._strikeStill);

            }

            this.addEnemisFromArra();
        

        }

      

        addEnemisFromArra() {

            for (var i: number = 0; i < this.enemArray.length; i++) {

                this.add(this.enemArray[i]);
                this.game.physics.enable(this.enemArray[i], Phaser.Physics.ARCADE);

                this.enemArray[i].body.setSize(46, 46, 0, 0);

            }

        }

        removeEnemisFromArray() {

            for (var i: number = 0; i < this.enemArray.length; i++) {
                this.enemArray[i].destroy(true);
                this.remove(this.enemArray[i]);

            }

        }



       addObstacle(object:Phaser.Sprite,pointer:Phaser.Point) {
            var x = Math.floor(pointer.x);
            var y = Math.floor(pointer.y);


            x = x - (x % 50);
            y = y - (y % 50);

            object.x = x;
            object.y = y;
            this.add(object);
            object.body.setSize(50, 50);
          
        }

       update() {
         
           if (!this._gameControler.gamePaused && this._obstacleControler.checkCollisionObstale) {

               this.game.physics.arcade.overlap(this._gameControler.player, this, this.collisionHome, null, this);

             
           }
           
        

       }


       collisionHome(player, home) {
         
           if (this._obstacleControler.checkCollisionObstale && !this._obstacleControler.enemyCollision) {
               
                             if (!mobile && home.name == "home" && !this._gameControler.lastPositionPlayer.equals(new Phaser.Point(this._gameControler.player.x, this._gameControler.player.y))) {
                                 this._obstacleControler.checkCollisionObstale = false;
                                 console.log("home, home");
                              //   this._home.body.setSize(0, 0, 0, 0);
                                 this.game.time.events.add(Phaser.Timer.SECOND * 6, function () {
              
                                     this._home.body.setSize(50, 50, 0, 0);
              
              
              
              
                                 }, this);
                                 console.log("pokazuj kurwa szmato", this._gameControler.bylemTu);
                                 if (!this._gameControler.bylemTu) {
                                     console.log("pokazuj kurwa szmato", this._gameControler.bylemTu);
                                     this._gameControler.bylemTu = true;
                                     console.log("pokazuj kurwa szmato 0");
                                     this._gameControler.level.showDrawStartDiamond();
                                    
                                 }
                                // this._gameControler.lastPositionPlayer = new Phaser.Point(this._gameControler.player.x, this._gameControler.player.y);
                                 
                             }
               if (home.name == "hole") {
                   this._obstacleControler.checkCollisionObstale = false;
                   this._gameControler.player.stars();
                   this._gameControler.lastPositionPlayer = new Phaser.Point(0, 0);
                   home.play("hole", 24, false);
                   //   this._hole.body.setSize(0, 0, -1000, 700);
                   this._audioTrap.play();
                   this._panleGameControler.panelGame.changeLife();
                   // this._gameControler.level.stopMovePlayer(new Phaser.Point(home.x, home.y));
               }
               else if (home.name == "strikeStill") {
                   this._obstacleControler.checkCollisionObstale = false;
                   this._gameControler.player.stars();
                   this._panleGameControler.panelGame.changeLife();
                   this._gameControler.lastPositionPlayer = new Phaser.Point(0, 0);
                   //   this._gameControler.level.stopMovePlayer(new Phaser.Point(home.x, home.y));

               }


           }
       }


       collisionHole(player, hole) {
           
          // this._obstacleControler.checkCollisionObstale = false;
          
         

       }

       countFrames(start: number, end: number): Array<number> {
           var countArr: Array<number> = new Array();
           for (var i: number = start; i < end; i++) {

               countArr.push(i);


           }

           return countArr;


       }


    }

}