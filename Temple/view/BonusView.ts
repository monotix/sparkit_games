﻿module Temple {

    export class BounsView extends Phaser.Group {


        private _gameControler: GameControler = GameControler.getInstance();
        private _diamondsControler: DiamondsControler = DiamondsControler.getInstance();
        private _panelGameControler: PanelGameControler = PanelGameControler.getInstance();
        private _bonusArr: Array<string> = new Array("invin", "foot", "dig", "bonus1", "bonus2", "bonus3", "bomb", "target", "freez");
        private _bounusAnim: Phaser.Sprite;
        private _currentPlay: number = 0;
        constructor(game: Phaser.Game) {

            super(game);

            this._bounusAnim = this.game.add.sprite(0, 0, "bonus", 0, this);
            this._bounusAnim.animations.add("bomb", this.countFrames(9, 35), 24, false);
            this._bounusAnim.animations.add("target", this.countFrames(36, 64), 24, false);
            this._bounusAnim.animations.add("freez", this.countFrames(65, 92), 24, false);
            this._bounusAnim.animations.add("invin", this.countFrames(93, 120), 24, false);
            this._bounusAnim.animations.add("foot", this.countFrames(121, 148), 24, false);
            this._bounusAnim.animations.add("dig", this.countFrames(149, 176), 24, false);
            this._bounusAnim.animations.add("bonus1", this.countFrames(177, 204), 24, false);
            this._bounusAnim.animations.add("bonus2", this.countFrames(205, 232), 24, false);
            this._bounusAnim.animations.add("bonus3", this.countFrames(234, 260), 24, false);

        }


        playBonus() {
            console.log("play bonus", this._bonusArr[this._currentPlay]);

            if (this._bonusArr.length == this._currentPlay) this._currentPlay = 0;
            var str: string = this._bonusArr[this._currentPlay];
            this.setBonus();
            this.visible = true;
            this._bounusAnim.animations.getAnimation(str).onComplete.add(this.animStop, this);
            this._bounusAnim.play(str, 24, false);
            this._currentPlay++;

        }

        setBonus() {
          
            var str: string = this._bonusArr[this._currentPlay];
            if (str == "dig") {
                this._gameControler._digFasterCollectDiamond = 1;
                this.game.time.events.add(Phaser.Timer.SECOND * 5, function () {

                    this._gameControler._digFasterCollectDiamond = 0;




                }, this)

            }

            else if (str == "bonus1") {

                this._panelGameControler.panelGame.addBonus(1);

            }

            else if (str == "bonus2") {

                this._panelGameControler.panelGame.addBonus(1);

            }
            else if (str == "bonus3") {

                this._panelGameControler.panelGame.addBonus(1);

            }
            else if (str == "foot") {

                if (mobile) this._gameControler._footSpeedUp = 50;
                else this._gameControler._footSpeedUp = 2;
                this.game.time.events.add(Phaser.Timer.SECOND * 5, function () {

                    this._gameControler._footSpeedUp = 0;




                }, this)

            }

            else if (str == "invin") {

                this._gameControler.player.stars(5);

            }

        }

        animStop() {

            this.visible = false;

        }





        countFrames(start: number, end: number): Array<number> {
            var countArr: Array<number> = new Array();
            for (var i: number = start; i < end; i++) {

                countArr.push(i);


            }

            return countArr;


        }

        public chanegeCurrentPlay(num: number = 0) {

            this._currentPlay = num;
        }


    }


} 