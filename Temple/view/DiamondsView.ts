﻿module Temple {

    export class DiamondsView extends Phaser.Group {


        private _gameControler: GameControler = GameControler.getInstance();
        private _obstacleControler: ObstacleControler = ObstacleControler.getInstance();
        private _diamondsControler: DiamondsControler = DiamondsControler.getInstance();
        private _diamond: Diamond;
        private _diamondArr: Array<string> = new Array("blueDiamond", "redDiamond", "greenDiamond");
        private _actualDiamond: Array<Diamond>;
        constructor(game: Phaser.Game) {


            super(game);
            this.enableBody = true;
            //this._diamond = new Diamond(this.game, 0, 0, "greenDiamond");
            // this._diamond.anchor.setTo(0.5, 0.5);
            //  this.whereToPut(new Phaser.Point(300, 200),this._diamond);
            this.addDiamonds();
        }


        addDiamonds() {

            var arr: Array<any> = this._diamondsControler.level();
            this._actualDiamond = new Array();
            for (var i: number = 0; i < arr.length; i++) {

                var diamond: Diamond = new Diamond(this.game, 0, 0, this._diamondArr[this.getRandom(0, 3)]);
                diamond.ktory = i;
                this.whereToPut((arr[i]), diamond);
                this._actualDiamond.push(diamond);

            }
            //console.log("ile diamentow", this._actualDiamond.length);

            this._actualDiamond = this.unique(this._actualDiamond);
            //console.log("ile diamentow po", this._actualDiamond.length);
            this.addDiamondToStage();
          //  this._actualDiamond = arr;
        }

       addDiamondToStage() {

           for (var i: number = 0; i < this._actualDiamond.length; i++) {


               this.add(this._actualDiamond[i]);
           }



        }

       unique = function (origArr) {
           var newArray = [],
               origLen = origArr.length,
               found;
          var  x = 0;
          var  y = 0;

        for (x = 0; x < origLen; x++) {
            found = undefined;
            for (y = 0; y < newArray.length; y++) {
               
                if (origArr[x].x == newArray[y].x && origArr[x].y == newArray[y].y) {
                    console.log(origArr[x].name, newArray[y].name);
                    found = true;

                }
            }
            if (!found) newArray.push(origArr[x]);
        }
        return newArray;
    }

        whereToPut(pos, diamond: Diamond) {

         //   var x = Math.floor(pointer.x);
         //   var y = Math.floor(pointer.y);
            
         
            var posx: number = (pos.x % 14);
            var posy: number = Math.floor(pos.y % 9);
            if (posy > 9) posy = 9;
            if (posy < 1) posy = 1;
            if (posx < 0) posx = 1;
            if (posx == 0 && posy == 1) posx = 2;
            console.log("diamond", posx, posy);
            var ustawx: number = posx * 50 + 50;
            var ustawy: number = posy * 50 + 50;
         //   console.log("reszta z dzielenia",posx,posy);
            console.log("pozycja", ustawx, ustawy);


            //14 cwiartek jedno po 50
            //wiec 25 to bedzie 1

            //

          //  x = x - (x % 50);
          //  y = y - (y % 50);

            diamond.x = ustawx + (25 - diamond.width / 2);
            diamond.y = ustawy + (25 - diamond.height / 2);

           // this.add(diamond);

        }


        checkCollision() {


            this.game.physics.arcade.overlap(this._gameControler.player, this, this.collisionDiamond, null, this);
        }

        collisionDiamond(player, diamond: Diamond) {

           
            if (this.ktoryKwadrat(player.x+25, player.y+25, diamond.x, diamond.y)) {
                console.log("jest kolizja z diamentem??", diamond.name);
                diamond.diamondDeastroy();
            }
            

        }

        checkCollisionAlpha() {


            this.game.physics.arcade.overlap(this._gameControler.player, this, this.collisionDiamondAlpha, null, this);
        }

        collisionDiamondAlpha(player, diamond: Diamond) {

            for (var i: number = 0; i < this._actualDiamond.length; i++) {
                this._actualDiamond[i].alpha = 1;
             
               
            }
            if (this.ktoryKwadrat(player.x+25, player.y+25, diamond.x, diamond.y)) {
                console.log("jest kolizja z diamentem??", diamond.name);
                diamond.alpha = 0.5;
            }


        }

        ktoryKwadrat(posX,posY,dX,dY):boolean {
            var ok: boolean = false;
            var ok2: boolean = false;
            if (Math.floor(posX / 50) % 14 == Math.floor(dX / 50) % 14) {
                ok = true;
            }
            if (Math.floor(posY / 50) % 14 == Math.floor(dY / 50) % 14) {
                ok2 = true;
            }

            if (ok && ok2) {
                return true;
            }


            return false;
        }

        getRandom(min: number, max: number): number {

            return Math.floor(Math.random() * (max - min) + min);

        }

        restart() {
            
            /* this.forEach(function (item) {
 
                 
                 if (item != undefined) {
                    // item.body.setSize(0, 0, -1000, 700);
                     item.destroy(true);
                     console.log("usuwam");
                 }
 
             }, this);*/
            for (var i: number = 0; i < this._actualDiamond.length; i++) {
                this.remove(this._actualDiamond[i]);
                this._actualDiamond[i].kill();
                this._actualDiamond[i].destroy(true);
                this._actualDiamond[i] = null;
            }

            this.addDiamonds();


        }

       


    }

} 