﻿module Temple {

    export class PanelGame extends Phaser.Group {


        private _gameControler: GameControler = GameControler.getInstance();
        private _obstacleControler: ObstacleControler = ObstacleControler.getInstance();

       

        private _bg: Phaser.Sprite;
        private _time: Phaser.Text;

        private _gems: Phaser.Text;
        private _gemsCount: number = 0;
        private _bonus: Phaser.Text;
        private _bonusCount: number = 0;
        private _seconds: number = 60;
        private _lifes: Phaser.Text;
        private _lifeCount: number = 0;
        private _liveAnim: Phaser.Sprite;
        private _collectDiamonds: Phaser.Sprite;
        private _collectGroup: Phaser.Group;
        private _glow: Phaser.Sprite;
        
        private _diamonds: Array<any> = new Array(
            { diamond : "redDiamond", i : 1},
            { diamond: "blueDiamond", i : 2},
            { diamond: "greenDiamond", i :0}
            );

        constructor(game: Phaser.Game) {
            super(game);


          
           
          
            this._bg = this.game.add.sprite(-40, -20, "gameScore", 0, this);

            this._liveAnim = this.game.add.sprite(664, 18, "life", 0, this);

            this._collectGroup = this.game.add.group();
         
            this._collectGroup.x = -30;
            this.add(this._collectGroup);
            this._collectGroup.visible = false;
           

            var style = { font: "45px Arial", fill: "#ffffff", align: "left" };
            var style2 = { font: "25px Arial", fill: "#ffffff", align: "left" };
            this._time = this.game.add.text(360, 2, "60", style);


            this._gems = this.game.add.text(520, 17, "0", style2);

            this._bonus = this.game.add.text(593, 17, "0", style2);

            this._glow = new Phaser.Sprite(this.game, 0, 0, "glow");

            this.addChild(this._gems);
            this.addChild(this._bonus);
            this.addChild(this._time);
            this._glow.x = 28;
            this.addChild(this._glow);
            this.setGlow();
            this.startTimer();
        }

        public setGlow(num: number = 0) {

            this._glow.frame = DiamondsControler.getInstance().currentLevel;

        }


        public showCollectDiamond(diamond: string, kolejnosc: number = 0, show: boolean = true) {
            if (!show) {
                this._collectGroup.forEach(function (item) {
                    item.kill();
                }, this);
               // this._collectGroup.visible = false;
                return 0;
            }

            this._collectGroup.visible = true;

            var collectDiamonds = new Phaser.Sprite(this.game, 0, 0, "collectDiamond", 0);
            collectDiamonds.x =  (collectDiamonds.width * kolejnosc) + 5 * (kolejnosc + 1);
            this._collectGroup.add(collectDiamonds);

            var i: number = this.checkIndecDiamond(diamond);
           // this._collectDiamonds.frame = i;

        }

       public removeCollectDiamond() {

            this._collectGroup.forEach(function (item) {
                item.kill();
            }, this);
        }


        checkIndecDiamond(diamondName:string):number {

            for (var i: number = 0; i < this._diamonds.length; i++) {

                if (this._diamonds[i].diamond == diamondName) {

                    return this._diamonds[i].i;
                }
                    
            }

            return 0;

        }



        changeLife() {
            if (this._lifeCount >= 3) return;
            this._lifeCount++;

            this._liveAnim.frame = this._lifeCount;
           // this._lifes.setText("" + this._lifeCount);

            if (this._lifeCount >= 3) {
                this._gameControler.gamePaused = true;
                this._gameControler.player.death();
           

            }
        }

        restartLife() {
            this._lifeCount = 0;
            this._liveAnim.frame = 0;
        }

        resetGems() {
            this._gemsCount =0;

            this._gems.setText("" + this._gemsCount);
            this.startTimer();
        }

        addGems() {
           
            this._gemsCount += 5;
           
            this._gems.setText("" + this._gemsCount);

        }

        addBonus(ile:number) {
           
            if (ile == -1) this._bonusCount = 1;
            this._bonusCount += ile;

            this._bonus.setText("" + this._bonusCount);

        }


        public startTimer() {
          
            this.resetTimer();
            this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);
        }

        public stopTimer() {

          
            this.game.time.events.removeAll();
        }


       
        
        private resetTimer() {
            console.log("RESET CZAS");
          //  this._time.setText("00:00");
          //  this._seconds = 0;
            
        }

        private updateTimer() {

            if (!this._gameControler.gamePaused) {
                if (this._seconds > 0) {

                    this._seconds--;
                }
                else {
                    this.stopTimer();
                    this._seconds = 60;
                    this._gameControler.endGame(this._gemsCount > 4);

                }

                var sec: String;


                sec = "" + this._seconds;




                this._time.setText("" + sec);
            }

        }
    }

}