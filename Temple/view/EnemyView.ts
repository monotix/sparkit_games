﻿module Temple {

    export class EnemyView extends Phaser.Group {


        private _gameControler: GameControler = GameControler.getInstance();
       
        private _diamondControler: DiamondsControler = DiamondsControler.getInstance();
        private _obstacleControler: ObstacleControler = ObstacleControler.getInstance();
        private _panleGameControler: PanelGameControler = PanelGameControler.getInstance();
        
        private positionVector: Phaser.Point;
        private moveVector: Phaser.Point;
        private enemArray: Array<Enemy>;
        private _bounds: Phaser.Sprite;
        private _boundsGr: Phaser.Group;
        constructor(game: Phaser.Game) {

            super(game);
            this.enableBody = true;
         

            /*
           
            var gra: Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
            gra.beginFill(0x000000, 0.5);
            gra.drawRect(50, 100, 700, 450);
            gra.endFill();

            this._boundsGr = this.game.add.group();
            this._boundsGr.enableBody = true;
            this._boundsGr.name = "bounds";
            this.add(this._boundsGr);

            this._bounds = new Phaser.Sprite(this.game, 0, 0, "",this._boundsGr);
            this._bounds.name = "bounds";
            this._bounds.addChild(gra);
            this._boundsGr.add(this._bounds);
            */
            //this.add(this._bounds);
            //this._bounds.checkWorldBounds = true;
          
            this.addEnemis();

         }


        addEnemis() {

            var currentLevel = this._diamondControler.currentLevel;
            this.enemArray = new Array();
            if (currentLevel == 0) {


            }
            else if (currentLevel == 1) {

                var _enemy = new Enemy(this.game, 0, 0, "enemy2");
                _enemy.position.setTo(400, 200);
                this.game.physics.enable(_enemy, Phaser.Physics.ARCADE);
               
                _enemy.body.setSize(50, 25, 0, 5);

                this.enemArray.push(_enemy);

            }
            else if (currentLevel == 2) {

                var _enemy = new Enemy(this.game, 0, 0, "enemy2");
                _enemy.position.setTo(400, 200);
                this.game.physics.enable(_enemy, Phaser.Physics.ARCADE);

                _enemy.body.setSize(50, 25, 0, 5);

                var _enemyHand = new Enemy(this.game, 0, 0, "enemy1");
                _enemyHand.position.setTo(300, 300);
                this.game.physics.enable(_enemyHand, Phaser.Physics.ARCADE);

                _enemyHand.body.setSize(50, 50, 0, 10);
                this.enemArray.push(_enemy);
                this.enemArray.push(_enemyHand);
            }

            else if (currentLevel == 3) {

                var _enemy = new Enemy(this.game, 0, 0, "enemy2");
                _enemy.position.setTo(400, 200);
                this.game.physics.enable(_enemy, Phaser.Physics.ARCADE);

                _enemy.body.setSize(50, 25, 0, 5);

                var _enemyHand = new Enemy(this.game, 0, 0, "enemy1");
                _enemyHand.position.setTo(300, 300);
                this.game.physics.enable(_enemyHand, Phaser.Physics.ARCADE);

                _enemyHand.body.setSize(50, 50, 0, 10);
                var _enemyHand2 = new Enemy(this.game, 0, 0, "enemy1");
                _enemyHand2.position.setTo(300, 300);
                this.game.physics.enable(_enemyHand2, Phaser.Physics.ARCADE);

                _enemyHand2.body.setSize(50, 50, 0, 10);

                this.enemArray.push(_enemy);
                this.enemArray.push(_enemyHand);
              
                this.enemArray.push(_enemyHand2);

            }
            else if (currentLevel == 4) {


                var _enemy = new Enemy(this.game, 0, 0, "enemy2");
                _enemy.position.setTo(400, 200);
                this.game.physics.enable(_enemy, Phaser.Physics.ARCADE);

                _enemy.body.setSize(50, 25, 0, 5);

                var _enemyHand = new Enemy(this.game, 0, 0, "enemy1");
                _enemyHand.position.setTo(300, 300);

                this.game.physics.enable(_enemyHand, Phaser.Physics.ARCADE);

                _enemyHand.body.setSize(50, 50, 0, 10);

                var _enemyHand2 = new Enemy(this.game, 0, 0, "enemy1");
                _enemyHand2.position.setTo(300, 300);

                this.game.physics.enable(_enemyHand2, Phaser.Physics.ARCADE);

                _enemyHand2.body.setSize(50, 50, 0, 10);

                this.enemArray.push(_enemy);
                this.enemArray.push(_enemyHand);

                this.enemArray.push(_enemyHand2);
            }

            else if (currentLevel == 5) {


                var _enemy = new Enemy(this.game, 0, 0, "enemy2");
                _enemy.position.setTo(400, 200);
                this.game.physics.enable(_enemy, Phaser.Physics.ARCADE);

                _enemy.body.setSize(50, 25, 0, 5);
                var _enemyHand = new Enemy(this.game, 0, 0, "enemy1");
                _enemyHand.position.setTo(300, 300);

                this.game.physics.enable(_enemyHand, Phaser.Physics.ARCADE);

                _enemyHand.body.setSize(50, 50, 0, 10);

                var _enemyHand2 = new Enemy(this.game, 0, 0, "enemy1");
                _enemyHand2.position.setTo(300, 300);

                this.game.physics.enable(_enemyHand2, Phaser.Physics.ARCADE);

                _enemyHand2.body.setSize(50, 50, 0, 10);

                this.enemArray.push(_enemy);
                this.enemArray.push(_enemyHand);

                this.enemArray.push(_enemyHand2);
            }

            this.addEnemisFromArra();
        }

        addEnemisFromArra() {

            for (var i: number = 0; i < this.enemArray.length; i++) {

                this.add(this.enemArray[i]);

            }

        }


        removeEnemisFromArray() {

            for (var i: number = 0; i < this.enemArray.length; i++) {
                this.enemArray[i].destroy(true);
                this.remove(this.enemArray[i]);

            }

        }



        update() {

           // this.positionVector.setTo((this.positionVector.x + (this.moveVector.x)),(this.positionVector.y + (this.moveVector.y)));
            
           //
           // this._enemy.moveTo();

           // console.log("gamepasued", this._gameControler.gamePaused, this._obstacleControler.checkCollisionObstale);
            if (!this._gameControler.gamePaused && this._obstacleControler.checkCollisionObstale && !this._obstacleControler.enemyCollision) {

                this.game.physics.arcade.overlap(this._gameControler.player, this, this.collisionEnemy, null, this);

                

            }

            if (this.enemArray != null && this.enemArray.length > 0) {

                for (var i: number = 0; i < this.enemArray.length; i++) {
             
                    if (this.enemArray[i].x < 74) {
                        this.enemArray[i].x = 75;
                        this.enemArray[i].makeStartEnemy();
                    }
                    else if(this.enemArray[i].x > 726) {
                        this.enemArray[i].x = 725;
                        this.enemArray[i].makeStartEnemy();
                    }
                    if (this.enemArray[i].y < 124 ) {
                        this.enemArray[i].y = 125;
                        this.enemArray[i].makeStartEnemy();

                    }
                    else if (this.enemArray[i].y > 526){
                        this.enemArray[i].y = 525;
                        this.enemArray[i].makeStartEnemy();

                    }
                   


                }

            }

            //this._enemy.update();
            //this._enemyHand.update();
        }

        stopEnemy() {
            this.forEach(function (item) {
               
                if(item.name !="bounds") item.makeStopEnemy();
            }, this);

        }

        startEnemy() {

            this.forEach(function (item) {
              
                if (item.name != "bounds") item.makeStartEnemy();
                
            }, this);

        }

        collisionEnemy(player, enemy:Enemy) {
             
           // this._gameControler.gamePaused = true;
            enemy.playAudio();
            this._gameControler.player.stars();
            this._panleGameControler.panelGame.changeLife();
          

        }


        restart() {
            this.forEach(function (item) {

               
                if (item.name != "bounds") item.restart();
            }, this);

        }


    }


}