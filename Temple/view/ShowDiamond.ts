﻿module Temple {

    export class ShowDiamond extends Phaser.Group {

        private _gameControler: GameControler = GameControler.getInstance();
        private _obstacleControler: ObstacleControler = ObstacleControler.getInstance();
        private _diamondsControler: DiamondsControler = DiamondsControler.getInstance();
        private _bgBlack: Phaser.Graphics;
        private _titleCircle: Phaser.Sprite;
        private _diamondArr: Array<string> = new Array("blueDiamond", "redDiamond", "greenDiamond");
        private _drawDiamonArr: Array<string>;
        private _currentDiamond: number = 0;
        private _maxDiamond: number = 2;
        private _diamond: Phaser.Sprite;
        private _audio: Phaser.Sound;
        constructor(game: Phaser.Game) {

            super(game);

            this._bgBlack = new Phaser.Graphics(this.game, 0, 0);
            this._bgBlack.beginFill(0x000000, 0.6);
            this._bgBlack.drawRect(0, 0, this.game.width, this.game.height);
            this._bgBlack.endFill();
            this.add(this._bgBlack);


            this._titleCircle = this.game.add.sprite(0, 0, "titleCircle", 0, this);
            this._titleCircle.x = this.game.width / 2 - this._titleCircle.width / 2;
            this._titleCircle.y = this.game.height / 2 - this._titleCircle.height / 2;
         
            this._audio = this.game.add.audio("showdiamond", 1, false);
            this.drawDiamond();
        }

        show(visible: boolean) {

            this.visible = visible;

        }

        drawDiamond() {
            this._gameControler.blockWall = true;
            this._gameControler.enemyView.stopEnemy();
            this._drawDiamonArr = new Array();
            for (var i: number = 0; i < this._diamondsControler.maxDiamonds; i++) {
                this._drawDiamonArr.push(this._diamondArr[this.getRandom(0, this._diamondArr.length)]);
               


            }
            

            this._diamondsControler.setDrawDiamonds(this._drawDiamonArr);
            this.showDrawDiamon();
            console.log("drawDiamond");

        }


        private blokadaShow: boolean = false;

        showDrawDiamon() {
            console.log("showDrawDiamon w klasie", this._currentDiamond);
            if (!this.blokadaShow) {
                this.blokadaShow = true;
                this.showPoblokadzie();

            }

        }

        showPoblokadzie() {

            this.visible = true;
            this._gameControler.blockWall = true;
            if (this._currentDiamond == 0) {
                this._gameControler.enemyView.stopEnemy();
                this._gameControler.gamePaused = true;
            }
            if (this._currentDiamond == this._diamondsControler.maxDiamonds) {


                this.visible = false;
                this._currentDiamond = 0;
                this._obstacleControler.checkCollisionObstale = true;

                this._gameControler.gamePaused = false;
                this._gameControler.blockWall = false;
                // console.log("whcodze i sie nie godze", this._gameControler.gamePaused, this._obstacleControler.checkCollisionObstale);
                this._gameControler.enemyView.startEnemy();
                this._gameControler.gamePaused = false;
                this.blokadaShow = false;
            }
            else {
                this._audio.play();
                this.addDiamond(this._drawDiamonArr[this._currentDiamond]);
                this.game.time.events.add(Phaser.Timer.SECOND * 2, function () {

                    this.removeChild(this._diamond);




                }, this);

                this.game.time.events.add(Phaser.Timer.SECOND * 2.5, function () {


                    this.showPoblokadzie();



                }, this);

                this._currentDiamond++;
            }

        }

        addDiamond(key:string) {

            this._diamond = new Phaser.Sprite(this.game, 0, 0, key);
            this._diamond.x = this.game.width / 2 - this._diamond.width / 2;
            this._diamond.y = this.game.height / 2 - this._diamond.height / 2;
            this.add(this._diamond);

        }

        getRandom(min: number, max: number): number {
            max;
            return Math.floor(Math.random() * (max - min) + min);

        }

    }

}