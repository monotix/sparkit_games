﻿module Temple {

    export class WallDoorsView extends Phaser.Group {

        private _gameControler: GameControler = GameControler.getInstance();
        private _wallGr: Phaser.Group;
        constructor(game: Phaser.Game) {

            super(game);

            console.log("chujnia");
            this.enableBody = true;
            
            var wall1 = new Phaser.Sprite(this.game, 400, 75, "wallDoor", 0);
            wall1.name = "door";
            wall1.anchor.setTo(0.5, 0.5);
            wall1.angle = 0;
            var wall2 = new Phaser.Sprite(this.game, 25, 300, "wallDoor", 0);
            wall2.anchor.setTo(0.5, 0.5);
            wall2.name = "door";
            wall2.angle = 270;
            var wall3 = new Phaser.Sprite(this.game, 775, 300, "wallDoor", 0);
            wall3.anchor.setTo(0.5, 0.5);
            wall3.angle = 90;
            wall3.name = "door";
            var wall4 = new Phaser.Sprite(this.game, 400, 575, "wallDoor", 0);
            wall4.anchor.setTo(0.5, 0.5);
            wall4.angle = 180;
            wall4.name = "door";

            
            this.add(wall1);
            this.add(wall2);
            this.add(wall3);
            this.add(wall4);

            wall1.body.setSize(25, 50);
            wall2.body.setSize(25, 50);
            wall3.body.setSize(25, 50);
            wall4.body.setSize(25, 50);

            this._wallGr = this.game.add.group();
            this._wallGr.y = 150;
            this._wallGr.x = 50;
            this._wallGr.enableBody = true;
            this.addWalls();
            this._wallGr.alpha = 0;
        }


        obrysujScene() {
            var marx: number = 0;
            var mary: number = 50;
            for (var i: number = 0; i < 1; i++) {

                for (var j: number = 0; j < 16; j++) {

                   
                    if (j == 7 || j == 8) {


                    }
                    else {

                        var wall: Wall = new Wall(this.game, 0, 0);
                        wall.x = marx + 50 * j;
                        wall.y = mary + 50 * i;

                        this._wallGr.add(wall);
                        wall.body.setSize(50, 50);
                    }
                }


            }
            mary = 100;
            for (var i: number = 0; i < 9; i++) {

                for (var j: number = 0; j < 1; j++) {


                    if (i == 3 || i == 4) {


                    }
                    else {

                        var wall: Wall = new Wall(this.game, 0, 0);
                        wall.x = marx + 50 * j;
                        wall.y = mary + 50 * i;

                        this._wallGr.add(wall);
                        wall.body.setSize(50, 50);
                    }
                }


            }

            mary = 100;
            marx = 750;
            for (var i: number = 0; i < 9; i++) {

                for (var j: number = 0; j < 1; j++) {


                    if (i == 3 || i == 4) {


                    }
                    else {

                        var wall: Wall = new Wall(this.game, 0, 0);
                        wall.x = marx + 50 * j;
                        wall.y = mary + 50 * i;

                        this._wallGr.add(wall);
                        wall.body.setSize(50, 50);
                    }
                }


            }

            marx = 0;
            mary = 550;
            for (var i: number = 0; i < 1; i++) {

                for (var j: number = 0; j < 16; j++) {


                    if (j == 7 || j == 8) {


                    }
                    else {

                        var wall: Wall = new Wall(this.game, 0, 0);
                        wall.x = marx + 50 * j;
                        wall.y = mary + 50 * i;

                        this._wallGr.add(wall);
                        wall.body.setSize(50, 50);
                    }
                }


            }



        }

        addWalls() {

            this.obrysujScene();

        }
        
        create() {

          

        }

        update() {
         
            if (!this._gameControler.gamePaused && !this._gameControler.collisonWall ) {

                this.game.physics.arcade.overlap(this._gameControler.player, this, this.collision, null, this);
                this.game.physics.arcade.overlap(this._gameControler.player, this._wallGr, this.kolizjaSciany, null, this);
            }
           
           // this.game.physics.arcade.overlap(this._gameControler.player, this, this.collision, null, this);

        }


        kolizjaSciany(player,wall) {

            console.log("kolizja sciana");
            var przesunX: number = 0;
            var przesunY: number = 0;
            
            console.log("przesunX", "przesunY", wall.x, wall.y, Math.floor(this._gameControler.player.x), Math.floor(this._gameControler.player.y));
            if (wall.x == this._gameControler.player.x ) {

                if (wall.y < this._gameControler.player.y) {
                    przesunY = 50;

                }
                else {

                    przesunY = 0;
                }

            }
            else if (wall.y == this._gameControler.player.y) {

                if (wall.x < this._gameControler.player.x) {
                    przesunX = 50;

                }
                else {

                    przesunX = 0;
                }

            }
            var stopIsc: boolean = false;
            if (wall.x != this._gameControler.player.x && wall.y != this._gameControler.player.y) stopIsc = true;

            if (wall.y == this._gameControler.player.y && wall.x == this._gameControler.player.x){

                przesunX = 0;
                przesunY = 0;
                stopIsc = true;

            }
           

            if (!stopIsc) this._gameControler.level.stopMovePlayer(new Phaser.Point(this._gameControler.player.x + przesunX, this._gameControler.player.y + przesunY));
            else {


            }
            console.log("kolizja sciana");
        }


        collision(player,wall) {
            if (wall.name == "door") {
                if (!this._gameControler.gamePaused && !this._gameControler.collisonWall) {
                    if (wall.position.x < 50) {
                        this._gameControler.player.x = 800;
                        this._gameControler.level.playMoveTo(new Phaser.Point(725, player.y));

                    }
                    else if (wall.position.x > 700) {
                        this._gameControler.player.x = -50;
                        this._gameControler.level.playMoveTo(new Phaser.Point(50, player.y));

                    }

                    if (wall.position.y < 100) {
                        this._gameControler.player.y = 550;
                        this._gameControler.level.playMoveTo(new Phaser.Point(player.x, 500));

                    }
                    else if (wall.position.y > 560) {
                        this._gameControler.player.y = -50;
                        this._gameControler.level.playMoveTo(new Phaser.Point(player.x, 100));


                    }
                }
            }
            else {
              

            }
        }


    }

}