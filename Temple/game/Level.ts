﻿module Temple {

    export class Level extends Phaser.State{
        

        private _gameBg: Phaser.Sprite;
        private _player: Player;
        private _gridPlayer: Phaser.Point;
        private _obrysGr: Phaser.Group;
        private _wallGroup: WallDoorsView;
        private _gameControler: GameControler;
        private _showDiamond: ShowDiamond;
        private _finishScreen: FinishScreen;
        private _obstacleView: ObstacleView;
        private _positionLeftRight: Phaser.Point = new Phaser.Point(0, 0);
        private _obstacleControler: ObstacleControler = ObstacleControler.getInstance();
        private _tween: Phaser.Tween;
        private _enemyView: EnemyView;
        private _diamondsView: DiamondsView;
        private _lastPosition: Phaser.Point = new Phaser.Point(0, 0);
        private _panelGame: PanelGame;
        private _bonusView: BounsView;
        private blockedLayer;
        private map;
        private cursors;
        private fireButton;
        
      
        create() {

            //the first parameter is the tileset name as specified in Tiled, the second is the key to the asset
        
            this.game.world.setBounds(50, 150,750, 550);

            this._gameBg = this.game.add.sprite(0, 0, "bgGame");
            this._gameBg.x = 50;
            this._gameBg.y = 200;
            
            this._obrysGr = this.game.add.group();
            this._player = new Player(this.game, "player");
          
            this.game.physics.enable(this.player, Phaser.Physics.ARCADE);
            this._player.body.setSize(50, 50, 0, 0);
            this._player.x = 50;
            this._player.y = 100;
         
           // this._player.anchor.setTo(0.65, 0.2);
           // this._player.body.setSize(50,50,-18,110)
           // this._player.input.enableSnap(32, 32, false);

            this._gameControler = GameControler.getInstance();
            this._gameControler.setLevel(this);
            DiamondsControler.getInstance().currentLevel = userLevel - 1;
           

            this._gridPlayer = new Phaser.Point(0, 0);
            

           // this._player.gridPosition = new Phaser.Point(0, 0);
            this.game.input.onDown.add(this.movePlayer, this);
           // this.obrysujScene();

            this._wallGroup = new WallDoorsView(this.game);
            this._wallGroup.y = 0;
            this._wallGroup.x = 0;
            this.stage.addChild(this._wallGroup);


            this._obstacleView = new ObstacleView(this.game);

            this.stage.addChild(this._obstacleView);

            this._diamondsView = new DiamondsView(this.game);

            this.stage.addChild(this._diamondsView);

            this.stage.addChild(this._player);

            this._enemyView = new EnemyView(this.game);
            this.stage.addChild(this._enemyView);
            this._enemyView.x = 0;
            this._enemyView.y = 0;

            this._gameControler.enemyView = this._enemyView;

            this._panelGame = new PanelGame(this.game);
            this.stage.addChild(this._panelGame);

            this._showDiamond = new ShowDiamond(this.game);
            this.stage.addChild(this._showDiamond);

            this._bonusView = new BounsView(this.game);
            this._bonusView.x = 400;
            this._bonusView.y = 100;

            this.stage.addChild(this._bonusView);


            this._gameControler.bonuseView = this._bonusView;
           
          
            DiamondsControler.getInstance().setShowDiamonds = this._showDiamond;
            PanelGameControler.getInstance().setPanelGame = this._panelGame;
          
            this.cursors = this.game.input.keyboard.createCursorKeys();
            this.fireButton = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
            this.game.input.keyboard.addKeyCapture([
                Phaser.Keyboard.LEFT,
                Phaser.Keyboard.RIGHT,
                Phaser.Keyboard.UP,
                Phaser.Keyboard.DOWN,
                Phaser.Keyboard.SPACEBAR
            ]);
        

        }

        
        


        obrysujScene() {
            var marx: number = 50;
            var mary: number = 100;
            for (var i: number = 0; i < 9; i++) {

                for (var j: number = 0; j < 14; j++) {

                    var gra: Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
                    gra.beginFill(Phaser.Color.getRandomColor(), 1);
                    gra.drawRect(0, 0, 50, 50);
                    gra.endFill();

                    gra.x = marx + 50 * j;
                    gra.y = mary + 50 * i;

                    this._obrysGr.add(gra);

                }


            }



        }

        public showFinish(win:boolean) {

            this._finishScreen = new FinishScreen(this.game);
            this._finishScreen.show(win);

            this.stage.addChild(this._finishScreen);
          
        }


        showDrawDiamond() {
            console.log("wywoluje z klasy level showDrawDiamond ile razy");
           // this._showDiamond.showDrawDiamon();
         
        }

        showDrawStartDiamond() {
            console.log("pokazuj kurwa szmato 1");
            this._showDiamond.drawDiamond();
        }


        playDeath() {

            this._player.death();

        }

        playAgainAfterDeath() {
            
            console.log("playe after death");
            this._panelGame.restartLife();
            this._player.stand();
            this._player.x = 50;
            this._player.y = 100;
            this.miejscePocztakowe(this._player.x, this._player.y);
            this._positionLeftRight = new Phaser.Point(this._player.x, this._player.y);
           // this._showDiamond.drawDiamond();

        }

        restartGame() {
            this._finishScreen = null;
            this._player.x = 100;
            this._player.y = 100;
            this._enemyView.restart();
            this._diamondsView.restart();
            this.showDrawStartDiamond();
            this._panelGame.resetGems();
            this._panelGame.restartLife();
            this._panelGame.removeCollectDiamond();
            this._panelGame.addBonus(-1);
            this._bonusView.chanegeCurrentPlay();
            this._enemyView.removeEnemisFromArray();
            this._enemyView.addEnemis();
            this._obstacleView.removeEnemisFromArray();
            this._obstacleView.addEnemis();
            this._panelGame.showCollectDiamond("", 0, false);


        }

        playMoveTo(point: Phaser.Point) {

            if (!this._gameControler.collisonWall) {
                this._gameControler.collisonWall = true;
                var x = Math.floor(point.x);
                var y = Math.floor(point.y);


                x = x - (x % 50);
                y = y - (y % 50);



                var tween = this.game.add.tween(this._player).to({ x: x, y: y }, 500, Phaser.Easing.Quadratic.InOut, true);
                tween.onComplete.add(this.moveComplete, this);


            }
           

        }

        moveComplete() {

            this._gameControler.collisonWall = false;

        }

        movePlayerComplete() {

          //  this._gameControler.gamePaused = false;
          
            if (!this._obstacleControler.checkCollisionObstale && !this._gameControler.lastPositionPlayer.equals(new Phaser.Point(this._player.x,this._player.y))) {
               // console.log("sprawdzam pozycje", this._gameControler.lastPositionPlayer.equals(new Phaser.Point(this._player.x, this._player.y)))
              //  console.log("powinien satnac??");
               
               // this._obstacleControler.checkCollisionObstale = true;
            }

            this.miejscePocztakowe(this._player.x, this._player.y);
            if (!this._gameControler.gamePaused) this._player.stand();
        }

        movePlayer(pointer) {
            if (!mobile) return 0;
            console.log("this._gameControler.gamePaused", !this._gameControler.gamePaused, !this._gameControler.blockWall,(this._tween == null || !this._tween.isRunning));
            if (!this._gameControler.blockWall && !this._gameControler.gamePaused && (this._tween == null || !this._tween.isRunning) ) {
                //this._gameControler.gamePaused = true;
                var x = Math.floor(pointer.x);
                var y = Math.floor(pointer.y);


                x = x - (x % 50);
                y = y - (y % 50);
                
                var duration = (this.game.physics.arcade.distanceToPointer(this._player, pointer) / (200 + this._gameControler._footSpeedUp)) * 1000;
             
                if (this._lastPosition.equals(new Phaser.Point(x, y))) {

                    this._player.diamond();
                    this._diamondsView.checkCollision();

                } else {

                    this._lastPosition = new Phaser.Point(x, y);

                }
                this.whereToMove(new Phaser.Point(x, y));
                var wchodze: boolean = false;
                if (this._player.x == x) wchodze = true;
                else if (this._player.y == y) wchodze = true;

                if (wchodze) {
                    console.log("bylem tu:", this._gameControler.bylemTu, x, y);
               /*     if (this._gameControler.bylemTu && x == 50 && y == 100) {

                    }
                    else if (this._gameControler.bylemTu) {
                        this._obstacleControler.checkCollisionObstale = true;
                        this._gameControler.bylemTu = false;
                    }*/
                    this._tween = this.game.add.tween(this._player).to({ x: x, y: y }, duration, Phaser.Easing.Quadratic.InOut, true);
                    this._tween.onComplete.add(this.movePlayerComplete, this);
                    this._positionLeftRight = new Phaser.Point(x, y);

                }
               
            }
        }

        stopTweenPlayer() {

            
            this.game.tweens.remove(this._tween);

        }

        stopMovePlayer(point:Phaser.Point) {

            this.game.tweens.remove(this._tween);
            if (this._tween != null && this._tween.isRunning) {
                this._tween.stop();
                this._tween = null;
            }
            var sc = new Phaser.Pointer(this.game, 1);
            sc.position.setTo(point.x, point.y);
            var x = Math.floor(sc.position.x);
            var y = Math.floor(sc.position.y);


            x = x - (x % 50);
            y = y - (y % 50);
           // var duration = (this.game.physics.arcade.distanceToPointer(this._player, new) / 200) * 1000;
            this._player.position.setTo(x, y);
           // this._tween = this.game.add.tween(this._player).to({ x: point.x, y: point.y }, 0, Phaser.Easing.Quadratic.InOut, true);
            //this._tween.onComplete.add(this.movePlayerComplete, this);

           // this._player.position.setTo(point.x, point.y);


        }

        whereToMove(point) {

            if (this._positionLeftRight.x == point.x) {

                if (this._positionLeftRight.y < point.y) {

                    this._player.down();
                }

                else if (this._positionLeftRight.y > point.y) {

                    this._player.up();
                }

            }

            if (this._positionLeftRight.y == point.y) {


                if (this._positionLeftRight.x < point.x) {

                    this._player.right();
                }

                else if (this._positionLeftRight.x > point.x) {

                    this._player.left();
                }

            }

        }

        private _speedPlayer: number = 2;
        private _spaceActive: boolean = false;
        private _isMove: boolean = false;
       update() {
           if (mobile) return 0;
          // this.game.physics.arcade.overlap(this._player, this._wallGroup, this.collision, null, this);
           if (!this._gameControler.blockWall && !this._gameControler.gamePaused && (this._tween == null || !this._tween.isRunning)) {
               this._diamondsView.checkCollisionAlpha();
               this._isMove = false;
               if (this.fireButton.isDown && !this._spaceActive) {
                 
                   this._spaceActive = true;
                   this._player.diamond();
                   this._diamondsView.checkCollision();
               }
               else if (!this.fireButton.isDown && this._spaceActive) {
                 
                   this._spaceActive = false;
               }


               else if (this.cursors.right.isDown) {
                   this._player.x += this._speedPlayer + this._gameControler._footSpeedUp ;
                   this._player.right();
                   this._isMove = true;
               }

              else if (this.cursors.left.isDown) {
                   this._player.x -= this._speedPlayer + this._gameControler._footSpeedUp;;
                   this._player.left();
                   this._isMove = true;
               }

             else  if (this.cursors.up.isDown) {
                   this._player.y -= this._speedPlayer + this._gameControler._footSpeedUp;
                   this._player.up();
                   this._isMove = true;
               }

              else if (this.cursors.down.isDown) {
                   this._player.y += this._speedPlayer + this._gameControler._footSpeedUp;
                   this._player.down();
                   this._isMove = true;
               }

             //  this.ktoryKwadrat(this._player.x, this._player.y);
             //console.log("jaka cwiartka x",(this._player.x));
             //console.log("jaka cwiartka y",(this._player.y));
             if (this._isMove) {
                 this.ktoryKwadrat(this._player.x, this._player.y);
                 this.miejscePocztakowe(this._player.x, this._player.y);
             }
               else if (!this.fireButton.isDown) this._player.stand();
           }

        }

       private _firstPos: boolean = false;
       private _changeFirstPos: boolean = false;

       miejscePocztakowe(posX,posY) {
           console.log("miejsce poczatkowe", Math.floor(this._player.y / 50) % 9 == 2,Math.floor(this._player.x / 50) % 14 == 1 , !this._firstPos);
           if (Math.floor(this._player.y / 50) % 9 == 2 && Math.floor(this._player.x / 50) % 14 == 1 && !this._firstPos) {
               console.log("miejsce poczatkowe wchodzi");
               if (posX < 51 && posY < 101) {
                   this._firstPos = true;
                   this.showDrawStartDiamond();
                   this._obstacleControler.checkCollisionObstale = false;
               }


           }

           else if (this._firstPos && (Math.floor(this._player.y / 50) % 9 != 2 || Math.floor(this._player.x / 50) % 14 != 1)) {
               this._firstPos = false;
           }



        

       }

       ktoryKwadrat(posX, posY) {
           var ok: boolean = false;
           var ok2: boolean = false;

           if (Math.floor(this._player.y / 50) % 9 != 2 || Math.floor(this._player.x / 50) % 14 != 1 && this._firstPos) {

               if (posX > 99 && posY > 149) {
                   this._firstPos = false;
                   this._obstacleControler.checkCollisionObstale = true;
               }
           }

           if (Math.floor(this._player.y / 50) % 9 == 1 && Math.floor(this._player.x / 50) % 14 == 7) {

               return 0;

           }

           
         
           if (Math.floor(this._player.y / 50) % 9 == 2 && Math.floor(this._player.x / 50) % 14 == 7) {

               return 0;

           }
          

           if (Math.floor(this._player.y / 50) % 9 == 5 && Math.floor(this._player.x / 50) % 14 == 0) {

               return 0;

           }

           if (Math.floor(this._player.y / 50) % 9 == 5 && Math.floor(this._player.x / 50) % 14 == 13) {

               return 0;

           }

           if (Math.floor(posX / 50) % 14 > 0 && Math.floor(posX / 50) % 14 < 14 && posX > 0 && posX < 800) {
               ok = true;
              // console.log("ok true");
           }
           else if (posX < 200) {
               this._player.x += this._speedPlayer;
           }
           else if (posX > 600) {
               this._player.x -= this._speedPlayer;
           }

           if (Math.floor(posY / 50) % 9 > 1 && Math.floor(posY / 50) % 9 < 9 && posY > 0 && posY < 600) {
               ok = true;
             //  console.log("ok true");
           }
           else if (posY < 200) {
               this._player.y += this._speedPlayer + this._gameControler._footSpeedUp;
           }
           else if (posY > 500) {
               this._player.y -= this._speedPlayer - this._gameControler._footSpeedUp;;
           }

           /*
           if (Math.floor(posY / 50) % 14 == Math.floor(dY / 50) % 14) {
               ok2 = true;
           }

           if (ok && ok2) {
               return true;
           }


           return false;*/
       }

       collision(player, wall) {

         //  console.log("kolizja ze sciana", wall);
          
       }

       render() {
          /*
             this.game.debug.quadTree(this.game.physics.arcade.quadTree);
       
             this.game.debug.body(this._player);
             this._wallGroup.forEach(function (item) {
                 // item.body.velocity = 0;
              
                 this.game.debug.body(item);
             }, this);
            
             this._obstacleView.forEach(function (item) {
                 // item.body.velocity = 0;
              
                 this.game.debug.body(item);
             }, this);
           */
          
       }


       public get player(): Player {

           return this._player;

       }
        
        
    }


} 