﻿module Temple {

    export class Instruction extends Phaser.State {

        private overButtonA: Phaser.Sound;
        private _mainMenuBg: Phaser.Sound;
        create() {

          
            var instruction = this.add.image(0, 0, "firstInstruction", 0);
            instruction.width = this.game.world.width;//this.stage.width;
            instruction.height = this.game.world.height;//this.stage.height;
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
          

            this._mainMenuBg = this.game.add.audio('instructionSn', 1, true);
            this._mainMenuBg.play("", 0, 1, false);

            var gameControler = GameControler.getInstance();
            gameControler.mainSound.volume = 0.05;
            // var instructionAudio = this.game.add.audio("chuja", 1, false);

            //  instructionAudio.play();
            var _buttons = this.add.group();

         

            var gameButton = this.add.button(0, 0, "games", this.onGameClick, this, 1, 0);
        
            
            _buttons.addChild(gameButton);

            var playButton = this.add.button(100, 0, "playGame", this.onPlayClick, this, 1, 0);
            playButton.onInputDown.add(this.overPlay2, this);


            _buttons.addChild(playButton);

            _buttons.x = 0;
            _buttons.y = this.game.stage.height - _buttons.height;

        }

        private onGameClick() {

            this.overButtonA.play();
            window.history.back();
        }

        private overPlay2() {

            this.overButtonA.play();

        }



        onPlayClick() {
            this._mainMenuBg.stop();
            this.game.state.start("Level", true, false);

        }



    }


} 