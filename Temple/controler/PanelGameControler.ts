﻿module Temple {

    export class PanelGameControler {

        public static _instance: PanelGameControler = null;

        private _panelGame: PanelGame;
        construct() {

            if (PanelGameControler._instance) {
                throw new Error("Error: Instantiation failed: Use PanelGameControler.getInstance() instead of new.");
            }
            PanelGameControler._instance = this;

        }


        public set setPanelGame(panelGame_: PanelGame) {

            this._panelGame = panelGame_;

        }

        public get panelGame(): PanelGame {

            return this._panelGame;
        }


        public static getInstance(): PanelGameControler {

            if (PanelGameControler._instance === null) {

                PanelGameControler._instance = new PanelGameControler();


            }


            return PanelGameControler._instance;



        }


    

    }


}  