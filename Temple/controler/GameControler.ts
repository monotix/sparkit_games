﻿module Temple {

    export class GameControler {

        public static _instance: GameControler = null;
       
        private _level: Level;
        private _gamePaused: boolean = true;
        public collisonWall: boolean = false;
        public blockWall: boolean = true;
        private _enemyView: EnemyView;
        public lastPositionPlayer: Phaser.Point = new Phaser.Point(0, 0);
        private _diamondsControler: DiamondsControler = DiamondsControler.getInstance();
        public enemyTap: boolean;
        private _bonuseView: BounsView;
        public _digFasterCollectDiamond: number = 0;
        public _footSpeedUp: number = 0;
        public bylemTu: boolean = true;
        public mainSound: Phaser.Sound;
        public puaseTime: boolean = false;
        construct() {

            if (GameControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            GameControler._instance = this;

        }


       


        public static getInstance(): GameControler {

            if (GameControler._instance === null) {

                GameControler._instance = new GameControler();


            }


            return GameControler._instance;



        }


        public get player():Player {

            return this._level.player;

        }

        setLevel(level: Level) {

            this._level = level;

        }

        public set enemyView(enemy: EnemyView) {

            this._enemyView = enemy;

        }

        public get enemyView(): EnemyView {

            return this._enemyView;

        }

        public get level(): Level {

            return this._level;
        }

        public get gamePaused(): boolean {

            return this._gamePaused;

        }

        public set gamePaused(paused: boolean) {

            this._gamePaused = paused;

        }

        public get bonuseView():BounsView {

            return this._bonuseView;
        }

        public set bonuseView(bonus: BounsView) {

            this._bonuseView = bonus;

        }


        endGame(win:boolean) {

            this.enemyView.stopEnemy();
            this._gamePaused = true;
            this.player.body.velocity.setTo(0, 0);
            this._diamondsControler.reset();
            this.player.stand();
            if (win) {
                this._diamondsControler.addMaxSize();
                PanelGameControler.getInstance().panelGame.setGlow();

            }
            this.level.showFinish(win);
           // this.level.showDrawStartDiamond();


        }

        public setSound(sound_: Phaser.Sound) {

            this.mainSound = sound_;
        }

    }


} 