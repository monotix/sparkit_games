﻿declare function APIsetLevel(userID, gameID, userLevel);
declare function APIgetLevel(userID, gameID);
declare var userID: any;
declare var gameID: any;

module Temple {

    export class DiamondsControler {

        public static _instance: DiamondsControler = null;

        private _drawDiamonds: Array<string>; 
        private _maxDiamonds: number = 2;
        private _currentDiamondArray: Array<any>;
        private _currentDiamondd: number = 0;
        private _showDiamonds: ShowDiamond;
        private _currentLevel: number = 0;
     //   private _ileZebrac: number = 2;
        private _panelGameControler: PanelGameControler = PanelGameControler.getInstance();
      //  private _gameControler: GameControler;
        construct() {

            if (DiamondsControler._instance) {
                throw new Error("Error: Instantiation failed: Use DiamondsControler.getInstance() instead of new.");
            }
            DiamondsControler._instance = this;

        }

        public set setShowDiamonds(showDiamonds: ShowDiamond) {

            this._showDiamonds = showDiamonds;

        }



        public static getInstance(): DiamondsControler {

            if (DiamondsControler._instance === null) {

                DiamondsControler._instance = new DiamondsControler();


            }


            return DiamondsControler._instance;



        }


        public setDrawDiamonds(diamondsArr: Array<string>) {

            this._drawDiamonds = diamondsArr;

        }



        public level():Array<any> {


            if (this._currentLevel == 0) {
                var arr: Array<any> = new Array(
                    { x: 15, y: 200 },
                    { x: 16, y: 200 },
                    { x: 17, y: 200 },
                    { x: 18, y: 200 },
                    { x: 19, y: 200 },
                    { x: 20, y: 200 },
                    { x: 21, y: 200 },
                    { x: 22, y: 200 },
                    { x: 23, y: 200 },
                    { x: 24, y: 250 },
                    { x: 25, y: 300 },
                    { x: 26, y: 350 },
                    { x: 27, y: 400 },
                    { x: 28, y: 450 },
                    { x: 29, y: 250 },
                    { x: 30, y: 300 },
                    { x: 31, y: 350 },
                    { x: 32, y: 400 }
                   
                    );
                /*

                 


                        { x: 26, y: 350 },
                    { x: 27, y: 400 },
                    { x: 28, y: 450 },
                    { x: 29, y: 250 },
                    { x: 30, y: 300 },
                    { x: 31, y: 350 },
                    { x: 20, y: 250 },
                    { x: 21, y: 250 },
                    { x: 22, y: 250 },
                    { x: 23, y: 250 },
                    { x: 24, y: 200 },
                    { x: 25, y: 200 },
                    { x: 26, y: 200 },
                    { x: 27, y: 200 },
                    { x: 28, y: 200 },
                    { x: 29, y: 300 },
                    { x: 30, y: 350 },
                    { x: 31, y: 250 },
                    { x: 32, y: 400 },
                    { x: 33, y: 450 },
                    { x: 34, y: 250 },
                    { x: 35, y: 300 },
                    { x: 36, y: 350 },
                    { x: 37, y: 400 }
                */
                return arr;

            }
            if (this._currentLevel == 1) {
                var arr: Array<any> = new Array(
                   
                    { x: 16, y: 200 },
                    { x: 17, y: 200 },
                    { x: 18, y: 200 },
                    { x: 19, y: 200 },
                    { x: 20, y: 200 },
                    { x: 21, y: 200 },
                    { x: 22, y: 200 },
                    { x: 23, y: 200 },
                    { x: 24, y: 250 },
                    { x: 25, y: 300 },

                    { x: 26, y: 350 },
                    { x: 27, y: 400 },
                    { x: 28, y: 450 },
                    { x: 29, y: 250 },
                    { x: 30, y: 300 },
                    { x: 31, y: 350 },
                    { x: 20, y: 250 },
                    { x: 21, y: 250 },
                    { x: 22, y: 250 },
                    { x: 23, y: 250 },
                    { x: 24, y: 200 },
                    { x: 25, y: 200 },
                    { x: 26, y: 200 },
                    { x: 27, y: 200 },
                    { x: 28, y: 200 },
                    { x: 29, y: 300 },
                    { x: 30, y: 350 },
                    { x: 31, y: 250 },
                    { x: 32, y: 400 },
                    { x: 33, y: 450 },
                    { x: 34, y: 250 },
                    { x: 35, y: 300 },
                    { x: 36, y: 350 },
                    { x: 37, y: 400 }
                
                    );

                return arr;

            }

            if (this._currentLevel == 2) {
                var arr: Array<any> = new Array(
                    { x: 15, y: 200 },
                    { x: 16, y: 200 },
                    { x: 17, y: 200 },
                    { x: 18, y: 200 },
                    { x: 19, y: 200 },
                    { x: 20, y: 200 },
                    { x: 21, y: 200 },
                    { x: 22, y: 200 },
                    { x: 23, y: 200 },
                    { x: 24, y: 250 },
                    { x: 25, y: 300 },
                    { x: 26, y: 350 },
                    { x: 27, y: 400 },
                    { x: 28, y: 450 },
                    { x: 29, y: 250 },
                    { x: 30, y: 300 },
                    { x: 31, y: 350 },
                    { x: 20, y: 200 },
                    { x: 21, y: 200 },
                    { x: 22, y: 200 },
                    { x: 23, y: 200 },
                    { x: 24, y: 200 },
                    { x: 25, y: 200 },
                    { x: 26, y: 200 },
                    { x: 27, y: 200 },
                    { x: 28, y: 200 },
                    { x: 29, y: 250 },
                    { x: 30, y: 300 },
                    { x: 31, y: 350 },
                    { x: 32, y: 400 },
                    { x: 33, y: 450 },
                    { x: 34, y: 250 },
                    { x: 35, y: 300 },
                    { x: 36, y: 350 },
                    { x: 37, y: 200 },
                    { x: 38, y: 200 },
                    { x: 39, y: 200 },
                    { x: 40, y: 200 },
                    { x: 41, y: 200 },
                    { x: 42, y: 200 },
                    { x: 43, y: 200 },
                    { x: 44, y: 200 },
                    { x: 45, y: 200 },
                    { x: 46, y: 250 },
                    { x: 47, y: 300 },
                    { x: 48, y: 350 },
                    { x: 49, y: 400 },
                    { x: 50, y: 450 },
                    { x: 51, y: 250 },
                    { x: 52, y: 300 },
                    { x: 53, y: 350 },
                    { x: 54, y: 400 }
                    );

                return arr;

            }

            if (this._currentLevel == 3) {
                var arr: Array<any> = new Array(
                    { x: 15, y: 200 },
                    { x: 16, y: 200 },
                    { x: 17, y: 200 },
                    { x: 18, y: 200 },
                    { x: 19, y: 200 },
                    { x: 20, y: 200 },
                    { x: 21, y: 200 },
                    { x: 22, y: 200 },
                    { x: 23, y: 200 },
                    { x: 24, y: 250 },
                    { x: 25, y: 300 },
                    { x: 26, y: 350 },
                    { x: 27, y: 400 },
                    { x: 28, y: 450 },
                    { x: 29, y: 250 },
                    { x: 30, y: 300 },
                    { x: 31, y: 350 },
                    { x: 20, y: 200 },
                    { x: 21, y: 200 },
                    { x: 22, y: 200 },
                    { x: 23, y: 200 },
                    { x: 24, y: 200 },
                    { x: 25, y: 200 },
                    { x: 26, y: 200 },
                    { x: 27, y: 200 },
                    { x: 28, y: 200 },
                    { x: 29, y: 250 },
                    { x: 30, y: 300 },
                    { x: 31, y: 350 },
                    { x: 32, y: 400 },
                    { x: 33, y: 450 },
                    { x: 34, y: 250 },
                    { x: 35, y: 300 },
                    { x: 36, y: 350 },
                    { x: 37, y: 200 },
                    { x: 38, y: 200 },
                    { x: 39, y: 200 },
                    { x: 40, y: 200 },
                    { x: 41, y: 200 },
                    { x: 42, y: 200 },
                    { x: 43, y: 200 },
                    { x: 44, y: 200 },
                    { x: 45, y: 200 },
                    { x: 46, y: 250 },
                    { x: 47, y: 300 },
                    { x: 48, y: 350 },
                    { x: 49, y: 400 },
                    { x: 50, y: 450 },
                    { x: 51, y: 250 },
                    { x: 52, y: 300 },
                    { x: 53, y: 350 },
                    { x: 55, y: 200 },
                    { x: 56, y: 200 },
                    { x: 57, y: 200 },
                    { x: 58, y: 200 },
                    { x: 59, y: 200 },
                    { x: 60, y: 200 },
                    { x: 61, y: 200 },
                    { x: 62, y: 200 },
                    { x: 63, y: 200 },
                    { x: 64, y: 250 },
                    { x: 65, y: 300 },
                    { x: 66, y: 350 },
                    { x: 67, y: 400 },
                    { x: 68, y: 450 },
                    { x: 69, y: 250 }
                    
                    );

                return arr;

            }

            if (this._currentLevel == 4) {
                var arr: Array<any> = new Array(
                   
                   // { x: 8, y: 200 },
                    { x: 9, y: 200 },
                    { x: 10, y: 200 },
                    { x: 11, y: 200 },
                    { x: 12, y: 200 },
                    { x: 13, y: 200 },
                    { x: 16, y: 200 },
                    { x: 17, y: 200 },
                    { x: 18, y: 200 },
                    { x: 19, y: 200 },
                    { x: 20, y: 200 },
                    { x: 21, y: 200 },
                    { x: 22, y: 200 },
                    { x: 23, y: 200 },
                    { x: 24, y: 250 },
                    { x: 25, y: 300 },
                    { x: 26, y: 350 },
                    { x: 27, y: 400 },
                    { x: 28, y: 450 },
                    { x: 29, y: 250 },
                    { x: 30, y: 300 },
                    { x: 31, y: 350 },
                    { x: 20, y: 200 },
                    { x: 21, y: 200 },
                    { x: 22, y: 200 },
                    { x: 23, y: 200 },
                    { x: 24, y: 200 },
                    { x: 25, y: 200 },
                    { x: 26, y: 200 },
                    { x: 27, y: 200 },
                    { x: 28, y: 200 },
                    { x: 29, y: 250 },
                    { x: 30, y: 300 },
                    { x: 31, y: 350 },
                    { x: 32, y: 400 },
                    { x: 33, y: 450 },
                    { x: 34, y: 250 },
                    { x: 35, y: 300 },
                    { x: 36, y: 350 },
                    { x: 37, y: 200 },
                    { x: 38, y: 200 },
                    { x: 39, y: 200 },
                    { x: 40, y: 200 },
                    { x: 41, y: 200 },
                    { x: 42, y: 200 },
                    { x: 43, y: 200 },
                    { x: 44, y: 200 },
                    { x: 45, y: 200 },
                    { x: 46, y: 250 },
                    { x: 47, y: 300 },
                    { x: 48, y: 350 },
                    { x: 49, y: 400 },
                    { x: 50, y: 450 },
                    { x: 51, y: 250 },
                    { x: 52, y: 300 },
                    { x: 53, y: 350 },
                    { x: 55, y: 200 },
                    { x: 56, y: 200 },
                    { x: 57, y: 200 },
                    { x: 58, y: 200 },
                    { x: 59, y: 200 },
                    { x: 60, y: 200 },
                    { x: 61, y: 200 },
                    { x: 62, y: 200 },
                    { x: 63, y: 200 },
                    { x: 64, y: 250 },
                    { x: 65, y: 300 },
                    { x: 66, y: 350 },
                    { x: 67, y: 400 },
                    { x: 68, y: 450 },
                    { x: 69, y: 250 },
                    { x: 70, y: 300 },
                    { x: 71, y: 350 },
                    { x: 73, y: 200 },
                    { x: 74, y: 200 },
                    { x: 75, y: 200 },
                    { x: 76, y: 200 },
                    { x: 77, y: 200 },
                    { x: 78, y: 200 },
                    { x: 79, y: 200 },
                    { x: 80, y: 200 },
                    { x: 81, y: 200 },
                    { x: 82, y: 250 }
                  
                    );

                return arr;

            }

            if (this._currentLevel == 5) {
                var arr: Array<any> = new Array(
                    { x: 15, y: 200 },
                    { x: 16, y: 200 },
                    { x: 17, y: 200 },
                    { x: 18, y: 200 },
                    { x: 19, y: 200 },
                    { x: 20, y: 200 },
                    { x: 21, y: 200 },
                    { x: 22, y: 200 },
                    { x: 23, y: 200 },
                    { x: 24, y: 250 },
                    { x: 25, y: 300 },
                    { x: 26, y: 350 },
                    { x: 27, y: 400 },
                    { x: 28, y: 450 },
                    { x: 29, y: 250 },
                    { x: 30, y: 300 },
                    { x: 31, y: 350 },
                    { x: 20, y: 200 },
                    { x: 21, y: 200 },
                    { x: 22, y: 200 },
                    { x: 23, y: 200 },
                    { x: 24, y: 200 },
                    { x: 25, y: 200 },
                    { x: 26, y: 200 },
                    { x: 27, y: 200 },
                    { x: 28, y: 200 },
                    { x: 29, y: 250 },
                    { x: 30, y: 300 },
                    { x: 31, y: 350 },
                    { x: 32, y: 400 },
                    { x: 33, y: 450 },
                    { x: 34, y: 250 },
                    { x: 35, y: 300 },
                    { x: 36, y: 350 },
                    { x: 37, y: 200 },
                    { x: 38, y: 200 },
                    { x: 39, y: 200 },
                    { x: 40, y: 200 },
                    { x: 41, y: 200 },
                    { x: 42, y: 200 },
                    { x: 43, y: 200 },
                    { x: 44, y: 200 },
                    { x: 45, y: 200 },
                    { x: 46, y: 250 },
                    { x: 47, y: 300 },
                    { x: 48, y: 350 },
                    { x: 49, y: 400 },
                    { x: 50, y: 450 },
                    { x: 51, y: 250 },
                    { x: 52, y: 300 },
                    { x: 53, y: 350 },
                    { x: 55, y: 200 },
                    { x: 56, y: 200 },
                    { x: 57, y: 200 },
                    { x: 58, y: 200 },
                    { x: 59, y: 200 },
                    { x: 60, y: 200 },
                    { x: 61, y: 200 },
                    { x: 62, y: 200 },
                    { x: 63, y: 200 },
                    { x: 64, y: 250 },
                    { x: 65, y: 300 },
                    { x: 66, y: 350 },
                    { x: 67, y: 400 },
                    { x: 68, y: 450 },
                    { x: 69, y: 250 },
                    { x: 70, y: 300 },
                    { x: 71, y: 350 },
                    { x: 73, y: 200 },
                    { x: 74, y: 200 },
                    { x: 75, y: 200 },
                    { x: 76, y: 200 },
                    { x: 77, y: 200 },
                    { x: 78, y: 200 },
                    { x: 79, y: 200 },
                    { x: 80, y: 200 },
                    { x: 81, y: 200 },
                    { x: 82, y: 250 },
                    { x: 83, y: 300 },
                    { x: 84, y: 350 },
                    { x: 85, y: 400 },
                    { x: 86, y: 450 },
                    { x: 87, y: 250 },
                    { x: 88, y: 300 },
                    { x: 89, y: 350 }
                   
                    
                    );

                return arr;

            }

        }


        public checkCorrectDiamonds(diamond: string,diamontObject:Diamond):boolean {
            console.log("checkCorrectDiamonds", this._drawDiamonds, "kurwy nie dzieci",diamond, this._currentDiamondd);
            if (this._currentDiamondArray == null) this._currentDiamondArray = new Array();
            if (this._drawDiamonds[this._currentDiamondd] == diamond) {

                this._currentDiamondd++;

                if (this._currentDiamondd == this._maxDiamonds) {   

                    diamontObject.destroy(true);
                    this.usunZebranDiamenty(this._currentDiamondArray);
                    this._currentDiamondArray = new Array();
                    this._panelGameControler.panelGame.addGems();
                    GameControler.getInstance().bonuseView.playBonus();
                    this._currentDiamondd = 0;
                    this._panelGameControler.panelGame.showCollectDiamond(diamond, this._currentDiamondd, false);

                }
                else {

                    this._currentDiamondArray.push(diamontObject);
                    this._panelGameControler.panelGame.showCollectDiamond(diamond, this._currentDiamondd, true);

                }

                return true;

            }
            else {

                this._currentDiamondd = 0;
                this._showDiamonds.showDrawDiamon();
                 this._currentDiamondArray = new Array();
                return false;

            }
            return

        }

       public  resetCollecTDiamod() {

           this._currentDiamondd = 0;
       
           this._currentDiamondArray = new Array();
        }


        private usunZebranDiamenty(arr:Array<any>) {

            for (var i: number = 0; i < arr.length; i++) {

                this._currentDiamondArray[i].destroy(true);

            }


        }


        public addMaxSize() {
            this._maxDiamonds++;
            this._currentLevel++;
            userLevel = this.currentLevel; 
          
            if (this._currentLevel == 4) {
                this._maxDiamonds = this._currentLevel + 2;
                this._currentLevel = 4;
            }
            else if (this._currentLevel < 4){
                APIsetLevel(userID, gameID, userLevel + 1);
            }
        }

        public get maxDiamonds():number {
            return this._maxDiamonds;

        }

        public changeGlow() {

            this._panelGameControler.panelGame.setGlow(this._currentLevel);

        }

        public get currentLevel(): number {

            return this._currentLevel;

        }

        public set currentLevel(num: number) {

            this._currentLevel = num;
            this._maxDiamonds = this._currentLevel + 2;
        }


        public reset() {
            this._currentDiamondd = 0;

        }
       

    }


}  