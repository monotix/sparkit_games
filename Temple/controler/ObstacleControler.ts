﻿module Temple {

    export class ObstacleControler {


        public static _instance: ObstacleControler = null;

        public checkCollisionObstale: boolean = true;
        public enemyCollision: boolean = false;
        construct() {

            if (ObstacleControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            ObstacleControler._instance = this;

        }




        public static getInstance(): ObstacleControler {

            if (ObstacleControler._instance === null) {

                ObstacleControler._instance = new ObstacleControler();


            }


            return ObstacleControler._instance;



        }

    }

}