﻿module Temple {

    export class Preloader extends Phaser.State {


        preload() {
          
            var bg = this.add.image(0, 0, "bg", 0);

            var blackProgress = this.add.image(0, 0, "progressBarBlack", 0);
            var blueProgress = this.add.sprite(0, 0, "progressBarBlue", 0);


            blackProgress.position.setTo(this.stage.width / 2 - blackProgress.width / 2, this.stage.height - blackProgress.height - 10);

            blueProgress.position = blackProgress.position;
            this.load.setPreloadSprite(blueProgress);


            this.load.image("mainMenuBg", "assets/mainMenu/SplashScreen.png");
            this.load.atlasJSONArray('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');
            this.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
            this.load.audio("MainMenuSn", "assets/mainMenu/BBThemeMusic.mp3");   //  \\ // 
            this.load.audio("overButtonA", "assets/audio/overButton.mp3");//       ||     ||
            this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");//    //\\  //\\


            this.load.audio("trap", "assets/audio/trap.wav");
            this.load.audio("handgoti", "assets/audio/handgoti.wav");
            this.load.audio("hand", "assets/audio/hand.wav");
            this.load.audio("bat", "assets/audio/bat.wav");
            this.load.audio("showdiamond", "assets/audio/showdiamond.wav");


          
            mobile = false;
            if (!this.game.device.desktop) {
                mobile = true;
            }


            if (mobile) {
                this.load.image("firstInstruction", "assets/03_Temple_of_Trouble.png");
                this.load.audio("instructionSn", "assets/audio/temple_of_trouble_02.mp3");

            }
            else {
                this.load.image("firstInstruction", "assets/02_Temple_of_Trouble.png");
                this.load.audio("instructionSn", "assets/audio/temple_of_trouble_01.mp3");
            }


            this.load.image("popup", "assets/popup/popup.png");
            this.load.image("logoGame", "assets/popup/logo.png");
            this.load.atlasJSONArray('games', 'assets/popup/games.png', 'assets/popup/games.json');
            this.load.atlasJSONArray('playGame', 'assets/popup/play.png', 'assets/popup/play.json');


            this.load.image("bgGame", "assets/temple/TempleGame.png");
            this.load.atlasJSONArray('player', 'assets/temple/player/character.png', 'assets/temple/player/character.json');
            this.load.atlasJSONArray('stars', 'assets/temple/player/stars.png', 'assets/temple/player/stars.json');
            this.load.atlasJSONArray('enemy2', 'assets/temple/enemy/enemy2.png', 'assets/temple/enemy/enemy2.json');
            this.load.atlasJSONArray('enemy1', 'assets/temple/enemy/enemy1.png', 'assets/temple/enemy/enemy1.json');
            //this.load.image("bgGame", "assets/temple/TempleGame.png");


            this.load.image("wallDoor", "assets/temple/items/wallDoor.png");

            this.load.atlasJSONArray('home', 'assets/temple/items/home.png', 'assets/temple/items/home.json');
            this.load.image("titleCircle", "assets/temple/items/titleCircle.png");


            this.load.image("blueDiamond", "assets/temple/items/blueDiamond.png");
            this.load.image("redDiamond", "assets/temple/items/redDiamond.png");
            this.load.image("greenDiamond", "assets/temple/items/greenDiamond.png");

            this.load.atlasJSONArray('hole', 'assets/temple/obstacle/hole.png', 'assets/temple/obstacle/hole.json');
            this.load.atlasJSONArray('collectDiamond', 'assets/temple/collectDiamod.png', 'assets/temple/collectDiamod.json');
            this.load.atlasJSONArray('life', 'assets/temple/life.png', 'assets/temple/life.json');
            this.load.atlasJSONArray('bonus', 'assets/temple/bonus.png', 'assets/temple/bonus.json');
            this.load.image("spikeStill", "assets/temple/obstacle/spikeStill.png");

           // this.load.tilemap('mapa', 'assets/temple/mapa.json', null, Phaser.Tilemap.TILED_JSON);
            this.load.atlasJSONArray('glow', 'assets/temple/glow.png', 'assets/temple/glow.json');


            this.load.image("gameScore", "assets/temple/gameScore.png");
            /* instructions */
         

            /* 


            */

        



            
            this.load.image("firstInstruction", "assets/instructionScreen.png");
         
            /* player */

        



         


            /* obstacle */



          //  this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");

         

         
        /*    for (var i: number = 1; i < 4; i++) {
                this.load.image("item"+i, "assets/robo/item"+i+".PNG");
            }

            for (var i: number = 1; i < 4; i++) {
                this.load.image("robot" + i, "assets/robo/robot" + i + ".PNG");
            }
            */
          

            /*skrzynie*/

         

        


            /*end skrzynie"*/


           
            /*  end roboty */

          
         

           this.load.atlasJSONArray("scoreScreen", "assets/scoreScreen/scoreScreen.png", "assets/scoreScreen/scoreScreen.json");
           this.load.atlasJSONArray("playScoreScreen", "assets/scoreScreen/scoreScreenButtons.png", "assets/scoreScreen/scoreScreenButtons.json");

           this.load.atlasJSONArray("fail1", "assets/feedbacks/fail1.png", "assets/feedbacks/fail1.json");
            
           this.load.atlasJSONArray("success1", "assets/feedbacks/success1.png", "assets/feedbacks/success1.json");


       

        }


        create() {
           
            this.game.state.start("MainMenu", true, false);

        }

    }


} 

var mobile = false;