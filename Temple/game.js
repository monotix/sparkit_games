var Greeter = (function () {
    function Greeter(element) {
        this.element = element;
        this.element.innerHTML += "The time is: ";
        this.span = document.createElement('span');
        this.element.appendChild(this.span);
        this.span.innerText = new Date().toUTCString();
    }
    Greeter.prototype.start = function () {
        var _this = this;
        this.timerToken = setInterval(function () { return _this.span.innerHTML = new Date().toUTCString(); }, 500);
    };
    Greeter.prototype.stop = function () {
        clearTimeout(this.timerToken);
    };
    return Greeter;
})();
window.onload = function () {
    var el = document.getElementById('content');
    var greeter = new Greeter(el);
    greeter.start();
};
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Temple;
(function (Temple) {
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            _super.apply(this, arguments);
        }
        Boot.prototype.preload = function () {
            this.load.image("bg", "assets/SplashScreen.png");
            this.load.image("progressBarBlack", "assets/preloader/ProgressBar.png");
            this.load.image("progressBarBlue", "assets/preloader/ProgressBarBlue.png");
        };
        Boot.prototype.create = function () {
            this.game.state.start("Preloader", true, false);
        };
        return Boot;
    })(Phaser.State);
    Temple.Boot = Boot;
})(Temple || (Temple = {}));
var Temple;
(function (Temple) {
    var DiamondsControler = (function () {
        function DiamondsControler() {
            this._maxDiamonds = 2;
            this._currentDiamondd = 0;
            this._currentLevel = 0;
            //   private _ileZebrac: number = 2;
            this._panelGameControler = Temple.PanelGameControler.getInstance();
        }
        //  private _gameControler: GameControler;
        DiamondsControler.prototype.construct = function () {
            if (DiamondsControler._instance) {
                throw new Error("Error: Instantiation failed: Use DiamondsControler.getInstance() instead of new.");
            }
            DiamondsControler._instance = this;
        };
        Object.defineProperty(DiamondsControler.prototype, "setShowDiamonds", {
            set: function (showDiamonds) {
                this._showDiamonds = showDiamonds;
            },
            enumerable: true,
            configurable: true
        });
        DiamondsControler.getInstance = function () {
            if (DiamondsControler._instance === null) {
                DiamondsControler._instance = new DiamondsControler();
            }
            return DiamondsControler._instance;
        };
        DiamondsControler.prototype.setDrawDiamonds = function (diamondsArr) {
            this._drawDiamonds = diamondsArr;
        };
        DiamondsControler.prototype.level = function () {
            if (this._currentLevel == 0) {
                var arr = new Array({ x: 15, y: 200 }, { x: 16, y: 200 }, { x: 17, y: 200 }, { x: 18, y: 200 }, { x: 19, y: 200 }, { x: 20, y: 200 }, { x: 21, y: 200 }, { x: 22, y: 200 }, { x: 23, y: 200 }, { x: 24, y: 250 }, { x: 25, y: 300 }, { x: 26, y: 350 }, { x: 27, y: 400 }, { x: 28, y: 450 }, { x: 29, y: 250 }, { x: 30, y: 300 }, { x: 31, y: 350 }, { x: 32, y: 400 });
                /*

                 


                        { x: 26, y: 350 },
                    { x: 27, y: 400 },
                    { x: 28, y: 450 },
                    { x: 29, y: 250 },
                    { x: 30, y: 300 },
                    { x: 31, y: 350 },
                    { x: 20, y: 250 },
                    { x: 21, y: 250 },
                    { x: 22, y: 250 },
                    { x: 23, y: 250 },
                    { x: 24, y: 200 },
                    { x: 25, y: 200 },
                    { x: 26, y: 200 },
                    { x: 27, y: 200 },
                    { x: 28, y: 200 },
                    { x: 29, y: 300 },
                    { x: 30, y: 350 },
                    { x: 31, y: 250 },
                    { x: 32, y: 400 },
                    { x: 33, y: 450 },
                    { x: 34, y: 250 },
                    { x: 35, y: 300 },
                    { x: 36, y: 350 },
                    { x: 37, y: 400 }
                */
                return arr;
            }
            if (this._currentLevel == 1) {
                var arr = new Array({ x: 16, y: 200 }, { x: 17, y: 200 }, { x: 18, y: 200 }, { x: 19, y: 200 }, { x: 20, y: 200 }, { x: 21, y: 200 }, { x: 22, y: 200 }, { x: 23, y: 200 }, { x: 24, y: 250 }, { x: 25, y: 300 }, { x: 26, y: 350 }, { x: 27, y: 400 }, { x: 28, y: 450 }, { x: 29, y: 250 }, { x: 30, y: 300 }, { x: 31, y: 350 }, { x: 20, y: 250 }, { x: 21, y: 250 }, { x: 22, y: 250 }, { x: 23, y: 250 }, { x: 24, y: 200 }, { x: 25, y: 200 }, { x: 26, y: 200 }, { x: 27, y: 200 }, { x: 28, y: 200 }, { x: 29, y: 300 }, { x: 30, y: 350 }, { x: 31, y: 250 }, { x: 32, y: 400 }, { x: 33, y: 450 }, { x: 34, y: 250 }, { x: 35, y: 300 }, { x: 36, y: 350 }, { x: 37, y: 400 });
                return arr;
            }
            if (this._currentLevel == 2) {
                var arr = new Array({ x: 15, y: 200 }, { x: 16, y: 200 }, { x: 17, y: 200 }, { x: 18, y: 200 }, { x: 19, y: 200 }, { x: 20, y: 200 }, { x: 21, y: 200 }, { x: 22, y: 200 }, { x: 23, y: 200 }, { x: 24, y: 250 }, { x: 25, y: 300 }, { x: 26, y: 350 }, { x: 27, y: 400 }, { x: 28, y: 450 }, { x: 29, y: 250 }, { x: 30, y: 300 }, { x: 31, y: 350 }, { x: 20, y: 200 }, { x: 21, y: 200 }, { x: 22, y: 200 }, { x: 23, y: 200 }, { x: 24, y: 200 }, { x: 25, y: 200 }, { x: 26, y: 200 }, { x: 27, y: 200 }, { x: 28, y: 200 }, { x: 29, y: 250 }, { x: 30, y: 300 }, { x: 31, y: 350 }, { x: 32, y: 400 }, { x: 33, y: 450 }, { x: 34, y: 250 }, { x: 35, y: 300 }, { x: 36, y: 350 }, { x: 37, y: 200 }, { x: 38, y: 200 }, { x: 39, y: 200 }, { x: 40, y: 200 }, { x: 41, y: 200 }, { x: 42, y: 200 }, { x: 43, y: 200 }, { x: 44, y: 200 }, { x: 45, y: 200 }, { x: 46, y: 250 }, { x: 47, y: 300 }, { x: 48, y: 350 }, { x: 49, y: 400 }, { x: 50, y: 450 }, { x: 51, y: 250 }, { x: 52, y: 300 }, { x: 53, y: 350 }, { x: 54, y: 400 });
                return arr;
            }
            if (this._currentLevel == 3) {
                var arr = new Array({ x: 15, y: 200 }, { x: 16, y: 200 }, { x: 17, y: 200 }, { x: 18, y: 200 }, { x: 19, y: 200 }, { x: 20, y: 200 }, { x: 21, y: 200 }, { x: 22, y: 200 }, { x: 23, y: 200 }, { x: 24, y: 250 }, { x: 25, y: 300 }, { x: 26, y: 350 }, { x: 27, y: 400 }, { x: 28, y: 450 }, { x: 29, y: 250 }, { x: 30, y: 300 }, { x: 31, y: 350 }, { x: 20, y: 200 }, { x: 21, y: 200 }, { x: 22, y: 200 }, { x: 23, y: 200 }, { x: 24, y: 200 }, { x: 25, y: 200 }, { x: 26, y: 200 }, { x: 27, y: 200 }, { x: 28, y: 200 }, { x: 29, y: 250 }, { x: 30, y: 300 }, { x: 31, y: 350 }, { x: 32, y: 400 }, { x: 33, y: 450 }, { x: 34, y: 250 }, { x: 35, y: 300 }, { x: 36, y: 350 }, { x: 37, y: 200 }, { x: 38, y: 200 }, { x: 39, y: 200 }, { x: 40, y: 200 }, { x: 41, y: 200 }, { x: 42, y: 200 }, { x: 43, y: 200 }, { x: 44, y: 200 }, { x: 45, y: 200 }, { x: 46, y: 250 }, { x: 47, y: 300 }, { x: 48, y: 350 }, { x: 49, y: 400 }, { x: 50, y: 450 }, { x: 51, y: 250 }, { x: 52, y: 300 }, { x: 53, y: 350 }, { x: 55, y: 200 }, { x: 56, y: 200 }, { x: 57, y: 200 }, { x: 58, y: 200 }, { x: 59, y: 200 }, { x: 60, y: 200 }, { x: 61, y: 200 }, { x: 62, y: 200 }, { x: 63, y: 200 }, { x: 64, y: 250 }, { x: 65, y: 300 }, { x: 66, y: 350 }, { x: 67, y: 400 }, { x: 68, y: 450 }, { x: 69, y: 250 });
                return arr;
            }
            if (this._currentLevel == 4) {
                var arr = new Array({ x: 9, y: 200 }, { x: 10, y: 200 }, { x: 11, y: 200 }, { x: 12, y: 200 }, { x: 13, y: 200 }, { x: 16, y: 200 }, { x: 17, y: 200 }, { x: 18, y: 200 }, { x: 19, y: 200 }, { x: 20, y: 200 }, { x: 21, y: 200 }, { x: 22, y: 200 }, { x: 23, y: 200 }, { x: 24, y: 250 }, { x: 25, y: 300 }, { x: 26, y: 350 }, { x: 27, y: 400 }, { x: 28, y: 450 }, { x: 29, y: 250 }, { x: 30, y: 300 }, { x: 31, y: 350 }, { x: 20, y: 200 }, { x: 21, y: 200 }, { x: 22, y: 200 }, { x: 23, y: 200 }, { x: 24, y: 200 }, { x: 25, y: 200 }, { x: 26, y: 200 }, { x: 27, y: 200 }, { x: 28, y: 200 }, { x: 29, y: 250 }, { x: 30, y: 300 }, { x: 31, y: 350 }, { x: 32, y: 400 }, { x: 33, y: 450 }, { x: 34, y: 250 }, { x: 35, y: 300 }, { x: 36, y: 350 }, { x: 37, y: 200 }, { x: 38, y: 200 }, { x: 39, y: 200 }, { x: 40, y: 200 }, { x: 41, y: 200 }, { x: 42, y: 200 }, { x: 43, y: 200 }, { x: 44, y: 200 }, { x: 45, y: 200 }, { x: 46, y: 250 }, { x: 47, y: 300 }, { x: 48, y: 350 }, { x: 49, y: 400 }, { x: 50, y: 450 }, { x: 51, y: 250 }, { x: 52, y: 300 }, { x: 53, y: 350 }, { x: 55, y: 200 }, { x: 56, y: 200 }, { x: 57, y: 200 }, { x: 58, y: 200 }, { x: 59, y: 200 }, { x: 60, y: 200 }, { x: 61, y: 200 }, { x: 62, y: 200 }, { x: 63, y: 200 }, { x: 64, y: 250 }, { x: 65, y: 300 }, { x: 66, y: 350 }, { x: 67, y: 400 }, { x: 68, y: 450 }, { x: 69, y: 250 }, { x: 70, y: 300 }, { x: 71, y: 350 }, { x: 73, y: 200 }, { x: 74, y: 200 }, { x: 75, y: 200 }, { x: 76, y: 200 }, { x: 77, y: 200 }, { x: 78, y: 200 }, { x: 79, y: 200 }, { x: 80, y: 200 }, { x: 81, y: 200 }, { x: 82, y: 250 });
                return arr;
            }
            if (this._currentLevel == 5) {
                var arr = new Array({ x: 15, y: 200 }, { x: 16, y: 200 }, { x: 17, y: 200 }, { x: 18, y: 200 }, { x: 19, y: 200 }, { x: 20, y: 200 }, { x: 21, y: 200 }, { x: 22, y: 200 }, { x: 23, y: 200 }, { x: 24, y: 250 }, { x: 25, y: 300 }, { x: 26, y: 350 }, { x: 27, y: 400 }, { x: 28, y: 450 }, { x: 29, y: 250 }, { x: 30, y: 300 }, { x: 31, y: 350 }, { x: 20, y: 200 }, { x: 21, y: 200 }, { x: 22, y: 200 }, { x: 23, y: 200 }, { x: 24, y: 200 }, { x: 25, y: 200 }, { x: 26, y: 200 }, { x: 27, y: 200 }, { x: 28, y: 200 }, { x: 29, y: 250 }, { x: 30, y: 300 }, { x: 31, y: 350 }, { x: 32, y: 400 }, { x: 33, y: 450 }, { x: 34, y: 250 }, { x: 35, y: 300 }, { x: 36, y: 350 }, { x: 37, y: 200 }, { x: 38, y: 200 }, { x: 39, y: 200 }, { x: 40, y: 200 }, { x: 41, y: 200 }, { x: 42, y: 200 }, { x: 43, y: 200 }, { x: 44, y: 200 }, { x: 45, y: 200 }, { x: 46, y: 250 }, { x: 47, y: 300 }, { x: 48, y: 350 }, { x: 49, y: 400 }, { x: 50, y: 450 }, { x: 51, y: 250 }, { x: 52, y: 300 }, { x: 53, y: 350 }, { x: 55, y: 200 }, { x: 56, y: 200 }, { x: 57, y: 200 }, { x: 58, y: 200 }, { x: 59, y: 200 }, { x: 60, y: 200 }, { x: 61, y: 200 }, { x: 62, y: 200 }, { x: 63, y: 200 }, { x: 64, y: 250 }, { x: 65, y: 300 }, { x: 66, y: 350 }, { x: 67, y: 400 }, { x: 68, y: 450 }, { x: 69, y: 250 }, { x: 70, y: 300 }, { x: 71, y: 350 }, { x: 73, y: 200 }, { x: 74, y: 200 }, { x: 75, y: 200 }, { x: 76, y: 200 }, { x: 77, y: 200 }, { x: 78, y: 200 }, { x: 79, y: 200 }, { x: 80, y: 200 }, { x: 81, y: 200 }, { x: 82, y: 250 }, { x: 83, y: 300 }, { x: 84, y: 350 }, { x: 85, y: 400 }, { x: 86, y: 450 }, { x: 87, y: 250 }, { x: 88, y: 300 }, { x: 89, y: 350 });
                return arr;
            }
        };
        DiamondsControler.prototype.checkCorrectDiamonds = function (diamond, diamontObject) {
            console.log("checkCorrectDiamonds", this._drawDiamonds, "kurwy nie dzieci", diamond, this._currentDiamondd);
            if (this._currentDiamondArray == null)
                this._currentDiamondArray = new Array();
            if (this._drawDiamonds[this._currentDiamondd] == diamond) {
                this._currentDiamondd++;
                if (this._currentDiamondd == this._maxDiamonds) {
                    diamontObject.destroy(true);
                    this.usunZebranDiamenty(this._currentDiamondArray);
                    this._currentDiamondArray = new Array();
                    this._panelGameControler.panelGame.addGems();
                    Temple.GameControler.getInstance().bonuseView.playBonus();
                    this._currentDiamondd = 0;
                    this._panelGameControler.panelGame.showCollectDiamond(diamond, this._currentDiamondd, false);
                }
                else {
                    this._currentDiamondArray.push(diamontObject);
                    this._panelGameControler.panelGame.showCollectDiamond(diamond, this._currentDiamondd, true);
                }
                return true;
            }
            else {
                this._currentDiamondd = 0;
                this._showDiamonds.showDrawDiamon();
                this._currentDiamondArray = new Array();
                return false;
            }
            return;
        };
        DiamondsControler.prototype.resetCollecTDiamod = function () {
            this._currentDiamondd = 0;
            this._currentDiamondArray = new Array();
        };
        DiamondsControler.prototype.usunZebranDiamenty = function (arr) {
            for (var i = 0; i < arr.length; i++) {
                this._currentDiamondArray[i].destroy(true);
            }
        };
        DiamondsControler.prototype.addMaxSize = function () {
            this._maxDiamonds++;
            this._currentLevel++;
            userLevel = this.currentLevel;
            if (this._currentLevel == 4) {
                this._maxDiamonds = this._currentLevel + 2;
                this._currentLevel = 4;
            }
            else if (this._currentLevel < 4) {
                APIsetLevel(userID, gameID, userLevel + 1);
            }
        };
        Object.defineProperty(DiamondsControler.prototype, "maxDiamonds", {
            get: function () {
                return this._maxDiamonds;
            },
            enumerable: true,
            configurable: true
        });
        DiamondsControler.prototype.changeGlow = function () {
            this._panelGameControler.panelGame.setGlow(this._currentLevel);
        };
        Object.defineProperty(DiamondsControler.prototype, "currentLevel", {
            get: function () {
                return this._currentLevel;
            },
            set: function (num) {
                this._currentLevel = num;
                this._maxDiamonds = this._currentLevel + 2;
            },
            enumerable: true,
            configurable: true
        });
        DiamondsControler.prototype.reset = function () {
            this._currentDiamondd = 0;
        };
        DiamondsControler._instance = null;
        return DiamondsControler;
    })();
    Temple.DiamondsControler = DiamondsControler;
})(Temple || (Temple = {}));
var Temple;
(function (Temple) {
    var GameControler = (function () {
        function GameControler() {
            this._gamePaused = true;
            this.collisonWall = false;
            this.blockWall = true;
            this.lastPositionPlayer = new Phaser.Point(0, 0);
            this._diamondsControler = Temple.DiamondsControler.getInstance();
            this._digFasterCollectDiamond = 0;
            this._footSpeedUp = 0;
            this.bylemTu = true;
            this.puaseTime = false;
        }
        GameControler.prototype.construct = function () {
            if (GameControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            GameControler._instance = this;
        };
        GameControler.getInstance = function () {
            if (GameControler._instance === null) {
                GameControler._instance = new GameControler();
            }
            return GameControler._instance;
        };
        Object.defineProperty(GameControler.prototype, "player", {
            get: function () {
                return this._level.player;
            },
            enumerable: true,
            configurable: true
        });
        GameControler.prototype.setLevel = function (level) {
            this._level = level;
        };
        Object.defineProperty(GameControler.prototype, "enemyView", {
            get: function () {
                return this._enemyView;
            },
            set: function (enemy) {
                this._enemyView = enemy;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GameControler.prototype, "level", {
            get: function () {
                return this._level;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GameControler.prototype, "gamePaused", {
            get: function () {
                return this._gamePaused;
            },
            set: function (paused) {
                this._gamePaused = paused;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GameControler.prototype, "bonuseView", {
            get: function () {
                return this._bonuseView;
            },
            set: function (bonus) {
                this._bonuseView = bonus;
            },
            enumerable: true,
            configurable: true
        });
        GameControler.prototype.endGame = function (win) {
            this.enemyView.stopEnemy();
            this._gamePaused = true;
            this.player.body.velocity.setTo(0, 0);
            this._diamondsControler.reset();
            this.player.stand();
            if (win) {
                this._diamondsControler.addMaxSize();
                Temple.PanelGameControler.getInstance().panelGame.setGlow();
            }
            this.level.showFinish(win);
            // this.level.showDrawStartDiamond();
        };
        GameControler.prototype.setSound = function (sound_) {
            this.mainSound = sound_;
        };
        GameControler._instance = null;
        return GameControler;
    })();
    Temple.GameControler = GameControler;
})(Temple || (Temple = {}));
var Temple;
(function (Temple) {
    var ObstacleControler = (function () {
        function ObstacleControler() {
            this.checkCollisionObstale = true;
            this.enemyCollision = false;
        }
        ObstacleControler.prototype.construct = function () {
            if (ObstacleControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            ObstacleControler._instance = this;
        };
        ObstacleControler.getInstance = function () {
            if (ObstacleControler._instance === null) {
                ObstacleControler._instance = new ObstacleControler();
            }
            return ObstacleControler._instance;
        };
        ObstacleControler._instance = null;
        return ObstacleControler;
    })();
    Temple.ObstacleControler = ObstacleControler;
})(Temple || (Temple = {}));
var Temple;
(function (Temple) {
    var PanelGameControler = (function () {
        function PanelGameControler() {
        }
        PanelGameControler.prototype.construct = function () {
            if (PanelGameControler._instance) {
                throw new Error("Error: Instantiation failed: Use PanelGameControler.getInstance() instead of new.");
            }
            PanelGameControler._instance = this;
        };
        Object.defineProperty(PanelGameControler.prototype, "setPanelGame", {
            set: function (panelGame_) {
                this._panelGame = panelGame_;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(PanelGameControler.prototype, "panelGame", {
            get: function () {
                return this._panelGame;
            },
            enumerable: true,
            configurable: true
        });
        PanelGameControler.getInstance = function () {
            if (PanelGameControler._instance === null) {
                PanelGameControler._instance = new PanelGameControler();
            }
            return PanelGameControler._instance;
        };
        PanelGameControler._instance = null;
        return PanelGameControler;
    })();
    Temple.PanelGameControler = PanelGameControler;
})(Temple || (Temple = {}));
var Temple;
(function (Temple) {
    var Instruction = (function (_super) {
        __extends(Instruction, _super);
        function Instruction() {
            _super.apply(this, arguments);
        }
        Instruction.prototype.create = function () {
            var instruction = this.add.image(0, 0, "firstInstruction", 0);
            instruction.width = this.game.world.width; //this.stage.width;
            instruction.height = this.game.world.height; //this.stage.height;
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            this._mainMenuBg = this.game.add.audio('instructionSn', 1, true);
            this._mainMenuBg.play("", 0, 1, false);
            var gameControler = Temple.GameControler.getInstance();
            gameControler.mainSound.volume = 0.05;
            // var instructionAudio = this.game.add.audio("chuja", 1, false);
            //  instructionAudio.play();
            var _buttons = this.add.group();
            var gameButton = this.add.button(0, 0, "games", this.onGameClick, this, 1, 0);
            _buttons.addChild(gameButton);
            var playButton = this.add.button(100, 0, "playGame", this.onPlayClick, this, 1, 0);
            playButton.onInputDown.add(this.overPlay2, this);
            _buttons.addChild(playButton);
            _buttons.x = 0;
            _buttons.y = this.game.stage.height - _buttons.height;
        };
        Instruction.prototype.onGameClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        Instruction.prototype.overPlay2 = function () {
            this.overButtonA.play();
        };
        Instruction.prototype.onPlayClick = function () {
            this._mainMenuBg.stop();
            this.game.state.start("Level", true, false);
        };
        return Instruction;
    })(Phaser.State);
    Temple.Instruction = Instruction;
})(Temple || (Temple = {}));
var Temple;
(function (Temple) {
    var Level = (function (_super) {
        __extends(Level, _super);
        function Level() {
            _super.apply(this, arguments);
            this._positionLeftRight = new Phaser.Point(0, 0);
            this._obstacleControler = Temple.ObstacleControler.getInstance();
            this._lastPosition = new Phaser.Point(0, 0);
            this._speedPlayer = 2;
            this._spaceActive = false;
            this._isMove = false;
            this._firstPos = false;
            this._changeFirstPos = false;
        }
        Level.prototype.create = function () {
            //the first parameter is the tileset name as specified in Tiled, the second is the key to the asset
            this.game.world.setBounds(50, 150, 750, 550);
            this._gameBg = this.game.add.sprite(0, 0, "bgGame");
            this._gameBg.x = 50;
            this._gameBg.y = 200;
            this._obrysGr = this.game.add.group();
            this._player = new Temple.Player(this.game, "player");
            this.game.physics.enable(this.player, Phaser.Physics.ARCADE);
            this._player.body.setSize(50, 50, 0, 0);
            this._player.x = 50;
            this._player.y = 100;
            this._positionLeftRight = new Phaser.Point(this._player.x, this._player.y);
            // this._player.anchor.setTo(0.65, 0.2);
            // this._player.body.setSize(50,50,-18,110)
            // this._player.input.enableSnap(32, 32, false);
            this._gameControler = Temple.GameControler.getInstance();
            this._gameControler.setLevel(this);
            Temple.DiamondsControler.getInstance().currentLevel = userLevel - 1;
            this._gridPlayer = new Phaser.Point(0, 0);
            // this._player.gridPosition = new Phaser.Point(0, 0);
            this.game.input.onDown.add(this.movePlayer, this);
            // this.obrysujScene();
            this._wallGroup = new Temple.WallDoorsView(this.game);
            this._wallGroup.y = 0;
            this._wallGroup.x = 0;
            this.stage.addChild(this._wallGroup);
            this._obstacleView = new Temple.ObstacleView(this.game);
            this.stage.addChild(this._obstacleView);
            this._diamondsView = new Temple.DiamondsView(this.game);
            this.stage.addChild(this._diamondsView);
            this.stage.addChild(this._player);
            this._enemyView = new Temple.EnemyView(this.game);
            this.stage.addChild(this._enemyView);
            this._enemyView.x = 0;
            this._enemyView.y = 0;
            this._gameControler.enemyView = this._enemyView;
            this._panelGame = new Temple.PanelGame(this.game);
            this.stage.addChild(this._panelGame);
            this._showDiamond = new Temple.ShowDiamond(this.game);
            this.stage.addChild(this._showDiamond);
            this._bonusView = new Temple.BounsView(this.game);
            this._bonusView.x = 400;
            this._bonusView.y = 100;
            this.stage.addChild(this._bonusView);
            this._gameControler.bonuseView = this._bonusView;
            Temple.DiamondsControler.getInstance().setShowDiamonds = this._showDiamond;
            Temple.PanelGameControler.getInstance().setPanelGame = this._panelGame;
            this.cursors = this.game.input.keyboard.createCursorKeys();
            this.fireButton = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
            this.game.input.keyboard.addKeyCapture([
                Phaser.Keyboard.LEFT,
                Phaser.Keyboard.RIGHT,
                Phaser.Keyboard.UP,
                Phaser.Keyboard.DOWN,
                Phaser.Keyboard.SPACEBAR
            ]);
        };
        Level.prototype.obrysujScene = function () {
            var marx = 50;
            var mary = 100;
            for (var i = 0; i < 9; i++) {
                for (var j = 0; j < 14; j++) {
                    var gra = new Phaser.Graphics(this.game, 0, 0);
                    gra.beginFill(Phaser.Color.getRandomColor(), 1);
                    gra.drawRect(0, 0, 50, 50);
                    gra.endFill();
                    gra.x = marx + 50 * j;
                    gra.y = mary + 50 * i;
                    this._obrysGr.add(gra);
                }
            }
        };
        Level.prototype.showFinish = function (win) {
            if (this._finishScreen == null)
                this._finishScreen = new Temple.FinishScreen(this.game);
            this._finishScreen.visible = true;
            this._finishScreen.show(win);
            this.stage.addChild(this._finishScreen);
        };
        Level.prototype.showDrawDiamond = function () {
            console.log("wywoluje z klasy level showDrawDiamond ile razy");
            // this._showDiamond.showDrawDiamon();
        };
        Level.prototype.showDrawStartDiamond = function () {
            console.log("pokazuj kurwa szmato 1");
            this._showDiamond.drawDiamond();
        };
        Level.prototype.playDeath = function () {
            this._player.death();
        };
        Level.prototype.playAgainAfterDeath = function () {
            console.log("playe after death");
            this._panelGame.restartLife();
            this._player.stand();
            this._player.x = 50;
            this._player.y = 100;
            this.miejscePocztakowe(this._player.x, this._player.y);
            this._positionLeftRight = new Phaser.Point(this._player.x, this._player.y);
            // this._showDiamond.drawDiamond();
        };
        Level.prototype.restartGame = function () {
            this._player.x = 50;
            this._player.y = 100;
            this._enemyView.restart();
            this._diamondsView.restart();
            this.showDrawStartDiamond();
            this._panelGame.resetGems();
            this._panelGame.restartLife();
            this._panelGame.removeCollectDiamond();
            this._panelGame.addBonus(-1);
            this._bonusView.chanegeCurrentPlay();
            this._enemyView.removeEnemisFromArray();
            this._enemyView.addEnemis();
            this._obstacleView.removeEnemisFromArray();
            this._obstacleView.addEnemis();
            this._panelGame.showCollectDiamond("", 0, false);
        };
        Level.prototype.playMoveTo = function (point) {
            if (!this._gameControler.collisonWall) {
                this._gameControler.collisonWall = true;
                var x = Math.floor(point.x);
                var y = Math.floor(point.y);
                x = x - (x % 50);
                y = y - (y % 50);
                var tween = this.game.add.tween(this._player).to({ x: x, y: y }, 500, Phaser.Easing.Quadratic.InOut, true);
                tween.onComplete.add(this.moveComplete, this);
            }
        };
        Level.prototype.moveComplete = function () {
            this._gameControler.collisonWall = false;
        };
        Level.prototype.movePlayerComplete = function () {
            //  this._gameControler.gamePaused = false;
            if (!this._obstacleControler.checkCollisionObstale && !this._gameControler.lastPositionPlayer.equals(new Phaser.Point(this._player.x, this._player.y))) {
            }
            this.miejscePocztakowe(this._player.x, this._player.y);
            if (!this._gameControler.gamePaused)
                this._player.stand();
        };
        Level.prototype.movePlayer = function (pointer) {
            if (!mobile)
                return 0;
            console.log("this._gameControler.gamePaused", !this._gameControler.gamePaused, !this._gameControler.blockWall, (this._tween == null || !this._tween.isRunning));
            if (!this._gameControler.blockWall && !this._gameControler.gamePaused && (this._tween == null || !this._tween.isRunning)) {
                //this._gameControler.gamePaused = true;
                var x = Math.floor(pointer.x);
                var y = Math.floor(pointer.y);
                x = x - (x % 50);
                y = y - (y % 50);
                var duration = (this.game.physics.arcade.distanceToPointer(this._player, pointer) / (200 + this._gameControler._footSpeedUp)) * 1000;
                if (this._lastPosition.equals(new Phaser.Point(x, y))) {
                    this._player.diamond();
                    this._diamondsView.checkCollision();
                }
                else {
                    this._lastPosition = new Phaser.Point(x, y);
                }
                this.whereToMove(new Phaser.Point(x, y));
                var wchodze = false;
                if (this._player.x == x)
                    wchodze = true;
                else if (this._player.y == y)
                    wchodze = true;
                if (wchodze) {
                    console.log("bylem tu:", this._gameControler.bylemTu, x, y);
                    /*     if (this._gameControler.bylemTu && x == 50 && y == 100) {
     
                         }
                         else if (this._gameControler.bylemTu) {
                             this._obstacleControler.checkCollisionObstale = true;
                             this._gameControler.bylemTu = false;
                         }*/
                    this._tween = this.game.add.tween(this._player).to({ x: x, y: y }, duration, Phaser.Easing.Quadratic.InOut, true);
                    this._tween.onComplete.add(this.movePlayerComplete, this);
                    this._positionLeftRight = new Phaser.Point(x, y);
                }
            }
        };
        Level.prototype.stopTweenPlayer = function () {
            this.game.tweens.remove(this._tween);
        };
        Level.prototype.stopMovePlayer = function (point) {
            this.game.tweens.remove(this._tween);
            if (this._tween != null && this._tween.isRunning) {
                this._tween.stop();
                this._tween = null;
            }
            var sc = new Phaser.Pointer(this.game, 1);
            sc.position.setTo(point.x, point.y);
            var x = Math.floor(sc.position.x);
            var y = Math.floor(sc.position.y);
            x = x - (x % 50);
            y = y - (y % 50);
            // var duration = (this.game.physics.arcade.distanceToPointer(this._player, new) / 200) * 1000;
            this._player.position.setTo(x, y);
            // this._tween = this.game.add.tween(this._player).to({ x: point.x, y: point.y }, 0, Phaser.Easing.Quadratic.InOut, true);
            //this._tween.onComplete.add(this.movePlayerComplete, this);
            // this._player.position.setTo(point.x, point.y);
        };
        Level.prototype.whereToMove = function (point) {
            if (this._positionLeftRight.x == point.x) {
                if (this._positionLeftRight.y < point.y) {
                    this._player.down();
                }
                else if (this._positionLeftRight.y > point.y) {
                    this._player.up();
                }
            }
            if (this._positionLeftRight.y == point.y) {
                if (this._positionLeftRight.x < point.x) {
                    this._player.right();
                }
                else if (this._positionLeftRight.x > point.x) {
                    this._player.left();
                }
            }
        };
        Level.prototype.update = function () {
            if (mobile)
                return 0;
            // this.game.physics.arcade.overlap(this._player, this._wallGroup, this.collision, null, this);
            if (!this._gameControler.blockWall && !this._gameControler.gamePaused && (this._tween == null || !this._tween.isRunning)) {
                this._diamondsView.checkCollisionAlpha();
                this._isMove = false;
                if (this.fireButton.isDown && !this._spaceActive) {
                    this._spaceActive = true;
                    this._player.diamond();
                    this._diamondsView.checkCollision();
                }
                else if (!this.fireButton.isDown && this._spaceActive) {
                    this._spaceActive = false;
                }
                else if (this.cursors.right.isDown) {
                    this._player.x += this._speedPlayer + this._gameControler._footSpeedUp;
                    this._player.right();
                    this._isMove = true;
                }
                else if (this.cursors.left.isDown) {
                    this._player.x -= this._speedPlayer + this._gameControler._footSpeedUp;
                    ;
                    this._player.left();
                    this._isMove = true;
                }
                else if (this.cursors.up.isDown) {
                    this._player.y -= this._speedPlayer + this._gameControler._footSpeedUp;
                    this._player.up();
                    this._isMove = true;
                }
                else if (this.cursors.down.isDown) {
                    this._player.y += this._speedPlayer + this._gameControler._footSpeedUp;
                    this._player.down();
                    this._isMove = true;
                }
                //  this.ktoryKwadrat(this._player.x, this._player.y);
                //console.log("jaka cwiartka x",(this._player.x));
                //console.log("jaka cwiartka y",(this._player.y));
                if (this._isMove) {
                    this.ktoryKwadrat(this._player.x, this._player.y);
                    this.miejscePocztakowe(this._player.x, this._player.y);
                }
                else if (!this.fireButton.isDown)
                    this._player.stand();
            }
        };
        Level.prototype.miejscePocztakowe = function (posX, posY) {
            console.log("miejsce poczatkowe", Math.floor(this._player.y / 50) % 9 == 2, Math.floor(this._player.x / 50) % 14 == 1, !this._firstPos);
            if (Math.floor(this._player.y / 50) % 9 == 2 && Math.floor(this._player.x / 50) % 14 == 1 && !this._firstPos) {
                console.log("miejsce poczatkowe wchodzi");
                if (posX < 51 && posY < 101) {
                    this._firstPos = true;
                    this.showDrawStartDiamond();
                    this._obstacleControler.checkCollisionObstale = false;
                }
            }
            else if (this._firstPos && (Math.floor(this._player.y / 50) % 9 != 2 || Math.floor(this._player.x / 50) % 14 != 1)) {
                this._firstPos = false;
            }
        };
        Level.prototype.ktoryKwadrat = function (posX, posY) {
            var ok = false;
            var ok2 = false;
            if (Math.floor(this._player.y / 50) % 9 != 2 || Math.floor(this._player.x / 50) % 14 != 1 && this._firstPos) {
                if (posX > 99 && posY > 149) {
                    this._firstPos = false;
                    this._obstacleControler.checkCollisionObstale = true;
                }
            }
            if (Math.floor(this._player.y / 50) % 9 == 1 && Math.floor(this._player.x / 50) % 14 == 7) {
                return 0;
            }
            if (Math.floor(this._player.y / 50) % 9 == 2 && Math.floor(this._player.x / 50) % 14 == 7) {
                return 0;
            }
            if (Math.floor(this._player.y / 50) % 9 == 5 && Math.floor(this._player.x / 50) % 14 == 0) {
                return 0;
            }
            if (Math.floor(this._player.y / 50) % 9 == 5 && Math.floor(this._player.x / 50) % 14 == 13) {
                return 0;
            }
            if (Math.floor(posX / 50) % 14 > 0 && Math.floor(posX / 50) % 14 < 14 && posX > 0 && posX < 800) {
                ok = true;
            }
            else if (posX < 200) {
                this._player.x += this._speedPlayer;
            }
            else if (posX > 600) {
                this._player.x -= this._speedPlayer;
            }
            if (Math.floor(posY / 50) % 9 > 1 && Math.floor(posY / 50) % 9 < 9 && posY > 0 && posY < 600) {
                ok = true;
            }
            else if (posY < 200) {
                this._player.y += this._speedPlayer + this._gameControler._footSpeedUp;
            }
            else if (posY > 500) {
                this._player.y -= this._speedPlayer - this._gameControler._footSpeedUp;
                ;
            }
            /*
            if (Math.floor(posY / 50) % 14 == Math.floor(dY / 50) % 14) {
                ok2 = true;
            }
 
            if (ok && ok2) {
                return true;
            }
 
 
            return false;*/
        };
        Level.prototype.collision = function (player, wall) {
            //  console.log("kolizja ze sciana", wall);
        };
        Level.prototype.render = function () {
            /*
               this.game.debug.quadTree(this.game.physics.arcade.quadTree);
         
               this.game.debug.body(this._player);
               this._wallGroup.forEach(function (item) {
                   // item.body.velocity = 0;
                
                   this.game.debug.body(item);
               }, this);
              
               this._obstacleView.forEach(function (item) {
                   // item.body.velocity = 0;
                
                   this.game.debug.body(item);
               }, this);
             */
        };
        Object.defineProperty(Level.prototype, "player", {
            get: function () {
                return this._player;
            },
            enumerable: true,
            configurable: true
        });
        return Level;
    })(Phaser.State);
    Temple.Level = Level;
})(Temple || (Temple = {}));
var Temple;
(function (Temple) {
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            console.log("start");
            _super.call(this, 800, 600, Phaser.AUTO, "content", null, true);
            this.state.add("Boot", Temple.Boot, false);
            this.state.add("Preloader", Temple.Preloader, false);
            this.state.add("MainMenu", Temple.MainMenu, false);
            this.state.add("PopupStart", Temple.PopupStart, false);
            this.state.add("Instruction", Temple.Instruction, false);
            this.state.add("Level", Temple.Level, false);
            this.state.start("Boot");
        }
        return Game;
    })(Phaser.Game);
    Temple.Game = Game;
})(Temple || (Temple = {}));
window.onload = function () {
    var game = new Temple.Game;
};
var userLevel = 1;
var Temple;
(function (Temple) {
    var MainMenu = (function (_super) {
        __extends(MainMenu, _super);
        function MainMenu() {
            _super.apply(this, arguments);
        }
        MainMenu.prototype.create = function () {
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var _mainMenuBg = this.game.add.audio('MainMenuSn', 1, true);
            _mainMenuBg.volume = 0.1;
            _mainMenuBg.play();
            var gameControler = Temple.GameControler.getInstance();
            gameControler.setSound(_mainMenuBg);
            var background = this.add.sprite(0, 0, 'mainMenuBg');
            var _buttons = this.add.group();
            var gameButton = this.add.button(0, 0, "games", this.onGameClick, this, 1, 0);
            //gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            _buttons.addChild(gameButton);
            var playButton = this.add.button(100, 0, "playGame", this.onPlayClick, this, 1, 0);
            playButton.onInputDown.add(this.overPlay2, this);
            _buttons.addChild(playButton);
            _buttons.x = 0;
            _buttons.y = this.game.stage.height - _buttons.height;
        };
        MainMenu.prototype.onGameClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        MainMenu.prototype.overPlay2 = function () {
            this.overButtonA.play();
        };
        MainMenu.prototype.onPlayClick = function () {
            this.game.state.start("PopupStart", true, false);
        };
        return MainMenu;
    })(Phaser.State);
    Temple.MainMenu = MainMenu;
})(Temple || (Temple = {}));
var Temple;
(function (Temple) {
    var Diamond = (function (_super) {
        __extends(Diamond, _super);
        function Diamond(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this._gameControler = Temple.GameControler.getInstance();
            this._diamondsControler = Temple.DiamondsControler.getInstance();
            this._panelGameControler = Temple.PanelGameControler.getInstance();
            this._maxHealth = 6;
            this._blokada = false;
            this.ktory = 0;
            this.name = key;
            // this.changeColor();
        }
        Diamond.prototype.diamondDeastroy = function () {
            this._maxHealth--;
            if (this._maxHealth > 0)
                this._maxHealth -= this._gameControler._digFasterCollectDiamond;
            console.log(this._maxHealth);
            if (this._maxHealth == 0 && !this._blokada) {
                this._blokada = true;
                console.log("zielony ??:", this.name, "Blokada kurwaaAA", this.ktory);
                if (this._diamondsControler.checkCorrectDiamonds(this.name, this)) {
                    this.tint = 0x000000;
                    if (this.body != null)
                        this.body.setSize(0, 0, -1000, 700);
                }
                else {
                    this.tint = 0x000000;
                    if (this.body != null)
                        this.body.setSize(0, 0, -1000, 700);
                    this._panelGameControler.panelGame.showCollectDiamond("", 0, false);
                    this._gameControler.level.showDrawDiamond();
                }
            }
        };
        Diamond.prototype.changeColor = function () {
            var bmd = this.game.make.bitmapData(this.width, this.height);
            bmd.draw(this, 0, 0, this.width, this.height);
            bmd.replaceRGB(0, 85, 255, 255, 0, 250, 40, 255, new Phaser.Rectangle(0, 0, this.width, this.height));
            this.game.add.sprite(0, 0, bmd);
        };
        return Diamond;
    })(Phaser.Sprite);
    Temple.Diamond = Diamond;
})(Temple || (Temple = {}));
var Temple;
(function (Temple) {
    var Enemy = (function (_super) {
        __extends(Enemy, _super);
        function Enemy(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this._gameControler = Temple.GameControler.getInstance();
            this._nextMove = 0;
            this._moveRate = 3000;
            this.Speed = 60;
            this.stopEnemy = true;
            this.outOfBounds = false;
            this.startPosition = new Phaser.Point(x, y);
            this.frame = 0;
            this.checkWorldBounds = true;
            this.anchor.setTo(0.5, 0.5);
            if (key == "enemy2") {
                this._audio = this.game.add.audio("bat", 1, false);
            }
            else if (key == "enemy1") {
                this._audio = this.game.add.audio("handgoti", 1, false);
            }
            // this.outOfBoundsKill = true;
            //  this.animations.add("play",[0,1]);
            //this.animations.play("play", 24, true);
            // this.moveTo();
        }
        Enemy.prototype.playAudio = function () {
            if (this._audio != null) {
                this._audio.play();
            }
        };
        Enemy.prototype.moveStop = function () {
        };
        Enemy.prototype.moveOn = function () {
            this.Speed = 0;
        };
        Object.defineProperty(Enemy.prototype, "moveRate", {
            set: function (moveSpeed) {
                this._moveRate = moveSpeed;
            },
            enumerable: true,
            configurable: true
        });
        Enemy.prototype.moveTo = function () {
            if (!this.stopEnemy) {
                // this.body.bounds.setTo(10, 10, 300, 300);
                //this.body.setBounds(100, 100, 550, 550);
                if (this.game.physics.arcade.distanceBetween(this, this._gameControler.player) < 800) {
                    if (this.game.time.now > this._nextMove) {
                        this._nextMove = this.game.time.now + this._moveRate;
                        // bullet.reset(this.turret.x, this.turret.y);
                        var x = this._gameControler.player.world.x;
                        var y = this._gameControler.player.world.y;
                        if (x > 650)
                            x = 650;
                        else if (x < 99)
                            x = 100;
                        if (y < 100)
                            y = 100;
                        else if (y > 640)
                            y = 400;
                        this.game.physics.arcade.moveToObject(this, this._gameControler.player, 60);
                    }
                }
                this.game.time.events.add(Phaser.Timer.SECOND * 3, function () {
                    this.outOfBounds = false;
                    this.moveTo();
                }, this);
            }
            // var point: Phaser.Point = new Phaser.Point(this._gameControler.player.x, this._gameControler.player.y);
            /*  var x = Math.floor(point.x);
              var y = Math.floor(point.y);


              x = x - (x % 50);
              y = y - (y % 50);



            //  var tween = this.game.add.tween(this).to({ x: x, y: y }, 250, Phaser.Easing.Quadratic.InOut, true);
            //  tween.onComplete.add(this.moveComplete, this);
          */
            /*
                var target = this._gameControler.player;
                var follower = this;
                if (follower.x == NaN) return;ss
                var distanceX: number = Math.floor( target.x - follower.x);
                var distanceY: number = Math.floor(target.y - follower.y);
                console.log("dystans", distanceX, distanceY);
                //get total distance as one number
               var distanceTotal:number = Math.floor( Math.sqrt(distanceX * distanceX + distanceY * distanceY));
               console.log("total", distanceTotal);
                //check if target is within agro range
               var turnRate:number = 0.3;
               var moveX: number = 0;
               var moveY: number = 0;
               var speed:number = 3;
               
                if (distanceTotal <= 200 && distanceTotal > 0 && distanceX > 0 && distanceY > 0 ) {
                    //calculate how much to move
                    var moveDistanceX: number = Math.floor(  distanceX / distanceTotal);
                    var moveDistanceY: number = Math.floor(  distanceY / distanceTotal);
                    //console.log("total GOWNO", distanceX , distanceTotal, 0/0);
                    //increase current speed
                    moveX += moveDistanceX;
                    moveY += moveDistanceY;
        
                    //get total move distance
                    var totalmove = Math.floor( Math.sqrt(moveX * moveX + moveY * moveY));
        
                    //apply easing
                    moveX = Math.floor( speed * moveX / totalmove);
                    moveY = Math.floor( speed * moveY / totalmove);
        
                    //move follower
                    console.log("movex", moveX, moveY);
                    follower.x += moveX;
                    follower.y += moveY;
        
                    //rotate follower toward target
                    follower.rotation = 180 * Math.atan2(moveY, moveX) / Math.PI;
                   
                }
            
            */
        };
        Enemy.prototype.moveComplete = function () {
            this.game.time.events.add(Phaser.Timer.SECOND * 2, function () {
                this.moveTo();
            }, this);
        };
        Enemy.prototype.makeStopEnemy = function () {
            this.stopEnemy = true;
            //  this.animations.stop("play");
            this.body.velocity.setTo(0, 0);
        };
        Enemy.prototype.makeStartEnemy = function () {
            this.stopEnemy = false;
            //    this.animations.play("play",24,true);
            this.moveTo();
        };
        Enemy.prototype.boundsCollision = function () {
            this.stopEnemy = true;
            this.body.velocity.setTo(0, 0);
            this.game.time.events.add(Phaser.Timer.SECOND * 2, function () {
                this.outOfBounds = false;
                this.makeStartEnemy();
            }, this);
        };
        Enemy.prototype.restart = function () {
            this.body.velocity.setTo(0, 0);
            this.position = this.startPosition;
            this.stopEnemy = true;
        };
        Enemy.prototype.update = function () {
            // console.log(this.game.world.bounds, new Phaser.Rectangle(this.x, this.y, 50, 50));
            if (!this.game.world.bounds.intersects(new Phaser.Rectangle(this.world.x, this.world.y, 200, 200), 1) && !this.outOfBounds) {
                console.log("blokun kurwe", this.game.world.bounds, new Phaser.Rectangle(this.world.x, this.world.y, 50, 50));
                this.body.velocity.setTo(0, 0);
                this.position.setTo(this._correctPosition.x, this._correctPosition.y);
                this.outOfBounds = true;
            }
            else {
                this._correctPosition = new Phaser.Point(this.x, this.y);
            }
        };
        return Enemy;
    })(Phaser.Sprite);
    Temple.Enemy = Enemy;
})(Temple || (Temple = {}));
var Temple;
(function (Temple) {
    var Player = (function (_super) {
        __extends(Player, _super);
        function Player(game, key) {
            _super.call(this, game, 0, 0, "", 0);
            this._gameControler = Temple.GameControler.getInstance();
            this._obstacleControler = Temple.ObstacleControler.getInstance();
            this._player = new Phaser.Sprite(this.game, -15, -135, key, 0);
            this._player.animations.add("stars", this.countFrames(11, 46), 24, false);
            this._player.animations.add("stand", this.countFrames(1, 9), 24, false);
            this._player.animations.add("left", this.countFrames(58, 73), 24, false);
            this._player.animations.add("right", this.countFrames(42, 57), 24, false);
            this._player.animations.add("up", this.countFrames(26, 41), 24, false);
            this._player.animations.add("down", this.countFrames(10, 25), 24, false);
            this._player.animations.add("diamond", this.countFrames(74, 84), 24, false);
            this._player.animations.add("tap", this.countFrames(85, 96), 24, false);
            this._player.animations.add("death", this.countFrames(109, 129), 24, false);
            this.addChild(this._player);
            this._stars = new Phaser.Sprite(this.game, 0, -60, "stars", 0);
            this._stars.animations.add("play");
            this._stars.visible = false;
            this.addChild(this._stars);
        }
        Player.prototype.death = function () {
            console.log("kusi ta smierc");
            this._obstacleControler.enemyCollision = true;
            this.body.velocity.setTo(0, 0);
            this._player.animations.getAnimation("death").onComplete.add(this.kuniec, this);
            this._player.animations.play("death", 24, false);
        };
        Player.prototype.kuniec = function () {
            console.log("koniec kusi ta smierc");
            this._gameControler.level.playAgainAfterDeath();
            this._obstacleControler.enemyCollision = false;
        };
        Player.prototype.stand = function () {
            this._player.animations.getAnimation("stand").onComplete.add(function () {
            }, this);
            this._player.animations.play("stand", 24, true);
        };
        Player.prototype.left = function () {
            this._player.animations.getAnimation("left").onComplete.add(function () {
            }, this);
            this._player.animations.play("left", 24, true);
        };
        Player.prototype.right = function () {
            this._player.animations.getAnimation("right").onComplete.add(function () {
            }, this);
            this._player.animations.play("right", 24, true);
        };
        Player.prototype.up = function () {
            this._player.animations.getAnimation("up").onComplete.add(function () {
            }, this);
            this._player.animations.play("up", 24, true);
        };
        Player.prototype.diamond = function () {
            this._player.animations.getAnimation("diamond").onComplete.add(function () {
            }, this);
            this._player.animations.play("diamond", 24, false);
        };
        Player.prototype.tap = function () {
            this._player.animations.getAnimation("tap").onComplete.add(function () {
                this.endstars();
            }, this);
            this._player.animations.play("tap", 24, true);
        };
        Player.prototype.down = function () {
            this._player.animations.getAnimation("down").onComplete.add(function () {
            }, this);
            this._player.animations.play("down", 24, true);
        };
        Player.prototype.stars = function (num) {
            if (num === void 0) { num = 3; }
            this._obstacleControler.checkCollisionObstale = false;
            this._obstacleControler.enemyCollision = true;
            this._stars.visible = true;
            this._stars.animations.play("play", 24, true);
            this.game.time.events.add(Phaser.Timer.SECOND * num, function () {
                this._stars.visible = false;
                this.endstars();
                this._obstacleControler.enemyCollision = false;
            }, this);
        };
        Player.prototype.endstars = function () {
            this._obstacleControler.checkCollisionObstale = true;
            this._gameControler.gamePaused = false;
            // this.stand();
        };
        Player.prototype.countFrames = function (start, end) {
            var countArr = new Array();
            for (var i = start; i < end; i++) {
                countArr.push(i);
            }
            return countArr;
        };
        return Player;
    })(Phaser.Sprite);
    Temple.Player = Player;
})(Temple || (Temple = {}));
/*

1-10 stoi
11-46 gwiazdki
47-62 idzie w dol
63-78 idzie w gore
79-84 idzie w prawo
95-110 idzie w lewo
111-121 wierci diament
122-145 u gory napis Tap
146 -223 umiera

*/
var Temple;
(function (Temple) {
    var Wall = (function (_super) {
        __extends(Wall, _super);
        function Wall(game, x, y) {
            _super.call(this, game, x, y, "");
            var grp = new Phaser.Graphics(game, x, y);
            grp.beginFill(0x000000, 1);
            grp.drawRect(0, 0, 50, 50);
            grp.endFill();
            this._wall = new Phaser.Sprite(game, x, y, "");
            this._wall.anchor.setTo(0.5, 0.5);
            this._wall.addChild(grp);
            this.name = "wall";
            this._wall.addChild(grp);
            this.addChild(this._wall);
        }
        return Wall;
    })(Phaser.Sprite);
    Temple.Wall = Wall;
})(Temple || (Temple = {}));
var Temple;
(function (Temple) {
    var Preloader = (function (_super) {
        __extends(Preloader, _super);
        function Preloader() {
            _super.apply(this, arguments);
        }
        Preloader.prototype.preload = function () {
            var bg = this.add.image(0, 0, "bg", 0);
            var blackProgress = this.add.image(0, 0, "progressBarBlack", 0);
            var blueProgress = this.add.sprite(0, 0, "progressBarBlue", 0);
            blackProgress.position.setTo(this.stage.width / 2 - blackProgress.width / 2, this.stage.height - blackProgress.height - 10);
            blueProgress.position = blackProgress.position;
            this.load.setPreloadSprite(blueProgress);
            this.load.image("mainMenuBg", "assets/mainMenu/SplashScreen.png");
            this.load.atlasJSONArray('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');
            this.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
            this.load.audio("MainMenuSn", "assets/mainMenu/BBThemeMusic.mp3"); //  \\ // 
            this.load.audio("overButtonA", "assets/audio/overButton.mp3"); //       ||     ||
            this.load.audio("clearButtonA", "assets/audio/clearButton.mp3"); //    //\\  //\\
            this.load.audio("trap", "assets/audio/trap.wav");
            this.load.audio("handgoti", "assets/audio/handgoti.wav");
            this.load.audio("hand", "assets/audio/hand.wav");
            this.load.audio("bat", "assets/audio/bat.wav");
            this.load.audio("showdiamond", "assets/audio/showdiamond.wav");
            mobile = false;
            if (!this.game.device.desktop) {
                mobile = true;
            }
            if (mobile) {
                this.load.image("firstInstruction", "assets/03_Temple_of_Trouble.png");
                this.load.audio("instructionSn", "assets/audio/temple_of_trouble_02.mp3");
            }
            else {
                this.load.image("firstInstruction", "assets/02_Temple_of_Trouble.png");
                this.load.audio("instructionSn", "assets/audio/temple_of_trouble_01.mp3");
            }
            this.load.image("popup", "assets/popup/popup.png");
            this.load.image("logoGame", "assets/popup/logo.png");
            this.load.atlasJSONArray('games', 'assets/popup/games.png', 'assets/popup/games.json');
            this.load.atlasJSONArray('playGame', 'assets/popup/play.png', 'assets/popup/play.json');
            this.load.image("bgGame", "assets/temple/TempleGame.png");
            this.load.atlasJSONArray('player', 'assets/temple/player/character.png', 'assets/temple/player/character.json');
            this.load.atlasJSONArray('stars', 'assets/temple/player/stars.png', 'assets/temple/player/stars.json');
            this.load.atlasJSONArray('enemy2', 'assets/temple/enemy/enemy2.png', 'assets/temple/enemy/enemy2.json');
            this.load.atlasJSONArray('enemy1', 'assets/temple/enemy/enemy1.png', 'assets/temple/enemy/enemy1.json');
            //this.load.image("bgGame", "assets/temple/TempleGame.png");
            this.load.image("wallDoor", "assets/temple/items/wallDoor.png");
            this.load.atlasJSONArray('home', 'assets/temple/items/home.png', 'assets/temple/items/home.json');
            this.load.image("titleCircle", "assets/temple/items/titleCircle.png");
            this.load.image("blueDiamond", "assets/temple/items/blueDiamond.png");
            this.load.image("redDiamond", "assets/temple/items/redDiamond.png");
            this.load.image("greenDiamond", "assets/temple/items/greenDiamond.png");
            this.load.atlasJSONArray('hole', 'assets/temple/obstacle/hole.png', 'assets/temple/obstacle/hole.json');
            this.load.atlasJSONArray('collectDiamond', 'assets/temple/collectDiamod.png', 'assets/temple/collectDiamod.json');
            this.load.atlasJSONArray('life', 'assets/temple/life.png', 'assets/temple/life.json');
            this.load.atlasJSONArray('bonus', 'assets/temple/bonus.png', 'assets/temple/bonus.json');
            this.load.image("spikeStill", "assets/temple/obstacle/spikeStill.png");
            // this.load.tilemap('mapa', 'assets/temple/mapa.json', null, Phaser.Tilemap.TILED_JSON);
            this.load.atlasJSONArray('glow', 'assets/temple/glow.png', 'assets/temple/glow.json');
            this.load.image("gameScore", "assets/temple/gameScore.png");
            /* instructions */
            /*


            */
            this.load.image("firstInstruction", "assets/instructionScreen.png");
            /* player */
            /* obstacle */
            //  this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");
            /*    for (var i: number = 1; i < 4; i++) {
                    this.load.image("item"+i, "assets/robo/item"+i+".PNG");
                }
    
                for (var i: number = 1; i < 4; i++) {
                    this.load.image("robot" + i, "assets/robo/robot" + i + ".PNG");
                }
                */
            /*skrzynie*/
            /*end skrzynie"*/
            /*  end roboty */
            this.load.atlasJSONArray("scoreScreen", "assets/scoreScreen/scoreScreen.png", "assets/scoreScreen/scoreScreen.json");
            this.load.atlasJSONArray("playScoreScreen", "assets/scoreScreen/scoreScreenButtons.png", "assets/scoreScreen/scoreScreenButtons.json");
            this.load.atlasJSONArray("fail1", "assets/feedbacks/fail1.png", "assets/feedbacks/fail1.json");
            this.load.atlasJSONArray("success1", "assets/feedbacks/success1.png", "assets/feedbacks/success1.json");
        };
        Preloader.prototype.create = function () {
            this.game.state.start("MainMenu", true, false);
        };
        return Preloader;
    })(Phaser.State);
    Temple.Preloader = Preloader;
})(Temple || (Temple = {}));
var mobile = false;
var Temple;
(function (Temple) {
    var BounsView = (function (_super) {
        __extends(BounsView, _super);
        function BounsView(game) {
            _super.call(this, game);
            this._gameControler = Temple.GameControler.getInstance();
            this._diamondsControler = Temple.DiamondsControler.getInstance();
            this._panelGameControler = Temple.PanelGameControler.getInstance();
            this._bonusArr = new Array("invin", "foot", "dig", "bonus1", "bonus2", "bonus3", "bomb", "target", "freez");
            this._currentPlay = 0;
            this._bounusAnim = this.game.add.sprite(0, 0, "bonus", 0, this);
            this._bounusAnim.animations.add("bomb", this.countFrames(9, 35), 24, false);
            this._bounusAnim.animations.add("target", this.countFrames(36, 64), 24, false);
            this._bounusAnim.animations.add("freez", this.countFrames(65, 92), 24, false);
            this._bounusAnim.animations.add("invin", this.countFrames(93, 120), 24, false);
            this._bounusAnim.animations.add("foot", this.countFrames(121, 148), 24, false);
            this._bounusAnim.animations.add("dig", this.countFrames(149, 176), 24, false);
            this._bounusAnim.animations.add("bonus1", this.countFrames(177, 204), 24, false);
            this._bounusAnim.animations.add("bonus2", this.countFrames(205, 232), 24, false);
            this._bounusAnim.animations.add("bonus3", this.countFrames(234, 260), 24, false);
        }
        BounsView.prototype.playBonus = function () {
            console.log("play bonus", this._bonusArr[this._currentPlay]);
            if (this._bonusArr.length == this._currentPlay)
                this._currentPlay = 0;
            var str = this._bonusArr[this._currentPlay];
            this.setBonus();
            this.visible = true;
            this._bounusAnim.animations.getAnimation(str).onComplete.add(this.animStop, this);
            this._bounusAnim.play(str, 24, false);
            this._currentPlay++;
        };
        BounsView.prototype.setBonus = function () {
            var str = this._bonusArr[this._currentPlay];
            if (str == "dig") {
                this._gameControler._digFasterCollectDiamond = 1;
                this.game.time.events.add(Phaser.Timer.SECOND * 5, function () {
                    this._gameControler._digFasterCollectDiamond = 0;
                }, this);
            }
            else if (str == "bonus1") {
                this._panelGameControler.panelGame.addBonus(1);
            }
            else if (str == "bonus2") {
                this._panelGameControler.panelGame.addBonus(1);
            }
            else if (str == "bonus3") {
                this._panelGameControler.panelGame.addBonus(1);
            }
            else if (str == "foot") {
                if (mobile)
                    this._gameControler._footSpeedUp = 50;
                else
                    this._gameControler._footSpeedUp = 2;
                this.game.time.events.add(Phaser.Timer.SECOND * 5, function () {
                    this._gameControler._footSpeedUp = 0;
                }, this);
            }
            else if (str == "invin") {
                this._gameControler.player.stars(5);
            }
        };
        BounsView.prototype.animStop = function () {
            this.visible = false;
        };
        BounsView.prototype.countFrames = function (start, end) {
            var countArr = new Array();
            for (var i = start; i < end; i++) {
                countArr.push(i);
            }
            return countArr;
        };
        BounsView.prototype.chanegeCurrentPlay = function (num) {
            if (num === void 0) { num = 0; }
            this._currentPlay = num;
        };
        return BounsView;
    })(Phaser.Group);
    Temple.BounsView = BounsView;
})(Temple || (Temple = {}));
var Temple;
(function (Temple) {
    var DiamondsView = (function (_super) {
        __extends(DiamondsView, _super);
        function DiamondsView(game) {
            _super.call(this, game);
            this._gameControler = Temple.GameControler.getInstance();
            this._obstacleControler = Temple.ObstacleControler.getInstance();
            this._diamondsControler = Temple.DiamondsControler.getInstance();
            this._diamondArr = new Array("blueDiamond", "redDiamond", "greenDiamond");
            this.unique = function (origArr) {
                var newArray = [], origLen = origArr.length, found;
                var x = 0;
                var y = 0;
                for (x = 0; x < origLen; x++) {
                    found = undefined;
                    for (y = 0; y < newArray.length; y++) {
                        if (origArr[x].x == newArray[y].x && origArr[x].y == newArray[y].y) {
                            console.log(origArr[x].name, newArray[y].name);
                            found = true;
                        }
                    }
                    if (!found)
                        newArray.push(origArr[x]);
                }
                return newArray;
            };
            this.enableBody = true;
            //this._diamond = new Diamond(this.game, 0, 0, "greenDiamond");
            // this._diamond.anchor.setTo(0.5, 0.5);
            //  this.whereToPut(new Phaser.Point(300, 200),this._diamond);
            this.addDiamonds();
        }
        DiamondsView.prototype.addDiamonds = function () {
            var arr = this._diamondsControler.level();
            this._actualDiamond = new Array();
            for (var i = 0; i < arr.length; i++) {
                var diamond = new Temple.Diamond(this.game, 0, 0, this._diamondArr[this.getRandom(0, 3)]);
                diamond.ktory = i;
                this.whereToPut((arr[i]), diamond);
                this._actualDiamond.push(diamond);
            }
            //console.log("ile diamentow", this._actualDiamond.length);
            this._actualDiamond = this.unique(this._actualDiamond);
            //console.log("ile diamentow po", this._actualDiamond.length);
            this.addDiamondToStage();
            //  this._actualDiamond = arr;
        };
        DiamondsView.prototype.addDiamondToStage = function () {
            for (var i = 0; i < this._actualDiamond.length; i++) {
                this.add(this._actualDiamond[i]);
            }
        };
        DiamondsView.prototype.whereToPut = function (pos, diamond) {
            //   var x = Math.floor(pointer.x);
            //   var y = Math.floor(pointer.y);
            var posx = (pos.x % 14);
            var posy = Math.floor(pos.y % 9);
            if (posy > 9)
                posy = 9;
            if (posy < 1)
                posy = 1;
            if (posx < 0)
                posx = 1;
            if (posx == 0 && posy == 1)
                posx = 2;
            console.log("diamond", posx, posy);
            var ustawx = posx * 50 + 50;
            var ustawy = posy * 50 + 50;
            //   console.log("reszta z dzielenia",posx,posy);
            console.log("pozycja", ustawx, ustawy);
            //14 cwiartek jedno po 50
            //wiec 25 to bedzie 1
            //
            //  x = x - (x % 50);
            //  y = y - (y % 50);
            diamond.x = ustawx + (25 - diamond.width / 2);
            diamond.y = ustawy + (25 - diamond.height / 2);
            // this.add(diamond);
        };
        DiamondsView.prototype.checkCollision = function () {
            this.game.physics.arcade.overlap(this._gameControler.player, this, this.collisionDiamond, null, this);
        };
        DiamondsView.prototype.collisionDiamond = function (player, diamond) {
            if (this.ktoryKwadrat(player.x + 25, player.y + 25, diamond.x, diamond.y)) {
                console.log("jest kolizja z diamentem??", diamond.name);
                diamond.diamondDeastroy();
            }
        };
        DiamondsView.prototype.checkCollisionAlpha = function () {
            this.game.physics.arcade.overlap(this._gameControler.player, this, this.collisionDiamondAlpha, null, this);
        };
        DiamondsView.prototype.collisionDiamondAlpha = function (player, diamond) {
            for (var i = 0; i < this._actualDiamond.length; i++) {
                this._actualDiamond[i].alpha = 1;
            }
            if (this.ktoryKwadrat(player.x + 25, player.y + 25, diamond.x, diamond.y)) {
                console.log("jest kolizja z diamentem??", diamond.name);
                diamond.alpha = 0.5;
            }
        };
        DiamondsView.prototype.ktoryKwadrat = function (posX, posY, dX, dY) {
            var ok = false;
            var ok2 = false;
            if (Math.floor(posX / 50) % 14 == Math.floor(dX / 50) % 14) {
                ok = true;
            }
            if (Math.floor(posY / 50) % 14 == Math.floor(dY / 50) % 14) {
                ok2 = true;
            }
            if (ok && ok2) {
                return true;
            }
            return false;
        };
        DiamondsView.prototype.getRandom = function (min, max) {
            return Math.floor(Math.random() * (max - min) + min);
        };
        DiamondsView.prototype.restart = function () {
            for (var i = 0; i < this._actualDiamond.length; i++) {
                this.remove(this._actualDiamond[i]);
                this._actualDiamond[i].kill();
                this._actualDiamond[i].destroy(true);
                this._actualDiamond[i] = null;
            }
            this.addDiamonds();
        };
        return DiamondsView;
    })(Phaser.Group);
    Temple.DiamondsView = DiamondsView;
})(Temple || (Temple = {}));
var Temple;
(function (Temple) {
    var EnemyView = (function (_super) {
        __extends(EnemyView, _super);
        function EnemyView(game) {
            _super.call(this, game);
            this._gameControler = Temple.GameControler.getInstance();
            this._diamondControler = Temple.DiamondsControler.getInstance();
            this._obstacleControler = Temple.ObstacleControler.getInstance();
            this._panleGameControler = Temple.PanelGameControler.getInstance();
            this.enableBody = true;
            /*
           
            var gra: Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
            gra.beginFill(0x000000, 0.5);
            gra.drawRect(50, 100, 700, 450);
            gra.endFill();

            this._boundsGr = this.game.add.group();
            this._boundsGr.enableBody = true;
            this._boundsGr.name = "bounds";
            this.add(this._boundsGr);

            this._bounds = new Phaser.Sprite(this.game, 0, 0, "",this._boundsGr);
            this._bounds.name = "bounds";
            this._bounds.addChild(gra);
            this._boundsGr.add(this._bounds);
            */
            //this.add(this._bounds);
            //this._bounds.checkWorldBounds = true;
            this.addEnemis();
        }
        EnemyView.prototype.addEnemis = function () {
            var currentLevel = this._diamondControler.currentLevel;
            this.enemArray = new Array();
            if (currentLevel == 0) {
            }
            else if (currentLevel == 1) {
                var _enemy = new Temple.Enemy(this.game, 0, 0, "enemy2");
                _enemy.position.setTo(400, 200);
                this.game.physics.enable(_enemy, Phaser.Physics.ARCADE);
                _enemy.body.setSize(50, 25, 0, 5);
                this.enemArray.push(_enemy);
            }
            else if (currentLevel == 2) {
                var _enemy = new Temple.Enemy(this.game, 0, 0, "enemy2");
                _enemy.position.setTo(400, 200);
                this.game.physics.enable(_enemy, Phaser.Physics.ARCADE);
                _enemy.body.setSize(50, 25, 0, 5);
                var _enemyHand = new Temple.Enemy(this.game, 0, 0, "enemy1");
                _enemyHand.position.setTo(300, 300);
                this.game.physics.enable(_enemyHand, Phaser.Physics.ARCADE);
                _enemyHand.body.setSize(50, 50, 0, 10);
                this.enemArray.push(_enemy);
                this.enemArray.push(_enemyHand);
            }
            else if (currentLevel == 3) {
                var _enemy = new Temple.Enemy(this.game, 0, 0, "enemy2");
                _enemy.position.setTo(400, 200);
                this.game.physics.enable(_enemy, Phaser.Physics.ARCADE);
                _enemy.body.setSize(50, 25, 0, 5);
                var _enemyHand = new Temple.Enemy(this.game, 0, 0, "enemy1");
                _enemyHand.position.setTo(300, 300);
                this.game.physics.enable(_enemyHand, Phaser.Physics.ARCADE);
                _enemyHand.body.setSize(50, 50, 0, 10);
                var _enemyHand2 = new Temple.Enemy(this.game, 0, 0, "enemy1");
                _enemyHand2.position.setTo(300, 300);
                this.game.physics.enable(_enemyHand2, Phaser.Physics.ARCADE);
                _enemyHand2.body.setSize(50, 50, 0, 10);
                this.enemArray.push(_enemy);
                this.enemArray.push(_enemyHand);
                this.enemArray.push(_enemyHand2);
            }
            else if (currentLevel == 4) {
                var _enemy = new Temple.Enemy(this.game, 0, 0, "enemy2");
                _enemy.position.setTo(400, 200);
                this.game.physics.enable(_enemy, Phaser.Physics.ARCADE);
                _enemy.body.setSize(50, 25, 0, 5);
                var _enemyHand = new Temple.Enemy(this.game, 0, 0, "enemy1");
                _enemyHand.position.setTo(300, 300);
                this.game.physics.enable(_enemyHand, Phaser.Physics.ARCADE);
                _enemyHand.body.setSize(50, 50, 0, 10);
                var _enemyHand2 = new Temple.Enemy(this.game, 0, 0, "enemy1");
                _enemyHand2.position.setTo(300, 300);
                this.game.physics.enable(_enemyHand2, Phaser.Physics.ARCADE);
                _enemyHand2.body.setSize(50, 50, 0, 10);
                this.enemArray.push(_enemy);
                this.enemArray.push(_enemyHand);
                this.enemArray.push(_enemyHand2);
            }
            else if (currentLevel == 5) {
                var _enemy = new Temple.Enemy(this.game, 0, 0, "enemy2");
                _enemy.position.setTo(400, 200);
                this.game.physics.enable(_enemy, Phaser.Physics.ARCADE);
                _enemy.body.setSize(50, 25, 0, 5);
                var _enemyHand = new Temple.Enemy(this.game, 0, 0, "enemy1");
                _enemyHand.position.setTo(300, 300);
                this.game.physics.enable(_enemyHand, Phaser.Physics.ARCADE);
                _enemyHand.body.setSize(50, 50, 0, 10);
                var _enemyHand2 = new Temple.Enemy(this.game, 0, 0, "enemy1");
                _enemyHand2.position.setTo(300, 300);
                this.game.physics.enable(_enemyHand2, Phaser.Physics.ARCADE);
                _enemyHand2.body.setSize(50, 50, 0, 10);
                this.enemArray.push(_enemy);
                this.enemArray.push(_enemyHand);
                this.enemArray.push(_enemyHand2);
            }
            this.addEnemisFromArra();
        };
        EnemyView.prototype.addEnemisFromArra = function () {
            for (var i = 0; i < this.enemArray.length; i++) {
                this.add(this.enemArray[i]);
            }
        };
        EnemyView.prototype.removeEnemisFromArray = function () {
            for (var i = 0; i < this.enemArray.length; i++) {
                this.enemArray[i].destroy(true);
                this.remove(this.enemArray[i]);
            }
        };
        EnemyView.prototype.update = function () {
            // this.positionVector.setTo((this.positionVector.x + (this.moveVector.x)),(this.positionVector.y + (this.moveVector.y)));
            //
            // this._enemy.moveTo();
            // console.log("gamepasued", this._gameControler.gamePaused, this._obstacleControler.checkCollisionObstale);
            if (!this._gameControler.gamePaused && this._obstacleControler.checkCollisionObstale && !this._obstacleControler.enemyCollision) {
                this.game.physics.arcade.overlap(this._gameControler.player, this, this.collisionEnemy, null, this);
            }
            if (this.enemArray != null && this.enemArray.length > 0) {
                for (var i = 0; i < this.enemArray.length; i++) {
                    if (this.enemArray[i].x < 74) {
                        this.enemArray[i].x = 75;
                        this.enemArray[i].makeStartEnemy();
                    }
                    else if (this.enemArray[i].x > 726) {
                        this.enemArray[i].x = 725;
                        this.enemArray[i].makeStartEnemy();
                    }
                    if (this.enemArray[i].y < 124) {
                        this.enemArray[i].y = 125;
                        this.enemArray[i].makeStartEnemy();
                    }
                    else if (this.enemArray[i].y > 526) {
                        this.enemArray[i].y = 525;
                        this.enemArray[i].makeStartEnemy();
                    }
                }
            }
            //this._enemy.update();
            //this._enemyHand.update();
        };
        EnemyView.prototype.stopEnemy = function () {
            this.forEach(function (item) {
                if (item.name != "bounds")
                    item.makeStopEnemy();
            }, this);
        };
        EnemyView.prototype.startEnemy = function () {
            this.forEach(function (item) {
                if (item.name != "bounds")
                    item.makeStartEnemy();
            }, this);
        };
        EnemyView.prototype.collisionEnemy = function (player, enemy) {
            // this._gameControler.gamePaused = true;
            enemy.playAudio();
            this._gameControler.player.stars();
            this._panleGameControler.panelGame.changeLife();
        };
        EnemyView.prototype.restart = function () {
            this.forEach(function (item) {
                if (item.name != "bounds")
                    item.restart();
            }, this);
        };
        return EnemyView;
    })(Phaser.Group);
    Temple.EnemyView = EnemyView;
})(Temple || (Temple = {}));
var Temple;
(function (Temple) {
    var FinishScreen = (function (_super) {
        __extends(FinishScreen, _super);
        function FinishScreen(game) {
            _super.call(this, game);
            this._gameControl = Temple.GameControler.getInstance();
            this.game = game;
            this.create();
        }
        FinishScreen.prototype.create = function () {
            this._bgBlack = new Phaser.Graphics(this.game, 0, 0);
            this._bgBlack.beginFill(0x000000, 0.6);
            this._bgBlack.drawRect(0, 0, this.game.width, this.game.height);
            this._bgBlack.endFill();
            this.add(this._bgBlack);
            this.gameOverSpr = new Temple.PopUp(this.game, this, 0, 0);
            this.youWinSpr = new Temple.PopUpWin(this.game, this, 0, 0);
            // this.visible = 
            this.add(this.gameOverSpr);
            this.addChild(this.youWinSpr);
            /*
            this._finishScreen = new Phaser.Sprite(this.game, 0, 0, "scoreScreen");
            this._finishScreen.animations.add("play");
            this._finishScreen.play("play", 24, true);
            this._finishScreen.x = this.game.width / 2 - this._finishScreen.width / 2;
            this._finishScreen.y = this.game.height / 2 - this._finishScreen.height / 2;


            this.addChild(this._finishScreen);

            this._playButton = new Phaser.Button(this.game, 0, 0, "playScoreScreen", this.playAgain, this, 1, 0, 1, 0);

            this._quit = new Phaser.Button(this.game, 300, 10, "playScoreScreen", this.quitClick, this, 3, 2, 3, 2);
            this._playButton.x = 257;
            this._playButton.y = 358;
            this._quit.x = 427;
            this._quit.y = 358;
            this._finishScreen.addChild(this._playButton);
            this._finishScreen.addChild(this._quit);


            this._finishScreen.visible = false;
            this._playButton.visible = false;

            this._quit.visible = false;

            */
        };
        FinishScreen.prototype.fail = function () {
            this._anim = new Phaser.Sprite(this.game, 0, 0, "fail1", 0);
            this._anim.animations.add("play");
            this._anim.x = this.game.width / 2 - this._anim.width / 2;
            this._anim.y = this.game.height / 2 - this._anim.height / 2;
            this._anim.animations.getAnimation("play").onComplete.add(this.completeAnim, this);
            this.add(this._anim);
            this._anim.animations.play("play", 24, false, true);
            this.screenWinOrGameOver = new Temple.PopUp(this.game, this, 0, 0);
        };
        FinishScreen.prototype.win = function () {
            this._anim = new Phaser.Sprite(this.game, 0, 0, "success1", 0);
            this._anim.animations.add("play");
            this._anim.x = this.game.width / 2 - this._anim.width / 2;
            this._anim.y = this.game.height / 2 - this._anim.height / 2;
            this._anim.animations.getAnimation("play").onComplete.add(this.completeAnim, this);
            this.add(this._anim);
            this._anim.animations.play("play", 12, false, true);
            this.screenWinOrGameOver = new Temple.PopUpWin(this.game, this, 0, 0);
        };
        FinishScreen.prototype.completeAnim = function () {
            this.game.time.events.add(Phaser.Timer.SECOND * 2, function () {
                this.addChild(this.screenWinOrGameOver);
                this.screenWinOrGameOver.activeButton();
                this.screenWinOrGameOver.visible = true;
            }, this);
        };
        FinishScreen.prototype.hideScreenAfterFinishPlaye = function () {
            this.playAgain();
        };
        FinishScreen.prototype.show = function (youWin) {
            if (youWin) {
                this.win();
            }
            else {
                this.fail();
            }
        };
        FinishScreen.prototype.playAgain = function () {
            this.removeChild(this._bgBlack);
            this.removeChild(this.screenWinOrGameOver);
            this.visible = false;
            this._gameControl.level.restartGame();
            //this.destroy(true);
            // this.removeAll(true);
            console.log("play again");
        };
        FinishScreen.prototype.quitClick = function () {
            console.log("quit");
        };
        FinishScreen.prototype.countFrames = function (start, end) {
            var countArr = new Array();
            for (var i = start; i < end; i++) {
                countArr.push(i);
            }
            return countArr;
        };
        return FinishScreen;
    })(Phaser.Group);
    Temple.FinishScreen = FinishScreen;
})(Temple || (Temple = {}));
var Temple;
(function (Temple) {
    var ObstacleView = (function (_super) {
        __extends(ObstacleView, _super);
        function ObstacleView(game) {
            _super.call(this, game);
            this._gameControler = Temple.GameControler.getInstance();
            this._obstacleControler = Temple.ObstacleControler.getInstance();
            this._panleGameControler = Temple.PanelGameControler.getInstance();
            this._diamondControler = Temple.DiamondsControler.getInstance();
            this.enableBody = true;
            this._home = new Phaser.Sprite(this.game, 0, 0, "home");
            this._home.name = "home";
            // this._home.body.setSize(50, 50, 0, 0);
            this.addObstacle(this._home, new Phaser.Point(50, 100));
            this._audioTrap = this.game.add.audio("trap", 1, false);
            this.addEnemis();
        }
        ObstacleView.prototype.addEnemis = function () {
            var currentLevel = this._diamondControler.currentLevel;
            this.enemArray = new Array();
            if (currentLevel == 0) {
                this._hole = new Phaser.Sprite(this.game, 0, 0, "hole");
                this._hole.animations.add("hole", this.countFrames(0, 57), 24, false);
                this._hole.name = "hole";
                this.addObstacle(this._hole, new Phaser.Point(300, 300));
                this._strikeStill = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                this._strikeStill.name = "strikeStill";
                this.addObstacle(this._strikeStill, new Phaser.Point(400, 300));
                this.enemArray.push(this._hole);
                this.enemArray.push(this._strikeStill);
            }
            else if (currentLevel == 1) {
                this._hole = new Phaser.Sprite(this.game, 0, 0, "hole");
                this._hole.animations.add("hole", this.countFrames(0, 57), 24, false);
                this._hole.name = "hole";
                this.addObstacle(this._hole, new Phaser.Point(300, 300));
                this._strikeStill = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                this._strikeStill.name = "strikeStill";
                this.addObstacle(this._strikeStill, new Phaser.Point(400, 300));
                var strikeStill = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                strikeStill.name = "strikeStill";
                this.addObstacle(strikeStill, new Phaser.Point(450, 300));
                this.enemArray.push(this._hole);
                this.enemArray.push(this._strikeStill);
                this.enemArray.push(strikeStill);
            }
            else if (currentLevel == 2) {
                this._hole = new Phaser.Sprite(this.game, 0, 0, "hole");
                this._hole.animations.add("hole", this.countFrames(0, 57), 24, false);
                this._hole.name = "hole";
                this.addObstacle(this._hole, new Phaser.Point(200, 250));
                this._strikeStill = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                this._strikeStill.name = "strikeStill";
                this.addObstacle(this._strikeStill, new Phaser.Point(350, 250));
                var strikeStill = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                strikeStill.name = "strikeStill";
                this.addObstacle(strikeStill, new Phaser.Point(450, 200));
                var hole = new Phaser.Sprite(this.game, 0, 0, "hole");
                hole.animations.add("hole", this.countFrames(0, 57), 24, false);
                hole.name = "hole";
                this.addObstacle(hole, new Phaser.Point(200, 200));
                this.enemArray.push(strikeStill);
                this.enemArray.push(hole);
                this.enemArray.push(this._hole);
                this.enemArray.push(this._strikeStill);
            }
            else if (currentLevel == 3) {
                this._hole = new Phaser.Sprite(this.game, 0, 0, "hole");
                this._hole.animations.add("hole", this.countFrames(0, 57), 24, false);
                this._hole.name = "hole";
                this.addObstacle(this._hole, new Phaser.Point(350, 100));
                this._strikeStill = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                this._strikeStill.name = "strikeStill";
                this.addObstacle(this._strikeStill, new Phaser.Point(400, 300));
                var strikeStill = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                strikeStill.name = "strikeStill";
                this.addObstacle(strikeStill, new Phaser.Point(450, 200));
                var hole = new Phaser.Sprite(this.game, 0, 0, "hole");
                hole.animations.add("hole", this.countFrames(0, 57), 24, false);
                hole.name = "hole";
                this.addObstacle(hole, new Phaser.Point(200, 250));
                var hole2 = new Phaser.Sprite(this.game, 0, 0, "hole");
                hole2.animations.add("hole", this.countFrames(0, 57), 24, false);
                hole2.name = "hole";
                this.addObstacle(hole2, new Phaser.Point(300, 250));
                this.enemArray.push(strikeStill);
                this.enemArray.push(hole);
                this.enemArray.push(hole2);
                this.enemArray.push(this._hole);
                this.enemArray.push(this._strikeStill);
            }
            else if (currentLevel == 4) {
                this._hole = new Phaser.Sprite(this.game, 0, 0, "hole");
                this._hole.animations.add("hole", this.countFrames(0, 57), 24, false);
                this._hole.name = "hole";
                this.addObstacle(this._hole, new Phaser.Point(300, 400));
                var hole2 = new Phaser.Sprite(this.game, 0, 0, "hole");
                hole2.animations.add("hole", this.countFrames(0, 57), 24, false);
                hole2.name = "hole";
                this.addObstacle(hole2, new Phaser.Point(650, 350));
                var hole = new Phaser.Sprite(this.game, 0, 0, "hole");
                hole.animations.add("hole", this.countFrames(0, 57), 24, false);
                hole.name = "hole";
                this.addObstacle(hole, new Phaser.Point(550, 250));
                this._strikeStill = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                this._strikeStill.name = "strikeStill";
                this.addObstacle(this._strikeStill, new Phaser.Point(200, 200));
                var strikeStill = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                strikeStill.name = "strikeStill";
                this.addObstacle(strikeStill, new Phaser.Point(450, 350));
                var strikeStill2 = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                strikeStill2.name = "strikeStill";
                this.addObstacle(strikeStill2, new Phaser.Point(500, 250));
                this.enemArray.push(strikeStill);
                this.enemArray.push(hole2);
                this.enemArray.push(hole);
                this.enemArray.push(this._hole);
                this.enemArray.push(this._strikeStill);
            }
            else if (currentLevel == 5) {
                this._hole = new Phaser.Sprite(this.game, 0, 0, "hole");
                this._hole.animations.add("hole", this.countFrames(0, 57), 24, false);
                this._hole.name = "hole";
                this.addObstacle(this._hole, new Phaser.Point(300, 400));
                var hole2 = new Phaser.Sprite(this.game, 0, 0, "hole");
                hole2.animations.add("hole", this.countFrames(0, 57), 24, false);
                hole2.name = "hole";
                this.addObstacle(hole2, new Phaser.Point(650, 350));
                var hole = new Phaser.Sprite(this.game, 0, 0, "hole");
                hole.animations.add("hole", this.countFrames(0, 57), 24, false);
                hole.name = "hole";
                this.addObstacle(hole, new Phaser.Point(550, 200));
                this._strikeStill = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                this._strikeStill.name = "strikeStill";
                this.addObstacle(this._strikeStill, new Phaser.Point(200, 150));
                var strikeStill = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                strikeStill.name = "strikeStill";
                this.addObstacle(strikeStill, new Phaser.Point(450, 350));
                var strikeStill2 = new Phaser.Sprite(this.game, 0, 0, "spikeStill");
                strikeStill2.name = "strikeStill";
                this.addObstacle(strikeStill2, new Phaser.Point(500, 200));
                this.enemArray.push(strikeStill);
                this.enemArray.push(hole2);
                this.enemArray.push(hole);
                this.enemArray.push(this._hole);
                this.enemArray.push(this._strikeStill);
            }
            this.addEnemisFromArra();
        };
        ObstacleView.prototype.addEnemisFromArra = function () {
            for (var i = 0; i < this.enemArray.length; i++) {
                this.add(this.enemArray[i]);
                this.game.physics.enable(this.enemArray[i], Phaser.Physics.ARCADE);
                this.enemArray[i].body.setSize(46, 46, 0, 0);
            }
        };
        ObstacleView.prototype.removeEnemisFromArray = function () {
            for (var i = 0; i < this.enemArray.length; i++) {
                this.enemArray[i].destroy(true);
                this.remove(this.enemArray[i]);
            }
        };
        ObstacleView.prototype.addObstacle = function (object, pointer) {
            var x = Math.floor(pointer.x);
            var y = Math.floor(pointer.y);
            x = x - (x % 50);
            y = y - (y % 50);
            object.x = x;
            object.y = y;
            this.add(object);
            object.body.setSize(50, 50);
        };
        ObstacleView.prototype.update = function () {
            if (!this._gameControler.gamePaused && this._obstacleControler.checkCollisionObstale) {
                this.game.physics.arcade.overlap(this._gameControler.player, this, this.collisionHome, null, this);
            }
        };
        ObstacleView.prototype.collisionHome = function (player, home) {
            if (this._obstacleControler.checkCollisionObstale && !this._obstacleControler.enemyCollision) {
                if (!mobile && home.name == "home" && !this._gameControler.lastPositionPlayer.equals(new Phaser.Point(this._gameControler.player.x, this._gameControler.player.y))) {
                    this._obstacleControler.checkCollisionObstale = false;
                    console.log("home, home");
                    //   this._home.body.setSize(0, 0, 0, 0);
                    this.game.time.events.add(Phaser.Timer.SECOND * 6, function () {
                        this._home.body.setSize(50, 50, 0, 0);
                    }, this);
                    console.log("pokazuj kurwa szmato", this._gameControler.bylemTu);
                    if (!this._gameControler.bylemTu) {
                        console.log("pokazuj kurwa szmato", this._gameControler.bylemTu);
                        this._gameControler.bylemTu = true;
                        console.log("pokazuj kurwa szmato 0");
                        this._gameControler.level.showDrawStartDiamond();
                    }
                }
                if (home.name == "hole") {
                    this._obstacleControler.checkCollisionObstale = false;
                    this._gameControler.player.stars();
                    this._gameControler.lastPositionPlayer = new Phaser.Point(0, 0);
                    home.play("hole", 24, false);
                    //   this._hole.body.setSize(0, 0, -1000, 700);
                    this._audioTrap.play();
                    this._panleGameControler.panelGame.changeLife();
                }
                else if (home.name == "strikeStill") {
                    this._obstacleControler.checkCollisionObstale = false;
                    this._gameControler.player.stars();
                    this._panleGameControler.panelGame.changeLife();
                    this._gameControler.lastPositionPlayer = new Phaser.Point(0, 0);
                }
            }
        };
        ObstacleView.prototype.collisionHole = function (player, hole) {
            // this._obstacleControler.checkCollisionObstale = false;
        };
        ObstacleView.prototype.countFrames = function (start, end) {
            var countArr = new Array();
            for (var i = start; i < end; i++) {
                countArr.push(i);
            }
            return countArr;
        };
        return ObstacleView;
    })(Phaser.Group);
    Temple.ObstacleView = ObstacleView;
})(Temple || (Temple = {}));
var Temple;
(function (Temple) {
    var PanelGame = (function (_super) {
        __extends(PanelGame, _super);
        function PanelGame(game) {
            _super.call(this, game);
            this._gameControler = Temple.GameControler.getInstance();
            this._obstacleControler = Temple.ObstacleControler.getInstance();
            this._gemsCount = 0;
            this._bonusCount = 0;
            this._seconds = 60;
            this._lifeCount = 0;
            this._diamonds = new Array({ diamond: "redDiamond", i: 1 }, { diamond: "blueDiamond", i: 2 }, { diamond: "greenDiamond", i: 0 });
            this._bg = this.game.add.sprite(-40, -20, "gameScore", 0, this);
            this._liveAnim = this.game.add.sprite(664, 18, "life", 0, this);
            this._collectGroup = this.game.add.group();
            this._collectGroup.x = -30;
            this.add(this._collectGroup);
            this._collectGroup.visible = false;
            var style = { font: "45px Arial", fill: "#ffffff", align: "left" };
            var style2 = { font: "25px Arial", fill: "#ffffff", align: "left" };
            this._time = this.game.add.text(360, 2, "60", style);
            this._gems = this.game.add.text(520, 17, "0", style2);
            this._bonus = this.game.add.text(593, 17, "0", style2);
            this._glow = new Phaser.Sprite(this.game, 0, 0, "glow");
            this.addChild(this._gems);
            this.addChild(this._bonus);
            this.addChild(this._time);
            this._glow.x = 28;
            this.addChild(this._glow);
            this.setGlow();
            this.startTimer();
        }
        PanelGame.prototype.setGlow = function (num) {
            if (num === void 0) { num = 0; }
            this._glow.frame = Temple.DiamondsControler.getInstance().currentLevel;
        };
        PanelGame.prototype.showCollectDiamond = function (diamond, kolejnosc, show) {
            if (kolejnosc === void 0) { kolejnosc = 0; }
            if (show === void 0) { show = true; }
            if (!show) {
                this._collectGroup.forEach(function (item) {
                    item.kill();
                }, this);
                // this._collectGroup.visible = false;
                return 0;
            }
            this._collectGroup.visible = true;
            var collectDiamonds = new Phaser.Sprite(this.game, 0, 0, "collectDiamond", 0);
            collectDiamonds.x = (collectDiamonds.width * kolejnosc) + 5 * (kolejnosc + 1);
            this._collectGroup.add(collectDiamonds);
            var i = this.checkIndecDiamond(diamond);
            // this._collectDiamonds.frame = i;
        };
        PanelGame.prototype.removeCollectDiamond = function () {
            this._collectGroup.forEach(function (item) {
                item.kill();
            }, this);
        };
        PanelGame.prototype.checkIndecDiamond = function (diamondName) {
            for (var i = 0; i < this._diamonds.length; i++) {
                if (this._diamonds[i].diamond == diamondName) {
                    return this._diamonds[i].i;
                }
            }
            return 0;
        };
        PanelGame.prototype.changeLife = function () {
            if (this._lifeCount >= 3)
                return;
            this._lifeCount++;
            this._liveAnim.frame = this._lifeCount;
            // this._lifes.setText("" + this._lifeCount);
            if (this._lifeCount >= 3) {
                this._gameControler.gamePaused = true;
                this._gameControler.player.death();
            }
        };
        PanelGame.prototype.restartLife = function () {
            this._lifeCount = 0;
            this._liveAnim.frame = 0;
        };
        PanelGame.prototype.resetGems = function () {
            this._gemsCount = 0;
            this._gems.setText("" + this._gemsCount);
            this.startTimer();
        };
        PanelGame.prototype.addGems = function () {
            this._gemsCount += 5;
            this._gems.setText("" + this._gemsCount);
        };
        PanelGame.prototype.addBonus = function (ile) {
            if (ile == -1)
                this._bonusCount = 1;
            this._bonusCount += ile;
            this._bonus.setText("" + this._bonusCount);
        };
        PanelGame.prototype.startTimer = function () {
            this.resetTimer();
            this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);
        };
        PanelGame.prototype.stopTimer = function () {
            this.game.time.events.removeAll();
        };
        PanelGame.prototype.resetTimer = function () {
            console.log("RESET CZAS");
            //  this._time.setText("00:00");
            //  this._seconds = 0;
        };
        PanelGame.prototype.updateTimer = function () {
            if (!this._gameControler.gamePaused) {
                if (this._seconds > 0) {
                    this._seconds--;
                }
                else {
                    this.stopTimer();
                    this._seconds = 60;
                    this._gameControler.endGame(this._gemsCount > 4);
                }
                var sec;
                sec = "" + this._seconds;
                this._time.setText("" + sec);
            }
        };
        return PanelGame;
    })(Phaser.Group);
    Temple.PanelGame = PanelGame;
})(Temple || (Temple = {}));
var Temple;
(function (Temple) {
    var PopUp = (function (_super) {
        __extends(PopUp, _super);
        function PopUp(game, level, x, y) {
            _super.call(this, game, x, y);
            this.level1 = level;
            this._game = game;
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var bgPopup = new Phaser.Sprite(this.game, 0, 0, "popup");
            this.addChild(bgPopup);
            this.logo = new Phaser.Sprite(this.game, 0, 0, "logoGame");
            this.addChild(this.logo);
            this.logo.x = (bgPopup.width / 2 - this.logo.width / 2);
            this.logo.y = this.logo.height - 20;
            var text = "Good try.\n Keep playing to win credits";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this.game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;
            textDescription.x = 400 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 130;
            this.addChild(textDescription);
            this.textLevel();
            //this.beatText();
            this.addButtons();
            this.visible = false;
        }
        PopUp.prototype.addButtons = function () {
            this.gamesBtn = new Phaser.Button(this._game, 0, 0, "games", this.gamesBtnClick, this, 1, 0);
            this.playBtn = new Phaser.Button(this._game, 100, 0, "playGame", this.playBtnClick, this, 1, 0);
            this.gamesBtn.inputEnabled = true;
            this.gamesBtn.input.priorityID = 10;
            this.playBtn.inputEnabled = true;
            this.playBtn.input.priorityID = 10;
            var buttonsGr = new Phaser.Group(this._game);
            this.addChild(buttonsGr);
            buttonsGr.add(this.gamesBtn);
            buttonsGr.add(this.playBtn);
            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;
        };
        PopUp.prototype.activeButton = function () {
            this.gamesBtn.inputEnabled = true;
            this.playBtn.inputEnabled = true;
        };
        PopUp.prototype.gamesBtnClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        PopUp.prototype.playBtnClick = function () {
            this.visible = false;
            this.gamesBtn.inputEnabled = false;
            //   this.gamesBtn.input.priorityID = 3;
            this.playBtn.inputEnabled = false;
            // this.playBtn.input.priorityID = 3;
            this.level1.hideScreenAfterFinishPlaye();
            //  this.game.state.start("PopupStart", true, false);
        };
        PopUp.prototype.textLevel = function () {
            var text = "GAME OVER";
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this._game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = this.game.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 25;
            this.addChild(textDescription);
        };
        return PopUp;
    })(Phaser.Sprite);
    Temple.PopUp = PopUp;
})(Temple || (Temple = {}));
function launchGame(result) {
    userLevel = result.level;
    if (userLevel > 4) {
        userLevel = 4;
        APIsetLevel(userID, gameID, userLevel);
    }
    console.log("user.level", result, result.level);
}
//declare function launchGame(result);
var Temple;
(function (Temple) {
    var PopupStart = (function (_super) {
        __extends(PopupStart, _super);
        function PopupStart() {
            _super.apply(this, arguments);
        }
        PopupStart.prototype.create = function () {
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var background = this.add.sprite(0, 0, 'mainMenuBg');
            var bgPopup = this.add.sprite(0, 0, "popup");
            this.logo = this.add.sprite(0, 0, "logoGame");
            this.logo.x = (bgPopup.width / 2 - this.logo.width / 2);
            this.logo.y = this.logo.height - 20;
            var text = "Collect gems in the right order.\n Get 14 gem points to win!\n Remember the order of the gems you see.";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;
            textDescription.x = this.game.stage.width / 2 - textDescription.wordWrapWidth / 2;
            textDescription.y = this.logo.y + this.logo.height + 120;
            this.textLevel();
            //this.beatText();
            this.addButtons();
        };
        PopupStart.prototype.addButtons = function () {
            var gamesBtn = this.add.button(0, 0, "games", this.gamesBtnClick, this, 1, 0);
            var playBtn = this.add.button(100, 0, "playGame", this.playBtnClick, this, 1, 0);
            var buttonsGr = this.add.group();
            buttonsGr.add(gamesBtn);
            buttonsGr.add(playBtn);
            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;
        };
        PopupStart.prototype.gamesBtnClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        PopupStart.prototype.playBtnClick = function () {
            this.game.state.start("Instruction", true, false);
        };
        PopupStart.prototype.beatText = function () {
            var text = "BEAT THIS LEVEL";
            //" 6";
            var style = { font: "bold 13px Arial", fill: "#660000", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = (this.game.width / 2) + (140) - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 100;
            var numberText = "6";
            var styleNumber = { font: "bold 17px Arial", fill: "#990000", align: "center" };
            var numberShow = this.add.text(0, 0, numberText, styleNumber);
            numberShow.x = textDescription.x + textDescription.width / 2 - numberShow.width / 2;
            numberShow.y = textDescription.y + textDescription.height;
            var tunesText = "MORE TUNES TO WIN";
            var tunesShow = this.add.text(0, 0, tunesText, style);
            tunesShow.wordWrap = true;
            tunesShow.wordWrapWidth = 100;
            tunesShow.x = textDescription.x + textDescription.width / 2 - tunesShow.width / 2;
            tunesShow.y = numberShow.y + numberShow.height;
            var creditsNmText = "25";
            var styleCredits = { font: "bold 17px Arial", fill: "#ff0000", align: "center" };
            var credistNmShow = this.add.text(0, 0, creditsNmText, styleCredits);
            credistNmShow.x = tunesShow.x + tunesShow.width / 2 - credistNmShow.width / 2;
            credistNmShow.y = tunesShow.y + tunesShow.height;
            var creditsText = "CREDITS";
            var credistShow = this.add.text(0, 0, creditsText, style);
            credistShow.x = tunesShow.x + tunesShow.width / 2 - credistShow.width / 2;
            credistShow.y = credistNmShow.y + credistNmShow.height;
        };
        PopupStart.prototype.textCredits = function () {
            var text = "Beat this level";
            //" 6";
            var style = { font: "bold 14px Arial", fill: "#660000", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = (this.game.width / 2) + 257 / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 100;
            var numberText = "6";
            var styleNumber = { font: "bold 18px Arial", fill: "#990000", align: "center" };
            var numberShow = this.add.text(0, 0, numberText, styleNumber);
            numberShow.x = textDescription.x + textDescription.width / 2 - numberShow.width / 2;
            numberShow.y = textDescription.y + textDescription.height;
        };
        PopupStart.prototype.textLevel = function () {
            //APIgetLevel(userID, gameID);
            var text = "LEVEL " + userLevel;
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = this.game.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 25;
        };
        return PopupStart;
    })(Phaser.State);
    Temple.PopupStart = PopupStart;
})(Temple || (Temple = {}));
var Temple;
(function (Temple) {
    var PopUpWin = (function (_super) {
        __extends(PopUpWin, _super);
        function PopUpWin(game, level, x, y) {
            _super.call(this, game, x, y);
            this._game = game;
            this.level1 = level;
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var bgPopup = new Phaser.Sprite(this.game, 0, 0, "popup");
            this.addChild(bgPopup);
            this.logo = new Phaser.Sprite(this.game, 0, 0, "logoGame");
            this.addChild(this.logo);
            this.logo.x = (bgPopup.width / 2 - this.logo.width / 2);
            this.logo.y = this.logo.height - 20;
            var text = "You finish the level.\n Keep Playing to win credits!";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this.game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;
            textDescription.x = 400 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 130;
            this.addChild(textDescription);
            this.textLevel();
            //this.beatText();
            this.addButtons();
            this.visible = false;
        }
        PopUpWin.prototype.addButtons = function () {
            this.gamesBtn = new Phaser.Button(this._game, 0, 0, "games", this.gamesBtnClick, this, 1, 0);
            this.playBtn = new Phaser.Button(this._game, 100, 0, "playGame", this.playBtnClick, this, 1, 0);
            this.gamesBtn.inputEnabled = true;
            this.gamesBtn.input.priorityID = 10;
            this.playBtn.inputEnabled = true;
            this.playBtn.input.priorityID = 10;
            var buttonsGr = new Phaser.Group(this._game);
            this.addChild(buttonsGr);
            buttonsGr.add(this.gamesBtn);
            buttonsGr.add(this.playBtn);
            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;
        };
        PopUpWin.prototype.activeButton = function () {
            this.gamesBtn.inputEnabled = true;
            this.playBtn.inputEnabled = true;
        };
        PopUpWin.prototype.gamesBtnClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        PopUpWin.prototype.playBtnClick = function () {
            this.visible = false;
            this.gamesBtn.inputEnabled = false;
            this.gamesBtn.input.priorityID = 3;
            this.playBtn.inputEnabled = false;
            this.playBtn.input.priorityID = 3;
            this.level1.hideScreenAfterFinishPlaye();
            //  this.game.state.start("PopupStart", true, false);
        };
        PopUpWin.prototype.textLevel = function () {
            var text = "GOOD JOB";
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this._game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = this.game.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 25;
            this.addChild(textDescription);
        };
        return PopUpWin;
    })(Phaser.Sprite);
    Temple.PopUpWin = PopUpWin;
})(Temple || (Temple = {}));
var Temple;
(function (Temple) {
    var ShowDiamond = (function (_super) {
        __extends(ShowDiamond, _super);
        function ShowDiamond(game) {
            _super.call(this, game);
            this._gameControler = Temple.GameControler.getInstance();
            this._obstacleControler = Temple.ObstacleControler.getInstance();
            this._diamondsControler = Temple.DiamondsControler.getInstance();
            this._diamondArr = new Array("blueDiamond", "redDiamond", "greenDiamond");
            this._currentDiamond = 0;
            this._maxDiamond = 2;
            this.blokadaShow = false;
            this._bgBlack = new Phaser.Graphics(this.game, 0, 0);
            this._bgBlack.beginFill(0x000000, 0.6);
            this._bgBlack.drawRect(0, 0, this.game.width, this.game.height);
            this._bgBlack.endFill();
            this.add(this._bgBlack);
            this._titleCircle = this.game.add.sprite(0, 0, "titleCircle", 0, this);
            this._titleCircle.x = this.game.width / 2 - this._titleCircle.width / 2;
            this._titleCircle.y = this.game.height / 2 - this._titleCircle.height / 2;
            this._audio = this.game.add.audio("showdiamond", 1, false);
            this.drawDiamond();
        }
        ShowDiamond.prototype.show = function (visible) {
            this.visible = visible;
        };
        ShowDiamond.prototype.drawDiamond = function () {
            this._gameControler.blockWall = true;
            this._gameControler.enemyView.stopEnemy();
            this._drawDiamonArr = new Array();
            for (var i = 0; i < this._diamondsControler.maxDiamonds; i++) {
                this._drawDiamonArr.push(this._diamondArr[this.getRandom(0, this._diamondArr.length)]);
            }
            this._diamondsControler.setDrawDiamonds(this._drawDiamonArr);
            this.showDrawDiamon();
            console.log("drawDiamond");
        };
        ShowDiamond.prototype.showDrawDiamon = function () {
            console.log("showDrawDiamon w klasie", this._currentDiamond);
            if (!this.blokadaShow) {
                this.blokadaShow = true;
                this.showPoblokadzie();
            }
        };
        ShowDiamond.prototype.showPoblokadzie = function () {
            this.visible = true;
            this._gameControler.blockWall = true;
            if (this._currentDiamond == 0) {
                this._gameControler.enemyView.stopEnemy();
                this._gameControler.gamePaused = true;
            }
            if (this._currentDiamond == this._diamondsControler.maxDiamonds) {
                this.visible = false;
                this._currentDiamond = 0;
                this._obstacleControler.checkCollisionObstale = true;
                this._gameControler.gamePaused = false;
                this._gameControler.blockWall = false;
                // console.log("whcodze i sie nie godze", this._gameControler.gamePaused, this._obstacleControler.checkCollisionObstale);
                this._gameControler.enemyView.startEnemy();
                this._gameControler.gamePaused = false;
                this.blokadaShow = false;
            }
            else {
                this._audio.play();
                this.addDiamond(this._drawDiamonArr[this._currentDiamond]);
                this.game.time.events.add(Phaser.Timer.SECOND * 2, function () {
                    this.removeChild(this._diamond);
                }, this);
                this.game.time.events.add(Phaser.Timer.SECOND * 2.5, function () {
                    this.showPoblokadzie();
                }, this);
                this._currentDiamond++;
            }
        };
        ShowDiamond.prototype.addDiamond = function (key) {
            this._diamond = new Phaser.Sprite(this.game, 0, 0, key);
            this._diamond.x = this.game.width / 2 - this._diamond.width / 2;
            this._diamond.y = this.game.height / 2 - this._diamond.height / 2;
            this.add(this._diamond);
        };
        ShowDiamond.prototype.getRandom = function (min, max) {
            max;
            return Math.floor(Math.random() * (max - min) + min);
        };
        return ShowDiamond;
    })(Phaser.Group);
    Temple.ShowDiamond = ShowDiamond;
})(Temple || (Temple = {}));
var Temple;
(function (Temple) {
    var WallDoorsView = (function (_super) {
        __extends(WallDoorsView, _super);
        function WallDoorsView(game) {
            _super.call(this, game);
            this._gameControler = Temple.GameControler.getInstance();
            console.log("chujnia");
            this.enableBody = true;
            var wall1 = new Phaser.Sprite(this.game, 400, 75, "wallDoor", 0);
            wall1.name = "door";
            wall1.anchor.setTo(0.5, 0.5);
            wall1.angle = 0;
            var wall2 = new Phaser.Sprite(this.game, 25, 300, "wallDoor", 0);
            wall2.anchor.setTo(0.5, 0.5);
            wall2.name = "door";
            wall2.angle = 270;
            var wall3 = new Phaser.Sprite(this.game, 775, 300, "wallDoor", 0);
            wall3.anchor.setTo(0.5, 0.5);
            wall3.angle = 90;
            wall3.name = "door";
            var wall4 = new Phaser.Sprite(this.game, 400, 575, "wallDoor", 0);
            wall4.anchor.setTo(0.5, 0.5);
            wall4.angle = 180;
            wall4.name = "door";
            this.add(wall1);
            this.add(wall2);
            this.add(wall3);
            this.add(wall4);
            wall1.body.setSize(25, 50);
            wall2.body.setSize(25, 50);
            wall3.body.setSize(25, 50);
            wall4.body.setSize(25, 50);
            this._wallGr = this.game.add.group();
            this._wallGr.y = 150;
            this._wallGr.x = 50;
            this._wallGr.enableBody = true;
            this.addWalls();
            this._wallGr.alpha = 0;
        }
        WallDoorsView.prototype.obrysujScene = function () {
            var marx = 0;
            var mary = 50;
            for (var i = 0; i < 1; i++) {
                for (var j = 0; j < 16; j++) {
                    if (j == 7 || j == 8) {
                    }
                    else {
                        var wall = new Temple.Wall(this.game, 0, 0);
                        wall.x = marx + 50 * j;
                        wall.y = mary + 50 * i;
                        this._wallGr.add(wall);
                        wall.body.setSize(50, 50);
                    }
                }
            }
            mary = 100;
            for (var i = 0; i < 9; i++) {
                for (var j = 0; j < 1; j++) {
                    if (i == 3 || i == 4) {
                    }
                    else {
                        var wall = new Temple.Wall(this.game, 0, 0);
                        wall.x = marx + 50 * j;
                        wall.y = mary + 50 * i;
                        this._wallGr.add(wall);
                        wall.body.setSize(50, 50);
                    }
                }
            }
            mary = 100;
            marx = 750;
            for (var i = 0; i < 9; i++) {
                for (var j = 0; j < 1; j++) {
                    if (i == 3 || i == 4) {
                    }
                    else {
                        var wall = new Temple.Wall(this.game, 0, 0);
                        wall.x = marx + 50 * j;
                        wall.y = mary + 50 * i;
                        this._wallGr.add(wall);
                        wall.body.setSize(50, 50);
                    }
                }
            }
            marx = 0;
            mary = 550;
            for (var i = 0; i < 1; i++) {
                for (var j = 0; j < 16; j++) {
                    if (j == 7 || j == 8) {
                    }
                    else {
                        var wall = new Temple.Wall(this.game, 0, 0);
                        wall.x = marx + 50 * j;
                        wall.y = mary + 50 * i;
                        this._wallGr.add(wall);
                        wall.body.setSize(50, 50);
                    }
                }
            }
        };
        WallDoorsView.prototype.addWalls = function () {
            this.obrysujScene();
        };
        WallDoorsView.prototype.create = function () {
        };
        WallDoorsView.prototype.update = function () {
            if (!this._gameControler.gamePaused && !this._gameControler.collisonWall) {
                this.game.physics.arcade.overlap(this._gameControler.player, this, this.collision, null, this);
                this.game.physics.arcade.overlap(this._gameControler.player, this._wallGr, this.kolizjaSciany, null, this);
            }
            // this.game.physics.arcade.overlap(this._gameControler.player, this, this.collision, null, this);
        };
        WallDoorsView.prototype.kolizjaSciany = function (player, wall) {
            console.log("kolizja sciana");
            var przesunX = 0;
            var przesunY = 0;
            console.log("przesunX", "przesunY", wall.x, wall.y, Math.floor(this._gameControler.player.x), Math.floor(this._gameControler.player.y));
            if (wall.x == this._gameControler.player.x) {
                if (wall.y < this._gameControler.player.y) {
                    przesunY = 50;
                }
                else {
                    przesunY = 0;
                }
            }
            else if (wall.y == this._gameControler.player.y) {
                if (wall.x < this._gameControler.player.x) {
                    przesunX = 50;
                }
                else {
                    przesunX = 0;
                }
            }
            var stopIsc = false;
            if (wall.x != this._gameControler.player.x && wall.y != this._gameControler.player.y)
                stopIsc = true;
            if (wall.y == this._gameControler.player.y && wall.x == this._gameControler.player.x) {
                przesunX = 0;
                przesunY = 0;
                stopIsc = true;
            }
            if (!stopIsc)
                this._gameControler.level.stopMovePlayer(new Phaser.Point(this._gameControler.player.x + przesunX, this._gameControler.player.y + przesunY));
            else {
            }
            console.log("kolizja sciana");
        };
        WallDoorsView.prototype.collision = function (player, wall) {
            if (wall.name == "door") {
                if (!this._gameControler.gamePaused && !this._gameControler.collisonWall) {
                    if (wall.position.x < 50) {
                        this._gameControler.player.x = 800;
                        this._gameControler.level.playMoveTo(new Phaser.Point(725, player.y));
                    }
                    else if (wall.position.x > 700) {
                        this._gameControler.player.x = -50;
                        this._gameControler.level.playMoveTo(new Phaser.Point(50, player.y));
                    }
                    if (wall.position.y < 100) {
                        this._gameControler.player.y = 550;
                        this._gameControler.level.playMoveTo(new Phaser.Point(player.x, 500));
                    }
                    else if (wall.position.y > 560) {
                        this._gameControler.player.y = -50;
                        this._gameControler.level.playMoveTo(new Phaser.Point(player.x, 100));
                    }
                }
            }
            else {
            }
        };
        return WallDoorsView;
    })(Phaser.Group);
    Temple.WallDoorsView = WallDoorsView;
})(Temple || (Temple = {}));
//# sourceMappingURL=game.js.map