﻿module Rocket {

   
    export class FinishScreen extends Phaser.Group {
        private _finishScreen: Phaser.Sprite;
        private _playButton: Phaser.Button;
        private _quit: Phaser.Button;
        private _game: Phaser.Game;
        private _gameControl: GameControler = GameControler.getInstance();
        constructor(game: Phaser.Game) {
            super(game);
            this._game = game;
            this.create();
        }

        create() {

            this._finishScreen = new Phaser.Sprite(this._game, 0, 0, "scoreScreen");
            this._finishScreen.animations.add("play");
            this._finishScreen.play("play", 24, true);

            this.addChild(this._finishScreen);

            this._playButton = new Phaser.Button(this._game, 0, 0, "playScoreScreen", this.playAgain, this, 1, 0, 1, 0);
           
            this._quit = new Phaser.Button(this._game, 300, 10, "playScoreScreen", this.quitClick, this, 3, 2, 3, 2);
            this._playButton.x = 257;
            this._playButton.y = 358;
            this._quit.x = 427;
            this._quit.y = 358;
            this.addChild(this._playButton);
            this.addChild(this._quit);

        }

        playAgain() {
            
            this._gameControl.playAgain();
            this.removeAll();
            this.destroy(true);
            console.log("play again");

        }

        quitClick() {
            console.log("quit");

        }

    }

} 