﻿module Rocket {

    export class Round extends Phaser.Group {

        private _gameControler: GameControler = GameControler.getInstance();
        private _animRound: Phaser.Sprite;
        private _game: Phaser.Game;
        private successArr: Array<Phaser.Sound>;
    
        constructor(game:Phaser.Game) {

            super(game);
            
/*
            this._contenerLetter = new Phaser.Group(game);
            this._contenerLetter.y = 30;
            this._contenerLetter.x = 10;
            this.addChild(this._contenerLetter);
            */
            this._game = game;
            this.setAnim();

            this.successArr = new Array();

            for (var i: number = 1; i < 5; i++) {

                
                this.successArr.push(this.game.sound.add("success" + i, 1, false));

            }
           // this.animations.play("firstRound", 24, false);
        }

        public setAnim() {
           
            if (this._animRound != null) {
                this.removeAll(true);
                this._animRound.kill();
                this._animRound = null;
                //
                //this._animRound.destroy(true);
                //this._animRound = null;
            }

            switch (this._gameControler.currentLevel) {
                case 0:
                    console.log("ustawiam Animacje round");
                    this._animRound = new Phaser.Sprite(this._game, 90, 90, "level1Pie");
                    this._animRound.animations.add("round1");
                  
                
                    break;

                case 1:
                    this._animRound = new Phaser.Sprite(this._game, 0, 0, "level2Pie");
                    this._animRound.animations.add("round1", this.doFrame(10, 27));
                    this._animRound.animations.add("round2", this.doFrame(28, 48));
                    break;

                case 2:
                    this._animRound = new Phaser.Sprite(this._game, 0, 0, "level3Pie");
                    this._animRound.animations.add("round1", this.doFrame(10, 27));
                    this._animRound.animations.add("round2", this.doFrame(28, 46));
                    this._animRound.animations.add("round3", this.doFrame(47, 65));
                    break;

                case 3:
                    this._animRound = new Phaser.Sprite(this._game, 0, 0, "level4Pie");
                    this._animRound.animations.add("round1", this.doFrame(10, 27));
                    this._animRound.animations.add("round2", this.doFrame(28, 46));
                    this._animRound.animations.add("round3", this.doFrame(47, 65));
                    this._animRound.animations.add("round4", this.doFrame(66, 85));
                    break;

                case 4:
                    this._animRound = new Phaser.Sprite(this._game, 0, 0, "level5Pie");
                    this._animRound.animations.add("round1", this.doFrame(10, 26));
                    this._animRound.animations.add("round2", this.doFrame(28, 46));
                    this._animRound.animations.add("round3", this.doFrame(47, 66));
                    this._animRound.animations.add("round4", this.doFrame(67, 86));
                    this._animRound.animations.add("round5", this.doFrame(87, 106));
                    break;

                case 5:
                    this._animRound = new Phaser.Sprite(this._game, 90, 90, "level6Pie");
                    this._animRound.animations.add("round1", this.doFrame(10, 27));
                    this._animRound.animations.add("round2", this.doFrame(28, 46));
                    this._animRound.animations.add("round3", this.doFrame(47, 66));
                    this._animRound.animations.add("round4", this.doFrame(67, 86));
                    this._animRound.animations.add("round5", this.doFrame(87, 105));
                    this._animRound.animations.add("round6", this.doFrame(106, 125));
                    break;

                default:
                    console.log("poza levelem");
            } 
            
          
            this.addChild(this._animRound);

        }


        public nextRound() {

            var ktory: number = this._gameControler.currentRoundPerLevel;
            if (this._gameControler.currentRoundPerLevel == this.successArr.length) {

                ktory = this.successArr.length - 1;

            }
            this._animRound.animations.play("round" + this._gameControler.currentRoundPerLevel, 24, false);

            this.successArr[ktory].onStop.add(this.playAgain,this);
            this.successArr[ktory].play();




        }

        playAgain() {

           // this._gameControler._showLetter.playAgainAfterSound();

            this._gameControler._gameLevel.showFinishLevel();
        }

        private doFrame(start: number, end: number):Array<number> {

            var frameArr: Array<number> = new Array();
            for (var i: number = start; i < end + 1; i++) {
                frameArr.push(i);


            }

            return frameArr;


        }





    }

}