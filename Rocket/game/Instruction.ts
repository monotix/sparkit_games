﻿module Rocket {

    export class Instruction extends Phaser.State{

        private overButtonA: Phaser.Sound;
        create() {

            var instruction = this.add.image(0, 0, "firstInstruction", 0);

            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
          
           // var instructionAudio = this.game.add.audio("chuja", 1, false);

          //  instructionAudio.play();
            var _buttons = this.add.group();

            var buttonsBg: Phaser.Sprite = this.add.sprite(0, 0, 'buttonsBg');
            _buttons.addChild(buttonsBg);

            var gameButton = this.add.button(14, 36, 'buttons', null, this, 0, 0, 1);


            _buttons.addChild(gameButton);

            var playButton = this.add.button(61, gameButton.y, 'buttons', this.onPlayClick, this, 3, 2);
            playButton.onInputDown.add(this.overPlay2, this);


            _buttons.addChild(playButton);

            _buttons.x = 0;
            _buttons.y = this.game.stage.height - _buttons.height;

        }

        private overPlay2() {

            this.overButtonA.play();

        }



        onPlayClick() {

            this.game.state.start("Level", true, false);

        }



    }


}