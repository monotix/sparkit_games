﻿module Rocket {

    export class Level extends Phaser.State {

        private _player: Player;
        private cursors;
        private starfield;
        private obstacles;
        private obstaclesGr;
        private _tween: Phaser.Tween = null;
        private LeftOrRight: number = 200;
        private changePosYAfterCollsion: boolean = true;
        private _countDown: Phaser.Sprite;
        private _sequence:Sequence;
        public _points:Round;
        private _gameControl: GameControler = GameControler.getInstance();
        private _showLetter: ShowLetter;
        private _letterArray: Array<PopLetter>;
        private _letterGr: Phaser.Group;
        private _formation: Formation;
        private _finishScreen: FinishScreen;
        private _finishScreenLevel: FinishLevel;
        private _jumperGr: Phaser.Group;
        private _scoreGr: Phaser.Group;
        private _checkScreen: Phaser.Sprite;
        public SPEED: number = -100;
        private _isDown: boolean = false;
        private _isLeft: boolean = false;
        private helpGr;
        private _helpBtn;
        private _instructionShow;
        preload() {

          
          


        }


       
     


        create() {

            this._gameControl.setGame(this);
            this._gameControl.setLevels();
            this._formation = Formation.getInstance();
            this.starfield = this.game.add.tileSprite(0, 0, 800, 600, 'gameBg');
            console.log("level start");

         
            this._scoreGr = this.add.group();

            this._sequence = new Sequence(this.game, 0, 5, "sequence");
            this._gameControl.setSequence(this._sequence);
            this._scoreGr.add(this._sequence);

            this._points = new Round(this.game);
            this._points.scale.setTo(0.5, 0.5);
            console.log("this._points.width", this._points.width);

            this._points.x = this.game.width - 135;
            this._points.y = -50;
            this._gameControl.setRound(this._points);
            this._scoreGr.add(this._points);

            this._countDown = this.add.sprite(0, 0, "countdown", 0);
            //this._countDown.anchor.setTo(0.5, 0.5);
            this._countDown.animations.add("countdownAnim");

            this._countDown.x = this.game.width / 2 - this._countDown.width / 2;
            this._countDown.y = this.game.height / 2 - this._countDown.height / 2;
            
            this._scoreGr.add(this._countDown);

            // this.game.physics.arcade.skipQuadTree = true;

            this._player = new Player(this.game, 0, 200, "player");
            this._player.anchor.setTo(0.5, 0.5);
            this._player.scale.setTo(0.6, 0.6);
            this._player.position.setTo(200, 200);
            this._player.body.enableBody = true;

            this._player.body.setSize(30, 40, -15, 0);
            

            this.game.physics.enable(this._player, Phaser.Physics.ARCADE);
            this.game.world.setBounds(0, 0, 100, 100);

            this._jumperGr = this.add.group();
          
         
            this.stage.addChild(this._player);
            this.obstaclesGr = this.add.group();
          
            this.stage.addChild(this.obstaclesGr);
           // this.obstaclesGr.addChild(this._jumperGr);

            this.obstaclesGr.enableBody = true;
            this._jumperGr.enableBody = true;

           

            var checkScreenGroup = this.add.group();
            checkScreenGroup.enableBody = true;
            this._checkScreen = this.add.sprite(0, 0, "", 0, checkScreenGroup);
            var black: Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
            black.beginFill(0x000000, 0);
            black.drawRect(0, 0, 100, 100);
            black.endFill();

            
           
            this._checkScreen.addChild(black);
            this._checkScreen.y =300;


            //checkScreenGroup.addChild(this._checkScreen);
           
           // checkScreenGroup.physicsBodyType = Phaser.Physics.ARCADE;
            
            this._checkScreen.checkWorldBounds = true;
            this._checkScreen.events.onOutOfBounds.add(this.checkScreen, this);
            this._checkScreen.body.velocity.y = this.SPEED;

           
/*
            for (var i: number = 0; i < 2; i++) {

                var enemy = this.obstaclesGr.create(300, i * 500, "jumper", i % 3);
               
                enemy.checkWorldBounds = true;
                enemy.events.onOutOfBounds.add(this.goodbye, this);
                enemy.body.velocity.y = -50;
                // (300, i * 300, "obstacles", 0, this.obstaclesGr);
                // enemy.body.collideWorldBounds = true;
                //    enemy.body.bounce.set(1);
                //enemy.enableBody = true;
                //enemy.physicsBodyType = Phaser.Physics.Arcade;
                


            }
            
            this._letterGr = this.add.group();
            this._letterArray = new Array();
            for (var i: number = 0; i < 10; i++) {

                var letter: PopLetter = new PopLetter(this.game,200, i * 200, "pop");
                letter.addLetter(Letters.validLetters[Math.floor(Math.random() * Letters.validLetters.length)]);
                //letter.collisonPlayAnim();
                letter.physicsEnabled = true;
                this.game.physics.enable(letter, Phaser.Physics.ARCADE);
                this._letterGr.addChild(letter);
                this._letterArray.push(letter);
                // (300, i * 300, "obstacles", 0, this.obstaclesGr);
                // enemy.body.collideWorldBounds = true;
                //    enemy.body.bounce.set(1);
                //enemy.enableBody = true;
                //enemy.physicsBodyType = Phaser.Physics.Arcade;
             
            }

            */
            
          //  this.obstaclesGr.create(400, 500, "jumper"); //addChild(new Jumper(this.game, 400, 500, "jumper"));


           // this.stage.addChild(this._letterGr);

           // this._letterGr.y = 600;

           
           


            this.setObject();
          

            if (!mobile && 1 == 0) {


                this.game.input.onUp.add(function () {
                    console.log('up');
                    this._isDown = false;

                }, this);
                this.game.input.onDown.add(function () {
                    this._isDown = true;
                    console.log('down', this._isDown);
                }, this);

            }


        
            this.helpGr = this.add.group();
            this.stage.addChild(this.helpGr);

            this._helpBtn = this.add.button(0, 0, "helpBtn", this.helpBtnClick, this, 1, 0, 0, 0, this.helpGr);
            this.helpGr.x = 800 - this.helpGr.width;
            this.helpGr.y = 600 - this.helpGr.height;

            this._helpBtn.buttonMode = true;
            this._helpBtn.inputEnabled = true;
            this._helpBtn.input.priorityID = 110;
            //this.showLetterMenu(new Array("a"));
           
            this.game.input.onDown.add(function () { if (this.game.paused) { this.game.paused = false; } if (this._instructionShow) { this.stage.removeChild(this._instructionShow); this._instructionShow.kill(); this._instructionShow = null; } }, this);
           

        }

        private helpBtnClick() {

            console.log("pokaz instrukcje");
            this.game.paused = !this.game.paused;

            if (this._instructionShow == null) this._instructionShow = this.add.image(0, 0, "firstInstruction", 0);
            

            // this.add.image(0, 0, "firstInstruction", 0);
            this._instructionShow.width = 800;//this.stage.width;
            this._instructionShow.height = 600;
            this.stage.addChild(this._instructionShow);

        }


        tweenStop() {

            if (this._tween != null) {

                this._tween.stop();

            }

        }

        stopMoveObject(stop_: boolean) {

            this.obstaclesGr.forEach(function (item) {
                // item.body.velocity = 0;
              
                if (item != undefined && stop_) {
                    item.body.velocity.y = 0;
                }
                else if (item != undefined && !stop_) {

                    item.body.velocity.y = this.SPEED;
                }
            }, this)

        }

        setPlayerDefaultPosition() {

            this._player.position.setTo(this._player.x, 200);

        }

        checkScreen(object) {
           

                this.stage.addChild(this._scoreGr);

                object.y = this.game.height;
                if (!this._gameControl.gameOver) {
                    var objectArray: Array<any> = this._formation.level1();

                   

                    for (var i: number = 0; i < objectArray.length; i++) {

                        var obst = objectArray[i];
                        obst.y += 600;
                        this.obstaclesGr.add(obst);
                        this.obstaclesGr.addChild(obst);
                        var rect: Phaser.Rectangle = obst.getBounce();
                       
                        obst.anchor.setTo(0.5, 0.5);
                        obst.body.setSize(rect.width, rect.height,rect.x, rect.y);
                        obst.enableBody = true;
                        obst.checkWorldBounds = true;
                    
                       
                      //  obst.events.onOutOfBounds.add(this.goodbye, this);
                        obst.body.velocity.y = this.SPEED;


                    }

                }

        }

        goodbye(object) {
            if (object.world.y < -600) {

               // object.kill();

            }
        
       //  console.log("goodbay", object.name, object.world.y );
           // object.kill();

        }
         
        setObject() {
            console.log("setobject");
            this.SPEED = -100;
            this.obstaclesGr.y = 600;
            this.cleanObstacle();
            if (this._finishScreen != null) {
                this.stage.removeChild(this._finishScreen);
                this._finishScreen = null;
            }

            this._gameControl.gameOver = false;
            this._checkScreen.body.velocity.y = this.SPEED;
         //   this._gameControl.collisionPlayer = false;

            if (this._showLetter) this._showLetter.destroy(true);

            this._player.position.setTo(200, 200);

           // this.obstaclesGr.y = this.game.height;

            //this._letterGr.y = 600;
            this._countDown.animations.frame = 1;
            this._countDown.alpha = 1;

            this._countDown.animations.getAnimation("countdownAnim").onComplete.add(function () {

            }, this);

            this.game.time.events.add(Phaser.Timer.SECOND * 1, function () {



                this._countDown.animations.play("countdownAnim", 24, false, false);



            }, this)


            this.game.time.events.add(Phaser.Timer.SECOND * 4, function () {

                this._countDown.alpha = 0;
                this._gameControl.collisionPlayer = false;
                this.game.input.onDown.add(this.movePlayer, this);

                this._countDown.animations.play("countdownAnim", 24, false, false);
                this._showLetter = new ShowLetter(this.game, 0, 0, "collectLetters");
                this._showLetter.addBgToDisplayLetter(this.add.sprite(0, 0, "collectLettersbg"));
                this._showLetter.anchor.setTo(0.5, 0.5);
                //this._showLetter.scale.setTo(0.5, 0.5)
                this._showLetter.x = 400;
                this._showLetter.y = 300;

                this._gameControl.setShowLetter(this._showLetter);

                this._scoreGr.addChild(this._showLetter);
                console.log("liczba przeszkod", this.obstaclesGr.length);
            }, this);
          
        }


        playAgain() {
            console.log("gram od poczatku");
            this.setObject();


        }
       

        movePlayer(pointer) {

            if (!this._gameControl.collisionPlayer) {

               
                if (this._player.x > pointer.x) {

                    this._player.startLeft();
                }
                else {
                    this._player.startRight();
                }
                




                var posx: number = pointer.x;
                if (posx < 100) posx = 100;
                else if (posx > 700) posx = 700;
            
                this.stopTween();
              
                if (!this._gameControl.collisionPlayer) {
                    var duration = (this.game.physics.arcade.distanceToPointer(this._player, pointer) / 200) * 1000;


                    this._tween = this.game.add.tween(this._player).to({ x:posx }, duration, Phaser.Easing.Linear.None).start();
                    this._tween.onComplete.add(this.endMove, this);

                }

            }


        }

        endMove() {
            this._player.playerPlay();

        }

        stopTween() {

            if (this._tween != null && this._tween.isRunning) {
                this.game.tweens.remove(this._tween);
               
            }
            


        }

        public stopAndREmoveLetters() {

            this.SPEED = 0;
            this.obstaclesGr.y = 600;
            this.cleanObstacle();
        }

        public showFinishLevel() {
            console.log("FInish screen: showFinishLevel");
            this._finishScreenLevel = new FinishLevel(this.game);
            this._finishScreenLevel.x = 0;// this.game.width / 2 - this._finishScreenLevel.width / 2;
            this._finishScreenLevel.y = 0;//this.game.height / 2 - this._finishScreenLevel.height / 2;
            this.stage.addChild(this._finishScreenLevel);
            this.SPEED = 0;
            this.obstaclesGr.y = 600;
            this.cleanObstacle();
        }

        public showFinish() {
            console.log("FInish screen: showFinish");
            this._finishScreen = new FinishScreen(this.game);
            this._finishScreen.x = 0;// this.game.width / 2 - this._finishScreen.width / 2;
            this._finishScreen.y = 0;//this.game.height / 2 - this._finishScreen.height/ 2;
            this.stage.addChild(this._finishScreen);
            this.SPEED = 0;
            this.cleanObstacle();
        }

        cleanObstacle() {
           
            if (this.obstaclesGr.length > 0) {
                this.obstaclesGr.removeAll();

            }
        /*    this.obstaclesGr.forEach(function (item) {
               // item.body.velocity = 0;
              
                if (item != undefined) {
                    this.obstaclesGr.removeChild(item);
                }
            }, this);*/
        }


        drawEnemy() {


            if (this.obstaclesGr.y + this.obstaclesGr.height < 600) {
                console.log(" poycja obstacle", this.obstaclesGr.y , this.obstaclesGr.height);
                var obstacle = this._formation.getObstacle();
                obstacle.y = this.obstaclesGr.height + this.obstaclesGr.y + obstacle.y ;
                this.obstaclesGr.addChild(obstacle);
               
               // if (obstacle instanceof ObstacleColumn) console.log("jestem przeszkoda", obstacle instanceof ObstacleColumn); 

            }





        }

      
        update() {
            if (this._gameControl.gameOver) {

               // this.game.paused = true;

            }
            else {
                this.starfield.tilePosition.y += 2;
                // this.starfield.fixedToCamera = true;
                // this._player.body.velocity.setTo(0, 0);
                // xml
                // kml
                // kxm
                // this._player.body.velocity.y = 100;
                // this.obstaclesGr.body.velocity.y = 10;
          
          



          
            


           

           
           
          
                if (!this._gameControl.collisionPlayer) {

                  

                    if (this.changePosYAfterCollsion) {

                        this.obstaclesGr.y += 400;

                        this.changePosYAfterCollsion = false;
                    }
                    else if (this._isDown) {

                        //
                        console.log("is downa");
                        var posx = this.game.input.x;
                        if (posx > 700) posx = 700;
                        else if (posx < 100) posx = 100;
                        this.game.physics.arcade.accelerateToXY(this._player, posx, 200, 1000, 180, 0);
                      

                   
                        
                        //this._player.rotation = 0;
                        this.game.physics.arcade.overlap(this.obstaclesGr, this._player, this.collision, null, this);
                    }
                    else if (mobile) {
                        //this.drawEnemy();
                        //   this.obstaclesGr.y -= 1;
                        this._player.body.velocity.setTo(0, 0);
                        this._player.rotation = 0;
                        this._player.playerPlay();
                        this.game.physics.arcade.accelerateToXY(this._player, posx, 200, 0, 0, 0);
                        this.game.physics.arcade.overlap(this.obstaclesGr, this._player, this.collision, null, this);

                    }
                    else if (!mobile){
                        this.game.physics.arcade.overlap(this.obstaclesGr, this._player, this.collision, null, this);

                    }
                   // this._player.y = 200;



                }
                else {

                    // this.obstaclesGr.y = 0;

                }

            }

        }

        public checkCollsionWithLetter() {
           
            if (!this._gameControl.hideLetterOnGame) {
             
                this.obstaclesGr.forEach(function (item) {
                    
                    if (item != undefined && item.name == "popletter") {
                        
                        item.visible = true;
                  //      this.obstaclesGr.remove(item);
                   //     item.kill();

                    }
                }, this);


            }
            else {
               
                this.obstaclesGr.forEach(function (item) {
                 
                    if (item != undefined &&  item.name == "popletter") {
                       
                        item.visible = false;
                        this.obstaclesGr.remove(item);
                       item.kill();
                    }
                }, this);
            }
            

        }

        collisionLetter(letter:PopLetter) {
            if (!this._gameControl.collisionPlayer) {

               //this.changePosYAfterCollsion = true;


               // this._gameControl.collisionPlayer = true;
               

                //zxczvbnm, \\ //\\ //--\\
               

              //  console.log(letter);
              //  letter.body.moves = false;
                
                letter.collisonPlayAnim();
                this._sequence.addLetter(letter.letter);
                this._gameControl.checkCorrectLetters(letter.letter);
                this.removeLetterFromCollision(letter);
                
                letter = null;
               
                //this.stopTween();
            }





        }

        removeLetterFromCollision(letter: PopLetter) {


           /* for (var i: number = 0; i < this._letterArray.length; i++) {

                if (this._letterArray[i] == letter) {

                    this._letterArray.splice(i, 1);
                    --i;
                    if (i < 0) i = 0;

                }



            }*/

        }

        public setPlayerToTop(topPlayer:boolean) {

          

            if (topPlayer) {
                
                this.stage.addChild(this._player);
            }
            else{
                this.stage.addChild(this.obstaclesGr);

            }
            this.stage.addChild(this._scoreGr);
        }

        collision(object,object2) {
            if (!this._gameControl.collisionPlayer) {

                console.log(object2.name,object2.key);
                if (object2.name == "popletter") {
                  //  this.obstaclesGr.removeChild(object2);
                  //  object2.body.collideWorldBounds = true;
                  //  this.setPlayerToTop(true);
                  
                    object2.body.setSize(1200, -1200, -1, -1);
                    object2.body.checkCollision.none = true;
                    object2.collisonPlayAnim();
                    this._sequence.addLetter(object2.letter);
                    this._gameControl.checkCorrectLetters(object2.letter);
                 

                }
                else if (object2.name == "jumper") {
                    
                    this.setPlayerToTop(true);
                   // object2.body.setSize(-2000, -2000, 0, 0);
                    //object2.body.checkCollision.none = true;
                    //object2.body.moves = false;
                    this.tweenStop();
                    this._gameControl.isJumping = true;
                    this._player.jump();
              

                }
                else if (object2.name == "electro") {

                    this.stopMoveObject(true);
                    this.setPlayerToTop(true);
                    this.changePosYAfterCollsion = true;


                    this._gameControl.collisionPlayer = true;

                 //   object2.body.setSize(-2000, -2000, 0, 0);
                 //   object2.body.checkCollision.none = true;
                    //object2.body.moves = false;
                    this.tweenStop();

                    this._player.electro();

                }
                else if (object2.name == "hole") {

                    this.stopMoveObject(true);
                    this.setPlayerToTop(true);
                    this.changePosYAfterCollsion = true;
                    console.log("hole", object2.width, object2.y, object2.height);
                    this._player.position.setTo(object2.x, object2.world.y+20);
                    this._gameControl.collisionPlayer = true;

                    //   object2.body.setSize(-2000, -2000, 0, 0);
                    //   object2.body.checkCollision.none = true;
                    //object2.body.moves = false;
                    this.tweenStop();

                    this._player.hole();

                }
                else if(!this._gameControl.isJumping) {
                    this.stopMoveObject(true);
                  
                    this.changePosYAfterCollsion = true;


                    this._gameControl.collisionPlayer = true;



                    this.stopTween();


                    this._player.collisionPlayer();


                  
                }
                //this.stopTween();
            }
            
            this.obstaclesGr.forEach(function (item) {
                // item.body.velocity = 0;
              
                if (item != undefined && item.world.y < -600) item.destroy(true);
            }, this);

            

        }

        collisionJumper() {
         
            if (!this._gameControl.collisionPlayer) {



             
                
               
               
                //this.stopTween();
            }


        }

        
        render() {

             /*this.game.debug.quadTree(this.game.physics.arcade.quadTree);
       
             this.obstaclesGr.forEach(function (item) {
                 // item.body.velocity = 0;
              
                 this.game.debug.body(item);
             }, this)

        */
        }

        public hideScreenAfterFinishPlaye() {

        }
        
        
    }


}