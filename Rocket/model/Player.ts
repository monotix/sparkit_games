﻿module Rocket {

    export class Player extends Phaser.Sprite {

       
        private _gameControl: GameControler = GameControler.getInstance();

        private electroAudio: Phaser.Sound;
        private electroAudio2: Phaser.Sound;
        private crashAudio: Phaser.Sound;
        private crashAudio2: Phaser.Sound;
        private jumpAudio: Phaser.Sound;
        private holeAudio: Phaser.Sound;
            constructor(game:Phaser.Game,x:number, y:number, key:string){

                super(game,x, y, key);

                this.anchor.setTo(0.5, 0.5);
                game.physics.enable(this, Phaser.Physics.ARCADE);

                
                this.animations.add("MoveForward", this.countFrames(1,22), 24, true);
                this.animations.add("collision", this.countFrames(141, 179), 24, false);

                this.animations.add("collisionBackward", this.countFrames(180, 229), 24, false);
                this.animations.add("startLeft", this.countFrames(23, 27), 24, false);
                this.animations.add("moveLeft", this.countFrames(28, 44), 24, false);
                this.animations.add("startRight", this.countFrames(46, 50), 24, false);
                this.animations.add("moveRight", this.countFrames(51, 69), 24, false);
                this.animations.add("jump", this.countFrames(71, 140), 24, false);
                this.animations.add("crashDown", this.countFrames(230, 260), 24, false);
                this.animations.add("electrocout", this.countFrames(265, 299), 24, false);

                this.animations.play("MoveForward", 24, true, false);

                this.electroAudio = this.game.sound.add("electro", 1, false, true);
                this.electroAudio2 = this.game.sound.add("electro1", 1, false, true);
              
                this.crashAudio = this.game.sound.add("crash", 1, false, true);
                this.crashAudio2 = this.game.sound.add("crash2", 1, false, true);
                this.jumpAudio = this.game.sound.add("jumpAudio", 1, false, true);
                this.holeAudio = this.game.sound.add("holeAudio", 1, false, true);
               
        }


            jump() {

                
                this.animations.play("jump", 24, false, false); 
                this._gameControl.blokadeClick(true);
                this.jumpAudio.play();
                this.animations.getAnimation("jump").onComplete.add(function () { this.playerPlay(false); this._gameControl.blokadeClick(false); this._gameControl._gameLevel.setPlayerToTop(false); this._gameControl.isJumping = false;}, this);


            }
            hole() {
                this.animations.play("crashDown", 24, false, false);
                // this._gameControl.blokadeClick(true);
                this.holeAudio.play();
              
                this.animations.getAnimation("crashDown").onComplete.add(function () { this._gameControl._gameLevel.stopMoveObject(false); this.playerPlay(true); }, this);



            }

            electro() {


                this.animations.play("electrocout", 24, false, false);
               // this._gameControl.blokadeClick(true);
              
                this.animations.getAnimation("electrocout").onComplete.add(function () { this._gameControl._gameLevel.stopMoveObject(false); this.playerPlay(true);  }, this);
                this.electroAudio.play();
                this.electroAudio2.play();


            }

            playerPlay(youAreDead: boolean = false) {
                if (youAreDead) {
                    
                    
                    this.animations.play("MoveForward", 24, true, false);
                    this._gameControl.collisionPlayer = false;
                   // this.game.time.events.add(Phaser.Timer.SECOND * 1, this.playAgain, this)
                  
                }
                else {
                   // this._gameControl.collisionPlayer = false;
                    //this.y -= 200;
                    this.animations.play("MoveForward", 24, true, false);
                }
                this.y = 200;
            }

            playAgain() {

                
                this._gameControl.collisionPlayer = false;


            }

            collisionPlayer() {
                this.crashAudio.play();
                this.crashAudio2.play();
                this.animations.play("collisionBackward", 24, false, false);
                this.animations.getAnimation("collisionBackward").onComplete.add(function () { this._gameControl._gameLevel.stopMoveObject(false); this.playerPlay(true); }, this);

            }

            collisionForwardPlayer() {

                this.animations.play("collision", 24, false, false);
                this.animations.getAnimation("collision").onComplete.add(function () { this.playerPlay(true); }, this);

            }

            startLeft() {
               
                this.animations.getAnimation("startLeft").onComplete.add(this.moveLeft, this);
                this.animations.play("startLeft", 24, false, false);

            }


            moveLeft() {

               
                this.animations.play("moveLeft", 24, false, false).onComplete.add(this.stopLeft, this);
             
            }

            stopLeft() {
              
                this.animations.stop("moveLeft");
                this.animations.frame = 44;

            }

            startRight() {
                this.animations.getAnimation("startRight").onComplete.add(this.moveRight, this);
                this.animations.play("startRight", 24, false, false);

            }


            moveRight() {

               
                this.animations.play("moveRight", 24, false, false).onComplete.add(function () { this.frame = 76;});

            }


            countFrames(start:number , num: number):Array<number>{
                var countArr: Array<number> = new Array();
                for (var i: number = start; i < num+1; i++) {

                    countArr.push(i);


                }

                return countArr;


            }
        


    }


}