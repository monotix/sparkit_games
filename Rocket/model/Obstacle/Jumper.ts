﻿module Rocket {

    export class Jumper extends Phaser.Sprite {

        constructor(game: Phaser.Game, x: number, y: number, key: string) {

            super(game, x, y, key);

            this.frame = Math.floor(Math.random() * this.animations.frameTotal);

        }


    }


}  