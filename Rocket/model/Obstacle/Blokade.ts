﻿module Rocket {

    export class Blokade extends Phaser.Sprite {

        constructor(game: Phaser.Game, x: number, y: number, key: string) {

            super(game, x, y, key);
            this.name = key;
            this.frame = Math.floor(Math.random() * this.animations.frameTotal);

        }


    }


} 