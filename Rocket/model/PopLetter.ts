﻿module Rocket {
    export class PopLetter extends Phaser.Sprite{

        private _letterShow: Phaser.Text;
        public letter;
        private posx: number;
        private posy: number;
        private bounceWidth: number;
        private bounceHeight: number;

        constructor(game: Phaser.Game, x: number, y: number, key: string, posx: number = 0, posy:number = 0,bounceWidth:number=0,bounceHeight:number=0){

            super(game, x, y, key);
            this.name = "popletter";
            this.animations.add("pop");
            this.posx = (posx == 0) ? 0 : posx;
            this.posy = (posy == 0) ? 0 : posy;
            this.bounceWidth = (bounceWidth == 0) ? this.width : bounceWidth;
            this.bounceHeight = (bounceHeight == 0) ? this.height : bounceHeight;

        }

        addLetter(str: string) {

            var style = { font: "40px Arial", fill: "#000000", align: "center" };
            if (this._letterShow) this._letterShow.destroy();
            this._letterShow = new Phaser.Text(this.game, 0, 0, str, style);
            this._letterShow.anchor.set(0.5, 0.5);
         //   this._letterShow.x = 40;
          //  this._letterShow.y = 25;
            this.addChild(this._letterShow);
            this.letter = str;

        }

        public getBounce(): Phaser.Rectangle {

            var rect: Phaser.Rectangle = new Phaser.Rectangle(this.posx,this.posy,this.bounceWidth,this.bounceHeight);
            
            return rect;

        }

       public collisonPlayAnim() {

           this.removeChild(this._letterShow);
           this.animations.getAnimation("pop").onComplete.add(function () { this.kill(); }, this);
         
           this.play("pop", 24, false, true);
           
            

        }


    }


} 