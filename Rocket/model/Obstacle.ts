﻿module Rocket {

    export class Obstacle extends Phaser.Sprite {

        private posx: number;
        private posy: number;
        private bounceWidth: number;
        private bounceHeight: number;
        constructor(game: Phaser.Game, x: number, y: number, key: string, posx: number = 0, posy: number = 0, bounceWidth: number = 0, bounceHeight: number = 0) {

            super(game, x, y, key);
            this.name = key;
            if (this.frame != null) {
                this.frame = Math.floor(Math.random() * this.animations.frameTotal);
            }
            this.posx = (posx == 0) ? 0 : posx;
            this.posy = (posy == 0) ? 0 : posy;
            this.bounceWidth = (bounceWidth == 0) ? this.width : bounceWidth;
            this.bounceHeight = (bounceHeight == 0) ? this.height : bounceHeight;

        }

        public getBounce(): Phaser.Rectangle {

            var rect: Phaser.Rectangle = new Phaser.Rectangle(this.posx, this.posy, this.bounceWidth, this.bounceHeight);

            return rect;

        }


    }

} 