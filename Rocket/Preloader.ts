﻿module Rocket {

    export class Preloader extends Phaser.State {


        preload() {
            
            var bg = this.add.image(0, 0, "bg", 0);

            var blackProgress = this.add.image(0, 0, "progressBarBlack", 0);
            var blueProgress = this.add.sprite(0, 0, "progressBarBlue", 0);


            blackProgress.position.setTo(this.stage.width / 2 - blackProgress.width / 2, this.stage.height - blackProgress.height - 10);

            blueProgress.position = blackProgress.position;
            this.load.setPreloadSprite(blueProgress);




            this.load.image("mainMenuBg", "assets/mainMenu/SplashScreen2.png");
            this.load.atlasJSONArray('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');
            this.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
            this.load.audio("MainMenuSn", "assets/mainMenu/BBThemeMusic.mp3");
            this.load.audio("overButtonA", "assets/audio/overButton.mp3");
            this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");
            

            this.load.audio("chuja", "assets/audio/instruction/1.mp3");
            this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");

            this.load.audio("electro", "assets/audio/player/electro.mp3");
            this.load.audio("electro1", "assets/audio/player/electro2.mp3");

            this.load.audio("crash", "assets/audio/player/crash.mp3");
            this.load.audio("crash2", "assets/audio/player/crash2.mp3");
            this.load.audio("jump", "assets/audio/player/jump.mp3");

            this.load.audio("holeAudio", "assets/audio/player/hole.mp3");

            this.load.audio("letterd", "assets/audio/letter/d.mp3");
            this.load.audio("letterp", "assets/audio/letter/p.mp3");
            this.load.audio("letterb", "assets/audio/letter/b.mp3");
            var licz = 1;
            for (var i: number = 13; i < 17; i++) {

                this.load.audio("fail" + licz, "assets/audio/rocket_racer_" + i + ".mp3");

                licz++;
            }
            licz = 1;
            for (var i: number = 10; i < 13; i++) {

                this.load.audio("success" + licz, "assets/audio/rocket_racer_" + i + ".mp3");

                licz++;
            }


            this.load.image("gameBg", "assets/rocket/rocketGameBg.png");
            this.load.atlasJSONArray("obstacles", "assets/rocket/obstacles/obstacles.png", "assets/rocket/obstacles/obstacles.json");
            this.load.atlasJSONArray("hole", "assets/rocket/obstacles/hole.png", "assets/rocket/obstacles/hole.json");
            this.load.atlasJSONArray("electro", "assets/rocket/obstacles/electro.png", "assets/rocket/obstacles/electro.json");
            this.load.atlasJSONArray("blokade", "assets/rocket/obstacles/blokade.png", "assets/rocket/obstacles/blokade.json");
            this.load.atlasJSONArray("wall", "assets/rocket/obstacles/wall.png", "assets/rocket/obstacles/wall.json");
            this.load.atlasJSONArray("jumper", "assets/rocket/obstacles/jumper.png", "assets/rocket/obstacles/jumper.json");

            this.load.atlasJSONArray("player", "assets/rocket/player/player2.png", "assets/rocket/player/player2.json");

            this.load.atlasJSONArray("countdown", "assets/rocket/countdown.png", "assets/rocket/countdown.json");

            this.load.image("sequence", "assets/rocket/sequence.png");
            this.load.atlasJSONArray("level1Pie", "assets/rocket/level1Pie1.png", "assets/rocket/level1Pie1.json");
            this.load.atlasJSONArray("level2Pie", "assets/rocket/level2Pie2.png", "assets/rocket/level2Pie2.json");
            this.load.atlasJSONArray("level3Pie", "assets/rocket/level3Pie3.png", "assets/rocket/level3Pie3.json");
            this.load.atlasJSONArray("level4Pie", "assets/rocket/level4Pie4.png", "assets/rocket/level4Pie4.json");
            this.load.atlasJSONArray("level5Pie", "assets/rocket/level5Pie5.png", "assets/rocket/level5Pie5.json");
            this.load.atlasJSONArray("level6Pie", "assets/rocket/level6Pie6.png", "assets/rocket/level6Pie6.json");

            this.load.atlasJSONArray("pop", "assets/rocket/pop.png", "assets/rocket/pop.json");

            var mobile: boolean;
            mobile = false
            if (!this.game.device.desktop) {
                mobile = true;
            }

            if (mobile) {
                this.load.image("firstInstruction", "assets/03A_rocket_Racer.png");
                this.load.audio("instructionSn", "assets/audio/rocket_racer_02.mp3");

            }
            else {
                this.load.image("firstInstruction", "assets/02A_rocket_Racer.png");
                this.load.audio("instructionSn", "assets/audio/rocket_racer_01.mp3");
            }

            this.load.image("popup", "assets/popup/popup.png");
            this.load.image("logoGame", "assets/popup/logo.png");
            this.load.atlasJSONArray('games', 'assets/popup/games.png', 'assets/popup/games.json');
            this.load.atlasJSONArray('playGame', 'assets/popup/play.png', 'assets/popup/play.json');
            this.load.atlasJSONArray('helpBtn', 'assets/popup/help.png', 'assets/popup/help.json');

            this.load.image("firstInstruction", "assets/InstructionScreen.png");

            this.load.image("collectLetters", "assets/rocket/collectLetters.png");
            this.load.image("collectLettersbg", "assets/rocket/TrickItemDisplay.png");

            this.load.atlasJSONArray("scoreScreen", "assets/scoreScreen/scoreScreen.png", "assets/scoreScreen/scoreScreen.json");
            this.load.atlasJSONArray("playScoreScreen", "assets/scoreScreen/scoreScreenButtons.png", "assets/scoreScreen/scoreScreenButtons.json");


        }


        create() {

            this.game.state.start("MainMenu", true, false);

        }

    }
} 

var mobile = false;