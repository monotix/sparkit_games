﻿module Rocket {
    export class ShowLetter extends Phaser.Sprite{


        private _bgToShowLetter: Phaser.Sprite;
        private _letters: Letters = Letters.getInstance();
        private _letterShow: Phaser.Text;
        private _gameControler: GameControler = GameControler.getInstance();
        private _letterD: Phaser.Sound;
        private _letterB: Phaser.Sound;
        private _letterP: Phaser.Sound;
        private failArr: Array<Phaser.Sound>;
        constructor(game:Phaser.Game,x:number,y:number,key:string) {

            super(game, x, y, key);
            this._letterB = this.game.sound.add("letterb", 1, false);
            this._letterD = this.game.sound.add("letterd", 1, false);
            this._letterP = this.game.sound.add("letterp", 1, false);
            this.failArr = new Array();
            for (var i: number = 1; i < 5; i++) {

             //   this.load.audio("fail" + i, "assets/audio/instruction/fail" + i + ".mp3");
                this.failArr.push(this.game.sound.add("fail" + i, 1, false));

            }
        }


        public addBgToDisplayLetter(bg:Phaser.Sprite) {

            this._bgToShowLetter = bg;
           // this._bgToShowLetter.anchor.setTo(0.5, 0.5);
            // this._bgToShowLetter.x = this.width / 2 - this._bgToShowLetter.width / 2;
            this._bgToShowLetter.x = - this._bgToShowLetter.width / 2;
            this._bgToShowLetter.y = - 30;
            this.addChild(this._bgToShowLetter);

            this._letters.getValidLetters(2, 3);
            //console.log(this._letters.randomPhoneme("d",new Array(),new Array()));
            this.showLetter(this._letters.chooseLetters[0]);
            this.whatToPlay(this._letters.chooseLetters[0]);
            this.game.time.events.add(Phaser.Timer.SECOND * 2, function () {


                this._bgToShowLetter.removeChild(this._letterShow);
                this._bgToShowLetter.alpha = 0;



            }, this)

            this.game.time.events.add(Phaser.Timer.SECOND * 2.5, function () {


                this._bgToShowLetter.alpha = 1;
                this.showLetter(this.showLetter(this._letters.chooseLetters[1]));
                this.whatToPlay(this._letters.chooseLetters[1]);


            }, this);

            this.game.time.events.add(Phaser.Timer.SECOND * 5, function () {


                this.alpha = 0;
            


            }, this)

        }

        private count: number = 0;
        private maxCount: number = 2;

        private showLetter(str: string) {

            if (this.count < this.maxCount) {

                var style = { font: "40px Arial", fill: "#000000", align: "center" };
                if (this._letterShow) this._letterShow.destroy();
                this._letterShow = new Phaser.Text(this.game, 0, 0, str, style);

                this._letterShow.x = this._bgToShowLetter.width / 2 - this._letterShow.width / 2;
                this._letterShow.y = this._bgToShowLetter.height / 2 - this._letterShow.height / 2;
                this._bgToShowLetter.alpha = 1;
                this._bgToShowLetter.addChild(this._letterShow);

            }

            this.count++;

        }

        whatToPlay(letterAudio: string) {

            if (letterAudio == "d") {
                this._letterD.play();
            }
            else if (letterAudio == "b") {
                this._letterB.play();
            }
            else if (letterAudio == "p") {
                this._letterP.play();
            }


        }


        playAgainAfterSound() {

            this.game.time.events.add(Phaser.Timer.SECOND * 1, function () {

                this.count = 0;
                this.alpha = 1;
                this.showLetter(this._letters.chooseLetters[0]);
                this.whatToPlay(this._letters.chooseLetters[0]);


            }, this)

         
       

            this.game.time.events.add(Phaser.Timer.SECOND * 2, function () {


                this._bgToShowLetter.removeChild(this._letterShow);
                this._bgToShowLetter.alpha = 0;



            }, this)

            this.game.time.events.add(Phaser.Timer.SECOND * 4, function () {


                this._bgToShowLetter.alpha = 1;
                this.showLetter(this.showLetter(this._letters.chooseLetters[1]));
                this.whatToPlay(this._letters.chooseLetters[1]);


            }, this);

            this.game.time.events.add(Phaser.Timer.SECOND * 5, function () {


                this.alpha = 0;
                this._gameControler.hideLetterOnGame = false;

                // this._gameControler.hideLetterOnGame = false;


            }, this)

        }
        public playAgain() {
           
           // this._gameControler.hideLetterOnGame = true;
            this._gameControler.hideLetterOnGame = true;
           
            this.failArr[this._gameControler._badLetters].onStop.add(this.playAgainAfterSound, this);
            this.failArr[this._gameControler._badLetters].play();
           


        }

    }

} 