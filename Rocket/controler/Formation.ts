﻿module Rocket {

    export class Formation {

        public static _instance: Formation = null;

        private _obstacleLevel1: Array<any>;
        private _gameControl: GameControler = GameControler.getInstance();

        //obstacle, jumper, hole, electro, blokade, wall,
        constructor() {


            Formation._instance = this;
            this.level1();

        
        }

        public static getInstance(): Formation {

            if (Formation._instance === null) {

                Formation._instance = new Formation();
              

            }

            return Formation._instance

        }


        level1():Array<any> {

            // jeden obiekt 350,700
            // jumper 320 1400
           
            var arr: Array<any>;
            if (this._gameControl.currentLevel == 0) arr = this._gameControl.getLevel2();
            else if (this._gameControl.currentLevel == 1) arr = this._gameControl.getLevel2();
            else if (this._gameControl.currentLevel == 2) arr = this._gameControl.getLevel3();
            else if (this._gameControl.currentLevel == 3) arr = this._gameControl.getLevel4();
            else if (this._gameControl.currentLevel == 4) arr = this._gameControl.getLevel5();
            else if (this._gameControl.currentLevel == 5) arr = this._gameControl.getLevel5();
            var newArr: Array<any> = new Array();
            var game = this._gameControl._gameLevel.game;
            
            for (var i: number=0; i < arr.length; i++) {
                var x: number = arr[i].x;
                var y: number = arr[i].y;
                var name: string = arr[i].name;
                
                if (name == "popletter") {

                    var letter:PopLetter = new PopLetter(game, x, y, "pop",0,0,60,55);
                  
                    
                    letter.addLetter(Letters.validLetters[Math.floor(Math.random() * Letters.validLetters.length)]);
                    newArr.push(letter);

                }

                else {

                    if (name == "jumper") {
                        newArr.push(new Obstacle(game, x, y, name, 0, -40, 90, 30));

                    }
                    else if (name == "electro") {
                        newArr.push(new Obstacle(game, x, y, name, 0, 50, 150, 25));

                    }
                    else if (name == "hole") {
                        newArr.push(new Obstacle(game, x, y, name, 0, 0, 190, 170));

                    }
                    else if (name == "obstacles") {
                        newArr.push(new Obstacle(game, x, y, name, 0, 0, 80, 100));

                    }
                    else if (name == "blokade") {
                        newArr.push(new Obstacle(game, x, y, name, 0, 0, 0, 25));

                    }
                    else {
                        newArr.push(new Obstacle(game, x, y, name));
                    }

                   // newArr.push(new Obstacle(game, x, y, name));

                }

            



            }

         


            return newArr;


        }


        level4() {

            var posArray: Array<number> = new Array();


        }


        public getObstacle(): any {

           return new ObstacleColumn(this._gameControl._gameLevel.game, 350, 700, "obstacles");
        }





    }



} 