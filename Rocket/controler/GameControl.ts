﻿module Rocket {

    export class GameControler {

        public static _instance: GameControler = null;

        public collisionPlayer: boolean = true;
        public speed: number = 1;
        private _letters: Letters = Letters.getInstance();
        private _cacheLattersArr: Array<string> = new Array();
       // private _needsLetters: Array<string>;
        private _correctLetters: number = 0;
        private _maxCorrectLetters: number = 2;
        public _badLetters: number = 0;
        private _maxBadLetters: number = 3;
        public sequenceLetters: Array<string>;
        public currentLevel: number = 0;
        public maxLevel: number = 6;
        public maxRoundPerLevel: number = 1;
        public currentRoundPerLevel: number = 0;
        private _round: Round;
        public _showLetter: ShowLetter;
        private _sequence: Sequence;
        public _hideLetterOnGame: boolean = false;
        public gameOver: boolean = false;
        public isJumping: boolean = false;
        public _gameLevel: Level;
        construct() {

            if (GameControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            GameControler._instance = this;

        }

         

        public static getInstance(): GameControler {

            if (GameControler._instance  === null) {

                GameControler._instance = new GameControler();


            }


            return GameControler._instance ;



        }


        public setGame(gameLevel:Level) {


            this._gameLevel = gameLevel;
            


        }

        public setSequence(sequence: Sequence) {


            this._sequence = sequence;



        }

        public checkCorrectLetters(str:string) {

            this._cacheLattersArr.push(str);
          
            var correct: boolean = (this._letters.chooseLetters[this._correctLetters] == str) ? true : false;
            console.log("sprawdzam co jest", this._letters.chooseLetters, this._correctLetters);
            console.log("corect", correct, this._correctLetters);
            if (correct) {
                this._correctLetters++;
                this.checkYourScore(true);
               
            }
            else {
                
                this._correctLetters = 0;
                this._badLetters++;
                if (this._badLetters < this._maxBadLetters) {

                    this._showLetter.playAgain();

                }
                this.hideLetterOnGame = true;
                
                this._sequence.removeLetters();
                this.checkYourScore(false);
            }


        }

        private checkYourScore(correctAnswer:boolean) {

            if (this._correctLetters == this._maxCorrectLetters) {


                this.currentRoundPerLevel++;
                this._round.nextRound();
                this.checkRoundPerLevel();

            }
            else if (this._badLetters == this._maxBadLetters) {


                console.log("show Again Letter");
                this.blokadeClick(true);
                this.gameOver = true;
                this._gameLevel.showFinish();

            }
       

          

        }


        public blokadeClick(blokade:boolean) {
            
            if (blokade) {
                this._gameLevel.input.onDown.removeAll();
            }
            else {
                
                
                this._gameLevel.input.onDown.add(this._gameLevel.movePlayer, this._gameLevel);

            }


        }

        private checkRoundPerLevel() {
            console.log("check round", this.currentRoundPerLevel, this.maxRoundPerLevel, this.currentLevel);

            if (this.currentLevel == this.maxLevel) {
                console.log("you win the game");
                this.blokadeClick(true);
                this.gameOver = true;
                this._gameLevel.showFinish();
                return 0;
            }

            if (this.currentRoundPerLevel == this.maxRoundPerLevel) {
                this.hideLetterOnGame = true;
                console.log("You win Level");

                // this._showLetter.playAgain();
                this._sequence.removeLetters();
              


                this._gameLevel.time.events.add(Phaser.Timer.SECOND * 4, function () {

                    this.nextLevel();
                   // this._gameLevel.playAgain();
                    this.hideLetterOnGame = false;

                }, this);
            }
            else {
                this._correctLetters = 0;
                this._sequence.removeLetters();
                this._letters.getValidLetters(2, 3);
               // this._showLetter.playAgainAfterSound();

            }


        }

        private nextLevel() {
            console.log("next Level");
            this.currentLevel++;
            this.maxRoundPerLevel = this.currentLevel + 1;
            this.currentRoundPerLevel = 0;
            this._correctLetters = 0;
            this._badLetters = 0;
            this._round.setAnim();

        }


        public playAgain() {

            this.maxRoundPerLevel = this.currentLevel + 1;
            this.currentRoundPerLevel = 0;
            this._correctLetters = 0;
            this._badLetters = 0;
            this._round.setAnim();
            this._gameLevel.playAgain();


        }


        



        public setRound(round: Round) {

            this._round = round;


        }

        public setShowLetter(showLetters: ShowLetter) {

            this._showLetter = showLetters;


        }

        public getLevel1():Array<any> {

            var arr: Array<any> = new Array(
                new Array(
                    { x: 521, y: 92, name: "electro" },
                    { x: 217, y: 585, name: "hole" },
                    { x: 226, y: 266, name: "popletter" },
                    { x: 591, y: 266, name: "popletter" }

                    ),
                new Array(
                    { x: 192, y: 196, name: "jumper" },
                    { x: 623, y: 172, name: "obstacles" },
                    { x: 197, y: 344, name: "popletter" },
                    { x: 400, y: 538, name: "popletter" }

                    )

                );

            return arr[Math.floor(Math.random()*arr.length)];


        }

        getLevel2(): Array<any> {

            var arr: Array<any> = new Array(
                new Array(
                    { x: 113, y: 82, name: "popletter" },
                    { x: 604, y: 267, name: "popletter" },
                    { x: 345, y: 530, name: "popletter" }

                    ),
                new Array(
                  
                    { x: 530, y: 76, name: "popletter" },
                    { x: 378, y: 543, name: "popletter" },
                    { x: 218, y: 297, name: "popletter" }

                    ),
                new Array(

                    { x: 109, y: 76, name: "popletter" },
                    { x: 693, y: 287, name: "popletter" },
                    { x: 693, y: 503, name: "popletter" }

                    ),

                //
                new Array(
                    { x: 249, y: 92, name: "popletter" },
                    { x: 458, y: 264, name: "popletter" },
                    { x: 264, y: 535, name: "popletter" }

                    ),
                new Array(

                    { x: 146, y: 126, name: "popletter" },
                    { x: 370, y: 350, name: "popletter" },
                    { x: 666, y: 526, name: "popletter" }

                    ),
                new Array(

                    { x: 360, y: 184, name: "popletter" },
                    { x: 118, y: 285, name: "popletter" },
                    { x: 115, y: 560, name: "popletter" }

                    ),
                 //
                new Array(
                    { x: 150, y: 281, name: "popletter" },
                    { x: 377, y: 281, name: "popletter" },
                    { x: 608, y: 281, name: "popletter" }

                    ),
                new Array(

                    { x: 454, y: 184, name: "popletter" },
                    { x: 115, y: 347, name: "popletter" },
                    { x: 668, y: 547, name: "popletter" }

                    ),
                new Array(

                    { x: 370, y: 38, name: "popletter" },
                    { x: 603, y: 234, name: "popletter" },
                    { x: 219, y: 548, name: "popletter" }

                    ),
                //
                new Array(
                    { x: 203, y: 204, name: "popletter" },
                    { x: 549, y: 181, name: "popletter" },
                    { x: 554, y: 538, name: "popletter" }

                    ),
                new Array(

                    { x: 244, y: 95, name: "popletter" },
                    { x: 643, y: 95, name: "popletter" },
                    { x: 244, y: 368, name: "popletter" }

                    ),
                new Array(

                    { x: 281, y: 50, name: "popletter" },
                    { x: 325, y: 256, name: "popletter" },
                    { x: 368, y: 467, name: "popletter" }

                    )

                );

            return arr[Math.floor(Math.random() * arr.length)];


        }

        getLevel3(): Array<any> {

            var arr: Array<any> = new Array(
                new Array(
                    { x:378, y: 54, name: "obstacles" },
                    { x: 675, y: 273, name: "popletter" },
                    { x: 164, y: 530, name: "popletter" }

                    ),
                new Array(
                    { x: 109, y: 54, name: "obstacles" },
                    { x: 531, y: 122, name: "popletter" },
                    { x: 356, y: 524, name: "popletter" }

                    ),
                new Array(
                    { x: 391, y: 405, name: "jumper" },
                    { x: 253, y: 59, name: "popletter" },
                    { x: 249, y: 235, name: "popletter" }

                    )
                , new Array(
                    { x: 606, y: 48, name: "obstacles" },
                    { x: 209, y: 448, name: "popletter" },
                    { x: 536, y: 448, name: "popletter" }

                    )
                , new Array(
                    { x: 664, y: 448, name: "obstacles" },
                    { x: 472, y: 169, name: "popletter" },
                    { x: 122, y: 153, name: "popletter" }

                    )
                //
                , new Array(
                    { x: 664, y: 448, name: "obstacles" },
                    { x: 472, y: 169, name: "popletter" },
                    { x: 122, y: 153, name: "popletter" }

                    )
                , new Array(
                    { x: 550, y: 404, name: "obstacles" },
                    { x: 208, y: 39, name: "popletter" },
                    { x: 342, y: 538, name: "popletter" }

                    )
                , new Array(
                    { x: 522, y: 434, name: "obstacles" },
                    { x: 522, y: 50, name: "popletter" },
                    { x: 120, y: 50, name: "popletter" }

                    )
            //
                , new Array(
                    { x: 276, y: 439, name: "jumper" },
                    { x: 110, y: 272, name: "popletter" },
                    { x: 485, y: 630, name: "popletter" }

                    )
                , new Array(
                    { x: 267, y: 419, name: "jumper" },
                    { x: 266, y: 311, name: "popletter" },
                    { x: 589, y: 42, name: "popletter" }

                    )
                

                );

            return arr[Math.floor(Math.random() * arr.length)];



        }

        getLevel4(): Array<any> {

            var arr: Array<any> = new Array(
                new Array(
                    { x: 521, y: 81, name: "electro" },
                    { x: 226, y: 265, name: "popletter" },
                    { x: 521, y: 265, name: "popletter" },
                    { x: 219, y: 611, name: "hole" }

                    ),
                new Array(
                    { x: 194, y: 197, name: "jumper" },
                    { x: 198, y: 342, name: "popletter" },
                    { x: 398, y: 619, name: "popletter" },
                    { x: 620, y: 170, name: "obstacles" }

                    ),
                new Array(
                    { x: 228, y: 114, name: "electro" },
                    { x: 228, y: 146, name: "popletter" },
                    { x: 359, y: 666, name: "popletter" },
                    { x: 521, y: 515, name: "obstacles" }

                    )
                , new Array(
                    { x: 559, y: 144, name: "hole" },
                    { x: 135, y: 323, name: "popletter" },
                    { x: 507, y: 349, name: "popletter" },
                    { x: 555, y: 566, name: "hole" }

                    )
                , new Array(
                    { x: 604, y: 441, name: "hole" },
                    { x: 226, y: 80, name: "popletter" },
                    { x: 592, y: 203, name: "popletter" },
                    { x: 149, y: 454, name: "obstacles" }

                    )
            //
                , new Array(
                    { x: 206, y: 116, name: "jumper" },
                    { x: 603, y: 264, name: "popletter" },
                    { x: 379, y: 544, name: "popletter" },
                    { x: 603, y: 116, name: "jumper" }

                    )
                , new Array(
                    { x: 399, y: 56, name: "obstacles" },
                    { x: 394, y: 260, name: "popletter" },
                    { x: 394, y: 456, name: "popletter" },
                    { x: 399, y: 591, name: "obstacles" }

                    )
                , new Array(
                    { x: 590, y: 180, name: "hole" },
                    { x: 179, y: 360, name: "popletter" },
                    { x: 654, y: 655, name: "popletter" },
                    { x: 180, y: 538, name: "electro" }

                    )
            //
                , new Array(
                    { x: 180, y: 231, name: "electro" },
                    { x: 153, y: 529, name: "popletter" },
                    { x: 660, y: 529, name: "popletter" },
                    { x: 521, y: 231, name: "electro" }

                    )
                , new Array(
                    { x: 418, y: 60, name: "obstacles" },
                    { x: 322, y: 289, name: "popletter" },
                    { x: 480, y: 289, name: "popletter" },
                    { x: 400, y: 484, name: "electro" }

                    )


                );


            return arr[Math.floor(Math.random() * arr.length)];

        }

        getLevel5(): Array<any> {

            var arr: Array<any> = new Array(
                new Array(
                    { x: 413, y: 210, name: "jumper" },
                    { x: 161, y: 66, name: "popletter" },
                    { x: 658, y: 730, name: "popletter" },
                    { x: 412, y: 312, name: "blokade" },
                    { x: 421, y: 688, name: "obstacles" }


                    ),
                new Array(
                    { x: 560, y: 185, name: "obstacles" },
                    { x: 220, y: 158, name: "popletter" },
                    { x: 377, y: 699, name: "popletter" },
                    { x: 203, y: 677, name: "obstacles" }

                    ),
                new Array(
                    { x: 228, y: 114, name: "electro" },
                    { x: 228, y: 146, name: "popletter" },
                    { x: 359, y: 666, name: "popletter" },
                    { x: 521, y: 515, name: "obstacles" }

                    )
                , new Array(
                    { x: 411, y: 230, name: "jumper" },
                    { x: 143, y: 160, name: "popletter" },
                    { x: 617, y: 160, name: "popletter" },
                    { x: 411, y: 340, name: "blokade" }

                    )
                , new Array(
                    { x: 411, y: 457, name: "jumper" },
                    { x: 588, y: 128, name: "electro" },
                    { x: 562, y: 411, name: "popletter" },
                    { x: 128, y: 151, name: "popletter" },
                    { x: 411, y: 558, name: "blokade" }

                    )
            //
                , new Array(
                    { x: 420, y: 610, name: "jumper" },
                    { x: 590, y: 77, name: "electro" },
                    { x: 562, y: 331, name: "popletter" },
                    { x: 128, y: 331, name: "popletter" }
                 

                    )
                , new Array(
                    { x: 399, y: 56, name: "obstacles" },
                    { x: 394, y: 260, name: "popletter" },
                    { x: 394, y: 456, name: "popletter" },
                    { x: 399, y: 591, name: "obstacles" }

                    )
                , new Array(
                    { x: 168, y: 187, name: "jumper" },
                    { x: 554, y: 141, name: "popletter" },
                    { x: 317, y: 684, name: "popletter" },
                    { x: 492, y: 703, name: "electro" },
                    { x: 415, y: 308, name: "blokade" }

                    )
            //
                , new Array(
                    { x: 594, y: 83, name: "electro" },
                    { x: 559, y: 313, name: "popletter" },
                    { x: 114, y: 645, name: "popletter" },
                    { x: 594, y: 534, name: "electro" }

                    )
                , new Array(
                    { x: 585, y: 183, name: "jumper" },
                    { x: 277, y: 56, name: "popletter" },
                    { x: 537, y: 683, name: "popletter" },
                    { x: 220, y: 709, name: "hole" }

                    )


                );

            return arr[Math.floor(Math.random() * arr.length)];

        }

      

        
        public set hideLetterOnGame(show: boolean) {

            this._hideLetterOnGame = show;
            this._gameLevel.checkCollsionWithLetter();
        }

        public get hideLetterOnGame(): boolean {

            return this._hideLetterOnGame;

        }


    }




}