﻿module Rocket {


   

    export class Letters {

        public phonemes: Array<string> = new Array(
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
            "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"
            );

        public static validLetters: Array<string> = new Array("b", "p", "d");

        public chooseLetters: Array<string>;
        public static _instance: Letters = null;

        construct() {

            if (Letters._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            Letters._instance = this;

        }

        public static getInstance(): Letters {

            if (Letters._instance === null) {

                Letters._instance = new Letters();


            }


            return Letters._instance;



        }

         public  getValidLetters(numPerWord: Number, letterVariation: Number): Array<string> {
            var validLettersRandom: Array<string> = new Array();
            var letterSequence: Array<string> = new Array();

            for (var i: number = 0; i < letterVariation; i++)
                validLettersRandom.push(Letters.validLetters[Math.floor(Math.random() * Letters.validLetters.length)]);

            for (var i: number = 0; i < numPerWord; i++)
                letterSequence.push(Letters.validLetters[Math.floor(Math.random() * Letters.validLetters.length)]);

            this.chooseLetters = letterSequence;
        return letterSequence;
        }

        public   randomPhoneme(activePhoneme: string, distractors: Array<string>, exclusionList: Array<string>): String {
        var rand: Number = Math.random();
        var letter: string;

        if (rand < 0.5 && distractors.length > 1) {
            do {
                letter = distractors[this.getRandom(distractors)];
            }
            while (letter == activePhoneme)
            return letter;
        }

        do {
            letter = this.phonemes[this.getRandom(this.phonemes)];
        }
        while (this.inList(letter, exclusionList));
        return letter;
         }

        inList(rand: String, exclusionList: Array<string>): boolean {
            for (var i: number = 0; i < exclusionList.length; i++) {
                if (exclusionList[i] == rand)
                    return true;
            }
            return false;
        }

        private   getRandom(arr: Array<any>): number {
            return Math.floor(Math.random() * arr.length);
        }

    }



} 