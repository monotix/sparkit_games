var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Rocket;
(function (Rocket) {
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            _super.apply(this, arguments);
        }
        Boot.prototype.preload = function () {
            this.load.image("bg", "assets/SplashScreen2.png");
            this.load.image("progressBarBlack", "assets/preloader/ProgressBar.png");
            this.load.image("progressBarBlue", "assets/preloader/ProgressBarBlue.png");
        };
        Boot.prototype.create = function () {
            this.game.state.start("Preloader", true, false);
        };
        return Boot;
    })(Phaser.State);
    Rocket.Boot = Boot;
})(Rocket || (Rocket = {}));
var Rocket;
(function (Rocket) {
    var Formation = (function () {
        //obstacle, jumper, hole, electro, blokade, wall,
        function Formation() {
            this._gameControl = Rocket.GameControler.getInstance();
            Formation._instance = this;
            this.level1();
        }
        Formation.getInstance = function () {
            if (Formation._instance === null) {
                Formation._instance = new Formation();
            }
            return Formation._instance;
        };
        Formation.prototype.level1 = function () {
            // jeden obiekt 350,700
            // jumper 320 1400
            var arr;
            if (this._gameControl.currentLevel == 0)
                arr = this._gameControl.getLevel2();
            else if (this._gameControl.currentLevel == 1)
                arr = this._gameControl.getLevel2();
            else if (this._gameControl.currentLevel == 2)
                arr = this._gameControl.getLevel3();
            else if (this._gameControl.currentLevel == 3)
                arr = this._gameControl.getLevel4();
            else if (this._gameControl.currentLevel == 4)
                arr = this._gameControl.getLevel5();
            else if (this._gameControl.currentLevel == 5)
                arr = this._gameControl.getLevel5();
            var newArr = new Array();
            var game = this._gameControl._gameLevel.game;
            for (var i = 0; i < arr.length; i++) {
                var x = arr[i].x;
                var y = arr[i].y;
                var name = arr[i].name;
                if (name == "popletter") {
                    var letter = new Rocket.PopLetter(game, x, y, "pop", 0, 0, 60, 55);
                    letter.addLetter(Rocket.Letters.validLetters[Math.floor(Math.random() * Rocket.Letters.validLetters.length)]);
                    newArr.push(letter);
                }
                else {
                    if (name == "jumper") {
                        newArr.push(new Rocket.Obstacle(game, x, y, name, 0, -40, 90, 30));
                    }
                    else if (name == "electro") {
                        newArr.push(new Rocket.Obstacle(game, x, y, name, 0, 50, 150, 25));
                    }
                    else if (name == "hole") {
                        newArr.push(new Rocket.Obstacle(game, x, y, name, 0, 0, 190, 170));
                    }
                    else if (name == "obstacles") {
                        newArr.push(new Rocket.Obstacle(game, x, y, name, 0, 0, 80, 100));
                    }
                    else if (name == "blokade") {
                        newArr.push(new Rocket.Obstacle(game, x, y, name, 0, 0, 0, 25));
                    }
                    else {
                        newArr.push(new Rocket.Obstacle(game, x, y, name));
                    }
                }
            }
            return newArr;
        };
        Formation.prototype.level4 = function () {
            var posArray = new Array();
        };
        Formation.prototype.getObstacle = function () {
            return new Rocket.ObstacleColumn(this._gameControl._gameLevel.game, 350, 700, "obstacles");
        };
        Formation._instance = null;
        return Formation;
    })();
    Rocket.Formation = Formation;
})(Rocket || (Rocket = {}));
var Rocket;
(function (Rocket) {
    var GameControler = (function () {
        function GameControler() {
            this.collisionPlayer = true;
            this.speed = 1;
            this._letters = Rocket.Letters.getInstance();
            this._cacheLattersArr = new Array();
            // private _needsLetters: Array<string>;
            this._correctLetters = 0;
            this._maxCorrectLetters = 2;
            this._badLetters = 0;
            this._maxBadLetters = 3;
            this.currentLevel = 0;
            this.maxLevel = 6;
            this.maxRoundPerLevel = 1;
            this.currentRoundPerLevel = 0;
            this._hideLetterOnGame = false;
            this.gameOver = false;
            this.isJumping = false;
        }
        GameControler.prototype.construct = function () {
            if (GameControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            GameControler._instance = this;
        };
        GameControler.getInstance = function () {
            if (GameControler._instance === null) {
                GameControler._instance = new GameControler();
            }
            return GameControler._instance;
        };
        GameControler.prototype.setLevels = function () {
            this.currentLevel = userLevel - 1;
            this.maxRoundPerLevel = this.currentLevel + 1;
        };
        GameControler.prototype.setGame = function (gameLevel) {
            this._gameLevel = gameLevel;
        };
        GameControler.prototype.setSequence = function (sequence) {
            this._sequence = sequence;
        };
        GameControler.prototype.checkCorrectLetters = function (str) {
            this._cacheLattersArr.push(str);
            var correct = (this._letters.chooseLetters[this._correctLetters] == str) ? true : false;
            console.log("sprawdzam co jest", this._letters.chooseLetters, this._correctLetters);
            console.log("corect", correct, this._correctLetters);
            if (correct) {
                this._correctLetters++;
                this.checkYourScore(true);
            }
            else {
                this._correctLetters = 0;
                this._badLetters++;
                if (this._badLetters < this._maxBadLetters) {
                    this._showLetter.playAgain();
                }
                this.hideLetterOnGame = true;
                this._sequence.removeLetters();
                this.checkYourScore(false);
            }
        };
        GameControler.prototype.checkYourScore = function (correctAnswer) {
            console.log("check your score", this._correctLetters == this._maxCorrectLetters);
            if (this._correctLetters == this._maxCorrectLetters) {
                this.currentRoundPerLevel++;
                this._round.nextRound();
                this.checkRoundPerLevel();
            }
            else if (this._badLetters == this._maxBadLetters) {
                console.log("show Again Letter");
                this.blokadeClick(true);
                this.gameOver = true;
                this._gameLevel.showFinish();
            }
        };
        GameControler.prototype.blokadeClick = function (blokade) {
            if (blokade) {
                this._gameLevel.input.onDown.removeAll();
            }
            else {
                this._gameLevel.input.onDown.add(this._gameLevel.movePlayer, this._gameLevel);
            }
        };
        GameControler.prototype.checkRoundPerLevel = function () {
            console.log("check round", this.currentRoundPerLevel, this.maxRoundPerLevel, this.currentLevel);
            if (this.currentLevel == this.maxLevel) {
                console.log("you win the game");
                this.blokadeClick(true);
                this.gameOver = true;
                this._gameLevel.showFinish();
                return 0;
            }
            console.log("this.currentRoundPerLevel == this.maxRoundPerLevel", this.currentRoundPerLevel, this.maxRoundPerLevel);
            if (this.currentRoundPerLevel == this.maxRoundPerLevel) {
                this.hideLetterOnGame = true;
                console.log("You win Level");
                // this._showLetter.playAgain();
                this._sequence.removeLetters();
                //  console.log(userID, gameID, this.currentLevel + 1);
                APIsetLevel(userID, gameID, this.currentLevel + 2);
                this._gameLevel.showFinishLevel();
            }
            else {
                this._correctLetters = 0;
                this._sequence.removeLetters();
                this._letters.getValidLetters(2, 3);
                this._gameLevel.stopAndREmoveLetters();
                this._showLetter.playAgainAfterSound();
            }
        }; /*502 915 239
przemek genca*/
        GameControler.prototype.nextLevel = function () {
            console.log("next Level");
            this.currentLevel++;
            this.maxRoundPerLevel = this.currentLevel + 1;
            this.currentRoundPerLevel = 0;
            this._correctLetters = 0;
            this._badLetters = 0;
            this._round.setAnim();
        };
        GameControler.prototype.playAgain = function () {
            this.maxRoundPerLevel = this.currentLevel + 1;
            this.currentRoundPerLevel = 0;
            this._correctLetters = 0;
            this._badLetters = 0;
            this._round.setAnim();
            this._gameLevel.playAgain();
        };
        GameControler.prototype.setRound = function (round) {
            this._round = round;
        };
        GameControler.prototype.setShowLetter = function (showLetters) {
            this._showLetter = showLetters;
        };
        GameControler.prototype.getLevel1 = function () {
            var arr = new Array(new Array({ x: 521, y: 92, name: "electro" }, { x: 217, y: 585, name: "hole" }, { x: 226, y: 266, name: "popletter" }, { x: 591, y: 266, name: "popletter" }), new Array({ x: 192, y: 196, name: "jumper" }, { x: 623, y: 172, name: "obstacles" }, { x: 197, y: 344, name: "popletter" }, { x: 400, y: 538, name: "popletter" }));
            return arr[Math.floor(Math.random() * arr.length)];
        };
        GameControler.prototype.getLevel2 = function () {
            var arr = new Array(new Array({ x: 113, y: 82, name: "popletter" }, { x: 604, y: 267, name: "popletter" }, { x: 345, y: 530, name: "popletter" }), new Array({ x: 530, y: 76, name: "popletter" }, { x: 378, y: 543, name: "popletter" }, { x: 218, y: 297, name: "popletter" }), new Array({ x: 109, y: 76, name: "popletter" }, { x: 693, y: 287, name: "popletter" }, { x: 693, y: 503, name: "popletter" }), new Array({ x: 249, y: 92, name: "popletter" }, { x: 458, y: 264, name: "popletter" }, { x: 264, y: 535, name: "popletter" }), new Array({ x: 146, y: 126, name: "popletter" }, { x: 370, y: 350, name: "popletter" }, { x: 666, y: 526, name: "popletter" }), new Array({ x: 360, y: 184, name: "popletter" }, { x: 118, y: 285, name: "popletter" }, { x: 115, y: 560, name: "popletter" }), new Array({ x: 150, y: 281, name: "popletter" }, { x: 377, y: 281, name: "popletter" }, { x: 608, y: 281, name: "popletter" }), new Array({ x: 454, y: 184, name: "popletter" }, { x: 115, y: 347, name: "popletter" }, { x: 668, y: 547, name: "popletter" }), new Array({ x: 370, y: 38, name: "popletter" }, { x: 603, y: 234, name: "popletter" }, { x: 219, y: 548, name: "popletter" }), new Array({ x: 203, y: 204, name: "popletter" }, { x: 549, y: 181, name: "popletter" }, { x: 554, y: 538, name: "popletter" }), new Array({ x: 244, y: 95, name: "popletter" }, { x: 643, y: 95, name: "popletter" }, { x: 244, y: 368, name: "popletter" }), new Array({ x: 281, y: 50, name: "popletter" }, { x: 325, y: 256, name: "popletter" }, { x: 368, y: 467, name: "popletter" }));
            return arr[Math.floor(Math.random() * arr.length)];
        };
        GameControler.prototype.getLevel3 = function () {
            var arr = new Array(new Array({ x: 378, y: 54, name: "obstacles" }, { x: 675, y: 273, name: "popletter" }, { x: 164, y: 530, name: "popletter" }), new Array({ x: 109, y: 54, name: "obstacles" }, { x: 531, y: 122, name: "popletter" }, { x: 356, y: 524, name: "popletter" }), new Array({ x: 391, y: 405, name: "jumper" }, { x: 253, y: 59, name: "popletter" }, { x: 249, y: 235, name: "popletter" }), new Array({ x: 606, y: 48, name: "obstacles" }, { x: 209, y: 448, name: "popletter" }, { x: 536, y: 448, name: "popletter" }), new Array({ x: 664, y: 448, name: "obstacles" }, { x: 472, y: 169, name: "popletter" }, { x: 122, y: 153, name: "popletter" }), new Array({ x: 664, y: 448, name: "obstacles" }, { x: 472, y: 169, name: "popletter" }, { x: 122, y: 153, name: "popletter" }), new Array({ x: 550, y: 404, name: "obstacles" }, { x: 208, y: 39, name: "popletter" }, { x: 342, y: 538, name: "popletter" }), new Array({ x: 522, y: 434, name: "obstacles" }, { x: 522, y: 50, name: "popletter" }, { x: 120, y: 50, name: "popletter" }), new Array({ x: 276, y: 439, name: "jumper" }, { x: 110, y: 272, name: "popletter" }, { x: 485, y: 630, name: "popletter" }), new Array({ x: 267, y: 419, name: "jumper" }, { x: 266, y: 311, name: "popletter" }, { x: 589, y: 42, name: "popletter" }));
            return arr[Math.floor(Math.random() * arr.length)];
        };
        GameControler.prototype.getLevel4 = function () {
            var arr = new Array(new Array({ x: 521, y: 81, name: "electro" }, { x: 226, y: 265, name: "popletter" }, { x: 521, y: 265, name: "popletter" }, { x: 219, y: 611, name: "hole" }), new Array({ x: 194, y: 197, name: "jumper" }, { x: 198, y: 342, name: "popletter" }, { x: 398, y: 619, name: "popletter" }, { x: 620, y: 170, name: "obstacles" }), new Array({ x: 228, y: 114, name: "electro" }, { x: 228, y: 146, name: "popletter" }, { x: 359, y: 666, name: "popletter" }, { x: 521, y: 515, name: "obstacles" }), new Array({ x: 559, y: 144, name: "hole" }, { x: 135, y: 323, name: "popletter" }, { x: 507, y: 349, name: "popletter" }, { x: 555, y: 566, name: "hole" }), new Array({ x: 604, y: 441, name: "hole" }, { x: 226, y: 80, name: "popletter" }, { x: 592, y: 203, name: "popletter" }, { x: 149, y: 454, name: "obstacles" }), new Array({ x: 206, y: 116, name: "jumper" }, { x: 603, y: 264, name: "popletter" }, { x: 379, y: 544, name: "popletter" }, { x: 603, y: 116, name: "jumper" }), new Array({ x: 399, y: 56, name: "obstacles" }, { x: 394, y: 260, name: "popletter" }, { x: 394, y: 456, name: "popletter" }, { x: 399, y: 591, name: "obstacles" }), new Array({ x: 590, y: 180, name: "hole" }, { x: 179, y: 360, name: "popletter" }, { x: 654, y: 655, name: "popletter" }, { x: 180, y: 538, name: "electro" }), new Array({ x: 180, y: 231, name: "electro" }, { x: 153, y: 529, name: "popletter" }, { x: 660, y: 529, name: "popletter" }, { x: 521, y: 231, name: "electro" }), new Array({ x: 418, y: 60, name: "obstacles" }, { x: 322, y: 289, name: "popletter" }, { x: 480, y: 289, name: "popletter" }, { x: 400, y: 484, name: "electro" }));
            return arr[Math.floor(Math.random() * arr.length)];
        };
        GameControler.prototype.getLevel5 = function () {
            var arr = new Array(new Array({ x: 413, y: 210, name: "jumper" }, { x: 161, y: 66, name: "popletter" }, { x: 658, y: 730, name: "popletter" }, { x: 412, y: 312, name: "blokade" }, { x: 421, y: 688, name: "obstacles" }), new Array({ x: 560, y: 185, name: "obstacles" }, { x: 220, y: 158, name: "popletter" }, { x: 377, y: 699, name: "popletter" }, { x: 203, y: 677, name: "obstacles" }), new Array({ x: 228, y: 114, name: "electro" }, { x: 228, y: 146, name: "popletter" }, { x: 359, y: 666, name: "popletter" }, { x: 521, y: 515, name: "obstacles" }), new Array({ x: 411, y: 230, name: "jumper" }, { x: 143, y: 160, name: "popletter" }, { x: 617, y: 160, name: "popletter" }, { x: 411, y: 340, name: "blokade" }), new Array({ x: 411, y: 457, name: "jumper" }, { x: 588, y: 128, name: "electro" }, { x: 562, y: 411, name: "popletter" }, { x: 128, y: 151, name: "popletter" }, { x: 411, y: 558, name: "blokade" }), new Array({ x: 420, y: 610, name: "jumper" }, { x: 590, y: 77, name: "electro" }, { x: 562, y: 331, name: "popletter" }, { x: 128, y: 331, name: "popletter" }), new Array({ x: 399, y: 56, name: "obstacles" }, { x: 394, y: 260, name: "popletter" }, { x: 394, y: 456, name: "popletter" }, { x: 399, y: 591, name: "obstacles" }), new Array({ x: 168, y: 187, name: "jumper" }, { x: 554, y: 141, name: "popletter" }, { x: 317, y: 684, name: "popletter" }, { x: 492, y: 703, name: "electro" }, { x: 415, y: 308, name: "blokade" }), new Array({ x: 594, y: 83, name: "electro" }, { x: 559, y: 313, name: "popletter" }, { x: 114, y: 645, name: "popletter" }, { x: 594, y: 534, name: "electro" }), new Array({ x: 585, y: 183, name: "jumper" }, { x: 277, y: 56, name: "popletter" }, { x: 537, y: 683, name: "popletter" }, { x: 220, y: 709, name: "hole" }));
            return arr[Math.floor(Math.random() * arr.length)];
        };
        Object.defineProperty(GameControler.prototype, "hideLetterOnGame", {
            get: function () {
                return this._hideLetterOnGame;
            },
            set: function (show) {
                this._hideLetterOnGame = show;
                this._gameLevel.checkCollsionWithLetter();
            },
            enumerable: true,
            configurable: true
        });
        GameControler.prototype.setSound = function (sound_) {
            this.mainSound = sound_;
        };
        GameControler._instance = null;
        return GameControler;
    })();
    Rocket.GameControler = GameControler;
})(Rocket || (Rocket = {}));
var Rocket;
(function (Rocket) {
    var Letters = (function () {
        function Letters() {
            this.phonemes = new Array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
        }
        Letters.prototype.construct = function () {
            if (Letters._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            Letters._instance = this;
        };
        Letters.getInstance = function () {
            if (Letters._instance === null) {
                Letters._instance = new Letters();
            }
            return Letters._instance;
        };
        Letters.prototype.getValidLetters = function (numPerWord, letterVariation) {
            var validLettersRandom = new Array();
            var letterSequence = new Array();
            for (var i = 0; i < letterVariation; i++)
                validLettersRandom.push(Letters.validLetters[Math.floor(Math.random() * Letters.validLetters.length)]);
            for (var i = 0; i < numPerWord; i++)
                letterSequence.push(Letters.validLetters[Math.floor(Math.random() * Letters.validLetters.length)]);
            this.chooseLetters = letterSequence;
            return letterSequence;
        };
        Letters.prototype.randomPhoneme = function (activePhoneme, distractors, exclusionList) {
            var rand = Math.random();
            var letter;
            if (rand < 0.5 && distractors.length > 1) {
                do {
                    letter = distractors[this.getRandom(distractors)];
                } while (letter == activePhoneme);
                return letter;
            }
            do {
                letter = this.phonemes[this.getRandom(this.phonemes)];
            } while (this.inList(letter, exclusionList));
            return letter;
        };
        Letters.prototype.inList = function (rand, exclusionList) {
            for (var i = 0; i < exclusionList.length; i++) {
                if (exclusionList[i] == rand)
                    return true;
            }
            return false;
        };
        Letters.prototype.getRandom = function (arr) {
            return Math.floor(Math.random() * arr.length);
        };
        Letters.validLetters = new Array("b", "p", "d");
        Letters._instance = null;
        return Letters;
    })();
    Rocket.Letters = Letters;
})(Rocket || (Rocket = {}));
var Rocket;
(function (Rocket) {
    var ShowLetter = (function (_super) {
        __extends(ShowLetter, _super);
        function ShowLetter(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this._letters = Rocket.Letters.getInstance();
            this._gameControler = Rocket.GameControler.getInstance();
            this.count = 0;
            this.maxCount = 2;
            this._letterB = this.game.sound.add("letterb", 1, false);
            this._letterD = this.game.sound.add("letterd", 1, false);
            this._letterP = this.game.sound.add("letterp", 1, false);
            this.failArr = new Array();
            for (var i = 1; i < 5; i++) {
                //   this.load.audio("fail" + i, "assets/audio/instruction/fail" + i + ".mp3");
                this.failArr.push(this.game.sound.add("fail" + i, 1, false));
            }
        }
        ShowLetter.prototype.addBgToDisplayLetter = function (bg) {
            this._bgToShowLetter = bg;
            // this._bgToShowLetter.anchor.setTo(0.5, 0.5);
            // this._bgToShowLetter.x = this.width / 2 - this._bgToShowLetter.width / 2;
            this._bgToShowLetter.x = -this._bgToShowLetter.width / 2;
            this._bgToShowLetter.y = -30;
            this.addChild(this._bgToShowLetter);
            this._letters.getValidLetters(2, 3);
            //console.log(this._letters.randomPhoneme("d",new Array(),new Array()));
            this.showLetter(this._letters.chooseLetters[0]);
            this.whatToPlay(this._letters.chooseLetters[0]);
            this.game.time.events.add(Phaser.Timer.SECOND * 2, function () {
                this._bgToShowLetter.removeChild(this._letterShow);
                this._bgToShowLetter.alpha = 0;
            }, this);
            this.game.time.events.add(Phaser.Timer.SECOND * 2.5, function () {
                this._bgToShowLetter.alpha = 1;
                this.showLetter(this.showLetter(this._letters.chooseLetters[1]));
                this.whatToPlay(this._letters.chooseLetters[1]);
            }, this);
            this.game.time.events.add(Phaser.Timer.SECOND * 5, function () {
                this.alpha = 0;
            }, this);
        };
        ShowLetter.prototype.showLetter = function (str) {
            if (this.count < this.maxCount) {
                var style = { font: "40px Arial", fill: "#000000", align: "center" };
                if (this._letterShow)
                    this._letterShow.destroy();
                this._letterShow = new Phaser.Text(this.game, 0, 0, str, style);
                this._letterShow.x = this._bgToShowLetter.width / 2 - this._letterShow.width / 2;
                this._letterShow.y = this._bgToShowLetter.height / 2 - this._letterShow.height / 2;
                this._bgToShowLetter.alpha = 1;
                this._bgToShowLetter.addChild(this._letterShow);
            }
            this.count++;
        };
        ShowLetter.prototype.whatToPlay = function (letterAudio) {
            if (letterAudio == "d") {
                this._letterD.play();
            }
            else if (letterAudio == "b") {
                this._letterB.play();
            }
            else if (letterAudio == "p") {
                this._letterP.play();
            }
        };
        ShowLetter.prototype.playAgainAfterSound = function () {
            this.game.time.events.add(Phaser.Timer.SECOND * 1, function () {
                this.count = 0;
                this.alpha = 1;
                this.showLetter(this._letters.chooseLetters[0]);
                this.whatToPlay(this._letters.chooseLetters[0]);
            }, this);
            this.game.time.events.add(Phaser.Timer.SECOND * 2, function () {
                this._bgToShowLetter.removeChild(this._letterShow);
                this._bgToShowLetter.alpha = 0;
            }, this);
            this.game.time.events.add(Phaser.Timer.SECOND * 4, function () {
                this._bgToShowLetter.alpha = 1;
                this.showLetter(this.showLetter(this._letters.chooseLetters[1]));
                this.whatToPlay(this._letters.chooseLetters[1]);
            }, this);
            this.game.time.events.add(Phaser.Timer.SECOND * 5, function () {
                this.alpha = 0;
                this._gameControler.hideLetterOnGame = false;
                this._gameControler._gameLevel.SPEED = -100;
                // this._gameControler.hideLetterOnGame = false;
            }, this);
        };
        ShowLetter.prototype.playAgain = function () {
            // this._gameControler.hideLetterOnGame = true;
            this._gameControler.hideLetterOnGame = true;
            this.failArr[this._gameControler._badLetters].onStop.add(this.playAgainAfterSound, this);
            this.failArr[this._gameControler._badLetters].play();
        };
        return ShowLetter;
    })(Phaser.Sprite);
    Rocket.ShowLetter = ShowLetter;
})(Rocket || (Rocket = {}));
var Rocket;
(function (Rocket) {
    var Instruction = (function (_super) {
        __extends(Instruction, _super);
        function Instruction() {
            _super.apply(this, arguments);
        }
        Instruction.prototype.create = function () {
            var instruction = this.add.image(0, 0, "firstInstruction", 0);
            instruction.width = this.game.world.width; //this.stage.width;
            instruction.height = this.game.world.height; //this.stage.height;
            this._mainMenuBg = this.game.add.audio('instructionSn', 1, true);
            this._mainMenuBg.play("", 0, 1, false);
            var gameControler = Rocket.GameControler.getInstance();
            gameControler.mainSound.volume = 0.05;
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            // var instructionAudio = this.game.add.audio("chuja", 1, false);
            //  instructionAudio.play();
            var _buttons = this.add.group();
            var gameButton = this.add.button(0, 0, "games", this.onGameClick, this, 1, 0);
            //gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            _buttons.addChild(gameButton);
            var playButton = this.add.button(100, 0, "playGame", this.onPlayClick, this, 1, 0);
            playButton.onInputDown.add(this.overPlay2, this);
            _buttons.addChild(playButton);
            _buttons.x = 0;
            _buttons.y = this.game.stage.height - _buttons.height;
        };
        Instruction.prototype.overPlay2 = function () {
            this.overButtonA.play();
        };
        Instruction.prototype.onGameClick = function () {
        };
        Instruction.prototype.onPlayClick = function () {
            this._mainMenuBg.stop();
            this.game.state.start("Level", true, false);
        };
        return Instruction;
    })(Phaser.State);
    Rocket.Instruction = Instruction;
})(Rocket || (Rocket = {}));
var Rocket;
(function (Rocket) {
    var Level = (function (_super) {
        __extends(Level, _super);
        function Level() {
            _super.apply(this, arguments);
            this._tween = null;
            this.LeftOrRight = 200;
            this.changePosYAfterCollsion = true;
            this._gameControl = Rocket.GameControler.getInstance();
            this.SPEED = -100;
            this._isDown = false;
            this._isLeft = false;
        }
        Level.prototype.preload = function () {
        };
        Level.prototype.create = function () {
            this._gameControl.setGame(this);
            this._gameControl.setLevels();
            this._formation = Rocket.Formation.getInstance();
            this.starfield = this.game.add.tileSprite(0, 0, 800, 600, 'gameBg');
            console.log("level start");
            this._scoreGr = this.add.group();
            this._sequence = new Rocket.Sequence(this.game, 0, 5, "sequence");
            this._gameControl.setSequence(this._sequence);
            this._scoreGr.add(this._sequence);
            this._points = new Rocket.Round(this.game);
            this._points.scale.setTo(0.5, 0.5);
            console.log("this._points.width", this._points.width);
            this._points.x = this.game.width - 135;
            this._points.y = -50;
            this._gameControl.setRound(this._points);
            this._scoreGr.add(this._points);
            this._countDown = this.add.sprite(0, 0, "countdown", 0);
            //this._countDown.anchor.setTo(0.5, 0.5);
            this._countDown.animations.add("countdownAnim");
            this._countDown.x = this.game.width / 2 - this._countDown.width / 2;
            this._countDown.y = this.game.height / 2 - this._countDown.height / 2;
            this._scoreGr.add(this._countDown);
            // this.game.physics.arcade.skipQuadTree = true;
            this._player = new Rocket.Player(this.game, 0, 200, "player");
            this._player.anchor.setTo(0.5, 0.5);
            this._player.scale.setTo(0.6, 0.6);
            this._player.position.setTo(200, 200);
            this._player.body.enableBody = true;
            this._player.body.setSize(30, 40, -15, 0);
            this.game.physics.enable(this._player, Phaser.Physics.ARCADE);
            this.game.world.setBounds(0, 0, 100, 100);
            this._jumperGr = this.add.group();
            this.stage.addChild(this._player);
            this.obstaclesGr = this.add.group();
            this.stage.addChild(this.obstaclesGr);
            // this.obstaclesGr.addChild(this._jumperGr);
            this.obstaclesGr.enableBody = true;
            this._jumperGr.enableBody = true;
            var checkScreenGroup = this.add.group();
            checkScreenGroup.enableBody = true;
            this._checkScreen = this.add.sprite(0, 0, "", 0, checkScreenGroup);
            var black = new Phaser.Graphics(this.game, 0, 0);
            black.beginFill(0x000000, 0);
            black.drawRect(0, 0, 100, 100);
            black.endFill();
            this._checkScreen.addChild(black);
            this._checkScreen.y = 300;
            //checkScreenGroup.addChild(this._checkScreen);
            // checkScreenGroup.physicsBodyType = Phaser.Physics.ARCADE;
            this._checkScreen.checkWorldBounds = true;
            this._checkScreen.events.onOutOfBounds.add(this.checkScreen, this);
            this._checkScreen.body.velocity.y = this.SPEED;
            /*
                        for (var i: number = 0; i < 2; i++) {
            
                            var enemy = this.obstaclesGr.create(300, i * 500, "jumper", i % 3);
                           
                            enemy.checkWorldBounds = true;
                            enemy.events.onOutOfBounds.add(this.goodbye, this);
                            enemy.body.velocity.y = -50;
                            // (300, i * 300, "obstacles", 0, this.obstaclesGr);
                            // enemy.body.collideWorldBounds = true;
                            //    enemy.body.bounce.set(1);
                            //enemy.enableBody = true;
                            //enemy.physicsBodyType = Phaser.Physics.Arcade;
                            
            
            
                        }
                        
                        this._letterGr = this.add.group();
                        this._letterArray = new Array();
                        for (var i: number = 0; i < 10; i++) {
            
                            var letter: PopLetter = new PopLetter(this.game,200, i * 200, "pop");
                            letter.addLetter(Letters.validLetters[Math.floor(Math.random() * Letters.validLetters.length)]);
                            //letter.collisonPlayAnim();
                            letter.physicsEnabled = true;
                            this.game.physics.enable(letter, Phaser.Physics.ARCADE);
                            this._letterGr.addChild(letter);
                            this._letterArray.push(letter);
                            // (300, i * 300, "obstacles", 0, this.obstaclesGr);
                            // enemy.body.collideWorldBounds = true;
                            //    enemy.body.bounce.set(1);
                            //enemy.enableBody = true;
                            //enemy.physicsBodyType = Phaser.Physics.Arcade;
                         
                        }
            
                        */
            //  this.obstaclesGr.create(400, 500, "jumper"); //addChild(new Jumper(this.game, 400, 500, "jumper"));
            // this.stage.addChild(this._letterGr);
            // this._letterGr.y = 600;
            this.setObject();
            if (!mobile && 1 == 0) {
                this.game.input.onUp.add(function () {
                    console.log('up');
                    this._isDown = false;
                }, this);
                this.game.input.onDown.add(function () {
                    this._isDown = true;
                    console.log('down', this._isDown);
                }, this);
            }
            this.helpGr = this.add.group();
            this.stage.addChild(this.helpGr);
            this._helpBtn = this.add.button(0, 0, "helpBtn", this.helpBtnClick, this, 1, 0, 0, 0, this.helpGr);
            this.helpGr.x = 800 - this.helpGr.width;
            this.helpGr.y = 600 - this.helpGr.height;
            this._helpBtn.buttonMode = true;
            this._helpBtn.inputEnabled = true;
            this._helpBtn.input.priorityID = 110;
            //this.showLetterMenu(new Array("a"));
            this.game.input.onDown.add(function () {
                if (this.game.paused) {
                    this.game.paused = false;
                }
                if (this._instructionShow) {
                    this.stage.removeChild(this._instructionShow);
                    this._instructionShow.kill();
                    this._instructionShow = null;
                }
            }, this);
        };
        Level.prototype.helpBtnClick = function () {
            console.log("pokaz instrukcje");
            this.game.paused = !this.game.paused;
            if (this._instructionShow == null)
                this._instructionShow = this.add.image(0, 0, "firstInstruction", 0);
            // this.add.image(0, 0, "firstInstruction", 0);
            this._instructionShow.width = 800; //this.stage.width;
            this._instructionShow.height = 600;
            this.stage.addChild(this._instructionShow);
        };
        Level.prototype.tweenStop = function () {
            if (this._tween != null) {
                this._tween.stop();
            }
        };
        Level.prototype.stopMoveObject = function (stop_) {
            this.obstaclesGr.forEach(function (item) {
                // item.body.velocity = 0;
                if (item != undefined && stop_) {
                    item.body.velocity.y = 0;
                }
                else if (item != undefined && !stop_) {
                    item.body.velocity.y = this.SPEED;
                }
            }, this);
        };
        Level.prototype.setPlayerDefaultPosition = function () {
            this._player.position.setTo(this._player.x, 200);
        };
        Level.prototype.checkScreen = function (object) {
            this.stage.addChild(this._scoreGr);
            object.y = this.game.height;
            if (!this._gameControl.gameOver) {
                var objectArray = this._formation.level1();
                for (var i = 0; i < objectArray.length; i++) {
                    var obst = objectArray[i];
                    obst.y += 600;
                    this.obstaclesGr.add(obst);
                    this.obstaclesGr.addChild(obst);
                    var rect = obst.getBounce();
                    obst.anchor.setTo(0.5, 0.5);
                    obst.body.setSize(rect.width, rect.height, rect.x, rect.y);
                    obst.enableBody = true;
                    obst.checkWorldBounds = true;
                    //  obst.events.onOutOfBounds.add(this.goodbye, this);
                    obst.body.velocity.y = this.SPEED;
                }
            }
        };
        Level.prototype.goodbye = function (object) {
            if (object.world.y < -600) {
            }
            //  console.log("goodbay", object.name, object.world.y );
            // object.kill();
        };
        Level.prototype.setObject = function () {
            console.log("setobject");
            this.SPEED = -100;
            this.obstaclesGr.y = 600;
            this.cleanObstacle();
            if (this._finishScreen != null) {
                this.stage.removeChild(this._finishScreen);
                this._finishScreen = null;
            }
            this._gameControl.gameOver = false;
            this._checkScreen.body.velocity.y = this.SPEED;
            //   this._gameControl.collisionPlayer = false;
            if (this._showLetter)
                this._showLetter.destroy(true);
            this._player.position.setTo(200, 200);
            // this.obstaclesGr.y = this.game.height;
            //this._letterGr.y = 600;
            this._countDown.animations.frame = 1;
            this._countDown.alpha = 1;
            this._countDown.animations.getAnimation("countdownAnim").onComplete.add(function () {
            }, this);
            this.game.time.events.add(Phaser.Timer.SECOND * 1, function () {
                this._countDown.animations.play("countdownAnim", 24, false, false);
            }, this);
            this.game.time.events.add(Phaser.Timer.SECOND * 4, function () {
                this._countDown.alpha = 0;
                this._gameControl.collisionPlayer = false;
                this.game.input.onDown.add(this.movePlayer, this);
                this._countDown.animations.play("countdownAnim", 24, false, false);
                this._showLetter = new Rocket.ShowLetter(this.game, 0, 0, "collectLetters");
                this._showLetter.addBgToDisplayLetter(this.add.sprite(0, 0, "collectLettersbg"));
                this._showLetter.anchor.setTo(0.5, 0.5);
                //this._showLetter.scale.setTo(0.5, 0.5)
                this._showLetter.x = 400;
                this._showLetter.y = 300;
                this._gameControl.setShowLetter(this._showLetter);
                this._scoreGr.addChild(this._showLetter);
                console.log("liczba przeszkod", this.obstaclesGr.length);
            }, this);
        };
        Level.prototype.playAgain = function () {
            console.log("gram od poczatku");
            this.setObject();
        };
        Level.prototype.movePlayer = function (pointer) {
            if (!this._gameControl.collisionPlayer) {
                if (this._player.x > pointer.x) {
                    this._player.startLeft();
                }
                else {
                    this._player.startRight();
                }
                var posx = pointer.x;
                if (posx < 100)
                    posx = 100;
                else if (posx > 700)
                    posx = 700;
                this.stopTween();
                if (!this._gameControl.collisionPlayer) {
                    var duration = (this.game.physics.arcade.distanceToPointer(this._player, pointer) / 200) * 1000;
                    this._tween = this.game.add.tween(this._player).to({ x: posx }, duration, Phaser.Easing.Linear.None).start();
                    this._tween.onComplete.add(this.endMove, this);
                }
            }
        };
        Level.prototype.endMove = function () {
            this._player.playerPlay();
        };
        Level.prototype.stopTween = function () {
            if (this._tween != null && this._tween.isRunning) {
                this.game.tweens.remove(this._tween);
            }
        };
        Level.prototype.stopAndREmoveLetters = function () {
            this.SPEED = 0;
            this.obstaclesGr.y = 600;
            this.cleanObstacle();
        };
        Level.prototype.showFinishLevel = function () {
            console.log("FInish screen: showFinishLevel");
            this._finishScreenLevel = new Rocket.FinishLevel(this.game);
            this._finishScreenLevel.x = 0; // this.game.width / 2 - this._finishScreenLevel.width / 2;
            this._finishScreenLevel.y = 0; //this.game.height / 2 - this._finishScreenLevel.height / 2;
            this.stage.addChild(this._finishScreenLevel);
            this.SPEED = 0;
            this.obstaclesGr.y = 600;
            this.cleanObstacle();
        };
        Level.prototype.showFinish = function () {
            console.log("FInish screen: showFinish");
            this._finishScreen = new Rocket.FinishScreen(this.game);
            this._finishScreen.x = 0; // this.game.width / 2 - this._finishScreen.width / 2;
            this._finishScreen.y = 0; //this.game.height / 2 - this._finishScreen.height/ 2;
            this.stage.addChild(this._finishScreen);
            this.SPEED = 0;
            this.cleanObstacle();
        };
        Level.prototype.cleanObstacle = function () {
            if (this.obstaclesGr.length > 0) {
                this.obstaclesGr.removeAll();
            }
            /*    this.obstaclesGr.forEach(function (item) {
                   // item.body.velocity = 0;
                  
                    if (item != undefined) {
                        this.obstaclesGr.removeChild(item);
                    }
                }, this);*/
        };
        Level.prototype.drawEnemy = function () {
            if (this.obstaclesGr.y + this.obstaclesGr.height < 600) {
                console.log(" poycja obstacle", this.obstaclesGr.y, this.obstaclesGr.height);
                var obstacle = this._formation.getObstacle();
                obstacle.y = this.obstaclesGr.height + this.obstaclesGr.y + obstacle.y;
                this.obstaclesGr.addChild(obstacle);
            }
        };
        Level.prototype.update = function () {
            if (this._gameControl.gameOver) {
            }
            else {
                this.starfield.tilePosition.y += 2;
                // this.starfield.fixedToCamera = true;
                // this._player.body.velocity.setTo(0, 0);
                // xml
                // kml
                // kxm
                // this._player.body.velocity.y = 100;
                // this.obstaclesGr.body.velocity.y = 10;
                if (!this._gameControl.collisionPlayer) {
                    if (this.changePosYAfterCollsion) {
                        this.obstaclesGr.y += 400;
                        this.changePosYAfterCollsion = false;
                    }
                    else if (this._isDown) {
                        //
                        console.log("is downa");
                        var posx = this.game.input.x;
                        if (posx > 700)
                            posx = 700;
                        else if (posx < 100)
                            posx = 100;
                        this.game.physics.arcade.accelerateToXY(this._player, posx, 200, 1000, 180, 0);
                        //this._player.rotation = 0;
                        this.game.physics.arcade.overlap(this.obstaclesGr, this._player, this.collision, null, this);
                    }
                    else if (mobile) {
                        //this.drawEnemy();
                        //   this.obstaclesGr.y -= 1;
                        this._player.body.velocity.setTo(0, 0);
                        this._player.rotation = 0;
                        this._player.playerPlay();
                        this.game.physics.arcade.accelerateToXY(this._player, posx, 200, 0, 0, 0);
                        this.game.physics.arcade.overlap(this.obstaclesGr, this._player, this.collision, null, this);
                    }
                    else if (!mobile) {
                        this.game.physics.arcade.overlap(this.obstaclesGr, this._player, this.collision, null, this);
                    }
                }
                else {
                }
            }
        };
        Level.prototype.checkCollsionWithLetter = function () {
            if (!this._gameControl.hideLetterOnGame) {
                this.obstaclesGr.forEach(function (item) {
                    if (item != undefined && item.name == "popletter") {
                        item.visible = true;
                    }
                }, this);
            }
            else {
                this.obstaclesGr.forEach(function (item) {
                    if (item != undefined && item.name == "popletter") {
                        item.visible = false;
                        this.obstaclesGr.remove(item);
                        item.kill();
                    }
                }, this);
            }
        };
        Level.prototype.collisionLetter = function (letter) {
            if (!this._gameControl.collisionPlayer) {
                //this.changePosYAfterCollsion = true;
                // this._gameControl.collisionPlayer = true;
                //zxczvbnm, \\ //\\ //--\\
                //  console.log(letter);
                //  letter.body.moves = false;
                letter.collisonPlayAnim();
                this._sequence.addLetter(letter.letter);
                this._gameControl.checkCorrectLetters(letter.letter);
                this.removeLetterFromCollision(letter);
                letter = null;
            }
        };
        Level.prototype.removeLetterFromCollision = function (letter) {
            /* for (var i: number = 0; i < this._letterArray.length; i++) {
 
                 if (this._letterArray[i] == letter) {
 
                     this._letterArray.splice(i, 1);
                     --i;
                     if (i < 0) i = 0;
 
                 }
 
 
 
             }*/
        };
        Level.prototype.setPlayerToTop = function (topPlayer) {
            if (topPlayer) {
                this.stage.addChild(this._player);
            }
            else {
                this.stage.addChild(this.obstaclesGr);
            }
            this.stage.addChild(this._scoreGr);
        };
        Level.prototype.collision = function (object, object2) {
            if (!this._gameControl.collisionPlayer) {
                console.log(object2.name, object2.key);
                if (object2.name == "popletter") {
                    //  this.obstaclesGr.removeChild(object2);
                    //  object2.body.collideWorldBounds = true;
                    //  this.setPlayerToTop(true);
                    object2.body.setSize(1200, -1200, -1, -1);
                    object2.body.checkCollision.none = true;
                    object2.collisonPlayAnim();
                    this._sequence.addLetter(object2.letter);
                    this._gameControl.checkCorrectLetters(object2.letter);
                }
                else if (object2.name == "jumper") {
                    this.setPlayerToTop(true);
                    // object2.body.setSize(-2000, -2000, 0, 0);
                    //object2.body.checkCollision.none = true;
                    //object2.body.moves = false;
                    this.tweenStop();
                    this._gameControl.isJumping = true;
                    this._player.jump();
                }
                else if (object2.name == "electro") {
                    this.stopMoveObject(true);
                    this.setPlayerToTop(true);
                    this.changePosYAfterCollsion = true;
                    this._gameControl.collisionPlayer = true;
                    //   object2.body.setSize(-2000, -2000, 0, 0);
                    //   object2.body.checkCollision.none = true;
                    //object2.body.moves = false;
                    this.tweenStop();
                    this._player.electro();
                }
                else if (object2.name == "hole") {
                    this.stopMoveObject(true);
                    this.setPlayerToTop(true);
                    this.changePosYAfterCollsion = true;
                    console.log("hole", object2.width, object2.y, object2.height);
                    this._player.position.setTo(object2.x, object2.world.y + 20);
                    this._gameControl.collisionPlayer = true;
                    //   object2.body.setSize(-2000, -2000, 0, 0);
                    //   object2.body.checkCollision.none = true;
                    //object2.body.moves = false;
                    this.tweenStop();
                    this._player.hole();
                }
                else if (!this._gameControl.isJumping) {
                    this.stopMoveObject(true);
                    this.changePosYAfterCollsion = true;
                    this._gameControl.collisionPlayer = true;
                    this.stopTween();
                    this._player.collisionPlayer();
                }
            }
            this.obstaclesGr.forEach(function (item) {
                // item.body.velocity = 0;
                if (item != undefined && item.world.y < -600)
                    item.destroy(true);
            }, this);
        };
        Level.prototype.collisionJumper = function () {
            if (!this._gameControl.collisionPlayer) {
            }
        };
        Level.prototype.render = function () {
            /*this.game.debug.quadTree(this.game.physics.arcade.quadTree);
      
            this.obstaclesGr.forEach(function (item) {
                // item.body.velocity = 0;
             
                this.game.debug.body(item);
            }, this)

       */
        };
        Level.prototype.hideScreenAfterFinishPlaye = function () {
        };
        return Level;
    })(Phaser.State);
    Rocket.Level = Level;
})(Rocket || (Rocket = {}));
var Rocket;
(function (Rocket) {
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            _super.call(this, 800, 600, Phaser.AUTO, "content", null);
            //console.log(this.scale);
            // this.scale.
            this.state.add("Boot", Rocket.Boot, false);
            this.state.add("Preloader", Rocket.Preloader, false);
            this.state.add("MainMenu", Rocket.MainMenu, false);
            this.state.add("PopupStart", Rocket.PopupStart, false);
            this.state.add("Instruction", Rocket.Instruction, false);
            this.state.add("Level", Rocket.Level, false);
            this.state.start("Boot");
        }
        return Game;
    })(Phaser.Game);
    Rocket.Game = Game;
})(Rocket || (Rocket = {}));
window.onload = function () {
    var game = new Rocket.Game;
};
var userLevel = 1;
var Rocket;
(function (Rocket) {
    var MainMenu = (function (_super) {
        __extends(MainMenu, _super);
        function MainMenu() {
            _super.apply(this, arguments);
        }
        MainMenu.prototype.create = function () {
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var _mainMenuBg = this.game.add.audio('MainMenuSn', 1, true);
            _mainMenuBg.volume = 0.1;
            _mainMenuBg.play();
            var gameControler = Rocket.GameControler.getInstance();
            gameControler.setSound(_mainMenuBg);
            var background = this.add.sprite(0, 0, 'mainMenuBg');
            var _buttons = this.add.group();
            var gameButton = this.add.button(0, 0, "games", this.onGameClick, this, 1, 0);
            //gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            _buttons.addChild(gameButton);
            var playButton = this.add.button(100, 0, "playGame", this.onPlayClick, this, 1, 0);
            playButton.onInputDown.add(this.overPlay2, this);
            _buttons.addChild(playButton);
            _buttons.x = 0;
            _buttons.y = this.game.stage.height - _buttons.height;
        };
        MainMenu.prototype.overPlay2 = function () {
            this.overButtonA.play();
        };
        MainMenu.prototype.onGameClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        MainMenu.prototype.onPlayClick = function () {
            this.game.state.start("PopupStart", true, false);
        };
        return MainMenu;
    })(Phaser.State);
    Rocket.MainMenu = MainMenu;
})(Rocket || (Rocket = {}));
var Rocket;
(function (Rocket) {
    var Blokade = (function (_super) {
        __extends(Blokade, _super);
        function Blokade(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this.name = key;
            this.frame = Math.floor(Math.random() * this.animations.frameTotal);
        }
        return Blokade;
    })(Phaser.Sprite);
    Rocket.Blokade = Blokade;
})(Rocket || (Rocket = {}));
var Rocket;
(function (Rocket) {
    var Electro = (function (_super) {
        __extends(Electro, _super);
        function Electro(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this.name = key;
            this.frame = Math.floor(Math.random() * this.animations.frameTotal);
        }
        return Electro;
    })(Phaser.Sprite);
    Rocket.Electro = Electro;
})(Rocket || (Rocket = {}));
var Rocket;
(function (Rocket) {
    var Hole = (function (_super) {
        __extends(Hole, _super);
        function Hole(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this.name = key;
            this.frame = Math.floor(Math.random() * this.animations.frameTotal);
        }
        return Hole;
    })(Phaser.Sprite);
    Rocket.Hole = Hole;
})(Rocket || (Rocket = {}));
var Rocket;
(function (Rocket) {
    var Jumper = (function (_super) {
        __extends(Jumper, _super);
        function Jumper(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this.frame = Math.floor(Math.random() * this.animations.frameTotal);
        }
        return Jumper;
    })(Phaser.Sprite);
    Rocket.Jumper = Jumper;
})(Rocket || (Rocket = {}));
var Rocket;
(function (Rocket) {
    var ObstacleColumn = (function (_super) {
        __extends(ObstacleColumn, _super);
        function ObstacleColumn(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this.name = key;
            this.frame = Math.floor(Math.random() * this.animations.frameTotal);
        }
        return ObstacleColumn;
    })(Phaser.Sprite);
    Rocket.ObstacleColumn = ObstacleColumn;
})(Rocket || (Rocket = {}));
var Rocket;
(function (Rocket) {
    var Wall = (function (_super) {
        __extends(Wall, _super);
        function Wall(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this.name = key;
            this.frame = Math.floor(Math.random() * this.animations.frameTotal);
        }
        return Wall;
    })(Phaser.Sprite);
    Rocket.Wall = Wall;
})(Rocket || (Rocket = {}));
var Rocket;
(function (Rocket) {
    var Obstacle = (function (_super) {
        __extends(Obstacle, _super);
        function Obstacle(game, x, y, key, posx, posy, bounceWidth, bounceHeight) {
            if (posx === void 0) { posx = 0; }
            if (posy === void 0) { posy = 0; }
            if (bounceWidth === void 0) { bounceWidth = 0; }
            if (bounceHeight === void 0) { bounceHeight = 0; }
            _super.call(this, game, x, y, key);
            this.name = key;
            if (this.frame != null) {
                this.frame = Math.floor(Math.random() * this.animations.frameTotal);
            }
            this.posx = (posx == 0) ? 0 : posx;
            this.posy = (posy == 0) ? 0 : posy;
            this.bounceWidth = (bounceWidth == 0) ? this.width : bounceWidth;
            this.bounceHeight = (bounceHeight == 0) ? this.height : bounceHeight;
        }
        Obstacle.prototype.getBounce = function () {
            var rect = new Phaser.Rectangle(this.posx, this.posy, this.bounceWidth, this.bounceHeight);
            return rect;
        };
        return Obstacle;
    })(Phaser.Sprite);
    Rocket.Obstacle = Obstacle;
})(Rocket || (Rocket = {}));
var Rocket;
(function (Rocket) {
    var Player = (function (_super) {
        __extends(Player, _super);
        function Player(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this._gameControl = Rocket.GameControler.getInstance();
            this.anchor.setTo(0.5, 0.5);
            game.physics.enable(this, Phaser.Physics.ARCADE);
            this.animations.add("MoveForward", this.countFrames(1, 22), 24, true);
            this.animations.add("collision", this.countFrames(141, 179), 24, false);
            this.animations.add("collisionBackward", this.countFrames(180, 229), 24, false);
            this.animations.add("startLeft", this.countFrames(23, 27), 24, false);
            this.animations.add("moveLeft", this.countFrames(28, 44), 24, false);
            this.animations.add("startRight", this.countFrames(46, 50), 24, false);
            this.animations.add("moveRight", this.countFrames(51, 69), 24, false);
            this.animations.add("jump", this.countFrames(71, 140), 24, false);
            this.animations.add("crashDown", this.countFrames(230, 260), 24, false);
            this.animations.add("electrocout", this.countFrames(265, 299), 24, false);
            this.animations.play("MoveForward", 24, true, false);
            this.electroAudio = this.game.sound.add("electro", 1, false, true);
            this.electroAudio2 = this.game.sound.add("electro1", 1, false, true);
            this.crashAudio = this.game.sound.add("crash", 1, false, true);
            this.crashAudio2 = this.game.sound.add("crash2", 1, false, true);
            this.jumpAudio = this.game.sound.add("jumpAudio", 1, false, true);
            this.holeAudio = this.game.sound.add("holeAudio", 1, false, true);
        }
        Player.prototype.jump = function () {
            this.animations.play("jump", 24, false, false);
            this._gameControl.blokadeClick(true);
            this.jumpAudio.play();
            this.animations.getAnimation("jump").onComplete.add(function () {
                this.playerPlay(false);
                this._gameControl.blokadeClick(false);
                this._gameControl._gameLevel.setPlayerToTop(false);
                this._gameControl.isJumping = false;
            }, this);
        };
        Player.prototype.hole = function () {
            this.animations.play("crashDown", 24, false, false);
            // this._gameControl.blokadeClick(true);
            this.holeAudio.play();
            this.animations.getAnimation("crashDown").onComplete.add(function () {
                this._gameControl._gameLevel.stopMoveObject(false);
                this.playerPlay(true);
            }, this);
        };
        Player.prototype.electro = function () {
            this.animations.play("electrocout", 24, false, false);
            // this._gameControl.blokadeClick(true);
            this.animations.getAnimation("electrocout").onComplete.add(function () {
                this._gameControl._gameLevel.stopMoveObject(false);
                this.playerPlay(true);
            }, this);
            this.electroAudio.play();
            this.electroAudio2.play();
        };
        Player.prototype.playerPlay = function (youAreDead) {
            if (youAreDead === void 0) { youAreDead = false; }
            if (youAreDead) {
                this.animations.play("MoveForward", 24, true, false);
                this._gameControl.collisionPlayer = false;
            }
            else {
                // this._gameControl.collisionPlayer = false;
                //this.y -= 200;
                this.animations.play("MoveForward", 24, true, false);
            }
            this.y = 200;
        };
        Player.prototype.playAgain = function () {
            this._gameControl.collisionPlayer = false;
        };
        Player.prototype.collisionPlayer = function () {
            this.crashAudio.play();
            this.crashAudio2.play();
            this.animations.play("collisionBackward", 24, false, false);
            this.animations.getAnimation("collisionBackward").onComplete.add(function () {
                this._gameControl._gameLevel.stopMoveObject(false);
                this.playerPlay(true);
            }, this);
        };
        Player.prototype.collisionForwardPlayer = function () {
            this.animations.play("collision", 24, false, false);
            this.animations.getAnimation("collision").onComplete.add(function () {
                this.playerPlay(true);
            }, this);
        };
        Player.prototype.startLeft = function () {
            this.animations.getAnimation("startLeft").onComplete.add(this.moveLeft, this);
            this.animations.play("startLeft", 24, false, false);
        };
        Player.prototype.moveLeft = function () {
            this.animations.play("moveLeft", 24, false, false).onComplete.add(this.stopLeft, this);
        };
        Player.prototype.stopLeft = function () {
            this.animations.stop("moveLeft");
            this.animations.frame = 44;
        };
        Player.prototype.startRight = function () {
            this.animations.getAnimation("startRight").onComplete.add(this.moveRight, this);
            this.animations.play("startRight", 24, false, false);
        };
        Player.prototype.moveRight = function () {
            this.animations.play("moveRight", 24, false, false).onComplete.add(function () {
                this.frame = 76;
            });
        };
        Player.prototype.countFrames = function (start, num) {
            var countArr = new Array();
            for (var i = start; i < num + 1; i++) {
                countArr.push(i);
            }
            return countArr;
        };
        return Player;
    })(Phaser.Sprite);
    Rocket.Player = Player;
})(Rocket || (Rocket = {}));
var Rocket;
(function (Rocket) {
    var PopLetter = (function (_super) {
        __extends(PopLetter, _super);
        function PopLetter(game, x, y, key, posx, posy, bounceWidth, bounceHeight) {
            if (posx === void 0) { posx = 0; }
            if (posy === void 0) { posy = 0; }
            if (bounceWidth === void 0) { bounceWidth = 0; }
            if (bounceHeight === void 0) { bounceHeight = 0; }
            _super.call(this, game, x, y, key);
            this.name = "popletter";
            this.animations.add("pop");
            this.posx = (posx == 0) ? 0 : posx;
            this.posy = (posy == 0) ? 0 : posy;
            this.bounceWidth = (bounceWidth == 0) ? this.width : bounceWidth;
            this.bounceHeight = (bounceHeight == 0) ? this.height : bounceHeight;
        }
        PopLetter.prototype.addLetter = function (str) {
            var style = { font: "40px Arial", fill: "#000000", align: "center" };
            if (this._letterShow)
                this._letterShow.destroy();
            this._letterShow = new Phaser.Text(this.game, 0, 0, str, style);
            this._letterShow.anchor.set(0.5, 0.5);
            //   this._letterShow.x = 40;
            //  this._letterShow.y = 25;
            this.addChild(this._letterShow);
            this.letter = str;
        };
        PopLetter.prototype.getBounce = function () {
            var rect = new Phaser.Rectangle(this.posx, this.posy, this.bounceWidth, this.bounceHeight);
            return rect;
        };
        PopLetter.prototype.collisonPlayAnim = function () {
            this.removeChild(this._letterShow);
            this.animations.getAnimation("pop").onComplete.add(function () {
                this.kill();
            }, this);
            this.play("pop", 24, false, true);
        };
        return PopLetter;
    })(Phaser.Sprite);
    Rocket.PopLetter = PopLetter;
})(Rocket || (Rocket = {}));
var Rocket;
(function (Rocket) {
    var Preloader = (function (_super) {
        __extends(Preloader, _super);
        function Preloader() {
            _super.apply(this, arguments);
        }
        Preloader.prototype.preload = function () {
            var bg = this.add.image(0, 0, "bg", 0);
            var blackProgress = this.add.image(0, 0, "progressBarBlack", 0);
            var blueProgress = this.add.sprite(0, 0, "progressBarBlue", 0);
            blackProgress.position.setTo(this.stage.width / 2 - blackProgress.width / 2, this.stage.height - blackProgress.height - 10);
            blueProgress.position = blackProgress.position;
            this.load.setPreloadSprite(blueProgress);
            this.load.image("mainMenuBg", "assets/mainMenu/SplashScreen2.png");
            this.load.atlasJSONArray('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');
            this.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
            this.load.audio("MainMenuSn", "assets/mainMenu/BBThemeMusic.mp3");
            this.load.audio("overButtonA", "assets/audio/overButton.mp3");
            this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");
            this.load.audio("chuja", "assets/audio/instruction/1.mp3");
            this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");
            this.load.audio("electro", "assets/audio/player/electro.mp3");
            this.load.audio("electro1", "assets/audio/player/electro2.mp3");
            this.load.audio("crash", "assets/audio/player/crash.mp3");
            this.load.audio("crash2", "assets/audio/player/crash2.mp3");
            this.load.audio("jump", "assets/audio/player/jump.mp3");
            this.load.audio("holeAudio", "assets/audio/player/hole.mp3");
            this.load.audio("letterd", "assets/audio/letter/d.mp3");
            this.load.audio("letterp", "assets/audio/letter/p.mp3");
            this.load.audio("letterb", "assets/audio/letter/b.mp3");
            var licz = 1;
            for (var i = 13; i < 17; i++) {
                this.load.audio("fail" + licz, "assets/audio/rocket_racer_" + i + ".mp3");
                licz++;
            }
            licz = 1;
            for (var i = 10; i < 13; i++) {
                this.load.audio("success" + licz, "assets/audio/rocket_racer_" + i + ".mp3");
                licz++;
            }
            this.load.image("gameBg", "assets/rocket/rocketGameBg.png");
            this.load.atlasJSONArray("obstacles", "assets/rocket/obstacles/obstacles.png", "assets/rocket/obstacles/obstacles.json");
            this.load.atlasJSONArray("hole", "assets/rocket/obstacles/hole.png", "assets/rocket/obstacles/hole.json");
            this.load.atlasJSONArray("electro", "assets/rocket/obstacles/electro.png", "assets/rocket/obstacles/electro.json");
            this.load.atlasJSONArray("blokade", "assets/rocket/obstacles/blokade.png", "assets/rocket/obstacles/blokade.json");
            this.load.atlasJSONArray("wall", "assets/rocket/obstacles/wall.png", "assets/rocket/obstacles/wall.json");
            this.load.atlasJSONArray("jumper", "assets/rocket/obstacles/jumper.png", "assets/rocket/obstacles/jumper.json");
            this.load.atlasJSONArray("player", "assets/rocket/player/player2.png", "assets/rocket/player/player2.json");
            this.load.atlasJSONArray("countdown", "assets/rocket/countdown.png", "assets/rocket/countdown.json");
            this.load.image("sequence", "assets/rocket/sequence.png");
            this.load.atlasJSONArray("level1Pie", "assets/rocket/level1Pie1.png", "assets/rocket/level1Pie1.json");
            this.load.atlasJSONArray("level2Pie", "assets/rocket/level2Pie2.png", "assets/rocket/level2Pie2.json");
            this.load.atlasJSONArray("level3Pie", "assets/rocket/level3Pie3.png", "assets/rocket/level3Pie3.json");
            this.load.atlasJSONArray("level4Pie", "assets/rocket/level4Pie4.png", "assets/rocket/level4Pie4.json");
            this.load.atlasJSONArray("level5Pie", "assets/rocket/level5Pie5.png", "assets/rocket/level5Pie5.json");
            this.load.atlasJSONArray("level6Pie", "assets/rocket/level6Pie6.png", "assets/rocket/level6Pie6.json");
            this.load.atlasJSONArray("pop", "assets/rocket/pop.png", "assets/rocket/pop.json");
            var mobile;
            mobile = false;
            if (!this.game.device.desktop) {
                mobile = true;
            }
            if (mobile) {
                this.load.image("firstInstruction", "assets/03A_rocket_Racer.png");
                this.load.audio("instructionSn", "assets/audio/rocket_racer_02.mp3");
            }
            else {
                this.load.image("firstInstruction", "assets/02A_rocket_Racer.png");
                this.load.audio("instructionSn", "assets/audio/rocket_racer_01.mp3");
            }
            this.load.image("popup", "assets/popup/popup.png");
            this.load.image("logoGame", "assets/popup/logo.png");
            this.load.atlasJSONArray('games', 'assets/popup/games.png', 'assets/popup/games.json');
            this.load.atlasJSONArray('playGame', 'assets/popup/play.png', 'assets/popup/play.json');
            this.load.atlasJSONArray('helpBtn', 'assets/popup/help.png', 'assets/popup/help.json');
            this.load.image("firstInstruction", "assets/InstructionScreen.png");
            this.load.image("collectLetters", "assets/rocket/collectLetters.png");
            this.load.image("collectLettersbg", "assets/rocket/TrickItemDisplay.png");
            this.load.atlasJSONArray("scoreScreen", "assets/scoreScreen/scoreScreen.png", "assets/scoreScreen/scoreScreen.json");
            this.load.atlasJSONArray("playScoreScreen", "assets/scoreScreen/scoreScreenButtons.png", "assets/scoreScreen/scoreScreenButtons.json");
        };
        Preloader.prototype.create = function () {
            this.game.state.start("MainMenu", true, false);
        };
        return Preloader;
    })(Phaser.State);
    Rocket.Preloader = Preloader;
})(Rocket || (Rocket = {}));
var mobile = false;
var Rocket;
(function (Rocket) {
    var FinishLevel = (function (_super) {
        __extends(FinishLevel, _super);
        function FinishLevel(game) {
            _super.call(this, game);
            this._gameControl = Rocket.GameControler.getInstance();
            this._game = game;
            this.create();
        }
        FinishLevel.prototype.create = function () {
            this._finishScreen = new Rocket.PopUpWin(this.game, this, 0, 0);
            this._finishScreen.visible = true;
            this.addChild(this._finishScreen);
        };
        FinishLevel.prototype.playAgain = function () {
            this.removeAll();
            this.destroy(true);
            this._gameControl._showLetter.playAgainAfterSound();
            this._gameControl.hideLetterOnGame = false;
            this._gameControl.nextLevel();
            console.log("play again");
        };
        FinishLevel.prototype.hideScreenAfterFinishPlaye = function () {
            this.playAgain();
        };
        FinishLevel.prototype.quitClick = function () {
            console.log("quit");
        };
        return FinishLevel;
    })(Phaser.Group);
    Rocket.FinishLevel = FinishLevel;
})(Rocket || (Rocket = {}));
var Rocket;
(function (Rocket) {
    var FinishScreen = (function (_super) {
        __extends(FinishScreen, _super);
        function FinishScreen(game) {
            _super.call(this, game);
            this._gameControl = Rocket.GameControler.getInstance();
            this._game = game;
            this.create();
        }
        FinishScreen.prototype.create = function () {
            this._finishScreen = new Rocket.PopUp(this.game, this, 0, 0);
            this._finishScreen.visible = true;
            this.addChild(this._finishScreen);
        };
        FinishScreen.prototype.playAgain = function () {
            this.removeAll();
            this.destroy(true);
            // this._gameLevel.playAgain();
            this._gameControl.hideLetterOnGame = false;
            this._gameControl.playAgain();
            console.log("play again skurwysyn");
        };
        FinishScreen.prototype.hideScreenAfterFinishPlaye = function () {
            this.playAgain();
        };
        FinishScreen.prototype.quitClick = function () {
            console.log("quit");
        };
        return FinishScreen;
    })(Phaser.Group);
    Rocket.FinishScreen = FinishScreen;
})(Rocket || (Rocket = {}));
var Rocket;
(function (Rocket) {
    var PopUp = (function (_super) {
        __extends(PopUp, _super);
        function PopUp(game, level, x, y) {
            _super.call(this, game, x, y);
            this.level1 = level;
            this._game = game;
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var bgPopup = new Phaser.Sprite(this.game, 0, 0, "popup");
            this.addChild(bgPopup);
            this.logo = new Phaser.Sprite(this.game, 0, 0, "logoGame");
            this.addChild(this.logo);
            this.logo.x = (bgPopup.width / 2 - this.logo.width / 2);
            this.logo.y = 70;
            var text = "Good try.\n Keep playing to win credits";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this.game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;
            textDescription.x = this.game.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 140;
            this.addChild(textDescription);
            this.textLevel();
            //this.beatText();
            this.addButtons();
            this.visible = false;
        }
        PopUp.prototype.addButtons = function () {
            this.gamesBtn = new Phaser.Button(this._game, 0, 0, "games", this.gamesBtnClick, this, 1, 0);
            this.playBtn = new Phaser.Button(this._game, 100, 0, "playGame", this.playBtnClick, this, 1, 0);
            this.gamesBtn.inputEnabled = true;
            this.gamesBtn.input.priorityID = 3;
            this.playBtn.inputEnabled = true;
            this.playBtn.input.priorityID = 3;
            var buttonsGr = new Phaser.Group(this._game);
            this.addChild(buttonsGr);
            buttonsGr.add(this.gamesBtn);
            buttonsGr.add(this.playBtn);
            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;
        };
        PopUp.prototype.gamesBtnClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        PopUp.prototype.playBtnClick = function () {
            this.overButtonA.play();
            this.visible = false;
            this.gamesBtn.inputEnabled = false;
            this.gamesBtn.input.priorityID = 3;
            this.playBtn.inputEnabled = false;
            this.playBtn.input.priorityID = 3;
            this.level1.hideScreenAfterFinishPlaye();
            //  this.game.state.start("PopupStart", true, false);
        };
        PopUp.prototype.textLevel = function () {
            var text = "GAME OVER";
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this._game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = this.game.stage.width / 2 - textDescription.width / 2 - 10;
            textDescription.y = this.logo.y + this.logo.height + 25;
            this.addChild(textDescription);
        };
        return PopUp;
    })(Phaser.Sprite);
    Rocket.PopUp = PopUp;
})(Rocket || (Rocket = {}));
function launchGame(result) {
    userLevel = result.level;
}
//declare function launchGame(result);
var Rocket;
(function (Rocket) {
    var PopupStart = (function (_super) {
        __extends(PopupStart, _super);
        function PopupStart() {
            _super.apply(this, arguments);
        }
        PopupStart.prototype.create = function () {
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var background = this.add.sprite(0, 0, 'mainMenuBg');
            var bgPopup = this.add.sprite(0, 0, "popup");
            bgPopup.y = 0;
            this.logo = this.add.sprite(0, 0, "logoGame");
            this.logo.x = (bgPopup.width / 2 - this.logo.width / 2);
            this.logo.y = 70;
            var text = "First, listen to the sounds.\n Then, move your mouse left and right to collect the letters that match the sequence! ";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;
            textDescription.x = this.game.stage.width / 2 - textDescription.wordWrapWidth / 2 + 5;
            textDescription.y = this.logo.y + this.logo.height + 130;
            this.textLevel();
            //this.beatText();
            this.addButtons();
        };
        PopupStart.prototype.addButtons = function () {
            var gamesBtn = this.add.button(0, 0, "games", this.gamesBtnClick, this, 1, 0);
            var playBtn = this.add.button(100, 0, "playGame", this.playBtnClick, this, 1, 0);
            playBtn.onInputDown.add(this.overPlay2, this);
            var buttonsGr = this.add.group();
            buttonsGr.add(gamesBtn);
            buttonsGr.add(playBtn);
            buttonsGr.x = this.game.stage.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.stage.height - 150;
        };
        PopupStart.prototype.overPlay2 = function () {
            this.overButtonA.play();
        };
        PopupStart.prototype.gamesBtnClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        PopupStart.prototype.playBtnClick = function () {
            this.overButtonA.play();
            this.game.state.start("Instruction", true, false);
        };
        PopupStart.prototype.beatText = function () {
            var text = "BEAT THIS LEVEL";
            //" 6";
            var style = { font: "bold 13px Arial", fill: "#660000", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = (this.game.width / 2) + (140) - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 100;
            var numberText = "6";
            var styleNumber = { font: "bold 17px Arial", fill: "#990000", align: "center" };
            var numberShow = this.add.text(0, 0, numberText, styleNumber);
            numberShow.x = textDescription.x + textDescription.width / 2 - numberShow.width / 2;
            numberShow.y = textDescription.y + textDescription.height;
            var tunesText = "MORE TUNES TO WIN";
            var tunesShow = this.add.text(0, 0, tunesText, style);
            tunesShow.wordWrap = true;
            tunesShow.wordWrapWidth = 100;
            tunesShow.x = textDescription.x + textDescription.width / 2 - tunesShow.width / 2;
            tunesShow.y = numberShow.y + numberShow.height;
            var creditsNmText = "25";
            var styleCredits = { font: "bold 17px Arial", fill: "#ff0000", align: "center" };
            var credistNmShow = this.add.text(0, 0, creditsNmText, styleCredits);
            credistNmShow.x = tunesShow.x + tunesShow.width / 2 - credistNmShow.width / 2;
            credistNmShow.y = tunesShow.y + tunesShow.height;
            var creditsText = "CREDITS";
            var credistShow = this.add.text(0, 0, creditsText, style);
            credistShow.x = tunesShow.x + tunesShow.width / 2 - credistShow.width / 2;
            credistShow.y = credistNmShow.y + credistNmShow.height;
        };
        PopupStart.prototype.textCredits = function () {
            var text = "Beat this level";
            //" 6";
            var style = { font: "bold 14px Arial", fill: "#660000", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = (this.game.width / 2) + 257 / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 100;
            var numberText = "6";
            var styleNumber = { font: "bold 18px Arial", fill: "#990000", align: "center" };
            var numberShow = this.add.text(0, 0, numberText, styleNumber);
            numberShow.x = textDescription.x + textDescription.width / 2 - numberShow.width / 2;
            numberShow.y = textDescription.y + textDescription.height;
        };
        PopupStart.prototype.textLevel = function () {
            //APIgetLevel(userID, gameID);
            var text = "LEVEL " + userLevel;
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = this.game.stage.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 25;
        };
        return PopupStart;
    })(Phaser.State);
    Rocket.PopupStart = PopupStart;
})(Rocket || (Rocket = {}));
var Rocket;
(function (Rocket) {
    var PopUpWin = (function (_super) {
        __extends(PopUpWin, _super);
        function PopUpWin(game, level, x, y) {
            _super.call(this, game, x, y);
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            this._game = game;
            this.level1 = level;
            var bgPopup = new Phaser.Sprite(this.game, 0, 0, "popup");
            this.addChild(bgPopup);
            this.logo = new Phaser.Sprite(this.game, 0, 0, "logoGame");
            this.addChild(this.logo);
            this.logo.x = (bgPopup.width / 2 - this.logo.width / 2);
            this.logo.y = 70;
            var text = "You finish the level.\n Keep Playing to win credits!";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this.game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;
            textDescription.x = this.game.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 140;
            this.addChild(textDescription);
            this.textLevel();
            //this.beatText();
            this.addButtons();
            this.visible = false;
        }
        PopUpWin.prototype.addButtons = function () {
            this.gamesBtn = new Phaser.Button(this._game, 0, 0, "games", this.gamesBtnClick, this, 1, 0);
            this.playBtn = new Phaser.Button(this._game, 100, 0, "playGame", this.playBtnClick, this, 1, 0);
            this.gamesBtn.inputEnabled = true;
            this.gamesBtn.input.priorityID = 3;
            this.playBtn.inputEnabled = true;
            this.playBtn.input.priorityID = 3;
            var buttonsGr = new Phaser.Group(this._game);
            this.addChild(buttonsGr);
            buttonsGr.add(this.gamesBtn);
            buttonsGr.add(this.playBtn);
            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;
        };
        PopUpWin.prototype.gamesBtnClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        PopUpWin.prototype.playBtnClick = function () {
            this.visible = false;
            this.gamesBtn.inputEnabled = false;
            this.gamesBtn.input.priorityID = 3;
            this.playBtn.inputEnabled = false;
            this.playBtn.input.priorityID = 3;
            this.level1.hideScreenAfterFinishPlaye();
            //  this.game.state.start("PopupStart", true, false);
            this.overButtonA.play();
        };
        PopUpWin.prototype.textLevel = function () {
            var text = "GOOD JOB";
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this._game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = this.game.stage.width / 2 - textDescription.width / 2 + 4;
            textDescription.y = this.logo.y + this.logo.height + 25;
            this.addChild(textDescription);
        };
        return PopUpWin;
    })(Phaser.Sprite);
    Rocket.PopUpWin = PopUpWin;
})(Rocket || (Rocket = {}));
var Rocket;
(function (Rocket) {
    var Round = (function (_super) {
        __extends(Round, _super);
        function Round(game) {
            _super.call(this, game);
            this._gameControler = Rocket.GameControler.getInstance();
            /*
                        this._contenerLetter = new Phaser.Group(game);
                        this._contenerLetter.y = 30;
                        this._contenerLetter.x = 10;
                        this.addChild(this._contenerLetter);
                        */
            this._game = game;
            this.setAnim();
            this.successArr = new Array();
            for (var i = 1; i < 5; i++) {
                this.successArr.push(this.game.sound.add("success" + i, 1, false));
            }
            // this.animations.play("firstRound", 24, false);
        }
        Round.prototype.setAnim = function () {
            if (this._animRound != null) {
                this.removeAll(true);
                this._animRound.kill();
                this._animRound = null;
            }
            switch (this._gameControler.currentLevel) {
                case 0:
                    console.log("ustawiam Animacje round");
                    this._animRound = new Phaser.Sprite(this._game, 90, 90, "level1Pie");
                    this._animRound.animations.add("round1");
                    break;
                case 1:
                    this._animRound = new Phaser.Sprite(this._game, 0, 0, "level2Pie");
                    this._animRound.animations.add("round1", this.doFrame(10, 27));
                    this._animRound.animations.add("round2", this.doFrame(28, 48));
                    break;
                case 2:
                    this._animRound = new Phaser.Sprite(this._game, 0, 0, "level3Pie");
                    this._animRound.animations.add("round1", this.doFrame(10, 27));
                    this._animRound.animations.add("round2", this.doFrame(28, 46));
                    this._animRound.animations.add("round3", this.doFrame(47, 65));
                    break;
                case 3:
                    this._animRound = new Phaser.Sprite(this._game, 0, 0, "level4Pie");
                    this._animRound.animations.add("round1", this.doFrame(10, 27));
                    this._animRound.animations.add("round2", this.doFrame(28, 46));
                    this._animRound.animations.add("round3", this.doFrame(47, 65));
                    this._animRound.animations.add("round4", this.doFrame(66, 85));
                    break;
                case 4:
                    this._animRound = new Phaser.Sprite(this._game, 0, 0, "level5Pie");
                    this._animRound.animations.add("round1", this.doFrame(10, 26));
                    this._animRound.animations.add("round2", this.doFrame(28, 46));
                    this._animRound.animations.add("round3", this.doFrame(47, 66));
                    this._animRound.animations.add("round4", this.doFrame(67, 86));
                    this._animRound.animations.add("round5", this.doFrame(87, 106));
                    break;
                case 5:
                    this._animRound = new Phaser.Sprite(this._game, 90, 90, "level6Pie");
                    this._animRound.animations.add("round1", this.doFrame(10, 27));
                    this._animRound.animations.add("round2", this.doFrame(28, 46));
                    this._animRound.animations.add("round3", this.doFrame(47, 66));
                    this._animRound.animations.add("round4", this.doFrame(67, 86));
                    this._animRound.animations.add("round5", this.doFrame(87, 105));
                    this._animRound.animations.add("round6", this.doFrame(106, 125));
                    break;
                default:
                    console.log("poza levelem");
            }
            this.addChild(this._animRound);
        };
        Round.prototype.nextRound = function () {
            var ktory = this._gameControler.currentRoundPerLevel;
            if (this._gameControler.currentRoundPerLevel == this.successArr.length) {
                ktory = this.successArr.length - 1;
            }
            this._animRound.animations.play("round" + this._gameControler.currentRoundPerLevel, 24, false);
            this.successArr[ktory].onStop.add(this.playAgain, this);
            this.successArr[ktory].play();
        };
        Round.prototype.playAgain = function () {
            // this._gameControler._showLetter.playAgainAfterSound();
            //  this._gameControler.checkRoundPerLevel();
            //  this._gameControler._gameLevel.showFinishLevel();
        };
        Round.prototype.doFrame = function (start, end) {
            var frameArr = new Array();
            for (var i = start; i < end + 1; i++) {
                frameArr.push(i);
            }
            return frameArr;
        };
        return Round;
    })(Phaser.Group);
    Rocket.Round = Round;
})(Rocket || (Rocket = {}));
var Rocket;
(function (Rocket) {
    var Sequence = (function (_super) {
        __extends(Sequence, _super);
        function Sequence(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this._collectLetters = new Array();
            this._contenerLetter = new Phaser.Group(game);
            this._contenerLetter.y = 30;
            this._contenerLetter.x = 0;
            this.addChild(this._contenerLetter);
        }
        Sequence.prototype.addLetter = function (letter) {
            var style = { font: "40px Arial", fill: "#000000", align: "center" };
            var letterShow = new Phaser.Text(this.game, 0, 0, letter, style);
            letterShow.x = this._contenerLetter.width + 20;
            this._contenerLetter.addChild(letterShow);
            this._collectLetters.push(letter);
        };
        Sequence.prototype.removeLetters = function () {
            this._contenerLetter.removeAll();
            this._collectLetters.splice(0, this._collectLetters.length);
        };
        return Sequence;
    })(Phaser.Sprite);
    Rocket.Sequence = Sequence;
})(Rocket || (Rocket = {}));
//# sourceMappingURL=game.js.map