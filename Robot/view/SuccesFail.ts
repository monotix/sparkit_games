﻿module Robo {


    export class SuccesFail extends Phaser.Group {

        private _currentSucces: number=1;
        private _currentFail: number=1;
        private _currentAnim: Phaser.Sprite;
        private _game: Phaser.Game;
        private _gameControl: GameControler;
        constructor(game:Phaser.Game) {

            super(game);
            this._game = game;
            this._gameControl = GameControler.getInstance();

        }

        showSucces() {
            this.checkAnim();
            this._currentAnim = new Phaser.Sprite(this._game, 0, 0, "success" + this._currentSucces, 0);
            this.ceneter();
          
            this._currentAnim.animations.add("play");
            this._currentAnim.animations.getAnimation("play").onComplete.add(this.finishAnimSucces, this);
            this._currentAnim.animations.play("play", 24, false);
            this.add(this._currentAnim);

            this._currentSucces++;

        }

        finishAnimSucces() {

            this.game.time.events.add(Phaser.Timer.SECOND * 2, function () {



                // this._countDown.animations.play("countdownAnim", 24, false, false);

                this._gameControl.playAgain();
                this.checkAnim();

            }, this)
           
        }


        showFail() {
            this.checkAnim();
            this._currentAnim = new Phaser.Sprite(this._game, 0, 0, "fail0", 0);
            this.ceneter();
            this._currentAnim.animations.add("play");
            this._currentAnim.animations.getAnimation("play").onComplete.add(this.finishAnimFail, this);
            this._currentAnim.animations.play("play", 24, false);
            this.add(this._currentAnim);
            this._currentFail++;
        }

        finishAnimFail() {

            this.game.time.events.add(Phaser.Timer.SECOND * 2, function () {



                // this._countDown.animations.play("countdownAnim", 24, false, false);

                this._gameControl.showFinish();
                this.checkAnim();

            }, this)
           
        }


        ceneter() {

            this._currentAnim.x = this._game.width / 2 - this._currentAnim.width / 2;
            this._currentAnim.y = this._game.height / 2 - this._currentAnim.height / 2;
        }

        checkAnim() {

            if (this._currentAnim != null) {
                this.remove(this._currentAnim);
                this._currentAnim.kill();
                this._currentAnim = null;
            }


        }

        

    }

}



