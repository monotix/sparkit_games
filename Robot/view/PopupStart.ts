﻿declare function APIsetLevel(userID, gameID, userLevel);
declare function APIgetLevel(userID, gameID);
declare var userID: any;
declare var gameID: any;
function launchGame(result) {
   
    userLevel = result.level;
 
    if (userLevel > 5) {

        userLevel = 5;
        APIsetLevel(userID, gameID, userLevel);
    }


}

//declare function launchGame(result);

module Robo {
    export class PopupStart extends Phaser.State {

        private logo;
        private overButtonA: Phaser.Sound;
        create() {

         

            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var background: Phaser.Sprite = this.add.sprite(0, 0, 'mainMenuBg');

            var bgPopup: Phaser.Sprite = this.add.sprite(0, 0, "popup");
            this.logo = this.add.sprite(0, 0, "logoGame");
         

            this.logo.x = (bgPopup.width / 2 - this.logo.width / 2) ;
            this.logo.y = this.logo.height - 20;



            var text = "Fill the robots with as many parts as you can.\n";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
          
            var textDescription = this.add.text(0, 0, text, style);
             //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;

            textDescription.x = this.game.width / 2 - textDescription.wordWrapWidth/2 +30;
            textDescription.y = this.logo.y + this.logo.height + 140;


            this.textLevel();
            //this.beatText();
         

            this.addButtons();
        
        }

        addButtons() {
            var gamesBtn = this.add.button(0, 0, "games", this.gamesBtnClick, this, 1, 0);
            var playBtn = this.add.button(100, 0, "playGame", this.playBtnClick, this, 1, 0);

            var buttonsGr = this.add.group();

            buttonsGr.add(gamesBtn);
            buttonsGr.add(playBtn);


            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;

           

        


        }

        gamesBtnClick() {


            this.overButtonA.play();
            window.history.back();
        }

        playBtnClick() {

          
            this.game.state.start("Instruction", true, false);

            
        }

        beatText() {


            var text = "BEAT THIS LEVEL";
            //" 6";
            var style = { font: "bold 13px Arial", fill: "#660000", align: "center" };

            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
          

            textDescription.x = (this.game.width / 2) + (140) - textDescription.width/2;
            textDescription.y = this.logo.y + this.logo.height + 100;

            var numberText = "6";
            var styleNumber = { font: "bold 17px Arial", fill: "#990000", align: "center" };

            var numberShow = this.add.text(0, 0, numberText, styleNumber);

            numberShow.x = textDescription.x + textDescription.width / 2 - numberShow.width / 2;
            numberShow.y = textDescription.y + textDescription.height;


            var tunesText = "MORE TUNES TO WIN";

            var tunesShow = this.add.text(0, 0, tunesText, style);
            tunesShow.wordWrap = true;
            tunesShow.wordWrapWidth = 100;
            tunesShow.x = textDescription.x + textDescription.width / 2 - tunesShow.width / 2;
            tunesShow.y = numberShow.y + numberShow.height;

            var creditsNmText = "25";
            var styleCredits = { font: "bold 17px Arial", fill: "#ff0000", align: "center" };


            var credistNmShow = this.add.text(0, 0, creditsNmText, styleCredits);
            credistNmShow.x = tunesShow.x + tunesShow.width / 2 - credistNmShow.width / 2;
            credistNmShow.y = tunesShow.y + tunesShow.height;

            
            var creditsText = "CREDITS";

            var credistShow = this.add.text(0, 0, creditsText, style);
            credistShow.x = tunesShow.x +tunesShow.width/2 - credistShow.width/2;
            credistShow.y = credistNmShow.y + credistNmShow.height;



        }

        textCredits() {

            var text = "Beat this level";
            //" 6";
            var style = { font: "bold 14px Arial", fill: "#660000", align: "center" };

            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
          

            textDescription.x = (this.game.width / 2) + 257 / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 100;

            var numberText = "6";
            var styleNumber = { font: "bold 18px Arial", fill: "#990000", align: "center" };

            var numberShow = this.add.text(0, 0, numberText, styleNumber);

            numberShow.x = textDescription.x + textDescription.width / 2 - numberShow.width / 2;
            numberShow.y = textDescription.y + textDescription.height;

        }

        textLevel() {
            //APIgetLevel(userID, gameID);
         
            var text = "LEVEL "+userLevel;
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };

            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
          

            textDescription.x = this.game.width / 2 - textDescription.width/2
            textDescription.y = this.logo.y + this.logo.height + 25;

        }




    }
}

