﻿module Robo {

    export class Robot extends Phaser.Sprite {

        public bitmapData: Phaser.BitmapData;
        public _addPoint: number = 0;
        private _game: Phaser.Game;
        private _itemGroup: Array<ItemInfo>;
        private _gameControler: GameControler;
        constructor(game: Phaser.Game, x: number, y: number, key: string) {

            super(game, x, y, key);
            this._game = game;
            this._gameControler = GameControler.getInstance();
            this.bitmapData = game.make.bitmapData(this.width, this.height);
            this.bitmapData.draw(this);
          //  this.bitmapData.addToWorld();
          //  this.bitmapData.replaceRGB(0, 85, 255, 255, 0, 250, 40, 255,new Phaser.Rectangle(0,0,200,200));
           
          //  this.bitmapData.x = -200;
            this.bitmapData.update(0, 0,this.width, this.height);
      
            this._itemGroup = new Array();
          /*  var col: Phaser.Color = new Phaser.Color();
            col = this.bitmapData.getPixel32(5, 5);*/
       //     console.log("color", col);
           // 4280427119

            //this._itemGroup = this.game.add.group();
        }

        public checkColorRectangle(x_: number, y_: number, width_: number, height_: number):boolean {

           /* this.bitmapData = this._game.make.bitmapData(this.width, this.height);
            this.bitmapData.draw(this);
            this.bitmapData.update(0, 0, this.width, this.height);
            */
          

          
            x_ = Math.floor(x_ - width_ / 2);
            y_ = Math.floor(y_ - height_ / 2);
            var maxWidth = Math.floor(width_ + x_);
            var maxHeight = Math.floor(y_ + height_);
            
          
            if (!this.checkItems(new Phaser.Rectangle(x_, y_, Math.floor(width_), Math.floor(height_)))) return false;

            for (var i: number = x_; i < maxWidth; i++) {

                for (var j: number = y_; j < maxHeight; j++) {
                  
                    if (this.bitmapData.getPixel32(i, j) != 4280427119) {
                    //    console.log("color inny niz gowno", i, j, this.bitmapData.getPixel32(i, j));
                 //       this.bitmapData.setPixel32(i, j, 255, 255, 255, 1);
                        return false;
                  }


                }

            }
            var graphics = new Phaser.Graphics(this._game, 0,0);
            graphics.beginFill(0xFF3300);
            graphics.lineStyle(10, 0xffd900, 1);
    
            // draw a shape
            graphics.moveTo(x_, y_);
            graphics.lineTo(maxWidth, y_);
            graphics.lineTo(maxWidth, maxHeight);
            graphics.lineTo(x_, maxHeight);
            graphics.lineTo(x_, y_);
            
            graphics.endFill();
          // this.addChild(graphics);
        //    this.bitmapData = this._game.make.bitmapData(this.width, this.height);
        //   this.bitmapData.draw(this);
        //    this.bitmapData.update(0,0, this.width, this.height);

          //  this.bitmapData.add(this);

            return true;
        }

        addItem(item: ItemInfo) {

            item.anchor.setTo(0.5, 0.5);
            this.addChild(item);
            this._itemGroup.push(item);

        }

        checkItems(rect:Phaser.Rectangle): boolean {

            if (this._itemGroup.length > 0) {

                for (var i: number = 0; i < this._itemGroup.length; i++) {
                    var hmm = this._itemGroup[i];
                    var rect2: Phaser.Rectangle = new Phaser.Rectangle(hmm.x, hmm.y, hmm.width, hmm.height);
                    console.log("przecina sie", rect, rect2, Phaser.Rectangle.intersects(rect, rect2));
                    if (Phaser.Rectangle.intersects(rect, rect2)) return false;
                }


            }

            return true;

        }

        collides(a, b) {
        if (a != undefined) {
            return !(
                ((a.y + a.height) < (b.y)) ||
                (a.y > (b.y + b.height)) ||
                ((a.x + a.width) < b.x) ||
                (a.x > (b.x + b.width))
                );
         }
        }
        

        public addPoint() {

          
            if (this._addPoint < 2) {
                this._addPoint++;
                this._gameControler.addPointsKolo();
            }

        }


        public getPoint(): number {

            return this._addPoint;

        }





    }


}