﻿module Robo {

    export class Level extends Phaser.State {


        private _itemsGr: Phaser.Group;
        private _robotsGr: Phaser.Group;
        private _gameControl: GameControler;
        private _itemsControl: ItemsControler;
        private _robotsControl: RobotsControler;
        private _objectToDrag: ItemInfo;
        private _currentRobot: Robot;
        private _bg1: Phaser.Group;
        private _bg2: Phaser.Group;
        private _bg3: Phaser.Group;

        private _tasmaRobot: Phaser.Sprite;
        private _tasmaSkrzynia: Phaser.Sprite;

        private _acceptRobot: AcceptRobot;
        private _gwizdek: Phaser.Sprite;
        private _koloGr: Phaser.Group;
     
        private _roboboControler: RoboboControler;
        private _finishScreen: FinishScreen;
        public _succesFailScreen: SuccesFail;
        private helpGr;
        private _helpBtn;
        private _instructionShow;
        preload() {


        }

        create() {

            this._gameControl = GameControler.getInstance();
            this._gameControl.setLevel(this);
            this._gameControl.setLevelNum(userLevel);
            this._itemsControl = ItemsControler.getInstance();
            this._itemsControl.setGame(this.game);

            this._robotsControl = RobotsControler.getInstance();
            this._robotsControl.setGame(this.game);

            this._roboboControler = RoboboControler.getInstance();
            this._roboboControler.setGame(this.game);
           
         

            this._bg1 = this.add.group();
          
           

            this.add.image(0, 0, "bg1", 1, this._bg1);

            this._koloGr = this.game.add.group();
            this._acceptRobot = new AcceptRobot(this.game, 0, 75, "kolozebate");

            this._gameControl.setKoloZebate(this._acceptRobot);

            this._koloGr.add(this._acceptRobot);

            this._gwizdek = new Phaser.Sprite(this.game, 170, -50, "gwizdek");
            this._robotsControl.setGwizdek(this._gwizdek);
            

            var roboboby = this._roboboControler.setRobobo();

            roboboby.x = this.game.width - roboboby.width - 150;
            roboboby.y = 14;

            this._koloGr.add(this._gwizdek);

            this._koloGr.add(roboboby);
                //.addChild(this._acceptRobot);
            
            this._tasmaRobot = this.add.sprite(-100, 450, "tasmaroboty", 1);
            this._tasmaRobot.animations.add("tasmaPlay");
            this._tasmaRobot.animations.play("tasmaPlay", 24, true);
            this._bg2 = this.add.group();

            this.add.image(0, 0, "bg2", 1, this._bg2);
            this._tasmaSkrzynia = this.add.sprite(-120, 560, "tasmaskrzynia", 0);  
            this._tasmaSkrzynia.animations.add("skrzyniaPlay");
            this._tasmaSkrzynia.play("skrzyniaPlay",51,true);
           
           
            this._itemsGr = this.add.group();
            this._itemsGr.enableBody = true;
           // this._itemsGr.y = this.game.height - 200;

            this._robotsGr = this.game.add.group();
            this._robotsGr.enableBody = true;
            
           
            this.game.input.onUp.add(this.stageUp,this);
            this.moveItems();
            this.moveRobots();

            this._bg3 = this.add.group();
            this.add.image(0, 0, "bg3", 1, this._bg3);


            this._succesFailScreen = new SuccesFail(this.game);
            this.stage.addChild(this._succesFailScreen);
            this.game.time.events.add(Phaser.Timer.SECOND * 2, function () {



                // this._countDown.animations.play("countdownAnim", 24, false, false);



            }, this);


            this.helpGr = this.add.group();
            this.stage.addChild(this.helpGr);

            this._helpBtn = this.add.button(0, 0, "helpBtn", this.helpBtnClick, this, 1, 0, 0, 0, this.helpGr);
            this.helpGr.x = 0;// 800 - this.helpGr.width;
            this.helpGr.y = 600 - this.helpGr.height;

            this._helpBtn.buttonMode = true;
            this._helpBtn.inputEnabled = true;
            this._helpBtn.input.priorityID = 110;
            //this.showLetterMenu(new Array("a"));
           
            this.game.input.onDown.add(function () { if (this.game.paused) { this.game.paused = false; } if (this._instructionShow) { this.stage.removeChild(this._instructionShow); this._instructionShow.kill(); this._instructionShow = null; } }, this);
           


           // this._succesFailScreen.showSucces();
        }

        private helpBtnClick() {

            console.log("pokaz instrukcje");
            this.game.paused = !this.game.paused;

            if (this._instructionShow == null) this._instructionShow = this.add.image(0, 0, "firstInstruction", 0);
            

            // this.add.image(0, 0, "firstInstruction", 0);
            this._instructionShow.width = this.game.world.width;//this.stage.width;
            this._instructionShow.height = this.game.world.height;
            this.stage.addChild(this._instructionShow);

        }

        restart() {

            this._roboboControler.restart();
            this._robotsControl.restart();
            this.moveItems();
            this.moveRobots();


        }


        moveRobots() {
            

            if (!this._gameControl.finishGame) {
                var robot: Robot = this._robotsControl.getNextItem();

                if (robot != null) {

                    this._robotsGr.add(robot);
                    robot.body.velocity.x = this._gameControl.SPEEDROBOT;
                    robot.checkWorldBounds = true;
                    this._currentRobot = robot;
                    robot.events.onOutOfBounds.add(this.robotsOutOfScreen, this);

                }



            }


                // this._countDown.animations.play("countdownAnim", 24, false, false);
                //this.moveRobots();


       



        }

        moveItems() {

            if (!this._gameControl.finishGame) {

                var item: ItemToDrag = this._itemsControl.getNextItem();

                if (item != null) {

                    this._itemsGr.add(item);
                    item.body.velocity.x = this._gameControl.SPEEDITEM;
                    item.checkWorldBounds = true;
                    //  item.events.onOutOfBounds.add(this.moveItems, this);
                    item.y = this.game.height - item.height - 10;
                    item.events.onInputDown.add(this.check, this);
                }


                this.game.time.events.add(Phaser.Timer.SECOND * 3, function () {



                    // this._countDown.animations.play("countdownAnim", 24, false, false);
                    this.moveItems();


                }, this)

            }

        }

        robotsOutOfScreen(robot:Robot) {

            console.log("robot points ", robot.getPoint());
            this._roboboControler.checkRobobo(robot.getPoint());
            this._gameControl.minusPointsKolo(robot.getPoint());
          //  this.moveRobots();
        }

        check(item:ItemToDrag,pointer) {
            //console.log("check item", item);
            var spr = item.addObjectToDrag();
            //spr.inputEnabled = true;
            //spr.input.enableDrag(true);
           
           // spr.events.onDragStart.add(this.dragStart, this);
            //spr.x = pointer.x;
            //spr.y = pointer.y;
            this.stage.addChild(spr);
            this._objectToDrag = spr;

            this.game.physics.enable(spr, Phaser.Physics.ARCADE);

        }

        stageUp() {
            this.game.physics.arcade.overlap(this._objectToDrag, this._robotsGr, this.collision, this.iCoTO, this);
         //   this._objectToDrag = null;
          //  this._objectToDrag.kill();
           // console.log("stage up");
         
            this.checkIfNotAdded(this._objectToDrag);
        }
        
        checkIfNotAdded(object:ItemInfo=null) {

           
            if (object!=null && !object.isAdded){

                console.log("object niedodany ");
                object.kill();

            }
            else {
                console.log("object dodany ");

            }

        }


        iCoTO() {

           

        }

        collision(object:ItemInfo, object2:Robot) {

           // console.log("kolizja", object.x - object2.x, object.y - object2.y);
          //  console.log("jaki kolor", object2.bitmapData.getPixel32(Math.floor(object.x-object2.x), object.y-object2.y));
            var posx: number = Math.floor(object.x - object2.x);
            var posy: number = Math.floor(object.y - object2.y);
          //  this.input.getLocalPosition(object2,);
           // console.log("kolor kurwa", object2.bitmapData.getPixel32(posx, posy));
            if (object2.bitmapData.getPixel32(posx, posy) == 4280427119 && object2.checkColorRectangle(posx, posy, object.width, object.height)  ) {

                //&& object2.checkColorRectangle(posx,posy,object.width,object.height) 
             
                object.isAdded = true;
                object.x = posx;
                object.y = posy;
                object2.addItem(object);
                object2.addPoint();
               this._objectToDrag = null;

            }
        }


        public showFinish() {
            console.log("FInish screen:");
            if (this._finishScreen == null) this._finishScreen = new FinishScreen(this.game);
            this._finishScreen.x = this.game.width / 2 - this._finishScreen.width / 2;
            this._finishScreen.y = this.game.height / 2 - this._finishScreen.height / 2;
            this.stage.addChild(this._finishScreen);
           
        }
        
        update() {

            if (this._objectToDrag != null) {
               // var pos = this.stage.position.x
               
                this._objectToDrag.position.setTo(this.game.input.x, this.game.input.y);

            }
            if (this._currentRobot != null) {


                if (this._currentRobot.x > this.game.width - this._currentRobot.width / 2) {


                    this.moveRobots();
                }

            }


        }

    }


} 