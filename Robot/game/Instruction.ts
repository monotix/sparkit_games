﻿module Robo {
    export class Instruction extends Phaser.State {

        private _buttons: Phaser.Group;
        private _mainMenuBg: Phaser.Sound;
        private overButtonA: Phaser.Sound;
        create() {


            this.overButtonA = this.game.add.audio("overButtonA", 1, false);


            var background: Phaser.Sprite = this.add.sprite(0, 0, 'InstructionsScreen');
            this._buttons = this.add.group();

            var buttonsBg: Phaser.Sprite = this.add.sprite(0, 0, 'buttonsBg');
            this._buttons.addChild(buttonsBg);

            var gameButton = this.add.button(14, 36, 'buttons', this.onGameClick, this, 0, 0, 1);
        
            //gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            this._buttons.addChild(gameButton);

            var playButton = this.add.button(61, gameButton.y, 'buttons', this.onPlayClick, this, 3, 2);
            playButton.onInputDown.add(this.overPlay2, this);
         
            //playButton.x = this._buttons.width - playButton.width - 10;
            this._buttons.addChild(playButton);

            this._buttons.x = 0;
            this._buttons.y = this.game.stage.height - this._buttons.height;

        }


        onGameClick() {


            this.overButtonA.play();
            window.history.back();
        }

        private overPlay2() {

            this.overButtonA.play();


        }

        onPlayClick() {

            // this._mainMenuBg.stop();
            
            this.game.state.start("Level", true, false);
        }

    }

} 