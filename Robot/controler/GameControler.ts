﻿declare function APIsetLevel(userID, gameID, userLevel);
declare function APIgetLevel(userID, gameID);
declare var userID: any;
declare var gameID: any;

module Robo {

    export class GameControler {


        public static _instance: GameControler = null; 

        public SPEEDITEM: number = 100;
        public SPEEDROBOT: number = 100;
        private _koloZebate: AcceptRobot;
        private _pointsKolo: number=0;
        public finishGame: boolean = false;
        private _currenLevel: number = 0;
        private _level: Level;
        construct() {

            if (GameControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            GameControler._instance = this;

        }

        setKoloZebate(kolo:AcceptRobot) {

            this._koloZebate = kolo;

        }


        public static getInstance(): GameControler {

            if (GameControler._instance === null) {

                GameControler._instance = new GameControler();


            }


            return GameControler._instance;



        }


        public setLevelNum(userlevel) {

            this._currenLevel = userlevel - 1;
            this.SPEEDROBOT = this.SPEEDROBOT + (this._currenLevel * 10);
        }


        setLevel(level: Level) {

            this._level = level;

        }

        nextLevel(points: number) {

            if (points > 9 && this._currenLevel < 6) {

                console.log("kolejny level");
                this.SPEEDROBOT += 10;
                this._currenLevel++;
                userLevel = this._currenLevel + 1;
                APIsetLevel(userID, gameID, userLevel);
                this._level._succesFailScreen.showSucces();
            }

            else {


                console.log("you lose");
                this.SPEEDROBOT = 100;
                if (this._currenLevel < 6) {
                    this._level._succesFailScreen.showSucces();
                }
                else {
                    this._level._succesFailScreen.showFail();
                }
               
            //    this._level.showFinish();


            }



        }

        showFinish() {

            this._level.showFinish();

        }

        playAgain() {

            this.finishGame = false;
            this._pointsKolo = 0;
            this._level.restart();
        }

        addPointsKolo(point: number=1) {

            this._pointsKolo += point;
            console.log("addd Point");
            this.playKolo();
        }

        minusPointsKolo(point: number) {

            this._pointsKolo -= point;
            this.playKolo();
        }

        playKolo() {

            if (this._pointsKolo > 1) this._koloZebate.playAll();
            else if (this._pointsKolo > 0) this._koloZebate.playHalf();
            else this._koloZebate.playStart();

        }

    }


} 