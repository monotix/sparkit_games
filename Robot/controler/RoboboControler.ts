﻿module Robo {

    export class RoboboControler {

        private _game: Phaser.Game;
        public static _instance: RoboboControler = null;

        private _roboboGr: Phaser.Group;
        private _roboboArr: Array<Robobo>;
        private _currentRobobo: number = 0;
        
        construct() {

            if (RoboboControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            RoboboControler._instance = this;

        }


        public static getInstance(): RoboboControler {

            if (RoboboControler._instance === null) {

                RoboboControler._instance = new RoboboControler();


            }


            return RoboboControler._instance;



        }

        setRobobo(): Phaser.Group {
            this._roboboGr = new Phaser.Group(this._game);
            this._roboboArr = new Array();
            var marginX: number = 0;
            var marginY: number = 0;
            for (var i: number = 1; i < 13; i++) {

                var robobo: Robobo = new Robobo(this._game, 0, 0, "robobo");
                robobo.x = marginX;
                robobo.y = marginY;
                this._roboboArr.push(robobo);
                this._roboboGr.add(robobo);
                marginX += robobo.width + 5;
                if (i % 6 == 0) {
                    marginX = 0;
                    marginY += robobo.height + 5;

                }

            }

            return this._roboboGr;
        }

        setGame(game: Phaser.Game) {

            this._game = game;
            this.setRobobo();
        }

        checkRobobo(point:number) {

          
            if (this._currentRobobo == 11) {
                console.log("finishGame");
                GameControler.getInstance().finishGame = true;
                var poprawne: number = 0;
                for (var i: number = 0; i < 11; i++){

                    if (this._roboboArr[i].accepted) poprawne++;
                }
                this._roboboArr[this._currentRobobo].checkScore(point);
                GameControler.getInstance().nextLevel(poprawne);
            }
            else {


                this._roboboArr[this._currentRobobo].checkScore(point);
                this._currentRobobo++;

            }
            

        }


        restartRobobo() {

            for (var i: number = 0; i < 12; i++) {

                this._roboboArr[i].restart();
            }
        }

        restart() {

            this._currentRobobo = 0;
            this.restartRobobo();

        }


    }


}  