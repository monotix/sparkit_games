﻿module Robo {

    export class RobotsControler {


        public static _instance: RobotsControler = null;
        private robotsArr: Array<Robot>;
        private currentItem: number = 0;
        private maxItem: number = 0;
        private _game: Phaser.Game;
        private _gwizdek: Phaser.Sprite;
        construct() {

            if (RobotsControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            RobotsControler._instance = this;

        }


        public static getInstance(): RobotsControler {

            if (RobotsControler._instance === null) {

                RobotsControler._instance = new RobotsControler();


            }


            return RobotsControler._instance;



        }

        setGame(game: Phaser.Game) {

            this._game = game;

        }


        setItem() {

            this.robotsArr = new Array();
            for (var i: number = 0; i < 12; i++) {
                var robot: Robot = new Robot(this._game, 0, 0, "robot" + i);
                robot.y = this._game.height - robot.height - 130;
                robot.x = -robot.width;
                this.robotsArr.push(robot);
            }

            this.maxItem = this.robotsArr.length;

        }

        getNextItem(): Robot {

            if (this.robotsArr == null) this.setItem();
            if (this.currentItem == this.maxItem) return null;
            if (this.currentItem > 0) this.playGwizek();
            var item = this.robotsArr[this.currentItem];
            this.currentItem++;

            return item;

        }

        setGwizdek(gwizdek: Phaser.Sprite) {
            gwizdek.animations.add("gwizdekPlay");
           
            this._gwizdek = gwizdek;

        }

        playGwizek() {

            this._gwizdek.animations.play("gwizdekPlay", 24, false);
        }


        restart() {

            this.currentItem = 0;
            this.setItem();

        }

    }



} 