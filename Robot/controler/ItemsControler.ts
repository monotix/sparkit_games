﻿module Robo {

    export class ItemsControler {


        public static _instance: ItemsControler = null;

        private _game: Phaser.Game;
        private itemsArray: Array<ItemToDrag>;
        private currentItem: number = 0;
        private maxItem: number = 0;
        construct() {

            if (ItemsControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            ItemsControler._instance = this;
           
        }


        public static getInstance(): ItemsControler {

            if (ItemsControler._instance === null) {

                ItemsControler._instance = new ItemsControler();


            }


            return ItemsControler._instance;



        }
        setGame(game: Phaser.Game) {

            this._game = game;

        }


        setItem(){
           
            this.itemsArray = new Array();
            for (var i: number = 1; i < 4; i++) {
              
                this.itemsArray.push(new ItemToDrag(this._game, 0, 0, "skrzynka"));
            }

            this.maxItem = this.itemsArray.length;

        }

        getNextItem():ItemToDrag {

           /* if (this.itemsArray == null) this.setItem();
            if (this.currentItem == this.maxItem) return null;
            var item = this.itemsArray[this.currentItem];
            this.currentItem++;
            */
            

            return new ItemToDrag(this._game, 0, 0, "skrzynka");

        }


    }


} 