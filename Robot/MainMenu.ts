﻿module Robo {

    export class MainMenu extends Phaser.State {

        private overButtonA: Phaser.Sound;
        create() {


            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var _mainMenuBg = this.game.add.audio('MainMenuSn', 1, true);
            _mainMenuBg.volume = 0.1;
            _mainMenuBg.play();
            var gameControler = GameControler.getInstance();
            gameControler.setSound(_mainMenuBg);

            var background: Phaser.Sprite = this.add.sprite(0, 0, 'mainMenuBg');
            var _buttons = this.add.group();

            

            var gameButton = this.add.button(0, 0, "games", this.onGameClick, this, 1, 0);
          

        
            //gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            _buttons.addChild(gameButton);

            var playButton = this.add.button(100, 0, "playGame", this.onPlayClick, this, 1, 0);
            playButton.onInputDown.add(this.overPlay2, this);


            _buttons.addChild(playButton);

            _buttons.x = 0;
            _buttons.y = this.game.stage.height - _buttons.height;

        }

        private onGameClick() {

            this.overButtonA.play();
            window.history.back();
        }

        private overPlay2() {

            this.overButtonA.play();


        }



        onPlayClick() {


            this.game.state.start("PopupStart", true, false);

        }


    }


} 