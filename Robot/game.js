var Greeter = (function () {
    function Greeter(element) {
        this.element = element;
        this.element.innerHTML += "The time is: ";
        this.span = document.createElement('span');
        this.element.appendChild(this.span);
        this.span.innerText = new Date().toUTCString();
    }
    Greeter.prototype.start = function () {
        var _this = this;
        this.timerToken = setInterval(function () { return _this.span.innerHTML = new Date().toUTCString(); }, 500);
    };
    Greeter.prototype.stop = function () {
        clearTimeout(this.timerToken);
    };
    return Greeter;
})();
window.onload = function () {
    var el = document.getElementById('content');
    var greeter = new Greeter(el);
    greeter.start();
};
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Robo;
(function (Robo) {
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            _super.apply(this, arguments);
        }
        Boot.prototype.preload = function () {
            this.load.image("bg", "assets/SplashScreen2.png");
            this.load.image("progressBarBlack", "assets/preloader/ProgressBar.png");
            this.load.image("progressBarBlue", "assets/preloader/ProgressBarBlue.png");
        };
        Boot.prototype.create = function () {
            this.game.state.start("Preloader", true, false);
        };
        return Boot;
    })(Phaser.State);
    Robo.Boot = Boot;
})(Robo || (Robo = {}));
var Robo;
(function (Robo) {
    var GameControler = (function () {
        function GameControler() {
            this.SPEEDITEM = 100;
            this.SPEEDROBOT = 100;
            this._pointsKolo = 0;
            this.finishGame = false;
            this._currenLevel = 0;
        }
        GameControler.prototype.construct = function () {
            if (GameControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            GameControler._instance = this;
        };
        GameControler.prototype.setKoloZebate = function (kolo) {
            this._koloZebate = kolo;
        };
        GameControler.getInstance = function () {
            if (GameControler._instance === null) {
                GameControler._instance = new GameControler();
            }
            return GameControler._instance;
        };
        GameControler.prototype.setLevelNum = function (userlevel) {
            this._currenLevel = userlevel - 1;
            this.SPEEDROBOT = this.SPEEDROBOT + (this._currenLevel * 10);
        };
        GameControler.prototype.setLevel = function (level) {
            this._level = level;
            this._robotsControl = Robo.RobotsControler.getInstance();
        };
        GameControler.prototype.nextLevel = function (points) {
            console.log("poprawne", points);
            if (points > 9 && this._currenLevel < 6) {
                this._robotsControl.removeRobots();
                console.log("kolejny level");
                this.SPEEDROBOT += 10;
                this._currenLevel++;
                userLevel = this._currenLevel + 1;
                APIsetLevel(userID, gameID, userLevel);
                this._level._succesFailScreen.showSucces();
            }
            else {
                console.log("you lose");
                //this.SPEEDROBOT = 100;
                if (this._currenLevel < 6) {
                    this._robotsControl.removeRobots();
                    this._level._succesFailScreen.showFail();
                }
                else {
                    this._level._succesFailScreen.showFail();
                }
            }
        };
        GameControler.prototype.showFinish = function () {
            this._level.showFinish();
        };
        GameControler.prototype.playAgain = function () {
            this.finishGame = false;
            this._pointsKolo = 0;
            this._level.restart();
        };
        GameControler.prototype.addPointsKolo = function (point) {
            if (point === void 0) { point = 1; }
            this._pointsKolo += point;
            console.log("addd Point");
            this.playKolo();
        };
        GameControler.prototype.minusPointsKolo = function (point) {
            this._pointsKolo -= point;
            this.playKolo();
        };
        GameControler.prototype.playKolo = function () {
            if (this._pointsKolo > 1)
                this._koloZebate.playAll();
            else if (this._pointsKolo > 0)
                this._koloZebate.playHalf();
            else
                this._koloZebate.playStart();
        };
        GameControler.prototype.setSound = function (sound_) {
            this.mainSound = sound_;
        };
        GameControler._instance = null;
        return GameControler;
    })();
    Robo.GameControler = GameControler;
})(Robo || (Robo = {}));
var Robo;
(function (Robo) {
    var ItemsControler = (function () {
        function ItemsControler() {
            this.currentItem = 0;
            this.maxItem = 0;
        }
        ItemsControler.prototype.construct = function () {
            if (ItemsControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            ItemsControler._instance = this;
        };
        ItemsControler.getInstance = function () {
            if (ItemsControler._instance === null) {
                ItemsControler._instance = new ItemsControler();
            }
            return ItemsControler._instance;
        };
        ItemsControler.prototype.setGame = function (game) {
            this._game = game;
        };
        ItemsControler.prototype.setItem = function () {
            this.itemsArray = new Array();
            for (var i = 1; i < 4; i++) {
                this.itemsArray.push(new Robo.ItemToDrag(this._game, 0, 0, "skrzynka"));
            }
            this.maxItem = this.itemsArray.length;
        };
        ItemsControler.prototype.getNextItem = function () {
            /* if (this.itemsArray == null) this.setItem();
             if (this.currentItem == this.maxItem) return null;
             var item = this.itemsArray[this.currentItem];
             this.currentItem++;
             */
            return new Robo.ItemToDrag(this._game, 0, 0, "skrzynka");
        };
        ItemsControler._instance = null;
        return ItemsControler;
    })();
    Robo.ItemsControler = ItemsControler;
})(Robo || (Robo = {}));
var Robo;
(function (Robo) {
    var RoboboControler = (function () {
        function RoboboControler() {
            this._currentRobobo = 0;
        }
        RoboboControler.prototype.construct = function () {
            if (RoboboControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            RoboboControler._instance = this;
        };
        RoboboControler.getInstance = function () {
            if (RoboboControler._instance === null) {
                RoboboControler._instance = new RoboboControler();
            }
            return RoboboControler._instance;
        };
        RoboboControler.prototype.setRobobo = function () {
            this._roboboGr = new Phaser.Group(this._game);
            this._roboboArr = new Array();
            var marginX = 0;
            var marginY = 0;
            for (var i = 1; i < 13; i++) {
                var robobo = new Robo.Robobo(this._game, 0, 0, "robobo");
                robobo.x = marginX;
                robobo.y = marginY;
                this._roboboArr.push(robobo);
                this._roboboGr.add(robobo);
                marginX += robobo.width + 5;
                if (i % 6 == 0) {
                    marginX = 0;
                    marginY += robobo.height + 5;
                }
            }
            return this._roboboGr;
        };
        RoboboControler.prototype.setGame = function (game) {
            this._game = game;
            this.setRobobo();
        };
        RoboboControler.prototype.checkRobobo = function (point) {
            if (this._currentRobobo == 11) {
                console.log("finishGame");
                Robo.GameControler.getInstance().finishGame = true;
                var poprawne = 0;
                for (var i = 0; i < 11; i++) {
                    if (this._roboboArr[i].accepted)
                        poprawne++;
                }
                this._roboboArr[this._currentRobobo].checkScore(point);
                Robo.GameControler.getInstance().nextLevel(poprawne);
            }
            else {
                this._roboboArr[this._currentRobobo].checkScore(point);
                this._currentRobobo++;
            }
        };
        RoboboControler.prototype.restartRobobo = function () {
            for (var i = 0; i < 12; i++) {
                this._roboboArr[i].restart();
            }
        };
        RoboboControler.prototype.restart = function () {
            this._currentRobobo = 0;
            this.restartRobobo();
        };
        RoboboControler._instance = null;
        return RoboboControler;
    })();
    Robo.RoboboControler = RoboboControler;
})(Robo || (Robo = {}));
var Robo;
(function (Robo) {
    var RobotsControler = (function () {
        function RobotsControler() {
            this.currentItem = 0;
            this.maxItem = 0;
        }
        RobotsControler.prototype.construct = function () {
            if (RobotsControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            RobotsControler._instance = this;
        };
        RobotsControler.getInstance = function () {
            if (RobotsControler._instance === null) {
                RobotsControler._instance = new RobotsControler();
            }
            return RobotsControler._instance;
        };
        RobotsControler.prototype.setGame = function (game) {
            this._game = game;
        };
        RobotsControler.prototype.setItem = function () {
            this.robotsArr = new Array();
            for (var i = 0; i < 12; i++) {
                var robot = new Robo.Robot(this._game, 0, 0, "robot" + i);
                robot.y = this._game.height - robot.height - 130;
                robot.x = -robot.width;
                this.robotsArr.push(robot);
            }
            this.maxItem = this.robotsArr.length;
        };
        RobotsControler.prototype.getNextItem = function () {
            if (this.robotsArr == null)
                this.setItem();
            if (this.currentItem == this.maxItem)
                return null;
            if (this.currentItem > 0)
                this.playGwizek();
            var item = this.robotsArr[this.currentItem];
            this.currentItem++;
            return item;
        };
        RobotsControler.prototype.removeRobots = function () {
            for (var i = 0; i < this.robotsArr.length; i++) {
                this.robotsArr[i].kill();
            }
        };
        RobotsControler.prototype.setGwizdek = function (gwizdek) {
            gwizdek.animations.add("gwizdekPlay");
            this._gwizdek = gwizdek;
        };
        RobotsControler.prototype.playGwizek = function () {
            this._gwizdek.animations.play("gwizdekPlay", 24, false);
        };
        RobotsControler.prototype.restart = function () {
            this.currentItem = 0;
            this.setItem();
        };
        RobotsControler._instance = null;
        return RobotsControler;
    })();
    Robo.RobotsControler = RobotsControler;
})(Robo || (Robo = {}));
var Robo;
(function (Robo) {
    var Instruction = (function (_super) {
        __extends(Instruction, _super);
        function Instruction() {
            _super.apply(this, arguments);
        }
        Instruction.prototype.create = function () {
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var instruction = this.add.image(0, 0, "firstInstruction", 0);
            instruction.width = this.game.world.width; //this.stage.width;
            instruction.height = this.game.world.height; //this.stage.height;
            this._mainMenuBg = this.game.add.audio('instructionSn', 1, true);
            this._mainMenuBg.play("", 0, 1, false);
            var gameControler = Robo.GameControler.getInstance();
            gameControler.mainSound.volume = 0.05;
            var background = this.add.sprite(0, 0, 'InstructionsScreen');
            this._buttons = this.add.group();
            var gameButton = this.add.button(0, 0, "games", this.onGameClick, this, 1, 0);
            //gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            this._buttons.addChild(gameButton);
            var playButton = this.add.button(100, 0, "playGame", this.onPlayClick, this, 1, 0);
            playButton.onInputDown.add(this.overPlay2, this);
            //playButton.x = this._buttons.width - playButton.width - 10;
            this._buttons.addChild(playButton);
            this._buttons.x = 0;
            this._buttons.y = this.game.stage.height - this._buttons.height;
        };
        Instruction.prototype.onGameClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        Instruction.prototype.overPlay2 = function () {
            this.overButtonA.play();
        };
        Instruction.prototype.onPlayClick = function () {
            // this._mainMenuBg.stop();
            this._mainMenuBg.stop();
            this.game.state.start("Level", true, false);
        };
        return Instruction;
    })(Phaser.State);
    Robo.Instruction = Instruction;
})(Robo || (Robo = {}));
var Robo;
(function (Robo) {
    var Level = (function (_super) {
        __extends(Level, _super);
        function Level() {
            _super.apply(this, arguments);
        }
        Level.prototype.preload = function () {
        };
        Level.prototype.create = function () {
            this._gameControl = Robo.GameControler.getInstance();
            this._gameControl.setLevel(this);
            this._gameControl.setLevelNum(userLevel);
            this._itemsControl = Robo.ItemsControler.getInstance();
            this._itemsControl.setGame(this.game);
            this._robotsControl = Robo.RobotsControler.getInstance();
            this._robotsControl.setGame(this.game);
            this._roboboControler = Robo.RoboboControler.getInstance();
            this._roboboControler.setGame(this.game);
            this._bg1 = this.add.group();
            this.add.image(0, 0, "bg1", 1, this._bg1);
            this._koloGr = this.game.add.group();
            this._acceptRobot = new Robo.AcceptRobot(this.game, 0, 75, "kolozebate");
            this._gameControl.setKoloZebate(this._acceptRobot);
            this._koloGr.add(this._acceptRobot);
            this._gwizdek = new Phaser.Sprite(this.game, 170, -50, "gwizdek");
            this._robotsControl.setGwizdek(this._gwizdek);
            var roboboby = this._roboboControler.setRobobo();
            roboboby.x = this.game.width - roboboby.width - 150;
            roboboby.y = 14;
            this._koloGr.add(this._gwizdek);
            this._koloGr.add(roboboby);
            //.addChild(this._acceptRobot);
            this._tasmaRobot = this.add.sprite(-100, 450, "tasmaroboty", 1);
            this._tasmaRobot.animations.add("tasmaPlay");
            this._tasmaRobot.animations.play("tasmaPlay", 24, true);
            this._bg2 = this.add.group();
            this.add.image(0, 0, "bg2", 1, this._bg2);
            this._tasmaSkrzynia = this.add.sprite(-120, 560, "tasmaskrzynia", 0);
            this._tasmaSkrzynia.animations.add("skrzyniaPlay");
            this._tasmaSkrzynia.play("skrzyniaPlay", 51, true);
            this._itemsGr = this.add.group();
            this._itemsGr.enableBody = true;
            // this._itemsGr.y = this.game.height - 200;
            this._robotsGr = this.game.add.group();
            this._robotsGr.enableBody = true;
            this.game.input.onUp.add(this.stageUp, this);
            this.moveItems();
            this.moveRobots();
            this._bg3 = this.add.group();
            this.add.image(0, 0, "bg3", 1, this._bg3);
            this._succesFailScreen = new Robo.SuccesFail(this.game);
            this.stage.addChild(this._succesFailScreen);
            this.game.time.events.add(Phaser.Timer.SECOND * 2, function () {
                // this._countDown.animations.play("countdownAnim", 24, false, false);
            }, this);
            this.helpGr = this.add.group();
            this.stage.addChild(this.helpGr);
            this._helpBtn = this.add.button(0, 0, "helpBtn", this.helpBtnClick, this, 1, 0, 0, 0, this.helpGr);
            this.helpGr.x = 0; // 800 - this.helpGr.width;
            this.helpGr.y = 600 - this.helpGr.height;
            this._helpBtn.buttonMode = true;
            this._helpBtn.inputEnabled = true;
            this._helpBtn.input.priorityID = 110;
            //this.showLetterMenu(new Array("a"));
            this.game.input.onDown.add(function () {
                if (this.game.paused) {
                    this.game.paused = false;
                }
                if (this._instructionShow) {
                    this.stage.removeChild(this._instructionShow);
                    this._instructionShow.kill();
                    this._instructionShow = null;
                }
            }, this);
            // this._succesFailScreen.showSucces();
        };
        Level.prototype.helpBtnClick = function () {
            console.log("pokaz instrukcje");
            this.game.paused = !this.game.paused;
            if (this._instructionShow == null)
                this._instructionShow = this.add.image(0, 0, "firstInstruction", 0);
            // this.add.image(0, 0, "firstInstruction", 0);
            this._instructionShow.width = this.game.world.width; //this.stage.width;
            this._instructionShow.height = this.game.world.height;
            this.stage.addChild(this._instructionShow);
        };
        Level.prototype.restart = function () {
            this._roboboControler.restart();
            this._robotsControl.restart();
            this.moveItems();
            this.moveRobots();
        };
        Level.prototype.moveRobots = function () {
            if (!this._gameControl.finishGame) {
                var robot = this._robotsControl.getNextItem();
                if (robot != null) {
                    this._robotsGr.add(robot);
                    robot.body.velocity.x = this._gameControl.SPEEDROBOT;
                    robot.checkWorldBounds = true;
                    this._currentRobot = robot;
                    robot.events.onOutOfBounds.add(this.robotsOutOfScreen, this);
                }
            }
            // this._countDown.animations.play("countdownAnim", 24, false, false);
            //this.moveRobots();
        };
        Level.prototype.moveItems = function () {
            if (!this._gameControl.finishGame) {
                var item = this._itemsControl.getNextItem();
                if (item != null) {
                    this._itemsGr.add(item);
                    item.body.velocity.x = this._gameControl.SPEEDITEM;
                    item.checkWorldBounds = true;
                    //  item.events.onOutOfBounds.add(this.moveItems, this);
                    item.y = this.game.height - item.height - 10;
                    item.events.onInputDown.add(this.check, this);
                }
                this.game.time.events.add(Phaser.Timer.SECOND * 3, function () {
                    // this._countDown.animations.play("countdownAnim", 24, false, false);
                    this.moveItems();
                }, this);
            }
        };
        Level.prototype.robotsOutOfScreen = function (robot) {
            console.log("robot points ", robot.getPoint());
            this._roboboControler.checkRobobo(robot.getPoint());
            this._gameControl.minusPointsKolo(robot.getPoint());
            //  this.moveRobots();
        };
        Level.prototype.check = function (item, pointer) {
            //console.log("check item", item);
            var spr = item.addObjectToDrag();
            //spr.inputEnabled = true;
            //spr.input.enableDrag(true);
            // spr.events.onDragStart.add(this.dragStart, this);
            //spr.x = pointer.x;
            //spr.y = pointer.y;
            this.stage.addChild(spr);
            this._objectToDrag = spr;
            this.game.physics.enable(spr, Phaser.Physics.ARCADE);
        };
        Level.prototype.stageUp = function () {
            this.game.physics.arcade.overlap(this._objectToDrag, this._robotsGr, this.collision, this.iCoTO, this);
            //   this._objectToDrag = null;
            //  this._objectToDrag.kill();
            // console.log("stage up");
            this.checkIfNotAdded(this._objectToDrag);
        };
        Level.prototype.checkIfNotAdded = function (object) {
            if (object === void 0) { object = null; }
            if (object != null && !object.isAdded) {
                console.log("object niedodany ");
                object.kill();
            }
            else {
                console.log("object dodany ");
            }
        };
        Level.prototype.iCoTO = function () {
        };
        Level.prototype.collision = function (object, object2) {
            // console.log("kolizja", object.x - object2.x, object.y - object2.y);
            //  console.log("jaki kolor", object2.bitmapData.getPixel32(Math.floor(object.x-object2.x), object.y-object2.y));
            var posx = Math.floor(object.x - object2.x);
            var posy = Math.floor(object.y - object2.y);
            //  this.input.getLocalPosition(object2,);
            // console.log("kolor kurwa", object2.bitmapData.getPixel32(posx, posy));
            if (object2.bitmapData.getPixel32(posx, posy) == 4280427119 && object2.checkColorRectangle(posx, posy, object.width, object.height)) {
                //&& object2.checkColorRectangle(posx,posy,object.width,object.height) 
                object.isAdded = true;
                object.x = posx;
                object.y = posy;
                object2.addItem(object);
                object2.addPoint();
                this._objectToDrag = null;
            }
        };
        Level.prototype.showFinish = function () {
            console.log("FInish screen:");
            if (this._finishScreen == null)
                this._finishScreen = new Robo.FinishScreen(this.game);
            //  this._finishScreen.x = this.game.width / 2 - this._finishScreen.width / 2;
            //   this._finishScreen.y = this.game.height / 2 - this._finishScreen.height / 2;
            this._finishScreen.visible = true;
            this.stage.addChild(this._finishScreen);
        };
        Level.prototype.update = function () {
            if (this._objectToDrag != null) {
                // var pos = this.stage.position.x
                this._objectToDrag.position.setTo(this.game.input.x, this.game.input.y);
            }
            if (this._currentRobot != null) {
                if (this._currentRobot.x > this.game.width - this._currentRobot.width / 2) {
                    this.moveRobots();
                }
            }
        };
        return Level;
    })(Phaser.State);
    Robo.Level = Level;
})(Robo || (Robo = {}));
var Robo;
(function (Robo) {
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            _super.call(this, 800, 600, Phaser.AUTO, "content", null);
            this.state.add("Boot", Robo.Boot, false);
            this.state.add("Preloader", Robo.Preloader, false);
            this.state.add("MainMenu", Robo.MainMenu, false);
            this.state.add("PopupStart", Robo.PopupStart, false);
            this.state.add("Instruction", Robo.Instruction, false);
            this.state.add("Level", Robo.Level, false);
            this.state.start("Boot");
        }
        return Game;
    })(Phaser.Game);
    Robo.Game = Game;
})(Robo || (Robo = {}));
window.onload = function () {
    var game = new Robo.Game;
};
var userLevel = 0;
var Robo;
(function (Robo) {
    var MainMenu = (function (_super) {
        __extends(MainMenu, _super);
        function MainMenu() {
            _super.apply(this, arguments);
        }
        MainMenu.prototype.create = function () {
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var _mainMenuBg = this.game.add.audio('MainMenuSn', 1, true);
            _mainMenuBg.volume = 0.1;
            _mainMenuBg.play();
            var gameControler = Robo.GameControler.getInstance();
            gameControler.setSound(_mainMenuBg);
            var background = this.add.sprite(0, 0, 'mainMenuBg');
            var _buttons = this.add.group();
            var gameButton = this.add.button(0, 0, "games", this.onGameClick, this, 1, 0);
            //gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            _buttons.addChild(gameButton);
            var playButton = this.add.button(100, 0, "playGame", this.onPlayClick, this, 1, 0);
            playButton.onInputDown.add(this.overPlay2, this);
            _buttons.addChild(playButton);
            _buttons.x = 0;
            _buttons.y = this.game.stage.height - _buttons.height;
        };
        MainMenu.prototype.onGameClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        MainMenu.prototype.overPlay2 = function () {
            this.overButtonA.play();
        };
        MainMenu.prototype.onPlayClick = function () {
            this.game.state.start("PopupStart", true, false);
        };
        return MainMenu;
    })(Phaser.State);
    Robo.MainMenu = MainMenu;
})(Robo || (Robo = {}));
var Robo;
(function (Robo) {
    var AcceptRobot = (function (_super) {
        __extends(AcceptRobot, _super);
        function AcceptRobot(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this._game = game;
            //this.animations.add("polowa",this.countFrames(1, 57));
            //   this.animations.add("calosc",this.countFrames(58,109));
        }
        AcceptRobot.prototype.playStart = function () {
            this.frame = 0;
        };
        AcceptRobot.prototype.playHalf = function () {
            console.log("polowa");
            //  this.animations.play("polowa", 24, false);
            this.frame = 57;
        };
        AcceptRobot.prototype.playAll = function () {
            console.log("calosc");
            //this.animations.play("calosc", 24, false);
            this.frame = 109;
        };
        AcceptRobot.prototype.countFrames = function (start, num) {
            var countArr = new Array();
            for (var i = start; i < num + 1; i++) {
                countArr.push(i);
            }
            return countArr;
        };
        return AcceptRobot;
    })(Phaser.Sprite);
    Robo.AcceptRobot = AcceptRobot;
})(Robo || (Robo = {}));
var Robo;
(function (Robo) {
    var ItemInfo = (function (_super) {
        __extends(ItemInfo, _super);
        function ItemInfo(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this.isAdded = false;
            this._game = game;
        }
        return ItemInfo;
    })(Phaser.Sprite);
    Robo.ItemInfo = ItemInfo;
})(Robo || (Robo = {}));
var Robo;
(function (Robo) {
    var ItemToDrag = (function (_super) {
        __extends(ItemToDrag, _super);
        function ItemToDrag(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this._game = game;
            var random = Math.floor(Math.random() * 12);
            this._whichItem = random;
            //  var skrzynia: Phaser.Sprite = new Phaser.Sprite(game, 0, 0, "skrzynka", 0);
            //  this.addChild(skrzynia);
            var skrzyniawyglad = new Phaser.Sprite(game, 0, 0, "skrzynkaWyglad", random);
            // skrzyniawyglad.scale.setTo(0.5, 0.5);
            skrzyniawyglad.y = -30;
            this.addChild(skrzyniawyglad);
            // this.toDrag = new ItemInfo(game, 0, 0, "item11");
            this.inputEnabled = true;
        }
        ItemToDrag.prototype.addObjectToDrag = function () {
            var spr = new Robo.ItemInfo(this._game, 0, 0, "itemDrag" + this._whichItem);
            spr.anchor.setTo(0.5, 0.5);
            return spr;
        };
        return ItemToDrag;
    })(Phaser.Sprite);
    Robo.ItemToDrag = ItemToDrag;
})(Robo || (Robo = {}));
var Robo;
(function (Robo) {
    var Robobo = (function (_super) {
        __extends(Robobo, _super);
        function Robobo(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this.accepted = false;
            this._game = game;
        }
        Robobo.prototype.checkScore = function (items) {
            console.log("ileeee ", items);
            if (items < 2) {
                this.frame = 2;
            }
            else {
                this.frame = 1;
                this.accepted = true;
            }
        };
        Robobo.prototype.restart = function () {
            this.frame = 0;
            this.accepted = false;
        };
        return Robobo;
    })(Phaser.Sprite);
    Robo.Robobo = Robobo;
})(Robo || (Robo = {}));
var Robo;
(function (Robo) {
    var Robot = (function (_super) {
        __extends(Robot, _super);
        function Robot(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this._addPoint = 0;
            this._game = game;
            this._gameControler = Robo.GameControler.getInstance();
            this.bitmapData = game.make.bitmapData(this.width, this.height);
            this.bitmapData.draw(this);
            //  this.bitmapData.addToWorld();
            //  this.bitmapData.replaceRGB(0, 85, 255, 255, 0, 250, 40, 255,new Phaser.Rectangle(0,0,200,200));
            //  this.bitmapData.x = -200;
            this.bitmapData.update(0, 0, this.width, this.height);
            this._itemGroup = new Array();
            /*  var col: Phaser.Color = new Phaser.Color();
              col = this.bitmapData.getPixel32(5, 5);*/
            //     console.log("color", col);
            // 4280427119
            //this._itemGroup = this.game.add.group();
        }
        Robot.prototype.checkColorRectangle = function (x_, y_, width_, height_) {
            /* this.bitmapData = this._game.make.bitmapData(this.width, this.height);
             this.bitmapData.draw(this);
             this.bitmapData.update(0, 0, this.width, this.height);
             */
            x_ = Math.floor(x_ - width_ / 2);
            y_ = Math.floor(y_ - height_ / 2);
            var maxWidth = Math.floor(width_ + x_);
            var maxHeight = Math.floor(y_ + height_);
            if (!this.checkItems(new Phaser.Rectangle(x_, y_, Math.floor(width_), Math.floor(height_))))
                return false;
            for (var i = x_; i < maxWidth; i++) {
                for (var j = y_; j < maxHeight; j++) {
                    if (this.bitmapData.getPixel32(i, j) != 4280427119) {
                        //    console.log("color inny niz gowno", i, j, this.bitmapData.getPixel32(i, j));
                        //       this.bitmapData.setPixel32(i, j, 255, 255, 255, 1);
                        return false;
                    }
                }
            }
            var graphics = new Phaser.Graphics(this._game, 0, 0);
            graphics.beginFill(0xFF3300);
            graphics.lineStyle(10, 0xffd900, 1);
            // draw a shape
            graphics.moveTo(x_, y_);
            graphics.lineTo(maxWidth, y_);
            graphics.lineTo(maxWidth, maxHeight);
            graphics.lineTo(x_, maxHeight);
            graphics.lineTo(x_, y_);
            graphics.endFill();
            // this.addChild(graphics);
            //    this.bitmapData = this._game.make.bitmapData(this.width, this.height);
            //   this.bitmapData.draw(this);
            //    this.bitmapData.update(0,0, this.width, this.height);
            //  this.bitmapData.add(this);
            return true;
        };
        Robot.prototype.addItem = function (item) {
            item.anchor.setTo(0.5, 0.5);
            this.addChild(item);
            this._itemGroup.push(item);
        };
        Robot.prototype.checkItems = function (rect) {
            if (this._itemGroup.length > 0) {
                for (var i = 0; i < this._itemGroup.length; i++) {
                    var hmm = this._itemGroup[i];
                    var rect2 = new Phaser.Rectangle(hmm.x, hmm.y, hmm.width, hmm.height);
                    console.log("przecina sie", rect, rect2, Phaser.Rectangle.intersects(rect, rect2));
                    if (Phaser.Rectangle.intersects(rect, rect2))
                        return false;
                }
            }
            return true;
        };
        Robot.prototype.collides = function (a, b) {
            if (a != undefined) {
                return !(((a.y + a.height) < (b.y)) || (a.y > (b.y + b.height)) || ((a.x + a.width) < b.x) || (a.x > (b.x + b.width)));
            }
        };
        Robot.prototype.addPoint = function () {
            if (this._addPoint < 2) {
                this._addPoint++;
                this._gameControler.addPointsKolo();
            }
        };
        Robot.prototype.getPoint = function () {
            return this._addPoint;
        };
        return Robot;
    })(Phaser.Sprite);
    Robo.Robot = Robot;
})(Robo || (Robo = {}));
var Robo;
(function (Robo) {
    var Preloader = (function (_super) {
        __extends(Preloader, _super);
        function Preloader() {
            _super.apply(this, arguments);
        }
        Preloader.prototype.preload = function () {
            var bg = this.add.image(0, 0, "bg", 0);
            var blackProgress = this.add.image(0, 0, "progressBarBlack", 0);
            var blueProgress = this.add.sprite(0, 0, "progressBarBlue", 0);
            blackProgress.position.setTo(this.stage.width / 2 - blackProgress.width / 2, this.stage.height - blackProgress.height - 10);
            blueProgress.position = blackProgress.position;
            this.load.setPreloadSprite(blueProgress);
            this.load.image("InstructionsScreen", "assets/InstructionScreen.png");
            this.load.image("mainMenuBg", "assets/mainMenu/SplashScreen2.png");
            this.load.atlasJSONArray('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');
            this.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
            this.load.audio("MainMenuSn", "assets/audio/ScubaDudeTheme.mp3");
            this.load.audio("overButtonA", "assets/audio/overButton.mp3");
            this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");
            var mobile;
            mobile = false;
            if (!this.game.device.desktop) {
                mobile = true;
            }
            if (mobile) {
                this.load.image("firstInstruction", "assets/03_scate_create.png");
                this.load.audio("instructionSn", "assets/audio/skate_create_02.mp3");
            }
            else {
                this.load.image("firstInstruction", "assets/02_scate_create.png");
                this.load.audio("instructionSn", "assets/audio/skate_create_01.mp3");
            }
            this.load.image("popup", "assets/popup/popup.png");
            this.load.image("logoGame", "assets/popup/logo.png");
            this.load.atlasJSONArray('games', 'assets/popup/games.png', 'assets/popup/games.json');
            this.load.atlasJSONArray('playGame', 'assets/popup/play.png', 'assets/popup/play.json');
            this.load.atlasJSONArray('helpBtn', 'assets/popup/help.png', 'assets/popup/help.json');
            for (var i = 1; i < 5; i++) {
                this.load.audio("fail" + i, "assets/audio/instruction/fail" + i + ".mp3");
            }
            for (var i = 1; i < 5; i++) {
                this.load.audio("success" + i, "assets/audio/instruction/success" + i + ".mp3");
            }
            this.load.image("item11", "assets/robo/item11.PNG");
            /*    for (var i: number = 1; i < 4; i++) {
                    this.load.image("item"+i, "assets/robo/item"+i+".PNG");
                }
    
                for (var i: number = 1; i < 4; i++) {
                    this.load.image("robot" + i, "assets/robo/robot" + i + ".PNG");
                }
                */
            this.load.image("bg1", "assets/robo/stage/bg_level0.png");
            this.load.image("bg2", "assets/robo/stage/bg_level1.png");
            this.load.image("bg3", "assets/robo/stage/bg_level2.png");
            for (var i = 0; i < 13; i++) {
                this.load.image("itemDrag" + i, "assets/robo/item/s" + i + ".png");
            }
            this.load.image("skrzynka", "assets/robo/item/skrzynka.png");
            this.load.atlasJSONArray("skrzynkaWyglad", "assets/robo/item/shapes.png", "assets/robo/item/shapes.json");
            for (var i = 0; i < 12; i++) {
                this.load.image("robot" + i, "assets/robo/robots/robot" + i + ".png");
            }
            /*  end roboty */
            this.load.atlasJSONArray("tasmaskrzynia", "assets/robo/stage/tasma_skrzynki.png", "assets/robo/stage/tasma_skrzynki.json");
            this.load.atlasJSONArray("tasmaroboty", "assets/robo/stage/tasma.png", "assets/robo/stage/tasma.json");
            this.load.atlasJSONArray("kolozebate", "assets/robo/stage/koloZebate.png", "assets/robo/stage/koloZebate.json");
            this.load.atlasJSONArray("gwizdek", "assets/robo/stage/gwizdek.png", "assets/robo/stage/gwizdek.json");
            this.load.atlasJSONArray("robobo", "assets/robo/stage/robobobo.png", "assets/robo/stage/robobobo.json");
            this.load.atlasJSONArray("scoreScreen", "assets/scoreScreen/scoreScreen.png", "assets/scoreScreen/scoreScreen.json");
            this.load.atlasJSONArray("playScoreScreen", "assets/scoreScreen/scoreScreenButtons.png", "assets/scoreScreen/scoreScreenButtons.json");
            for (var i = 1; i < 6; i++) {
                this.load.atlasJSONArray("fail" + i, "assets/feedbacks/fail" + i + ".png", "assets/feedbacks/fail" + i + ".json");
            }
            for (var i = 1; i < 5; i++) {
                this.load.atlasJSONArray("success" + i, "assets/feedbacks/success" + i + ".png", "assets/feedbacks/success" + i + ".json");
            }
            for (var i = 1; i < 5; i++) {
                this.load.audio("congrats" + i, "assets/feedbacks/congrats" + i + ".mp3");
            }
            this.load.audio("failure1", "assets/feedbacks/failure1.mp3");
            this.load.audio("failure2", "assets/feedbacks/failure2.mp3");
        };
        Preloader.prototype.create = function () {
            this.game.state.start("MainMenu", true, false);
        };
        return Preloader;
    })(Phaser.State);
    Robo.Preloader = Preloader;
})(Robo || (Robo = {}));
var Robo;
(function (Robo) {
    var FinishScreen = (function (_super) {
        __extends(FinishScreen, _super);
        function FinishScreen(game) {
            _super.call(this, game);
            this._gameControl = Robo.GameControler.getInstance();
            this._game = game;
            this.create();
        }
        FinishScreen.prototype.create = function () {
            this._finishScreen = new Robo.PopUpWin(this.game, this, 0, 0);
            //this._finishScreen.animations.add("play");
            //  this._finishScreen.play("play", 24, true);
            this._finishScreen.visible = true;
            this._finishScreen.activeButton();
            this.addChild(this._finishScreen);
            /*  this._playButton = new Phaser.Button(this._game, 0, 0, "playScoreScreen", this.playAgain, this, 1, 0, 1, 0);
  
              this._quit = new Phaser.Button(this._game, 300, 10, "playScoreScreen", this.quitClick, this, 3, 2, 3, 2);
              this._playButton.x = 257;
              this._playButton.y = 358;
              this._quit.x = 427;
              this._quit.y = 358;
              this.addChild(this._playButton);
              this.addChild(this._quit);*/
        };
        FinishScreen.prototype.playAgain = function () {
            this._gameControl.playAgain();
            this.removeAll();
            this.destroy(true);
            console.log("play again");
        };
        FinishScreen.prototype.hideScreenAfterFinishPlaye = function () {
            this.playAgain();
        };
        FinishScreen.prototype.quitClick = function () {
            console.log("quit");
        };
        return FinishScreen;
    })(Phaser.Group);
    Robo.FinishScreen = FinishScreen;
})(Robo || (Robo = {}));
var Robo;
(function (Robo) {
    var PopUp = (function (_super) {
        __extends(PopUp, _super);
        function PopUp(game, level, x, y) {
            _super.call(this, game, x, y);
            this.level1 = level;
            this._game = game;
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var bgPopup = new Phaser.Sprite(this.game, 0, 0, "popup");
            this.addChild(bgPopup);
            this.logo = new Phaser.Sprite(this.game, 0, 0, "logoGame");
            this.addChild(this.logo);
            this.logo.x = (bgPopup.width / 2 - this.logo.width / 2);
            this.logo.y = this.logo.height - 20;
            var text = "Good try.\n Keep playing to win credits";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this.game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;
            textDescription.x = this.game.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 140;
            this.addChild(textDescription);
            this.textLevel();
            //this.beatText();
            this.addButtons();
            this.visible = false;
        }
        PopUp.prototype.addButtons = function () {
            this.gamesBtn = new Phaser.Button(this._game, 0, 0, "games", this.gamesBtnClick, this, 1, 0);
            this.playBtn = new Phaser.Button(this._game, 100, 0, "playGame", this.playBtnClick, this, 1, 0);
            this.gamesBtn.inputEnabled = true;
            this.gamesBtn.input.priorityID = 10;
            this.playBtn.inputEnabled = true;
            this.playBtn.input.priorityID = 10;
            var buttonsGr = new Phaser.Group(this._game);
            this.addChild(buttonsGr);
            buttonsGr.add(this.gamesBtn);
            buttonsGr.add(this.playBtn);
            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;
        };
        PopUp.prototype.activeButton = function () {
            this.gamesBtn.inputEnabled = true;
            this.playBtn.inputEnabled = true;
        };
        PopUp.prototype.gamesBtnClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        PopUp.prototype.playBtnClick = function () {
            this.visible = false;
            this.gamesBtn.inputEnabled = false;
            //   this.gamesBtn.input.priorityID = 3;
            this.playBtn.inputEnabled = false;
            // this.playBtn.input.priorityID = 3;
            this.level1.hideScreenAfterFinishPlaye();
            //  this.game.state.start("PopupStart", true, false);
        };
        PopUp.prototype.textLevel = function () {
            var text = "GAME OVER";
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this._game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = this.game.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 25;
            this.addChild(textDescription);
        };
        return PopUp;
    })(Phaser.Sprite);
    Robo.PopUp = PopUp;
})(Robo || (Robo = {}));
function launchGame(result) {
    userLevel = result.level;
    if (userLevel > 5) {
        userLevel = 5;
        APIsetLevel(userID, gameID, userLevel);
    }
}
//declare function launchGame(result);
var Robo;
(function (Robo) {
    var PopupStart = (function (_super) {
        __extends(PopupStart, _super);
        function PopupStart() {
            _super.apply(this, arguments);
        }
        PopupStart.prototype.create = function () {
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var background = this.add.sprite(0, 0, 'mainMenuBg');
            var bgPopup = this.add.sprite(0, 0, "popup");
            this.logo = this.add.sprite(0, 0, "logoGame");
            this.logo.x = (bgPopup.width / 2 - this.logo.width / 2);
            this.logo.y = this.logo.height - 20;
            var text = "Fill the robots with as many parts as you can.\n";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;
            textDescription.x = this.game.width / 2 - textDescription.wordWrapWidth / 2 + 30;
            textDescription.y = this.logo.y + this.logo.height + 140;
            this.textLevel();
            //this.beatText();
            this.addButtons();
        };
        PopupStart.prototype.addButtons = function () {
            var gamesBtn = this.add.button(0, 0, "games", this.gamesBtnClick, this, 1, 0);
            var playBtn = this.add.button(100, 0, "playGame", this.playBtnClick, this, 1, 0);
            var buttonsGr = this.add.group();
            buttonsGr.add(gamesBtn);
            buttonsGr.add(playBtn);
            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;
        };
        PopupStart.prototype.gamesBtnClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        PopupStart.prototype.playBtnClick = function () {
            this.game.state.start("Instruction", true, false);
        };
        PopupStart.prototype.beatText = function () {
            var text = "BEAT THIS LEVEL";
            //" 6";
            var style = { font: "bold 13px Arial", fill: "#660000", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = (this.game.width / 2) + (140) - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 100;
            var numberText = "6";
            var styleNumber = { font: "bold 17px Arial", fill: "#990000", align: "center" };
            var numberShow = this.add.text(0, 0, numberText, styleNumber);
            numberShow.x = textDescription.x + textDescription.width / 2 - numberShow.width / 2;
            numberShow.y = textDescription.y + textDescription.height;
            var tunesText = "MORE TUNES TO WIN";
            var tunesShow = this.add.text(0, 0, tunesText, style);
            tunesShow.wordWrap = true;
            tunesShow.wordWrapWidth = 100;
            tunesShow.x = textDescription.x + textDescription.width / 2 - tunesShow.width / 2;
            tunesShow.y = numberShow.y + numberShow.height;
            var creditsNmText = "25";
            var styleCredits = { font: "bold 17px Arial", fill: "#ff0000", align: "center" };
            var credistNmShow = this.add.text(0, 0, creditsNmText, styleCredits);
            credistNmShow.x = tunesShow.x + tunesShow.width / 2 - credistNmShow.width / 2;
            credistNmShow.y = tunesShow.y + tunesShow.height;
            var creditsText = "CREDITS";
            var credistShow = this.add.text(0, 0, creditsText, style);
            credistShow.x = tunesShow.x + tunesShow.width / 2 - credistShow.width / 2;
            credistShow.y = credistNmShow.y + credistNmShow.height;
        };
        PopupStart.prototype.textCredits = function () {
            var text = "Beat this level";
            //" 6";
            var style = { font: "bold 14px Arial", fill: "#660000", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = (this.game.width / 2) + 257 / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 100;
            var numberText = "6";
            var styleNumber = { font: "bold 18px Arial", fill: "#990000", align: "center" };
            var numberShow = this.add.text(0, 0, numberText, styleNumber);
            numberShow.x = textDescription.x + textDescription.width / 2 - numberShow.width / 2;
            numberShow.y = textDescription.y + textDescription.height;
        };
        PopupStart.prototype.textLevel = function () {
            //APIgetLevel(userID, gameID);
            var text = "LEVEL " + userLevel;
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = this.game.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 25;
        };
        return PopupStart;
    })(Phaser.State);
    Robo.PopupStart = PopupStart;
})(Robo || (Robo = {}));
var Robo;
(function (Robo) {
    var PopUpWin = (function (_super) {
        __extends(PopUpWin, _super);
        function PopUpWin(game, level, x, y) {
            _super.call(this, game, x, y);
            this._game = game;
            this.level1 = level;
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var bgPopup = new Phaser.Sprite(this.game, 0, 0, "popup");
            this.addChild(bgPopup);
            this.logo = new Phaser.Sprite(this.game, 0, 0, "logoGame");
            this.addChild(this.logo);
            this.logo.x = (bgPopup.width / 2 - this.logo.width / 2);
            this.logo.y = this.logo.height - 20;
            var text = "You finish the level.\n Keep Playing to win credits!";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this.game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;
            textDescription.x = this.game.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 140;
            this.addChild(textDescription);
            this.textLevel();
            //this.beatText();
            this.addButtons();
            this.visible = false;
        }
        PopUpWin.prototype.addButtons = function () {
            this.gamesBtn = new Phaser.Button(this._game, 0, 0, "games", this.gamesBtnClick, this, 1, 0);
            this.playBtn = new Phaser.Button(this._game, 100, 0, "playGame", this.playBtnClick, this, 1, 0);
            this.gamesBtn.inputEnabled = true;
            this.gamesBtn.input.priorityID = 10;
            this.playBtn.inputEnabled = true;
            this.playBtn.input.priorityID = 10;
            var buttonsGr = new Phaser.Group(this._game);
            this.addChild(buttonsGr);
            buttonsGr.add(this.gamesBtn);
            buttonsGr.add(this.playBtn);
            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;
        };
        PopUpWin.prototype.activeButton = function () {
            this.gamesBtn.inputEnabled = true;
            this.playBtn.inputEnabled = true;
        };
        PopUpWin.prototype.gamesBtnClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        PopUpWin.prototype.playBtnClick = function () {
            this.visible = false;
            this.gamesBtn.inputEnabled = false;
            this.gamesBtn.input.priorityID = 3;
            this.playBtn.inputEnabled = false;
            this.playBtn.input.priorityID = 3;
            this.level1.hideScreenAfterFinishPlaye();
            //  this.game.state.start("PopupStart", true, false);
        };
        PopUpWin.prototype.textLevel = function () {
            var text = "GOOD JOB";
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this._game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = this.game.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 25;
            this.addChild(textDescription);
        };
        return PopUpWin;
    })(Phaser.Sprite);
    Robo.PopUpWin = PopUpWin;
})(Robo || (Robo = {}));
var Robo;
(function (Robo) {
    var SuccesFail = (function (_super) {
        __extends(SuccesFail, _super);
        function SuccesFail(game) {
            _super.call(this, game);
            this._currentSucces = 1;
            this._currentFail = 1;
            this._game = game;
            this._gameControl = Robo.GameControler.getInstance();
        }
        SuccesFail.prototype.showSucces = function () {
            this.checkAnim();
            this._currentAnim = new Phaser.Sprite(this._game, 0, 0, "success" + this._currentSucces, 0);
            this.ceneter();
            this._currentAnim.animations.add("play");
            this._currentAnim.animations.getAnimation("play").onComplete.add(this.finishAnimSucces, this);
            this._currentAnim.animations.play("play", 24, false);
            this.add(this._currentAnim);
            this._currentSucces++;
        };
        SuccesFail.prototype.finishAnimSucces = function () {
            this.game.time.events.add(Phaser.Timer.SECOND * 2, function () {
                // this._countDown.animations.play("countdownAnim", 24, false, false);
                //   this._gameControl.playAgain();
                this.checkAnim();
                this.screenWinOrGameOver = new Robo.PopUpWin(this.game, this, 0, 0);
                this.addChild(this.screenWinOrGameOver);
                this.screenWinOrGameOver.activeButton();
                this.screenWinOrGameOver.visible = true;
            }, this);
        };
        SuccesFail.prototype.showFail = function () {
            this.checkAnim();
            this._currentAnim = new Phaser.Sprite(this._game, 0, 0, "success1", 0);
            this.ceneter();
            this._currentAnim.animations.add("play");
            this._currentAnim.animations.getAnimation("play").onComplete.add(this.finishAnimFail, this);
            this._currentAnim.animations.play("play", 24, false);
            this.add(this._currentAnim);
            this._currentFail++;
        };
        SuccesFail.prototype.finishAnimFail = function () {
            this.game.time.events.add(Phaser.Timer.SECOND * 2, function () {
                // this._countDown.animations.play("countdownAnim", 24, false, false);
                //  this._gameControl.showFinish();
                this.checkAnim();
                this.screenWinOrGameOver = new Robo.PopUp(this.game, this, 0, 0);
                this.addChild(this.screenWinOrGameOver);
                this.screenWinOrGameOver.activeButton();
                this.screenWinOrGameOver.visible = true;
            }, this);
        };
        SuccesFail.prototype.ceneter = function () {
            this._currentAnim.x = this._game.width / 2 - this._currentAnim.width / 2;
            this._currentAnim.y = this._game.height / 2 - this._currentAnim.height / 2;
        };
        SuccesFail.prototype.checkAnim = function () {
            if (this._currentAnim != null) {
                this.remove(this._currentAnim);
                this._currentAnim.kill();
                this._currentAnim = null;
            }
        };
        SuccesFail.prototype.hideScreenAfterFinishPlaye = function () {
            this.screenWinOrGameOver.visible = false;
            this.screenWinOrGameOver.kill();
            this.screenWinOrGameOver = null;
            this._gameControl.playAgain();
        };
        return SuccesFail;
    })(Phaser.Group);
    Robo.SuccesFail = SuccesFail;
})(Robo || (Robo = {}));
//# sourceMappingURL=game.js.map