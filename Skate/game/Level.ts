﻿module Skate {

    export class Level extends Phaser.State {


        private _bg: Phaser.Group;
      
        private _gameControler: GameControler;

      
        private _prompt: PromptView;

        private _sequenceGr: Phaser.Group;
      


     
        private _showLetterGr: Phaser.Group;

      

        private _finishScreenLevel: FinishLevel;
        private _finishScreen: FinishScreen;
        private _special: Phaser.Sprite;
        private _jumpBtn: Phaser.Button;
        private _timer: number = 0;
        private _timerCounter;
        private rightKey;
        private _blokada: Phaser.Graphics; 
        private jumpPressed: boolean = false;
        private _blokadaGr: Phaser.Group;
        private _objectToDrag: ShapeToDrag;
        private _itemsGr: Phaser.Group;
        public _itemsDropGr: Phaser.Group;
        private _trashGr: Phaser.Group;
        private _drawItemShow: DrawItemView;

        private _rotationBtn: RotationBtn;
        private _flipBtn: FlipBtn;
        private _zoomBtn: ZoomBtn;

        private _buttonChannge;
        private _clearBtn: Phaser.Button;
        private _finishBtn: Phaser.Button;
        private _textGr: Phaser.Group;
        private _pointTxt: Phaser.Text;
        private _designText: Phaser.Text;
        private helpGr;
        private _helpBtn;
        private _instructionShow;
        create() {

            this._gameControler = GameControler.getInstance();

            this._gameControler.setLevel(this);

            this._gameControler.setUserLevel(userLevel);

            this._blokadaGr = this.add.group();

            this._bg = this.game.add.group();
            this.game.add.image(0, 0, "bgGame", 0, this._bg);

            this._trashGr = this.add.group();
            this._trashGr.enableBody = true;
            this._trashGr.add(new Trash(this.game, 605, 370, "trash", this));
          

            this._itemsGr = this.game.add.group();
            this._itemsGr.enableBody = true;
            this._itemsGr.y = 435;
            this._itemsGr.x = 60;
          
            this._textGr = this.game.add.group();
            this.stage.addChild(this._textGr);
            this._textGr.x = 400;
            this._textGr.y = 90;


            this._drawItemShow = new DrawItemView(this.game);
            this.stage.addChild(this._drawItemShow);
            this._drawItemShow.showItem();

            this._itemsDropGr = this.add.group();
            this._itemsDropGr.enableBody = true;

            this._rotationBtn = new RotationBtn(this.game, 100, 5, "rotation");
            this.stage.addChild(this._rotationBtn);

            this._zoomBtn = new ZoomBtn(this.game, 50, 95, "zoom");

            this.stage.addChild(this._zoomBtn);

            this._flipBtn = new FlipBtn(this.game, 150, 95, "flip");
            this.stage.addChild(this._flipBtn);

            this._finishBtn = this.game.add.button(660, 10, "finish", this.finishBtnClick, this, 2, 0, 1);
            this._clearBtn = this.game.add.button(550, 5, "clear", this.clearBtnClick, this, 2, 0, 1);
           
           

            this.addText();
            
            this.addItems();

            this.game.input.onUp.add(this.stageUp, this);

            this.helpGr = this.add.group();
            this.stage.addChild(this.helpGr);
           
            this._helpBtn = this.add.button(0, 0, "helpBtn", this.helpBtnClick, this, 1, 0, 0, 0, this.helpGr);
            this.helpGr.x = 800 - this.helpGr.width;
            this.helpGr.y = 300 - this.helpGr.height;

            this._helpBtn.buttonMode = true;
            this._helpBtn.inputEnabled = true;
            this._helpBtn.input.priorityID = 110;
            //this.showLetterMenu(new Array("a"));
           
            this.game.input.onDown.add(function () { if (this.game.paused) { this.game.paused = false; } if (this._instructionShow) { this.stage.removeChild(this._instructionShow); this._instructionShow.kill(); this._instructionShow = null; } }, this);
           

        }

        private helpBtnClick() {

            console.log("pokaz instrukcje");
            this.game.paused = !this.game.paused;

            if (this._instructionShow == null) this._instructionShow = this.add.image(0, 0, "firstInstruction", 0);
            

            // this.add.image(0, 0, "firstInstruction", 0);
            this._instructionShow.width = this.game.world.width;//this.stage.width;
            this._instructionShow.height = this.game.world.height;
            this.stage.addChild(this._instructionShow);

        }


        playAgain() {

            this._drawItemShow.showItem();
        }

        addText() {

            var style = { font: "20px Arial", fill: "#ffffff", align: "left" };
          

            this._pointTxt = this.game.add.text(0, 0, "credits: 000", style);
            this._pointTxt.x = 0
            this._pointTxt.y = 0;
            this._textGr.add(this._pointTxt);
            this._designText = this.game.add.text(0, 0, "design: 1/3", style);
            this._designText.x = this._pointTxt.x
            this._designText.y = this._pointTxt.y + this._pointTxt.height;
            this._textGr.add(this._designText);

        }

        addDesign(count: number=1) {
            this._designText.setText("design: " + count+"/3");

        }

        addPoints(point: number = 5) {

            this._pointTxt.setText("credits: " + point);
        }

        public showFinish(win: boolean) {

            if (this._finishScreen == null) this._finishScreen = new FinishScreen(this.game);
            this._finishScreen.visible = true;
            this._finishScreen.show(win);

            this.stage.addChild(this._finishScreen);

        }

        public showMiniWin() {

            if (this._finishScreen == null) this._finishScreen = new FinishScreen(this.game);
            this._finishScreen.visible = true;
            this.stage.addChild(this._finishScreen);
            this._finishScreen.miniwin();

            

        }


        finishBtnClick() {

            this._drawItemShow.chechCorrectAnswer();

        }

        public clearBtnClick() {

            this._drawItemShow.clearItem();
            this._itemsDropGr.forEach(function (item) {
                item.kill();
            }, this);
            this._buttonChannge = null;
            this._gameControler.reset();

        }

        addButton(dragItem) {

            if (this._buttonChannge) {

                this.stage.removeChild(this._buttonChannge);
                this._buttonChannge = null;
            }
            this._buttonChannge = dragItem;
            this.stage.addChild(this._buttonChannge);
            this._gameControler.dragButton = true;

        }

        

        addItems() {

            var margin: number = 20;
            var posx: number = 0;
            var posy: number = 0;
            for (var i: number = 0; i < 10; i++) {

                var shape: ShapeToDrag = new ShapeToDrag(this.game, 0, 0, "Shape" + i, this);
                shape.x = posx;
                shape.y = posy;

                posx = shape.x + shape.width + margin;
                if ((i + 1) % 5 == 0) {
                    posx = 0;
                    posy = shape.y + shape.height + margin;
                }
                this._itemsGr.add(shape);
                shape.inputEnabled = true;
                shape.events.onInputDown.add(this.check, this);



            }


        }

        blockadToClick(block:boolean) {

            this._itemsGr.forEach(function (item) {

                item.inputEnabled = block;

            }, this);




        }


        check(item: ShapeToDrag, pointer) {
         
            var spr = item.addObjectToDrag();
            spr.position.setTo(pointer.x, pointer.y);
            this.stage.addChild(spr);
            this._objectToDrag = spr;

            this.game.physics.enable(spr, Phaser.Physics.ARCADE);

        }


        checkIfNotAdded(object: ShapeToDrag = null) {


            if (object != null) {

                console.log("object niedodany ");
                object.kill();

            }
            else {
              

            }

        }

        stageUp() {
            this.game.physics.arcade.overlap(this._objectToDrag, this._drawItemShow, this.collision, null, this);
        
            console.log("stageeup");
            //   this._objectToDrag = null;
            //  this._objectToDrag.kill();
            // console.log("stage up");
         
            this.checkIfNotAdded(this._objectToDrag);
        }
        
        private _dragCollision: boolean = false;
        onShapeDragStop(item:ShapeToDrag) {
            this._dragCollision = false;
            this.game.physics.arcade.overlap(item, this._drawItemShow, this.collision, null, this);
            this.game.physics.arcade.overlap(item, this._trashGr, this.collisionTrash, null, this);
            if (!this._dragCollision) {
                item.goToStartPosition();
            }
           
        }



        collision(object, object2) {

           
            object.x = object2.position.x;
            object.y = object2.position.y;
          
            
            if (!object.isAdded && this._drawItemShow.addItem(object)) {
              
                this._dragCollision = true;
               
                this._itemsDropGr.add(object);
              
                this._objectToDrag = null;
            }
            else if (object.isAdded) {
                console.log("jest juz dodany ten object");
                //object.goToStartPosition();
            }
            else {
                object.goToStartPosition();
            }
           

        }

        collisionTrash(object, object2) {

            console.log("trash collison",object);
            this._drawItemShow.removeItem(object);
            this._dragCollision = true;
            //object.destroy(true);

        }

/*
        stageUp() {
            this.game.physics.arcade.overlap(this._objectToDrag, this._robotsGr, this.collision, this.iCoTO, this);
            //   this._objectToDrag = null;
            //  this._objectToDrag.kill();
            // console.log("stage up");
         
            this.checkIfNotAdded(this._objectToDrag);
        }
*/
        update() {

            if (this._objectToDrag != null) {
                // var pos = this.stage.position.x
               
                this._objectToDrag.position.setTo(this.game.input.x, this.game.input.y);

            }

          /*  if (this._gameControler.dragButtonType.length > 0) {

               // this._buttonChannge.position.setTo(this.game.input.x, this.game.input.y);

                if (this._buttonChannge.y > 400) {

                    this.stage.removeChild(this._buttonChannge);
                    this._buttonChannge = null;
                    this._gameControler.dragButton = false;

                }

            }*/
        }

    }
}
