var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Skate;
(function (Skate) {
    var Instruction = (function (_super) {
        __extends(Instruction, _super);
        function Instruction() {
            _super.apply(this, arguments);
        }
        Instruction.prototype.create = function () {
            var instruction = this.add.image(0, 0, "firstInstruction", 0);
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            // var instructionAudio = this.game.add.audio("chuja", 1, false);
            //  instructionAudio.play();
            var _buttons = this.add.group();
            var buttonsBg = this.add.sprite(0, 0, 'buttonsBg');
            _buttons.addChild(buttonsBg);
            var gameButton = this.add.button(14, 36, 'buttons', null, this, 0, 0, 1);
            _buttons.addChild(gameButton);
            var playButton = this.add.button(61, gameButton.y, 'buttons', this.onPlayClick, this, 3, 2);
            playButton.onInputDown.add(this.overPlay2, this);
            _buttons.addChild(playButton);
            _buttons.x = 0;
            _buttons.y = this.game.stage.height - _buttons.height;
        };
        Instruction.prototype.overPlay2 = function () {
            this.overButtonA.play();
        };
        Instruction.prototype.onPlayClick = function () {
            this.game.state.start("Level", true, false);
        };
        return Instruction;
    })(Phaser.State);
    Skate.Instruction = Instruction;
})(Skate || (Skate = {}));
//# sourceMappingURL=Instruction.js.map