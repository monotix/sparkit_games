var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Skate;
(function (Skate) {
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            _super.apply(this, arguments);
        }
        Boot.prototype.preload = function () {
            this.load.image("bg", "assets/splashScreen.png");
            this.load.image("progressBarBlack", "assets/preloader/ProgressBar.png");
            this.load.image("progressBarBlue", "assets/preloader/ProgressBarBlue.png");
        };
        Boot.prototype.create = function () {
            this.game.state.start("Preloader", true, false);
        };
        return Boot;
    })(Phaser.State);
    Skate.Boot = Boot;
})(Skate || (Skate = {}));
//# sourceMappingURL=Boot.js.map