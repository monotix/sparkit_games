var Greeter = (function () {
    function Greeter(element) {
        this.element = element;
        this.element.innerHTML += "The time is: ";
        this.span = document.createElement('span');
        this.element.appendChild(this.span);
        this.span.innerText = new Date().toUTCString();
    }
    Greeter.prototype.start = function () {
        var _this = this;
        this.timerToken = setInterval(function () { return _this.span.innerHTML = new Date().toUTCString(); }, 500);
    };
    Greeter.prototype.stop = function () {
        clearTimeout(this.timerToken);
    };
    return Greeter;
})();
window.onload = function () {
    var el = document.getElementById('content');
    var greeter = new Greeter(el);
    greeter.start();
};
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Skate;
(function (Skate) {
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            _super.apply(this, arguments);
        }
        Boot.prototype.preload = function () {
            console.log("witam bucie");
            this.load.image("bg", "assets/splashScreen.png");
            this.load.image("progressBarBlack", "assets/preloader/ProgressBar.png");
            this.load.image("progressBarBlue", "assets/preloader/ProgressBarBlue.png");
        };
        Boot.prototype.create = function () {
            this.game.state.start("Preloader", true, false);
        };
        return Boot;
    })(Phaser.State);
    Skate.Boot = Boot;
})(Skate || (Skate = {}));
var Skate;
(function (Skate) {
    var GameControler = (function () {
        function GameControler() {
            this.finishGame = false;
            this._currenLevel = 0;
            this.gameOver = true;
            this.maxLevel = 3;
            this.currentLevel = 0;
            this.drageStop = false;
            this.dragButton = false;
            this.dragButtonType = "";
            this._deisgnCount = 1;
        }
        GameControler.prototype.construct = function () {
            if (GameControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            GameControler._instance = this;
        };
        GameControler.getInstance = function () {
            if (GameControler._instance === null) {
                GameControler._instance = new GameControler();
            }
            return GameControler._instance;
        };
        GameControler.prototype.endGame = function (win) {
            if (win) {
                this.level.addPoints();
                this._deisgnCount++;
                if (this._deisgnCount > 3) {
                    this.level.showFinish(true);
                    this.level.addDesign(1);
                    this.nextLevel();
                }
                else {
                    console.log("mini wygrana");
                    this.level.addDesign(this._deisgnCount);
                    this.level.showMiniWin();
                }
            }
            else {
                this.level.showFinish(false);
            }
        };
        GameControler.prototype.restart = function () {
            this._currenLevel = 0;
            //this.level.playAgain();
        };
        GameControler.prototype.nextLevel = function () {
            this.currentLevel++;
            //   this.level.showPrompt(true, true);
        };
        GameControler.prototype.setLevel = function (level) {
            this._level = level;
        };
        Object.defineProperty(GameControler.prototype, "level", {
            get: function () {
                return this._level;
            },
            enumerable: true,
            configurable: true
        });
        GameControler.prototype.setUserLevel = function (level_) {
            this._currenLevel = level_ - 1;
        };
        GameControler.prototype.reset = function () {
            this.drageStop = false;
            this.dragButton = false;
        };
        GameControler.prototype.setSound = function (sound_) {
            this.mainSound = sound_;
        };
        GameControler._instance = null;
        return GameControler;
    })();
    Skate.GameControler = GameControler;
})(Skate || (Skate = {}));
var Skate;
(function (Skate) {
    var ItemsControler = (function () {
        function ItemsControler() {
            this._gameControler = Skate.GameControler.getInstance();
            this.finishGame = false;
            this._currenLevel = 0;
            this.gameOver = true;
            this.maxLevel = 3;
            this.currentLevel = 0;
        }
        ItemsControler.prototype.construct = function () {
            if (ItemsControler._instance) {
                throw new Error("Error: Instantiation failed: Use ItemsControler.getInstance() instead of new.");
            }
            ItemsControler._instance = this;
        };
        ItemsControler.getInstance = function () {
            if (ItemsControler._instance === null) {
                ItemsControler._instance = new ItemsControler();
            }
            return ItemsControler._instance;
        };
        ItemsControler.prototype.level1 = function () {
            var arr;
            if (this._gameControler.currentLevel == 0) {
                arr = new Array(new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape1", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape2", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape3", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape1", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape2", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape3", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape1", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape2", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape3", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }));
            }
            else if (this._gameControler.currentLevel == 1) {
                arr = new Array(new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape1", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape2", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape3", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape1", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape2", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape3", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape1", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape2", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape3", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }));
            }
            else if (this._gameControler.currentLevel == 2) {
                arr = new Array(new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape1", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape2", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape3", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape1", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape2", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape3", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape1", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape2", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape3", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }));
            }
            else if (this._gameControler.currentLevel == 3) {
                arr = new Array(new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape1", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape2", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape3", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape1", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape2", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape3", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape1", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape2", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape3", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }));
            }
            else if (this._gameControler.currentLevel == 4) {
                arr = new Array(new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape1", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape2", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape3", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape1", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape2", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape3", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape1", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape2", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape3", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }));
            }
            else if (this._gameControler.currentLevel == 5) {
                arr = new Array(new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape1", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape2", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape3", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape1", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape2", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape3", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape1", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape2", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape3", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }));
            }
            else if (this._gameControler.currentLevel == 6) {
                arr = new Array(new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape1", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape2", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape3", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape1", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape2", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape3", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape1", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape2", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape3", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }), new Array({ name: "Shape4", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape5", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 }, { name: "Shape6", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }));
            }
            return arr[this._gameControler._deisgnCount - 1];
        };
        ItemsControler._instance = null;
        return ItemsControler;
    })();
    Skate.ItemsControler = ItemsControler;
})(Skate || (Skate = {}));
var Skate;
(function (Skate) {
    var Instruction = (function (_super) {
        __extends(Instruction, _super);
        function Instruction() {
            _super.apply(this, arguments);
        }
        Instruction.prototype.create = function () {
            var background = this.add.sprite(0, 0, 'mainMenuBg');
            var instruction = this.add.image(0, 0, "firstInstruction", 0);
            instruction.width = this.game.world.width; //this.stage.width;
            instruction.height = this.game.world.height; //this.stage.height;
            this._mainMenuBg = this.game.add.audio('instructionSn', 1, true);
            this._mainMenuBg.play("", 0, 1, false);
            var gameControler = Skate.GameControler.getInstance();
            gameControler.mainSound.volume = 0.05;
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            // var instructionAudio = this.game.add.audio("chuja", 1, false);
            //  instructionAudio.play();
            var _buttons = this.add.group();
            var gameButton = this.add.button(0, 0, "games", this.onGameClick, this, 1, 0);
            //gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            _buttons.addChild(gameButton);
            var playButton = this.add.button(100, 0, "playGame", this.onPlayClick, this, 1, 0);
            playButton.onInputDown.add(this.overPlay2, this);
            _buttons.addChild(playButton);
            _buttons.x = 0;
            _buttons.y = this.game.stage.height - _buttons.height;
        };
        Instruction.prototype.onGameClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        Instruction.prototype.overPlay2 = function () {
            this.overButtonA.play();
        };
        Instruction.prototype.onPlayClick = function () {
            this._mainMenuBg.stop();
            this.game.state.start("Level", true, false);
        };
        return Instruction;
    })(Phaser.State);
    Skate.Instruction = Instruction;
})(Skate || (Skate = {}));
var Skate;
(function (Skate) {
    var Level = (function (_super) {
        __extends(Level, _super);
        function Level() {
            _super.apply(this, arguments);
            this._timer = 0;
            this.jumpPressed = false;
            this._dragCollision = false;
        }
        Level.prototype.create = function () {
            this._gameControler = Skate.GameControler.getInstance();
            this._gameControler.setLevel(this);
            this._gameControler.setUserLevel(userLevel);
            this._blokadaGr = this.add.group();
            this._bg = this.game.add.group();
            this.game.add.image(0, 0, "bgGame", 0, this._bg);
            this._trashGr = this.add.group();
            this._trashGr.enableBody = true;
            this._trashGr.add(new Skate.Trash(this.game, 605, 370, "trash", this));
            this._itemsGr = this.game.add.group();
            this._itemsGr.enableBody = true;
            this._itemsGr.y = 435;
            this._itemsGr.x = 60;
            this._textGr = this.game.add.group();
            this.stage.addChild(this._textGr);
            this._textGr.x = 400;
            this._textGr.y = 90;
            this._drawItemShow = new Skate.DrawItemView(this.game);
            this.stage.addChild(this._drawItemShow);
            this._drawItemShow.showItem();
            this._itemsDropGr = this.add.group();
            this._itemsDropGr.enableBody = true;
            this._rotationBtn = new Skate.RotationBtn(this.game, 100, 5, "rotation");
            this.stage.addChild(this._rotationBtn);
            this._zoomBtn = new Skate.ZoomBtn(this.game, 50, 95, "zoom");
            this.stage.addChild(this._zoomBtn);
            this._flipBtn = new Skate.FlipBtn(this.game, 150, 95, "flip");
            this.stage.addChild(this._flipBtn);
            this._finishBtn = this.game.add.button(660, 10, "finish", this.finishBtnClick, this, 2, 0, 1);
            this._clearBtn = this.game.add.button(550, 5, "clear", this.clearBtnClick, this, 2, 0, 1);
            this.addText();
            this.addItems();
            this.game.input.onUp.add(this.stageUp, this);
            this.helpGr = this.add.group();
            this.stage.addChild(this.helpGr);
            this._helpBtn = this.add.button(0, 0, "helpBtn", this.helpBtnClick, this, 1, 0, 0, 0, this.helpGr);
            this.helpGr.x = 800 - this.helpGr.width;
            this.helpGr.y = 300 - this.helpGr.height;
            this._helpBtn.buttonMode = true;
            this._helpBtn.inputEnabled = true;
            this._helpBtn.input.priorityID = 110;
            //this.showLetterMenu(new Array("a"));
            this.game.input.onDown.add(function () {
                if (this.game.paused) {
                    this.game.paused = false;
                }
                if (this._instructionShow) {
                    this.stage.removeChild(this._instructionShow);
                    this._instructionShow.kill();
                    this._instructionShow = null;
                }
            }, this);
        };
        Level.prototype.helpBtnClick = function () {
            console.log("pokaz instrukcje");
            this.game.paused = !this.game.paused;
            if (this._instructionShow == null)
                this._instructionShow = this.add.image(0, 0, "firstInstruction", 0);
            // this.add.image(0, 0, "firstInstruction", 0);
            this._instructionShow.width = this.game.world.width; //this.stage.width;
            this._instructionShow.height = this.game.world.height;
            this.stage.addChild(this._instructionShow);
        };
        Level.prototype.playAgain = function () {
            this._drawItemShow.showItem();
        };
        Level.prototype.addText = function () {
            var style = { font: "20px Arial", fill: "#ffffff", align: "left" };
            this._pointTxt = this.game.add.text(0, 0, "credits: 000", style);
            this._pointTxt.x = 0;
            this._pointTxt.y = 0;
            this._textGr.add(this._pointTxt);
            this._designText = this.game.add.text(0, 0, "design: 1/3", style);
            this._designText.x = this._pointTxt.x;
            this._designText.y = this._pointTxt.y + this._pointTxt.height;
            this._textGr.add(this._designText);
        };
        Level.prototype.addDesign = function (count) {
            if (count === void 0) { count = 1; }
            this._designText.setText("design: " + count + "/3");
        };
        Level.prototype.addPoints = function (point) {
            if (point === void 0) { point = 5; }
            this._pointTxt.setText("credits: " + point);
        };
        Level.prototype.showFinish = function (win) {
            if (this._finishScreen == null)
                this._finishScreen = new Skate.FinishScreen(this.game);
            this._finishScreen.visible = true;
            this._finishScreen.show(win);
            this.stage.addChild(this._finishScreen);
        };
        Level.prototype.showMiniWin = function () {
            if (this._finishScreen == null)
                this._finishScreen = new Skate.FinishScreen(this.game);
            this._finishScreen.visible = true;
            this.stage.addChild(this._finishScreen);
            this._finishScreen.miniwin();
        };
        Level.prototype.finishBtnClick = function () {
            this._drawItemShow.chechCorrectAnswer();
        };
        Level.prototype.clearBtnClick = function () {
            this._drawItemShow.clearItem();
            this._itemsDropGr.forEach(function (item) {
                item.kill();
            }, this);
            this._buttonChannge = null;
            this._gameControler.reset();
        };
        Level.prototype.addButton = function (dragItem) {
            if (this._buttonChannge) {
                this.stage.removeChild(this._buttonChannge);
                this._buttonChannge = null;
            }
            this._buttonChannge = dragItem;
            this.stage.addChild(this._buttonChannge);
            this._gameControler.dragButton = true;
        };
        Level.prototype.addItems = function () {
            var margin = 20;
            var posx = 0;
            var posy = 0;
            for (var i = 0; i < 10; i++) {
                var shape = new Skate.ShapeToDrag(this.game, 0, 0, "Shape" + i, this);
                shape.x = posx;
                shape.y = posy;
                posx = shape.x + shape.width + margin;
                if ((i + 1) % 5 == 0) {
                    posx = 0;
                    posy = shape.y + shape.height + margin;
                }
                this._itemsGr.add(shape);
                shape.inputEnabled = true;
                shape.events.onInputDown.add(this.check, this);
            }
        };
        Level.prototype.blockadToClick = function (block) {
            this._itemsGr.forEach(function (item) {
                item.inputEnabled = block;
            }, this);
        };
        Level.prototype.check = function (item, pointer) {
            var spr = item.addObjectToDrag();
            spr.position.setTo(pointer.x, pointer.y);
            this.stage.addChild(spr);
            this._objectToDrag = spr;
            this.game.physics.enable(spr, Phaser.Physics.ARCADE);
        };
        Level.prototype.checkIfNotAdded = function (object) {
            if (object === void 0) { object = null; }
            if (object != null) {
                console.log("object niedodany ");
                object.kill();
            }
            else {
            }
        };
        Level.prototype.stageUp = function () {
            this.game.physics.arcade.overlap(this._objectToDrag, this._drawItemShow, this.collision, null, this);
            console.log("stageeup");
            //   this._objectToDrag = null;
            //  this._objectToDrag.kill();
            // console.log("stage up");
            this.checkIfNotAdded(this._objectToDrag);
        };
        Level.prototype.onShapeDragStop = function (item) {
            this._dragCollision = false;
            this.game.physics.arcade.overlap(item, this._drawItemShow, this.collision, null, this);
            this.game.physics.arcade.overlap(item, this._trashGr, this.collisionTrash, null, this);
            if (!this._dragCollision) {
                item.goToStartPosition();
            }
        };
        Level.prototype.collision = function (object, object2) {
            object.x = object2.position.x;
            object.y = object2.position.y;
            if (!object.isAdded && this._drawItemShow.addItem(object)) {
                this._dragCollision = true;
                this._itemsDropGr.add(object);
                this._objectToDrag = null;
            }
            else if (object.isAdded) {
                console.log("jest juz dodany ten object");
            }
            else {
                object.goToStartPosition();
            }
        };
        Level.prototype.collisionTrash = function (object, object2) {
            console.log("trash collison", object);
            this._drawItemShow.removeItem(object);
            this._dragCollision = true;
            //object.destroy(true);
        };
        /*
                stageUp() {
                    this.game.physics.arcade.overlap(this._objectToDrag, this._robotsGr, this.collision, this.iCoTO, this);
                    //   this._objectToDrag = null;
                    //  this._objectToDrag.kill();
                    // console.log("stage up");
                 
                    this.checkIfNotAdded(this._objectToDrag);
                }
        */
        Level.prototype.update = function () {
            if (this._objectToDrag != null) {
                // var pos = this.stage.position.x
                this._objectToDrag.position.setTo(this.game.input.x, this.game.input.y);
            }
            /*  if (this._gameControler.dragButtonType.length > 0) {
  
                 // this._buttonChannge.position.setTo(this.game.input.x, this.game.input.y);
  
                  if (this._buttonChannge.y > 400) {
  
                      this.stage.removeChild(this._buttonChannge);
                      this._buttonChannge = null;
                      this._gameControler.dragButton = false;
  
                  }
  
              }*/
        };
        return Level;
    })(Phaser.State);
    Skate.Level = Level;
})(Skate || (Skate = {}));
var Skate;
(function (Skate) {
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            console.log("start");
            _super.call(this, 800, 600, Phaser.AUTO, "content", null, true);
            this.state.add("Boot", Skate.Boot, false);
            this.state.add("Preloader", Skate.Preloader, false);
            this.state.add("MainMenu", Skate.MainMenu, false);
            this.state.add("PopupStart", Skate.PopupStart, false);
            this.state.add("Instruction", Skate.Instruction, false);
            this.state.add("Level", Skate.Level, false);
            this.state.start("Boot");
        }
        return Game;
    })(Phaser.Game);
    Skate.Game = Game;
})(Skate || (Skate = {}));
window.onload = function () {
    var game = new Skate.Game;
};
var userLevel = 1;
var Skate;
(function (Skate) {
    var MainMenu = (function (_super) {
        __extends(MainMenu, _super);
        function MainMenu() {
            _super.apply(this, arguments);
        }
        MainMenu.prototype.create = function () {
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var _mainMenuBg = this.game.add.audio('MainMenuSn', 1, true);
            _mainMenuBg.volume = 0.1;
            _mainMenuBg.play();
            var gameControler = Skate.GameControler.getInstance();
            gameControler.setSound(_mainMenuBg);
            var background = this.add.sprite(0, 0, 'mainMenuBg');
            var _buttons = this.add.group();
            var gameButton = this.add.button(0, 0, "games", this.onGameClick, this, 1, 0);
            //gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            _buttons.addChild(gameButton);
            var playButton = this.add.button(100, 0, "playGame", this.onPlayClick, this, 1, 0);
            playButton.onInputDown.add(this.overPlay2, this);
            _buttons.addChild(playButton);
            _buttons.x = 0;
            _buttons.y = this.game.stage.height - _buttons.height;
        };
        MainMenu.prototype.onGameClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        MainMenu.prototype.overPlay2 = function () {
            this.overButtonA.play();
        };
        MainMenu.prototype.onPlayClick = function () {
            this.game.state.start("PopupStart", true, false);
        };
        return MainMenu;
    })(Phaser.State);
    Skate.MainMenu = MainMenu;
})(Skate || (Skate = {}));
var Skate;
(function (Skate) {
    var FlipBtn = (function (_super) {
        __extends(FlipBtn, _super);
        function FlipBtn(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this._gamesControler = Skate.GameControler.getInstance();
            this._clicked = false;
            this.inputEnabled = true;
            //this.physicsEnabled = true;
            //  this.setFrames(2, 0, 1);
            this._drag = new Phaser.Sprite(this.game, 0, 0, "rotationDrag");
            this._drag.anchor.setTo(0.5, 0.5);
            this.events.onInputDown.add(this.onDown, this);
        }
        FlipBtn.prototype.onDown = function () {
            //  this._level.onShapeDown(this);
            this._clicked = (!this._clicked) ? true : false;
            if (this._clicked) {
                if (this._gamesControler.lastDragButton != null)
                    this._gamesControler.lastDragButton.setDefault();
                this.frame = 1;
                this._gamesControler.lastDragButton = this;
                this._gamesControler.dragButtonType = "flip";
                this._gamesControler.dragButton = true;
            }
            else {
                this._gamesControler.dragButton = false;
                this._gamesControler.dragButtonType = "";
                this.frame = 0;
            }
            //  this._gamesControler.level.addButton(this._drag);
        };
        FlipBtn.prototype.setDefault = function () {
            this.frame = 0;
            this._gamesControler.dragButton = false;
            this._gamesControler.dragButtonType = "";
            this._clicked = false;
        };
        FlipBtn.prototype.onStartDrag = function () {
            //this._level.onShapeDragStart(this);
            // console.log("i co teraz");
        };
        FlipBtn.prototype.onStopDrag = function () {
        };
        FlipBtn.prototype.addObjectToDrag = function () {
            return this._drag;
        };
        return FlipBtn;
    })(Phaser.Button);
    Skate.FlipBtn = FlipBtn;
})(Skate || (Skate = {}));
var Skate;
(function (Skate) {
    var RotationBtn = (function (_super) {
        __extends(RotationBtn, _super);
        function RotationBtn(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this._gamesControler = Skate.GameControler.getInstance();
            this._clicked = false;
            this.setFrames(2, 0, 1);
            this.inputEnabled = true;
            //this.physicsEnabled = true;
            this._drag = new Phaser.Sprite(this.game, 0, 0, "rotationDrag");
            this._drag.anchor.setTo(0.5, 0.5);
            this.events.onInputDown.add(this.onDown, this);
        }
        RotationBtn.prototype.onDown = function () {
            //  this._level.onShapeDown(this);
            this._clicked = (!this._clicked) ? true : false;
            if (this._clicked) {
                if (this._gamesControler.lastDragButton != null)
                    this._gamesControler.lastDragButton.setDefault();
                this._gamesControler.lastDragButton = this;
                this._gamesControler.dragButtonType = "rotation";
                this._gamesControler.dragButton = true;
                this.frame = 1;
            }
            else {
                this._gamesControler.dragButton = false;
                this._gamesControler.dragButtonType = "";
                this.frame = 0;
            }
            // this._gamesControler.level.addButton(this._drag);
        };
        RotationBtn.prototype.setDefault = function () {
            this.frame = 0;
            this._gamesControler.dragButtonType = "";
            this._gamesControler.dragButton = false;
            this._clicked = false;
        };
        RotationBtn.prototype.onStartDrag = function () {
            //this._level.onShapeDragStart(this);
            // console.log("i co teraz");
        };
        RotationBtn.prototype.onStopDrag = function () {
        };
        RotationBtn.prototype.addObjectToDrag = function () {
            return this._drag;
        };
        return RotationBtn;
    })(Phaser.Button);
    Skate.RotationBtn = RotationBtn;
})(Skate || (Skate = {}));
var Skate;
(function (Skate) {
    var ShapeToDrag = (function (_super) {
        __extends(ShapeToDrag, _super);
        function ShapeToDrag(game, x, y, key, level, scalex, scaley, rotation) {
            if (scalex === void 0) { scalex = 1; }
            if (scaley === void 0) { scaley = 1; }
            if (rotation === void 0) { rotation = 0; }
            _super.call(this, game, x, y, key);
            this.isAdded = false;
            this._gamesControler = Skate.GameControler.getInstance();
            this.wasDrag = false;
            this.name = key;
            this.scale.setTo(scalex, scaley);
            this.rotation = rotation;
            this._level = level;
            //  this._shape = new Phaser.Sprite(game, x, y, key);
        }
        ShapeToDrag.prototype.addObjectToDrag = function () {
            var spr = new ShapeToDrag(this.game, 0, 0, this.name, this._level);
            spr.anchor.setTo(0.5, 0.5);
            return spr;
        };
        ShapeToDrag.prototype.addStartPosition = function (point) {
            this._positionStart = point;
        };
        ShapeToDrag.prototype.goToStartPosition = function () {
            this.x = this._positionStart.x;
            this.y = this._positionStart.y;
        };
        ShapeToDrag.prototype.setDrag = function () {
            this.inputEnabled = true;
            this.events.onInputDown.add(this.onDown, this);
        };
        ShapeToDrag.prototype.onDown = function () {
            //  this._level.onShapeDown(this);
            console.log("i co teraz on down");
            if (this._gamesControler.dragButton) {
                this.input.disableDrag();
                this.changeItemView(this._gamesControler.dragButtonType);
            }
            else if (!this.wasDrag) {
                this.input.enableDrag();
                this.events.onDragStart.add(this.onStartDrag, this);
                this.events.onDragStop.add(this.onStopDrag, this);
                this.wasDrag = true;
            }
        };
        ShapeToDrag.prototype.changeItemView = function (whatType) {
            if (whatType == "rotation") {
                this.rotation += 45;
            }
            else if (whatType == "zoom") {
                var skala = 1;
                if (this.scale.x < 0) {
                    skala = -1;
                }
                var nowaSkala = 1;
                if (this.scale.x == 1.2 || this.scale.x == -1.2) {
                    nowaSkala = 1;
                }
                else {
                    nowaSkala = 1.2;
                }
                this.scale.setTo(nowaSkala * skala, nowaSkala);
            }
            else if ("flip") {
                if (this.scale.x > 0) {
                    this.scale.setTo(-this.scale.x, this.scale.y);
                }
                else {
                    this.scale.setTo(this.scale.x, this.scale.y);
                }
            }
        };
        ShapeToDrag.prototype.onStartDrag = function () {
            //this._level.onShapeDragStart(this);
            // console.log("i co teraz");
        };
        ShapeToDrag.prototype.onStopDrag = function () {
            this._level.onShapeDragStop(this);
        };
        return ShapeToDrag;
    })(Phaser.Sprite);
    Skate.ShapeToDrag = ShapeToDrag;
})(Skate || (Skate = {}));
var Skate;
(function (Skate) {
    var Trash = (function (_super) {
        __extends(Trash, _super);
        function Trash(game, x, y, key, level) {
            _super.call(this, game, x, y, key);
            this.levelStage = level;
            //this.physicsEnabled = true;
            this.inputEnabled = true;
            this.events.onInputOver.add(this.over, this);
            this.events.onInputOut.add(this.out, this);
        }
        Trash.prototype.over = function () {
            this.frame = 1;
        };
        Trash.prototype.out = function () {
            this.frame = 0;
        };
        return Trash;
    })(Phaser.Sprite);
    Skate.Trash = Trash;
})(Skate || (Skate = {}));
var Skate;
(function (Skate) {
    var ZoomBtn = (function (_super) {
        __extends(ZoomBtn, _super);
        function ZoomBtn(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this._gamesControler = Skate.GameControler.getInstance();
            this._clicked = false;
            this.inputEnabled = true;
            // this.setFrames(2, 0, 1);
            //this.physicsEnabled = true;
            this._drag = new Phaser.Sprite(this.game, 0, 0, "rotationDrag");
            this._drag.anchor.setTo(0.5, 0.5);
            this.events.onInputDown.add(this.onDown, this);
        }
        ZoomBtn.prototype.onDown = function () {
            //  this._level.onShapeDown(this);
            this._clicked = (!this._clicked) ? true : false;
            if (this._clicked) {
                if (this._gamesControler.lastDragButton != null)
                    this._gamesControler.lastDragButton.setDefault();
                this._gamesControler.lastDragButton = this;
                this._gamesControler.dragButtonType = "zoom";
                this._gamesControler.dragButton = true;
                this.frame = 1;
            }
            else {
                this._gamesControler.dragButtonType = "";
                this._gamesControler.dragButton = false;
                this.frame = 0;
            }
        };
        ZoomBtn.prototype.setDefault = function () {
            this.frame = 0;
            this._gamesControler.dragButtonType = "";
            this._gamesControler.dragButton = false;
            this._clicked = false;
        };
        ZoomBtn.prototype.onStartDrag = function () {
            //this._level.onShapeDragStart(this);
            // console.log("i co teraz");
        };
        ZoomBtn.prototype.onStopDrag = function () {
        };
        ZoomBtn.prototype.addObjectToDrag = function () {
            return this._drag;
        };
        return ZoomBtn;
    })(Phaser.Button);
    Skate.ZoomBtn = ZoomBtn;
})(Skate || (Skate = {}));
var Skate;
(function (Skate) {
    var Preloader = (function (_super) {
        __extends(Preloader, _super);
        function Preloader() {
            _super.apply(this, arguments);
        }
        Preloader.prototype.preload = function () {
            var bg = this.add.image(0, 0, "bg", 0);
            var blackProgress = this.add.image(0, 0, "progressBarBlack", 0);
            var blueProgress = this.add.sprite(0, 0, "progressBarBlue", 0);
            blackProgress.position.setTo(this.stage.width / 2 - blackProgress.width / 2, this.stage.height - blackProgress.height - 10);
            blueProgress.position = blackProgress.position;
            this.load.setPreloadSprite(blueProgress);
            this.load.image("mainMenuBg", "assets/mainMenu/splashScreen.png");
            this.load.atlasJSONArray('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');
            this.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
            this.load.audio("MainMenuSn", "assets/audio/SCThemeMusic.mp3");
            this.load.audio("overButtonA", "assets/audio/overButton.mp3");
            this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");
            var mobile;
            mobile = false;
            if (!this.game.device.desktop) {
                mobile = true;
            }
            if (mobile) {
                this.load.image("firstInstruction", "assets/03_scate_create.png");
                this.load.audio("instructionSn", "assets/audio/skate_create_02.mp3");
            }
            else {
                this.load.image("firstInstruction", "assets/02_scate_create.png");
                this.load.audio("instructionSn", "assets/audio/skate_create_01.mp3");
            }
            this.load.image("popup", "assets/popup/popup.png");
            this.load.image("logoGame", "assets/popup/logo.png");
            this.load.atlasJSONArray('games', 'assets/popup/games.png', 'assets/popup/games.json');
            this.load.atlasJSONArray('playGame', 'assets/popup/play.png', 'assets/popup/play.json');
            this.load.atlasJSONArray('helpBtn', 'assets/popup/help.png', 'assets/popup/help.json');
            this.load.image("bgGame", "assets/skate/gameBackground.png");
            /* instructions */
            this.load.image('prompt', 'assets/snow/prompt/instruction1.png');
            this.load.audio("WakeIntro", "assets/audio/instruction/WakeIntro.mp3");
            this.load.audio("WakeSuccess0", "assets/audio/instruction/WakeSuccess0.mp3");
            this.load.audio("WakeWrong0", "assets/audio/instruction/WakeWrong0.mp3");
            this.load.image("firstInstruction", "assets/instructionsScreen.png");
            this.load.image("sequence", "assets/snow/letterClip.png");
            /* player */
            this.load.atlasJSONArray('trash', 'assets/skate/trash.png', 'assets/skate/trash.json');
            this.load.atlasJSONArray('flip', 'assets/skate/flipBtn.png', 'assets/skate/flipBtn.json');
            this.load.atlasJSONArray('rotation', 'assets/skate/rotateRightBtn.png', 'assets/skate/rotateRightBtn.json');
            this.load.image("rotationDrag", "assets/skate/rotation.png");
            this.load.atlasJSONArray('countdown', 'assets/skate/countdown.png', 'assets/skate/countdown.json');
            this.load.atlasJSONArray('zoom', 'assets/skate/enlarge.png', 'assets/skate/enlarge.json');
            // this.load.image("clear", "assets/skate/clearBtn.png");
            //this.load.image("finish", "assets/skate/finishBtn.png");
            this.load.atlasJSONArray('clear', 'assets/skate/clearBtn.png', 'assets/skate/clearBtn.json');
            this.load.atlasJSONArray('finish', "assets/skate/finishBtn.png", "assets/skate/finishBtn.json");
            for (var i = 0; i < 10; i++) {
                this.load.image("Shape" + i, "assets/skate/items/Shape" + i + ".png");
            }
            /* obstacle */
            /*    this.load.image("piasek", "assets/water/obstacle/wood2.png");
                this.load.image("wood", "assets/water/obstacle/wood.png");
                this.load.image("hammer", "assets/water/obstacle/hammer.png");
                this.load.image("turtle", "assets/water/obstacle/turtle.png");
                this.load.image("zielone", "assets/water/obstacle/zielone.png");
                this.load.image("ball", "assets/water/obstacle/ball.png");
                this.load.image("ball2", "assets/water/obstacle/ball2.png");
                this.load.image("walen", "assets/water/obstacle/walen.png");
                */
            //  this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");
            /*   for (var i: number = 1; i < 5; i++) {
   
                   this.load.audio("failaudio" + i, "assets/audio/instruction/fail" + i + ".mp3");
   
   
               }*/
            /* for (var i: number = 1; i < 5; i++) {
 
                 this.load.audio("success" + i, "assets/audio/instruction/success" + i + ".mp3");
 
 
             }
             */
            /*    for (var i: number = 1; i < 4; i++) {
                    this.load.image("item"+i, "assets/robo/item"+i+".PNG");
                }
    
                for (var i: number = 1; i < 4; i++) {
                    this.load.image("robot" + i, "assets/robo/robot" + i + ".PNG");
                }
                */
            /*skrzynie*/
            /*end skrzynie"*/
            this.load.atlasJSONArray("scoreScreen", "assets/scoreScreen/scoreScreen.png", "assets/scoreScreen/scoreScreen.json");
            this.load.atlasJSONArray("playScoreScreen", "assets/scoreScreen/scoreScreenButtons.png", "assets/scoreScreen/scoreScreenButtons.json");
            for (var i = 1; i < 6; i++) {
                this.load.atlasJSONArray("fail" + i, "assets/feedbacks/fail" + i + ".png", "assets/feedbacks/fail" + i + ".json");
            }
            for (var i = 1; i < 3; i++) {
                this.load.atlasJSONArray("success" + i, "assets/feedbacks/success" + i + ".png", "assets/feedbacks/success" + i + ".json");
            }
            for (var i = 1; i < 5; i++) {
                this.load.audio("congrats" + i, "assets/feedbacks/congrats" + i + ".mp3");
            }
            this.load.audio("failad1", "assets/feedbacks/failure1.mp3");
            this.load.audio("failad2", "assets/feedbacks/failure2.mp3");
        };
        Preloader.prototype.create = function () {
            this.game.state.start("MainMenu", true, false);
        };
        return Preloader;
    })(Phaser.State);
    Skate.Preloader = Preloader;
})(Skate || (Skate = {}));
var Skate;
(function (Skate) {
    var DrawItemView = (function (_super) {
        __extends(DrawItemView, _super);
        function DrawItemView(game) {
            _super.call(this, game);
            this._itemsControler = Skate.ItemsControler.getInstance();
            this._gamesControler = Skate.GameControler.getInstance();
            this.enableBody = true;
            this._bgBlack = new Phaser.Graphics(this.game, 0, 0);
            this._bgBlack.beginFill(0x000000, 0.6);
            this._bgBlack.drawRect(0, 0, this.game.width, this.game.height);
            this._bgBlack.endFill();
            this._bgSpr = new Phaser.Sprite(this.game, 0, 0, "");
            this._bgSpr.addChild(this._bgBlack);
            this.add(this._bgSpr);
            this._bgSpr.buttonMode = true;
            this._bgSpr.inputEnabled = true;
            this._bgSpr.input.priorityID = 10;
            this._countdown = new Phaser.Sprite(this.game, 660, 10, "countdown");
            this._countdown.animations.add("play");
            this.add(this._countdown);
        }
        DrawItemView.prototype.blokada = function (block) {
            this._bgSpr.buttonMode = block;
            this._bgSpr.inputEnabled = block;
        };
        DrawItemView.prototype.showItem = function () {
            this._countdown.play("play", 24, false);
            this.blokada(true);
            this._dropItemArr = new Array();
            this._itemsToCheckArr = new Array();
            this._gamesControler.drageStop = true;
            this.visible = true;
            var itemArr = this._itemsControler.level1();
            console.log("c o pokazuje", itemArr);
            for (var i = 0; i < itemArr.length; i++) {
                var obj = itemArr[i];
                var item = new Skate.ShapeToDrag(this.game, obj.x + 60, obj.y + 110, obj.name, this._gamesControler.level, obj.xscale, obj.yscale, obj.rotation);
                item.anchor.setTo(0.5, 0.5);
                this.add(item);
                this._itemsToCheckArr.push(item);
            }
            this.game.time.events.add(Phaser.Timer.SECOND * 5, function () {
                this._gamesControler.drageStop = false;
                this.visible = false;
                this.blokada(false);
            }, this);
        };
        DrawItemView.prototype.chechCorrectAnswer = function () {
            var maxCorect = this._itemsToCheckArr.length;
            var _currentCount = 0;
            for (var j = 0; j < this._itemsToCheckArr.length; j++) {
                for (var i = 0; i < this._dropItemArr.length; i++) {
                    if (this._dropItemArr[i].name == this._itemsToCheckArr[j].name && this._dropItemArr[i].x == this._itemsToCheckArr[j].x && this._dropItemArr[i].y == this._itemsToCheckArr[j].y && this._dropItemArr[i].rotation == this._itemsToCheckArr[j].rotation && this._dropItemArr[i].scale.x == this._itemsToCheckArr[j].scale.x) {
                        _currentCount++;
                    }
                }
            }
            if (maxCorect == _currentCount) {
                this._gamesControler.endGame(true);
                this.clearItem();
            }
            else
                this._gamesControler.endGame(false);
        };
        DrawItemView.prototype.addItem = function (item) {
            var isFind = false;
            for (var i = 0; i < this._dropItemArr.length; i++) {
                console.log(this._dropItemArr[i].x, item.x);
                if (this._dropItemArr[i].x == item.x && this._dropItemArr[i].y == item.y && this._dropItemArr[i] != item) {
                    console.log("niby false");
                    item.kill();
                    item = null;
                    isFind = true;
                    return false;
                }
            }
            if (!isFind) {
                this._dropItemArr.push(item);
                item.isAdded = true;
                item.addStartPosition(new Phaser.Point(item.x, item.y));
                item.setDrag();
            }
            return true;
        };
        DrawItemView.prototype.removeItem = function (item) {
            for (var i = 0; i < this._dropItemArr.length; i++) {
                if (this._dropItemArr[i] == item) {
                    this._dropItemArr.slice(i, 1);
                    item.destroy(true);
                    return 0;
                }
            }
        };
        DrawItemView.prototype.clearItem = function () {
            for (var i = 0; i < this._dropItemArr.length; i++) {
                //this._gamesControler.level._itemsDropGr.removeChild(this._dropItemArr[0]);
                this._dropItemArr[0].destroy(true);
                this._dropItemArr.splice(0, 1);
            }
            this.forEach(function (item) {
                // item.kill();
                if (item instanceof Skate.ShapeToDrag)
                    item.kill();
            }, this);
            this._dropItemArr = null;
            this._dropItemArr = new Array();
        };
        return DrawItemView;
    })(Phaser.Group);
    Skate.DrawItemView = DrawItemView;
})(Skate || (Skate = {}));
var Skate;
(function (Skate) {
    var FinishLevel = (function (_super) {
        __extends(FinishLevel, _super);
        function FinishLevel(game) {
            _super.call(this, game);
            this._gameControl = Skate.GameControler.getInstance();
            this._game = game;
            this.create();
        }
        FinishLevel.prototype.create = function () {
            this._finishScreen = new Phaser.Sprite(this._game, 0, 0, "scoreScreen");
            this._finishScreen.animations.add("play");
            this._finishScreen.play("play", 24, true);
            this.addChild(this._finishScreen);
            this._playButton = new Phaser.Button(this._game, 0, 0, "playScoreScreen", this.playAgain, this, 1, 0, 1, 0);
            this._quit = new Phaser.Button(this._game, 300, 10, "playScoreScreen", this.quitClick, this, 3, 2, 3, 2);
            this._playButton.x = 257;
            this._playButton.y = 358;
            this._quit.x = 427;
            this._quit.y = 358;
            this.addChild(this._playButton);
            this.addChild(this._quit);
        };
        FinishLevel.prototype.playAgain = function () {
            //this._gameControl.level.playAgain();
            this.removeAll();
            this.destroy(true);
            console.log("play again");
        };
        FinishLevel.prototype.quitClick = function () {
            console.log("quit");
        };
        return FinishLevel;
    })(Phaser.Group);
    Skate.FinishLevel = FinishLevel;
})(Skate || (Skate = {}));
var Skate;
(function (Skate) {
    var FinishScreen = (function (_super) {
        __extends(FinishScreen, _super);
        function FinishScreen(game) {
            _super.call(this, game);
            this._gameControl = Skate.GameControler.getInstance();
            this._failCount = 0;
            this._correctCount = 0;
            this.game = game;
            this.create();
        }
        FinishScreen.prototype.create = function () {
            this._bgBlack = new Phaser.Graphics(this.game, 0, 0);
            this._bgBlack.beginFill(0x000000, 0.6);
            this._bgBlack.drawRect(0, 0, this.game.width, this.game.height);
            this._bgBlack.endFill();
            this._bgSpr = new Phaser.Sprite(this.game, 0, 0, "");
            this._bgSpr.addChild(this._bgBlack);
            this.add(this._bgSpr);
            this._bgSpr.buttonMode = true;
            this._bgSpr.inputEnabled = true;
            this._bgSpr.input.priorityID = 10;
            this._bgSpr.events.onInputDown.add(this.onDown, this);
            this.gameOverSpr = new Skate.PopUp(this.game, this, 0, 0);
            this.youWinSpr = new Skate.PopUpWin(this.game, this, 0, 0);
            // this.visible = 
            this.add(this.gameOverSpr);
            this.addChild(this.youWinSpr);
            /*
                        this._finishScreen = new Phaser.Sprite(this.game, 0, 0, "scoreScreen");
                        this._finishScreen.animations.add("play");
                        this._finishScreen.play("play", 24, true);
                        this._finishScreen.x = this.game.width / 2 - this._finishScreen.width / 2;
                        this._finishScreen.y = this.game.height / 2 - this._finishScreen.height / 2;
            
                        
                        this.addChild(this._finishScreen);
            
                        this._playButton = new Phaser.Button(this.game, 0, 0, "playScoreScreen", this.playAgain, this, 1, 0, 1, 0);
            
                        this._quit = new Phaser.Button(this.game, 300, 10, "playScoreScreen", this.quitClick, this, 3, 2, 3, 2);
                        this._playButton.x = 257;
                        this._playButton.y = 358;
                        this._playButton.input.priorityID = 10;
                        this._quit.input.priorityID = 10;
                        this._quit.x = 427;
                        this._quit.y = 358;
                        this._finishScreen.addChild(this._playButton);
                        this._finishScreen.addChild(this._quit);
            
            
                        this._finishScreen.visible = false;
                        this._playButton.visible = false;
            
                        this._quit.visible = false;
                        */
        };
        FinishScreen.prototype.onDown = function () {
            console.log("ondown finish");
        };
        FinishScreen.prototype.fail = function () {
            console.log("fail");
            this._failCount++;
            this._audio = this.game.add.audio("failad" + this._failCount, 1, false);
            // this._audio.onStop.add(this.stopAudio, this);
            this._audio.play();
            this._anim = new Phaser.Sprite(this.game, 0, 0, "fail1", 0);
            this._anim.animations.add("play");
            this._anim.x = this.game.width / 2 - this._anim.width / 2;
            this._anim.y = this.game.height / 2 - this._anim.height / 2;
            //  this._anim.animations.getAnimation("play").onComplete.add(this.completeAnim, this);
            this.add(this._anim);
            this._anim.animations.play("play", 24, false, true);
            if (this._failCount == 3) {
                this.screenWinOrGameOver = new Skate.PopUp(this.game, this, 0, 0);
                this._failCount = 1;
                this._audio.onStop.add(this.stopAudio, this);
                this._anim.animations.getAnimation("play").onComplete.add(this.completeAnim, this);
            }
            else {
                this._audio.onStop.add(this.playAgain, this);
            }
        };
        FinishScreen.prototype.completeAnimMini = function () {
            console.log("mini win anim end");
            this.playAgain();
        };
        FinishScreen.prototype.win = function () {
            this._correctCount++;
            APIsetLevel(userID, gameID, userLevel + 1);
            if (this._correctCount == 5)
                this._correctCount = 1;
            this._audio = this.game.add.audio("congrats" + this._correctCount, 1, false);
            this._audio.onStop.add(this.stopAudio, this);
            this._audio.play();
            this._anim = new Phaser.Sprite(this.game, 0, 0, "success1", 0);
            this._anim.animations.add("play");
            this._anim.x = this.game.width / 2 - this._anim.width / 2;
            this._anim.y = this.game.height / 2 - this._anim.height / 2;
            //%  this._anim.animations.getAnimation("play").onComplete.add(this.completeAnim, this);
            // this._gameControl.nextLevel();
            this.add(this._anim);
            this._anim.animations.play("play", 12, false, true);
            this.screenWinOrGameOver = new Skate.PopUpWin(this.game, this, 0, 0);
        };
        FinishScreen.prototype.miniwin = function () {
            console.log("przed Animacja");
            this._correctCount++;
            //APIsetLevel(userID, gameID, userLevel+1);
            if (this._correctCount == 5)
                this._correctCount = 1;
            this._audio = this.game.add.audio("congrats" + this._correctCount, 1, false);
            //     this._audio.onStop.add(this.stopAudio, this);
            this._audio.play();
            this._anim = new Phaser.Sprite(this.game, 0, 0, "success1", 0);
            this._anim.animations.add("play");
            this._anim.x = this.game.width / 2 - this._anim.width / 2;
            this._anim.y = this.game.height / 2 - this._anim.height / 2;
            this._anim.animations.getAnimation("play").onComplete.add(this.completeAnimMini, this);
            // this._gameControl.nextLevel();
            this.add(this._anim);
            this._anim.animations.play("play", 12, false, true);
            // this.screenWinOrGameOver = new PopUpWin(this.game, this, 0, 0);
        };
        FinishScreen.prototype.hideThis = function () {
        };
        FinishScreen.prototype.stopAudio = function () {
            this.completeAnim();
        };
        FinishScreen.prototype.completeAnim = function () {
            this.screenWinOrGameOver.activeButton();
            this.game.time.events.add(Phaser.Timer.SECOND * 2, function () {
                this.addChild(this.screenWinOrGameOver);
                this.screenWinOrGameOver.activeButton();
                this.screenWinOrGameOver.visible = true;
            }, this);
        };
        FinishScreen.prototype.show = function (youWin) {
            console.log("win lose", youWin);
            if (youWin) {
                this.win();
            }
            else {
                this.fail();
            }
        };
        FinishScreen.prototype.playAgain = function () {
            if (this.screenWinOrGameOver != null) {
                this.screenWinOrGameOver.kill();
            }
            this._gameControl.level.clearBtnClick();
            /*  this._quit.kill();
              this._playButton.kill()
              this._finishScreen.kill();
              this.removeChild(this._bgBlack);
              this.removeChild(this._bgSpr);
              this._bgSpr.kill();
              this._audio = null;
              //this.destroy(true);
              // this.removeAll(true);
              this._gameControl.level.playAgain();
              console.log("play again");
             */
            /*this.removeChild(this._bgBlack);
            this.removeChild(this._bgSpr);
            this._bgSpr.kill();
            this.removeChild(this.gameOverSpr);
            this.removeChild(this.youWinSpr);
            this.gameOverSpr.kill();
            this.youWinSpr.kill();*/
            //this.destroy(true);
            // this.removeAll(true);
            this.visible = false;
            this._gameControl.level.playAgain();
        };
        FinishScreen.prototype.hideScreenAfterFinishPlaye = function () {
            this.playAgain();
        };
        FinishScreen.prototype.quitClick = function () {
            console.log("quit");
        };
        FinishScreen.prototype.countFrames = function (start, end) {
            var countArr = new Array();
            for (var i = start; i < end; i++) {
                countArr.push(i);
            }
            return countArr;
        };
        return FinishScreen;
    })(Phaser.Group);
    Skate.FinishScreen = FinishScreen;
})(Skate || (Skate = {}));
var Skate;
(function (Skate) {
    var PopUp = (function (_super) {
        __extends(PopUp, _super);
        function PopUp(game, level, x, y) {
            _super.call(this, game, x, y);
            this.level1 = level;
            this._game = game;
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var bgPopup = new Phaser.Sprite(this.game, 0, 0, "popup");
            this.addChild(bgPopup);
            this.logo = new Phaser.Sprite(this.game, 0, 0, "logoGame");
            this.addChild(this.logo);
            this.logo.x = (bgPopup.width / 2 - this.logo.width / 2);
            this.logo.y = this.logo.height - 20;
            var text = "Good try.\n Keep playing to win credits";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this.game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;
            textDescription.x = this.game.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 100 + (140 / 2 - textDescription.height / 2);
            this.addChild(textDescription);
            this.textLevel();
            //this.beatText();
            this.addButtons();
            this.visible = false;
        }
        PopUp.prototype.addButtons = function () {
            this.gamesBtn = new Phaser.Button(this._game, 0, 0, "games", this.gamesBtnClick, this, 1, 0);
            this.playBtn = new Phaser.Button(this._game, 100, 0, "playGame", this.playBtnClick, this, 1, 0);
            this.gamesBtn.inputEnabled = true;
            this.gamesBtn.input.priorityID = 10;
            this.playBtn.inputEnabled = true;
            this.playBtn.input.priorityID = 10;
            var buttonsGr = new Phaser.Group(this._game);
            this.addChild(buttonsGr);
            buttonsGr.add(this.gamesBtn);
            buttonsGr.add(this.playBtn);
            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;
        };
        PopUp.prototype.activeButton = function () {
            this.gamesBtn.inputEnabled = true;
            this.playBtn.inputEnabled = true;
        };
        PopUp.prototype.gamesBtnClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        PopUp.prototype.playBtnClick = function () {
            this.visible = false;
            this.gamesBtn.inputEnabled = false;
            //   this.gamesBtn.input.priorityID = 3;
            this.playBtn.inputEnabled = false;
            // this.playBtn.input.priorityID = 3;
            this.level1.hideScreenAfterFinishPlaye();
            //  this.game.state.start("PopupStart", true, false);
        };
        PopUp.prototype.textLevel = function () {
            var text = "GAME OVER";
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this._game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = this.game.stage.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 25;
            this.addChild(textDescription);
        };
        return PopUp;
    })(Phaser.Sprite);
    Skate.PopUp = PopUp;
})(Skate || (Skate = {}));
function launchGame(result) {
    userLevel = result.level;
}
//declare function launchGame(result);
var Skate;
(function (Skate) {
    var PopupStart = (function (_super) {
        __extends(PopupStart, _super);
        function PopupStart() {
            _super.apply(this, arguments);
        }
        PopupStart.prototype.create = function () {
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var background = this.add.sprite(0, 0, 'mainMenuBg');
            var bgPopup = this.add.sprite(0, 0, "popup");
            this.logo = this.add.sprite(0, 0, "logoGame");
            this.logo.x = (bgPopup.width / 2 - this.logo.width / 2);
            this.logo.y = this.logo.height - 20;
            var text = "Pay close attention to the design on the skateboard.";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;
            textDescription.x = this.game.width / 2 - textDescription.wordWrapWidth / 2 + 30;
            textDescription.y = this.logo.y + this.logo.height + 100 + (140 / 2 - textDescription.height / 2);
            this.textLevel();
            //this.beatText();
            this.addButtons();
        };
        PopupStart.prototype.addButtons = function () {
            var gamesBtn = this.add.button(0, 0, "games", this.gamesBtnClick, this, 1, 0);
            var playBtn = this.add.button(100, 0, "playGame", this.playBtnClick, this, 1, 0);
            var buttonsGr = this.add.group();
            buttonsGr.add(gamesBtn);
            buttonsGr.add(playBtn);
            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;
        };
        PopupStart.prototype.gamesBtnClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        PopupStart.prototype.playBtnClick = function () {
            this.overButtonA.play();
            this.game.state.start("Instruction", true, false);
        };
        PopupStart.prototype.beatText = function () {
            var text = "BEAT THIS LEVEL";
            //" 6";
            var style = { font: "bold 13px Arial", fill: "#660000", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = (this.game.width / 2) + (140) - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 100;
            var numberText = "6";
            var styleNumber = { font: "bold 17px Arial", fill: "#990000", align: "center" };
            var numberShow = this.add.text(0, 0, numberText, styleNumber);
            numberShow.x = textDescription.x + textDescription.width / 2 - numberShow.width / 2;
            numberShow.y = textDescription.y + textDescription.height;
            var tunesText = "MORE TUNES TO WIN";
            var tunesShow = this.add.text(0, 0, tunesText, style);
            tunesShow.wordWrap = true;
            tunesShow.wordWrapWidth = 100;
            tunesShow.x = textDescription.x + textDescription.width / 2 - tunesShow.width / 2;
            tunesShow.y = numberShow.y + numberShow.height;
            var creditsNmText = "25";
            var styleCredits = { font: "bold 17px Arial", fill: "#ff0000", align: "center" };
            var credistNmShow = this.add.text(0, 0, creditsNmText, styleCredits);
            credistNmShow.x = tunesShow.x + tunesShow.width / 2 - credistNmShow.width / 2;
            credistNmShow.y = tunesShow.y + tunesShow.height;
            var creditsText = "CREDITS";
            var credistShow = this.add.text(0, 0, creditsText, style);
            credistShow.x = tunesShow.x + tunesShow.width / 2 - credistShow.width / 2;
            credistShow.y = credistNmShow.y + credistNmShow.height;
        };
        PopupStart.prototype.textCredits = function () {
            var text = "Beat this level";
            //" 6";
            var style = { font: "bold 14px Arial", fill: "#660000", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = (this.game.width / 2) + 257 / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 100;
            var numberText = "6";
            var styleNumber = { font: "bold 18px Arial", fill: "#990000", align: "center" };
            var numberShow = this.add.text(0, 0, numberText, styleNumber);
            numberShow.x = textDescription.x + textDescription.width / 2 - numberShow.width / 2;
            numberShow.y = textDescription.y + textDescription.height;
        };
        PopupStart.prototype.textLevel = function () {
            //APIgetLevel(userID, gameID);
            var text = "LEVEL " + userLevel;
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = this.game.stage.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 25;
        };
        return PopupStart;
    })(Phaser.State);
    Skate.PopupStart = PopupStart;
})(Skate || (Skate = {}));
var Skate;
(function (Skate) {
    var PopUpWin = (function (_super) {
        __extends(PopUpWin, _super);
        function PopUpWin(game, level, x, y) {
            _super.call(this, game, x, y);
            this._game = game;
            this.level1 = level;
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var bgPopup = new Phaser.Sprite(this.game, 0, 0, "popup");
            this.addChild(bgPopup);
            this.logo = new Phaser.Sprite(this.game, 0, 0, "logoGame");
            this.addChild(this.logo);
            this.logo.x = (bgPopup.width / 2 - this.logo.width / 2);
            this.logo.y = this.logo.height - 20;
            var text = "You finish the level.\n Keep Playing to win credits!";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this.game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;
            textDescription.x = this.game.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 100 + (140 / 2 - textDescription.height / 2);
            this.addChild(textDescription);
            this.textLevel();
            //this.beatText();
            this.addButtons();
            this.visible = false;
        }
        PopUpWin.prototype.addButtons = function () {
            this.gamesBtn = new Phaser.Button(this._game, 0, 0, "games", this.gamesBtnClick, this, 1, 0);
            this.playBtn = new Phaser.Button(this._game, 100, 0, "playGame", this.playBtnClick, this, 1, 0);
            this.gamesBtn.inputEnabled = true;
            this.gamesBtn.input.priorityID = 10;
            this.playBtn.inputEnabled = true;
            this.playBtn.input.priorityID = 10;
            var buttonsGr = new Phaser.Group(this._game);
            this.addChild(buttonsGr);
            buttonsGr.add(this.gamesBtn);
            buttonsGr.add(this.playBtn);
            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;
        };
        PopUpWin.prototype.activeButton = function () {
            this.gamesBtn.inputEnabled = true;
            this.playBtn.inputEnabled = true;
        };
        PopUpWin.prototype.gamesBtnClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        PopUpWin.prototype.playBtnClick = function () {
            this.visible = false;
            this.gamesBtn.inputEnabled = false;
            this.gamesBtn.input.priorityID = 3;
            this.playBtn.inputEnabled = false;
            this.playBtn.input.priorityID = 3;
            this.level1.hideScreenAfterFinishPlaye();
            //  this.game.state.start("PopupStart", true, false);
        };
        PopUpWin.prototype.textLevel = function () {
            var text = "GOOD JOB";
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this._game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = this.game.stage.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 25;
            this.addChild(textDescription);
        };
        return PopUpWin;
    })(Phaser.Sprite);
    Skate.PopUpWin = PopUpWin;
})(Skate || (Skate = {}));
var Skate;
(function (Skate) {
    var PromptView = (function (_super) {
        __extends(PromptView, _super);
        function PromptView(game, parent) {
            _super.call(this, game);
            game.add.existing(this);
            this._parent = parent;
            this._kayak = this.game.add.sprite(0, 0, 'prompt', 0);
            // this._kayak.animations.add('anim');
            this._kayak.scale.setTo(0.6, 0.6);
            this.addChild(this._kayak);
            var style = { font: "18px Arial", fill: "#FFFFFF", align: "left" };
            this._textView = this.game.add.text(17, 75, "", style);
            this.addChild(this._textView);
            this.visible = false;
            this.y = this.game.height - this.height;
            this.x = this.game.width - this.width;
        }
        PromptView.prototype.playIntro = function () {
            // this._parent.blokada(true);
            this.visible = true;
            // this._kayak.animations.play('anim', 31, true, false);
            this._textView.setText("Get ready to shred! Collect the red letters only. remember the order and sounds of the letters, and then combine them to make a word. be careful don't wipe out on anything!!");
            this._audio = this.game.add.audio('WakeIntro', 1, false);
            this._audio.play();
            this._audio.onStop.addOnce(this.onAudioComplete, this);
        };
        PromptView.prototype.playFeedbackGood = function (nextLevel) {
            if (nextLevel === void 0) { nextLevel = false; }
            if (!nextLevel) {
                //  this._parent.blokada(true);
                this.visible = true;
                this._kayak.animations.play('anim', 31, true, false);
                this._textView.setText("Holy Moley!  I'm Impressed!\nThe Next Sequence is...");
                this._audio = this.game.add.audio('WakeSuccess0', 1, false);
                this._audio.play();
                this._audio.onStop.addOnce(this.onAudioComplete, this);
            }
            else {
            }
        };
        PromptView.prototype.playFeedbackWrong = function (end) {
            if (end === void 0) { end = false; }
            //   this._parent.blokada(true);
            this.visible = true;
            this._kayak.animations.play('anim', 31, true, false);
            this._textView.setText("That was a fair go, but let's try again.\nHave you found a way to remember\nthe sounds yet?");
            this._audio = this.game.add.audio('WakeWrong0', 1, false);
            this._audio.play();
            this._audio.onStop.addOnce(this.onAudioComplete, this);
            if (end) {
                this._audio.onStop.addOnce(this.onAudioCompleteEnd, this);
            }
            else {
                this._audio.onStop.addOnce(this.onAudioComplete, this);
            }
        };
        PromptView.prototype.onAudioComplete = function () {
            this.visible = false;
            //this._parent.onPromptAudioComplete();
        };
        PromptView.prototype.onAudioCompleteNextLevel = function () {
            this.visible = false;
            //  this._parent.showFinishLevel();
        };
        PromptView.prototype.onAudioCompleteEnd = function () {
            this.visible = false;
            //  this._parent.showFinish();
        };
        return PromptView;
    })(Phaser.Group);
    Skate.PromptView = PromptView;
})(Skate || (Skate = {}));
//# sourceMappingURL=game.js.map