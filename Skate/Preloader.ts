﻿module Skate {

    export class Preloader extends Phaser.State {


        preload() {
          
            var bg = this.add.image(0, 0, "bg", 0);

            var blackProgress = this.add.image(0, 0, "progressBarBlack", 0);
            var blueProgress = this.add.sprite(0, 0, "progressBarBlue", 0);


            blackProgress.position.setTo(this.stage.width / 2 - blackProgress.width / 2, this.stage.height - blackProgress.height - 10);

            blueProgress.position = blackProgress.position;
            this.load.setPreloadSprite(blueProgress);


            this.load.image("mainMenuBg", "assets/mainMenu/splashScreen.png");
            this.load.atlasJSONArray('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');
            this.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
            this.load.audio("MainMenuSn", "assets/audio/SCThemeMusic.mp3");
            this.load.audio("overButtonA", "assets/audio/overButton.mp3");
            this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");


            var mobile: boolean;
            mobile = false
            if (!this.game.device.desktop) {
                mobile = true;
            }


            if (mobile) {
                this.load.image("firstInstruction", "assets/03_scate_create.png");
                this.load.audio("instructionSn", "assets/audio/skate_create_02.mp3");

            }
            else {
                this.load.image("firstInstruction", "assets/02_scate_create.png");
                this.load.audio("instructionSn", "assets/audio/skate_create_01.mp3");
            }




            this.load.image("popup", "assets/popup/popup.png");
            this.load.image("logoGame", "assets/popup/logo.png");
            this.load.atlasJSONArray('games', 'assets/popup/games.png', 'assets/popup/games.json');
            this.load.atlasJSONArray('playGame', 'assets/popup/play.png', 'assets/popup/play.json');
            this.load.atlasJSONArray('helpBtn', 'assets/popup/help.png', 'assets/popup/help.json');
            

            this.load.image("bgGame", "assets/skate/gameBackground.png");


            /* instructions */
            this.load.image('prompt', 'assets/snow/prompt/instruction1.png');
            this.load.audio("WakeIntro", "assets/audio/instruction/WakeIntro.mp3");
            this.load.audio("WakeSuccess0", "assets/audio/instruction/WakeSuccess0.mp3");
            this.load.audio("WakeWrong0", "assets/audio/instruction/WakeWrong0.mp3");

            
            this.load.image("firstInstruction", "assets/instructionsScreen.png");
            this.load.image("sequence", "assets/snow/letterClip.png");
            /* player */

       
            this.load.atlasJSONArray('trash', 'assets/skate/trash.png', 'assets/skate/trash.json');
            this.load.atlasJSONArray('flip', 'assets/skate/flipBtn.png', 'assets/skate/flipBtn.json');
            this.load.atlasJSONArray('rotation', 'assets/skate/rotateRightBtn.png', 'assets/skate/rotateRightBtn.json');

            this.load.image("rotationDrag", "assets/skate/rotation.png");
          
            this.load.atlasJSONArray('countdown', 'assets/skate/countdown.png', 'assets/skate/countdown.json');

            
            this.load.atlasJSONArray('zoom', 'assets/skate/enlarge.png', 'assets/skate/enlarge.json');

           // this.load.image("clear", "assets/skate/clearBtn.png");
            //this.load.image("finish", "assets/skate/finishBtn.png");

            this.load.atlasJSONArray('clear', 'assets/skate/clearBtn.png', 'assets/skate/clearBtn.json');
            this.load.atlasJSONArray('finish', "assets/skate/finishBtn.png", "assets/skate/finishBtn.json");
           // this.load.image("rotationDrag", "assets/skate/rotation.png");

            for (var i: number = 0; i < 10; i++) {

                this.load.image("Shape" + i, "assets/skate/items/Shape" + i + ".png");


            }


            /* obstacle */

        /*    this.load.image("piasek", "assets/water/obstacle/wood2.png");
            this.load.image("wood", "assets/water/obstacle/wood.png");
            this.load.image("hammer", "assets/water/obstacle/hammer.png");
            this.load.image("turtle", "assets/water/obstacle/turtle.png");
            this.load.image("zielone", "assets/water/obstacle/zielone.png");
            this.load.image("ball", "assets/water/obstacle/ball.png");
            this.load.image("ball2", "assets/water/obstacle/ball2.png");
            this.load.image("walen", "assets/water/obstacle/walen.png");
            */
          

          //  this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");

         

         /*   for (var i: number = 1; i < 5; i++) {

                this.load.audio("failaudio" + i, "assets/audio/instruction/fail" + i + ".mp3");


            }*/

           /* for (var i: number = 1; i < 5; i++) {

                this.load.audio("success" + i, "assets/audio/instruction/success" + i + ".mp3");


            }
            */
        /*    for (var i: number = 1; i < 4; i++) {
                this.load.image("item"+i, "assets/robo/item"+i+".PNG");
            }

            for (var i: number = 1; i < 4; i++) {
                this.load.image("robot" + i, "assets/robo/robot" + i + ".PNG");
            }
            */
          
            /*skrzynie*/

           

        

            /*end skrzynie"*/


            this.load.atlasJSONArray("scoreScreen", "assets/scoreScreen/scoreScreen.png", "assets/scoreScreen/scoreScreen.json");
            this.load.atlasJSONArray("playScoreScreen", "assets/scoreScreen/scoreScreenButtons.png", "assets/scoreScreen/scoreScreenButtons.json");


            for (var i: number = 1; i < 6; i++) {

                this.load.atlasJSONArray("fail" + i, "assets/feedbacks/fail" + i + ".png", "assets/feedbacks/fail" + i + ".json");



            }


            for (var i: number = 1; i < 3; i++) {

                this.load.atlasJSONArray("success" + i, "assets/feedbacks/success" + i + ".png", "assets/feedbacks/success" + i + ".json");



            }

            for (var i: number = 1; i < 5; i++) {
                this.load.audio("congrats" + i, "assets/feedbacks/congrats" + i + ".mp3");



            }

            this.load.audio("failad1", "assets/feedbacks/failure1.mp3");
            this.load.audio("failad2", "assets/feedbacks/failure2.mp3");


        }


        create() {
           
            this.game.state.start("MainMenu", true, false);

        }

    }


} 