﻿module Skate {

    export class Trash extends Phaser.Sprite {
        private levelStage:Level
        constructor(game: Phaser.Game, x: number, y: number, key: string,level:Level) {


            super(game, x, y, key);
            this.levelStage = level;
            //this.physicsEnabled = true;

            this.inputEnabled = true;
            this.events.onInputOver.add(this.over, this);
            this.events.onInputOut.add(this.out, this);

        }

        over() {

            this.frame = 1;
        }

        out() {

            this.frame = 0;
        }
    }


} 