﻿module Skate {

    export class RotationBtn extends Phaser.Button{

        private _drag: Phaser.Sprite;
        private _gamesControler: GameControler = GameControler.getInstance();
        private _clicked: boolean = false;
        constructor(game: Phaser.Game, x: number, y: number, key: string) {


            super(game, x, y, key);
            
            this.setFrames(2, 0, 1);
            this.inputEnabled = true;
            //this.physicsEnabled = true;
            this._drag = new Phaser.Sprite(this.game, 0, 0, "rotationDrag");
            this._drag.anchor.setTo(0.5, 0.5);
            this.events.onInputDown.add(this.onDown, this);

        
           
        }

        private onDown() {
            //  this._level.onShapeDown(this);
            this._clicked = (!this._clicked) ? true : false;
            if (this._clicked) {

                if (this._gamesControler.lastDragButton != null) this._gamesControler.lastDragButton.setDefault();
                this._gamesControler.lastDragButton = this;
                this._gamesControler.dragButtonType = "rotation";
                this._gamesControler.dragButton = true;
                this.frame = 1;

            }
            else {
                this._gamesControler.dragButton = false;
                this._gamesControler.dragButtonType = "";
                this.frame = 0;
            }
           // this._gamesControler.level.addButton(this._drag);
             
           
        }

        setDefault() {

            this.frame = 0;
            this._gamesControler.dragButtonType = "";
            this._gamesControler.dragButton = false;
            this._clicked = false;
        }

        private onStartDrag() {
            //this._level.onShapeDragStart(this);
            // console.log("i co teraz");
        }

        private onStopDrag() {
         

        }

        addObjectToDrag(): Phaser.Sprite {



          

            return this._drag;

        }

    }


} 