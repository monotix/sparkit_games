﻿module Skate {

    export class FlipBtn extends Phaser.Button {

        private _drag: Phaser.Sprite;
        private _gamesControler: GameControler = GameControler.getInstance();
        private _clicked: boolean = false;
        constructor(game: Phaser.Game, x: number, y: number, key: string) {


            super(game, x, y, key);
            this.inputEnabled = true;
            //this.physicsEnabled = true;
          //  this.setFrames(2, 0, 1);
            this._drag = new Phaser.Sprite(this.game, 0, 0, "rotationDrag");
            this._drag.anchor.setTo(0.5, 0.5);
            this.events.onInputDown.add(this.onDown, this);



        }

        private onDown() {
            //  this._level.onShapeDown(this);
            this._clicked = (!this._clicked) ? true : false;
            if (this._clicked) {

                if (this._gamesControler.lastDragButton != null) this._gamesControler.lastDragButton.setDefault();
                this.frame = 1;
                this._gamesControler.lastDragButton = this;
                this._gamesControler.dragButtonType = "flip";
                this._gamesControler.dragButton = true;

            }
            else {
                this._gamesControler.dragButton = false;
                this._gamesControler.dragButtonType = "";
                this.frame = 0;
            }
          //  this._gamesControler.level.addButton(this._drag);


        }

        setDefault() {

            this.frame = 0;
            this._gamesControler.dragButton = false;
            this._gamesControler.dragButtonType = "";
            this._clicked = false;

        }

        private onStartDrag() {
            //this._level.onShapeDragStart(this);
            // console.log("i co teraz");
        }

        private onStopDrag() {


        }

        addObjectToDrag(): Phaser.Sprite {





            return this._drag;

        }

    }


}  