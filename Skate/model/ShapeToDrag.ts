﻿module Skate {

    export class ShapeToDrag extends Phaser.Sprite {

      
        private _positionStart: Phaser.Point;
        public isAdded: boolean = false;
        private _level: Level;
        private _gamesControler: GameControler = GameControler.getInstance();
        constructor(game: Phaser.Game, x: number, y: number, key: string, level: Level, scalex: number = 1, scaley: number = 1, rotation:number = 0) {

            super(game, x, y, key);
            this.name = key;
            this.scale.setTo(scalex, scaley);
            this.rotation = rotation;

            this._level = level;
          //  this._shape = new Phaser.Sprite(game, x, y, key);

        }

        addObjectToDrag(): ShapeToDrag {



            var spr = new ShapeToDrag(this.game, 0, 0, this.name,this._level);
            spr.anchor.setTo(0.5, 0.5);

            return spr;

        }

        addStartPosition(point: Phaser.Point) {

            this._positionStart = point;
        }

        goToStartPosition() {

            this.x = this._positionStart.x;
            this.y = this._positionStart.y;

        }


        setDrag() {
            this.inputEnabled = true;
         
            this.events.onInputDown.add(this.onDown, this);

        }

        private wasDrag: boolean = false;
        private onDown() {
          //  this._level.onShapeDown(this);
            console.log("i co teraz on down");
            if (this._gamesControler.dragButton) {

                this.input.disableDrag();
                this.changeItemView(this._gamesControler.dragButtonType);

            }
            else if(!this.wasDrag) {
                this.input.enableDrag();
                this.events.onDragStart.add(this.onStartDrag, this);
                this.events.onDragStop.add(this.onStopDrag, this);
                this.wasDrag = true;
            }
        }

        changeItemView(whatType: string) {

            if (whatType == "rotation") {

                this.rotation += 45;

            }
            else if (whatType == "zoom") {
                var skala = 1;
                if (this.scale.x < 0) {
                    skala = -1;
                }
                
                var nowaSkala:number = 1;
                if (this.scale.x == 1.2 || this.scale.x == -1.2) {

                    nowaSkala = 1;

                }
                else {
                    nowaSkala = 1.2;

                }
                this.scale.setTo(nowaSkala * skala, nowaSkala);
            }
            else if ("flip") {

                if (this.scale.x > 0) {
                    this.scale.setTo(-this.scale.x, this.scale.y);
                }
                else {
                    this.scale.setTo(this.scale.x, this.scale.y);
                }


            }



        }

        private onStartDrag() {
            //this._level.onShapeDragStart(this);
           // console.log("i co teraz");
        }

        private onStopDrag() {
            this._level.onShapeDragStop(this);
        }

    }


}