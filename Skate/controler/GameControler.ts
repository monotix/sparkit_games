﻿module Skate {

    export class GameControler {


        public static _instance: GameControler = null;

      

        public finishGame: boolean = false;
        private _currenLevel: number = 0;
        private _level: Level;
     
        public gameOver: boolean = true;
        public maxLevel: number = 3;
        public currentLevel: number = 0;
        public drageStop: boolean = false;
        public dragButton: boolean = false;
        public dragButtonType: string = "";
        public lastDragButton;
        private _deisgnCount: number = 1;
        construct() {

            if (GameControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            GameControler._instance = this;

        }




        public static getInstance(): GameControler {

            if (GameControler._instance === null) {

                GameControler._instance = new GameControler();


            }


            return GameControler._instance;



        }

        public endGame(win: boolean) {
          
            if (win) {
                this.level.addPoints();
                this._deisgnCount++;
                this.level.addDesign(this._deisgnCount);
                this.level.showFinish(true);
            }

            else {
                this.level.showFinish(false);

            }

        }

        public restart() {
            this._currenLevel = 0;
            //this.level.playAgain();
        }

        public nextLevel() {

            this.currentLevel++;

         //   this.level.showPrompt(true, true);

        }

        setLevel(level: Level) {

            this._level = level;

        }

        public get level(): Level {

            return this._level;
        }

        reset() {

            this.drageStop = false;
            this.dragButton = false;
        }
     

    }


}  