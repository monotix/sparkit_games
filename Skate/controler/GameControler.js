var Skate;
(function (Skate) {
    var GameControler = (function () {
        function GameControler() {
            this.SPEEDBOAT = -150;
            this.SPEEDROBOT = 100;
            this.SPEEDX = -150;
            this.SPEEDY = -100;
            this.finishGame = false;
            this._currenLevel = 0;
            this._collisionPlayer = false;
            this.gameOver = true;
            this.maxLevel = 3;
            this.currentLevel = 0;
        }
        GameControler.prototype.construct = function () {
            if (GameControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            GameControler._instance = this;
        };
        Object.defineProperty(GameControler.prototype, "collisionPlayer", {
            get: function () {
                return this._collisionPlayer;
            },
            set: function (isColliding) {
                this._collisionPlayer = isColliding;
            },
            enumerable: true,
            configurable: true
        });
        GameControler.getInstance = function () {
            if (GameControler._instance === null) {
                GameControler._instance = new GameControler();
            }
            return GameControler._instance;
        };
        GameControler.prototype.restart = function () {
            this._currenLevel = 0;
            this.level.playAgain();
        };
        GameControler.prototype.nextLevel = function () {
            this.currentLevel++;
            this.level.showPrompt(true, true);
        };
        GameControler.prototype.setLevel = function (level) {
            this._level = level;
        };
        Object.defineProperty(GameControler.prototype, "level", {
            get: function () {
                return this._level;
            },
            enumerable: true,
            configurable: true
        });
        GameControler._instance = null;
        return GameControler;
    })();
    Skate.GameControler = GameControler;
})(Skate || (Skate = {}));
//# sourceMappingURL=GameControler.js.map