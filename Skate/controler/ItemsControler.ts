﻿module Skate {

    export class ItemsControler {


        public static _instance: ItemsControler = null;



        public finishGame: boolean = false;
        private _currenLevel: number = 0;
        private _level: Level;

        public gameOver: boolean = true;
        public maxLevel: number = 3;
        public currentLevel: number = 0;
        construct() {

            if (ItemsControler._instance) {
                throw new Error("Error: Instantiation failed: Use ItemsControler.getInstance() instead of new.");
            }
            ItemsControler._instance = this;

        }




        public static getInstance(): ItemsControler {

            if (ItemsControler._instance === null) {

                ItemsControler._instance = new ItemsControler();


            }


            return ItemsControler._instance;



        }

       
        level1():Array<any> {
          
            var arr: Array<any> = new Array(
                { name: "Shape1", x: 473.95, y: 132, xscale: 1, yscale: 1, rotation: 0 },
                { name: "Shape2", x: 361.95, y: 131, xscale: 1, yscale: 1, rotation: 0 },
                { name: "Shape3", x: 31, y: 229, xscale: 1, yscale: 1, rotation: 0 }
            )

            return arr;
        }



    }


}   