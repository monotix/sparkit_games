var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Skate;
(function (Skate) {
    var PlayerPlay = (function (_super) {
        __extends(PlayerPlay, _super);
        function PlayerPlay(game) {
            _super.call(this, game);
            this._tween = null;
            this._game = game;
            this._gameControler = Skate.GameControler.getInstance();
            this.enableBody = true;
            this._obstacleControl = Skate.ObstacleControler.getInstance();
            this.game.input.onDown.add(this.movePlayer, this);
            this.addPlayer();
        }
        PlayerPlay.prototype.addPlayer = function () {
            this._player = new Skate.Player(this._game, 0, 0, "");
            this._player.x = 400; // this._player.width - 400
            this._player.y = 300;
            this.add(this._player);
            this._leftRight = this.player.x;
            this._player.body.setSize(60, 20, -100, 0);
            this._player.forward();
        };
        PlayerPlay.prototype.movePlayer = function (pointer) {
            if (!this._gameControler.collisionPlayer) {
                if (this._leftRight < pointer.x) {
                    this._player.right();
                }
                else {
                    this._player.left();
                }
                this._leftRight = pointer.x;
                var posy = pointer.y;
                // if (posy < 300) posy = 300;
                if (posy > 500)
                    posy = 500;
                // if (pointer.x > 630) return;
                this.tweenStop();
                posy = Math.floor(posy);
                var posx = Math.floor(pointer.x);
                if (!this._gameControler.collisionPlayer) {
                    var duration = (this.game.physics.arcade.distanceToPointer(this._player, pointer) / 200) * 1000;
                    this._tween = this.game.add.tween(this._player).to({ y: posy, x: posx }, duration, Phaser.Easing.Linear.None).start();
                    this._tween.onComplete.add(this.endMove, this);
                }
            }
        };
        PlayerPlay.prototype.endMove = function () {
            // this._player.forward();
        };
        PlayerPlay.prototype.changePos = function (pointer) {
            console.log("zmiana pozycji", pointer);
            this.tweenStop();
            if (!this._gameControler.collisionPlayer) {
                // var duration = (this.game.physics.arcade.distanceToPointer(this._player, pointer) / 200) * 1000;
                this._tween = this.game.add.tween(this._player).to({ y: pointer.y, x: pointer.x }, 2, Phaser.Easing.Linear.None).start();
                this._tween.onComplete.add(this.endMove, this);
            }
        };
        PlayerPlay.prototype.tweenStop = function () {
            console.log("tween stop znikam");
            if (this._tween != null) {
                this._tween.stop();
            }
        };
        PlayerPlay.prototype.stopTween = function () {
            console.log("usuwam tween", this._tween != null, this._tween.isRunning);
            if (this._tween != null && this._tween.isRunning) {
                this.game.tweens.remove(this._tween);
            }
        };
        Object.defineProperty(PlayerPlay.prototype, "player", {
            get: function () {
                return this._player;
            },
            enumerable: true,
            configurable: true
        });
        return PlayerPlay;
    })(Phaser.Group);
    Skate.PlayerPlay = PlayerPlay;
})(Skate || (Skate = {}));
//# sourceMappingURL=PlayerPlay.js.map