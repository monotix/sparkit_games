﻿module Skate {

    export class DrawItemView extends Phaser.Group {

        private _itemsControler: ItemsControler = ItemsControler.getInstance();
        private _gamesControler: GameControler = GameControler.getInstance();
        private _itemsToCheckArr: Array<any>;
        private _dropItemArr: Array<any>;
        private _bgBlack: Phaser.Graphics;
        private _bgSpr: Phaser.Sprite;
        private _countdown: Phaser.Sprite;
        constructor(game:Phaser.Game) {

            super(game);

            this.enableBody = true;

            this._bgBlack = new Phaser.Graphics(this.game, 0, 0);
            this._bgBlack.beginFill(0x000000, 0.6);
            this._bgBlack.drawRect(0, 0, this.game.width, this.game.height);
            this._bgBlack.endFill();

            this._bgSpr = new Phaser.Sprite(this.game, 0, 0, "");
            this._bgSpr.addChild(this._bgBlack);
            this.add(this._bgSpr);


            this._bgSpr.buttonMode = true;
            this._bgSpr.inputEnabled = true;
            this._bgSpr.input.priorityID = 10;


            this._countdown = new Phaser.Sprite(this.game, 660, 10, "countdown");
            this._countdown.animations.add("play");
            this.add(this._countdown);
           
        }

        blokada(block:boolean) {

           
                this._bgSpr.buttonMode = block;
                this._bgSpr.inputEnabled = block;

            



        }

        showItem() {

            this._countdown.play("play", 24, false);
            this.blokada(true);
            this._dropItemArr = new Array();
            this._itemsToCheckArr = new Array();
            this._gamesControler.drageStop = true;

            this.visible = true;
            var itemArr: Array<any> = this._itemsControler.level1();

            for (var i: number = 0; i < itemArr.length; i++) {

                var obj = itemArr[i];
                var item: ShapeToDrag = new ShapeToDrag(this.game, obj.x+60, obj.y+110, obj.name,this._gamesControler.level,obj.xscale,obj.yscale,obj.rotation);
                item.anchor.setTo(0.5, 0.5);
                this.add(item);
                this._itemsToCheckArr.push(item);

            }

            this.game.time.events.add(Phaser.Timer.SECOND * 5, function () {

                this._gamesControler.drageStop = false;
                this.visible = false;
                this.blokada(false);


            }, this);

        }


        chechCorrectAnswer() {

            var maxCorect: number = this._itemsToCheckArr.length;
            var _currentCount: number = 0;
           

            for (var j: number = 0; j < this._itemsToCheckArr.length; j++) {

                for (var i: number = 0; i < this._dropItemArr.length; i++) {

                    if (this._dropItemArr[i].name == this._itemsToCheckArr[j].name && this._dropItemArr[i].x == this._itemsToCheckArr[j].x && this._dropItemArr[i].y == this._itemsToCheckArr[j].y && this._dropItemArr[i].rotation == this._itemsToCheckArr[j].rotation && this._dropItemArr[i].scale.x == this._itemsToCheckArr[j].scale.x ) {
                        _currentCount++;
                       // this._dropItemArr[i].x = 0;
                    }

                }

            }
            console.log("_currentCount", _currentCount);
          
            if (maxCorect == _currentCount) this._gamesControler.endGame(true);
            else  this._gamesControler.endGame(false);

        }

        addItem(item):boolean {
            
            var isFind: boolean = false;
            for (var i: number = 0; i < this._dropItemArr.length; i++) {
                console.log(this._dropItemArr[i].x, item.x);
                if (this._dropItemArr[i].x == item.x && this._dropItemArr[i].y == item.y && this._dropItemArr[i] != item) {
                    console.log("niby false");
                    isFind = true;
                    return false;
                }

            }

            if (!isFind) {
                this._dropItemArr.push(item);
                item.isAdded = true;
                item.addStartPosition(new Phaser.Point(item.x, item.y));
                item.setDrag();
            }
            return true;

        }

        removeItem(item) {

            for (var i: number = 0; i < this._dropItemArr.length; i++) {
               
                if (this._dropItemArr[i] == item) {
                    this._dropItemArr.slice(i, 1);
                    item.destroy(true);
                    return 0;
                }

            }
        }

        clearItem() {

           
            for (var i: number = 0; i < this._dropItemArr.length; i++) {

              
                this._dropItemArr[0].destroy(true);
                this._dropItemArr.splice(0, 1);
                   
                    
                   

            }

           this._dropItemArr = new Array();

        }




    }


}
