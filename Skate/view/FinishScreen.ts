﻿module Skate {


    export class FinishScreen extends Phaser.Group {
        private _finishScreen: Phaser.Sprite;
        private _playButton: Phaser.Button;
        private _quit: Phaser.Button;
        private _game: Phaser.Game;
        private _anim: Phaser.Sprite;
        private _gameControl: GameControler = GameControler.getInstance();
        private _bgBlack: Phaser.Graphics;
        private _bgSpr: Phaser.Sprite;
        private _audio: Phaser.Sound;
  
        private _failCount: number = 0;
        private _correctCount: number = 0;
        constructor(game: Phaser.Game) {
            super(game);

            this.game = game;
            this.create();
        }

        create() {



            this._bgBlack = new Phaser.Graphics(this.game, 0, 0);
            this._bgBlack.beginFill(0x000000, 0.6);
            this._bgBlack.drawRect(0, 0, this.game.width, this.game.height);
            this._bgBlack.endFill();
            
            this._bgSpr = new Phaser.Sprite(this.game, 0, 0, "");
            this._bgSpr.addChild(this._bgBlack);
            this.add(this._bgSpr);

           
            this._bgSpr.buttonMode = true;
            this._bgSpr.inputEnabled = true;
            this._bgSpr.input.priorityID = 10;

            this._bgSpr.events.onInputDown.add(this.onDown, this);


          

            this._finishScreen = new Phaser.Sprite(this.game, 0, 0, "scoreScreen");
            this._finishScreen.animations.add("play");
            this._finishScreen.play("play", 24, true);
            this._finishScreen.x = this.game.width / 2 - this._finishScreen.width / 2;
            this._finishScreen.y = this.game.height / 2 - this._finishScreen.height / 2;

            
            this.addChild(this._finishScreen);

            this._playButton = new Phaser.Button(this.game, 0, 0, "playScoreScreen", this.playAgain, this, 1, 0, 1, 0);

            this._quit = new Phaser.Button(this.game, 300, 10, "playScoreScreen", this.quitClick, this, 3, 2, 3, 2);
            this._playButton.x = 257;
            this._playButton.y = 358;
            this._playButton.input.priorityID = 10;
            this._quit.input.priorityID = 10;
            this._quit.x = 427;
            this._quit.y = 358;
            this._finishScreen.addChild(this._playButton);
            this._finishScreen.addChild(this._quit);


            this._finishScreen.visible = false;
            this._playButton.visible = false;

            this._quit.visible = false;
            
        }

        onDown() {
            console.log("ondown finish");
        }

        fail() {
            this._failCount++;
            if (this._failCount == 3) this._failCount = 1;
            this._audio = this.game.add.audio("failad" + this._failCount, 1, false);
            this._audio.onStop.add(this.stopAudio, this);
            this._audio.play();
          
            this._anim = new Phaser.Sprite(this.game, 0, 0, "fail1", 0);
            this._anim.animations.add("play");
            this._anim.x = this.game.width / 2 - this._anim.width / 2;
            this._anim.y = this.game.height / 2 - this._anim.height / 2;
          //  this._anim.animations.getAnimation("play").onComplete.add(this.completeAnim, this);
            this.add(this._anim);
            this._anim.animations.play("play", 24, false, true);
        }

        win() {
            this._correctCount++;
            if (this._correctCount == 5) this._correctCount = 1;
            this._audio = this.game.add.audio("congrats" + this._correctCount, 1, false);
            this._audio.onStop.add(this.stopAudio, this);
            this._audio.play();
            this._anim = new Phaser.Sprite(this.game, 0, 0, "success1", 0);
            this._anim.animations.add("play");
            this._anim.x = this.game.width / 2 - this._anim.width / 2;
            this._anim.y = this.game.height / 2 - this._anim.height / 2;
          //%  this._anim.animations.getAnimation("play").onComplete.add(this.completeAnim, this);
            this.add(this._anim);
            this._anim.animations.play("play", 12, false, true);

        }

        stopAudio() {

            this.completeAnim()
        }

        completeAnim() {
            this.game.time.events.add(Phaser.Timer.SECOND * 2, function () {

                this._finishScreen.visible = true;
                this._playButton.visible = true;

                this._quit.visible = true;




            }, this)


        }

        show(youWin: boolean) {

            if (youWin) {

                this.win();
            }
            else {
                this.fail();

            }
        }

        playAgain() {

            this._gameControl.level.clearBtnClick();
            this._quit.kill();
            this._playButton.kill()
            this._finishScreen.kill();
            this.removeChild(this._bgBlack);
            this.removeChild(this._bgSpr);
            this._bgSpr.kill();
            this._audio = null;
            //this.destroy(true);
            // this.removeAll(true);
            this._gameControl.level.playAgain();
            console.log("play again");

        }

        quitClick() {
            console.log("quit");

        }


        countFrames(start: number, end: number): Array<number> {
            var countArr: Array<number> = new Array();
            for (var i: number = start; i < end; i++) {

                countArr.push(i);


            }

            return countArr;


        }

    }

}   