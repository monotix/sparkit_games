var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Skate;
(function (Skate) {
    var ObstacleView = (function (_super) {
        __extends(ObstacleView, _super);
        function ObstacleView(game) {
            _super.call(this, game);
            this._game = game;
            this._gameControler = Skate.GameControler.getInstance();
            this._letterControler = Skate.LetterControler.getInstance();
            this.enableBody = true;
            this._obstacleControl = Skate.ObstacleControler.getInstance();
            this._boundsOut = new Phaser.Sprite(this._game, 0, 0, "");
            var grfx = new Phaser.Graphics(this.game, 0, 0);
            grfx.beginFill(0x000000, 1);
            grfx.drawRect(0, 0, 100, 100);
            grfx.endFill();
            this._boundsOut.addChild(grfx);
            this._boundsOut.y = this.game.height - 100;
            this._boundsOut.x = this.game.width;
            // this.add(this._boundsOut);
            /*       this._boundsOut.body.velocity.x = -270;//this._gameControler.SPEEDX;
                   this._boundsOut.body.velocity.y = 0;
                   this._boundsOut.body.immovable = true;
                   this._boundsOut.body.checkCollision.none = true;
                   this._boundsOut.checkWorldBounds = true;*/
            // this._boundsOut.events.onOutOfBounds.add(this.getObstacle, this);
        }
        ObstacleView.prototype.randomBoats = function () {
            //     var rand = this.getRandom(this.boatsStrArr.length)
            //     var str: string = this.boatsStrArr[rand];
            //   var y_ = (rand == 0) ? 100 : 50;
            //    var boats: Phaser.Sprite = new Phaser.Sprite(this._game, 0, y_, str, 0);
            //    boats.frame = this.getRandom(boats.animations.frameTotal);
            return;
        };
        ObstacleView.prototype.getObstacle = function () {
            console.log("outofScreen", this._boundsOut.x, this._boundsOut.y);
            this.delObstacles();
            // this._boundsOut.y = this.game.height - 100;
            // this._boundsOut.x = this.game.width;
            this._obstacleArr = new Array();
            this._longerObstacle = new Array();
            //   if (!this._gameControler.collisionPlayer && this._gameControler.SPEEDX != 0) {
            var obstcaleArr = this._obstacleControl.level1();
            var max = obstcaleArr.length;
            for (var i = 0; i < max; i++) {
                var obj = obstcaleArr[i];
                if (obj.name == "popletter") {
                    var popletter = new Skate.PopLetter(this._game, obj.x + 800, obj.y + 320, obj.name);
                    var letter = this._letterControler.getPopLetter();
                    popletter.isRed(letter[0].correct);
                    popletter.addLetter(letter[0].str);
                    this.getTheLonger(popletter.x, popletter.width, i);
                    this.add(popletter);
                    popletter.body.velocity = new Phaser.Point(this._gameControler.SPEEDX, this._gameControler.SPEEDY);
                    this._obstacleArr.push(popletter);
                    popletter.body.setSize(60, 50, 20, 0);
                }
                else {
                    var obstacle = new Skate.Obstacle(this._game, obj.x + 800, obj.y + 320, obj.name);
                    this.getTheLonger(obstacle.x, obstacle.width, i);
                    this.add(obstacle);
                    obstacle.body.velocity = new Phaser.Point(this._gameControler.SPEEDX, this._gameControler.SPEEDY);
                    // obstacle.body.velocity.y = this._gameControler.SPEEDBOAT;
                    if (obj.name == "wood") {
                        obstacle.body.setSize(100, 50, 0, 0);
                    }
                    else if (obj.name == "jumper2") {
                        obstacle.body.setSize(100, 50, 15, 25);
                    }
                    else if (obj.name == "choinka") {
                        obstacle.body.setSize(100, 50, 0, 225);
                    }
                    else if (obj.name == "balwan") {
                        obstacle.body.setSize(70, 50, 0, 50);
                    }
                    this._obstacleArr.push(obstacle);
                }
            }
            //   }
            var obstacleFarAway = this._obstacleArr[this._longerObstacle[0][1]];
            obstacleFarAway.checkWorldBounds = true;
            this._whenAddNewScreen = obstacleFarAway;
            //  console.log("obstacle ile oboiektow wylosowano", max, obstacleFarAway);
            //  obstacleFarAway.events.onOutOfBounds.add(this.obstacleOutOfScreen, this);
            //   this.game.time.events.add(Phaser.Timer.SECOND * 4, this.getObstacle, this)
        };
        ObstacleView.prototype.addAgain = function () {
        };
        ObstacleView.prototype.getTheLonger = function (posx, width_, i) {
            if (this._longerObstacle.length > 0) {
                if (this._longerObstacle[0][0] < posx + width_) {
                    this._longerObstacle[0] = new Array(posx + width_, i);
                }
            }
            else {
                this._longerObstacle.push(new Array(posx + width_, i));
            }
        };
        ObstacleView.prototype.changeLetterToCoin = function () {
            this.forEach(function (item) {
                if (item.name == "popletter" || item.name == "correctLetter") {
                    item.isCoin();
                }
            }, this);
        };
        ObstacleView.prototype.obstacleOutOfScreen = function () {
            console.log("boats out of screen");
            // this.delObstacle();
            // this.getObstacle();
        };
        ObstacleView.prototype.delObstacle = function () {
            for (var i = 0; i < this._obstacleArr.length; i++) {
                if (this._obstacleArr[i].world.x + this._obstacleArr[i].width < 0) {
                    this._obstacleArr[i].destroy(true);
                    this._obstacleArr.splice(i, 1);
                    --i;
                }
            }
        };
        ObstacleView.prototype.delObstacles = function () {
            this.forEach(function (item) {
                if (item != null && item != undefined && this.toGlobal(item.position).x < -1000) {
                    console.log("niszcze obiekt", item.name, this.toGlobal(item.position).x);
                    item.destroy(true);
                }
                //  if (!stop && (item.name == "boatsmedium" || item.name == "boatssmall")) item.body.velocity.x = -70;
            }, this);
        };
        ObstacleView.prototype.changeSpeed = function (stop, changePos) {
            if (stop === void 0) { stop = false; }
            if (changePos === void 0) { changePos = false; }
            var speed = (stop) ? 0 : this._gameControler.SPEEDX;
            var speedy = (stop) ? 0 : this._gameControler.SPEEDY;
            console.log("stop", stop, speed);
            this.forEach(function (item) {
                item.body.velocity = new Phaser.Point(speed, speedy);
                if (changePos) {
                    item.x += 150;
                    item.y += 100;
                }
                //  if (!stop && (item.name == "boatsmedium" || item.name == "boatssmall")) item.body.velocity.x = -70;
            }, this);
            //   this._boundsOut.body.velocity.x = -270;
            //   this._boundsOut.body.velocity.y = 0;
        };
        ObstacleView.prototype.removeObstacleLetters = function () {
            this.forEach(function (item) {
                if (item != undefined && item.name == "popletter")
                    item.destroy(true);
            }, this);
        };
        ObstacleView.prototype.removeObstacle = function () {
            this.forEach(function (item) {
                if (item != undefined)
                    item.destroy(true);
            }, this);
        };
        ObstacleView.prototype.getRandom = function (max) {
            return Math.floor(Math.random() * max);
        };
        ObstacleView.prototype.update = function () {
            console.log("osttatni", this.toGlobal(this._whenAddNewScreen.position).x);
            if (this._whenAddNewScreen != null && this.toGlobal(this._whenAddNewScreen.position).x < 500) {
                this.getObstacle();
            }
        };
        return ObstacleView;
    })(Phaser.Group);
    Skate.ObstacleView = ObstacleView;
})(Skate || (Skate = {}));
//# sourceMappingURL=ObstacleView.js.map