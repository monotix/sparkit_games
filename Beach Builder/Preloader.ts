﻿module BeachBuilder {
    export class Preloader extends Phaser.State {
        private progresBar: Phaser.Sprite;
        private progressBarBlue: Phaser.Sprite;
       


        private reolad() {

            var tween = this.add.tween(this.progressBarBlue).to({ alpha: 1 }, 1000, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.reolad2, this);
        }

        private reolad2() {
            var tween = this.add.tween(this.progressBarBlue).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.reolad, this);

        }
        preload() {
           var bg: Phaser.Sprite = this.game.add.sprite(0, 0, "bg");
           
           this.progresBar= this.game.add.sprite(0, 0, "progressBar");
         
            
         //  this.progresBar.x = this.game.width / 2 - this.progresBar.width / 2;
         //  this.progresBar.y = this.game.height - this.progresBar.height - 20;
         
           
           this.progresBar.position.setTo(this.stage.width / 2 - this.progresBar.width / 2, this.stage.height - this.progresBar.height - 10);

           this.progressBarBlue = this.add.sprite(0, 0, 'progressBarBlue');
           this.progressBarBlue.position = this.progresBar.position;
           this.load.setPreloadSprite(this.progressBarBlue);
 

           var mobile: boolean;
           mobile = false
           if (!this.game.device.desktop) {
               mobile = true;
           }

           if (mobile) {
               this.load.image("firstInstruction", "assets/03_beach_builder.png");
               this.load.audio("instructionSn", "assets/audio/beach_builder_02.mp3");

           }
           else {
               this.load.image("firstInstruction", "assets/02_beach_builder.png");
               this.load.audio("instructionSn", "assets/audio/beach_builder_01.mp3");
           }



           this.load.image("popup", "assets/popup/popup.png");
           this.load.image("logoGame", "assets/popup/logo.png");
           this.load.atlasJSONArray('games', 'assets/popup/games.png', 'assets/popup/games.json');
           this.load.atlasJSONArray('playGame', 'assets/popup/play.png', 'assets/popup/play.json');
           this.load.atlasJSONArray('helpBtn', 'assets/popup/help.png', 'assets/popup/help.json');

           this.load.image("InstructionsScreen", "assets/SplashScreen2.png");

           this.load.image("mainMenuBg", "assets/mainMenu/SplashScreen2.png");
           this.load.atlasJSONArray('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');
           this.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
           this.load.audio("MainMenuSn", "assets/mainMenu/BBThemeMusic.mp3");
           this.load.audio("overButtonA", "assets/audio/overButton.mp3");
           this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");


           this.load.atlasJSONArray("doneInGameBtn", "assets/Level1/buttons/doneInGame.png", "assets/Level1/buttons/doneInGame.json");
           this.load.atlasJSONArray("clearBtn", "assets/Level1/buttons/clear.png", "assets/Level1/buttons/clear.json");


           this.load.image("TextLevel1", "assets/Level1/TextLevel1.png");
           this.load.atlasJSONArray("doneBtn", "assets/Level1/buttons/textDoneBtn.png", "assets/Level1/buttons/textDoneBtn.json");

           this.load.image("youWin", "assets/Level1/youWin.png");
           this.load.image("youLose", "assets/Level1/youLose.png");

           this.load.image("bgLevel1", "assets/Level1/bgLevel1.png");
           this.load.image("sunBg", "assets/Level1/sun.png");
           this.load.image("sunsetBg", "assets/Level1/sunset.png");
           this.load.image("moonBg", "assets/Level1/moon.png");

           /* items to drop */
           this.load.atlasJSONArray("redChair", "assets/Level1/objects/redChair.png", "assets/Level1/objects/redChair.json");
           this.load.atlasJSONArray("blueChair", "assets/Level1/objects/blueChair.png", "assets/Level1/objects/blueChair.json");
           this.load.atlasJSONArray("greenChair", "assets/Level1/objects/greenChair.png", "assets/Level1/objects/greenChair.json");
           this.load.atlasJSONArray("umbrellaYellow", "assets/Level1/objects/umbrellaYellow.png", "assets/Level1/objects/umbrellaYellow.json");
           this.load.atlasJSONArray("towelGreen", "assets/Level1/objects/towelGreen.png", "assets/Level1/objects/towelGreen.json");
           this.load.atlasJSONArray("towelOrange", "assets/Level1/objects/towelOrange.png", "assets/Level1/objects/towelOrange.json");
           this.load.atlasJSONArray("highLightLeft", "assets/Level1/objects/hightlightBenchLeft.png", "assets/Level1/objects/hightlightBenchLeft.json");
           this.load.atlasJSONArray("highLightRight", "assets/Level1/objects/hightlightBench2.png", "assets/Level1/objects/hightlightBench2.json");
           /*end item to drop */

           /*items to drag*/

           this.load.image("wind", "assets/Level1/downBarForObjects/items/propeller.png");
           this.load.image("drums", "assets/Level1/downBarForObjects/items/drums.png");
           this.load.image("zzzz", "assets/Level1/downBarForObjects/items/zzzz.png");

           this.load.image("ball", "assets/Level1/downBarForObjects/items/BubbleGum.png");
           this.load.image("towel", "assets/Level1/downBarForObjects/items/towel.png");
           this.load.image("drink", "assets/Level1/downBarForObjects/items/drink.png");

          
           /*end items to drag*/

           /* time of day */

           this.load.image("day", "assets/Level1/objects/SunBtn.png");
           this.load.image("moon", "assets/Level1/objects/MoonBtn.png");
           this.load.image("sunset", "assets/Level1/objects/sunsetBtn.png");

           /*end time of day */

           /* people to drag */
           /*  this.load.atlasJSONArray("gab", "assets/Level1/downBarForObjects/gab.png", "assets/Level1/downBarForObjects/gab.json");
             this.load.atlasJSONArray("kaz", "assets/Level1/downBarForObjects/kaz.png", "assets/Level1/downBarForObjects/kaz.json");
             this.load.atlasJSONArray("mo", "assets/Level1/downBarForObjects/mo.png", "assets/Level1/downBarForObjects/mo.json");
             this.load.atlasJSONArray("ridl", "assets/Level1/downBarForObjects/ridl.png", "assets/Level1/downBarForObjects/ridl.json");*/


           /* animations */
           var animNameArray: Array<string> = new Array("_default", "_wind", "_drum", "_towel", "_drink", "_ball", "_zzzz");
           for (var i: number = 0; i < 7; i++) {

               this.load.atlasJSONArray("gab" + animNameArray[i], "assets/Level1/downBarForObjects/gab/gab" + animNameArray[i] + ".png", "assets/Level1/downBarForObjects/gab/gab" + animNameArray[i] + ".json");
               this.load.atlasJSONArray("kaz" + animNameArray[i], "assets/Level1/downBarForObjects/kaz/kaz" + animNameArray[i] + ".png", "assets/Level1/downBarForObjects/kaz/kaz" + animNameArray[i] + ".json");
               this.load.atlasJSONArray("mo" + animNameArray[i], "assets/Level1/downBarForObjects/mo/mo" + animNameArray[i] + ".png", "assets/Level1/downBarForObjects/mo/mo" + animNameArray[i] + ".json");
               this.load.atlasJSONArray("ridl" + animNameArray[i], "assets/Level1/downBarForObjects/ridl/ridl" + animNameArray[i] + ".png", "assets/Level1/downBarForObjects/ridl/ridl" + animNameArray[i] + ".json");


           }

           for (var i: number = 0; i < 20; i++) {

               this.load.audio("lessonsA" + (i), "assets/audio/level/prompt1A_" + (i + 1) + ".mp3");


           }


           for (var i: number = 1; i < 6; i++) {

               this.load.atlasJSONArray("fail" + i , "assets/feedbacks/fail" + i + ".png", "assets/feedbacks/fail" + i + ".json");
            


           }


           for (var i: number = 1; i < 5; i++) {

               this.load.atlasJSONArray("success" + i, "assets/feedbacks/success" + i + ".png", "assets/feedbacks/success" + i + ".json");



           }

           for (var i: number = 1; i < 5; i++) {
               this.load.audio("congrats"+i, "assets/feedbacks/congrats"+ i +".mp3");
              


           }

           this.load.audio("failure1", "assets/feedbacks/failure1.mp3");
           this.load.audio("failure2", "assets/feedbacks/failure2.mp3");
           /* end animations */

           /* skateboard as people */

           this.load.image("skateboard1", "assets/Level1/objects/skateboard1.png");
           this.load.image("skateboard2", "assets/Level1/objects/skateboard2.png");
           this.load.image("skateboard3", "assets/Level1/objects/skateboard3.png");
           this.load.image("skateboard4", "assets/Level1/objects/skateboard4.png");

           /* end skateboard as people */

           this.load.audio("overButtonA", "assets/audio/overButton.wav");
           this.load.audio("clearButtonA", "assets/audio/clearButton.wav");



           this.load.atlasJSONArray("scoreScreen", "assets/scoreScreen/scoreScreen.png", "assets/scoreScreen/scoreScreen.json");
           this.load.atlasJSONArray("playScoreScreen", "assets/scoreScreen/scoreScreenButtons.png", "assets/scoreScreen/scoreScreenButtons.json");

           this.load.text('xml', 'assets/level1.xml');

        }

        update() {
            //this.progressBarBlue.x = this.game.load.progress;
            console.log(this.progressBarBlue.x);
        }


        mainMenu() {

          
        }

        create() {

           
         
            
            this.game.state.start("MainMenu");
          
           
        }

    }

}