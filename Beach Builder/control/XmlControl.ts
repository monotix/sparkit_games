﻿module BeachBuilder {

    export class XmlControl {

        public static _instance: XmlControl = null;
        private _xml: XMLDocument;
        public levelNum:number = 0;
        constructor() {

            
            if (XmlControl._instance) {
                throw new Error("Error: Instantiation failed: Use XmlControl.getInstance() instead of new.");
            }
            XmlControl._instance = this;
         
          /*  var parser = new DOMParser();
            var loadedXml: XMLDocument;
            var xml = this.cache.getText('xml');
            
            loadedXml = parser.parseFromString(xml, "application/xml");

            console.log("xml", loadedXml.querySelector("scene"));*/
        }

        public set xml(xmlLoaded: XMLDocument) {

            this._xml = xmlLoaded;
        }

        public get xml(): XMLDocument {

            return this._xml;

        }

        public getMaxLevel(): number {


            return this._xml.getElementsByTagName("level").length;
        }

        public getDescription(id: number): string {

            var maxItems: number = this._xml.getElementsByTagName("level")[this.levelNum].childNodes[id].childNodes.length;
            for (var i: number = 0; i < maxItems; i++) {
                if (this._xml.getElementsByTagName("level")[this.levelNum].childNodes[id].childNodes[i].nodeName == "desc") {
                    console.log(i, this._xml.getElementsByTagName("level")[this.levelNum].childNodes[id].childNodes[i].textContent);
                    return this._xml.getElementsByTagName("level")[this.levelNum].childNodes[id].childNodes[i].textContent;
                }

            }
            return "";


        }

        public getScene(id: number):Array<string> {
           
          
            var levelXml = this._xml.getElementsByTagName("level");
            console.log("w xml", this.levelNum);
            var childsceneList = levelXml.item(this.levelNum).childNodes;
            var maxItems: number = childsceneList[id].childNodes.length;
            var itemArray: Array<any> = new Array();
            console.log(levelXml.item(this.levelNum).childNodes, childsceneList[id].childNodes,childsceneList[id].childNodes);
            for (var i:number = 0; i < maxItems; i++) {
               
             
                if (childsceneList[id].childNodes[i].nodeName == "item") {
                    var desc: Array<string> = new Array();
                    console.log("szukam bledu id", id, "child nodes id", i);
                    desc.push(childsceneList[id].childNodes[i].attributes.getNamedItem("location").nodeValue);
                    desc.push(childsceneList[id].childNodes[i].attributes.getNamedItem("people").nodeValue);
                    desc.push(childsceneList[id].childNodes[i].attributes.getNamedItem("action").nodeValue);


                    itemArray.push(desc);

                }

            }
           
            return itemArray;

        }

        public getMaxRandomScenes(): number {
            console.log("ile plansz w levelu nr ", this.levelNum, this._xml.getElementsByTagName("level")[this.levelNum].childNodes.length);
            return this._xml.getElementsByTagName("level")[this.levelNum].childNodes.length;
            
        }

        public timeOfDay(id:number):string {
            var maxItems: number = this._xml.getElementsByTagName("scene")[id].childNodes.length;
            var itemArray: Array<any> = new Array();
            for (var i: number = 0; i < maxItems; i++) {
              
                if (this._xml.getElementsByTagName("scene")[id].childNodes[i].nodeName == "time") {
                   return this._xml.getElementsByTagName("scene")[id].childNodes[i].attributes.getNamedItem("id").nodeValue;
                   

                }

            }

            return "";



        }


        public static getInstance(): XmlControl {

            if (XmlControl._instance === null) {

                XmlControl._instance = new XmlControl();

            }

            return XmlControl._instance;



        }



    }

} 