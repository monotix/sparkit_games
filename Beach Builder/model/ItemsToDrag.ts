﻿module BeachBuilder {

    export class ItemsToDrag extends ItemsObject {

        public isEmpty: Boolean;
        public ownName: String;
        public id: number;
        public startX: number;
        public startY: number;
       
        public initialScale: Phaser.Point;
        private leftTopPoint: Phaser.Point;
        private rightBottomPoint: Phaser.Point;
        protected _game: Level1;
        public _items: Phaser.Sprite;
        public isSkateBoard: boolean;
        constructor(game: Phaser.Game, state: Level1, point: Phaser.Point, widthHeight: Phaser.Point, key: string = "", id: number = 0, name:string ="", scale: number = 1, isSkateBoard: boolean = false) {

            
            super(game, point.x, point.y, widthHeight);
            this.name = name;
           
            this.startX = this.x;
            this.startY = this.y;

            this.isSkateBoard = isSkateBoard

            this._game = state;
            this.initialScale =  this.scale = new Phaser.Point(scale, scale);
         
            this._rectangle.offset(-this._rectangle.halfWidth, -this._rectangle.halfHeight);
            if (key.length > 0) {
                this._items = new Phaser.Sprite(game, 0, 0, key,0);
                this._items.anchor.setTo(0.5, 0.5);
                this.addChild(this._items);

            }
            this.id = id;
            this.inputEnabled = true;
            this.input.enableDrag();
            this.events.onDragStart.add(this.onStartDrag, this);
            this.events.onDragStop.add(this.onStopDrag, this);
            
            this.answer = "";
           
            game.add.existing(this);


           //  console.log("constructor people");

        }

       protected onStopDrag() {
          
           this._game.itemsOnDragStop(this);
        }

       protected onStartDrag() {
          
            this._game.itemsOnDragStart(this);
        }


    }

} 