﻿module BeachBuilder {

    export class TimeOfDayBtn extends Phaser.Button {

        private _game: Level1;
        private _id: number;
        constructor(game: Phaser.Game, state: Level1,x:number,y:number, key:string, id:number) {

            super(game, x, y, key, this.timeOfDayBtnClick,this)
            this.anchor.setTo(0.5, 0.5);
            this._game = state;
            this._id = id;


        }

        timeOfDayBtnClick() {

            this._game.changeTimeOfDay(this._id);


        }



    }

} 