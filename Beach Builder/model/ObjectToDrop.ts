﻿module BeachBuilder {

    export class ObjectToDrop extends ItemsObject{

        public isEmpty: Boolean = true;
        public dropedObjectName: String;
        public ownName: String;
        private _object: Phaser.Sprite;
        public id: number;
        public _isAlpha: boolean = false;
        protected _game: Level1;
        public positionPeople: Phaser.Point;
        constructor(game: Phaser.Game, state: Level1, point: Phaser.Point, key: string, frame: number, widthHeight:Phaser.Point, id: number = 0, scale:number = 1, positionPeople_:Phaser.Point = new Phaser.Point(0,0),name:string="") {


            super(game, point.x, point.y, widthHeight)
            this.key = key;
            this.name = name;

            if (positionPeople_.x == 0 && positionPeople_.y == 0) {

                this.positionPeople = new Phaser.Point(point.x, point.y);
             
            }

            else {
                this.positionPeople = positionPeople_;
            }
            this.frame = frame;
            this._game = state;
            this.ownName = key;
            this.anchor.setTo(0.5, 0.5);
            this.scale = new Phaser.Point(scale, scale);
            
           // this._rectangle.offset(-this._rectangle.halfWidth, -this._rectangle.halfHeight);
            
            this._rectangle = new Phaser.Rectangle(this.x - this._rectangle.width/2, this.y - (this._rectangle.height*1.2), this._rectangle.width, this._rectangle.height);
           /* var grap: Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
         
            grap.beginFill(0x000000, 0.5);
            grap.drawRect(- this._rectangle.halfWidth, - this._rectangle.halfHeight, this._rectangle.width, this._rectangle.height);
            grap.endFill();
            this.addChild(grap);*/
          
            this._object =  new Phaser.Sprite(game, 0, 0, key, 0);
            this._object.anchor.setTo(0.5, 0.5);
            this.addChild(this._object);
            this.answer = "";

            game.add.existing(this);



        }
        create(){

            
            

        }

        public changeFrame(id: number, alpha:boolean =false) {
            this._object.frame = id;
            if (this._isAlpha && this._object.frame == 0) {
                this._object.alpha = 0;

            }
            else {
                this._object.alpha = 1;
            }
          
         
        }

        public restart() {

            this.isEmpty = true;
            this.answer = "";


        }


        public set isAlpha(show:boolean) {
            this._isAlpha = true;
            this._object.alpha = 0;
        }


    }

} 