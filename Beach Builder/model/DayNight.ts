﻿module BeachBuilder {

    export class DayNight extends Phaser.Sprite{

        private _day: Phaser.Sprite;
        private _night: Phaser.Sprite;
        private _sunset: Phaser.Sprite;

        private _timeOfDay: Phaser.Sprite;
        public answer: string = "day";
        constructor(game: Phaser.Game, x: number, y: number, day: string, night: string, sunset:string) {

            super(game, x, y)

            this._day = new Phaser.Sprite(game, 0, 0, day);
            this._night = new Phaser.Sprite(game, 0, 0, night);
            this._sunset = new Phaser.Sprite(game, 0, 0, sunset);

            this._timeOfDay = this._day;
            this.addChild(this._timeOfDay);


        }

        public changeTimeOfDay(id: number) {

            this._timeOfDay = null;
            this.removeChild(this._timeOfDay);
            var nazwaPory: string = "";
            if (id == 0) {
                nazwaPory = "day";
                this._timeOfDay = this._day;

            }
            else if (id == 1) {

                this._timeOfDay = this._night;
                nazwaPory = "sunset";
            }

            else if (id == 2) {

                this._timeOfDay = this._sunset;
                nazwaPory = "night";

            }

            this.answer = nazwaPory;

            this.addChild(this._timeOfDay);

        }

    }


}