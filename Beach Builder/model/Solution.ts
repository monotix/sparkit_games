﻿module BeachBuilder {

    export class Solutions {

        private static _instance: Solutions = null;
        private itemsToDropNameArray: Array<String> = new Array("redChair", "blueChair", "greenChair", "umbrellaYellow", "towelGreen", "towelOrange", "highLightLeft", "highLightRight");
        private animNameArray: Array<string> = new Array("_default", "_wind", "_drum", "_towel", "_drink", "_gum", "_zzzz");
        private peopleNameArray: Array<string> = new Array("ridl", "mo", "gab", "kaz");

        public levels: Array<Places>;

        constructor() {

            if(Solutions._instance) {
                throw new Error("Error: Instantiation failed: Use Solutions.getInstance() instead of new.");
            }
            Solutions._instance = this;
           this.level1Desc();
        }

        public static getInstance(): Solutions {
            if (Solutions._instance === null) {
                Solutions._instance = new Solutions();
            }
            return Solutions._instance;
        }

        public level1(peopleInPlace: Array<any>, peopleAction: Array<any>) {




        }


        private level1Desc() {
            console.log("Level1 Ustawiony");
            this.levels = new Array();
            var level1 = new Places();
            //Kaz plays ball on the red chair.
            level1.setSolution("redChair", "kaz", "kaz", "_gum");
            this.levels.push(level1);

            var level2 = new Places();
            //It is dark.  Mo sits on the green towel.
            level2.setSolution("towelGreen", "mo", "", "", 2);
            this.levels.push(level2);

            //Ridl sleeps on the picnic table.
            var level3 = new Places();
            
            level3.setSolution("highLightLeft", "ridl", "ridl", "_zzzz");
            this.levels.push(level3);

            //Kaz sits on the red chair.  Mo sits next to her.  The sun is setting.  It is getting late.
            var level4 = new Places();

            level4.setSolution("redChair", "kaz", "", "", 1);
            level4.setSolution("blueChair", "mo", "", "", 1);
            this.levels.push(level4);


            //Ridl and Gabe love drums. They play them well.  Ridl is on the red chair.  Gabe is on the green chair.
            var level5 = new Places();

            level5.setSolution("redChair", "ridl", "ridl", "_drum");
            level5.setSolution("greenChair", "gab", "gab", "_drum");
            this.levels.push(level5);


            //Kaz sleeps under the shade.  Mo has a drink at the table.
            var level6 = new Places();

            level6.setSolution("umbrellaYellow", "kaz", "kaz", "_zzzz");
            level6.setSolution("highLightLeft", "mo", "mo", "_drink");
            this.levels.push(level6);


            //It is dark.  Mo and Kaz sit at the table. 
            var level7 = new Places();

            level7.setSolution("highLightLeft", "kaz");
            level7.setSolution("highLightRight", "mo");
            this.levels.push(level7);


            //Mo sits on the red chair.  Kaz sits on the blue chair.  It is late.  Kaz is happy.  Mo is happy.

            var level8 = new Places();

            level8.setSolution("redChair", "mo","","",1);
            level8.setSolution("blueChair", "kaz","","",1);
            this.levels.push(level8);


            //Mo sleeps on the red chair.  The sun is going down.  Ridl is drying off on the green chair.
            var level9 = new Places();

            level9.setSolution("redChair", "mo", "mo", "_zzzz", 1);
            level9.setSolution("greenChair", "ridl", "ridl", "_towel", 1);
            this.levels.push(level9);


            //Kaz likes her drink.  She sits on the red chair.
            var level10 = new Places();

            level10.setSolution("redChair", "kaz", "kaz", "_drink");
            //level10.setSolution("greenChair", "ridl", "ridl", "_towel", 1);
            this.levels.push(level10);


            // Mo plays ball on the red chair
            var level11 = new Places();

            level11.setSolution("redChair", "mo", "mo", "_gum");
            
            this.levels.push(level11);

            // It is dark.  Kaz sits on the green towel.
            var level12 = new Places();

            level12.setSolution("towelGreen", "kaz", "", "",0);

            this.levels.push(level12);


            //Gabe sleeps on the picnic table.

            var level13 = new Places();

            level13.setSolution("highLightLeft", "gab", "gab", "_zzzz");

            this.levels.push(level13);



            //Mo sits on the red chair.  Kaz sits next to her.  The sun is setting.  It is getting late.

            var level14 = new Places();

            level14.setSolution("redChair", "mo", "", "", 1);
            level14.setSolution("blueChair", "kaz", "", "", 1);

            this.levels.push(level14);


            //Ridl and Gabe love drums.  They play them well.  Gabe is on the red chair. Ridl is on the green chair.



            var level15 = new Places();

            level15.setSolution("redChair", "gab", "gab", "_drum");
            level15.setSolution("greenChair", "ridl", "ridl", "_drum");

            this.levels.push(level15);

            //Mo sleeps under the shade. Kaz has a drink at the table.

            var level16 = new Places();

            level16.setSolution("umbrellaYellow", "mo", "mo", "_zzzz");
            level16.setSolution("highLightLeft", "kaz", "kaz", "_drink");

            this.levels.push(level16);


            //It is dark.  Ridl and Gabe sit at the table. 


            var level17 = new Places();

            level17.setSolution("highLightLeft", "ridl", "", "", 2);
            level17.setSolution("highLightRight", "gab", "", "", 2)

            this.levels.push(level17);

            //Kaz sits on the red chair.  Mo sits on the blue chair.  It is late.  Kaz is happy.  Mo is happy.

            var level18 = new Places();

            level18.setSolution("redChair", "kaz", "", "", 1);
            level18.setSolution("blueChair", "mo", "", "", 1);

            this.levels.push(level18);


            //Kaz sleeps on the red chair.  The sun is going down.  Gabe is drying off on the green chair.
            var level19 = new Places();

            level19.setSolution("redChair", "kaz", "kaz", "_zzzz", 1);
            level19.setSolution("greenChair", "gab", "gab", "_towel", 1);

            this.levels.push(level19);

           // Mo likes her drink.She sits on the red chair.


            var level20 = new Places();

            level20.setSolution("redChair", "mo", "mo", "_drink");

            this.levels.push(level20);

            //Kaz and Brad read books.  They sit on benches.


            var level21 = new Places();

            level21.setSolution(" ", "gab", "gab", "_zzzz");

            this.levels.push(level21);
        }

        //kaz red chair play ball
        //3
        //4
    }

} 