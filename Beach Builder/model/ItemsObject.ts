﻿module BeachBuilder {

    export class ItemsObject extends Phaser.Sprite {

        public _rectangle: Phaser.Rectangle;
      
        public initialX: number;
        public initialY: number;
        public answer: string;
        constructor(game:Phaser.Game,x:number,y:number,widthHeight:Phaser.Point) {

            super(game, x, y)
            this.initialX = x;
            this.initialY = y;
            this._rectangle = new Phaser.Rectangle(0, 0, widthHeight.x, widthHeight.y);
        }



    }

} 