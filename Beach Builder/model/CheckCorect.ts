﻿module BeachBuilder {

    export class CheckCorect{

        public solution: Solutions;
        private randomLevel: number = 11;
        private _xml: XmlControl = null;
        private _gameControl: GameControler;
        private _incommon: string;
        constructor() {

            this.solution = Solutions.getInstance();
            this._gameControl = GameControler.getInstance();
        }

        /*
            dupa
            podajelokacje
               sprawdzam czy ma odpowiedz czlowieka
                i czy czlowiek ma animacje

        */
        private checkPeopleAnim(who: string, action: string, peopleAction: Array<PeopleToDrag>):boolean{
            var max: number = peopleAction.length;

            for (var i: number = 0; i < max; i++) {
                //console.log(peopleAction[i].name, peopleAction[i].answer);
                if (peopleAction[i].name == who && peopleAction[i].answer == action) {

                   

                    return true;

                }


            }

            return false;
        }

        private actionOfPeople(who: string, peopleAction: Array<PeopleToDrag>): string {
            var max: number = peopleAction.length;

            for (var i: number = 0; i < max; i++) {
                //console.log(peopleAction[i].name, peopleAction[i].answer);
                if (peopleAction[i].name == who) {


                    return peopleAction[i].answer;
                  

                }


            }

            return "";
        }

        private zwrocAkcjeCzlowieka(czlek: string, peopleAction:Array<PeopleToDrag>):string{

            for (var i: number = 0; i < peopleAction.length; i++) {
               
                

                
               
                if (czlek == peopleAction[i].name) {

                    return peopleAction[i].answer;

                }

            }
            
            return "";

        }


        public checkAnswers(dropObject: Array<ObjectToDrop>, peopleAction: Array<PeopleToDrag>, timeOfDay: string, wspolne: Array<any>): boolean {

            if (this._xml === null) this._xml = XmlControl.getInstance();
            var arr: Array<any> = this._xml.getScene(this._gameControl.actualRandomTextId);
            console.log(arr);
            var pass: boolean = false;
            var ileznalezionoStolow: number = 0;
            var tableWzorzec: Array<any> = new Array();
            for (var i: number = 0; i < arr.length; i++) {

                if (arr[i][0] == this._incommon) {
                    tableWzorzec.push(arr[i]);
                    ileznalezionoStolow++;
                }
            }

            console.log("!!!wrzoce", tableWzorzec.length, tableWzorzec);

           
            /*
           
            for (var i: number = 0; i < arr.length; i++) {
                for (var j: number = 0; j < dropObject.length; j++) {
                    //znajudjemy stol sprawdzamy czy kto siedzi,,
                    console.log("wspolne check ", dropObject[j].name, this._incommon, dropObject[j].name, arr[i][0])
                    

                    if (dropObject[j].name == this._incommon && dropObject[j].name == arr[i][0]) {
                        
                        if (arr[i][1] == dropObject[j].answer) {

                            prawidloweOdpowiedzi++;
                           

                        }



                    }

                    else if (dropObject[j].name == this._incommon && dropObject[j].answer.length > 0) {
                        return false;

                    }


                }

            }

            if (prawidloweOdpowiedzi == ileznalezionoStolow) pass = true;

            if (!pass) return pass;*/
            
            var odpowiedziZGryArray: Array<any> = new Array();
            var odpowiedziArray: Array<any> = new Array();
            for (var j: number = 0; j < dropObject.length; j++) {
               // console.log(dropObject[j].name, "umbrellaYellow");
             
                odpowiedziZGryArray.push(new Array(dropObject[j].name, dropObject[j].answer, this.zwrocAkcjeCzlowieka(dropObject[j].answer,peopleAction)));
                odpowiedziArray.push(new Array(dropObject[j].name,"",""));
                

            }
           
            var countTable: number = 0;
            var idOfTableArray: Array<number> = new Array();
            for (var k: number = 0; k < arr.length; k++) {
               
                for (var j: number = 0; j < odpowiedziArray.length; j++) {


                 
                    if (dropObject[j].name == arr[k][0] && dropObject[j].name != this._incommon) {

                        odpowiedziArray[j][0] = arr[k][0];
                        odpowiedziArray[j][1] = arr[k][1];
                        odpowiedziArray[j][2] = arr[k][2];


                    }
                    if (dropObject[j].name == this._incommon && k == 0) {

                        idOfTableArray.push(j);

                    }

                }

            }
         
            for (var i: number=0; i < tableWzorzec.length; i++) {

            
                odpowiedziArray[idOfTableArray[i]] = tableWzorzec[i];
                

            }

            
            console.log("odpowiedzi wzorzec",odpowiedziArray);
            console.log("odpowiedzi z gry",odpowiedziZGryArray);



           
      

          

            var prawidloweOdpowiedziStol: number = 0;

            var tableArray:Array<any> = new Array();
                for (var j: number = 0; j < odpowiedziZGryArray.length; j++) {
                    //znajudjemy stol sprawdzamy czy kto siedzi,,
                    console.log("wspolne check ",  odpowiedziZGryArray[j][0] , this._incommon)


                    if (odpowiedziZGryArray[j][0] == this._incommon && odpowiedziZGryArray[j][1].length > 0) {
                        console.log("weszlo", odpowiedziZGryArray[j][1], odpowiedziArray[j][1], odpowiedziZGryArray[j][2], odpowiedziArray[j][2]);
                        tableArray.push(odpowiedziZGryArray[j]);

                    }
                    
                    

                }
                console.log(tableArray[0], tableArray[1], tableWzorzec[0], tableWzorzec[1]);
                if (tableArray.length < ileznalezionoStolow) return false;
                
                
                if (tableArray.length > tableWzorzec.length) return;

                for (var i: number = 0; i < tableArray.length; i) {
                 
                    console.log(i, tableArray.length,tableArray[i][1] , tableWzorzec[tableWzorzec.length - 1][1] , tableArray[i][2] , tableWzorzec[tableWzorzec.length - 1][2]);
                    if (tableArray[i][1] == tableWzorzec[tableWzorzec.length - 1][1] && tableArray[i][2] == tableWzorzec[tableWzorzec.length - 1][2]) {
                        prawidloweOdpowiedziStol++;
                        tableArray.splice(i, 1);
                        tableWzorzec.splice(tableWzorzec.length - 1, 1);
                        i = 0;
                        console.log("wchodzi", tableArray, tableWzorzec);
                    }
                    else {
                        i++;
                    }

                    console.log("ppo spradzwa", i, tableArray.length);
                   


                }
                console.log(prawidloweOdpowiedziStol ,ileznalezionoStolow);
            if (prawidloweOdpowiedziStol == ileznalezionoStolow) pass = true;

            if (!pass) return pass;
           
            for (var j: number = 0; j < odpowiedziZGryArray.length; j++) {


              
                if (odpowiedziZGryArray[j][0] != this._incommon && odpowiedziZGryArray[j][0] == odpowiedziArray[j][0] && odpowiedziZGryArray[j][1] == odpowiedziArray[j][1] && odpowiedziZGryArray[j][2] == odpowiedziArray[j][2]) {
                       
                  
                       
                        
                     

                          
                    pass = true;




                }
                else if (odpowiedziZGryArray[j][0] != this._incommon) {
                   
                    return false;
                }
             /*   else if (odpowiedziZGryArray[j][0] == this._incommon && odpowiedziZGryArray[j][0] == odpowiedziArray[j][0] && odpowiedziZGryArray[j][1] == odpowiedziArray[j][1] && odpowiedziZGryArray[j][2] == odpowiedziArray[j][2]){

                    prawidloweOdpowiedziStol++;
                }*/


   

            }

           
          

          
           
            if (this._xml.timeOfDay(0).length > 0) {
                pass = false;
                console.log("sprawdzam dzien", this._xml.timeOfDay(0), timeOfDay);
                if (this._xml.timeOfDay(0) == timeOfDay) {
                    pass = true;
                }

            }

            return pass;

        }
            
           /* this.randomLevel = GameControler.getInstance().actualRandomTextId;
            console.log("aktualny lewel to", this.randomLevel);
            for (var i: number = 0; i < dropObject.length; i++){
              //  console.log("ludzie", this.solution.levels[this.randomLevel].placeAnswer[i], dropObject[i].answer, i);
                if (this.solution.levels[this.randomLevel].placeAnswer[i] != dropObject[i].answer) {
                    return false;

                }

            }

            for (var i: number = 0; i < peopleAction.length; i++) {
                //console.log("ludzie", this.solution.levels[this.randomLevel].peopleAnswer[i] , peopleAction[i].answer, i);
                if (this.solution.levels[this.randomLevel].peopleAnswer[i] != peopleAction[i].answer) {
                    return false;

                }

            }

            if (this.solution.levels[this.randomLevel].timeOfDay != -1) {

                if (this.solution.levels[this.randomLevel].timeOfDay != timeOfDay) {
                    return false;
                }

            }*/

            

        public set incomon(incomonObject: string) {

            this._incommon = incomonObject;

        }



    }


}