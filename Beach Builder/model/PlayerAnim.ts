﻿module BeachBuilder{


    export class PlayerAnim extends PeopleToDrag {
        
        private animationArray:Array<Phaser.Sprite> = new Array();
        private nameAnimationArray: Array<string> = new Array();
        private _actualAnim: Phaser.Sprite;
        
        public addAnimation(anim: Phaser.Sprite, nameAnim:string) {

            //this.scale.add(0.072, 0.072);
            anim.anchor.setTo(0.5, 0.5);
            this.animationArray.push(anim);
            this.nameAnimationArray.push(nameAnim);
            


        }

        public playAnimation(id: number) {
            this.removeChild(this._actualAnim);
            this._actualAnim = null;
            this.answer = this.nameAnimationArray[id];
            this._actualAnim = this.animationArray[id];
            this.addChild(this._actualAnim);
            this._actualAnim.animations.add(this.nameAnimationArray[id]);
            this._actualAnim.animations.play(this.nameAnimationArray[id], 24, true, false);

            
        }


        public playAnim() {

            if (!this._actualAnim) {
                this._actualAnim = this._items;

            }
            this._actualAnim.animations.add("");
            this._actualAnim.animations.play("",24, true, false);



        }

        public restart() {

            this.startX = this.x = this.initialX;
            this.startY = this.y = this.initialY;
            this.scale = this.initialScale;
            this.isDroped = false;
            this.lastobjecToDrop = null;
            this.removeChild(this._actualAnim);
            this._actualAnim = null;
            console.log("nazwa animacji", this.nameAnimationArray[0]);
         //   this._actualAnim = this.animationArray[0];
            this.addChild(this._items);
            this._items.frame = 0;
            this._items.animations.stop();
            this.answer = "";
           

        }

        protected onStopDrag() {

            this._game.peopleOnDragStop(this);
            this._rectangle = new Phaser.Rectangle(this.x - this._rectangle.halfWidth, this.y - this._rectangle.halfHeight, this._rectangle.width, this._rectangle.height);
            if (this.x == this.initialX && this.y == this.initialY) this.scale = this.initialScale;
        }

        protected onStartDrag() {
           // this.scale = new Phaser.Point(1.2, 1.2);
            this.scale = new Phaser.Point(1, 1);
            this._game.peopleOnDragStart(this);
        }



    }




}