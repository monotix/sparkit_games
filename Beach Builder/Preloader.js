var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var BeachBuilder;
(function (BeachBuilder) {
    var Preloader = (function (_super) {
        __extends(Preloader, _super);
        function Preloader() {
            _super.apply(this, arguments);
        }
        Preloader.prototype.preload = function () {
            var bg = this.game.add.sprite(0, 0, 'splashScreen');
            this.load.image("progressBar", "assets/preloader/ProgressBar.png");
            this.load.image("progressBarBlue", "assets/preloader/ProgressBarBlue.png");
        };
        Preloader.prototype.create = function () {
        };
        return Preloader;
    })(Phaser.State);
    BeachBuilder.Preloader = Preloader;
})(BeachBuilder || (BeachBuilder = {}));
//# sourceMappingURL=Preloader.js.map