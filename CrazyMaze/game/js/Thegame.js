Main.Thegame = function (game) {
    this.game = game;

    //settings
    this.score = 0;
    this.lives = 3;
    this.frame = 0;
    this.creditsAll = 0;
    this.currentLevel = 6;
    this.mouseIsDown = false;
    var platforms, scoreText, livesText;
    this.blockedScreen = false;
    this.flashSpeed = 80;
    this.dblClickDelay = 0;
    this.moveDelay = 0;
    this.playerDiesFlag = false;
    this.playerGoUpFlag = false;

    this.setGravityx = 0;
    this.setGravityy = 0;
    this.hamsterSpeed = 90;
    this.finalRotation = 0;
    _self = this;
};

Main.Thegame.prototype = {

    preload: function () {},
    render: function () {
        //this.game.debug.spriteInfo(player, 32, 32);
        //this.game.debug.body(player);
    },
    create: function () {

        this.timeDelay = 0;
        this.game.world.setBounds(0, 0, 800, 600);
        this.game.physics.startSystem(Phaser.Physics.P2JS);
        this.createBackground();
        this.createSlides();
        this.createObstacles();
        this.createTraps();
        this.createPlayer();
        this.game.add.sprite(0, 0, 'overlay' + this.currentLevel);

        this.game.physics.p2.createContactMaterial(playerMaterial, bordersPolygonSpriteMaterial, {
            friction: 0,
            restitution: 0.5
        });
        this.game.input.enabled = true;
        //console.log(this.game)
        this.game.physics.p2.setPostBroadphaseCallback(this.checkOverlap, this);
        this.game.input.onDown.add(this.beginSwipe, this);
    },

    beginSwipe: function () {
        this.startX = this.game.input.worldX;
        this.startY = this.game.input.worldY;
        this.game.input.onDown.remove(this.beginSwipe);
        this.game.input.onUp.add(this.endSwipe);
    },
    endSwipe: function () {

        endX = _self.game.input.worldX;
        endY = _self.game.input.worldY;
        var distX = _self.startX - endX;
        var distY = _self.startY - endY;

        if (Math.abs(distX) > Math.abs(distY) * 2 && Math.abs(distX) > 10) {

            if (distX > 0) {
                _self.swipeMove = "left";
            } else {
                _self.swipeMove = "right";
            }
            _self.moveTo = Math.abs(distX);
        }
        if (Math.abs(distY) > Math.abs(distX) * 2 && Math.abs(distY) > 10) {
            if (distY > 0) {
                _self.swipeMove = "up";
            } else {
                _self.swipeMove = "down";
            }
            _self.moveTo = Math.abs(distY);
        }
        _self.game.input.onDown.add(_self.beginSwipe);
        _self.game.input.onUp.remove(_self.endSwipe);
    },

    update: function () {
        if (this.finalRotation > player.body.angle + 5) {
            player.body.angle = parseInt(player.body.angle) + 5;
        }
        if (this.finalRotation < player.body.angle - 5) {
            player.body.angle = parseInt(player.body.angle - 5);
        }

        this.game.physics.p2.gravity.x = this.setGravityx;
        this.game.physics.p2.gravity.y = this.setGravityy;

        if (!this.playerDiesFlag) {
            if (!this.blockedScreen) {
                this.leftKey = this.game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
                this.rightKey = this.game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
                this.upKey = this.game.input.keyboard.addKey(Phaser.Keyboard.UP);
                this.downKey = this.game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
                this.zKey = this.game.input.keyboard.addKey(Phaser.Keyboard.Z);
            }
            if (this.leftKey.isDown || (this.swipeMove == "left" && this.moveTo > 0)) {
                //  Move to the left
                player.body.velocity.x = -this.hamsterSpeed;
                if (!this.playerGoUpFlag) {
                    player.animations.play('walk');
                }
                this.finalRotation = 90;
                this.moveTo--;
            } else if (this.rightKey.isDown || (this.swipeMove == "right" && this.moveTo > 0)) {
                //  Move to the right
                player.body.velocity.x = this.hamsterSpeed;
                if (!this.playerGoUpFlag) {
                    player.animations.play('walk');
                }
                this.finalRotation = -90;
                this.moveTo--;
            } else if (this.upKey.isDown || (this.swipeMove == "up" && this.moveTo > 0)) {
                //  Move up
                player.body.velocity.y = -this.hamsterSpeed;
                if (!this.playerGoUpFlag) {
                    player.animations.play('walk');
                }
                this.finalRotation = -180;
                this.moveTo--;
            } else if (this.downKey.isDown || (this.swipeMove == "down" && this.moveTo > 0)) {
                //  Move up
                player.body.velocity.y = this.hamsterSpeed;
                if (!this.playerGoUpFlag) {
                    player.animations.play('walk');
                }
                this.finalRotation = 0;
                this.moveTo--;
            } else {
                this.slowDown();

            }
            if (this.zKey.isDown && this.playerGoUpFlag == false) {
                this.playerGoUpFlag = true;
                //            this.swipeMove = false;
                this.playerJump();
            }
        }
    },
    slowDown: function () {
        if (player.body.velocity.y > 0) {
            player.body.velocity.y--;
        }
        if (player.body.velocity.y < 0) {
            player.body.velocity.y++;
        }
        if (player.body.velocity.x > 0) {
            player.body.velocity.x--;
        }
        if (player.body.velocity.x < 0) {
            player.body.velocity.x++;
        }
    },

    checkOverlap: function (body1, body2) {
        //console.log(body1.sprite);  

        if ((body1.sprite.name === 'slide1' && body2.sprite.name === 'hamster') ||
            (body2.sprite.name === 'slide1' && body1.sprite.name === 'hamster')) {
            if (this.upKey.isDown || (this.swipeMove == "up" && this.moveTo > 0)) {
                player.body.velocity.y = -this.hamsterSpeed / 2;
            } else {
                player.body.velocity.y = this.hamsterSpeed * 2;
            }
            return false;
        }


        if ((body1.sprite.name === 'slide2' && body2.sprite.name === 'hamster') ||
            (body2.sprite.name === 'slide2' && body1.sprite.name === 'hamster')) {
            if (this.upKey.isDown || (this.swipeMove == "up" && this.moveTo > 0)) {
                player.body.velocity.y = this.hamsterSpeed / 2;
            } else {
                player.body.velocity.y = -this.hamsterSpeed * 2;
            }
            return false;
        }

        //przeskok przez przeszkody
        if (this.playerGoUpFlag & ((body1.sprite.name === 'obstacles' && body2.sprite.name === 'hamster') ||
                (body2.sprite.name === 'obstacles' && body1.sprite.name === 'hamster'))) {
            return false;
        }


        //pułapki
        if ((body1.sprite.name === 'traps' && body2.sprite.name === 'hamster') ||
            (body2.sprite.name === 'traps' && body1.sprite.name === 'hamster')) {
            if (this.playerGoUpFlag) {
                console.log('up');
                return false;
            } else {
                if (!this.playerDiesFlag) {
                this.playerDiesFlag = true;
                console.log('kill');
                this.playerDies();
                 }
                return false;
            }
        }


        //
        return true;
    },

    quit: function () {},
    playAgain: function () {
        console.log("playagain");
        this.onBlockScreen(false);
        this.stage.removeChild(this.scoreScreenGr);
        this.defaultScore();
    },
    defaultScore: function () {
        this.fixedSatellites = 0;
        this.hitByRock = 0;
        satellite.animations.play('walk', 20, true);

    },
    youWin: function (finishLevel) {
        this.game.state.start("instructions", Main.Instructions);
        //this.game.state.start("guessgame", Main.GuessGame);
    },
    restart: function () {
        this.satellitesToFix = 3;
        this.fixedSatellites = 0;
        this.baddiesCount = 0;
        this.hitByRock = 0;
        this.create();
    },
    createBackground: function () {
        //  A simple background for our game        
        room = this.game.add.sprite(0, 0, 'gameBg');
        room.name = "room";

        this.game.add.sprite(0, 0, 'underlay' + this.currentLevel);
        gameArea = this.game.add.sprite(0, 0, 'game' + this.currentLevel);
        gameArea.name = "game";
        bordersPolygonSpriteMaterial = this.game.physics.p2.createMaterial(); //create material


        bordersPolygonSprite = this.game.add.sprite(0, 0, 'game' + this.currentLevel);
        this.game.physics.p2.enableBody(bordersPolygonSprite);
        bordersPolygonSprite.body.clearShapes();
        bordersPolygonSprite.body.loadPolygon('gamePhysics' + this.currentLevel, 'game');
        bordersPolygonSprite.body.static = true;
        bordersPolygonSprite.body.x = 400
        bordersPolygonSprite.body.y = 300;
        bordersPolygonSprite.body.setMaterial(bordersPolygonSpriteMaterial);

    },
    playerJump: function () {
        this.playerGoUpFlag = true;
        var anim = player.animations.add('jump', Main.frameRange(14, 42));
        anim.onComplete.add(function () {
            _self.playerGoUpFlag = false;
        }, player);
        anim.play(24);
    },
    playerDies: function () {
        player.animations.stop();
        player.body.velocity.y = 0;
        player.body.velocity.x = 0;
        var anim = player.animations.add('die', Main.frameRange(43, 65));
        anim.onComplete.add(function () {
            _self.playerDiesFlag = false;
            _self.lives--;
            if (_self.lives > 1) {
                //restart level
            } else {
                //end game
            }
        }, player);
        anim.play(30, false, true);
    },
    setPlayerOnSlide: function () {
        this.playerOnSlideDelay = this.game.time.now + 50
    },
    createPlayer: function () {
        player = this.game.add.sprite(90, 90, 'player');
        player.name = "hamster";
        this.game.physics.p2.enableBody(player);
        player.body.clearShapes();
        var circleShape = new p2.Circle(0.8);
        player.body.addShape(circleShape)
        player.animations.add('walk', Main.frameRange(1, 14));
        player.anchor.setTo(0.5, 0.72);
        playerMaterial = this.game.physics.p2.createMaterial();
        player.body.setMaterial(playerMaterial);
    },

    createObstacles: function () {
        obstaclesPolygon = this.game.add.sprite(0, 0, 'game' + this.currentLevel);
        obstaclesPolygon.name = "obstacles";
        this.game.physics.p2.enableBody(obstaclesPolygon);
        obstaclesPolygon.body.clearShapes();
        obstaclesPolygon.body.loadPolygon('obstaclesPhysics' + this.currentLevel, 'game');
        obstaclesPolygon.body.static = true;
        obstaclesPolygon.body.x = 400
        obstaclesPolygon.body.y = 300;
        obstaclesPolygon.body.setMaterial(bordersPolygonSpriteMaterial);
    },
    createTraps: function () {
        trapsPolygon = this.game.add.sprite(0, 0, 'game' + this.currentLevel);
        trapsPolygon.name = "traps";
        this.game.physics.p2.enableBody(trapsPolygon);
        trapsPolygon.body.clearShapes();
        trapsPolygon.body.loadPolygon('trapsPhysics' + this.currentLevel, 'game');
        trapsPolygon.body.static = true;
        trapsPolygon.body.x = 400
        trapsPolygon.body.y = 300;
    },

    createSlides: function () {
        board1 = this.game.add.sprite(231, 210, 'slide1');
        board1.name = "slide1";
        this.game.physics.p2.enableBody(board1);
        board1.body.static = true;

        board2 = this.game.add.sprite(535, 275, 'slide2');
        board2.name = "slide2";
        this.game.physics.p2.enableBody(board2);
        board2.body.static = true;
    },

    randomNum: function (max, min) {
        if (min === void 0) {
            min = 0;
        }
        return Math.floor(Math.random() * (max - min) + min);
    },

    onBlockScreen: function (show) {
        if (show) {
            console.log('blok');
            this.blockedScreen = true;
            this.blockScreen.inputEnabled = true;
            this.blockScreen.input.priorityID = 1;
            this.stage.addChild(this.blockScreen);
        } else {
            this.blockedScreen = false;
            this.blockScreen.inputEnabled = false;
            this.blockScreen.input.priorityID = 0;
            this.blockScreen.kill();
        }
    }
}