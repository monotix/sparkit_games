var Main = {};


Main.frameRange = function (posstart, posend) {
    hitFrames = [];
    for (a = posstart; a < posend + 1; a++) {
        hitFrames.push(a);
    }
    return hitFrames;
}


Main.Boot = function (game) {
    this.game = game;

};

Main.Boot.prototype = {

    preload: function () {
        this.game.load.image('splash', 'assets/splashScreen.png');
        this.load.image('loaderFull', 'assets/ProgressBarBlue.png');
        this.load.image('loaderEmpty', 'assets/ProgressBar.png');
    },

    create: function () {
        // this.game.stage.enableOrientationCheck(true, false, 'orientation');
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.state.start('preloader', Main.Preloader);
    },


}