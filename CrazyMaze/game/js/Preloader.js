Main.Preloader = function (game) {
    this.game = game;
};

Main.Preloader.prototype = {
    loaderFull: Phaser.Sprite,
    loaderEmpty: Phaser.Sprite,
    test: Phaser.Sprite,
    preload: function () {
        this.game.add.sprite(0, 0, 'splash');

        this.loaderEmpty = this.add.sprite(35, 485, 'loaderEmpty');
        this.loaderEmpty.crop = new Phaser.Rectangle(0, 0, this.loaderEmpty.width, this.loaderEmpty.height);
        this.loaderEmpty.name = 'loaderEmpty';
        this.loaderFull = this.add.sprite(35, 485, 'loaderFull');
        this.loaderFull.crop = new Phaser.Rectangle(0, 0, 0, this.loaderFull.height);
        this.loaderFull.name = 'loaderFull';

        this.game.load.atlas('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');
        this.game.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
        this.game.load.atlas('countdown', 'assets/countdown.png', 'assets/countdown.json');
        this.game.load.atlas('player', 'assets/player.png', 'assets/player.json');


        this.load.audio("overButtonA", "assets/audio/overButton.wav");
        this.load.audio("clearButtonA", "assets/audio/clearButton.wav");
        this.load.atlas("scoreScreen", "assets/scoreScreen/scoreScreen.png", "assets/scoreScreen/scoreScreen.json");
        this.load.atlas("playScoreScreen", "assets/scoreScreen/scoreScreenButtons.png", "assets/scoreScreen/scoreScreenButtons.json");

        this.game.load.image('instructionScreen', 'assets/instructionsScreen.png');
        this.game.load.image('gameBg', 'assets/gameBg.png');
        this.game.load.physics('roomPhysics', 'assets/gameBg.json');

        this.game.load.image('board', 'assets/board.png');
        for(a = 1; a<7; a++) {
        this.game.load.image('overlay'+a, 'assets/level'+a+'/overlay.png');
        this.game.load.image('underlay'+a, 'assets/level'+a+'/underlay.png');
        this.game.load.image('game'+a, 'assets/level'+a+'/game.png');
        this.game.load.physics('gamePhysics'+a, 'assets/level'+a+'/game.json');
        this.game.load.physics('obstaclesPhysics'+a, 'assets/level'+a+'/obstacles.json');
        this.game.load.physics('trapsPhysics'+a, 'assets/level'+a+'/traps.json');
        }
        this.game.load.image('slide1', 'assets/level6/slide1.png');
        this.game.load.image('slide2', 'assets/level6/slide2.png');
        
        




        this.load.onFileComplete.add(this.fileLoaded, this);
    },
    create: function () {
        this.game.state.start('mainmenu', Main.MainMenu);
        //this.game.state.start('thegame', Main.Thegame);
    },
    fileLoaded: function (progress) {
        // console.log(this.loaderFull.crop.width);
        this.loaderEmpty.crop.left = (135 / 100) * progress;
        this.loaderFull.crop.width = (135 / 100) * progress;
    }
}