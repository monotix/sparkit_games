﻿module Hanglide {

    export class LettersControler {


        public static _instance: LettersControler = null;
        private _currentLetters: Array<string>;
        private _currentWorld: string;
        private _maxLetter: number = 2;
        private _corretAnswer: number = 0;
        private _maxBadAnswer: number = 3;
        private _badAnswer: number = 0;
        private _gameControler: GameControler = GameControler.getInstance();
       
        private _currentSequence: number = 0;
        private _maxSequence: number = 2;
        private _lettersView: LettersView;
        public correctLeters: Array<string> = new Array();
        construct() {

            if (LettersControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            LettersControler._instance = this;

        }





        public static getInstance(): LettersControler {

            if (LettersControler._instance === null) {

                LettersControler._instance = new LettersControler();


            }


            return LettersControler._instance;



        }


        public phonemes: Array<string> = new Array(
            "a","e","i","o","u"
            );


      


        public randomPhoneme(): string {
            var i: number = Math.floor(Math.random() * this.phonemes.length);
            return this.phonemes[i];
        }

        public addCorrectLetters(str: string) {
            this.correctLeters.push(str);
            if (this.correctLeters.length > 3) {


            }

        }

        setRandomLetters(howmany: number = 1) {
            howmany = this._gameControler.currentLevel + 1;
            this._currentLetters = new Array();
            console.log("ile liter do zapamietania", howmany, this._gameControler.currentLevel);
            for (var i: number = 0; i < howmany; i++) {
                var num = this.getRandomNumber(this.phonemes.length - 1);
                console.log("phenomes", num);
                this._currentLetters.push(this.phonemes[num]);


            }
            console.log("current", this._currentLetters);
            this._currentWorld = this._currentLetters[0];
            this._maxLetter = this._currentLetters.length;


        }

        checkWord(str: string) {

            if (str == this._currentLetters[this._corretAnswer]) {
                console.log("dobra liltera");
                this._gameControler.level._panel.slotsAddLetter(this._corretAnswer,str);
                this._corretAnswer++;
                this.checkWinOrDie(false);
            }
            else {
                //this._badAnswer++;
                //this.checkWinOrDie(true);
              
                this._corretAnswer = 0;
                this._badAnswer = 0;
                this._gameControler.level._panel.clearSlots();
                    this._gameControler.showLetter.showLetters();
             
                console.log("zla liltera");
            }

        }

        checkWinOrDie(die: boolean) {
            console.log("die", this._badAnswer, "goog", this._corretAnswer);
            if (this._badAnswer == this._maxBadAnswer) {

                console.log("die you");
              //  this._gameControler.level.showPrompt(false, true);
                
                this._corretAnswer = 0;
                this._badAnswer = 0;
                return;

            }
           
            else if (this._corretAnswer == this._maxLetter) {

                this._currentSequence++;
                this._badAnswer = 0;
                
                this._gameControler.level._panel.addPoints();
                this._gameControler.level._panel.clearSlots();
                    this._corretAnswer = 0;

                  //  this.setRandomLetters(this._gameControler.currentLevel + 1);
                 //   this._gameControler.level.showPrompt(true);
                  //  this._gameControler.level.sequenceLevel(this._currentSequence + 1 + "/" + 2);

               

                return;
            }

          /*  if (die) {
                this._corretAnswer = 0;
             //   this._gameControler.level.showPrompt(false);
             //   this._gameControler.level.removeSequence();
            }
            else {



            }
            */


        }

        changeMaxLetter() {

            if (this._gameControler.currentLevel % 2 == 0) this._maxLetter = 3;
            else this._maxLetter = 2;


        }



        getRandomNumber(max: number, min: number = 0): number {
            //--max;
            return Math.floor(Math.random() * (max - min) + min);

        }

        isCorrectLetter(str: string): boolean {

            for (var i: number = 0; i < this.phonemes.length; i++) {
                if (str == this.phonemes[i]) return true;


            }
            return false;

        }

        getPopLetter(): Array<any> {

            var randNum: number = this.getRandomNumber(2);

            console.log("random number letter", randNum);
            var str: string;
            if (randNum == 1) {

                str = this.phonemes[this.getRandomNumber(this.phonemes.length)];

            }
            else {

                var num: number = this.getRandomNumber(this._currentLetters.length);

                str = this._currentLetters[num];

            }

          //  var letter = this.isCorrectLetter(str);
            var arr: Array<any> = new Array({ str: str});



            return arr;

        }

        public get currentLetters(): Array<string> {

            return this._currentLetters;

        }

        public set currentLetters(arr:Array<string>) {

            this._currentLetters = arr;

        }


        public get maxSequence():number {

            return this._maxSequence;
        }
       
        level1(): Array<any> {
             /*  new Array(

                    { x: 192, y: 21, name: "popletter" },
                    { x: 27, y: 183, name: "popletter" }


                    ),

                new Array(

                    { x: 31, y: 33, name: "popletter" },
                    { x: 311, y: 293, name: "popletter" }
                    ),
            */
            var arr;
            if (this._gameControler.currentLevel == 0) arr = Level1.Level();
            else if (this._gameControler.currentLevel == 1) arr = Level2.Level();   
            else if (this._gameControler.currentLevel == 2) arr = Level3.Level();   
            else if (this._gameControler.currentLevel == 3) arr = Level4.Level();
            else if (this._gameControler.currentLevel == 4) arr = Level5.Level();

          //[this.getRandomNumber(arr.length, 0)]
            return arr;

        }

        public set lettersView(letttersV: LettersView) {

            this._lettersView = letttersV;

        }

        public get lettersView():LettersView {

            return this._lettersView;

        }



    }


}  