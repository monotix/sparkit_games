﻿declare function APIsetLevel(userID, gameID, userLevel);
declare function APIgetLevel(userID, gameID);
declare var userID: any;
declare var gameID: any;
module Hanglide {

    export class GameControler {


        public static _instance: GameControler = null;

      

        public finishGame: boolean = false;
        private _currenLevel: number = 0;
        private _level: Level;
     
        public gameOver: boolean = true;
        public maxLevel: number = 3;
        public currentLevel: number = 0;
        public drageStop: boolean = false;
        public dragButton: boolean = false;
        public dragButtonType: string = "";
        public SPEEDX: number = 150;
        public SPEEDY: number = 150;
        private _showLetters: ShowLetter;
        private _panel: PanelGame;
        public latawiecStop: boolean = false;
        public mainSound: Phaser.Sound;
        public stopTime: boolean = true;
        construct() {

            if (GameControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            GameControler._instance = this;

        }




        public static getInstance(): GameControler {

            if (GameControler._instance === null) {

                GameControler._instance = new GameControler();


            }


            return GameControler._instance;



        }

        public endGame(win: boolean) {
            console.log("this._currenLevel", this.currentLevel);
            if (win && this.currentLevel < 4) {

                this.currentLevel++;
                this._panel.clearSlots();
                this._panel.changeSlots();
                LettersControler.getInstance().changeMaxLetter();
                this.level.showFinish(true);
                APIsetLevel(userID, gameID, userLevel + 1);
            }
            else if (this.currentLevel > 3 && win){
                this._panel.clearSlots();
                this._panel.changeSlots();
                LettersControler.getInstance().changeMaxLetter();
                this.level.showFinish(true);

            }

            else {
                this.level.showFinish(false);
                this._panel.clearSlots();
            }

            LettersControler.getInstance().lettersView.removeObstacle();
           

        }

        public restart() {
            this._currenLevel = 0;
            //this.level.playAgain();
        }

        public nextLevel() {

            this.currentLevel++;

         //   this.level.showPrompt(true, true);

        }

        setLevel(level: Level) {

            this._level = level;

        }

        public get level(): Level {

            return this._level;
        }

        reset() {

            this.drageStop = false;
            this.dragButton = false;
        }

        public set showLetter(letters: ShowLetter) {

            this._showLetters = letters;
        }

        public get showLetter(): ShowLetter {

            return this._showLetters;

        }
     

        public set panel(panelGame: PanelGame) {

            this._panel = panelGame;
        }

        public get panel(): PanelGame {
            return this._panel;
        }

        public setSound(sound_: Phaser.Sound) {
            this.mainSound = sound_;
        }

    }


}  