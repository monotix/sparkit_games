﻿module Hanglide {

    export class Level extends Phaser.State {


        private _bg: Phaser.Group;
        private _highBg: Phaser.Group;
      
        private _gameControler: GameControler;

      
    

        private _sequenceGr: Phaser.Group;
      


     
        private _showLetterGr: Phaser.Group;

      

      
        private _finishScreen: FinishScreen;
        private _special: Phaser.Sprite;
        private _jumpBtn: Phaser.Button;
        private _timer: number = 0;
        private _timerCounter;
        private rightKey;
        private _blokada: Phaser.Graphics; 
        private jumpPressed: boolean = false;
        private _blokadaGr: Phaser.Group;
      
        private _itemsGr: Phaser.Group;
        private _itemsDropGr: Phaser.Group;
        private _trashGr: Phaser.Group;
      

        private _player: Player;
        private _playerGr: Phaser.Group;
        private _tween: Phaser.Tween;
        private _showLetter: ShowLetter;
        public _panel: PanelGame;
        private _bgImg: Phaser.TileSprite;
        private _bgLowCloudImg: Phaser.TileSprite;
        private _bgHighCloudImg: Phaser.TileSprite;
        private _letersView: LettersView;
        private _isDown: boolean = false;

        private _currentLevel: number = 0;
        private helpGr: Phaser.Group;
        private _helpBtn: Phaser.Button;
        private _finishGr: Phaser.Group;
        private _instructionShow;
       
        create() {

            this._gameControler = GameControler.getInstance();

            this._gameControler.setLevel(this);
            this._gameControler.currentLevel = (userLevel)-1;
            this._playerGr = this.add.group();
            this.stage.addChild(this._playerGr);
            this._player = new Player(this.game, "player");
            this._playerGr.add(this._player);
            this._playerGr.enableBody = true;
            this._player.checkWorldBounds = true;
            this._player.anchor.setTo(0.5, 0.5);
            this._player.x = this.world.width / 2;
            this._player.y = this.world.height / 2;
            this._blokadaGr = this.add.group();

            this.game.physics.enable(this._player, Phaser.Physics.ARCADE);

            this._bg = this.game.add.group();
            this._bgImg = this.game.add.tileSprite(0, 0, 800, 600, "bgGame" + this._gameControler.currentLevel, 0, this._bg);
            this._bgLowCloudImg = this.game.add.tileSprite(0, 0, 800, 600, "lowCloud", 0, this._bg);
            this._bgLowCloudImg.alpha = 0.5;

            this._trashGr = this.add.group();
            this._trashGr.enableBody = true;

            this._letersView = new LettersView(this.game);
            this.stage.addChild(this._letersView);

            this._highBg = this.game.add.group();
            this._bgHighCloudImg = this.game.add.tileSprite(0, 0, 800, 600, "highCloud", 0, this._highBg);
            this._bgHighCloudImg.alpha = 0.5;
            this.stage.addChild(this._highBg);

            this._showLetter = new ShowLetter(this.game, 0, 0, "");
            this._gameControler.showLetter = this._showLetter;


          //  this._showLetterGr = this.add.group();

            //this._showLetterGr.add(this._showLetter)
            this.stage.addChild(this._showLetter);
            this._showLetter.playCountDown();

            this._panel = new PanelGame(this.game);
            this._gameControler.panel = this._panel;
            this._panel.startTimer();


         

            this._panel.x = this.world.width/2 -  this._panel.width / 2;
            this.stage.addChild(this._panel);
        
       
            this.game.input.onUp.add(function () {
                console.log('up');
                this._isDown = false;
                
            },this);
            this.game.input.onDown.add(function () {
                this._isDown = true;
                console.log('down', this._isDown);
            },this);


         
            this.helpGr = this.add.group();
            this.stage.addChild(this.helpGr);
            this._helpBtn =  this.add.button(0, 0, "helpBtn", this.helpBtnClick, this, 1, 0,0,0,this.helpGr);
            this.helpGr.x = this.game.stage.width - this.helpGr.width;
            this.helpGr.y = this.game.stage.height - this.helpGr.height;

            this._helpBtn.buttonMode = true;
            this._helpBtn.inputEnabled = true;
            this._helpBtn.input.priorityID = 11;


          
            this.game.input.onDown.add(function () { if (this.game.paused) { this.game.paused = false; } if (this._instructionShow) { this.stage.removeChild(this._instructionShow); this._instructionShow.kill(); this._instructionShow = null; }}, this);
            
          
        }

        private helpBtnClick() {

            console.log("pokaz instrukcje");
            this.game.paused = !this.game.paused;

            if (this._instructionShow == null) this._instructionShow = this.add.image(0, 0, "firstInstruction", 0);
            

           // this.add.image(0, 0, "firstInstruction", 0);
            this._instructionShow.width = this.game.world.width;//this.stage.width;
            this._instructionShow.height = this.game.world.height;
            this.stage.addChild(this._instructionShow);

        }

        public playAgain() {
            this.changeBackground();
            this._letersView.getObstacle(true);
            this._panel.startTimer();
            this._showLetter.randomLetters();
            this._showLetter.playCountDown();
        }

        changeBackground() {

            this._bgImg = this.game.add.tileSprite(0, 0, 800, 600, "bgGame" + this._gameControler.currentLevel, 0, this._bg);
          
        }
        public showFinish(win: boolean) {

            this._finishScreen = new FinishScreen(this.game);
            this._finishScreen.show(win);

            this._letersView.changeSpeed(true);
            this._letersView.removeObstacleLetters();
            LettersControler.getInstance().changeMaxLetter();
            this.stage.addChild(this._finishScreen);
            this.stage.addChild(this.helpGr);

        }

        movePlayer(pointer) {
           
         /*   if (this._tween != null && this._tween && this._tween.isRunning) {
                this._tween.stop();
            }

            var rotation = (this.game.physics.arcade.angleToPointer(this._player, pointer));

            var duration = (this.game.physics.arcade.distanceToPointer(this._player, pointer) / 300) * 1000;
         //   this._player.body.velocity = 50;
          //  this._tween = this.game.add.tween(this._player).to({x: pointer.x, y:pointer.y ,rotation : rotation }, duration, Phaser.Easing.Quadratic.InOut, true);
            //    this._tween.onComplete.add(this.movePlayerComplete, this);
           

            this._tween = this.game.add.tween(this._player).to({
                x: Math.floor(pointer.x),
                y: Math.floor(pointer.y),
                rotation: rotation
            }, duration, Phaser.Easing.Quadratic.Out, true).interpolation(function (v, k) {
                return Phaser.Math.bezierInterpolation(v, k);
            });*/

            //tween.repeat(Infinity);
            
        }


        update() {

            this._bgImg.tilePosition.y += 1;
            this._bgLowCloudImg.tilePosition.y += 1.5;
            this._bgHighCloudImg.tilePosition.y += 2;
            
            if (this._isDown && !this._gameControler.latawiecStop && mobile) {

              //  this._player.rotation = this.game.physics.arcade.moveToPointer(this._player, 60, this.game.input.activePointer, 500);
                this._player.rotation =  this.game.physics.arcade.moveToPointer(this._player, 60, this.game.input.activePointer, 500);

            }
            else if (!this._gameControler.latawiecStop && !mobile) {

                this._player.rotation = this.game.physics.arcade.moveToPointer(this._player, 60, this.game.input.activePointer, 350);
                //this.game.physics.arcade.moveToPointer(this._player, 60, this.game.input.activePointer, 500);

            }
            else {
                this._player.body.velocity.setTo(0, 0);

            }
            this.game.physics.arcade.overlap(this._player, this._letersView, this.collision, null, this);

        }

        collision(object, object2) {

       


                if (object2.name == "popletter") {

                    object2.body.setSize(0, 0, -1200, -1600);
                    // this._sequence.addLetter(object2.letter);
                    LettersControler.getInstance().checkWord(object2.letter);
                  
                    object2.visible = false;
                    object2.kill();
                    object2.destroy(true);
                    this._letersView.removeObjectArray();


                }
              


            

        }

      


        render() {
            
              /* this.game.debug.quadTree(this.game.physics.arcade.quadTree);
         
               this.game.debug.body(this._player);
               this._letersView.forEach(function (item) {
                    item.body.velocity = 0;
                
                   this.game.debug.body(item);
               }, this);
              
              
                   // item.body.velocity = 0;
                
               this.game.debug.body(this._player);
              
             */

        }

     

    }
}
