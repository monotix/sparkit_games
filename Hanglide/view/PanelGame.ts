﻿module Hanglide {


    export class PanelGame extends Phaser.Group {

        private _timer: Phaser.Sprite;
        private _points: Phaser.Sprite;
        private _slots: Phaser.Sprite;
        private _time: Phaser.Text;
        private _seconds: number = 60;
        private _pointTxt: Phaser.Text;
        private _pointsCount: number = 0;
        private _gameControler: GameControler = GameControler.getInstance();
        private _slotGr: Phaser.Group;
        public stopTime: boolean = true;
        constructor(game: Phaser.Game) {

            super(game);


           
            this._slots = new Phaser.Sprite(this.game, 0, 0, "slots", 0);
            
            this.add(this._slots);
            this._slotGr = this.game.add.group();
            this.add(this._slotGr);
            this._timer = new Phaser.Sprite(this.game, 0, 0, "timer", 0);
            this._timer.x = this._slots.x + this._slots.width;
            this._points = new Phaser.Sprite(this.game, 0, 0, "points", 0);

            this._points.x = this._timer.x + this._timer.width - 50;
            this._points.y = 5;

            this.add(this._points);
            this.add(this._timer);
           
            var style = { font: "45px Arial", fill: "#ffffff", align: "left" };
            this._time = this.game.add.text(0, 0, "60", style);
            this._time.x = this._timer.x + this._timer.width / 2 - this._time.width / 2;
            this._time.y = 10;
            this.addChild(this._time);

            this._pointTxt = this.game.add.text(0, 0, "0", style);
            this._pointTxt.x = this._points.x + this._points.width / 2 - this._pointTxt.width / 2;
            this._pointTxt.y = 8;
            this.add(this._pointTxt);
            this.changeSlots();
        }

        changeSlots() {
            console.log("this._gameControler.currentLevel ", this._gameControler.currentLevel);
            if (this._gameControler.currentLevel % 2 == 0) this._slots.frame = 0;
            else this._slots.frame = 1;
        }

        slotsAddLetter(countCorretAnswer: number,letter:string) {

            var popLetter: PopLetter = new PopLetter(this.game, 0, 0, "popletter");
            popLetter.x = this._slots.x + popLetter.width * countCorretAnswer + 6;
            popLetter.y = 4;
            popLetter.addLetter(letter);
            this._slotGr.add(popLetter);

        }

        clearSlots() {
            this._slotGr.forEach(function (item) {

                item.kill();

            }, this);

        }

        public addPoints(point:number=5) {

            this._pointsCount += point;
            
            this._pointTxt.setText("" + this._pointsCount);

        }

        public startTimer() {

          
            this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);
        }

        public stopTimer() {


            this.game.time.events.removeAll();
        }


        private updateTimer() {

            if (!this._gameControler.stopTime) {

                if (this._seconds > 0) {

                    this._seconds--;
                }
                else {
                    this.stopTimer();
                    this._seconds = 60;
                    this._gameControler.endGame(this._pointsCount > 5);

                }

                var sec: String;


                sec = "" + this._seconds;




                this._time.setText("" + sec);
                this._time.x = this._timer.x + this._timer.width / 2 - this._time.width / 2;
            }
        }

    }

}