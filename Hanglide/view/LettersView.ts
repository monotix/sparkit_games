﻿module Hanglide {

    export class LettersView extends Phaser.Group {



        private _longerObstacle: Array<any>;
        private _game: Phaser.Game;
        private _obstacleArr: Array<any>;
        private _whenAddNewScreen: PopLetter;
        private _gameControler: GameControler;
      
        private _letterControler: LettersControler;
        private _boundsOut: Phaser.Sprite;
        private _letterObjectArr: Array<any>;
        constructor(game: Phaser.Game) {

            super(game);
            this._game = game;
            this._gameControler = GameControler.getInstance();
            this._letterControler = LettersControler.getInstance();



            this.enableBody = true;


         

            this._boundsOut = new Phaser.Sprite(this._game, 0, 0, "");
            var grfx: Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);

            grfx.beginFill(0x000000, 1);
            grfx.drawRect(0, 0, 100, 100);
            grfx.endFill();

            this._boundsOut.addChild(grfx);
            this._boundsOut.y = this.game.height - 100;
            this._boundsOut.x = this.game.width;
           
            this._letterControler.lettersView = this;
         
         
        }







        getObstacle(newLevel:boolean = false) {
          
            console.log("przed obstacle:");
           
            if (this._letterObjectArr != null && this._letterObjectArr.length > 0 && !newLevel) return 0;
            console.log("po obstacle:");
            this.delObstacles();
          
            this._obstacleArr = new Array();
            this._longerObstacle = new Array();
         
            var obstcaleArr: Array<any> = this._letterControler.level1();
            var max: number = obstcaleArr.length;
         
            this._letterObjectArr = new Array();
            for (var i: number = 0; i < max; i++) {

                var obj = obstcaleArr[i];
              
              
                  
                    var popletter: PopLetter = new PopLetter(this._game, obj[0].x, obj[0].y, "popletter");
                    popletter.points = obj;
                    var letter = this._letterControler.getPopLetter();

                    popletter.addLetter(letter[0].str);

                 
                    this.add(popletter);
                    //popletter.body.velocity = new Phaser.Point(this._gameControler.SPEEDX, this._gameControler.SPEEDY);
                    this._obstacleArr.push(popletter);
                    popletter.body.setSize(40, 40, 0, 5);
                    popletter.body.velocity.setTo(0, 0);
                
                    this._letterObjectArr.push(popletter);

                  



            }




            

        

        }

        addAgain() {


        }

        getTheLonger(posx: number, width_: number, i: number) {
          
            if (this._longerObstacle.length > 0) {

                if (this._longerObstacle[0][0] < posx + width_) {

                    this._longerObstacle[0] = new Array(posx + width_, i);

                }
            }
            else {

                this._longerObstacle.push(new Array(posx + width_, i));

            }

        }



       


        obstacleOutOfScreen() {
            console.log("boats out of screen");
            // this.delObstacle();
            // this.getObstacle();
        }

        delObstacle() {


            for (var i: number = 0; i < this._obstacleArr.length; i++) {

                if (this._obstacleArr[i].world.x + this._obstacleArr[i].width < 0) {
                    this._obstacleArr[i].destroy(true);
                    this._obstacleArr.splice(i, 1);

                    --i;


                }

            }

        }

        delObstacles() {


            this.forEach(function (item) {

                if (item != null && item != undefined && this.toGlobal(item.position).x < -1000) {
                    console.log("niszcze obiekt", item.name, this.toGlobal(item.position).x);
                    item.destroy(true)

                }
                //  if (!stop && (item.name == "boatsmedium" || item.name == "boatssmall")) item.body.velocity.x = -70;
              
            }, this);


        }
        private _stop: boolean = false;
        changeSpeed(stop: boolean = false, changePos: boolean = false) {
            var speed = (stop) ? 0 : this._gameControler.SPEEDX;
            var speedy = (stop) ? 0 : this._gameControler.SPEEDY;
            this._gameControler.latawiecStop = stop;
            this._stop = stop;
            this.forEach(function (item) {
                console.log("stop kurwa", stop, speed);
             //   item.body.velocity.setTo(speed, speedy);
             //   item.body.velocity = new Phaser.Point(speed, speedy);
                if (changePos) {
                 //   item.x += 150;
               //     item.y += 100;

                }
                //  if (!stop && (item.name == "boatsmedium" || item.name == "boatssmall")) item.body.velocity.x = -70;
              
            }, this);
            //   this._boundsOut.body.velocity.x = -270;
            //   this._boundsOut.body.velocity.y = 0;

        }


        removeObstacleLetters() {


            this.forEach(function (item) {
                if (item != undefined && item.name == "popletter") item.destroy(true);

            }, this);
            if (this._letterObjectArr != null) {

                for (var i: number = 0; i < this._letterObjectArr.length; i++) {

                    this._letterObjectArr[0].destroy(true);
                    this._letterObjectArr.splice(0, 1);

                }

                

            }

        }

        removeObstacle() {

            this._whenAddNewScreen = null;
            this.forEach(function (item) {
                if (item != undefined) item.destroy(true);

            }, this);

            
        }



        getRandom(max: number): number {

            return Math.floor(Math.random() * max);

        }


        checkObjectArrayIsEmpty() {
            console.log("sprawdzam ile zostalo", this._letterObjectArr.length);
            if (this._letterObjectArr != null && this._letterObjectArr.length < 1) {

                this.getObstacle();

            }


        }

        removeObjectArray() {

            this._letterObjectArr.shift();
            this.checkObjectArrayIsEmpty();

        }

        private licz: number = 0;

        update() {

          //  console.log("osttatni", this.toGlobal(this._whenAddNewScreen.position).x);
            if (this._whenAddNewScreen != null && this.toGlobal(this._whenAddNewScreen.position).y < -50) {

              //  this.getObstacle();

            }
            if (this._stop) return 0;
            this.licz++;
          //  if (this.licz > 1) {
                this.forEach(function (item) {

                    if (item != null && item.name == "popletter") {

                        item.pathindex = Math.min(item.pathindex, item.points.length - 1);
                        // this.game.physics.arcade.moveToXY(item, item.points[item.pathindex].x, item.points[item.pathindex].y, 100);
                        item.x = item.points[item.pathindex].x;
                        item.y = item.points[item.pathindex].y;
                        item.pathindex++;
                        if (item.pathindex >= item.points.length) {

                            item.destroy(true);
                            this.remove(item);
                            this._letterObjectArr.shift();
                            this.checkObjectArrayIsEmpty();
                            //this.getObstacle();
                        }


                    }

                }, this);
                this.licz = 0;
          //  }

           

        }

    }


}  