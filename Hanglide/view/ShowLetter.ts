﻿module Hanglide {


    export class ShowLetter extends Phaser.Group {

        private _bgBlack: Phaser.Graphics;
        private _bgBlackContener: Phaser.Sprite;
        private _bg: Phaser.Sprite;
        private _countDown: Phaser.Sprite;
        //aeiou
        private _lettersControler: LettersControler = LettersControler.getInstance();
        private _lettersArr: Array<string> = new Array("a", "e", "i", "o", "u");
        private _drawLeters: Array<string>;
        private _letter: Phaser.Sprite;
        
        constructor(game:Phaser.Game,x:number,y:number,key:string) {

            super(game);

            this._bgBlack = new Phaser.Graphics(this.game, 0, 0);
            this._bgBlack.beginFill(0x000000, 0.6);
            this._bgBlack.drawRect(0, 0, this.game.world.width, this.game.world.height);
            this._bgBlack.endFill();

            this._bgBlackContener = new Phaser.Sprite(this.game, 0, 0, "", 0);
            this._bgBlackContener.addChild(this._bgBlack);
            this.addChild(this._bgBlackContener);

            this._bg = new Phaser.Sprite(this.game, 0, 0, "letterBg", 0);
            this.addChild(this._bg);

            this._bgBlackContener.buttonMode = true;
            this._bgBlackContener.inputEnabled = true;
            this._bgBlackContener.input.priorityID = 10;

            this._countDown = new Phaser.Sprite(this.game, 0, 0, "countdown", 0);
            this._countDown.animations.add("play");
            this._countDown.x = this.game.world.width / 2 - this._countDown.width / 2;
            this._countDown.y = this.game.height / 2 - this._countDown.height / 2;

            this._letter = new Phaser.Sprite(this.game, 0, 0, "letters", 0);
            this._letter.x = this.game.world.width / 2 - this._letter.width / 2;
            this._letter.y = this.game.world.height / 2 - this._letter.height / 2; 

            this._letter.animations.add("a", [0]);
            this._letter.animations.add("e", [1]);
            this._letter.animations.add("i", [2]);
            this._letter.animations.add("o", [3]);
            this._letter.animations.add("u", [4]);

            this.randomLetters();

        }


        playCountDown() {
            LettersControler.getInstance().lettersView.changeSpeed(true);
            this._letter.visible = false;
            this._bg.visible = false;
            this.addChild(this._countDown);
            this._countDown.animations.getAnimation("play").onComplete.add(this.finishCount, this);
            this._countDown.play("play", 24, false);
            this.visible = true;
        }

        finishCount() {

            this.removeChild(this._countDown);
            this.showLetters();

        }

        private _currentCount: number=0;
        private _maxLetter: number;
        showLetters() {
            this._lettersControler.lettersView.changeSpeed(true);
            this.visible = true;
            this.add(this._letter);
            this._maxLetter = this._drawLeters.length;
            this._letter.visible = true;
            this._bg.visible = true;
            this._letter.animations.play(this._drawLeters[this._currentCount], 24, false);
            if (this._currentCount < this._maxLetter) {

                this.game.time.events.add(Phaser.Timer.SECOND * 2, function () {

                    this._letter.visible = false;

                    this._currentCount++;


                }, this)

                this.game.time.events.add(Phaser.Timer.SECOND * 2.5, function () {

                 
                    this._letter.visible = true;
                    this.showLetters();



                }, this)

            }
            else {


                this._currentCount = 0;
                this.visible = false;

                LettersControler.getInstance().lettersView.getObstacle();
                LettersControler.getInstance().lettersView.changeSpeed(false);
            }

          
          

        }

        randomLetters() {

            var ileLiter: number = this._lettersControler.maxSequence;
            this._drawLeters = new Array();
            for (var i: number=0; i < ileLiter; i++) {
                console.log(this._lettersArr[this.getRandomNumber(this._lettersArr.length, 0)]);
                this._drawLeters.push(this._lettersArr[this.getRandomNumber(this._lettersArr.length, 0)]);


            }

            this._lettersControler.currentLetters = this._drawLeters;
          



        }


        getRandomNumber(max: number, min: number = 0): number {
            //--max;
            return Math.floor(Math.random() * (max - min) + min);

        }


    }


}