﻿declare function APIsetLevel(userID, gameID, userLevel);
declare function APIgetLevel(userID, gameID);
declare var userID: any;
declare var gameID: any;

module Hanglide {


    export class FinishScreen extends Phaser.Group {
        private _finishScreen: Phaser.Sprite;
        private _playButton: Phaser.Button;
        private _quit: Phaser.Button;
        private _game: Phaser.Game;
        private _anim: Phaser.Sprite;
        private _gameControl: GameControler = GameControler.getInstance();
        private _bgBlack: Phaser.Graphics;
        private _bgSpr: Phaser.Sprite;
        private gameOverSpr: Phaser.Sprite;
        private youWinSpr: Phaser.Sprite;
        private screenWinOrGameOver: any;
      
        constructor(game: Phaser.Game) {
            super(game);

            this.game = game;
            this.create();
        }

        create() {



            this._bgBlack = new Phaser.Graphics(this.game, 0, 0);
            this._bgBlack.beginFill(0x000000, 0.6);
            this._bgBlack.drawRect(0, 0, this.game.width, this.game.height);
            this._bgBlack.endFill();
            
            this._bgSpr = new Phaser.Sprite(this.game, 0, 0, "");
            this._bgSpr.addChild(this._bgBlack);
            this.add(this._bgSpr);

           
            this._bgSpr.buttonMode = true;
            this._bgSpr.inputEnabled = true;
            this._bgSpr.input.priorityID = 10;

            this._bgSpr.events.onInputDown.add(this.onDown, this);


           this.gameOverSpr = new PopUp(this.game, this, 0, 0);
            this.youWinSpr = new PopUpWin(this.game, this, 0, 0);


          

          

            
            this.add(this.gameOverSpr);

            this.addChild(this.youWinSpr);

           // this._playButton = new Phaser.Button(this.game, 0, 0, "playScoreScreen", this.playAgain, this, 1, 0, 1, 0);

          //  this._quit = new Phaser.Button(this.game, 300, 10, "playScoreScreen", this.quitClick, this, 3, 2, 3, 2);
          //  this._playButton.x = 257;
        //    this._playButton.y = 358;
        //    this._playButton.input.priorityID = 10;
        //    this._quit.input.priorityID = 10;
        //    this._quit.x = 427;
       //     this._quit.y = 358;
       //     this._finishScreen.addChild(this._playButton);
       //     this._finishScreen.addChild(this._quit);


          //  this._finishScreen.visible = false;
          //  this._playButton.visible = false;

         //   this._quit.visible = false;
            
        }

        onDown() {
            console.log("ondown finish");
        }

        fail() {

            this._anim = new Phaser.Sprite(this.game, 0, 0, "fail1", 0);
            this._anim.animations.add("play");
            this._anim.x = this.game.width / 2 - this._anim.width / 2;
            this._anim.y = this.game.height / 2 - this._anim.height / 2;
            this._anim.animations.getAnimation("play").onComplete.add(this.completeAnim, this);
            this.add(this._anim);
            this._anim.animations.play("play", 24, false, true);

            this.screenWinOrGameOver = this.gameOverSpr;

        }

        win() {
         
            this._anim = new Phaser.Sprite(this.game, 0, 0, "success1", 0);
            this._anim.animations.add("play");
            this._anim.x = this.game.width / 2 - this._anim.width / 2;
            this._anim.y = this.game.height / 2 - this._anim.height / 2;
         //   this._anim.animations.getAnimation("play").onComplete.add(this.completeAnim, this);
          //  this.add(this._anim);
            this._anim.animations.play("play", 12, false, true);

            this.screenWinOrGameOver = this.youWinSpr;
            this.completeAnim();

        }

        completeAnim() {

            this.screenWinOrGameOver.activeButton();
            

            this.game.time.events.add(Phaser.Timer.SECOND * 2, function () {

                this.screenWinOrGameOver.visible = true;
                




            }, this)


        }

        show(youWin: boolean) {

            if (youWin) {

                this.win();
            }
            else {
                this.fail();

            }
        }

        playAgain() {

           
            console.log("play again");
            this.removeChild(this._bgBlack);
            this.removeChild(this._bgSpr);
            this._bgSpr.kill();
            this.removeChild(this.gameOverSpr);
            this.removeChild(this.youWinSpr);
            this.gameOverSpr.kill();
            this.youWinSpr.kill();
            //this.destroy(true);
            // this.removeAll(true);
            this._gameControl.level.playAgain();
          

        }

        quitClick() {
            console.log("quit");

        }


        countFrames(start: number, end: number): Array<number> {
            var countArr: Array<number> = new Array();
            for (var i: number = start; i < end; i++) {

                countArr.push(i);


            }

            return countArr;


        }


        public hideScreenAfterFinishPlaye() {

            this.playAgain();
        }

    }

}   