﻿module Hanglide {

    export class Preloader extends Phaser.State {


        preload() {
          
            var bg = this.add.image(0, 0, "bg", 0);

            var blackProgress = this.add.image(0, 0, "progressBarBlack", 0);
            var blueProgress = this.add.sprite(0, 0, "progressBarBlue", 0);


            blackProgress.position.setTo(this.stage.width / 2 - blackProgress.width / 2, this.stage.height - blackProgress.height - 10);

            blueProgress.position = blackProgress.position;
            this.load.setPreloadSprite(blueProgress);


            this.load.image("mainMenuBg", "assets/mainMenu/splashScreen.png");
            this.load.atlasJSONArray('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');
            this.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
            this.load.audio("MainMenuSn", "assets/audio/ScubaDudeTheme.mp3");
            this.load.audio("overButtonA", "assets/audio/overButton.mp3");
            this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");

            for (var i: number = 0; i < 5; i++) {
                this.load.image("bgGame"+i, "assets/hanglide/Background"+i+".png"); 
            }
            this.load.image("lowCloud", "assets/hanglide/LowCloud0.png");
            this.load.image("highCloud", "assets/hanglide/HighCloud0.png");


            this.load.image("player", "assets/hanglide/player/player.png");
            this.load.image("letterBg", "assets/hanglide/letter.png");

            this.load.image("timer", "assets/hanglide/panel/timer.png");
            this.load.image("points", "assets/hanglide/panel/points.png");
            this.load.atlasJSONArray('letters', 'assets/hanglide/letters/letters.png', 'assets/hanglide/letters/letters.json');
            this.load.atlasJSONArray('popletter', 'assets/hanglide/letters/letters2.png', 'assets/hanglide/letters/letters2.json');
            /* instructions */
          //  this.load.image('prompt', 'assets/snow/prompt/instruction1.png');
            this.load.audio("WakeIntro", "assets/audio/instruction/WakeIntro.mp3");
            this.load.audio("WakeSuccess0", "assets/audio/instruction/WakeSuccess0.mp3");
            this.load.audio("WakeWrong0", "assets/audio/instruction/WakeWrong0.mp3");

            
            this.load.image("firstInstruction", "assets/instructionsScreen.png");
           // this.load.image("sequence", "assets/snow/letterClip.png");
            /* player */
            this.load.atlasJSONArray('slots', 'assets/hanglide/panel/slots.png', 'assets/hanglide/panel/slots.json');
       
           
          
            this.load.atlasJSONArray('countdown', 'assets/hanglide/countdown.png', 'assets/hanglide/countdown.json');

           

           // this.load.image("clear", "assets/Hanglide/clearBtn.png");
            //this.load.image("finish", "assets/Hanglide/finishBtn.png");

      
           // this.load.image("rotationDrag", "assets/Hanglide/rotation.png");



            /* obstacle */

        /*    this.load.image("piasek", "assets/water/obstacle/wood2.png");
            this.load.image("wood", "assets/water/obstacle/wood.png");
            this.load.image("hammer", "assets/water/obstacle/hammer.png");
            this.load.image("turtle", "assets/water/obstacle/turtle.png");
            this.load.image("zielone", "assets/water/obstacle/zielone.png");
            this.load.image("ball", "assets/water/obstacle/ball.png");
            this.load.image("ball2", "assets/water/obstacle/ball2.png");
            this.load.image("walen", "assets/water/obstacle/walen.png");
            */
          

          //  this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");

         

            for (var i: number = 1; i < 5; i++) {

                this.load.audio("fail" + i, "assets/audio/instruction/fail" + i + ".mp3");


            }

            for (var i: number = 1; i < 5; i++) {

                this.load.audio("success" + i, "assets/audio/instruction/success" + i + ".mp3");


            }

        /*    for (var i: number = 1; i < 4; i++) {
                this.load.image("item"+i, "assets/robo/item"+i+".PNG");
            }

            for (var i: number = 1; i < 4; i++) {
                this.load.image("robot" + i, "assets/robo/robot" + i + ".PNG");
            }
            */
          
            /*skrzynie*/

           

        

            /*end skrzynie"*/


            this.load.atlasJSONArray("scoreScreen", "assets/scoreScreen/scoreScreen.png", "assets/scoreScreen/scoreScreen.json");
            this.load.atlasJSONArray("playScoreScreen", "assets/scoreScreen/scoreScreenButtons.png", "assets/scoreScreen/scoreScreenButtons.json");


            for (var i: number = 1; i < 6; i++) {

                this.load.atlasJSONArray("fail" + i, "assets/feedbacks/fail" + i + ".png", "assets/feedbacks/fail" + i + ".json");



            }


            for (var i: number = 1; i < 5; i++) {

                this.load.atlasJSONArray("success" + i, "assets/feedbacks/success" + i + ".png", "assets/feedbacks/success" + i + ".json");



            }

            for (var i: number = 1; i < 5; i++) {
                this.load.audio("congrats" + i, "assets/feedbacks/congrats" + i + ".mp3");



            }

            this.load.audio("failure1", "assets/feedbacks/failure1.mp3");
            this.load.audio("failure2", "assets/feedbacks/failure2.mp3");


        }


        create() {
           
            this.game.state.start("MainMenu", true, false);

        }

    }


} 