Main.Welcome = function (game) {
    this.game = game;
};

Main.Welcome.prototype = {
    preload: function () {},
    create: function () {
        this.createBackground();
        this.showCountdown();
    },
    showCountdown: function () {
        countdown = this.game.add.sprite(320, 240, 'countdown');
        var anim = countdown.animations.add('start', 0, 30);
        anim.onComplete.add(this.launchTheGame, this);
        countdown.play('start');
    },
    createBackground: function () {
        //  A simple background for our game
        this.game.add.sprite(0, 0, 'space');
        this.game.add.sprite(200, 540, 'shuttle_back');
        shuttle_front = this.game.add.sprite(0, 450, 'shuttle_front');

    },
    launchTheGame: function () {
        this.game.state.start("thegame", Main.Thegame);
    }
};