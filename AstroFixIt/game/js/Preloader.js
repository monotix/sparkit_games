Main.Preloader = function (game) {
    this.game = game;
};

Main.Preloader.prototype = {
    loaderFull: Phaser.Sprite,
    loaderEmpty: Phaser.Sprite,
    test: Phaser.Sprite,
    preload: function () {
        this.game.add.sprite(0, 0, 'splash');

        this.loaderEmpty = this.add.sprite(0, 0, 'loaderEmpty');
		this.loaderEmpty.position.setTo(this.game.width/2 - this.loaderEmpty.width/2,485);
        //this.loaderEmpty.crop = new Phaser.Rectangle(0, 0, this.loaderEmpty.width, this.loaderEmpty.height);
        this.loaderEmpty.name = 'loaderEmpty';
        this.loaderFull = this.add.sprite(0, 0, 'loaderFull');
		this.loaderFull.position.setTo(this.game.width/2 - this.loaderEmpty.width/2,485);
        //this.loaderFull.crop = new Phaser.Rectangle(0, 0, 0, this.loaderFull.height);
        this.loaderFull.name = 'loaderFull';
		this.load.setPreloadSprite(this.loaderFull);
		
		
        this.game.load.atlas('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');
        this.game.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');

        this.game.load.atlas('countdown', 'assets/countdown.png', 'assets/countdown.json');

        // asteroidy
        this.game.load.atlas('asteroidSmall0', 'assets/asteroids/asteroidSmall0.png', 'assets/asteroids/asteroidSmall0.json');
        this.game.load.atlas('asteroidSmall1', 'assets/asteroids/asteroidSmall1.png', 'assets/asteroids/asteroidSmall1.json');
        this.game.load.atlas('asteroidSmall2', 'assets/asteroids/asteroidSmall2.png', 'assets/asteroids/asteroidSmall2.json');
        this.game.load.atlas('asteroidChunk0', 'assets/asteroids/asteroidChunk0.png', 'assets/asteroids/asteroidChunk0.json');
        this.game.load.atlas('asteroidChunk1', 'assets/asteroids/asteroidChunk1.png', 'assets/asteroids/asteroidChunk1.json');
        this.game.load.atlas('asteroidChunk2', 'assets/asteroids/asteroidChunk2.png', 'assets/asteroids/asteroidChunk2.json');
        this.game.load.atlas('asteroidChunk3', 'assets/asteroids/asteroidChunk3.png', 'assets/asteroids/asteroidChunk3.json');
        this.game.load.atlas('asteroidLarge', 'assets/asteroids/asteroidLarge.png', 'assets/asteroids/asteroidLarge.json');

        //satelity
        this.game.load.atlas('satelite0', 'assets/satellites/satelite0.png', 'assets/satellites/satelite0.json');
        this.game.load.atlas('satelite1', 'assets/satellites/satelite1.png', 'assets/satellites/satelite1.json');
        this.game.load.atlas('satelite2', 'assets/satellites/satelite2.png', 'assets/satellites/satelite2.json');

        //George Clooney
        this.game.load.atlas('jump', 'assets/astronauta/jump.png', 'assets/astronauta/jump.json');
        this.game.load.atlas('die', 'assets/astronauta/die.png', 'assets/astronauta/die.json');
        this.game.load.atlas('react', 'assets/astronauta/react.png', 'assets/astronauta/react.json');
        this.game.load.atlas('repair', 'assets/astronauta/repair.png', 'assets/astronauta/repair.json');
        this.game.load.atlas('spawn', 'assets/astronauta/spawn.png', 'assets/astronauta/spawn.json');
        this.game.load.atlas('walkLeftRight', 'assets/astronauta/walkLeftRight.png', 'assets/astronauta/walkLeftRight.json');
        this.game.load.atlas('health', 'assets/health.png', 'assets/health.json');
        this.game.load.image('armLeft', 'assets/astronauta/armLeft.png');
        this.game.load.image('armRight', 'assets/astronauta/armRight.png');

        //audio
        this.game.load.audio("Audiodie", "assets/audio/die.mp3");
        this.game.load.audio("Audiojump", "assets/audio/jump.mp3");
        this.game.load.audio("AudiofinishRepair", "assets/audio/finishRepair.mp3");
        this.game.load.audio("AudiolargeExplosion", "assets/audio/largeExplosion.mp3");
        this.game.load.audio("AudioplayerHit", "assets/audio/playerHit.mp3");
        this.game.load.audio("playerShoot", "assets/audio/playerShoot.mp3");
        this.game.load.audio("Audiorepair", "assets/audio/repair.mp3");
        this.game.load.audio("AudiosmallExplosion", "assets/audio/smallExplosion.mp3");

        //feedbacks
        for (var i = 1; i < 6; i++) {
            this.load.atlas("fail" + i, "assets/feedbacks/fail" + i + ".png", "assets/feedbacks/fail" + i + ".json");
        }
        for (var i = 1; i < 5; i++) {
            this.load.atlas("success" + i, "assets/feedbacks/success" + i + ".png", "assets/feedbacks/success" + i + ".json");
        }
        for (var i = 1; i < 5; i++) {
            this.load.audio("congrats" + i, "assets/feedbacks/congrats" + i + ".mp3");
        }
        this.load.audio("failure1", "assets/feedbacks/failure1.mp3");
        this.load.audio("failure2", "assets/feedbacks/failure2.mp3");

        this.load.audio("overButtonA", "assets/audio/overButton.wav");
        this.load.audio("clearButtonA", "assets/audio/clearButton.wav");
        this.load.atlas("scoreScreen", "assets/scoreScreen/scoreScreen.png", "assets/scoreScreen/scoreScreen.json");
        this.load.atlas("playScoreScreen", "assets/scoreScreen/scoreScreenButtons.png", "assets/scoreScreen/scoreScreenButtons.json");

        this.game.load.image('bullet', 'assets/bullet.png');
        this.game.load.image('instructionScreen', 'assets/instructionScreen.png');
        this.game.load.image('space', 'assets/background.png');
        this.game.load.image('shuttle_front', 'assets/shuttle_front.png');
        this.game.load.image('shuttle_back', 'assets/shuttle_back.png');
        this.game.load.image('target', 'assets/target.png');

        this.load.onFileComplete.add(this.fileLoaded, this);
    },
    create: function () {
        this.game.state.start('mainmenu', Main.MainMenu);
        //this.game.state.start('thegame', Main.Thegame);
    },
    fileLoaded: function (progress) {
        // console.log(this.loaderFull.crop.width);
        this.loaderEmpty.crop.left = (135 / 100) * progress;
        this.loaderFull.crop.width = (135 / 100) * progress;
    }
}