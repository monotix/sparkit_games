Main.Thegame = function (game) {
    this.game = game;

    this.fireRate = 100;
    this.nextFire = 0;
    this.score = 0;
    this.lives = 3;
    this.frame = 0;
    this.creditsAll = 0;
    this.currentLevel = 1;
    this.levelSettings = Main.levels["level" + this.currentLevel];
    this.baddiesCount = 0;
    this.satellitesToFix = 0;
    this.fixedSatellitesArray = new Array();

    //set flags 
    this.playerGoBackFlag = false;
    this.playerGoUpFlag = false;
    
    this.startX = 0;
    this.startY = 0;
    this.hitByRock = 0;
    this.moveTo = 0;
    this.maxAsteroids = 2 + Math.round(1.5 * this.currentLevel);
    this.randomRock = new Array(
        "asteroidSmall0",
        "asteroidSmall1",
        "asteroidSmall2"
    );
    var platforms, scoreText, livesText;
    this.blockedScreen = false;
    this.swipeMove = false;
};

Main.Thegame.prototype = {
    preload: function () {},
    render: function () {
        // this.game.debug.spriteInfo(dummy, 32, 32);
        //    this.game.debug.body(player);
    },
    create: function () {
        _self = this;
        this.zKey = this.game.input.keyboard.addKey(Phaser.Keyboard.Z);
        this.xKey = this.game.input.keyboard.addKey(Phaser.Keyboard.X);
        this.spaceKey = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

        this.timeDelay = 0;
        this.jumpDelay = 0;
        this.game.world.setBounds(0, 0, 800, 1070);
        this.createBackground();
        this.createSatellites();

        this.createBullets();
        this.createPlayer();

        this.createRocks();
        this.createTarget();
        this.createHealth();
        this.createAudio();
        this.createFeedback();

        this.game.input.onDown.add(this.beginSwipe, this);
    },

    beginSwipe: function () {
        this.startX = this.game.input.worldX;
        this.startY = this.game.input.worldY;
        this.game.input.onDown.remove(this.beginSwipe);
        this.game.input.onUp.add(this.endSwipe);
    },
    endSwipe: function () {
        endX = _self.game.input.worldX;
        endY = _self.game.input.worldY;
        // determining x and y distance travelled by mouse/finger from the start
        // of the swipe until the end
        var distX = _self.startX - endX;
        var distY = _self.startY - endY;
        // in order to have an horizontal swipe, we need that x distance is at least twice the y distance
        // and the amount of horizontal distance is at least 10 pixels
        if (Math.abs(distX) > Math.abs(distY) * 2 && Math.abs(distX) > 10) {

            //console.log(distX);
            // moving left, calling move function with horizontal and vertical tiles to move as arguments
            if (distX > 0) {
                _self.swipeMove = "left";
            } else {
                _self.swipeMove = "right";
            }
            _self.moveTo = Math.abs(distX);
        } 
        // in order to have a vertical swipe, we need that y distance is at least twice the x distance
        // and the amount of vertical distance is at least 10 pixels
        if (Math.abs(distY) > Math.abs(distX) * 2 && Math.abs(distY) > 10) {
            if (distY > 0) {
                _self.swipeMove = "up";
            } else {
                _self.swipeMove = "down";
            }
        }
        // stop listening for the player to release finger/mouse, let's start listening for the player to click/touch
        _self.game.input.onDown.add(_self.beginSwipe);
        _self.game.input.onUp.remove(_self.endSwipe);
    },

    update: function () {
        //console.log(this.swipeMove);
        if (!this.blockedScreen) {
            //  Reset the players velocity (movement)
            player.body.velocity.x = 0;
            if (player.y > 280) {
                if (this.zKey.isDown || this.swipeMove == "left" && this.moveTo > 0) {
                    //  Move to the left
                    player.body.velocity.x = -150;
                    player.animations.play('left');
                    this.moveTo--;
                } else if (this.xKey.isDown || (this.swipeMove == "right" && this.moveTo > 0)) {
                        //  Move to the right
                        player.body.velocity.x = 150;
                        player.animations.play('right');
                         this.moveTo--;
                    } else if ((this.spaceKey.isDown || this.swipeMove == "up") && this.playerGoUpFlag == false) {
                        this.playerGoUpFlag = true;
                        this.swipeMove = false;
                        this.playerJump();
                    } else {
                        player.animations.stop();
                        player.frame = 88;
                    }
            }

            target.position.x = this.game.input.activePointer.position.x - 12;
            target.position.y = this.game.input.activePointer.position.y - 12;

            if (this.game.input.activePointer.isDown && !dummy.input.checkPointerOver(this.game.input.activePointer)) {
                this.fire();
                this.game.input.activePointer.reset();
            }

            this.smallrocks.forEach(function (rock) {
                rock.angle += 0.5;
                if (rock.body.y > 600) {
                    if (this.game.time.now > this.timeDelay) {
                        rock.kill();
                        this.baddiesCount--;
                        this.timeDelay = this.game.time.now + 1000

                    }
                }

            }, this);
            this.bigrocks.forEach(function (rock) {
                rock.angle -= 0.1;
                if (rock.body.y > 600) {
                    if (this.game.time.now > this.timeDelay) {
                        rock.kill();
                        this.baddiesCount--;
                        this.timeDelay = this.game.time.now + 1000

                    }
                }
            }, this);
            this.bigrockchunks.forEach(function (item) {
                if (item.body.y > 600) {
                    item.kill();
                }
            }, this);

            //  this.scoreText.text = 'Asteroids: ' + this.baddiesCount;
            //  this.livesText.text = 'Level: ' + this.currentLevel;
            while (this.baddiesCount < this.maxAsteroids) {
                this.createRock();
                //console.log('new rock');
            }
            this.collisionCheck();
        }
    },

    createAudio: function () {
        this.Audiodie = this.game.add.audio("Audiodie", 1, false);
        this.AudiofinishRepair = this.game.add.audio("AudiofinishRepair", 1, false);
        this.Audiojump = this.game.add.audio("Audiojump", 1, false);
        this.AudiolargeExplosion = this.game.add.audio("AudiolargeExplosion", 1, false);
        this.AudioplayerShoot = this.game.add.audio("playerShoot", 1, false);
        this.AudioplayerHit = this.game.add.audio("AudioplayerHit", 1, false);
        this.AudiosmallExplosion = this.game.add.audio("AudiosmallExplosion", 1, false);
        this.Audiorepair = this.game.add.audio("Audiorepair", 1, false);

        this.congratsAudioArray = new Array();
        this.successAnimArray = new Array();
        for (var i = 1; i < 5; i++) {
            this.successAnimArray.push(new Phaser.Sprite(this.game, 0, 0, "success" + i, 0));
        }
        for (var i = 1; i < 5; i++) {
            this.congratsAudioArray.push(this.game.add.audio("congrats" + i, 1, false));
        }
        this.failArray = new Array();
        for (var i = 1; i < 6; i++) {
            this.failArray.push(new Phaser.Sprite(this.game, 0, 0, "fail" + i, 0));
        }
        /* audio */
        this._overButtonA = this.game.add.audio("overButtonA", 1, false);
        // this.overButtonA = this.game.add.audio("overButtonA", 1, false);
        this._clearButtonA = this.game.add.audio("clearButtonA", 1, false);
        /*end audio*/
    },


    createFeedback: function () {
        this.scoreScreenGr = this.add.group();
        this.scoreScreenSpr = this.game.add.sprite(0, 0, "scoreScreen", 0, this.scoreScreenGr);
        this.scoreScreenSpr.animations.add("score");
        this.scoreScreenSpr.animations.play("score", 48, true);
        var scorePlay = this.game.add.button(0, 0, "playScoreScreen", this.playAgain, this, 1, 0, 1, 0, this.scoreScreenGr);
        var quit = this.game.add.button(300, 10, "playScoreScreen", this.quit, this, 3, 2, 3, 2, this.scoreScreenGr);
        scorePlay.inputEnabled = true;
        scorePlay.input.priorityID = 3;
        quit.inputEnabled = true;
        quit.input.priorityID = 3;
        this.scoreScreenSpr.addChild(scorePlay);
        this.scoreScreenSpr.addChild(quit);
        scorePlay.x = 257;
        scorePlay.y = 358;
        quit.x = 427;
        quit.y = 358;
        this.scoreScreenGr.x = -this.scoreScreenGr.width;
        this.scoreScreenGr.y = -this.scoreScreenGr.height;
        this.scoreScreenGr.visible = false;
        //this.stage.removeChild(this.scoreScreenGr);
        var blokScreenSprite = new Phaser.Graphics(this.game, 0, 0);
        blokScreenSprite.beginFill(0x000000, 0);
        blokScreenSprite.drawRect(0, 0, 800, 600);
        blokScreenSprite.endFill();
        this.blockScreen = new Phaser.Sprite(this.game, 0, 0);
        this.blockScreen.addChild(blokScreenSprite);
        this.blockScreen.addChild(this.scoreScreenGr);
        //this.stage.addChild(this.blockScreen);
    },
    quit: function () {},
    playAgain: function () {
        console.log("playagain");
        //this.scoreScreenGr.kill();
        this.onBlockScreen(false);
        //this.scoreScreenSpr.inputEnabled = false;
        //  this.scoreScreenSpr.input.priorityID = 0;
        //this.scoreScreenGr.visible = false;
        this.stage.removeChild(this.scoreScreenGr);
        this.defaultScore();
        this.restart();
        this.restartNextLevel();
    },
    defaultScore: function () {
        this.fixedSatellitesArray = new Array();
        this.hitByRock = 0;
        satellite.animations.play('walk', 20, true);
    },

    youLose: function () {
        player.loadTexture('die', 20);
        player.animations.add('die');
        //player.animations.play('die', 20, true);
        this.Audiodie.play();

        this.onBlockScreen(true);
        this.fail = this.failArray[this.randomNum(this.failArray.length)];
        this.fail.x = this.game.width / 2 - this.fail.width / 2;
        this.fail.y = this.game.height / 2 - this.fail.height / 2;
        this.stage.addChild(this.fail);
        var anim = this.fail.animations.add("anim");
        anim.onComplete.add(function () {
            this.game.time.events.add(Phaser.Timer.SECOND * 2, this.animationStopped, this);
        }, this);
        this.fail.animations.play("anim", 24, false, false);
        //this.game.time.events.add(Phaser.Timer.SECOND * 6, this.animationStopped, this);
        //this.add.image(0, 0, "youLose");
    },
    youWin: function (finishLevel) {
        this.onBlockScreen(true);
        if (finishLevel === void 0) {
            finishLevel = false;
        }
        this.succesAnimatioScreen = this.successAnimArray[this.randomNum(this.successAnimArray.length)];
        this.succesAnimatioScreen.x = this.game.width / 2 - this.succesAnimatioScreen.width / 2;
        this.succesAnimatioScreen.y = this.game.height / 2 - this.succesAnimatioScreen.height / 2;
        this.stage.addChild(this.succesAnimatioScreen);
        var anim = this.succesAnimatioScreen.animations.add("animAgain");
        if (finishLevel) {
            console.log('tu');
            anim.onComplete.add(function () {
                this.game.time.events.add(Phaser.Timer.SECOND * 2, this.restartNextLevel, this);
            }, this);
        } else {
            console.log('tam');
            anim.onComplete.add(function () {
                this.game.time.events.add(Phaser.Timer.SECOND * 2, this.playAgain, this);
            }, this);
        }
        this.succesAnimatioScreen.animations.play("animAgain", 24, false, false);
        this.congratsAudioArray[this.randomNum(this.congratsAudioArray.length)].play();
        //  this.congratsAudioArray[this._gameControler.randomNum(this.congratsAudioArray.length)].onStop.add(this.finishAudioBadAnswer, this);
    },
    restart: function () {
        this.fixedSatellitesArray = new Array();
        this.baddiesCount = 0;
        this.hitByRock = 0;
        this.create();
    },
    restartNextLevel: function () {
        console.log("restartNextLevel");
        this.succesAnimatioScreen.kill();
        this.onBlockScreen(false);
        this.currentLevel++;
        this.levelSettings = Main.levels["level" + this.currentLevel];

        this.restart();
    },
    animationStopped: function () {
        this.stage.removeChild(this.fail);
        this.scoreScreenGr.x = 100;
        this.scoreScreenGr.y = 100;
        this.scoreScreenSpr.inputEnabled = true;
        this.scoreScreenSpr.input.priorityID = 2;
        this.scoreScreenGr.visible = true;
        this.stage.addChild(this.scoreScreenGr);
    },

    createHealth: function () {
        health = this.game.add.sprite(652, 512, 'health');
        health.animations.add('firstLife', Main.frameRange(1, 9));
        health.animations.add('secondLife', Main.frameRange(11, 20));
        health.animations.add('thirdLife', Main.frameRange(21, 30));
        health.animations.add('fourthLife', Main.frameRange(31, 40));
        health.animations.add('fifthLife', Main.frameRange(41, 50));
        health.animations.add('death', Main.frameRange(51, 85));
        health.animations.play('firstLife', 20, true);
        health.bringToTop();
    },

    createBullets: function () {
        bullets = this.game.add.group();
        bullets.enableBody = true;
        bullets.immovable = true;
        bullets.physicsBodyType = Phaser.Physics.ARCADE;
        bullets.createMultiple(50, 'bullet');
        bullets.setAll('checkWorldBounds', true);
        bullets.setAll('outOfBoundsKill', true);
    },

    createBackground: function () {
        //  A simple background for our game
        this.game.add.sprite(0, 0, 'space');
        this.game.add.sprite(200, 540, 'shuttle_back');
        shuttle_front = this.game.add.sprite(0, 450, 'shuttle_front');

    },

    createTarget: function () {
        target = this.game.add.sprite(32, this.game.world.height - 150, 'target');
    },

    createSatellites: function () {
        this.satellites = this.game.add.group();
        this.satellites.enableBody = true;
        this.satellites.immovable = true;
        self = this;
        levelSatellitesArray = this.levelSettings.satellites;
        levelSatellitesArray.forEach(function (entry) {
            self.createSatellite(entry[0], entry[1], entry[2]);

        });
        this.satellitesToFix = levelSatellitesArray.length;
    },
    createSatellite: function (x, y, item) {
        satellite = this.satellites.create(x, y, item);
        satellite.body.setSize(130, 40, 0, 0);
        satellite.animations.add('walk', Main.frameRange(0, 39));
        satellite.animations.play('walk', 20, true);
    },

    createRocks: function () {
        this.smallrocks = this.game.add.group();
        this.smallrocks.enableBody = true;
        this.smallrocks.setAll('checkWorldBounds', true);
        this.smallrocks.setAll('outOfBoundsKill', true);

        this.bigrocks = this.game.add.group();
        this.bigrocks.enableBody = true;
        this.bigrocks.setAll('checkWorldBounds', true);
        this.bigrocks.setAll('outOfBoundsKill', true);

        this.bigrockchunks = this.game.add.group();
        this.bigrockchunks.enableBody = true;
        this.bigrockchunks.setAll('checkWorldBounds', true);
        this.bigrockchunks.setAll('outOfBoundsKill', true);
    },

    createRock: function () {
        var leftRight = Math.random() < 0.5 ? "left" : "right";
        var bigSmall = Math.random() < 0.5 ? "big" : "small";

        if (leftRight == "left") {
            var xpos = -1 * this.maxAsteroids * Math.random() * 50;
            var rockVelocity = Math.random() * 130;
        } else {
            var xpos = 800 + this.maxAsteroids * Math.random() * 50;
            var rockVelocity = -1 * Math.random() * 120;
        }

        if (bigSmall == "big") {
            var item = "asteroidLarge";
            rock = this.bigrocks.create(xpos, this.maxAsteroids * Math.random() * 100, item);
            rock.body.width = 60;
            rock.body.height = 60;
        } else {
            var item = this.randomRock[Math.floor(Math.random() * this.randomRock.length)]
            rock = this.smallrocks.create(xpos, this.maxAsteroids * Math.random() * 100, item);
            rock.body.width = 40;
            rock.body.height = 40;
        }
        rock.animations.add('walk', Main.frameRange(0, 10));
        rock.animations.play('walk', 20, true);
        this.game.physics.arcade.enable(rock);
        rock.anchor.setTo(0.5, 0.5);
        rock.body.bounce.y = 0.5;
        rock.body.bounce.x = 0.5;
        rock.body.gravity.y = 0;
        rock.body.velocity.x = rockVelocity;
        this.baddiesCount++;
    },

    createPlayer: function () {
        player = this.game.add.sprite(this.game.world.width / 2 - 389 / 2, this.game.world.height - 50, 'walkLeftRight', 1);
        this.game.physics.arcade.enable(player);
        player.enableBody = true;
        player.inputEnabled = true;
        //player.input.pixelPerfectOver = true;
        player.body.setSize(57, 651, 190, 130);
        //Player.body.bounce.y = 0;
        //player.body.gravity.y = 100;
        player.body.collideWorldBounds = true;
        player.animations.add('left', Main.frameRange(1, 22), 30, true);
        player.animations.add('right', Main.frameRange(23, 44), 30, true);
        //player.animations.add('spawn');
        //player.animations.add('react');
        player.animations.add('die');

        armleft = this.game.add.sprite(185, 180, 'armLeft');
        armright = this.game.add.sprite(250, 180, 'armRight');
        dummy = this.game.add.sprite(150, 130);
        
        dummy.inputEnabled = true;
        player.addChild(armleft);
        player.addChild(armright);
        player.addChild(dummy);
        dummy.width = 150;
        dummy.height = 651;
        armright.anchor.setTo(0.5, 0.1);
        armleft.anchor.setTo(0.5, 0.1);

        player.bringToTop();
        shuttle_front.bringToTop();

    },

    nextLevel: function () {
        console.log("next level");
        this.onBlockScreen(true);
        var losowanieAnim = this.randomNum(this.successAnimArray.length);
        this.succesAnimatioScreen = this.successAnimArray[losowanieAnim];
        this.succesAnimatioScreen.x = this.game.width / 2 - this.succesAnimatioScreen.width / 2;
        this.succesAnimatioScreen.y = this.game.height / 2 - this.succesAnimatioScreen.height / 2;
        this.stage.addChild(this.succesAnimatioScreen);
        var anim = this.succesAnimatioScreen.animations.add("animAgain");
        anim.onComplete.add(function () {
            this.game.time.events.add(Phaser.Timer.SECOND * 2, this.restartNextLevel, this);
        }, this);
        this.succesAnimatioScreen.animations.play("animAgain", 24, false, false);
        //  this.congratsAudioArray[this._gameControler.randomNum(this.congratsAudioArray.length)].onStop.add(this.finishAudioBadAnswer, this);
        var jakaLiczba = this.randomNum(this.congratsAudioArray.length);
        console.log("jaka liczba dla audio gratulacji", jakaLiczba);
        this.congratsAudioArray[jakaLiczba].play();
    },
    fixingSatellite: function (player, satellite) {
        if (this.fixedSatellitesArray.indexOf(this.satellites.getIndex(satellite)) == -1) {
            player.body.velocity.y = 0;
            player.animations.stop();
            player.body.moves = false;
            player.y = player.y + 5;
            satellite.animations.add('hit', Main.frameRange(40, 80));
            satellite.animations.play('hit', 20, false, false);
            satellite.body.enable = false;
            player.loadTexture('repair', 6);
            player.animations.add('repair');
            player.animations.play('repair', 20, false, false);
            this.Audiorepair.play();
            this.fixedSatellitesArray.push(this.satellites.getIndex(satellite));
            player.events.onAnimationComplete.add(function () {
                this.AudiofinishRepair.play();
                this.playerGoBack();
                if (this.fixedSatellitesArray.length == this.satellitesToFix) {
                    if (this.game.time.now > this.timeDelay) {
                        this.youWin(1);
                        this.timeDelay = this.game.time.now + 1000;
                    }
                }
            }, this);
        } else {
            this.playerGoBack();
        }
    },
    playerGoBack: function () {
        if(this.playerGoUpFlag == true) {
        console.log('godown');
        player.body.angle = 180;
        player.body.velocity.y = 100;
        player.body.moves = true;
        this.playerGoUpFlag = false;
        player.loadTexture('walkLeftRight', 1);
        }
    },
    playerJump: function () {
        if (this.game.time.now > this.jumpDelay) {
        this.playerGoUpFlag = true;
            console.log('goup');
            console.log('goup');
            player.body.velocity.y = -150;
            player.loadTexture('jump', 1);
            player.animations.add('jump', Main.frameRange(10, 17));
            this.Audiojump.play();
            player.animations.play('jump', 30, false, false);
            this.jumpDelay = this.game.time.now + 1000;
        }
    },
    collisionCheck: function () {
        if (!this.blockedScreen) {
            this.game.physics.arcade.collide(this.smallrocks, this.bigrockchunks);
            this.game.physics.arcade.overlap(bullets, this.smallrocks, this.hitSmallRock, null, this);
            this.game.physics.arcade.overlap(bullets, this.bigrocks, this.hitBigRock, null, this);
            this.game.physics.arcade.overlap(bullets, this.bigrockchunks, this.hitBigRockChunk, null, this);
            this.game.physics.arcade.overlap(player, this.satellites, this.fixingSatellite, null, this);
            this.game.physics.arcade.collide(player, this.smallrocks, this.crashWithRock, null, this);
            this.game.physics.arcade.collide(player, this.bigrocks, this.crashWithRock, null, this);
            this.game.physics.arcade.collide(player, this.bigrockchunks, this.crashWithRock, null, this);
//console.log(this.playerGoBackFlag);
            // reset go down flag
            if (player.y >= -120) {
                this.playerGoBackFlag = false;
            }

            if (player.y < -120 && this.playerGoBackFlag == false) {
                this.playerGoBackFlag = true;
                console.log('s');
                player.animations.stop();
                player.body.moves = false;
                this.playerGoBack();
            }
        }
    },

    randomNum: function (max, min) {
        if (min === void 0) {
            min = 0;
        }
        return Math.floor(Math.random() * (max - min) + min);
    },

    killBullet: function (bullet, platform) {
        bullet.kill();
    },

    hitSmallRock: function (bullet, rock) {
        bullet.kill();
        if (this.game.time.now > this.timeDelay) {
            rock.body.enable = false;
            rock.animations.add('hit', Main.frameRange(11, 33));
            this.AudiosmallExplosion.play();

            rock.animations.play('hit', 20, false, true);

            if (this.baddiesCount > 0) {
                this.baddiesCount--;
            }
            this.timeDelay = this.game.time.now + 1000
        }
    },

    hitBigRockChunk: function (bullet, rock) {
        bullet.kill();
        if (this.game.time.now > this.timeDelay) {
            this.AudiosmallExplosion.play();
            rock.body.enable = false;
            rock.animations.add('hit', Main.frameRange(11, 33));
            rock.animations.play('hit', 20, false, true);
            this.timeDelay = this.game.time.now + 1000
        }
    },

    hitBigRock: function (bullet, rock) {
        bullet.kill();

        if (this.game.time.now > this.timeDelay) {
            rock.body.enable = false;
            rock.animations.add('hit', Main.frameRange(11, 33));
            rock.animations.play('hit', 20, false, true);
            this.AudiolargeExplosion.play();
            this.makeChunk("0", rock.x, rock.y);
            this.makeChunk("1", rock.x, rock.y);
            this.makeChunk("2", rock.x, rock.y);
            this.makeChunk("3", rock.x, rock.y);
            if (this.baddiesCount > 0) {
                this.baddiesCount--;
            }
            this.timeDelay = this.game.time.now + 1000
        }
    },

    crashWithRock: function (player, rock) {
        //   player.kill();
        if (this.game.time.now > this.timeDelay) {
            rock.body.enable = false;
            rock.animations.add('hit', Main.frameRange(44, 59));
            rock.animations.play('hit', 20, false, true);
            this.AudioplayerHit.play();

            if (this.game.time.now > this.timeDelay) {
                this.hitByRock++;
                switch (this.hitByRock) {
                case 0:
                    health.animations.play('firstLife', 20, true);
                    break;
                case 1:
                    health.animations.play('secondLife', 20, true);
                    break;
                case 2:
                    health.animations.play('thirdLife', 20, true);
                    break;
                case 3:
                    health.animations.play('fourthLife', 20, true);
                    break;
                case 4:
                    health.animations.play('fifthLife', 20, true);
                    break;
                case 5:
                    health.animations.play('death', 20, true);
                    this.youLose();
                    break;
                }
                this.timeDelay = this.game.time.now + 1000
            }
        }
    },
    makeChunk: function (index, x, y) {
        switch (index) {
        case "0":
            velx = 0;
            y = y - 30;
            vely = -50;
            break;
        case "1":
            velx = -50;
            x = x - 30;
            vely = 0;
            break;
        case "2":
            velx = 50;
            x = x + 30;
            vely = 0;
            break;
        case "3":
            velx = 0;
            vely = 50;
            y = y + 30;
            break;
        }
        var item = "asteroidChunk" + index;
        var rockVelocity = -1 * Math.random() * 100;
        rock = this.bigrockchunks.create(x, y, item);
        rock.body.width = 30;
        rock.body.height = 30;
        rock.animations.add('walk', Main.frameRange(0, 10));
        rock.animations.play('walk', 20, true);
        this.game.physics.arcade.enable(rock);
        rock.anchor.setTo(0.5, 0.5);
        rock.body.bounce.y = 0.5;
        rock.body.bounce.x = 0.5;
        rock.body.gravity.y = 0;
        rock.body.velocity.x = velx;
        rock.body.velocity.y = vely;
    },
    fire: function () {
        if (!this.blockedScreen) {
            if (this.game.time.now > this.nextFire && bullets.countDead() > 0) {
                this.nextFire = this.game.time.now + this.fireRate;
                var bullet = bullets.getFirstDead();
                bullet.reset(player.x + 20 + 389 / 2, player.y + 200);
                bullet.rotation = this.game.physics.arcade.angleToPointer(bullet);
                this.game.physics.arcade.moveToPointer(bullet, 300);
                this.AudioplayerShoot.play();
                /*
				tutaj troche pozmienialem katy aby w miare wizualnie pociski lecialy z pistoleta
				*/
                if (this.game.input.activePointer.position.x < player.x + player.width / 2) {      
                    armleft.rotation = this.game.physics.arcade.angleToPointer(bullet) + 80;
                    armright.rotation = 0;
                    bullet.position.setTo(bullet.position.x - 40, bullet.position.y - 20);
                } else {      
                    armright.rotation = (this.game.physics.arcade.angleToPointer(bullet) - 100) + 180;
                    armleft.rotation = 0;
                    bullet.position.setTo(bullet.position.x + 40, bullet.position.y - 25);
                }
            }
        }
    },
    onBlockScreen: function (show) {
        if (show) {
            console.log('blok');
            this.blockedScreen = true;
            this.blockScreen.inputEnabled = true;
            this.blockScreen.input.priorityID = 1;
            this.stage.addChild(this.blockScreen);
        } else {
            this.blockedScreen = false;
            this.blockScreen.inputEnabled = false;
            this.blockScreen.input.priorityID = 0;
            this.blockScreen.kill();
        }
    }
}