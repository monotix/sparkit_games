var Main = {};


Main.frameRange = function (posstart, posend) {
    hitFrames = [];
    for (a = posstart; a < posend + 1; a++) {
        hitFrames.push(a);
    }
    return hitFrames;
}

Main.levels = {
    "level1": {
        "satellites": [
                [180, 11, "satelite0"],
                [486, 55, "satelite0"]
            ]
    },
    "level2": {
        "satellites": [
                [117, 206, "satelite1"],
                [550, 190, "satelite0"]
            ]
    },
    "level3": {
        "satellites": [
                [121, 126, "satelite0"],
                [315, 33, "satelite1"],
                [550, 126, "satelite0"]
            ]
    },
	"level4": {
        "satellites": [
                [333, 18, "satelite1"],
                [207, 174, "satelite0"],
                [334, 230, "satelite0"],
				[486, 145, "satelite0"]
            ]
    },
	"level5": {
        "satellites": [
                [121, 126, "satelite0"],
                [315, 33, "satelite1"],
                [550, 126, "satelite0"]
            ]
    },
	"level6": {
        "satellites": [
                [276, 48, "satelite0"],
                [146, 122, "satelite0"],
                [336, 229, "satelite0"],
				[521, 59, "satelite2"]
            ]
    }
};

Main.Boot = function (game) {
    this.game = game;

};

Main.Boot.prototype = {

    preload: function () {
        this.game.load.image('splash', 'assets/splashScreen.png');
        this.load.image('loaderFull', 'assets/ProgressBarBlue.png');
        this.load.image('loaderEmpty', 'assets/ProgressBar.png');
    },

    create: function () {
        // this.game.stage.enableOrientationCheck(true, false, 'orientation');
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.state.start('preloader', Main.Preloader);
    },


}