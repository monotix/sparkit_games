﻿module Litery {

    export class Preloader extends Phaser.State {


        preload() {
          
            this.load.image("szare", "letters/szare.png");
            this.load.image("orange", "letters/poma.png");
            this.load.image("green", "letters/ziel.png");
            this.load.image("bg", "letters/bg.png");
            this.load.image("bgDrop", "letters/bgDrop.png");
            this.load.atlasJSONArray('doneBtn', 'letters/done.png', 'letters/done.json');
            this.load.atlasJSONArray('clearBtn', 'letters/clearbtn.png', 'letters/clearbtn.json');
            
          

        }


        create() {
           
            this.game.state.start("MainMenu", true, false);

        }

    }


} 