var Greeter = (function () {
    function Greeter(element) {
        this.element = element;
        this.element.innerHTML += "The time is: ";
        this.span = document.createElement('span');
        this.element.appendChild(this.span);
        this.span.innerText = new Date().toUTCString();
    }
    Greeter.prototype.start = function () {
        var _this = this;
        this.timerToken = setInterval(function () { return _this.span.innerHTML = new Date().toUTCString(); }, 500);
    };
    Greeter.prototype.stop = function () {
        clearTimeout(this.timerToken);
    };
    return Greeter;
})();
window.onload = function () {
    var el = document.getElementById('content');
    var greeter = new Greeter(el);
    greeter.start();
};
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Litery;
(function (Litery) {
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            _super.apply(this, arguments);
        }
        Boot.prototype.preload = function () {
        };
        Boot.prototype.create = function () {
            this.game.state.start("Preloader", true, false);
        };
        return Boot;
    })(Phaser.State);
    Litery.Boot = Boot;
})(Litery || (Litery = {}));
var Litery;
(function (Litery) {
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            console.log("start");
            _super.call(this, 800, 600, Phaser.AUTO, "content", null, true);
            this.state.add("Boot", Litery.Boot, false);
            this.state.add("Preloader", Litery.Preloader, false);
            this.state.add("MainMenu", Litery.MainMenu, false);
            this.state.start("Boot");
        }
        return Game;
    })(Phaser.Game);
    Litery.Game = Game;
})(Litery || (Litery = {}));
window.onload = function () {
    var game = new Litery.Game;
};
var Litery;
(function (Litery) {
    var DropLetter = (function (_super) {
        __extends(DropLetter, _super);
        function DropLetter(game, parent) {
            _super.call(this, game, parent, "drop", true, true);
            this._parent = parent;
            this.bgDrop = new Phaser.Sprite(game, 0, 0, "bgDrop");
            this.addChild(this.bgDrop);
            var let1 = new Litery.PlaceToDrop(game);
            this.add(let1);
            var let2 = new Litery.PlaceToDrop(game);
            let2.x = let1.x + let1.width + 15;
            this.add(let2);
            var let3 = new Litery.PlaceToDrop(game);
            let3.x = let2.x + let2.width + 15;
            this.add(let3);
            var let4 = new Litery.PlaceToDrop(game);
            let4.x = let3.x + let3.width + 15;
            this.add(let4);
            this._arr = new Array(let1, let2, let3, let4);
        }
        DropLetter.prototype.clear = function () {
            for (var i = 0; i < this._arr.length; i++) {
                this._arr[i].clear();
            }
        };
        DropLetter.prototype.checkLetter = function () {
            var licz = 0;
            for (var i = 0; i < this._arr.length; i++) {
                if (this._arr[i].isAdded) {
                    console.log(this._arr[i]._letter.letterString());
                    if (this._arr[i]._letter.letterString() == this._parent._arrAnswear[i]) {
                        licz++;
                    }
                    else {
                    }
                }
            }
            if (licz == 4) {
                console.log("you win");
            }
            else {
                console.log("you lose");
            }
        };
        return DropLetter;
    })(Phaser.Group);
    Litery.DropLetter = DropLetter;
})(Litery || (Litery = {}));
var Litery;
(function (Litery) {
    var Letter = (function (_super) {
        __extends(Letter, _super);
        function Letter(game, letter, bgColor) {
            if (bgColor === void 0) { bgColor = 1; }
            _super.call(this, game, 0, 0, "");
            this.name = letter;
            var bg;
            if (bgColor == 1) {
                bg = new Phaser.Sprite(this.game, 0, 0, "szare");
            }
            else if (bgColor == 2) {
                bg = new Phaser.Sprite(this.game, 0, 0, "green");
            }
            else if (bgColor == 3) {
                bg = new Phaser.Sprite(this.game, 0, 0, "orange");
            }
            this._letter = new Phaser.Sprite(game, 0, 0, "");
            this._letter.addChild(bg);
            this.addChild(this._letter);
            var color = "#00FFFF";
            var style = { font: "40px Arial", fill: color, align: "left" };
            console.log("log", letter);
            var _letterShow = new Phaser.Text(this.game, 0, 0, letter, style);
            //    _letterShow.x = this._letter.width / 2 - _letterShow.width / 2;
            //    _letterShow.y = this._letter.height / 2 - _letterShow.height / 2;
            _letterShow.x = bg.width / 2 - _letterShow.width / 2;
            _letterShow.y = bg.height / 2 - _letterShow.height / 2;
            this._letter.addChild(_letterShow);
        }
        return Letter;
    })(Phaser.Sprite);
    Litery.Letter = Letter;
})(Litery || (Litery = {}));
var Litery;
(function (Litery) {
    var LetterWord = (function (_super) {
        __extends(LetterWord, _super);
        function LetterWord(game, letter) {
            _super.call(this, game, 0, 0, "");
            this.isAdded = false;
            this.name = letter;
            var bg = new Phaser.Sprite(this.game, 0, 0, "szare");
            this._letter = new Phaser.Sprite(game, 0, 0, "");
            this._letter.addChild(bg);
            this.addChild(this._letter);
            var color = "#00FFFF";
            var style = { font: "40px Arial", fill: color, align: "left" };
            console.log("log", letter);
            var _letterShow = new Phaser.Text(this.game, 0, 0, letter, style);
            //    _letterShow.x = this._letter.width / 2 - _letterShow.width / 2;
            //    _letterShow.y = this._letter.height / 2 - _letterShow.height / 2;
            this._letterStr = letter;
            this._letter.addChild(_letterShow);
            this.inputEnabled = true;
            //  this.input.enableDrag();
        }
        LetterWord.prototype.addObjectToDrag = function () {
            var spr = new LetterWord(this.game, this._letterStr);
            spr.anchor.setTo(0.5, 0.5);
            return spr;
        };
        LetterWord.prototype.letterString = function () {
            return this._letterStr;
        };
        return LetterWord;
    })(Phaser.Sprite);
    Litery.LetterWord = LetterWord;
})(Litery || (Litery = {}));
var Litery;
(function (Litery) {
    var Letters = (function (_super) {
        __extends(Letters, _super);
        function Letters(game, x, y, key) {
            _super.call(this, game, 0, 0, "");
            this.alphabet = new Array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
            this.words = [
                { a: new Array({ a: "s<a>t" }) },
                { b: [{ b: "<b>ust" }, { bb: "bu<bb>le" }] },
                { c: [{ c: "<c>at" }, { k: "<k>ink" }, { ck: "lu<ck>" }, { ch: "<ch>rome" }] },
                { d: [{ d: "<d>id" }, { dd: "la<dd>er" }, { ed: "wast<ed>" }] },
                { e: [{ e: "t<e>n" }, { ea: "h<ea>d" }, { ai: "s<ai>d" }, { ie: "fr<ie>nd" }] }
            ];
            this._arrAnswear = new Array("a", "b", "c", "d");
            //this.game.add.sprite(0, 0, "bg");
            var bg = new Phaser.Sprite(this.game, 0, 0, "bg");
            this.addChild(bg);
            //      this._dropLetter = new DropLetter(this.game);
            //    this.addChild(this._dropLetter);
            this._objectsToDropGr = new Litery.DropLetter(this.game, this);
            this._objectsToDropGr.y = 0;
            this._objectsToDropGr.enableBody = true;
            this.addChild(this._objectsToDropGr);
            this._wordsGr = this.game.add.group();
            this._wordsGr.y = 100;
            this._wordsGr.x = 500;
            this.addChild(this._wordsGr);
            this.makeLetter();
            this.game.input.onUp.add(this.stageUp, this);
            var gameButton = new Phaser.Button(this.game, 0, 0, "clearBtn", this.onGameClear, this, 1, 0);
            gameButton.x = 300;
            gameButton.y = 500;
            this.addChild(gameButton);
            var gameButton = new Phaser.Button(this.game, 0, 0, "doneBtn", this.onDoneClick, this, 1, 0);
            gameButton.x = 500;
            gameButton.y = 500;
            this.addChild(gameButton);
        }
        Letters.prototype.onDoneClick = function () {
            this._objectsToDropGr.checkLetter();
        };
        Letters.prototype.onGameClear = function () {
            this._objectsToDropGr.clear();
        };
        Letters.prototype.makeLetter = function () {
            var stalaY = 100;
            var posx = 0;
            var posy = 0;
            var licz = 1;
            for (var i = 0; i < this.alphabet.length; i++) {
                var letter = new Litery.Letter(this.game, this.alphabet[i], 1);
                letter.inputEnabled = true;
                letter.input.useHandCursor = true;
                letter.events.onInputDown.add(this.letterOnDown, this);
                letter.x = posx;
                letter.y = posy + stalaY;
                this.addChild(letter);
                posx += 50 + 10;
                console.log(posx);
                if (licz % 5 == 0 && licz != 0) {
                    posy += 60;
                    posx = 0;
                }
                licz++;
            }
            console.log("ile", i);
            var letter1 = new Litery.PlaceToDrop(this.game);
            //  letter.inputEnabled = true;
            //   this._objectsToDropGr.add(letter);
            //var letter2: Letter = new Letter(this.game, "");
            //  letter2.x = 100;
            //    this._objectsToDropGr.add(letter2);
        };
        Letters.prototype.letterOnDown = function (letter) {
            for (var i = 0; i < this.words.length; i++) {
                for (var name in this.words[i]) {
                    if (name == letter.name) {
                        this.drawWords(this.words[i][name]);
                    }
                }
            }
        };
        Letters.prototype.drawWords = function (arr) {
            this._wordsGr.forEach(function (item) {
                item.kill();
            }, this);
            var posX = 0;
            var posY = 0;
            for (var i = 0; i < arr.length; i++) {
                console.log("gra", arr[i]);
                for (var name in arr[i]) {
                    console.log("nazwa ? ", name);
                    console.log(arr[i][name]);
                    var letter = new Litery.LetterWord(this.game, name);
                    letter.events.onInputDown.add(this.check, this);
                    letter.y = posY;
                    this._wordsGr.add(letter);
                    var word = new Litery.Word(this.game, arr[i][name]);
                    word.x = letter.width + 30;
                    word.y = posY;
                    posY += word.height + 30;
                    this._wordsGr.add(word);
                }
            }
        };
        Letters.prototype.check = function (item) {
            var spr = item.addObjectToDrag();
            spr.anchor.set(0.5, 0.5);
            this.stage.addChild(spr);
            this._objectToDrag = spr;
            this.game.physics.enable(spr, Phaser.Physics.ARCADE);
        };
        Letters.prototype.checkSecondTime = function (item) {
            item.isAdded = false;
            item.placeToDrop.isAdded = false;
            var spr = item;
            this.stage.addChild(spr);
            this._objectToDrag = spr;
            this.game.physics.enable(spr, Phaser.Physics.ARCADE);
        };
        Letters.prototype.stageUp = function () {
            this.game.physics.arcade.overlap(this._objectToDrag, this._objectsToDropGr, this.collision, null, this);
            // if (this._objectToDrag != null) this.checkIfNotAdded(this._objectToDrag);
            this.checkIfNotAdded(this._objectToDrag);
        };
        Letters.prototype.checkIfNotAdded = function (object) {
            if (object === void 0) { object = null; }
            if (object != null && !object.isAdded) {
                console.log("object niedodany ");
                object.kill();
            }
            else {
                console.log("object dodany ");
            }
        };
        Letters.prototype.collision = function (object, object2) {
            console.log("kolizja");
            // this._objectsToDropGr.add(object);
            //object.x = object2.x;
            // object.y = object2.y;
            if (!object2.isAdded) {
                object2.addLetter(object);
                //   object.x = 0;
                //  object.y = 0;
                object.events.onInputDown.add(this.checkSecondTime, this);
                //  object2.addChild(object);
                object.isAdded = true;
            }
            else {
                object.kill();
            }
            this._objectToDrag = null;
        };
        Letters.prototype.update = function () {
            if (this._objectToDrag != null) {
                // var pos = this.stage.position.x
                this._objectToDrag.position.setTo(this.game.input.x, this.game.input.y);
            }
        };
        return Letters;
    })(Phaser.Sprite);
    Litery.Letters = Letters;
})(Litery || (Litery = {}));
var Litery;
(function (Litery) {
    var PlaceToDrop = (function (_super) {
        __extends(PlaceToDrop, _super);
        function PlaceToDrop(game) {
            _super.call(this, game, 0, 0, "");
            this.isAdded = false;
            this._bg = new Phaser.Sprite(game, 0, 0, "szare");
            this.addChild(this._bg);
        }
        PlaceToDrop.prototype.addLetter = function (letter) {
            if (!this.isAdded) {
                this._letter = letter;
                this._letter.x = 0;
                this._letter.y = 0;
                this.addChild(this._letter);
                this._letter.placeToDrop = this;
                this.isAdded = true;
            }
        };
        PlaceToDrop.prototype.clear = function () {
            if (this._letter != null) {
                this.removeChild(this._letter);
                this.isAdded = false;
                this._letter.isAdded = false;
                this._letter.kill();
            }
        };
        return PlaceToDrop;
    })(Phaser.Sprite);
    Litery.PlaceToDrop = PlaceToDrop;
})(Litery || (Litery = {}));
var Litery;
(function (Litery) {
    var Word = (function (_super) {
        __extends(Word, _super);
        function Word(game, letter) {
            _super.call(this, game, 0, 0, "");
            this.name = letter;
            this._letter = new Phaser.Sprite(game, 0, 0, "");
            // this._letter.addChild(gra);
            this.addChild(this._letter);
            var first = letter.indexOf("<");
            var last;
            var firstText;
            var lastText;
            var middle;
            console.log("index", first);
            if (first != -1) {
                last = letter.indexOf(">");
                firstText = letter.slice(0, first);
                lastText = letter.slice(last + 1, letter.length);
                middle = letter.slice(first + 1, last);
                var color2 = "#FF0000";
                var style2 = { font: "40px Arial", fill: color2, align: "left" };
                var color = "#000000";
                var style = { font: "40px Arial", fill: color, align: "left" };
                var _letterShow = new Phaser.Text(this.game, 0, 0, firstText, style);
                var _letterMiddle = new Phaser.Text(this.game, 0, 0, middle, style2);
                var _letterEnd = new Phaser.Text(this.game, 0, 0, lastText, style);
                this._letter.addChild(_letterShow);
                _letterMiddle.x = _letterShow.x + _letterShow.width;
                _letterEnd.x = _letterMiddle.x + _letterMiddle.width;
                this._letter.addChild(_letterMiddle);
                this._letter.addChild(_letterEnd);
                //    _letterShow.x = this._letter.width / 2 - _letterShow.width / 2;
                //    _letterShow.y = this._letter.height / 2 - _letterShow.height / 2;
                //  this._letter.addChild(_letterShow);
                console.log("wchodze tu");
            }
        }
        return Word;
    })(Phaser.Sprite);
    Litery.Word = Word;
})(Litery || (Litery = {}));
var Litery;
(function (Litery) {
    var MainMenu = (function (_super) {
        __extends(MainMenu, _super);
        function MainMenu() {
            _super.apply(this, arguments);
        }
        MainMenu.prototype.create = function () {
            console.log("witamy w dupie");
            var letters = new Litery.Letters(this.game, 0, 0, "");
            this.stage.addChild(letters);
        };
        MainMenu.prototype.overPlay2 = function () {
        };
        MainMenu.prototype.onPlayClick = function () {
            this.game.state.start("Instruction", true, false);
        };
        return MainMenu;
    })(Phaser.State);
    Litery.MainMenu = MainMenu;
})(Litery || (Litery = {}));
var Litery;
(function (Litery) {
    var Preloader = (function (_super) {
        __extends(Preloader, _super);
        function Preloader() {
            _super.apply(this, arguments);
        }
        Preloader.prototype.preload = function () {
            this.load.image("szare", "letters/szare.png");
            this.load.image("orange", "letters/poma.png");
            this.load.image("green", "letters/ziel.png");
            this.load.image("bg", "letters/bg.png");
            this.load.image("bgDrop", "letters/bgDrop.png");
            this.load.atlasJSONArray('doneBtn', 'letters/done.png', 'letters/done.json');
            this.load.atlasJSONArray('clearBtn', 'letters/clearbtn.png', 'letters/clearbtn.json');
        };
        Preloader.prototype.create = function () {
            this.game.state.start("MainMenu", true, false);
        };
        return Preloader;
    })(Phaser.State);
    Litery.Preloader = Preloader;
})(Litery || (Litery = {}));
//# sourceMappingURL=game.js.map