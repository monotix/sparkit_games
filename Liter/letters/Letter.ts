﻿module Litery {

    export class Letter extends Phaser.Sprite {


        private _letter: Phaser.Sprite;
         
        constructor(game: Phaser.Game, letter: string, bgColor:number=1) {

            super(game, 0, 0, "");

            this.name = letter; 
            var bg: Phaser.Sprite;

            if (bgColor == 1) {
                bg = new Phaser.Sprite(this.game, 0, 0, "szare");
            }
            else if (bgColor == 2) {
                bg = new Phaser.Sprite(this.game, 0, 0, "green");
            }
            else if (bgColor == 3) {

                bg = new Phaser.Sprite(this.game, 0, 0, "orange");

            }

            

            this._letter = new Phaser.Sprite(game, 0, 0, "");
            this._letter.addChild(bg);
            this.addChild(this._letter);

            var color: string = "#00FFFF";
            var style = { font: "40px Arial", fill: color, align: "left" };
            console.log("log", letter);
            var _letterShow = new Phaser.Text(this.game, 0, 0, letter, style);
        //    _letterShow.x = this._letter.width / 2 - _letterShow.width / 2;
        //    _letterShow.y = this._letter.height / 2 - _letterShow.height / 2;
            _letterShow.x = bg.width / 2 - _letterShow.width / 2;
            _letterShow.y = bg.height / 2 - _letterShow.height / 2;
            this._letter.addChild(_letterShow);
           
        }




    }


} 