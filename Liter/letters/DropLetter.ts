﻿module Litery {

    export class DropLetter extends Phaser.Group {

        private bgDrop: Phaser.Sprite;
        private _dropGr: Phaser.Group;
        private _arr: Array<PlaceToDrop>;
        private _parent: Letters;
        constructor(game: Phaser.Game, parent:Letters) {

            super(game,parent,"drop",true,true);
            this._parent = parent;
            this.bgDrop = new Phaser.Sprite(game, 0, 0, "bgDrop");
            this.addChild(this.bgDrop);

           

            var let1: PlaceToDrop = new PlaceToDrop(game);
            
            this.add(let1);

            var let2: PlaceToDrop = new PlaceToDrop(game);
            let2.x = let1.x + let1.width + 15;

            this.add(let2);

            var let3: PlaceToDrop = new PlaceToDrop(game);
            let3.x = let2.x + let2.width + 15;
            this.add(let3);

            var let4: PlaceToDrop = new PlaceToDrop(game);
            let4.x = let3.x + let3.width + 15;

            this.add(let4);


            this._arr = new Array(let1, let2, let3, let4);


        }


        public clear() {


            for (var i: number = 0; i < this._arr.length; i++) {

                this._arr[i].clear();

            }


        }


        public checkLetter() {
            var licz: number = 0;
            for (var i: number = 0; i < this._arr.length; i++) {

                if (this._arr[i].isAdded) {
                   console.log( this._arr[i]._letter.letterString());

                   if (this._arr[i]._letter.letterString() == this._parent._arrAnswear[i]) {

                     
                       licz++;

                   }
                   else {

                   }
                }

                
            }

            if (licz == 4) {

                console.log("you win");
            }
            else {
                console.log("you lose");

            }

        }

    }


}
   
   
    