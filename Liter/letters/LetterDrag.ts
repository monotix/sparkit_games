﻿module Litery {

    export class LetterWord extends Phaser.Sprite {


        private _letter: Phaser.Sprite;
        private _letterStr: string;
        public isAdded: boolean = false;
        public placeToDrop: PlaceToDrop;
        constructor(game: Phaser.Game, letter: string) {

            super(game, 0, 0, "");

            this.name = letter;
            var bg = new Phaser.Sprite(this.game, 0, 0, "szare");
            this._letter = new Phaser.Sprite(game, 0, 0, "");
            this._letter.addChild(bg);
            this.addChild(this._letter);

            var color: string = "#00FFFF";
            var style = { font: "40px Arial", fill: color, align: "left" };
            console.log("log", letter);
            var _letterShow = new Phaser.Text(this.game, 0, 0, letter, style);
            //    _letterShow.x = this._letter.width / 2 - _letterShow.width / 2;
            //    _letterShow.y = this._letter.height / 2 - _letterShow.height / 2;

            this._letterStr = letter;

            this._letter.addChild(_letterShow);
            this.inputEnabled = true;
          //  this.input.enableDrag();

        }

        addObjectToDrag(): LetterWord {



            var spr = new LetterWord(this.game, this._letterStr);
            spr.anchor.setTo(0.5, 0.5);

            return spr;

        }

        public letterString(): string {

            return this._letterStr;
        }

    }


}  