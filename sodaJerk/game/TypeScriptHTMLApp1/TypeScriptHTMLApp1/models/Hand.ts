﻿module SodaJerk {

   

    export class Hand extends Phaser.Sprite {

        private _parent: MainGame;
        private _direction: number;
        private _positionForPlayer: Phaser.Point;
        private _hand: Phaser.Button;
        private _handhasCake: boolean = false;
        private _cake:Cake;
        public positionMap:number;
      
        constructor(game: Phaser.Game, parent: MainGame, position, direction, positionMap_) {

            super(game, 0, 0);
            this._parent = parent;
            this.positionMap = positionMap_;
            this._positionForPlayer = position;
            this._direction = direction;
            this._hand = new Phaser.Button(this.game, 0, 0, "transparent", this.handClick, this);
            this._hand.anchor.set(0.5, 0.5);
            this.addChild(this._hand);
            this._hand.inputEnabled = true;
            this._hand.input.useHandCursor = true;
            // new Phaser.Point(580, 332);
        }

        public resetState() {
            if (this._cake != null) {
                this.removeChild(this._cake);
                this._cake = null;
                
            }

            this._handhasCake = false;
        }

        handClick() {
          
                if (this._parent.playerView.movePlayer(this._positionForPlayer, -1)) {
                 //   this.removeChild(this._cake);
               //     this._cake = null;
                //    this._handhasCake = false;
                }


          
        }


        addCake(cake:Cake) {

            this._cake = cake;
          
            this._cake.x = -10;
            this._cake.y = -20;//this._hand.y;
            this._cake.setPositionAndDirection(this._positionForPlayer, this._direction);
            this._cake.addTopHand();
            this.addChild(this._cake);
            this._cake.hand = this;
            this._handhasCake = true;
            
        }

        setHasCake(val:boolean) {
            this._handhasCake = val;
            console.log("zabieram ciasto", this._handhasCake);
        }

    }

}