﻿module SodaJerk {

    export class CustomerConfiguration {
        public phase1: Phaser.Point;
        public phase2: Phaser.Point;
        public phase3: Phaser.Point;
        public phase4: Phaser.Point;
        public phase5: Phaser.Point;
        public phaseAngry: Phaser.Point;
        public phaseHappy: Phaser.Point;
        public timer: Phaser.Point;
        public order: Phaser.Point;
        public placeToClick: Phaser.Point;
        public positionForPlayer: Phaser.Point;
      
        public startPositions: Array<Phaser.Point>;
        constructor() {

        }
    }

}  