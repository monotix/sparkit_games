﻿module SodaJerk {

   

    export class Cake extends Phaser.Sprite {

        private _parent: MainGame;
        private _direction: number;
        private _positionForPlayer: Phaser.Point;
        public hand: Hand;
        private _cake: Phaser.Button;
        public _hasCream: boolean = false;
        private _cream;
        private _isAddToplayer: boolean = false;
        private _positionMap: number;
        public arrayAnswer:Array<number> = new Array();
        constructor(game: Phaser.Game, parent: MainGame,layerId:number, position:Phaser.Point, direction) {

            super(game, 0, 0);
            this._parent = parent;
            this._positionForPlayer = position;
            this._direction = direction;

            this._cake = new Phaser.Button(this.game, 0, 0, 'layer0', this.onCookSelect, this);
            this._cake.frame = layerId;
            this.addChild(this._cake);

            this.arrayAnswer.push(layerId);

      //      this._cake.inputEnabled = true;
          //  this._cake.input.priorityID = 10;
            // new Phaser.Point(580, 332);
        }

        setPositionAndDirection(pos: Phaser.Point, direct: number) {

            this._positionForPlayer = pos;
            this._direction = direct;

        }

        onCookSelect() {
            console.log("klikam w ciasto", PlayerControler.getInstance().playerHasCake, this._isAddToplayer);
           // if (!this._isAddToplayer) {
                if (this._parent.playerView.movePlayer(this._positionForPlayer, -2, this)) {
                    console.log("klikam w ciasto", PlayerControler.getInstance().playerHasCake);
                  //  if (!this._parent.playerView._haveCake) {
                        this._isAddToplayer = true;
                        this.hand.setHasCake(false);
                  //  }
                }
           // }

        }

        addToplayer() {
            this._cake.interactive = false;
            this._cake.inputEnabled = false;
            this._cake.input.priorityID = 0;
        }

        addTopHand() {
            this._cake.interactive = true;
            this._cake.inputEnabled = true;
            this._cake.input.priorityID = 10;
            this._isAddToplayer = false;
        }


        addCream(cream:Cream) {

            this._hasCream = true;
            this._cream = cream;
            this.arrayAnswer.push(cream.layerId);
            this.addChild(this._cream);
        }

    }

} 