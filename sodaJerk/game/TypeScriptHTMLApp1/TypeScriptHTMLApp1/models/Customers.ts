﻿module SodaJerk {

    export class Customers {

        public static _instance: Customers = null;
        private _configurations: Array<CustomerConfiguration>;
        public maxCustomer: number = 0;
        constructor() {

            if (Customers._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            Customers._instance = this;
            this.initConfigurations();
        }


        public static getInstance(): Customers {

            if (Customers._instance === null) {

                Customers._instance = new Customers();
            }

            return Customers._instance;
        }

        public getConfigByCustomerID(customerID: string): CustomerConfiguration {
            if (customerID == '0') {
                this.maxCustomer = 6;
                return this._configurations[0];
            } else if (customerID == '1') {
                this.maxCustomer = 9;
                return this._configurations[1];
            } else if (customerID == '2') {
                return this._configurations[2];
            } else if (customerID == '3') {
                return this._configurations[3];
            }
        }
       

        private initConfigurations(): void {
            this._configurations = new Array();

            var c0: CustomerConfiguration = new CustomerConfiguration();
            c0.phase1 = new Phaser.Point(3, 31);
            c0.phase2 = new Phaser.Point(32, 55);
            c0.phase3 = new Phaser.Point(56, 74);
            c0.phase4 = new Phaser.Point(75, 87);
            c0.phase5 = new Phaser.Point(88, 93);
            c0.phaseAngry = new Phaser.Point(94, 136);
            c0.phaseHappy = new Phaser.Point(137, 170);
            c0.timer = new Phaser.Point(198, 145);
            c0.order = new Phaser.Point(316, 141);
            c0.placeToClick = new Phaser.Point(206, 181);
            c0.startPositions = new Array(
                new Phaser.Point(86, 266),
                new Phaser.Point(0, 218),
                new Phaser.Point(-79, 171),
                new Phaser.Point(-159, 122)
              
                );
            this._configurations.push(c0);

            var c1: CustomerConfiguration = new CustomerConfiguration();
            c1.phase1 = new Phaser.Point(3, 31);
            c1.phase2 = new Phaser.Point(32, 106);
            c1.phase3 = new Phaser.Point(107, 142);
            c1.phase4 = new Phaser.Point(143, 153);
            c1.phase5 = new Phaser.Point(154, 167);
            c1.phaseAngry = new Phaser.Point(169, 217);
            c1.phaseHappy = new Phaser.Point(218, 285);
            c1.timer = new Phaser.Point(50, 147);
            c1.order = new Phaser.Point(168, 143);
            c1.placeToClick = new Phaser.Point(38, 203);
            c1.startPositions = new Array(
                new Phaser.Point(234, 267),
                new Phaser.Point(151, 210),
                new Phaser.Point(77, 170),
                new Phaser.Point(-11, 120)
                );
            this._configurations.push(c1);

            var c2: CustomerConfiguration = new CustomerConfiguration();
            c2.phase1 = new Phaser.Point(3, 15);
            c2.phase2 = new Phaser.Point(16, 27);
            c2.phase3 = new Phaser.Point(28, 45);
            c2.phase4 = new Phaser.Point(46, 59);
            c2.phase5 = new Phaser.Point(60, 71);
            c2.phaseAngry = new Phaser.Point(72, 122);
            c2.phaseHappy = new Phaser.Point(123, 171);
            c2.timer = new Phaser.Point(145, 555);
            c2.order = new Phaser.Point(263, 551);
            c2.placeToClick = new Phaser.Point(143, 601);
            c2.startPositions = new Array(
                new Phaser.Point(142, -126),
                new Phaser.Point(52, -190),
                new Phaser.Point(-24, -235),
                new Phaser.Point(-96, -288)
                );
            this._configurations.push(c2);

            var c3: CustomerConfiguration = new CustomerConfiguration();
            c3.phase1 = new Phaser.Point(3, 31);
            c3.phase2 = new Phaser.Point(3, 31);
            c3.phase3 = new Phaser.Point(32, 41);
            c3.phase4 = new Phaser.Point(42, 51);
            c3.phase5 = new Phaser.Point(52, 70);
            c3.phaseAngry = new Phaser.Point(71, 128);
            c3.phaseHappy = new Phaser.Point(129, 189);
            c3.timer = new Phaser.Point(50, 17);
            c3.order = new Phaser.Point(136, -68);
            c3.placeToClick = new Phaser.Point(36, 68);
            c3.positionForPlayer = new Phaser.Point(100, 400)
            c3.startPositions = new Array(
                new Phaser.Point(244, 432),
                new Phaser.Point(154, 381),
                new Phaser.Point(80, 341),
                new Phaser.Point(-7, 294)
                );
            this._configurations.push(c3);
        }

    }


} 