﻿module SodaJerk {

    export class PlayerControler {


        public static _instance: PlayerControler = null;

        public playerHasCake: boolean = false;
        public machinMakeCream: boolean = false;
        public machinMakeCake: boolean = false;
        public mainSound: Phaser.Sound;
        construct() {

            if (PlayerControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            PlayerControler._instance = this;

        }


        public static getInstance(): PlayerControler {

            if (PlayerControler._instance === null) {

                PlayerControler._instance = new PlayerControler();


            }


            return PlayerControler._instance;



        }

        public setSound(sound_: Phaser.Sound) {

            this.mainSound = sound_;
        }





    }

}