var Greeter = (function () {
    function Greeter(element) {
        this.element = element;
        this.element.innerHTML += "The time is: ";
        this.span = document.createElement('span');
        this.element.appendChild(this.span);
        this.span.innerText = new Date().toUTCString();
    }
    Greeter.prototype.start = function () {
        var _this = this;
        this.timerToken = setInterval(function () { return _this.span.innerHTML = new Date().toUTCString(); }, 500);
    };
    Greeter.prototype.stop = function () {
        clearTimeout(this.timerToken);
    };
    return Greeter;
})();
window.onload = function () {
    var el = document.getElementById('content');
    var greeter = new Greeter(el);
    greeter.start();
};
var SodaJerk;
(function (SodaJerk) {
    var PlayerControler = (function () {
        function PlayerControler() {
            this.playerHasCake = false;
            this.machinMakeCream = false;
            this.machinMakeCake = false;
        }
        PlayerControler.prototype.construct = function () {
            if (PlayerControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            PlayerControler._instance = this;
        };
        PlayerControler.getInstance = function () {
            if (PlayerControler._instance === null) {
                PlayerControler._instance = new PlayerControler();
            }
            return PlayerControler._instance;
        };
        PlayerControler.prototype.setSound = function (sound_) {
            this.mainSound = sound_;
        };
        PlayerControler._instance = null;
        return PlayerControler;
    })();
    SodaJerk.PlayerControler = PlayerControler;
})(SodaJerk || (SodaJerk = {}));
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var SodaJerk;
(function (SodaJerk) {
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            _super.call(this, 800, 600, Phaser.AUTO, "content", null);
            this.state.add("Boot", SodaJerk.Boot, false);
            this.state.add("Preloader", SodaJerk.Preloader, false);
            this.state.add("MainMenu", SodaJerk.MainMenu, false);
            this.state.add("Instruction", SodaJerk.Instruction, false);
            this.state.add("PopupStart", SodaJerk.PopupStart, false);
            this.state.add("MainGame", SodaJerk.MainGame, false);
            this.state.start("Boot");
        }
        return Game;
    })(Phaser.Game);
    SodaJerk.Game = Game;
})(SodaJerk || (SodaJerk = {}));
window.onload = function () {
    var game = new SodaJerk.Game;
};
var userLevel = 1;
var SodaJerk;
(function (SodaJerk) {
    var Cake = (function (_super) {
        __extends(Cake, _super);
        function Cake(game, parent, layerId, position, direction) {
            _super.call(this, game, 0, 0);
            this._hasCream = false;
            this._isAddToplayer = false;
            this.arrayAnswer = new Array();
            this._parent = parent;
            this._positionForPlayer = position;
            this._direction = direction;
            this._cake = new Phaser.Button(this.game, 0, 0, 'layer0', this.onCookSelect, this);
            this._cake.frame = layerId;
            this.addChild(this._cake);
            this.arrayAnswer.push(layerId);
            //      this._cake.inputEnabled = true;
            //  this._cake.input.priorityID = 10;
            // new Phaser.Point(580, 332);
        }
        Cake.prototype.setPositionAndDirection = function (pos, direct) {
            this._positionForPlayer = pos;
            this._direction = direct;
        };
        Cake.prototype.onCookSelect = function () {
            console.log("klikam w ciasto", SodaJerk.PlayerControler.getInstance().playerHasCake, this._isAddToplayer);
            // if (!this._isAddToplayer) {
            if (this._parent.playerView.movePlayer(this._positionForPlayer, -2, this)) {
                console.log("klikam w ciasto", SodaJerk.PlayerControler.getInstance().playerHasCake);
                //  if (!this._parent.playerView._haveCake) {
                this._isAddToplayer = true;
                this.hand.setHasCake(false);
            }
            // }
        };
        Cake.prototype.addToplayer = function () {
            this._cake.interactive = false;
            this._cake.inputEnabled = false;
            this._cake.input.priorityID = 0;
        };
        Cake.prototype.addTopHand = function () {
            this._cake.interactive = true;
            this._cake.inputEnabled = true;
            this._cake.input.priorityID = 10;
            this._isAddToplayer = false;
        };
        Cake.prototype.addCream = function (cream) {
            this._hasCream = true;
            this._cream = cream;
            this.arrayAnswer.push(cream.layerId);
            this.addChild(this._cream);
        };
        return Cake;
    })(Phaser.Sprite);
    SodaJerk.Cake = Cake;
})(SodaJerk || (SodaJerk = {}));
var SodaJerk;
(function (SodaJerk) {
    var Cream = (function (_super) {
        __extends(Cream, _super);
        function Cream(game, layerId) {
            _super.call(this, game, 0, 0);
            this.layerId = layerId;
            console.log("kurwa jaki krem", layerId);
            this.addChild(new Phaser.Sprite(this.game, 0, 0, 'layer1', layerId));
            //   this._cake = this.game.add.button(position.x, position.y, 'layer' + layerId, this.onCookSelect, this);
            // new Phaser.Point(580, 332);
        }
        return Cream;
    })(Phaser.Sprite);
    SodaJerk.Cream = Cream;
})(SodaJerk || (SodaJerk = {}));
var SodaJerk;
(function (SodaJerk) {
    var CustomerConfiguration = (function () {
        function CustomerConfiguration() {
        }
        return CustomerConfiguration;
    })();
    SodaJerk.CustomerConfiguration = CustomerConfiguration;
})(SodaJerk || (SodaJerk = {}));
var SodaJerk;
(function (SodaJerk) {
    var Customers = (function () {
        function Customers() {
            this.maxCustomer = 0;
            if (Customers._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            Customers._instance = this;
            this.initConfigurations();
        }
        Customers.getInstance = function () {
            if (Customers._instance === null) {
                Customers._instance = new Customers();
            }
            return Customers._instance;
        };
        Customers.prototype.getConfigByCustomerID = function (customerID) {
            if (customerID == '0') {
                this.maxCustomer = 6;
                return this._configurations[0];
            }
            else if (customerID == '1') {
                this.maxCustomer = 9;
                return this._configurations[1];
            }
            else if (customerID == '2') {
                return this._configurations[2];
            }
            else if (customerID == '3') {
                return this._configurations[3];
            }
        };
        Customers.prototype.initConfigurations = function () {
            this._configurations = new Array();
            var c0 = new SodaJerk.CustomerConfiguration();
            c0.phase1 = new Phaser.Point(3, 31);
            c0.phase2 = new Phaser.Point(32, 55);
            c0.phase3 = new Phaser.Point(56, 74);
            c0.phase4 = new Phaser.Point(75, 87);
            c0.phase5 = new Phaser.Point(88, 93);
            c0.phaseAngry = new Phaser.Point(94, 136);
            c0.phaseHappy = new Phaser.Point(137, 170);
            c0.timer = new Phaser.Point(198, 145);
            c0.order = new Phaser.Point(316, 141);
            c0.placeToClick = new Phaser.Point(206, 181);
            c0.startPositions = new Array(new Phaser.Point(86, 266), new Phaser.Point(0, 218), new Phaser.Point(-79, 171), new Phaser.Point(-159, 122));
            this._configurations.push(c0);
            var c1 = new SodaJerk.CustomerConfiguration();
            c1.phase1 = new Phaser.Point(3, 31);
            c1.phase2 = new Phaser.Point(32, 106);
            c1.phase3 = new Phaser.Point(107, 142);
            c1.phase4 = new Phaser.Point(143, 153);
            c1.phase5 = new Phaser.Point(154, 167);
            c1.phaseAngry = new Phaser.Point(169, 217);
            c1.phaseHappy = new Phaser.Point(218, 285);
            c1.timer = new Phaser.Point(50, 147);
            c1.order = new Phaser.Point(168, 143);
            c1.placeToClick = new Phaser.Point(38, 203);
            c1.startPositions = new Array(new Phaser.Point(234, 267), new Phaser.Point(151, 210), new Phaser.Point(77, 170), new Phaser.Point(-11, 120));
            this._configurations.push(c1);
            var c2 = new SodaJerk.CustomerConfiguration();
            c2.phase1 = new Phaser.Point(3, 15);
            c2.phase2 = new Phaser.Point(16, 27);
            c2.phase3 = new Phaser.Point(28, 45);
            c2.phase4 = new Phaser.Point(46, 59);
            c2.phase5 = new Phaser.Point(60, 71);
            c2.phaseAngry = new Phaser.Point(72, 122);
            c2.phaseHappy = new Phaser.Point(123, 171);
            c2.timer = new Phaser.Point(145, 555);
            c2.order = new Phaser.Point(263, 551);
            c2.placeToClick = new Phaser.Point(143, 601);
            c2.startPositions = new Array(new Phaser.Point(142, -126), new Phaser.Point(52, -190), new Phaser.Point(-24, -235), new Phaser.Point(-96, -288));
            this._configurations.push(c2);
            var c3 = new SodaJerk.CustomerConfiguration();
            c3.phase1 = new Phaser.Point(3, 31);
            c3.phase2 = new Phaser.Point(3, 31);
            c3.phase3 = new Phaser.Point(32, 41);
            c3.phase4 = new Phaser.Point(42, 51);
            c3.phase5 = new Phaser.Point(52, 70);
            c3.phaseAngry = new Phaser.Point(71, 128);
            c3.phaseHappy = new Phaser.Point(129, 189);
            c3.timer = new Phaser.Point(50, 17);
            c3.order = new Phaser.Point(136, -68);
            c3.placeToClick = new Phaser.Point(36, 68);
            c3.positionForPlayer = new Phaser.Point(100, 400);
            c3.startPositions = new Array(new Phaser.Point(244, 432), new Phaser.Point(154, 381), new Phaser.Point(80, 341), new Phaser.Point(-7, 294));
            this._configurations.push(c3);
        };
        Customers._instance = null;
        return Customers;
    })();
    SodaJerk.Customers = Customers;
})(SodaJerk || (SodaJerk = {}));
var SodaJerk;
(function (SodaJerk) {
    var Hand = (function (_super) {
        __extends(Hand, _super);
        function Hand(game, parent, position, direction, positionMap_) {
            _super.call(this, game, 0, 0);
            this._handhasCake = false;
            this._parent = parent;
            this.positionMap = positionMap_;
            this._positionForPlayer = position;
            this._direction = direction;
            this._hand = new Phaser.Button(this.game, 0, 0, "transparent", this.handClick, this);
            this._hand.anchor.set(0.5, 0.5);
            this.addChild(this._hand);
            this._hand.inputEnabled = true;
            this._hand.input.useHandCursor = true;
            // new Phaser.Point(580, 332);
        }
        Hand.prototype.resetState = function () {
            if (this._cake != null) {
                this.removeChild(this._cake);
                this._cake = null;
            }
            this._handhasCake = false;
        };
        Hand.prototype.handClick = function () {
            if (this._parent.playerView.movePlayer(this._positionForPlayer, -1)) {
            }
        };
        Hand.prototype.addCake = function (cake) {
            this._cake = cake;
            this._cake.x = -10;
            this._cake.y = -20; //this._hand.y;
            this._cake.setPositionAndDirection(this._positionForPlayer, this._direction);
            this._cake.addTopHand();
            this.addChild(this._cake);
            this._cake.hand = this;
            this._handhasCake = true;
        };
        Hand.prototype.setHasCake = function (val) {
            this._handhasCake = val;
            console.log("zabieram ciasto", this._handhasCake);
        };
        return Hand;
    })(Phaser.Sprite);
    SodaJerk.Hand = Hand;
})(SodaJerk || (SodaJerk = {}));
var SodaJerk;
(function (SodaJerk) {
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            _super.apply(this, arguments);
        }
        Boot.prototype.preload = function () {
            this.load.image("bg", "assets/game/splashScreen.png");
            this.load.image("progressBarBlack", "assets/commons/ProgressBar.png");
            this.load.image("progressBarBlue", "assets/commons/ProgressBarBlue.png");
        };
        Boot.prototype.create = function () {
            this.game.state.start("Preloader", true, false);
        };
        return Boot;
    })(Phaser.State);
    SodaJerk.Boot = Boot;
})(SodaJerk || (SodaJerk = {}));
var SodaJerk;
(function (SodaJerk) {
    var Instruction = (function (_super) {
        __extends(Instruction, _super);
        function Instruction() {
            _super.apply(this, arguments);
        }
        Instruction.prototype.create = function () {
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            this._mainMenuBg = this.game.add.audio('instructionSn', 1, true);
            this._mainMenuBg.play("", 0, 1, false);
            var background = this.add.sprite(0, 0, 'InstructionsScreen');
            var gameControler = SodaJerk.PlayerControler.getInstance();
            gameControler.mainSound.volume = 0.05;
            background.width = this.game.width;
            background.height = this.game.height;
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var _buttons = this.add.group();
            var gameButton = this.add.button(0, 0, "games", this.onGameClick, this, 1, 0);
            //gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            _buttons.addChild(gameButton);
            var playButton = this.add.button(100, 0, "playGame", this.onPlayClick, this, 1, 0);
            playButton.onInputDown.add(this.overPlay2, this);
            //playButton.x = this._buttons.width - playButton.width - 10;
            _buttons.addChild(playButton);
            _buttons.x = 0;
            _buttons.y = this.game.stage.height - _buttons.height;
        };
        Instruction.prototype.onGameClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        Instruction.prototype.overPlay2 = function () {
            this.overButtonA.play();
        };
        Instruction.prototype.onPlayClick = function () {
            this._mainMenuBg.stop();
            var gameControler = SodaJerk.PlayerControler.getInstance();
            gameControler.mainSound.volume = 0.1;
            this.game.state.start("MainGame", true, false);
        };
        return Instruction;
    })(Phaser.State);
    SodaJerk.Instruction = Instruction;
})(SodaJerk || (SodaJerk = {}));
var SodaJerk;
(function (SodaJerk) {
    var MainGame = (function (_super) {
        __extends(MainGame, _super);
        function MainGame() {
            _super.apply(this, arguments);
            this._customerArray = new Array();
            this._customerTimeCounter = 10;
            this.level = 0;
            this._maxCustomer = 0;
            this._counter = 0;
        }
        MainGame.prototype.create = function () {
            this.level = userLevel - 1;
            this.checkLevel();
            this.initGameData();
            //game background
            this.playerView = new SodaJerk.PlayerView(this.game, this);
            var bg = this.game.add.sprite(0, 0, 'gameBg');
            //back bg
            this._backBg = new SodaJerk.BackwardView(this.game, this);
            this._backBg.showSceneByID(0);
            this._backBg.position.setTo(-4, -122);
            //front bg
            //cooking station
            this.initCookingStations(547, 333, 0, 92);
            var frontBg = this.game.add.sprite(-2, 284, 'frontBg');
            this.game.stage.addChildAt(this.playerView, 1);
            this.game.stage.addChildAt(frontBg, 2);
            this._customersLayer = this.game.add.group(this, 'customers', true);
            this.panelPoint = new SodaJerk.PointPanelView(this.game, this);
            this.game.stage.addChild(this.panelPoint);
            this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);
            this.showFirstCustomer();
        };
        MainGame.prototype.showFirstCustomer = function () {
            //pokazuje pierwszego
            var randomCustomer = this.game.rnd.integerInRange(0, 3);
            this.showCustomer(String(randomCustomer));
        };
        MainGame.prototype.playAgain = function () {
            console.log("playAgain");
            this.checkLevel();
            this._counter = 0;
            this.playerView.playerReset();
            this._cookingStation.resetState();
            this._backBg.layetonestation.resetState();
            this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);
            this.showFirstCustomer();
        };
        MainGame.prototype.checkLevel = function () {
            if (this.level == 0) {
                this._maxCustomer = 7;
            }
            else if (this.level == 2) {
                this._maxCustomer = 10;
            }
            else if (this.level == 3) {
                this._maxCustomer = 13;
            }
        };
        MainGame.prototype.endGame = function () {
            this.removeAllCustomer();
            if (this.panelPoint.checkSccore()) {
                //you win
                this.youwin();
                if (this.level < 4) {
                    this.level++;
                    APIsetLevel(userID, gameID, userLevel + this.level + 1);
                }
                this.checkLevel();
            }
            else {
                // you lose
                this.youLose();
            }
        };
        MainGame.prototype.youwin = function () {
            this.screenWinOrGameOver = new SodaJerk.PopUpWin(this.game, this, 0, 0);
            this.addTostage();
        };
        MainGame.prototype.youLose = function () {
            this.screenWinOrGameOver = new SodaJerk.PopUp(this.game, this, 0, 0);
            this.addTostage();
        };
        MainGame.prototype.addTostage = function () {
            this.game.stage.addChild(this.screenWinOrGameOver);
            this.screenWinOrGameOver.activeButton();
            this.screenWinOrGameOver.visible = true;
        };
        MainGame.prototype.updateTimer = function () {
            if (this._maxCustomer < 0) {
                // console.log("end game timer");
                this.game.time.events.removeAll();
                this.endGame();
                return;
            }
            var randomCustomer = this.game.rnd.integerInRange(0, 3);
            if (this._counter == this._customerTimeCounter && this._customerArray.length < 4 && this._maxCustomer > 0) {
                // console.log("nowy customer", this._maxCustomer);
                this.showCustomer(String(randomCustomer));
                this._counter = 0;
            }
            this._counter++;
            // console.log("time " + this._counter);
        };
        MainGame.prototype.initGameData = function () {
        };
        MainGame.prototype.initCookingStations = function (xpos, ypos, stationID, maxFrames) {
            this._cookingStation = new SodaJerk.CookingStation(this.game, this, stationID, maxFrames);
            this._cookingStation.position.setTo(xpos, ypos);
            this.game.stage.addChildAt(this._cookingStation, 2);
        };
        MainGame.prototype.showCustomer = function (customerID) {
            var c = new SodaJerk.CustomerView(this.game, this, customerID, 3 - this._customerArray.length);
            this._customerArray.push(c);
            this._customersLayer.addChild(c);
            c.startOrder(5);
        };
        MainGame.prototype.removeAllCustomer = function () {
            for (var i = 0; i < this._customerArray.length; i++) {
                //this._customerArray[i] = null;
                this._customerArray.splice(i, 1);
            }
            this._customersLayer.removeAll(true);
        };
        MainGame.prototype.removeCustomer = function (c) {
            for (var i = 0; i < this._customerArray.length; i++) {
                if (this._customerArray[i] == c) {
                    //this._customerArray[i] = null;
                    this._customerArray.splice(i, 1);
                    this._maxCustomer--;
                }
            }
            for (var i = 0; i < this._customerArray.length; i++) {
                this._customerArray[i].changePosition(3 - i);
            }
        };
        MainGame.prototype.movePlayer = function (point, cake) {
            if (cake === void 0) { cake = null; }
            this.playerView.movePlayer(point, cake);
        };
        //Delegates
        //Cooking machines
        //delegate when machine end cooking.
        MainGame.prototype.takeCook = function (cake) {
            console.log("bierz ciasto");
            this.playerView.addFood(cake);
        };
        return MainGame;
    })(Phaser.State);
    SodaJerk.MainGame = MainGame;
})(SodaJerk || (SodaJerk = {}));
var SodaJerk;
(function (SodaJerk) {
    var MainMenu = (function (_super) {
        __extends(MainMenu, _super);
        function MainMenu() {
            _super.apply(this, arguments);
        }
        MainMenu.prototype.create = function () {
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var _mainMenuBg = this.game.add.audio('MainMenuSn', 1, true);
            _mainMenuBg.play("", 0, 0.1, true);
            _mainMenuBg.volume = 0.1;
            var gameControler = SodaJerk.PlayerControler.getInstance();
            gameControler.setSound(_mainMenuBg);
            var background = this.add.sprite(0, 0, 'mainMenuBg');
            var _buttons = this.add.group();
            var buttonsBg = this.add.sprite(0, 0, 'buttonsBg');
            // _buttons.addChild(buttonsBg);
            var gameButton = this.add.button(0, 0, "games", this.onGameClick, this, 1, 0);
            //gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            _buttons.addChild(gameButton);
            var playButton = this.add.button(100, 0, "playGame", this.onPlayClick, this, 1, 0);
            playButton.onInputDown.add(this.overPlay2, this);
            _buttons.addChild(playButton);
            _buttons.x = 0;
            _buttons.y = this.game.stage.height - _buttons.height;
        };
        MainMenu.prototype.overPlay2 = function () {
            this.overButtonA.play();
        };
        MainMenu.prototype.onGameClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        MainMenu.prototype.onPlayClick = function () {
            this.game.state.start("PopupStart", true, false);
        };
        return MainMenu;
    })(Phaser.State);
    SodaJerk.MainMenu = MainMenu;
})(SodaJerk || (SodaJerk = {}));
var SodaJerk;
(function (SodaJerk) {
    var Preloader = (function (_super) {
        __extends(Preloader, _super);
        function Preloader() {
            _super.apply(this, arguments);
        }
        Preloader.prototype.preload = function () {
            var bg = this.add.image(0, 0, "bg", 0);
            var blackProgress = this.add.image(0, 0, "progressBarBlack", 0);
            var blueProgress = this.add.sprite(0, 0, "progressBarBlue", 0);
            blackProgress.position.setTo(this.stage.width / 2 - blackProgress.width / 2, this.stage.height - blackProgress.height - 10);
            blueProgress.position = blackProgress.position;
            this.load.setPreloadSprite(blueProgress);
            //this.load.image("InstructionsScreen", "assets/game/instructionsScreen.png");
            mobile = false;
            if (!this.game.device.desktop) {
                mobile = true;
            }
            if (mobile) {
                this.load.image("InstructionsScreen", "assets/popup/03_soda_jerk.png");
                this.load.audio("instructionSn", "assets/audio/soda_jerk_02.mp3");
            }
            else {
                this.load.image("InstructionsScreen", "assets/popup/03_soda_jerk.png");
                this.load.audio("instructionSn", "assets/audio/soda_jerk_01.mp3");
            }
            this.load.image("popup", "assets/popup/popup.png");
            this.load.image("logoGame", "assets/popup/logo.png");
            this.load.atlasJSONArray('games', 'assets/popup/games.png', 'assets/popup/games.json');
            this.load.atlasJSONArray('playGame', 'assets/popup/play.png', 'assets/popup/play.json');
            this.load.atlasJSONArray('helpBtn', 'assets/popup/help.png', 'assets/popup/help.json');
            this.load.image("mainMenuBg", "assets/game/splashScreen.png");
            this.load.atlasJSONArray('buttons', 'assets/commons/mainMenuButtons.png', 'assets/commons/mainMenuButtons.json');
            this.load.image('buttonsBg', 'assets/commons/naviMenuBackground.png');
            this.load.audio("MainMenuSn", "assets/game/SodaJerkTheme.mp3");
            this.load.audio("overButtonA", "assets/commons/overButton.mp3");
            this.load.audio("clearButtonA", "assets/commons/clearButton.mp3");
            this.load.audio("customerShowAudio", "assets/game/customerShow.wav");
            this.load.audio("customerHideAudio", "assets/game/customerHide.wav");
            this.load.audio("customerHideWrongAudio", "assets/game/customerHideWrong.wav");
            this.load.audio("stationCookWorkEnd", "assets/game/stationCookWorkEnd.wav");
            this.load.audio("stationCookWork0", "assets/game/stationCookWork0.wav");
            this.load.audio("stationCookWork1", "assets/game/stationCookWork1.wav");
            this.load.image("gameBg", "assets/game/gameBg.png");
            this.load.image("frontBg", "assets/game/front.png");
            this.load.atlasJSONArray('station0', 'assets/game/station1.png', 'assets/game/station1.json');
            this.load.atlasJSONArray('station1', 'assets/game/station2.png', 'assets/game/station2.json');
            this.load.atlasJSONArray('timer', 'assets/game/timer.png', 'assets/game/timer.json');
            this.load.image("transparent", "assets/game/50x50.png");
            this.load.atlasJSONArray('wajcha', 'assets/game/s1options0Button.png', 'assets/game/s1options0Button.json');
            this.load.atlasJSONArray('wajcha2', 'assets/game/s1options1Button.png', 'assets/game/s1options1Button.json');
            this.load.atlasJSONArray('wajcha3', 'assets/game/s1options2Button.png', 'assets/game/s1options2Button.json');
            this.load.atlasJSONArray('wajcha4', 'assets/game/s1options3Button.png', 'assets/game/s1options3Button.json');
            this.load.atlasJSONArray('ciasto', 'assets/game/s0option0Button.png', 'assets/game/s0option0Button.json');
            this.load.atlasJSONArray('ciasto2', 'assets/game/s0option1Button.png', 'assets/game/s0option1Button.json');
            this.load.atlasJSONArray('ciasto3', 'assets/game/s0option2Button.png', 'assets/game/s0option2Button.json');
            this.load.atlasJSONArray('ciasto4', 'assets/game/s0option3Button.png', 'assets/game/s0option3Button.json');
            this.load.atlasJSONArray('trash', 'assets/game/trash.png', 'assets/game/trash.json');
            this.load.atlasJSONArray('panelPoints', 'assets/game/gui2.png', 'assets/game/gui2.json');
            this.load.atlasJSONArray('player', 'assets/game/player.png', 'assets/game/player.json');
            this.load.image("orderBg", "assets/game/order.png");
            this.load.image("s0", "assets/game/s0_1.png");
            this.load.image("s1", "assets/game/s0_2.png");
            this.load.image("s2", "assets/game/s0_3.png");
            this.load.image("s3", "assets/game/s1_3.png");
            this.load.image("s4", "assets/game/s1_4.png");
            this.load.image("s5", "assets/game/s3_2.png");
            this.load.image("s6", "assets/game/s3_3.png");
            this.load.image("s7", "assets/game/s4_4.png");
            this.load.image("s8", "assets/game/s4_5.png");
            this.load.image("s9", "assets/game/s5_4.png");
            this.load.image("s10", "assets/game/s5_5.png");
            this.load.image("s11", "assets/game/s0_0.png");
            for (var i = 0; i < 4; i++) {
                this.load.image("station0Button" + i, "assets/game/station1_b" + i + ".png");
                this.load.image("station1Button" + i, "assets/game/station2_b" + i + ".png");
                this.load.atlasJSONArray('customer' + i, 'assets/game/customer' + i + '.png', 'assets/game/customer' + i + '.json');
                this.load.atlasJSONArray('layer' + i, 'assets/game/layer' + i + '.png', 'assets/game/layer' + i + '.json');
            }
        };
        Preloader.prototype.create = function () {
            this.game.state.start("MainMenu", true, false);
        };
        return Preloader;
    })(Phaser.State);
    SodaJerk.Preloader = Preloader;
})(SodaJerk || (SodaJerk = {}));
var mobile;
var SodaJerk;
(function (SodaJerk) {
    var BackwardView = (function (_super) {
        __extends(BackwardView, _super);
        function BackwardView(game, parent) {
            _super.call(this, game);
            game.add.existing(this);
            this._parent = parent;
        }
        BackwardView.prototype.showSceneByID = function (sceneID) {
            this.forEach(this.reset, this);
            switch (sceneID) {
                case 0:
                    this.initScene0();
                    break;
                case 1:
                    this.initScene1();
                    break;
                case 2:
                    this.initScene2();
                    break;
                case 3:
                    this.initScene3();
                    break;
                case 4:
                    this.initScene4();
                    break;
            }
        };
        BackwardView.prototype.reset = function (sprite) {
            sprite.destroy();
        };
        BackwardView.prototype.initScene0 = function () {
            var s11 = new Phaser.Sprite(this.game, 0, 0, 's11');
            this.addChild(s11);
            var s1 = new Phaser.Sprite(this.game, 0, 0, 's1');
            this.addChild(s1);
            var s3 = new Phaser.Sprite(this.game, 0, 0, 's2');
            this.addChild(s3);
            this.layetonestation = new SodaJerk.LayerOneStation(this.game, this._parent, 1, 32);
            this.layetonestation.position.setTo(106, 138);
            this.addChild(this.layetonestation);
            var s0 = new Phaser.Sprite(this.game, 0, 0, 's0');
            this.addChild(s0);
        };
        BackwardView.prototype.initScene1 = function () {
            var s11 = this.game.add.sprite(0, 0, 's11');
            this.addChild(s11);
            var s0 = this.game.add.sprite(0, 0, 's0');
            this.addChild(s0);
            var s1 = this.game.add.sprite(0, 0, 's1');
            this.addChild(s1);
            var s3 = this.game.add.sprite(0, 0, 's3');
            this.addChild(s3);
            var s4 = this.game.add.sprite(0, 0, 's4');
            this.addChild(s4);
        };
        BackwardView.prototype.initScene2 = function () {
            var s11 = this.game.add.sprite(0, 0, 's11');
            this.addChild(s11);
            var s0 = this.game.add.sprite(0, 0, 's0');
            this.addChild(s0);
            var s1 = this.game.add.sprite(0, 0, 's5');
            this.addChild(s1);
            var s3 = this.game.add.sprite(0, 0, 's6');
            this.addChild(s3);
            var s5 = this.game.add.sprite(0, 0, 's3');
            this.addChild(s5);
            var s4 = this.game.add.sprite(0, 0, 's4');
            this.addChild(s4);
        };
        BackwardView.prototype.initScene3 = function () {
            var s11 = this.game.add.sprite(0, 0, 's11');
            this.addChild(s11);
            var s0 = this.game.add.sprite(0, 0, 's0');
            this.addChild(s0);
            var s1 = this.game.add.sprite(0, 0, 's5');
            this.addChild(s1);
            var s3 = this.game.add.sprite(0, 0, 's6');
            this.addChild(s3);
            var s5 = this.game.add.sprite(0, 0, 's7');
            this.addChild(s5);
            var s4 = this.game.add.sprite(0, 0, 's8');
            this.addChild(s4);
            var s4 = this.game.add.sprite(0, 0, 's4');
            this.addChild(s4);
        };
        BackwardView.prototype.initScene4 = function () {
            var s11 = this.game.add.sprite(0, 0, 's11');
            this.addChild(s11);
            var s0 = this.game.add.sprite(0, 0, 's0');
            this.addChild(s0);
            var s1 = this.game.add.sprite(0, 0, 's5');
            this.addChild(s1);
            var s3 = this.game.add.sprite(0, 0, 's6');
            this.addChild(s3);
            var s5 = this.game.add.sprite(0, 0, 's7');
            this.addChild(s5);
            var s4 = this.game.add.sprite(0, 0, 's8');
            this.addChild(s4);
            var s5 = this.game.add.sprite(0, 0, 's9');
            this.addChild(s5);
            var s6 = this.game.add.sprite(0, 0, 's10');
            this.addChild(s6);
        };
        return BackwardView;
    })(Phaser.Group);
    SodaJerk.BackwardView = BackwardView;
})(SodaJerk || (SodaJerk = {}));
var SodaJerk;
(function (SodaJerk) {
    var CookingStation = (function (_super) {
        __extends(CookingStation, _super);
        function CookingStation(game, parent, stationID, maxFrames) {
            _super.call(this, game);
            this._isMachineReady = true;
            game.add.existing(this);
            this._parent = parent;
            this._stationID = stationID;
            this._maxFrames = maxFrames;
            this.initStation();
            this.initButtons();
        }
        CookingStation.prototype.initStation = function () {
            this._station = new Phaser.Sprite(this.game, 0, 0, 'station' + this._stationID, 0);
            this._station.animations.add('anim', this.countFrames(1, this._maxFrames), 24, true);
            this.addChild(this._station);
            this._handCake = new SodaJerk.Hand(this.game, this._parent, new Phaser.Point(560, 282), 2, 1);
            this._handCake.x = 50;
            this._handCake.y = 20;
            this.addChild(this._handCake);
            this._workingAudio = this.game.add.audio("stationCookWork" + this._stationID, 0.5, true);
        };
        CookingStation.prototype.resetState = function () {
            this._isMachineReady = true;
            if (this._cake != null)
                this._cake = null;
            this._handCake.resetState();
            this.resetStation();
        };
        CookingStation.prototype.initButtons = function () {
            /*
            var b0: Phaser.Button = this.game.add.button(3, 74, 'station'+this._stationID+'Button0', this.onButtonClicked, this, 1, 1, 1, 1);
            b0.name = '0';
       //     this.addChild(b0);
            var b1: Phaser.Button = this.game.add.button(22, 85, 'station' + this._stationID + 'Button1', this.onButtonClicked, this, 1, 1, 1, 1);
            b1.name = '1';
       //     this.addChild(b1);
            var b2: Phaser.Button = this.game.add.button(106, 13, 'station' + this._stationID + 'Button2', this.onButtonClicked, this, 1, 1, 1, 1);
            b2.name = '2';
        //    this.addChild(b2);
            var b3: Phaser.Button = this.game.add.button(126, 24, 'station' + this._stationID + 'Button3', this.onButtonClicked, this, 1, 1, 1, 1);
            b3.name = '3';
         //   this.addChild(b3);
            */
            this._cookPosition = new Phaser.Point(35, 2);
            this.dodajWajchy();
            this.dodajCiasto();
            this.smieci();
        };
        CookingStation.prototype.handCake = function () {
            console.log("hand cake");
            // this.startCooking(10);
            // this._parent.movePlayer(new Phaser.Point(580, 332), 1);
        };
        CookingStation.prototype.smieci = function () {
            var trash = this.game.add.button(417, 37, "trash", this.trash, this, 1, 0, 1, 1);
        };
        CookingStation.prototype.trash = function () {
            this._parent.playerView.movePlayer(new Phaser.Point(370, 67), 0);
        };
        CookingStation.prototype.dodajWajchy = function () {
        };
        CookingStation.prototype.dodajCiasto = function () {
            if (this._stationID == 0) {
                var ciasto = this.game.add.button(650, 342, "ciasto", this.onButtonClicked, this, 1, 0, 1, 1);
                ciasto.name = "2";
                this.game.stage.addChild(ciasto);
                var ciasto2 = this.game.add.button(669, 354, "ciasto2", this.onButtonClicked, this, 1, 0, 1, 1);
                ciasto2.name = "1";
                this.game.stage.addChild(ciasto2);
                var ciasto = this.game.add.button(545, 405, "ciasto3", this.onButtonClicked, this, 1, 0, 1, 1);
                ciasto.name = "0";
                this.game.stage.addChild(ciasto);
                var ciasto2 = this.game.add.button(564, 416, "ciasto4", this.onButtonClicked, this, 1, 0, 1, 1);
                ciasto2.name = "3";
                this.game.stage.addChild(ciasto2);
            }
        };
        CookingStation.prototype.onButtonClicked = function (button) {
            //console.log("machine ready",!this._isMachineReady);
            if (!this._isMachineReady)
                return;
            console.log(button.name);
            this._selectedButtonID = parseInt(button.name);
            this._isMachineReady = false;
            this.startCooking(2);
        };
        CookingStation.prototype.countFrames = function (startFrame, endFrame) {
            var countArr = new Array();
            for (var i = startFrame; i < endFrame + 1; i++) {
                countArr.push(i);
            }
            return countArr;
        };
        CookingStation.prototype.startCooking = function (seconds) {
            this._station.animations.play('anim');
            this._timer = this.game.time.events.add(seconds * 1000, this.onCookingComplete, this);
            this.hideWajchy(true);
            this._workingAudio.play("", 0, 0.5, true);
        };
        CookingStation.prototype.resetStation = function () {
            this._station.animations.stop('anim', true);
            this._station.frame = 0;
            this.game.time.events.remove(this._timer);
            this._workingAudio.stop();
        };
        CookingStation.prototype.onCookingComplete = function () {
            this.resetStation();
            this.showCook();
            this.hideWajchy(false);
            var completeAudio = this.game.add.sound("stationCookWorkEnd", 1, 0);
            completeAudio.play("", 0, 1);
        };
        CookingStation.prototype.showCook = function () {
            console.log("show cock w ciescie id", this._selectedButtonID);
            this._cake = new SodaJerk.Cake(this.game, this._parent, this._selectedButtonID, new Phaser.Point(0, 0), 2);
            this._handCake.addCake(this._cake);
            this._cake.hand = this._handCake;
            this._isMachineReady = true;
        };
        CookingStation.prototype.onCookSelect = function () {
            this._parent.playerView.movePlayer(new Phaser.Point(this._station.world.x + 60, this._station.world.y + 30), 1, this._cake);
        };
        CookingStation.prototype.hideWajchy = function (hide) {
            if (this._wajchy) {
                for (var i = 0; i < this._wajchy.length; i++) {
                    var b = this._wajchy[i];
                    b.visible = !hide;
                }
            }
        };
        return CookingStation;
    })(Phaser.Group);
    SodaJerk.CookingStation = CookingStation;
})(SodaJerk || (SodaJerk = {}));
var SodaJerk;
(function (SodaJerk) {
    var CookView = (function (_super) {
        __extends(CookView, _super);
        function CookView(game, parent) {
            _super.call(this, game);
            game.add.existing(this);
            this._parent = parent;
        }
        CookView.prototype.buildCook = function (layersID) {
            this._layersID = layersID;
            for (var i = 0; i < this._layersID.length; i++) {
                var l = this.game.add.sprite(25, 26, 'layer' + i, this._layersID[i]);
                this.addChild(l);
            }
        };
        return CookView;
    })(Phaser.Group);
    SodaJerk.CookView = CookView;
})(SodaJerk || (SodaJerk = {}));
var SodaJerk;
(function (SodaJerk) {
    var CustomerView = (function (_super) {
        __extends(CustomerView, _super);
        function CustomerView(game, parent, customerID, position) {
            _super.call(this, game);
            this._isCorrectOrder = false;
            this._showMenu = false;
            this._showMenuTimer = 0;
            this._readyForOrder = false;
            this._end = false;
            game.add.existing(this);
            /*
            losowanie poprawnych odpowiedzi

            */
            var randomCiasto = this.game.rnd.integerInRange(0, 3);
            var randomKrem = this.game.rnd.integerInRange(0, 3);
            var randomPolewa = this.game.rnd.integerInRange(0, 3);
            var randomOwoc = this.game.rnd.integerInRange(0, 3);
            this._answerCorrect = new Array();
            this._answerCorrect.push(randomCiasto);
            this._answerCorrect.push(randomKrem);
            //
            var showAudio = this.game.add.audio('customerShowAudio', 1, true);
            showAudio.play("", 0, 0.5, false);
            this._parent = parent;
            this._customerID = customerID;
            this._config = SodaJerk.Customers.getInstance().getConfigByCustomerID(customerID);
            this._spritToClick = new Phaser.Sprite(this.game, this._config.placeToClick.x, this._config.placeToClick.y, "orderBg");
            this._spritToClick.alpha = 0;
            this.add(this._spritToClick);
            this.initCustomer();
            var p = this._config.startPositions[position];
            this.position.setTo(p.x, p.y);
        }
        CustomerView.prototype.changePosition = function (position) {
            var p = this._config.startPositions[position];
            this.position.setTo(p.x, p.y);
        };
        CustomerView.prototype.initCustomer = function () {
            this._customer = this.game.add.sprite(0, 0, 'customer' + this._customerID, 0);
            this._customer.animations.add('phase1', this.countFrames(this._config.phase1.x, this._config.phase1.y), 24, true);
            this._customer.animations.add('phase2', this.countFrames(this._config.phase2.x, this._config.phase2.y), 24, true);
            this._customer.animations.add('phase3', this.countFrames(this._config.phase3.x, this._config.phase3.y), 24, true);
            this._customer.animations.add('phase4', this.countFrames(this._config.phase4.x, this._config.phase4.y), 24, true);
            this._customer.animations.add('phase5', this.countFrames(this._config.phase5.x, this._config.phase5.y), 24, true);
            this._customer.animations.add('angry', this.countFrames(this._config.phaseAngry.x, this._config.phaseAngry.y), 24, true);
            this._customer.animations.add('happy', this.countFrames(this._config.phaseHappy.x, this._config.phaseHappy.y), 24, true);
            this.addChild(this._customer);
            this._emotions = this.game.add.sprite(this._config.timer.x, this._config.timer.y, 'timer', 0);
            this.addChild(this._emotions);
            this._order = this.game.add.group(this, 'order');
            var orderBg = this.game.add.sprite(0, 0, 'orderBg', 0);
            this._order.addChild(orderBg);
            this._order.visible = false;
            var cook = new SodaJerk.CookView(this.game, this._parent);
            cook.buildCook(this._answerCorrect);
            this._order.addChild(cook);
            //this._order.position.setTo(this._config.order.x, this._config.order.y);
            //this.addChild(this._order);
            this._spritToClick.inputEnabled = true;
            this._spritToClick.events.onInputDown.add(this.clickCustomer, this);
        };
        CustomerView.prototype.clickCustomer = function (sprite, pointer) {
            console.log("click customer", pointer.x, pointer.y);
            if (!this._showMenu && !this._readyForOrder) {
                this._showMenu = true;
                this._parent.playerView.movePlayer(new Phaser.Point(this._spritToClick.world.x + this._spritToClick.width + 40, this._spritToClick.world.y - 40), -2, null, null, this, true);
            }
            else {
                if (SodaJerk.PlayerControler.getInstance().playerHasCake && this._readyForOrder) {
                    // this._parent.playerView.movePlayer(new Phaser.Point(pointer.x + 320, pointer.y-30),-3,null,null,this);
                    this._parent.playerView.movePlayer(new Phaser.Point(this._spritToClick.world.x + this._spritToClick.width + 40, this._spritToClick.world.y - 40), -3, null, null, this);
                }
                else {
                    console.log("pozycja customera", this._spritToClick.world.x, this._spritToClick.world.y);
                    this._parent.playerView.movePlayer(new Phaser.Point(this._spritToClick.world.x + this._spritToClick.width + 40, this._spritToClick.world.y - 40), -2);
                }
            }
        };
        CustomerView.prototype.countFrames = function (startFrame, endFrame) {
            var countArr = new Array();
            for (var i = startFrame; i < endFrame + 1; i++) {
                countArr.push(i);
            }
            return countArr;
        };
        CustomerView.prototype.playPhaseAnimation = function () {
            this._customer.animations.play('phase' + this._curentPhase, 24, true);
        };
        CustomerView.prototype.onFeedbackComplete = function () {
            this._customer.visible = false;
            this._parent.removeCustomer(this);
        };
        CustomerView.prototype.hideEmotions = function () {
            this._emotions.visible = false;
            this.removeChild(this._emotions);
            this._emotions.destroy(true);
        };
        CustomerView.prototype.showMenu = function () {
            this._customer.animations.stop('phase' + this._curentPhase, false);
            this._customer.frame = 1;
        };
        CustomerView.prototype.playHappyAnimation = function () {
            console.log("gra happy");
            this._customer.animations.play('happy', 24, false, true);
            this.removeOrder();
            this._customer.animations.getAnimation('happy').onComplete.add(this.onFeedbackComplete, this);
            var showAudio = this.game.add.audio('customerHideAudio', 1, true);
            showAudio.play("", 0, 0.5, false);
        };
        CustomerView.prototype.playAngryAnimation = function () {
            console.log("gra angry");
            this._customer.animations.play('angry', 24, false, true);
            this.removeOrder();
            this._customer.animations.getAnimation('angry').onComplete.add(this.onFeedbackComplete, this);
            var showAudio = this.game.add.audio('customerHideWrongAudio', 1, true);
            showAudio.play("", 0, 0.5, false);
        };
        CustomerView.prototype.removeOrder = function () {
            this.hideEmotions();
            this._order.removeAll();
            this.removeChild(this._order);
        };
        CustomerView.prototype.newCustomer = function () {
        };
        CustomerView.prototype.playFeedback = function () {
            console.log("koncze odpowiedz", this._isCorrectOrder, this._emotions.frame);
            if (this._isCorrectOrder) {
                this._parent.panelPoint.changeScore(this._emotions.frame);
                this.playHappyAnimation();
            }
            else {
                this.playAngryAnimation();
            }
        };
        CustomerView.prototype.startOrder = function (phaseLength) {
            // this._isCorrectOrder = true;
            this._curentPhase = 1;
            this._timer = this.game.time.events.add(phaseLength * 1000, this.updateTimer, this);
            this._timer.loop = true;
            this.playPhaseAnimation();
            this._emotions.frame = this._curentPhase - 1;
        };
        CustomerView.prototype.updateTimer = function () {
            console.log("this._showMenu", this._showMenu);
            if (this._showMenu) {
                this._showMenuTimer++;
                console.log("menu timer", this._showMenuTimer);
                if (this._showMenuTimer > 1) {
                    this._order.position.setTo(this._config.order.x, this._config.order.y);
                    this.addChild(this._order);
                    this._showMenu = false;
                    this._readyForOrder = true;
                    this._order.visible = true;
                    //   this._curentPhase = 0;
                    this._customer.animations.play('phase' + this._curentPhase, 24, true);
                }
            }
            else if (!this._end) {
                this._curentPhase++;
                if (this._curentPhase == 6) {
                    this.game.time.events.remove(this._timer);
                    this.playFeedback();
                }
                else {
                    this.playPhaseAnimation();
                    this._emotions.frame = this._curentPhase - 1;
                }
            }
        };
        CustomerView.prototype.checkAnswer = function (answer) {
            console.log("sprawdzam", answer, this._answerCorrect);
            this._end = true;
            var correct = 0;
            for (var i = 0; i < this._answerCorrect.length; i++) {
                if (answer[i] == this._answerCorrect[i]) {
                }
                else {
                    this._isCorrectOrder = false;
                    this.playFeedback();
                    return;
                }
            }
            this._isCorrectOrder = true;
            this.playFeedback();
        };
        return CustomerView;
    })(Phaser.Group);
    SodaJerk.CustomerView = CustomerView;
})(SodaJerk || (SodaJerk = {}));
var SodaJerk;
(function (SodaJerk) {
    var LayerOneStation = (function (_super) {
        __extends(LayerOneStation, _super);
        function LayerOneStation() {
            _super.apply(this, arguments);
            this._hasCake = false;
        }
        LayerOneStation.prototype.initButtons = function () {
            /*
               var b0: Phaser.Button = this.game.add.button(78, 90, 'station' + this._stationID + 'Button0', this.onButtonClicked, this, 1, 1, 1, 1);
               b0.name = '0';
       //        this.addChild(b0);
               var b1: Phaser.Button = this.game.add.button(97, 80, 'station' + this._stationID + 'Button1', this.onButtonClicked, this, 1, 1, 1, 1);
               b1.name = '1';
       //        this.addChild(b1);
               var b2: Phaser.Button = this.game.add.button(116, 69, 'station' + this._stationID + 'Button2', this.onButtonClicked, this, 1, 1, 1, 1);
               b2.name = '2';
      //         this.addChild(b2);
               var b3: Phaser.Button = this.game.add.button(135, 59, 'station' + this._stationID + 'Button3', this.onButtonClicked, this, 1, 1, 1, 1);
               b3.name = '3';
          //     this.addChild(b3);
               */
            this._parent.machinCream = this;
            this._handCook = new SodaJerk.Hand(this.game, this._parent, new Phaser.Point(225, 162), 0, 2);
            this._handCook.x = 140;
            this._handCook.y = 172;
            this.addChild(this._handCook);
            this._wajchy = new Array();
            // if (this._stationID == 0) {
            var wajcha = this.game.add.button(175, 102, "wajcha", this.robKrem, this, 1, 0, 1, 1);
            wajcha.name = "0";
            this._wajchy.push(wajcha);
            var wajcha2 = this.game.add.button(194, 91, "wajcha2", this.robKrem, this, 1, 0, 1, 1);
            wajcha2.name = "1";
            this._wajchy.push(wajcha2);
            var wajcha3 = this.game.add.button(213, 80, "wajcha3", this.robKrem, this, 1, 0, 1, 1);
            wajcha3.name = "2";
            this._wajchy.push(wajcha3);
            var wajcha4 = this.game.add.button(232, 69, "wajcha4", this.robKrem, this, 1, 0, 1, 1);
            wajcha4.name = "3";
            this._wajchy.push(wajcha4);
            // var wajcha3 = this.game.add.button(215, 80, "wajcha3", this.onButtonClicked, this, 1, 0, 1, 1);
            // var wajcha4 = this.game.add.button(235, 79, "wajcha4", this.onButtonClicked, this, 1, 0, 1, 1);
            //  }
        };
        LayerOneStation.prototype.handCook = function () {
            console.log("hand cook");
            // this.startCooking(10);
            // this._parent.playerView.movePlayer(new Phaser.Point(190, 172), 2);
        };
        LayerOneStation.prototype.robKrem = function (button) {
            this._parent.playerView.movePlayer(new Phaser.Point(190, 172), 2);
            this._selectedButtonID = parseInt(button.name);
            //    this._isMachineReady = false;
            ///   this.startCooking(10);
        };
        LayerOneStation.prototype.showCook = function () {
            var cream = new SodaJerk.Cream(this.game, this._selectedButtonID);
            this.cream.addCream(cream);
            this._handCook.addCake(this.cream);
            SodaJerk.PlayerControler.getInstance().machinMakeCream = false;
            this.cream.hand = this._handCook;
            this._hasCake = false;
            console.log("add cream");
        };
        LayerOneStation.prototype.addCake = function (food) {
            if (!this._hasCake && !food._hasCream) {
                console.log("czemukutasie dodajesz kurwa ciasto");
                this.cream = food;
                this.cream.setPositionAndDirection(new Phaser.Point(190, 172), 0);
                console.log(this.cream);
                this.startCooking(3);
                this._hasCake = true;
                return true;
            }
            return false;
        };
        return LayerOneStation;
    })(SodaJerk.CookingStation);
    SodaJerk.LayerOneStation = LayerOneStation;
})(SodaJerk || (SodaJerk = {}));
var SodaJerk;
(function (SodaJerk) {
    var PlayerView = (function (_super) {
        __extends(PlayerView, _super);
        function PlayerView(game, parent) {
            _super.call(this, game);
            this._isTrash = false;
            this._lastPosMove = new Phaser.Point(400, 300);
            this._lastPos = -1;
            this._giveToCustomer = false;
            this._cusomerShowMenu = false;
            this._mainGame = parent;
            this._player = new Phaser.Sprite(this.game, 400, 300, 'player', 0);
            this.add(this._player);
            this._player.animations.add('up', this.countFrames(90, 97), 24, true);
            this._player.animations.add('leftup', this.countFrames(110, 117), 24, true);
            this._player.animations.add('left', this.countFrames(131, 137), 24, true);
            this._player.animations.add('leftdown', this.countFrames(149, 156), 24, true);
            this._player.animations.add('right', this.countFrames(50, 57), 24, true);
            this._player.animations.add('rightup', this.countFrames(70, 77), 24, true);
            this._player.animations.add('down', this.countFrames(10, 17), 24, true);
            this._player.animations.add('rightdown', this.countFrames(30, 37), 24, true);
            //this.playAnim(0);
            this.initPlayer();
        }
        PlayerView.prototype.initPlayer = function () {
        };
        PlayerView.prototype.addFood = function (food) {
        };
        PlayerView.prototype.playerReset = function () {
            if (this._cake != null) {
                this._player.removeChild(this._cake);
                this._cake = null;
            }
            SodaJerk.PlayerControler.getInstance().playerHasCake = false;
            SodaJerk.PlayerControler.getInstance().machinMakeCream = false;
            this._lastPosMove = new Phaser.Point(400, 300);
            this._player.position.set(this._lastPosMove.x, this._lastPosMove.y);
        };
        PlayerView.prototype.movePlayer = function (point, position, cake, hand, customer_, customerShowMenu_) {
            if (cake === void 0) { cake = null; }
            if (hand === void 0) { hand = null; }
            if (customer_ === void 0) { customer_ = null; }
            if (customerShowMenu_ === void 0) { customerShowMenu_ = false; }
            var przepusc = ((this._lastPosMove.x != point.x && this._lastPosMove.y != point.y) || (this._lastPos != position) && cake != null) ? true : false;
            console.log("kurwaa max", this._lastPos, position);
            /* if (!przepusc && position == 2) {
 
                 przepusc = true;
             }
             else if (cake != null && position == -2 && !this._haveCake && this._lastPosMove.x == point.x && this._lastPosMove.y == point.y) {
 
                 this._cake = cake;
                 this._haveCake = true;
                 PlayerControler.getInstance().playerHasCake = true;
                 this._player.addChild(this._cake);
 
             }*/
            //console.log("dodaje cos kurwa", point, position, cake, przepusc);
            if (this._tween == null) {
                console.log("mam ciasto", SodaJerk.PlayerControler.getInstance().playerHasCake, this._cake == null);
                console.log("kat ", Phaser.Math.radToDeg(Phaser.Math.angleBetweenPoints(this._lastPosMove, point)));
                var kat = Phaser.Math.radToDeg(Phaser.Math.angleBetweenPoints(this._lastPosMove, point));
                this._lastPos = position;
                if (customerShowMenu_) {
                    this._cusomerShowMenu = customerShowMenu_;
                    this._customer = customer_;
                }
                if (position == -3) {
                    this._customer = customer_;
                    this._giveToCustomer = true;
                }
                else {
                    if (cake && !SodaJerk.PlayerControler.getInstance().playerHasCake) {
                        this._cake = cake;
                        SodaJerk.PlayerControler.getInstance().playerHasCake = true;
                    }
                    else {
                    }
                    if (position == 0) {
                        this._isTrash = true;
                    }
                    else {
                        this._isTrash = false;
                    }
                    if (SodaJerk.PlayerControler.getInstance().playerHasCake && position == 2 && !SodaJerk.PlayerControler.getInstance().machinMakeCream) {
                        this._isCream = true;
                        SodaJerk.PlayerControler.getInstance().machinMakeCream = true;
                    }
                    else {
                        this._isCream = false;
                        SodaJerk.PlayerControler.getInstance().machinMakeCream = false;
                    }
                }
                if ((this._lastPosMove.x != point.x && point.y != this._lastPosMove.y)) {
                    this.playAnim(kat);
                    var duration = (this.game.physics.arcade.distanceToXY(this._player, point.x, point.y) / 200) * 1000;
                    this._tween = this.game.add.tween(this._player).to({ x: point.x, y: point.y }, duration, Phaser.Easing.Linear.None).start();
                    this._tween.onComplete.add(this.endMove, this);
                    this._lastPosMove = point;
                    return true;
                }
                else {
                    this.endMove();
                }
            }
        };
        PlayerView.prototype.endMove = function () {
            this._tween = null;
            console.log("koniec ruchu", SodaJerk.PlayerControler.getInstance().playerHasCake, this._isTrash, this._isCream);
            if (this._giveToCustomer) {
                if (this._cake.arrayAnswer.length > 1) {
                    console.log("sprawdzam", this._cake.arrayAnswer);
                    this._customer.checkAnswer(this._cake.arrayAnswer);
                    this._player.removeChild(this._cake);
                    SodaJerk.PlayerControler.getInstance().playerHasCake = false;
                    this._cake.destroy(true);
                    this._cake = null;
                }
                this._customer = null;
                this._giveToCustomer = false;
            }
            else if (this._cusomerShowMenu) {
                this._customer.showMenu();
                this._cusomerShowMenu = false;
            }
            else {
                if (SodaJerk.PlayerControler.getInstance().playerHasCake) {
                    this._player.addChild(this._cake);
                }
                if (this._isTrash) {
                    if (SodaJerk.PlayerControler.getInstance().playerHasCake) {
                        this._player.removeChild(this._cake);
                        SodaJerk.PlayerControler.getInstance().playerHasCake = false;
                        this._cake.destroy(true);
                        this._cake = null;
                    }
                }
                if (SodaJerk.PlayerControler.getInstance().playerHasCake && this._isCream) {
                    if (this._mainGame.machinCream.addCake(this._cake)) {
                        SodaJerk.PlayerControler.getInstance().machinMakeCream = true;
                        this._player.removeChild(this._cake);
                        this._cake = null;
                        SodaJerk.PlayerControler.getInstance().playerHasCake = false;
                    }
                }
            }
            this.playerStop();
        };
        PlayerView.prototype.playerStop = function () {
            this._player.animations.stop();
            //  this._player.frame = 1;
        };
        PlayerView.prototype.playAnim = function (kat) {
            console.log("kat", kat);
            kat = Math.floor(kat);
            if (kat < 0) {
                if (kat < 0 && kat > -45) {
                    this._player.animations.play('rightup', 24, true);
                }
                else if (kat < -135 && kat > -180) {
                    this._player.animations.play('leftup', 24, true);
                }
                else {
                    this._player.animations.play('up', 24, true);
                }
            }
            else {
                if (kat > 136 && kat < 180) {
                    this._player.animations.play('leftdown', 24, true);
                }
                else if (kat > 0 && kat < 45) {
                    this._player.animations.play('rightdown', 24, true);
                }
                else {
                    this._player.animations.play('down', 24, true);
                }
            }
            /*
            if (kat > 0 && kat < 45) {
                this._player.animations.play('downright', 24, true);

            }
            else if (kat > 45 && kat < 90) {
                this._player.animations.play('down', 24, true);

            }
            else if (kat > 90 && kat < 135) {
                this._player.animations.play('up', 24, true);

            }
            else if (kat > 136 && kat < 180) {
                this._player.animations.play('leftup', 24, true);

            }
            else if (kat > 180 && kat < 225) {
                this._player.animations.play('left', 24, true);

            }
            else if (kat > 225 && kat < 270) {
                this._player.animations.play('leftdown', 24, true);

            }
            else if (kat > 270 && kat < 315) {
                this._player.animations.play('down', 24, true);

            }
            else if (kat > 315 && kat < 360) {
                this._player.animations.play('downright', 24, true);

            }
            else if (1 == 1) {

                this._player.animations.stop();
            }

         
            */
        };
        PlayerView.prototype.countFrames = function (startFrame, endFrame) {
            var countArr = new Array();
            for (var i = startFrame; i < endFrame + 1; i++) {
                countArr.push(i);
            }
            return countArr;
        };
        return PlayerView;
    })(Phaser.Group);
    SodaJerk.PlayerView = PlayerView;
})(SodaJerk || (SodaJerk = {}));
var SodaJerk;
(function (SodaJerk) {
    var PointPanelView = (function (_super) {
        __extends(PointPanelView, _super);
        function PointPanelView(game, parent) {
            _super.call(this, game, 0, 0, "panelPoints", 0);
            this._count = 0;
            this.frame = 3;
            this.addText();
        }
        PointPanelView.prototype.addText = function () {
            this.text = "0/15";
            var style = { font: "bold 22px Arial", fill: "#FFFFFF", align: "center" };
            this.textDescription = new Phaser.Text(this.game, 0, 0, this.text, style);
            this.addChild(this.textDescription);
            this.textDescription.x = this.width / 2 - this.textDescription.width / 2;
            this.textDescription.y = 5;
        };
        PointPanelView.prototype.changeScore = function (num) {
            if (num === void 0) { num = 1; }
            console.log("dodaje punkt", num);
            this._count += num;
            this.frame = this._count;
            this.textDescription.text = "" + this._count + "/15";
        };
        PointPanelView.prototype.checkSccore = function () {
            if (this._count == 15)
                return true;
            return false;
        };
        return PointPanelView;
    })(Phaser.Sprite);
    SodaJerk.PointPanelView = PointPanelView;
})(SodaJerk || (SodaJerk = {}));
var SodaJerk;
(function (SodaJerk) {
    var PopUp = (function (_super) {
        __extends(PopUp, _super);
        function PopUp(game, level, x, y) {
            _super.call(this, game, x, y);
            this.level1 = level;
            this._game = game;
            var bgPopup = new Phaser.Sprite(this.game, 0, 0, "popup");
            this.addChild(bgPopup);
            this.logo = new Phaser.Sprite(this.game, 0, 0, "logoGame");
            this.addChild(this.logo);
            this.logo.x = (bgPopup.width / 2 - this.logo.width / 2);
            this.logo.y = 70;
            var text = "Good try.\n Keep playing to win credits";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this.game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;
            textDescription.x = 400 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 140;
            this.addChild(textDescription);
            this.textLevel();
            //this.beatText();
            this.addButtons();
            this.visible = false;
        }
        PopUp.prototype.addButtons = function () {
            this.gamesBtn = new Phaser.Button(this._game, 0, 0, "games", this.gamesBtnClick, this, 1, 0);
            this.playBtn = new Phaser.Button(this._game, 100, 0, "playGame", this.playBtnClick, this, 1, 0);
            this.gamesBtn.inputEnabled = true;
            this.gamesBtn.input.priorityID = 10;
            this.playBtn.inputEnabled = true;
            this.playBtn.input.priorityID = 10;
            var buttonsGr = new Phaser.Group(this._game);
            this.addChild(buttonsGr);
            buttonsGr.add(this.gamesBtn);
            buttonsGr.add(this.playBtn);
            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;
        };
        PopUp.prototype.activeButton = function () {
            this.gamesBtn.inputEnabled = true;
            this.playBtn.inputEnabled = true;
        };
        PopUp.prototype.gamesBtnClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        PopUp.prototype.playBtnClick = function () {
            this.visible = false;
            this.gamesBtn.inputEnabled = false;
            //   this.gamesBtn.input.priorityID = 3;
            this.playBtn.inputEnabled = false;
            // this.playBtn.input.priorityID = 3;
            this.level1.playAgain();
            //  this.game.state.start("PopupStart", true, false);
        };
        PopUp.prototype.textLevel = function () {
            var text = "GAME OVER";
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this._game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = 400 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 25;
            this.addChild(textDescription);
        };
        return PopUp;
    })(Phaser.Sprite);
    SodaJerk.PopUp = PopUp;
})(SodaJerk || (SodaJerk = {}));
function launchGame(result) {
    userLevel = result.level;
    if (userLevel >= 5) {
        userLevel = 5;
        APIsetLevel(userID, gameID, userLevel);
    }
}
//declare function launchGame(result);
var SodaJerk;
(function (SodaJerk) {
    var PopupStart = (function (_super) {
        __extends(PopupStart, _super);
        function PopupStart() {
            _super.apply(this, arguments);
        }
        PopupStart.prototype.create = function () {
            var background = this.add.sprite(0, 0, 'mainMenuBg');
            var bgPopup = this.add.sprite(0, 0, "popup");
            this.logo = this.add.sprite(0, 0, "logoGame");
            this.logo.x = (bgPopup.width / 2 - this.logo.width / 2);
            this.logo.y = 70;
            var text = "It’s your job to serve the hungry soda shop customers!";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;
            textDescription.x = 400 - textDescription.wordWrapWidth / 2 + 15;
            textDescription.y = this.logo.y + this.logo.height + 145;
            this.textLevel();
            //this.beatText();
            this.addButtons();
        };
        PopupStart.prototype.addButtons = function () {
            var gamesBtn = this.add.button(0, 0, "games", this.gamesBtnClick, this, 1, 0);
            var playBtn = this.add.button(100, 0, "playGame", this.playBtnClick, this, 1, 0);
            var buttonsGr = this.add.group();
            buttonsGr.add(gamesBtn);
            buttonsGr.add(playBtn);
            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;
        };
        PopupStart.prototype.gamesBtnClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        PopupStart.prototype.playBtnClick = function () {
            this.game.state.start("Instruction", true, false);
        };
        PopupStart.prototype.beatText = function () {
            var text = "BEAT THIS LEVEL";
            //" 6";
            var style = { font: "bold 13px Arial", fill: "#660000", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = (this.game.width / 2) + (140) - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 100;
            var numberText = "6";
            var styleNumber = { font: "bold 17px Arial", fill: "#990000", align: "center" };
            var numberShow = this.add.text(0, 0, numberText, styleNumber);
            numberShow.x = textDescription.x + textDescription.width / 2 - numberShow.width / 2;
            numberShow.y = textDescription.y + textDescription.height;
            var tunesText = "MORE TUNES TO WIN";
            var tunesShow = this.add.text(0, 0, tunesText, style);
            tunesShow.wordWrap = true;
            tunesShow.wordWrapWidth = 100;
            tunesShow.x = textDescription.x + textDescription.width / 2 - tunesShow.width / 2;
            tunesShow.y = numberShow.y + numberShow.height;
            var creditsNmText = "25";
            var styleCredits = { font: "bold 17px Arial", fill: "#ff0000", align: "center" };
            var credistNmShow = this.add.text(0, 0, creditsNmText, styleCredits);
            credistNmShow.x = tunesShow.x + tunesShow.width / 2 - credistNmShow.width / 2;
            credistNmShow.y = tunesShow.y + tunesShow.height;
            var creditsText = "CREDITS";
            var credistShow = this.add.text(0, 0, creditsText, style);
            credistShow.x = tunesShow.x + tunesShow.width / 2 - credistShow.width / 2;
            credistShow.y = credistNmShow.y + credistNmShow.height;
        };
        PopupStart.prototype.textCredits = function () {
            var text = "Beat this level";
            //" 6";
            var style = { font: "bold 14px Arial", fill: "#660000", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = (this.game.width / 2) + 257 / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 100;
            var numberText = "6";
            var styleNumber = { font: "bold 18px Arial", fill: "#990000", align: "center" };
            var numberShow = this.add.text(0, 0, numberText, styleNumber);
            numberShow.x = textDescription.x + textDescription.width / 2 - numberShow.width / 2;
            numberShow.y = textDescription.y + textDescription.height;
        };
        PopupStart.prototype.textLevel = function () {
            //APIgetLevel(userID, gameID);
            var text = "LEVEL " + userLevel;
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = this.game.stage.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 25;
        };
        return PopupStart;
    })(Phaser.State);
    SodaJerk.PopupStart = PopupStart;
})(SodaJerk || (SodaJerk = {}));
var SodaJerk;
(function (SodaJerk) {
    var PopUpWin = (function (_super) {
        __extends(PopUpWin, _super);
        function PopUpWin(game, level, x, y) {
            _super.call(this, game, x, y);
            this._game = game;
            this.level1 = level;
            var bgPopup = new Phaser.Sprite(this.game, 0, 0, "popup");
            this.addChild(bgPopup);
            this.logo = new Phaser.Sprite(this.game, 0, 0, "logoGame");
            this.addChild(this.logo);
            this.logo.x = (400 - this.logo.width / 2);
            this.logo.y = 70;
            var text = "You finish the level.\n Keep Playing to win credits!";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this.game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;
            textDescription.x = 400 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 140;
            this.addChild(textDescription);
            this.textLevel();
            //this.beatText();
            this.addButtons();
            this.visible = false;
        }
        PopUpWin.prototype.addButtons = function () {
            this.gamesBtn = new Phaser.Button(this._game, 0, 0, "games", this.gamesBtnClick, this, 1, 0);
            this.playBtn = new Phaser.Button(this._game, 100, 0, "playGame", this.playBtnClick, this, 1, 0);
            this.gamesBtn.inputEnabled = true;
            this.gamesBtn.input.priorityID = 10;
            this.playBtn.inputEnabled = true;
            this.playBtn.input.priorityID = 10;
            var buttonsGr = new Phaser.Group(this._game);
            this.addChild(buttonsGr);
            buttonsGr.add(this.gamesBtn);
            buttonsGr.add(this.playBtn);
            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;
        };
        PopUpWin.prototype.activeButton = function () {
            this.gamesBtn.inputEnabled = true;
            this.playBtn.inputEnabled = true;
        };
        PopUpWin.prototype.gamesBtnClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        PopUpWin.prototype.playBtnClick = function () {
            this.visible = false;
            this.gamesBtn.inputEnabled = false;
            this.gamesBtn.input.priorityID = 3;
            this.playBtn.inputEnabled = false;
            this.playBtn.input.priorityID = 3;
            this.level1.playAgain();
            //  this.game.state.start("PopupStart", true, false);
        };
        PopUpWin.prototype.textLevel = function () {
            var text = "GOOD JOB";
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this._game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = 400 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 25;
            this.addChild(textDescription);
        };
        return PopUpWin;
    })(Phaser.Sprite);
    SodaJerk.PopUpWin = PopUpWin;
})(SodaJerk || (SodaJerk = {}));
//# sourceMappingURL=game.js.map