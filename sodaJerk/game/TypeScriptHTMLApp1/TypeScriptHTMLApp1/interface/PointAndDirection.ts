﻿module SodaJerk {

    interface PointAndDirection {
        direction: number;
        positionForPlayer: Phaser.Point;

    }

}