﻿module SodaJerk {

    export class Boot extends Phaser.State {

        preload() {


            this.load.image("bg", "assets/game/splashScreen.png");
            this.load.image("progressBarBlack", "assets/commons/ProgressBar.png");
            this.load.image("progressBarBlue", "assets/commons/ProgressBarBlue.png");

        }

        create() {

            this.game.state.start("Preloader", true, false);

        }

    }



} 