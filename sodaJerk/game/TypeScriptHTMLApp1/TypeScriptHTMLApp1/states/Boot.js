var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var SodaJerk;
(function (SodaJerk) {
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            _super.apply(this, arguments);
        }
        Boot.prototype.preload = function () {
            this.load.image("bg", "assets/game/splashScreen.png");
            this.load.image("progressBarBlack", "assets/commons/ProgressBar.png");
            this.load.image("progressBarBlue", "assets/commons/ProgressBarBlue.png");
        };
        Boot.prototype.create = function () {
            this.game.state.start("Preloader", true, false);
        };
        return Boot;
    })(Phaser.State);
    SodaJerk.Boot = Boot;
})(SodaJerk || (SodaJerk = {}));
//# sourceMappingURL=Boot.js.map