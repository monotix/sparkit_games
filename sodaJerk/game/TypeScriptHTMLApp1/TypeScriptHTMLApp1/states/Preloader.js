var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var SodaJerk;
(function (SodaJerk) {
    var Preloader = (function (_super) {
        __extends(Preloader, _super);
        function Preloader() {
            _super.apply(this, arguments);
        }
        Preloader.prototype.preload = function () {
            var bg = this.add.image(0, 0, "bg", 0);
            var blackProgress = this.add.image(0, 0, "progressBarBlack", 0);
            var blueProgress = this.add.sprite(0, 0, "progressBarBlue", 0);
            blackProgress.position.setTo(this.stage.width / 2 - blackProgress.width / 2, this.stage.height - blackProgress.height - 10);
            blueProgress.position = blackProgress.position;
            this.load.setPreloadSprite(blueProgress);
            this.load.image("InstructionsScreen", "assets/InstructionsScreen.png");
            this.load.image("mainMenuBg", "assets/game/splashScreen.png");
            this.load.atlasJSONArray('buttons', 'assets/commons/mainMenuButtons.png', 'assets/commons/mainMenuButtons.json');
            this.load.image('buttonsBg', 'assets/commons/naviMenuBackground.png');
            this.load.audio("MainMenuSn", "assets/game/SodaJerkTheme.mp3");
            this.load.audio("overButtonA", "assets/commons/overButton.mp3");
            this.load.audio("clearButtonA", "assets/commons/clearButton.mp3");
        };
        Preloader.prototype.create = function () {
            this.game.state.start("MainMenu", true, false);
        };
        return Preloader;
    })(Phaser.State);
    SodaJerk.Preloader = Preloader;
})(SodaJerk || (SodaJerk = {}));
//# sourceMappingURL=Preloader.js.map