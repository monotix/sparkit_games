﻿declare function APIsetLevel(userID, gameID, userLevel);
declare function APIgetLevel(userID, gameID);
declare var userID: any;
declare var gameID: any;
module SodaJerk {
    export class MainGame extends Phaser.State {

        private _cookingStation: CookingStation;
        //layers
        private _customersLayer: Phaser.Group;
        public playerView: PlayerView;
        public machinCream;
        public panelPoint: PointPanelView;
        private _customerArray: Array<CustomerView> = new Array();
        private _customerTimeCounter = 10;
        public level: number = 0;
        private _maxCustomer: number = 0;
        private screenWinOrGameOver;
        private _backBg: BackwardView;
        create() {

            this.level = userLevel - 1;
            this.checkLevel();
            this.initGameData();
            //game background
            this.playerView = new PlayerView(this.game, this);
          
            var bg: Phaser.Sprite = this.game.add.sprite(0, 0, 'gameBg');
            //back bg
         
            this._backBg = new BackwardView(this.game, this);
            this._backBg.showSceneByID(0);
            this._backBg.position.setTo(-4, -122);
            //front bg
          
            //cooking station
            this.initCookingStations(547, 333, 0, 92);
            var frontBg: Phaser.Sprite = this.game.add.sprite(-2, 284, 'frontBg');
            
            this.game.stage.addChildAt(this.playerView, 1);
            this.game.stage.addChildAt(frontBg, 2);
          
            this._customersLayer = this.game.add.group(this, 'customers', true);

            this.panelPoint = new PointPanelView(this.game, this);
            this.game.stage.addChild(this.panelPoint);

            this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);

            this.showFirstCustomer();
        }

        private showFirstCustomer(): void {
            //pokazuje pierwszego
            var randomCustomer = this.game.rnd.integerInRange(0, 3);
            this.showCustomer(String(randomCustomer));
        }

        public playAgain() {

            console.log("playAgain");
            this.checkLevel();
    
            
            this._counter = 0;
            this.playerView.playerReset();

            this._cookingStation.resetState();
            this._backBg.layetonestation.resetState();

            this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);


            this.showFirstCustomer();
        }

        checkLevel() {

            if (this.level == 0) {
                this._maxCustomer = 7;
            }
            else if (this.level == 2) {
                this._maxCustomer = 10;
            }
            else if (this.level == 3) {
                this._maxCustomer = 13;
            }

        }


        endGame() {
            
            this.removeAllCustomer();
            if (this.panelPoint.checkSccore()) {
                //you win
                this.youwin();
                if (this.level < 4) {
                    this.level++;
                    APIsetLevel(userID, gameID, userLevel+this.level+1);
                }
                this.checkLevel();

            }

            else {

                // you lose
                this.youLose();
            }


        }


        youwin() {
         
            this.screenWinOrGameOver = new PopUpWin(this.game, this, 0, 0);
            this.addTostage();
        }

        youLose() {
         
            this.screenWinOrGameOver = new PopUp(this.game, this, 0, 0);
            this.addTostage();
        }

        addTostage() {
            this.game.stage.addChild(this.screenWinOrGameOver);
            this.screenWinOrGameOver.activeButton();
            this.screenWinOrGameOver.visible = true;
        }

        private _counter: number = 0;

        updateTimer() {

            if (this._maxCustomer < 0) {
               // console.log("end game timer");
                this.game.time.events.removeAll();
                this.endGame();
                return;
            }
            var randomCustomer = this.game.rnd.integerInRange(0, 3);
            if (this._counter == this._customerTimeCounter && this._customerArray.length < 4 && this._maxCustomer > 0) {
               // console.log("nowy customer", this._maxCustomer);
                this.showCustomer(String(randomCustomer));
                this._counter = 0;
              
            }

            this._counter++;
           
           // console.log("time " + this._counter);
        }
       

        private initGameData(): void {
        }


        private initCookingStations(xpos: number, ypos:number,stationID:number, maxFrames:number): void {
            this._cookingStation = new CookingStation(this.game, this,stationID,maxFrames);
            this._cookingStation.position.setTo(xpos, ypos);
            this.game.stage.addChildAt(this._cookingStation, 2);
        }

        private showCustomer(customerID: string): void {
            var c: CustomerView = new CustomerView(this.game, this, customerID, 3-this._customerArray.length);
            this._customerArray.push(c);
            this._customersLayer.addChild(c);
            c.startOrder(5);
        }

        removeAllCustomer() {

           

            for (var i: number = 0; i < this._customerArray.length; i++) {

              
                
                //this._customerArray[i] = null;
                this._customerArray.splice(i, 1);



            }


            this._customersLayer.removeAll(true)


        }


        public removeCustomer(c:CustomerView) {

            for (var i: number = 0; i < this._customerArray.length; i++) {

                if (this._customerArray[i] == c) {
                    
                    //this._customerArray[i] = null;
                    this._customerArray.splice(i, 1);
                    this._maxCustomer--;
                }

            }

            for (var i: number = 0; i < this._customerArray.length; i++) {
                this._customerArray[i].changePosition(3 - i);
                
            }
        }

        public movePlayer(point: Phaser.Point,cake=null) {

           this.playerView.movePlayer(point,cake);

        }

        //Delegates
        //Cooking machines
        //delegate when machine end cooking.
        public takeCook(cake): void {
            console.log("bierz ciasto");
            this.playerView.addFood(cake);
        }

    }

} 