var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Robo;
(function (Robo) {
    var Instruction = (function (_super) {
        __extends(Instruction, _super);
        function Instruction() {
            _super.apply(this, arguments);
        }
        Instruction.prototype.create = function () {
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var background = this.add.sprite(0, 0, 'InstructionsScreen');
            this._buttons = this.add.group();
            var buttonsBg = this.add.sprite(0, 0, 'buttonsBg');
            this._buttons.addChild(buttonsBg);
            var gameButton = this.add.button(14, 36, 'buttons', this.onGameClick, this, 0, 0, 1);
            this._buttons.addChild(gameButton);
            var playButton = this.add.button(61, gameButton.y, 'buttons', this.onPlayClick, this, 3, 2);
            playButton.onInputDown.add(this.overPlay2, this);
            //playButton.x = this._buttons.width - playButton.width - 10;
            this._buttons.addChild(playButton);
            this._buttons.x = 0;
            this._buttons.y = this.game.stage.height - this._buttons.height;
        };
        Instruction.prototype.onGameClick = function () {
        };
        Instruction.prototype.overPlay2 = function () {
            this.overButtonA.play();
        };
        Instruction.prototype.onPlayClick = function () {
            // this.game.state.start("Level", true, false);
        };
        return Instruction;
    })(Phaser.State);
    Robo.Instruction = Instruction;
})(Robo || (Robo = {}));
//# sourceMappingURL=Instruction.js.map