﻿module SodaJerk {

    export class Preloader extends Phaser.State {


        preload() {
          
            var bg = this.add.image(0, 0, "bg", 0);

            var blackProgress = this.add.image(0, 0, "progressBarBlack", 0);
            var blueProgress = this.add.sprite(0, 0, "progressBarBlue", 0);


            blackProgress.position.setTo(this.stage.width / 2 - blackProgress.width / 2, this.stage.height - blackProgress.height - 10);

            blueProgress.position = blackProgress.position;
            this.load.setPreloadSprite(blueProgress);

            //this.load.image("InstructionsScreen", "assets/game/instructionsScreen.png");


            mobile = false;
            if (!this.game.device.desktop) {
                mobile = true;
            }


            if (mobile) {
                this.load.image("InstructionsScreen", "assets/popup/03_soda_jerk.png");
                this.load.audio("instructionSn", "assets/audio/soda_jerk_02.mp3");

            }
            else {
                this.load.image("InstructionsScreen", "assets/popup/03_soda_jerk.png");
                this.load.audio("instructionSn", "assets/audio/soda_jerk_01.mp3");
            }

            this.load.image("popup", "assets/popup/popup.png");
            this.load.image("logoGame", "assets/popup/logo.png");
            this.load.atlasJSONArray('games', 'assets/popup/games.png', 'assets/popup/games.json');
            this.load.atlasJSONArray('playGame', 'assets/popup/play.png', 'assets/popup/play.json');
            this.load.atlasJSONArray('helpBtn', 'assets/popup/help.png', 'assets/popup/help.json');     


            this.load.image("mainMenuBg", "assets/game/splashScreen.png");
            this.load.atlasJSONArray('buttons', 'assets/commons/mainMenuButtons.png', 'assets/commons/mainMenuButtons.json');
            this.load.image('buttonsBg', 'assets/commons/naviMenuBackground.png');
            this.load.audio("MainMenuSn", "assets/game/SodaJerkTheme.mp3");
            this.load.audio("overButtonA", "assets/commons/overButton.mp3");
            this.load.audio("clearButtonA", "assets/commons/clearButton.mp3");

            
            this.load.audio("customerShowAudio", "assets/game/customerShow.wav");
            this.load.audio("customerHideAudio", "assets/game/customerHide.wav");
            this.load.audio("customerHideWrongAudio", "assets/game/customerHideWrong.wav");
            this.load.audio("stationCookWorkEnd", "assets/game/stationCookWorkEnd.wav");
            this.load.audio("stationCookWork0", "assets/game/stationCookWork0.wav");
            this.load.audio("stationCookWork1", "assets/game/stationCookWork1.wav");

            this.load.image("gameBg", "assets/game/gameBg.png");
            this.load.image("frontBg", "assets/game/front.png");
            this.load.atlasJSONArray('station0', 'assets/game/station1.png', 'assets/game/station1.json');
            this.load.atlasJSONArray('station1', 'assets/game/station2.png', 'assets/game/station2.json');
            this.load.atlasJSONArray('timer', 'assets/game/timer.png', 'assets/game/timer.json');

            this.load.image("transparent", "assets/game/50x50.png");

            this.load.atlasJSONArray('wajcha', 'assets/game/s1options0Button.png', 'assets/game/s1options0Button.json');
            this.load.atlasJSONArray('wajcha2', 'assets/game/s1options1Button.png', 'assets/game/s1options1Button.json');
            this.load.atlasJSONArray('wajcha3', 'assets/game/s1options2Button.png', 'assets/game/s1options2Button.json');
            this.load.atlasJSONArray('wajcha4', 'assets/game/s1options3Button.png', 'assets/game/s1options3Button.json');

            this.load.atlasJSONArray('ciasto', 'assets/game/s0option0Button.png', 'assets/game/s0option0Button.json');
            this.load.atlasJSONArray('ciasto2', 'assets/game/s0option1Button.png', 'assets/game/s0option1Button.json');
            this.load.atlasJSONArray('ciasto3', 'assets/game/s0option2Button.png', 'assets/game/s0option2Button.json');
            this.load.atlasJSONArray('ciasto4', 'assets/game/s0option3Button.png', 'assets/game/s0option3Button.json');

            this.load.atlasJSONArray('trash', 'assets/game/trash.png', 'assets/game/trash.json');

            this.load.atlasJSONArray('panelPoints', 'assets/game/gui2.png', 'assets/game/gui2.json');

        

            this.load.atlasJSONArray('player', 'assets/game/player.png', 'assets/game/player.json');

            this.load.image("orderBg", "assets/game/order.png");

            this.load.image("s0", "assets/game/s0_1.png");
            this.load.image("s1", "assets/game/s0_2.png");
            this.load.image("s2", "assets/game/s0_3.png");
            this.load.image("s3", "assets/game/s1_3.png");
            this.load.image("s4", "assets/game/s1_4.png");
            this.load.image("s5", "assets/game/s3_2.png");
            this.load.image("s6", "assets/game/s3_3.png");
            this.load.image("s7", "assets/game/s4_4.png");
            this.load.image("s8", "assets/game/s4_5.png");
            this.load.image("s9", "assets/game/s5_4.png");
            this.load.image("s10", "assets/game/s5_5.png");
            this.load.image("s11", "assets/game/s0_0.png");

            for (var i: number = 0; i < 4; i++) {
                this.load.image("station0Button" + i, "assets/game/station1_b" + i + ".png");
                this.load.image("station1Button" + i, "assets/game/station2_b" + i + ".png");
                this.load.atlasJSONArray('customer' + i, 'assets/game/customer' + i + '.png', 'assets/game/customer' + i + '.json');
                this.load.atlasJSONArray('layer'+i, 'assets/game/layer'+i+'.png', 'assets/game/layer'+i+'.json');
            }
        }


        create() {
           
            this.game.state.start("MainMenu", true, false);

        }

    }


} 

var mobile;