﻿module SodaJerk {

    export class CookView extends Phaser.Group {
        private _parent: MainGame;
        private _layersID: Array<number>;
        constructor(game: Phaser.Game, parent: MainGame) {
            super(game);
            game.add.existing(this);

            this._parent = parent;
        }

        public buildCook(layersID: Array<number>): void {
            this._layersID = layersID;

            for (var i: number = 0; i < this._layersID.length; i++) {
                var l: Phaser.Sprite = this.game.add.sprite(25, 26, 'layer' + i, this._layersID[i]);
                this.addChild(l);
            }
        }
    }
} 