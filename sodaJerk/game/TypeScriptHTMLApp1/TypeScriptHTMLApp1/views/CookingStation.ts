﻿module SodaJerk {

    export class CookingStation extends Phaser.Group {
        public _parent: MainGame;
        private _station: Phaser.Sprite;
        public _selectedButtonID: number;
        public _stationID: number;
        private _maxFrames: number;
        private _timer: Phaser.TimerEvent;
        public _cookPosition: Phaser.Point;
        private _isMachineReady: boolean = true;
        private _cake: Cake;
        private _handCake: Hand;
        public _wajchy:Array<Phaser.Button>
        private _workingAudio: Phaser.Sound;
        private _pozycja: number;  // 0 kosz, 1 robienie cistaa, 2 robienie kremu, 3 customer
        constructor(game: Phaser.Game, parent: MainGame, stationID:number, maxFrames:number) {
            super(game);
            game.add.existing(this);
            this._parent = parent;
            this._stationID = stationID;
            this._maxFrames = maxFrames;
            this.initStation();
            this.initButtons();
        }

        private initStation(): void {
            this._station = new Phaser.Sprite(this.game,0, 0, 'station'+this._stationID, 0);
            this._station.animations.add('anim', this.countFrames(1, this._maxFrames), 24, true);
            this.addChild(this._station);

            this._handCake = new Hand(this.game, this._parent, new Phaser.Point(560, 282), 2, 1);
            this._handCake.x = 50;
            this._handCake.y = 20;
           
            this.addChild(this._handCake);
         
            this._workingAudio = this.game.add.audio("stationCookWork" + this._stationID, 0.5, true);
        }

        public resetState() {

            this._isMachineReady = true;
            if (this._cake != null) this._cake = null;
            this._handCake.resetState();
            this.resetStation();
        }

        public initButtons(): void {
            /*
            var b0: Phaser.Button = this.game.add.button(3, 74, 'station'+this._stationID+'Button0', this.onButtonClicked, this, 1, 1, 1, 1);
            b0.name = '0';
       //     this.addChild(b0);
            var b1: Phaser.Button = this.game.add.button(22, 85, 'station' + this._stationID + 'Button1', this.onButtonClicked, this, 1, 1, 1, 1);
            b1.name = '1';
       //     this.addChild(b1);
            var b2: Phaser.Button = this.game.add.button(106, 13, 'station' + this._stationID + 'Button2', this.onButtonClicked, this, 1, 1, 1, 1);
            b2.name = '2';
        //    this.addChild(b2);
            var b3: Phaser.Button = this.game.add.button(126, 24, 'station' + this._stationID + 'Button3', this.onButtonClicked, this, 1, 1, 1, 1);
            b3.name = '3';
         //   this.addChild(b3);
            */

            this._cookPosition = new Phaser.Point(35, 2);
            this.dodajWajchy();
            this.dodajCiasto();
            this.smieci();
        }

        handCake() {
            console.log("hand cake");
           // this.startCooking(10);

           // this._parent.movePlayer(new Phaser.Point(580, 332), 1);
        }

     

        smieci() {
            var trash = this.game.add.button(417, 37, "trash", this.trash, this, 1, 0, 1, 1);
        }

        trash() {

            this._parent.playerView.movePlayer(new Phaser.Point(370, 67),0);
        

        }

        private  dodajWajchy() {


        }

        private dodajCiasto() {

            if (this._stationID == 0) {
                var ciasto = this.game.add.button(650, 342, "ciasto", this.onButtonClicked, this, 1, 0, 1, 1);
                ciasto.name = "2";
                this.game.stage.addChild(ciasto);
                var ciasto2 = this.game.add.button(669, 354, "ciasto2", this.onButtonClicked, this, 1, 0, 1, 1);
                ciasto2.name = "1";
                this.game.stage.addChild(ciasto2);
                var ciasto = this.game.add.button(545, 405, "ciasto3", this.onButtonClicked, this, 1, 0, 1, 1);
                ciasto.name = "0";
                this.game.stage.addChild(ciasto);
                var ciasto2 = this.game.add.button(564, 416, "ciasto4", this.onButtonClicked, this, 1, 0, 1, 1);
                ciasto2.name = "3";
                this.game.stage.addChild(ciasto2);
                // var wajcha3 = this.game.add.button(215, 80, "wajcha3", this.onButtonClicked, this, 1, 0, 1, 1);
                // var wajcha4 = this.game.add.button(235, 79, "wajcha4", this.onButtonClicked, this, 1, 0, 1, 1);
            }
        }

        public onButtonClicked(button: Phaser.Button): void {
            //console.log("machine ready",!this._isMachineReady);
            if (!this._isMachineReady) return;
            console.log(button.name);
            this._selectedButtonID = parseInt(button.name);
            this._isMachineReady = false;
            this.startCooking(2);
        }

        private countFrames(startFrame: number, endFrame: number): Array<number> {
            var countArr: Array<number> = new Array();
            for (var i: number = startFrame; i < endFrame + 1; i++) {
                countArr.push(i);
            }
            return countArr;
        }

        public startCooking(seconds: number): void {
            this._station.animations.play('anim');
            this._timer = this.game.time.events.add(seconds * 1000, this.onCookingComplete, this);
            this.hideWajchy(true);
            this._workingAudio.play("", 0, 0.5, true);
        }

        private resetStation(): void {
            this._station.animations.stop('anim', true);
            this._station.frame = 0;
            this.game.time.events.remove(this._timer);
            this._workingAudio.stop();
        }

        private onCookingComplete(): void {
            this.resetStation();
            this.showCook();
            this.hideWajchy(false);
            var completeAudio: Phaser.Sound = this.game.add.sound("stationCookWorkEnd", 1, 0);
            completeAudio.play("", 0, 1);
        }

        public showCook(): void {
            console.log("show cock w ciescie id", this._selectedButtonID);
            this._cake = new Cake(this.game, this._parent, this._selectedButtonID, new Phaser.Point(0, 0), 2);
            this._handCake.addCake(this._cake);
            this._cake.hand = this._handCake;
            this._isMachineReady = true;

            
        }

        private onCookSelect(): void {
            this._parent.playerView.movePlayer(new Phaser.Point(this._station.world.x+60, this._station.world.y+30),1, this._cake);
        }

        private hideWajchy(hide:boolean):void {
            if (this._wajchy) {
                for (var i: number = 0; i < this._wajchy.length; i++) {
                    var b: Phaser.Button = this._wajchy[i];
                    b.visible = !hide;
                }
            }
        }
        /*
        public takeCook(): void {
            this._isMachineReady = true;
        }
        */
    }
}