﻿module SodaJerk {

    export class PointPanelView extends Phaser.Sprite {
        private text;
        private textDescription:Phaser.Text;
        private _count: number = 0;
         
        constructor(game: Phaser.Game, parent: MainGame) {

            super(game, 0, 0, "panelPoints", 0);
            this.frame = 3;
            this.addText();
        }

        addText() {

            this.text = "0/15";
            var style = { font: "bold 22px Arial", fill: "#FFFFFF", align: "center" };

            this.textDescription = new Phaser.Text(this.game,0, 0, this.text, style);
            this.addChild(this.textDescription);
            this.textDescription.x = this.width / 2 - this.textDescription.width / 2;
            this.textDescription.y = 5;
        }


        public changeScore(num = 1) {
            console.log("dodaje punkt", num);
            this._count += num;
            this.frame = this._count;
            this.textDescription.text = ""+ this._count +"/15";

        }

        public checkSccore(): boolean {

            if (this._count == 15) return true;

            return false;

        }


    }


} 