﻿module SodaJerk {

    export class PlayerView extends Phaser.Group {

        private _mainGame: MainGame
        private _player: Phaser.Sprite;
        private _cake:Cake;
      
        private _isTrash: boolean = false;
        private _lastPosMove: Phaser.Point = new Phaser.Point(400, 300);
        private _lastPos: number = -1;
        private _tween;
        private _isCream;
        private _giveToCustomer: boolean = false;
        private _customer: CustomerView;
        private _cusomerShowMenu: boolean = false;
        constructor(game: Phaser.Game, parent: MainGame) {

            super(game);
            this._mainGame = parent;

            this._player = new Phaser.Sprite(this.game, 400, 300, 'player', 0);
            this.add(this._player);
            this._player.animations.add('up', this.countFrames(90, 97), 24, true);
            this._player.animations.add('leftup', this.countFrames(110, 117), 24, true);
            this._player.animations.add('left', this.countFrames(131, 137), 24, true);
            this._player.animations.add('leftdown', this.countFrames(149, 156), 24, true);
            this._player.animations.add('right', this.countFrames(50, 57), 24, true);
            this._player.animations.add('rightup', this.countFrames(70, 77), 24, true);
            this._player.animations.add('down', this.countFrames(10, 17), 24, true);
            this._player.animations.add('rightdown', this.countFrames(30, 37), 24, true);
         
            //this.playAnim(0);
            this.initPlayer();


        }

        initPlayer() {

           

            
        }


        addFood(food) {


        }


        public playerReset() {

            if (this._cake!=null){
                this._player.removeChild(this._cake);
                this._cake = null;
             }

            PlayerControler.getInstance().playerHasCake = false;
            PlayerControler.getInstance().machinMakeCream = false;

            this._lastPosMove = new Phaser.Point(400, 300);
            this._player.position.set(this._lastPosMove.x, this._lastPosMove.y);



        }

        movePlayer(point: Phaser.Point, position: number, cake: Cake = null, hand: Hand = null, customer_: CustomerView = null, customerShowMenu_: boolean = false) {
            
        
            var przepusc: boolean = ((this._lastPosMove.x != point.x && this._lastPosMove.y != point.y) || (this._lastPos != position) && cake!=null) ? true : false;
            console.log("kurwaa max", this._lastPos, position);
           /* if (!przepusc && position == 2) {

                przepusc = true;
            }
            else if (cake != null && position == -2 && !this._haveCake && this._lastPosMove.x == point.x && this._lastPosMove.y == point.y) {

                this._cake = cake;
                this._haveCake = true;
                PlayerControler.getInstance().playerHasCake = true;
                this._player.addChild(this._cake);

            }*/
            //console.log("dodaje cos kurwa", point, position, cake, przepusc);

           

            if (this._tween == null) {
                console.log("mam ciasto", PlayerControler.getInstance().playerHasCake, this._cake == null);
              
                console.log("kat ", Phaser.Math.radToDeg(Phaser.Math.angleBetweenPoints(this._lastPosMove, point)));
                var kat = Phaser.Math.radToDeg(Phaser.Math.angleBetweenPoints(this._lastPosMove, point));
                this._lastPos = position;
                if (customerShowMenu_) {
                    this._cusomerShowMenu = customerShowMenu_;
                    this._customer = customer_;
                    
                }
                if (position == -3) {
                    this._customer = customer_;
                    this._giveToCustomer = true;
                }
                else {

                    if (cake && !PlayerControler.getInstance().playerHasCake) {
                        this._cake = cake;

                        PlayerControler.getInstance().playerHasCake = true;

                    }
                    else {
                        //   this._haveCake = false;
                    }


                    if (position == 0) {

                        this._isTrash = true;
                    }
                    else {
                        this._isTrash = false;
                    }



                    if (PlayerControler.getInstance().playerHasCake && position == 2 && !PlayerControler.getInstance().machinMakeCream) {
                        this._isCream = true;

                        PlayerControler.getInstance().machinMakeCream = true;

                    }
                    else {

                        this._isCream = false;
                        PlayerControler.getInstance().machinMakeCream = false;
                    }

                }

            

                if ((this._lastPosMove.x != point.x && point.y != this._lastPosMove.y)) {

                    this.playAnim(kat);
                    var duration = (this.game.physics.arcade.distanceToXY(this._player, point.x, point.y) / 200) * 1000;


                    this._tween = this.game.add.tween(this._player).to({ x: point.x, y: point.y }, duration, Phaser.Easing.Linear.None).start();
                    this._tween.onComplete.add(this.endMove, this);
                    this._lastPosMove = point;
                    return true;
                }
                else {

                    this.endMove();

                }

            }

        }

        endMove() {
            this._tween = null;
            console.log("koniec ruchu", PlayerControler.getInstance().playerHasCake, this._isTrash, this._isCream);

            if (this._giveToCustomer) {

                if (this._cake.arrayAnswer.length > 1) {
                    console.log("sprawdzam", this._cake.arrayAnswer);
                    this._customer.checkAnswer(this._cake.arrayAnswer);
                
                    this._player.removeChild(this._cake);
                    PlayerControler.getInstance().playerHasCake = false;
                 
                    this._cake.destroy(true);
                    this._cake = null;

                }
                this._customer = null;
                this._giveToCustomer = false;
            }
            else if (this._cusomerShowMenu){

                this._customer.showMenu();
                this._cusomerShowMenu = false;

            }
            else {
                if (PlayerControler.getInstance().playerHasCake) {
                    this._player.addChild(this._cake);
                }

                if (this._isTrash) {

                    if (PlayerControler.getInstance().playerHasCake) {

                        this._player.removeChild(this._cake);
                        PlayerControler.getInstance().playerHasCake = false;

                        this._cake.destroy(true);
                        this._cake = null;

                    }


                }

                if (PlayerControler.getInstance().playerHasCake && this._isCream) {

                    if (this._mainGame.machinCream.addCake(this._cake)) {
                        PlayerControler.getInstance().machinMakeCream = true;
                        this._player.removeChild(this._cake);
                        this._cake = null;

                        PlayerControler.getInstance().playerHasCake = false;

                    }

                }
            }

            this.playerStop();
        }


        playerStop() {

            this._player.animations.stop();
          //  this._player.frame = 1;
        }


        playAnim(kat) {
            console.log("kat", kat);
            kat = Math.floor(kat);
            if (kat < 0) {


                if (kat < 0 && kat > -45) {
                    this._player.animations.play('rightup', 24, true);

                }
                else if (kat < -135 && kat > -180) {
                    this._player.animations.play('leftup', 24, true);

                }
                else {
                    this._player.animations.play('up', 24, true);
                }

            }

            else {

                if (kat > 136 && kat < 180) {
                    this._player.animations.play('leftdown', 24, true);

                }
                else if (kat > 0 && kat < 45) {
                    this._player.animations.play('rightdown', 24, true);

                }
                else {
                    this._player.animations.play('down', 24, true);
                }
            }
            

            /*
            if (kat > 0 && kat < 45) {
                this._player.animations.play('downright', 24, true);

            }
            else if (kat > 45 && kat < 90) {
                this._player.animations.play('down', 24, true);

            }
            else if (kat > 90 && kat < 135) {
                this._player.animations.play('up', 24, true);

            }
            else if (kat > 136 && kat < 180) {
                this._player.animations.play('leftup', 24, true);

            }
            else if (kat > 180 && kat < 225) {
                this._player.animations.play('left', 24, true);

            }
            else if (kat > 225 && kat < 270) {
                this._player.animations.play('leftdown', 24, true);

            }
            else if (kat > 270 && kat < 315) {
                this._player.animations.play('down', 24, true);

            }
            else if (kat > 315 && kat < 360) {
                this._player.animations.play('downright', 24, true);

            }
            else if (1 == 1) {

                this._player.animations.stop();
            }

         
            */

        }

        private countFrames(startFrame: number, endFrame: number): Array<number> {
            var countArr: Array<number> = new Array();
            for (var i: number = startFrame; i < endFrame + 1; i++) {
                countArr.push(i);
            }
            return countArr;
        }
    }


}