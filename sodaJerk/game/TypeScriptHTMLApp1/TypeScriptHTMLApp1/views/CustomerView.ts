﻿module SodaJerk {

    export class CustomerView extends Phaser.Group {
        private _parent: MainGame;
        private _customerID: string;
        private _customer: Phaser.Sprite;
        private _config: CustomerConfiguration;
        private _timer: Phaser.TimerEvent;
        private _curentPhase: number;
        private _isCorrectOrder: boolean = false;
        private _emotions: Phaser.Sprite;
        private _order: Phaser.Group;
        private _answerCorrect: Array<number>;
        private _spritToClick: Phaser.Sprite;
        private _showMenu: boolean = false;
        private _showMenuTimer: number = 0;
        private _readyForOrder: boolean = false;
        constructor(game: Phaser.Game, parent: MainGame, customerID:string, position) {
            super(game);
            game.add.existing(this);
            
            /*
            losowanie poprawnych odpowiedzi

            */
            var randomCiasto = this.game.rnd.integerInRange(0, 3);
            var randomKrem = this.game.rnd.integerInRange(0, 3);
            var randomPolewa = this.game.rnd.integerInRange(0, 3);
            var randomOwoc = this.game.rnd.integerInRange(0, 3);

            this._answerCorrect = new Array();
            this._answerCorrect.push(randomCiasto);
            this._answerCorrect.push(randomKrem);
            //
            var showAudio: Phaser.Sound = this.game.add.audio('customerShowAudio', 1, true);
            showAudio.play("", 0, 0.5, false);

            this._parent = parent;
            this._customerID = customerID;
            this._config = Customers.getInstance().getConfigByCustomerID(customerID);

            this._spritToClick = new Phaser.Sprite(this.game, this._config.placeToClick.x, this._config.placeToClick.y, "orderBg");
            this._spritToClick.alpha = 0;
            this.add(this._spritToClick);

            this.initCustomer();

            var p: Phaser.Point = this._config.startPositions[position];
            this.position.setTo(p.x, p.y);

           
        }

        public changePosition(position) {

            var p: Phaser.Point = this._config.startPositions[position];
            this.position.setTo(p.x, p.y);
        }

        private initCustomer(): void {
            

            this._customer = this.game.add.sprite(0, 0, 'customer' + this._customerID, 0);
            this._customer.animations.add('phase1', this.countFrames(this._config.phase1.x, this._config.phase1.y), 24, true);
            this._customer.animations.add('phase2', this.countFrames(this._config.phase2.x, this._config.phase2.y), 24, true);
            this._customer.animations.add('phase3', this.countFrames(this._config.phase3.x, this._config.phase3.y), 24, true);
            this._customer.animations.add('phase4', this.countFrames(this._config.phase4.x, this._config.phase4.y), 24, true);
            this._customer.animations.add('phase5', this.countFrames(this._config.phase5.x, this._config.phase5.y), 24, true);
            this._customer.animations.add('angry', this.countFrames(this._config.phaseAngry.x, this._config.phaseAngry.y), 24, true);
            this._customer.animations.add('happy', this.countFrames(this._config.phaseHappy.x, this._config.phaseHappy.y), 24, true);
            this.addChild(this._customer);

            this._emotions = this.game.add.sprite(this._config.timer.x, this._config.timer.y, 'timer', 0);
            this.addChild(this._emotions);

            this._order = this.game.add.group(this, 'order');
            var orderBg: Phaser.Sprite = this.game.add.sprite(0, 0, 'orderBg', 0);
            this._order.addChild(orderBg);
            this._order.visible = false;
            var cook: CookView = new CookView(this.game, this._parent);
            cook.buildCook(this._answerCorrect);
            this._order.addChild(cook);
            //this._order.position.setTo(this._config.order.x, this._config.order.y);
            //this.addChild(this._order);

            this._spritToClick.inputEnabled = true;
            this._spritToClick.events.onInputDown.add(this.clickCustomer, this);
        }

        private clickCustomer(sprite,pointer) {

            console.log("click customer", pointer.x, pointer.y);
            if (!this._showMenu && !this._readyForOrder) {

                this._showMenu = true;
              
                this._parent.playerView.movePlayer(new Phaser.Point(this._spritToClick.world.x + this._spritToClick.width + 40, this._spritToClick.world.y - 40), -2, null, null,this,true);
               
            }
            else {

                if (PlayerControler.getInstance().playerHasCake && this._readyForOrder) {
                    // this._parent.playerView.movePlayer(new Phaser.Point(pointer.x + 320, pointer.y-30),-3,null,null,this);
                    this._parent.playerView.movePlayer(new Phaser.Point(this._spritToClick.world.x + this._spritToClick.width + 40, this._spritToClick.world.y - 40), -3, null, null, this);
                }
                else {
                    console.log("pozycja customera", this._spritToClick.world.x, this._spritToClick.world.y);
                    this._parent.playerView.movePlayer(new Phaser.Point(this._spritToClick.world.x + this._spritToClick.width + 40, this._spritToClick.world.y - 40), -2);
                    //this._parent.playerView.movePlayer(new Phaser.Point(pointer.x + 220, pointer.y-30),-2);
                }
            }
        }

        private countFrames(startFrame: number, endFrame: number): Array<number> {
            var countArr: Array<number> = new Array();
            for (var i: number = startFrame; i < endFrame + 1; i++) {
                countArr.push(i);
            }
            return countArr;
        }

        private playPhaseAnimation(): void {
            this._customer.animations.play('phase' + this._curentPhase, 24, true);
        }

        private onFeedbackComplete(): void {
            this._customer.visible = false;
        
            this._parent.removeCustomer(this);
        }

        private hideEmotions(): void {
            this._emotions.visible = false;
            this.removeChild(this._emotions);
            this._emotions.destroy(true);
        }


        public showMenu() {
            this._customer.animations.stop('phase' + this._curentPhase,false);
            this._customer.frame = 1;

        }

        private playHappyAnimation(): void {
            console.log("gra happy");
            this._customer.animations.play('happy', 24, false, true);
            this.removeOrder();
            this._customer.animations.getAnimation('happy').onComplete.add(this.onFeedbackComplete, this);
            var showAudio: Phaser.Sound = this.game.add.audio('customerHideAudio', 1, true);
            showAudio.play("", 0, 0.5, false);
        }

        private playAngryAnimation(): void {
            console.log("gra angry");
            this._customer.animations.play('angry', 24, false, true);
            this.removeOrder();
            this._customer.animations.getAnimation('angry').onComplete.add(this.onFeedbackComplete, this);
            var showAudio: Phaser.Sound = this.game.add.audio('customerHideWrongAudio', 1, true);
            showAudio.play("", 0, 0.5, false);
        }

        removeOrder() {
            this.hideEmotions();
            this._order.removeAll();
            this.removeChild(this._order);
        }

        newCustomer() {
        }

        private playFeedback(): void {
            console.log("koncze odpowiedz", this._isCorrectOrder, this._emotions.frame);
            if (this._isCorrectOrder) {
                this._parent.panelPoint.changeScore(this._emotions.frame);
                this.playHappyAnimation();
               
            } else {
                this.playAngryAnimation();
            }
        }

        public startOrder(phaseLength: number): void {
           // this._isCorrectOrder = true;
            this._curentPhase = 1;
            this._timer = this.game.time.events.add(phaseLength*1000, this.updateTimer, this);
            this._timer.loop = true;
            this.playPhaseAnimation();
            this._emotions.frame = this._curentPhase - 1;
        }

        private _end: boolean = false;
        public updateTimer(): void {
            console.log("this._showMenu", this._showMenu);
            if (this._showMenu) {
                this._showMenuTimer++;
                console.log("menu timer", this._showMenuTimer);
                if (this._showMenuTimer > 1) {
                    this._order.position.setTo(this._config.order.x, this._config.order.y);
                    this.addChild(this._order);
                    this._showMenu = false;
                    this._readyForOrder = true;
                    this._order.visible = true;
                 //   this._curentPhase = 0;
                    this._customer.animations.play('phase' + this._curentPhase, 24, true);
                }

            }
            else if (!this._end ) {
                this._curentPhase++;
                if (this._curentPhase == 6) {
                    this.game.time.events.remove(this._timer);
                    this.playFeedback();
                } else {
                    this.playPhaseAnimation();
                    this._emotions.frame = this._curentPhase - 1;
                }
            }
        }


        public checkAnswer(answer: Array<number>) {

            console.log("sprawdzam", answer, this._answerCorrect);
            this._end = true;
            var correct: number = 0;
            for (var i: number = 0; i < this._answerCorrect.length; i++) {

                if (answer[i] == this._answerCorrect[i]) {
                   // correct++;
                }
                else {
                    this._isCorrectOrder = false;
                    this.playFeedback();
                    return;
                }
            }
         
            this._isCorrectOrder = true;
            this.playFeedback();
        }


    }
}