﻿module SodaJerk {

    export class BackwardView extends Phaser.Group {
        private _parent: MainGame;
        public layetonestation: LayerOneStation;
        constructor(game: Phaser.Game, parent: MainGame) {
            super(game);
            game.add.existing(this);

            this._parent = parent;
        }

        public showSceneByID(sceneID: number): void {
            this.forEach(this.reset, this);

            switch (sceneID) {
                case 0:
                    this.initScene0();
                    break;

                case 1:
                    this.initScene1();
                    break;

                case 2:
                    this.initScene2();
                    break;

                case 3:
                    this.initScene3();
                    break;

                case 4:
                    this.initScene4();
                    break;
            }
        }

        private reset(sprite: Phaser.Sprite): void {
            sprite.destroy();
        }

        private initScene0(): void {
            var s11: Phaser.Sprite = new Phaser.Sprite(this.game,0, 0, 's11');
            this.addChild(s11);

            var s1: Phaser.Sprite = new Phaser.Sprite(this.game,0, 0, 's1');
            this.addChild(s1);

            var s3: Phaser.Sprite = new Phaser.Sprite(this.game,0, 0, 's2');
           this.addChild(s3);

           this.layetonestation = new LayerOneStation(this.game, this._parent, 1, 32);
           this.layetonestation.position.setTo(106, 138);
           this.addChild(this.layetonestation);

           var s0: Phaser.Sprite = new Phaser.Sprite(this.game,0, 0, 's0');
            this.addChild(s0);
        }

        private initScene1(): void {
            var s11: Phaser.Sprite = this.game.add.sprite(0, 0, 's11');
            this.addChild(s11);

            var s0: Phaser.Sprite = this.game.add.sprite(0, 0, 's0');
            this.addChild(s0);

            var s1: Phaser.Sprite = this.game.add.sprite(0, 0, 's1');
            this.addChild(s1);

            var s3: Phaser.Sprite = this.game.add.sprite(0, 0, 's3');
            this.addChild(s3);

            var s4: Phaser.Sprite = this.game.add.sprite(0, 0, 's4');
            this.addChild(s4);
        }

        private initScene2(): void {
            var s11: Phaser.Sprite = this.game.add.sprite(0, 0, 's11');
            this.addChild(s11);

            var s0: Phaser.Sprite = this.game.add.sprite(0, 0, 's0');
            this.addChild(s0);

            var s1: Phaser.Sprite = this.game.add.sprite(0, 0, 's5');
            this.addChild(s1);

            var s3: Phaser.Sprite = this.game.add.sprite(0, 0, 's6');
            this.addChild(s3);

            var s5: Phaser.Sprite = this.game.add.sprite(0, 0, 's3');
            this.addChild(s5);

            var s4: Phaser.Sprite = this.game.add.sprite(0, 0, 's4');
            this.addChild(s4);
        }

        private initScene3(): void {
            var s11: Phaser.Sprite = this.game.add.sprite(0, 0, 's11');
            this.addChild(s11);

            var s0: Phaser.Sprite = this.game.add.sprite(0, 0, 's0');
            this.addChild(s0);

            var s1: Phaser.Sprite = this.game.add.sprite(0, 0, 's5');
            this.addChild(s1);

            var s3: Phaser.Sprite = this.game.add.sprite(0, 0, 's6');
            this.addChild(s3);

            var s5: Phaser.Sprite = this.game.add.sprite(0, 0, 's7');
            this.addChild(s5);

            var s4: Phaser.Sprite = this.game.add.sprite(0, 0, 's8');
            this.addChild(s4);

            var s4: Phaser.Sprite = this.game.add.sprite(0, 0, 's4');
            this.addChild(s4);
        }

        private initScene4(): void {
            var s11: Phaser.Sprite = this.game.add.sprite(0, 0, 's11');
            this.addChild(s11);

            var s0: Phaser.Sprite = this.game.add.sprite(0, 0, 's0');
            this.addChild(s0);

            var s1: Phaser.Sprite = this.game.add.sprite(0, 0, 's5');
            this.addChild(s1);

            var s3: Phaser.Sprite = this.game.add.sprite(0, 0, 's6');
            this.addChild(s3);

            var s5: Phaser.Sprite = this.game.add.sprite(0, 0, 's7');
            this.addChild(s5);

            var s4: Phaser.Sprite = this.game.add.sprite(0, 0, 's8');
            this.addChild(s4);

            var s5: Phaser.Sprite = this.game.add.sprite(0, 0, 's9');
            this.addChild(s5);

            var s6: Phaser.Sprite = this.game.add.sprite(0, 0, 's10');
            this.addChild(s6);
        }


    }
}  