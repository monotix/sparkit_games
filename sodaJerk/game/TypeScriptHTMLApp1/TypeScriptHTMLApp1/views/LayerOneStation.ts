﻿module SodaJerk {

    export class LayerOneStation extends CookingStation {

        private _handCook: Hand;
        public cream: Cake;
        private _hasCake: boolean = false;

        public initButtons(): void {
         /*
            var b0: Phaser.Button = this.game.add.button(78, 90, 'station' + this._stationID + 'Button0', this.onButtonClicked, this, 1, 1, 1, 1);
            b0.name = '0';
    //        this.addChild(b0);
            var b1: Phaser.Button = this.game.add.button(97, 80, 'station' + this._stationID + 'Button1', this.onButtonClicked, this, 1, 1, 1, 1);
            b1.name = '1';
    //        this.addChild(b1);
            var b2: Phaser.Button = this.game.add.button(116, 69, 'station' + this._stationID + 'Button2', this.onButtonClicked, this, 1, 1, 1, 1);
            b2.name = '2';
   //         this.addChild(b2);
            var b3: Phaser.Button = this.game.add.button(135, 59, 'station' + this._stationID + 'Button3', this.onButtonClicked, this, 1, 1, 1, 1);
            b3.name = '3';
       //     this.addChild(b3);
            */
        
            this._parent.machinCream = this;
            this._handCook = new Hand(this.game, this._parent, new Phaser.Point(225, 162), 0, 2);
            this._handCook.x = 140;
            this._handCook.y = 172;
            this.addChild(this._handCook);
         
          
            this._wajchy = new Array();
           // if (this._stationID == 0) {


            var wajcha = this.game.add.button(175, 102, "wajcha", this.robKrem, this, 1, 0, 1, 1);
            wajcha.name = "0";
            this._wajchy.push(wajcha);

            var wajcha2 = this.game.add.button(194, 91, "wajcha2", this.robKrem, this, 1, 0, 1, 1);
            wajcha2.name = "1";
            this._wajchy.push(wajcha2);
            var wajcha3 = this.game.add.button(213, 80, "wajcha3", this.robKrem, this, 1, 0, 1, 1);
            wajcha3.name = "2";
            this._wajchy.push(wajcha3);

            var wajcha4 = this.game.add.button(232, 69, "wajcha4", this.robKrem, this, 1, 0, 1, 1);
            wajcha4.name = "3";
            this._wajchy.push(wajcha4);
                // var wajcha3 = this.game.add.button(215, 80, "wajcha3", this.onButtonClicked, this, 1, 0, 1, 1);
                // var wajcha4 = this.game.add.button(235, 79, "wajcha4", this.onButtonClicked, this, 1, 0, 1, 1);


          //  }

        }

        handCook() {
            console.log("hand cook");
            // this.startCooking(10);
            
          // this._parent.playerView.movePlayer(new Phaser.Point(190, 172), 2);
        }

       robKrem(button: Phaser.Button): void {

            this._parent.playerView.movePlayer(new Phaser.Point(190,172),2)
         
            this._selectedButtonID = parseInt(button.name);
        //    this._isMachineReady = false;
         ///   this.startCooking(10);
        }

       public showCook(): void {
         
           var cream = new Cream(this.game, this._selectedButtonID);

           this.cream.addCream(cream);
      
           this._handCook.addCake(this.cream);
           PlayerControler.getInstance().machinMakeCream = false;
           this.cream.hand = this._handCook;
           this._hasCake = false;
           console.log("add cream");
       
       }

        addCake(food:Cake) {
          
            if (!this._hasCake && !food._hasCream) {
                console.log("czemukutasie dodajesz kurwa ciasto");
                this.cream = food;
                this.cream.setPositionAndDirection(new Phaser.Point(190, 172), 0);
                console.log(this.cream);
                this.startCooking(3);
                this._hasCake = true;
               
                return true;
            }

            return false;
        }
    }
}