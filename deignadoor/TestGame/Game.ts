﻿module DesignADoor {

    export class Game extends Phaser.Game {

        constructor() {
            super(800, 600, Phaser.CANVAS, 'content', null);
            //var rnd: number = Math.floor(Math.random() * 5) + 1
            var gameControler = GameControler.getInstance().setLevel(1);
            this.state.add('Boot', Boot, false);
            this.state.add('Preloader', Preloader, false);
            this.state.add('LevelInfoView', LevelInfoView, false);
            this.state.add('TutorialView', TutorialView, false);
            this.state.add('GameView', GameView, false);
            this.state.add('MenuView', MenuView, false);
            this.state.start('Boot');
        }

        //Navigation delegates
        public onMainMenuPlaySelect() {
            this.state.start('LevelInfoView');
        }

        public onMainMenuGamesSelect() {
        }

        public onMainMenuStartSelect() {
            this.state.start('GameView');
        }

        public onLevelStartSelect() {
            this.state.start('TutorialView');
        }

    }

} 

window.onload = () => {

    var game = new DesignADoor.Game;

};