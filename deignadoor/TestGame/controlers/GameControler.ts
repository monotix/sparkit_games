﻿module DesignADoor {

    export class GameControler {

        private static _instance: GameControler = null;

        private _level: number = 0;
        private _answers: LevelData;
        private _slotsPosition: Array<any>;
        private _gameState: string;
        private _gameAudioMusic: Phaser.Sound;
        private _levelPoints: number = 0;
        private _coins: number = 0;

        public userAnswerIsGood = true;
        public userDoorID: number = 1;
        public userLifes: number = 3;

        constructor() {
            if (GameControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            GameControler._instance = this;
        }

        public static getInstance(): GameControler {
            if (GameControler._instance === null) {
                GameControler._instance = new GameControler();
            }
            return GameControler._instance;
        }

        public setLevel(value: number): void {
            this._level = value;
            this._levelPoints = 0;
            this.userLifes = 3;
            this.levelCreator();
        }

        public addLevelPoint(): void {
            this._levelPoints ++;
            this._coins += 5;
        }

        public removeLevelPoint(): void {
            if (this._levelPoints > 0) {
                this._levelPoints--;
            }
            
            if (this._coins >= 5) {
                this._coins -= 5
            } 

            this.userLifes--;
        }

        public getCoins(): number {
            return this._coins;
        }

        public getLevelPoint(): number {
            return this._levelPoints;
        }


        public getLevel(): number {
            return this._level;
        }

        public getAnswers(): LevelData {
            return this._answers;
        }
        /*
            * game state:
            * 'drag'
            * 'rotate'
            * 'scale'
        */
        public setGameState(value: string) {
            this._gameState = value;
        }

        public getGameState(): string {
            return this._gameState;
        }
        private shuffleArray(array: Array<any>): Array<any> {
            //console.log('wjescie: ' + array.length);
            for (var j, x, i = array.length; i; j = Math.floor(Math.random() * i), x = array[--i], array[i] = array[j], array[j] = x);
            //console.log('wyjscie: ' + array.length);
            return array;
        }

        //audio
        public playGameMusic(game:Phaser.Game) {
            if (this._gameAudioMusic != undefined) {
                this._gameAudioMusic.play();
                return;
            }
            this._gameAudioMusic = game.add.audio('gameAudio', 0.6, true);
            this._gameAudioMusic.play();
        }
        public minumumVolumeGameMusic(minimum: boolean) {
            if (minimum) {
                this._gameAudioMusic.volume = 0.2;
            } else {
                this._gameAudioMusic.volume = 0.6;
            }
        }
        public stopGameMusic() {
            if (this._gameAudioMusic == undefined) return;
            this._gameAudioMusic.stop();
        }
        //Levels creator
        public refresh() {
            this._answers = new LevelData(this.positions(), this.randomIDs(this.maxColoursID()), this.randomIDs(this.maxShapesID()), this.rotations(), this.scales());
            this._answers.canColour = this.canColour();
            this._answers.canRotate = this.canRotate();
            this._answers.canScale = this.canScale();
            
            //console.log('colour:' + this._answers.canColour + ' rotate:' + this._answers.canRotate + ' scale:' + this._answers.canScale);
            //console.log(this._answers.colours + ' \n ' + this._answers.scales + ' \n ' + this._answers.rotations);
        }
        private levelCreator() {
            this._gameState = 'drag';
            this.refresh();
        }

        private maxShapesID(): number {
            return 6;
        }

        private maxColoursID(): number {
            return 6;
        }

        private canScale(): boolean {
            return this._level >= 4;

        }

        private canColour(): boolean {
            return this._level >= 3;
        }

        private canRotate(): boolean {
            return this._level >= 5;
        }

        private randomIDs(max:number): Array<number> {
            var r: Array<number> = new Array();
            for (var i: number = 1; i <= max; i++) {
                r.push(i);
            }
            return this.shuffleArray(r);
        }

        private scales(): Array<Phaser.Point> {
            var a: Array<Phaser.Point> = new Array();
            if (this.canScale()) {
                a.push(new Phaser.Point(1.5, 1.5));
                a.push(new Phaser.Point(1, 1));
                a.push(new Phaser.Point(1, 1));
                a.push(new Phaser.Point(1, 1));
                a.push(new Phaser.Point(1, 1));
                a.push(new Phaser.Point(1.5, 1.5));
            } else {
                a.push(new Phaser.Point(1, 1));
                a.push(new Phaser.Point(1, 1));
                a.push(new Phaser.Point(1, 1));
                a.push(new Phaser.Point(1, 1));
                a.push(new Phaser.Point(1, 1));
                a.push(new Phaser.Point(1, 1));
            }

            return this.shuffleArray(a);
        }

        private rotations(): Array<number> {
            var a: Array<number> = new Array();
            if (this.canRotate()) {
                a.push(0);
                a.push(0);
                a.push(90);
                a.push(90);
                a.push(0);
                a.push(-90);
                a.push(-90);
                a.push(180);
                a.push(180);
                a.push(0);
                a.push(-180);
            } else {
                a.push(0);
                a.push(0);
                a.push(0);
                a.push(0);
                a.push(0);
                a.push(0);
                a.push(0);
                a.push(0);
            }

            return this.shuffleArray(a);
        }

        private positions(): Array<Array<DShapePoint>>{
            //random slot
            var rnd: number = Math.floor(Math.random() * 5);
            if (this._slotsPosition != undefined) return this._slotsPosition[rnd];

            this._slotsPosition = new Array();

            //set1
            var set1: Array<DShapePoint> = new Array();
            set1.push(this.makePoint(380, 108));
            set1.push(this.makePoint(500, 108));
            set1.push(this.makePoint(450, 235));
            this._slotsPosition.push(set1);

            //set2
            var set2: Array<DShapePoint> = new Array();
            set2.push(this.makePoint(382, 359));
            set2.push(this.makePoint(500, 108));
            set2.push(this.makePoint(450, 235));
            this._slotsPosition.push(set2);

            //set3
            var set3: Array<DShapePoint> = new Array();
            set3.push(this.makePoint(500, 359));
            set3.push(this.makePoint(500, 108));
            set3.push(this.makePoint(450, 235));
            this._slotsPosition.push(set3);

            //set4
            var set4: Array<DShapePoint> = new Array();
            set4.push(this.makePoint(500, 359));
            set4.push(this.makePoint(382, 360));
            set4.push(this.makePoint(450, 235));
            this._slotsPosition.push(set4);

            //set5
            var set5: Array<DShapePoint> = new Array();
            set5.push(this.makePoint(380, 119));
            set5.push(this.makePoint(380, 360));
            set5.push(this.makePoint(450, 235));
            this._slotsPosition.push(set5);

            return this._slotsPosition[rnd];
        }

        private makePoint(x:number,y:number): DShapePoint {
            return new DShapePoint(x, y);
        }
    }

}  