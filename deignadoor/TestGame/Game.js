﻿var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var DesignADoor;
(function (DesignADoor) {
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            _super.apply(this, arguments);
        }
        Boot.prototype.preload = function () {
            this.load.image('mainMenuBg', 'assets/splashBackground.png');
            this.load.image('progressBar', 'assets/ProgressBar.png');
            this.load.image('progressBarBlue', 'assets/ProgressBarBlue.png');
        };

        Boot.prototype.create = function () {
            //  Unless you specifically need to support multitouch I would recommend setting this to 1
            this.input.maxPointers = 1;

            //  Phaser will automatically pause if the browser tab the game is in loses focus. You can disable that here:
            this.stage.disableVisibilityChange = true;

            if (this.game.device.desktop) {
                //  If you have any desktop specific settings, they can go in here
            } else {
                //  Same goes for mobile settings.
            }

            this.game.state.start('Preloader', true, false);
        };
        return Boot;
    })(Phaser.State);
    DesignADoor.Boot = Boot;
})(DesignADoor || (DesignADoor = {}));
var DesignADoor;
(function (DesignADoor) {
    var GameControler = (function () {
        function GameControler() {
            this._level = 0;
            this._levelPoints = 0;
            this._coins = 0;
            this.userAnswerIsGood = true;
            this.userDoorID = 1;
            this.userLifes = 3;
            if (GameControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            GameControler._instance = this;
        }
        GameControler.getInstance = function () {
            if (GameControler._instance === null) {
                GameControler._instance = new GameControler();
            }
            return GameControler._instance;
        };

        GameControler.prototype.setLevel = function (value) {
            this._level = value;
            this._levelPoints = 0;
            this.userLifes = 3;
            this.levelCreator();
        };

        GameControler.prototype.addLevelPoint = function () {
            this._levelPoints++;
            this._coins += 5;
        };

        GameControler.prototype.removeLevelPoint = function () {
            if (this._levelPoints > 0) {
                this._levelPoints--;
            }

            if (this._coins >= 5) {
                this._coins -= 5;
            }

            this.userLifes--;
        };

        GameControler.prototype.getCoins = function () {
            return this._coins;
        };

        GameControler.prototype.getLevelPoint = function () {
            return this._levelPoints;
        };

        GameControler.prototype.getLevel = function () {
            return this._level;
        };

        GameControler.prototype.getAnswers = function () {
            return this._answers;
        };

        /*
        * game state:
        * 'drag'
        * 'rotate'
        * 'scale'
        */
        GameControler.prototype.setGameState = function (value) {
            this._gameState = value;
        };

        GameControler.prototype.getGameState = function () {
            return this._gameState;
        };
        GameControler.prototype.shuffleArray = function (array) {
            for (var j, x, i = array.length; i; j = Math.floor(Math.random() * i), x = array[--i], array[i] = array[j], array[j] = x)
                ;

            //console.log('wyjscie: ' + array.length);
            return array;
        };

        //audio
        GameControler.prototype.playGameMusic = function (game) {
            if (this._gameAudioMusic != undefined) {
                this._gameAudioMusic.play();
                return;
            }
            this._gameAudioMusic = game.add.audio('gameAudio', 0.6, true);
            this._gameAudioMusic.play();
        };
        GameControler.prototype.minumumVolumeGameMusic = function (minimum) {
            if (minimum) {
                this._gameAudioMusic.volume = 0.2;
            } else {
                this._gameAudioMusic.volume = 0.6;
            }
        };
        GameControler.prototype.stopGameMusic = function () {
            if (this._gameAudioMusic == undefined)
                return;
            this._gameAudioMusic.stop();
        };

        //Levels creator
        GameControler.prototype.refresh = function () {
            this._answers = new DesignADoor.LevelData(this.positions(), this.randomIDs(this.maxColoursID()), this.randomIDs(this.maxShapesID()), this.rotations(), this.scales());
            this._answers.canColour = this.canColour();
            this._answers.canRotate = this.canRotate();
            this._answers.canScale = this.canScale();
            //console.log('colour:' + this._answers.canColour + ' rotate:' + this._answers.canRotate + ' scale:' + this._answers.canScale);
            //console.log(this._answers.colours + ' \n ' + this._answers.scales + ' \n ' + this._answers.rotations);
        };
        GameControler.prototype.levelCreator = function () {
            this._gameState = 'drag';
            this.refresh();
        };

        GameControler.prototype.maxShapesID = function () {
            return 6;
        };

        GameControler.prototype.maxColoursID = function () {
            return 6;
        };

        GameControler.prototype.canScale = function () {
            return this._level >= 4;
        };

        GameControler.prototype.canColour = function () {
            return this._level >= 3;
        };

        GameControler.prototype.canRotate = function () {
            return this._level >= 5;
        };

        GameControler.prototype.randomIDs = function (max) {
            var r = new Array();
            for (var i = 1; i <= max; i++) {
                r.push(i);
            }
            return this.shuffleArray(r);
        };

        GameControler.prototype.scales = function () {
            var a = new Array();
            if (this.canScale()) {
                a.push(new Phaser.Point(1.5, 1.5));
                a.push(new Phaser.Point(1, 1));
                a.push(new Phaser.Point(1, 1));
                a.push(new Phaser.Point(1, 1));
                a.push(new Phaser.Point(1, 1));
                a.push(new Phaser.Point(1.5, 1.5));
            } else {
                a.push(new Phaser.Point(1, 1));
                a.push(new Phaser.Point(1, 1));
                a.push(new Phaser.Point(1, 1));
                a.push(new Phaser.Point(1, 1));
                a.push(new Phaser.Point(1, 1));
                a.push(new Phaser.Point(1, 1));
            }

            return this.shuffleArray(a);
        };

        GameControler.prototype.rotations = function () {
            var a = new Array();
            if (this.canRotate()) {
                a.push(0);
                a.push(0);
                a.push(90);
                a.push(90);
                a.push(0);
                a.push(-90);
                a.push(-90);
                a.push(180);
                a.push(180);
                a.push(0);
                a.push(-180);
            } else {
                a.push(0);
                a.push(0);
                a.push(0);
                a.push(0);
                a.push(0);
                a.push(0);
                a.push(0);
                a.push(0);
            }

            return this.shuffleArray(a);
        };

        GameControler.prototype.positions = function () {
            //random slot
            var rnd = Math.floor(Math.random() * 5);
            if (this._slotsPosition != undefined)
                return this._slotsPosition[rnd];

            this._slotsPosition = new Array();

            //set1
            var set1 = new Array();
            set1.push(this.makePoint(380, 108));
            set1.push(this.makePoint(500, 108));
            set1.push(this.makePoint(450, 235));
            this._slotsPosition.push(set1);

            //set2
            var set2 = new Array();
            set2.push(this.makePoint(382, 359));
            set2.push(this.makePoint(500, 108));
            set2.push(this.makePoint(450, 235));
            this._slotsPosition.push(set2);

            //set3
            var set3 = new Array();
            set3.push(this.makePoint(500, 359));
            set3.push(this.makePoint(500, 108));
            set3.push(this.makePoint(450, 235));
            this._slotsPosition.push(set3);

            //set4
            var set4 = new Array();
            set4.push(this.makePoint(500, 359));
            set4.push(this.makePoint(382, 360));
            set4.push(this.makePoint(450, 235));
            this._slotsPosition.push(set4);

            //set5
            var set5 = new Array();
            set5.push(this.makePoint(380, 119));
            set5.push(this.makePoint(380, 360));
            set5.push(this.makePoint(450, 235));
            this._slotsPosition.push(set5);

            return this._slotsPosition[rnd];
        };

        GameControler.prototype.makePoint = function (x, y) {
            return new DesignADoor.DShapePoint(x, y);
        };
        GameControler._instance = null;
        return GameControler;
    })();
    DesignADoor.GameControler = GameControler;
})(DesignADoor || (DesignADoor = {}));
var DesignADoor;
(function (DesignADoor) {
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            _super.call(this, 800, 600, Phaser.CANVAS, 'content', null);

            //var rnd: number = Math.floor(Math.random() * 5) + 1
            var gameControler = DesignADoor.GameControler.getInstance().setLevel(1);
            this.state.add('Boot', DesignADoor.Boot, false);
            this.state.add('Preloader', DesignADoor.Preloader, false);
            this.state.add('LevelInfoView', DesignADoor.LevelInfoView, false);
            this.state.add('TutorialView', DesignADoor.TutorialView, false);
            this.state.add('GameView', DesignADoor.GameView, false);
            this.state.add('MenuView', DesignADoor.MenuView, false);
            this.state.start('Boot');
        }
        //Navigation delegates
        Game.prototype.onMainMenuPlaySelect = function () {
            this.state.start('LevelInfoView');
        };

        Game.prototype.onMainMenuGamesSelect = function () {
        };

        Game.prototype.onMainMenuStartSelect = function () {
            this.state.start('GameView');
        };

        Game.prototype.onLevelStartSelect = function () {
            this.state.start('TutorialView');
        };
        return Game;
    })(Phaser.Game);
    DesignADoor.Game = Game;
})(DesignADoor || (DesignADoor = {}));

window.onload = function () {
    var game = new DesignADoor.Game;
};
var DesignADoor;
(function (DesignADoor) {
    var DShapePoint = (function () {
        function DShapePoint(x, y) {
            this._x = x;
            this._y = y;
        }
        DShapePoint.prototype.getX = function () {
            return this._x;
        };

        DShapePoint.prototype.getY = function () {
            return this._y;
        };
        return DShapePoint;
    })();
    DesignADoor.DShapePoint = DShapePoint;
})(DesignADoor || (DesignADoor = {}));
var DesignADoor;
(function (DesignADoor) {
    var LevelData = (function () {
        function LevelData(positions, colours, shapes, rotations, scales) {
            this.positions = positions;
            this.colours = colours;
            this.shapes = shapes;
            this.rotations = rotations;
            this.scales = scales;
        }
        LevelData.prototype.getCorrectColourByShapeID = function (shapeID) {
            for (var i = 0; i < this.shapes.length; i++) {
                var sID = this.shapes[i];
                if (shapeID == sID) {
                    return this.colours[i];
                }
            }
        };
        return LevelData;
    })();
    DesignADoor.LevelData = LevelData;
})(DesignADoor || (DesignADoor = {}));
var DesignADoor;
(function (DesignADoor) {
    var Preloader = (function (_super) {
        __extends(Preloader, _super);
        function Preloader() {
            _super.apply(this, arguments);
        }
        Preloader.prototype.preload = function () {
            // preloader sprite
            var bg = this.game.add.sprite(0, 0, 'mainMenuBg');
            this.preloadBarBg = this.game.add.sprite(0, 0, 'progressBar');
            this.preloadBarBg.position.setTo(this.stage.width / 2 - this.preloadBarBg.width / 2, this.stage.height - this.preloadBarBg.height - 10);

            this.preloadBar = this.add.sprite(0, 0, 'progressBarBlue');
            this.preloadBar.position = this.preloadBarBg.position;
            this.load.setPreloadSprite(this.preloadBar);

            //  assets
            //
            this.load.image('coinsBg', 'assets/ui/coinProgressTab.png');
            this.load.image('coin', 'assets/ui/coin.png');
            this.load.image('life', 'assets/ui/lifeicon.png');
            this.load.spritesheet('coinsSpinning', 'assets/ui/spinning_coin_gold.png', 32, 32, 8);
            this.load.audio('gameAudio', 'assets/audio/designADoorTheme.mp3');
            this.load.audio('tiktakAudio', 'assets/audio/tiktaker.mp3');
            this.load.audio('ringAudio', 'assets/audio/rings.mp3');
            this.load.audio('overAudio', 'assets/audio/onOver.mp3');

            //TutorialView
            this.load.image('tutorialDoor', 'assets/tutorialDoor.png');
            this.load.image('tutorialBg', 'assets/tutorialBackground.png');
            this.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
            this.load.atlasJSONArray('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');
            this.load.atlasJSONArray('tutorialTexts', 'assets/tutorialTexts.png', 'assets/tutorialTexts.json');

            //MenuView
            this.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
            this.load.atlasJSONArray('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');

            //GameView
            this.load.image('bg', 'assets/ui/UIBackground.png');
            this.load.image('shapesBg', 'assets/ui/UIShapeBackgund.png');
            this.load.image('colorBg', 'assets/ui/UIColorBackground.png');
            this.load.image('scaleBg', 'assets/ui/UIScaleBackground.png');
            this.load.image('clockArrow', 'assets/ui/clockArrow.png');
            this.load.image('clockBackground', 'assets/ui/clockBackground.png');
            this.load.image('clockCenter', 'assets/ui/clockCenter.png');
            this.load.image('clockSpring', 'assets/ui/clockSpring.png');
            this.load.image('clockSign', 'assets/ui/clockSign.png');
            this.load.image('blocker', 'assets/ui/UIBlocker.png');

            this.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
            this.load.atlasJSONArray('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');
            this.load.atlasJSONArray('colours', 'assets/ui/colours.png', 'assets/ui/colours.json');
            this.load.atlasJSONArray('paints', 'assets/ui/paints.png', 'assets/ui/paints.json');
            this.load.atlasJSONArray('scale', 'assets/ui/UIScaleButton.png', 'assets/ui/UIScaleButton.json');
            this.load.atlasJSONArray('rotate', 'assets/ui/UIRotateButton.png', 'assets/ui/UIRotateButton.json');
            this.load.atlasJSONArray('reset', 'assets/ui/UIResetButton.png', 'assets/ui/UIResetButton.json');
            this.load.atlasJSONArray('check', 'assets/ui/UICheckButton.png', 'assets/ui/UICheckButton.json');
            this.load.atlasJSONArray('feedbackText', 'assets/ui/feedbackTexts.png', 'assets/ui/feedbackTexts.json');

            for (var i = 1; i <= 6; i++) {
                var shapeID = i;
                var shapeKey = 'shape' + shapeID;
                this.load.atlasJSONArray(shapeKey, 'assets/ui/' + shapeKey + '.png', 'assets/ui/' + shapeKey + '.json');

                var atlasURL = 'assets/tutorial' + i;
                this.load.atlasJSONArray('tutorialAnimation' + i, atlasURL + '.png', atlasURL + '.json');

                //
                this.load.atlasJSONArray('door' + i, 'assets/ui/door' + i + '.jpg', 'assets/ui/door' + i + '.json');
                this.game.load.audio('audioTutorial' + i, 'assets/audio/tutorial' + i + '.mp3');

                this.load.image('levelInfo' + i, 'assets/level' + i + '.png');
            }
            this.load.image('levelComplete', 'assets/levelComplete.png');
            for (var i = 1; i <= 5; i++) {
                this.game.load.audio('audioFeedback' + i, 'assets/audio/feedback' + i + '.mp3');
            }
        };

        Preloader.prototype.create = function () {
            var tween = this.add.tween(this.preloadBar).to({ alpha: 0 }, 500, Phaser.Easing.Linear.None, true);
            var tween2 = this.add.tween(this.preloadBarBg).to({ alpha: 0 }, 500, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.startMainMenu, this);
        };

        Preloader.prototype.startMainMenu = function () {
            this.game.state.start('MenuView', true, false);
        };
        return Preloader;
    })(Phaser.State);
    DesignADoor.Preloader = Preloader;
})(DesignADoor || (DesignADoor = {}));
var DesignADoor;
(function (DesignADoor) {
    var ClockView = (function (_super) {
        __extends(ClockView, _super);
        function ClockView(game, state) {
            _super.call(this, game);
            this._game = state;
            game.add.existing(this);

            this.createBackground();
            this.createClockArrow();
            this.createSign();

            this.startTimer();
        }
        ClockView.prototype.createSign = function () {
            this._spring = this.game.add.sprite(this.width / 2, this.height / 2 + 20, 'clockSpring');
            this._spring.anchor.setTo(0.5, 1);
            this._spring.visible = false;
            this.addChild(this._spring);

            this._sign = this.game.add.sprite(this._spring.x, this.height / 2 + 20, 'clockSign');
            this._sign.anchor.setTo(0.5, 1);
            this._sign.visible = false;
            this.addChild(this._sign);

            this.sendToBack(this._sign);
            this.sendToBack(this._spring);
        };

        ClockView.prototype.createBackground = function () {
            this._background = this.game.add.sprite(0, 0, 'clockBackground');
            this.addChild(this._background);
        };

        ClockView.prototype.createClockArrow = function () {
            this._arrow = this.game.add.sprite(100, 87, 'clockArrow');
            this._arrow.anchor.setTo(0.5, 1);
            this.addChild(this._arrow);

            var centerClock = this.game.add.sprite(100, 87, 'clockCenter');
            centerClock.anchor.setTo(0.5, 0.5);
            this.addChild(centerClock);
        };

        ClockView.prototype.startTimer = function () {
            DesignADoor.GameControler.getInstance().minumumVolumeGameMusic(true);
            this._tiktak = this._game.add.audio('tiktakAudio', 1, true);
            this._tiktak.play();

            var tween = this.game.add.tween(this._arrow).to({ angle: -360 }, 10000, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.onTimerComplete, this);
        };

        ClockView.prototype.onTimerComplete = function () {
            this._tiktak.stop();
            var ring = this._game.add.audio('ringAudio', 1, false);
            ring.play();
            this._sign.visible = true;
            this._spring.visible = true;

            var springScale = new Phaser.Point(1, 1.2);
            var springTween = this.game.add.tween(this._spring).to({ scale: springScale, y: this.height / 2 - 20 }, 1000, Phaser.Easing.Bounce.Out, true, 1500);
            var signTween = this.game.add.tween(this._sign).to({ y: -20 }, 1000, Phaser.Easing.Bounce.Out, true, 1500);
            var clockTween = this.game.add.tween(this._background).to({ x: -5 }, 100, Phaser.Easing.Linear.None, true, 0, 10);
            var arrowTween = this.game.add.tween(this._arrow).to({ x: this._arrow.x - 5 }, 100, Phaser.Easing.Linear.None, true, 0, 10);

            signTween.onComplete.add(this.onSignComplete, this);
        };

        ClockView.prototype.onSignComplete = function () {
            DesignADoor.GameControler.getInstance().minumumVolumeGameMusic(false);
            this._game.onClockTimerComplete();
        };
        return ClockView;
    })(Phaser.Group);
    DesignADoor.ClockView = ClockView;
})(DesignADoor || (DesignADoor = {}));
var DesignADoor;
(function (DesignADoor) {
    var CoinsView = (function (_super) {
        __extends(CoinsView, _super);
        function CoinsView(game, state) {
            _super.call(this, game);
            this._state = state;
            this.game.add.existing(this);

            this.createBackground();
        }
        CoinsView.prototype.createBackground = function () {
            var xpos = 250;
            var bg = this._state.add.sprite(xpos + 40, 0, 'coinsBg');
            var coin = this._state.add.sprite(0, 0, 'coin');
            coin.scale = new Phaser.Point(0.5, 0.5);
            coin.position.setTo(xpos + 50, bg.height / 2 - coin.height / 2);

            var numOfCoins;
            if (DesignADoor.GameControler.getInstance().getCoins() < 10) {
                numOfCoins = '  ' + DesignADoor.GameControler.getInstance().getCoins();
            } else if (DesignADoor.GameControler.getInstance().getCoins() < 100) {
                numOfCoins = ' ' + DesignADoor.GameControler.getInstance().getCoins();
            } else {
                numOfCoins = '' + DesignADoor.GameControler.getInstance().getCoins();
            }

            var style = { font: "32px vag_rounded_bold", fill: "#ffffff", align: "center", stroke: "#000000", strokeThickness: 4 };
            this._text = this._state.add.text(0, 0, numOfCoins, style);
            this._text.position.setTo(xpos + 67, bg.height / 2 - this._text.height / 2 + 2);

            var animCoin = this._state.add.sprite(0, 0, 'coinsSpinning');
            animCoin.position.setTo(xpos, bg.height / 2 - animCoin.height / 2);
            animCoin.animations.add('anim');
            animCoin.animations.play('anim', 12, true);
        };

        CoinsView.prototype.updateCoin = function () {
            var numOfCoins;
            if (DesignADoor.GameControler.getInstance().getCoins() < 10) {
                numOfCoins = '  ' + DesignADoor.GameControler.getInstance().getCoins();
            } else if (DesignADoor.GameControler.getInstance().getCoins() < 100) {
                numOfCoins = ' ' + DesignADoor.GameControler.getInstance().getCoins();
            } else {
                numOfCoins = '' + DesignADoor.GameControler.getInstance().getCoins();
            }
            this._text.setText('' + numOfCoins);
        };
        return CoinsView;
    })(Phaser.Group);
    DesignADoor.CoinsView = CoinsView;
})(DesignADoor || (DesignADoor = {}));
var DesignADoor;
(function (DesignADoor) {
    var DColour = (function (_super) {
        __extends(DColour, _super);
        function DColour(game, state, x, y, colourID) {
            var colourFrame = (colourID - 1) * 2;
            _super.call(this, game, x, y, 'colours', colourFrame);
            this.colourID = colourID;
            this.startX = x;
            this.startY = y;
            this.anchor.setTo(0.5, 0.5);
            this.inputEnabled = true;
            this._game = state;
            this.anchor.setTo(0.5, 0.5);
            this.hitArea = new Phaser.Rectangle(-25, -70, 50, 140);
            this.inputEnabled = true;
            this.input.enableDrag();
            this.events.onDragStart.add(this.onStartDrag, this);
            this.events.onDragStop.add(this.onStopDrag, this);
            game.add.existing(this);
        }
        DColour.prototype.onStartDrag = function () {
            var keyFrame = (this.colourID - 1) + 12;
            this.frame = keyFrame;
            this._game.onColourDragStart(this);
        };

        DColour.prototype.onStopDrag = function () {
            this.goBack();
            this._game.onColourDragStop(this);
        };

        DColour.prototype.goBack = function () {
            var tween = this._game.add.tween(this).to({ x: this.startX, y: this.startY }, 500, Phaser.Easing.Bounce.Out, true, 250);
            tween.onComplete.add(this.onBackComplete, this);
        };

        DColour.prototype.onBackComplete = function () {
            var keyFrame = (this.colourID - 1) * 2;
            this.frame = keyFrame;
        };
        return DColour;
    })(Phaser.Sprite);
    DesignADoor.DColour = DColour;
})(DesignADoor || (DesignADoor = {}));
var DesignADoor;
(function (DesignADoor) {
    var DShape = (function (_super) {
        __extends(DShape, _super);
        function DShape(game, state, x, y, shapeID, colourID) {
            var shapeKey = 'shape' + shapeID;
            _super.call(this, game, x, y, shapeKey, colourID - 1);
            this._game = state;
            this.shapeID = shapeID;
            this.colourID = colourID;
            this.startX = x;
            this.startY = y;
            this.slotID = -1;
            this.inSlotShapeID = -1;
            this.inShapeSlotID = -1;
            this.anchor.setTo(0.5, 0.5);
            this.inputEnabled = true;
            this.input.enableDrag();
            this.events.onDragStart.add(this.onStartDrag, this);
            this.events.onDragStop.add(this.onStopDrag, this);
            this.events.onInputDown.add(this.onDown, this);

            game.add.existing(this);
        }
        DShape.prototype.preload = function () {
        };

        DShape.prototype.create = function () {
        };

        DShape.prototype.onDown = function () {
            this._game.onShapeDown(this);
        };

        DShape.prototype.onStartDrag = function () {
            this._game.onShapeDragStart(this);
        };

        DShape.prototype.onStopDrag = function () {
            this._game.onShapeDragStop(this);
        };
        return DShape;
    })(Phaser.Sprite);
    DesignADoor.DShape = DShape;
})(DesignADoor || (DesignADoor = {}));
var DesignADoor;
(function (DesignADoor) {
    var GameView = (function (_super) {
        __extends(GameView, _super);
        function GameView(game) {
            _super.call(this);
            this._textWrongCounter = 3;
            this._textGoodCounter = 0;
            this._game = game;
        }
        GameView.prototype.preload = function () {
        };

        GameView.prototype.create = function () {
            if (DesignADoor.GameControler.getInstance().userAnswerIsGood) {
                DesignADoor.GameControler.getInstance().refresh();
            }
            this._level = DesignADoor.GameControler.getInstance().getLevel();
            this._gameState = DesignADoor.GameControler.getInstance().getGameState();
            this._levelData = DesignADoor.GameControler.getInstance().getAnswers();
            this._overSound = this.add.sound('overAudio');

            var backgound = this.add.sprite(0, 0, 'bg');

            this._buttons = this.add.group();

            var buttonsBg = this.add.sprite(0, 0, 'buttonsBg');
            this._buttons.addChild(buttonsBg);

            var gameButton = this.add.button(10, 0, 'buttons', this.onHelpClick, this, 1, 0);
            gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            this._buttons.addChild(gameButton);

            var playButton = this.add.button(0, gameButton.y, 'buttons', this.onGamesClick, this, 5, 4);
            playButton.x = this._buttons.width - playButton.width - 10;
            this._buttons.addChild(playButton);

            this._buttons.x = 10;
            this._buttons.y = this.game.stage.height - this._buttons.height - 10;

            this.createShapesPanel();
            this.createColourPanel();
            this.createScalePanel();
            this.createRightPanel();
            this.createAnswers();
            this.createCoins();
            this._blocker = this.add.sprite(0, 0, 'blocker');
            this._blocker.inputEnabled = true;
            this.createClock();
        };

        GameView.prototype.createCoins = function () {
            this._coins = new DesignADoor.CoinsView(this.game, this);
            this._coins.position.setTo(140, 0);
        };

        GameView.prototype.createAnswers = function () {
            this._dropSlots = new Array();
            this._userAnswers = new Array();
            for (var i = 0; i < this._levelData.positions.length; i++) {
                var p = this._levelData.positions[i];
                var sAngle = this._levelData.rotations[i];
                var sScale = this._levelData.scales[i];
                var shape = new DesignADoor.DShape(this.game, this, p.getX(), p.getY(), this._levelData.shapes[i], this._levelData.colours[i]);
                shape.angle = sAngle;
                shape.scale = sScale;
                shape.inputEnabled = false;
                shape.slotID = i;
                this._dropSlots.push(shape);
            }
        };

        GameView.prototype.createClock = function () {
            this._clock = new DesignADoor.ClockView(this.game, this);
            this._clock.position.setTo(15, 150);
            this._clock.bringToTop(this._clock.children);
        };

        GameView.prototype.onClockTimerComplete = function () {
            this.blockUI(false);
            this._clock.visible = false;
            this.hideAnswers(true);
        };

        GameView.prototype.createRightPanel = function () {
            var resetButton = this.add.button(730, 293, 'reset', this.onGameReset, this, 1, 0);
            resetButton.anchor.setTo(0.5, 0.5);

            var checkButton = this.add.button(730, 420, 'check', this.onGameCheck, this, 1, 0);
            checkButton.anchor.setTo(0.5, 0.5);

            this._lives = new DesignADoor.LivesView(this.game, this);
            this._lives.position.setTo(this.stage.width - this._lives.width - 50, 10);
        };

        GameView.prototype.createShapesPanel = function () {
            var rnd;
            if (DesignADoor.GameControler.getInstance().userAnswerIsGood) {
                rnd = Math.floor(Math.random() * 5) + 1;
            } else {
                rnd = DesignADoor.GameControler.getInstance().userDoorID;
            }

            var bg = this.add.sprite(10, 13, 'shapesBg');
            this._door = this.add.sprite(226, 0, 'door' + rnd, 0);
            this._door.animations.add('anim');
            this._doorID = rnd;
            DesignADoor.GameControler.getInstance().userDoorID = this._doorID;

            this._textPositions = new Array();
            this._textPositions.push(new Phaser.Point(this._door.x + 235, 291));
            this._textPositions.push(new Phaser.Point(this._door.x + 235, 262));
            this._textPositions.push(new Phaser.Point(this._door.x + 223, 203));
            this._textPositions.push(new Phaser.Point(this._door.x + 233, 267));
            this._textPositions.push(new Phaser.Point(this._door.x + 230, 317));
            this._textPositions.push(new Phaser.Point(this._door.x + 230, 277));

            this._textFeedback = this.add.sprite(0, 0, 'feedbackText', 0);
            this._textFeedback.anchor.setTo(0.5, 0.5);
            this._textFeedback.visible = false;

            var xpos = 63;
            var ypos = 55;
            for (var i = 1; i <= 6; i++) {
                var shapeID = i;
                var shapeKey = 'shape' + shapeID;
                var colourID;
                if (!this._levelData.canColour) {
                    colourID = this._levelData.getCorrectColourByShapeID(i);
                } else {
                    colourID = Math.floor(Math.random() * 6) + 1;
                }

                var shape = new DesignADoor.DShape(this.game, this, xpos, ypos, shapeID, colourID);

                xpos += 88;
                if (i % 2 == 0) {
                    ypos += 63;
                    xpos = 63;
                }
                //console.log('shape: '+ shapeID + 'colour: ' + colourID + ' xpos: ' + xpos + ' ypos: ' + ypos);
            }
        };

        GameView.prototype.createColourPanel = function () {
            var bg = this.add.sprite(10, 230, 'colorBg');
            var xpos = 32;
            for (var i = 1; i <= 6; i++) {
                var colour = new DesignADoor.DColour(this.game, this, xpos, 290, i);
                xpos += 30;

                if (!this._levelData.canColour) {
                    colour.alpha = 0.5;
                    colour.inputEnabled = false;
                }
            }
        };

        GameView.prototype.createScalePanel = function () {
            var bg = this.add.sprite(10, 360, 'scaleBg');

            this._scaleButton = this.add.button(25, 380, 'scale', this.onScaleSelected, this, 0, 0);
            if (!this._levelData.canScale) {
                this._scaleButton.alpha = 0.5;
                this._scaleButton.inputEnabled = false;
            }
            this._rotateButton = this.add.button(114, 380, 'rotate', this.onRotateSelect, this, 0, 0);
            if (!this._levelData.canRotate) {
                this._rotateButton.alpha = 0.5;
                this._rotateButton.inputEnabled = false;
            }
        };

        GameView.prototype.onHelpClick = function () {
            this._overSound.play();
        };

        GameView.prototype.onGamesClick = function () {
            this._overSound.play();
            this.game.state.start('GameView', true, false);
        };

        GameView.prototype.blockUI = function (block) {
            this._blocker.visible = block;
            this._blocker.bringToTop();
        };

        GameView.prototype.hideAnswers = function (hide) {
            for (var i = 0; i < this._dropSlots.length; i++) {
                var slot = this._dropSlots[i];
                slot.visible = !hide;
            }
        };

        //Check answers&reset
        GameView.prototype.onGameReset = function () {
            this._overSound.play();
            for (var i = 0; i < this._userAnswers.length; i++) {
                var s = this._userAnswers[i];
                this.selectSlot(s.inShapeSlotID, s.shapeID, false);
                s.destroy();
            }
            this._userAnswers = new Array();

            for (var j = 0; j < this._dropSlots.length; j++) {
                var correctAnswer = this._dropSlots[j];
                correctAnswer.checked = false;
            }
        };

        GameView.prototype.onGameCheck = function () {
            this._overSound.play();
            this.blockUI(true);
            var userPoints = 0;
            for (var i = 0; i < this._userAnswers.length; i++) {
                var userAnswer = this._userAnswers[i];
                for (var j = 0; j < this._dropSlots.length; j++) {
                    var correctAnswer = this._dropSlots[j];

                    if (userAnswer.shapeID == correctAnswer.shapeID && userAnswer.colourID == correctAnswer.colourID && userAnswer.angle == correctAnswer.angle && userAnswer.scale.x == correctAnswer.scale.x && !correctAnswer.checked) {
                        userPoints++;
                        correctAnswer.checked = true;
                        //console.log('user shape: ' + userAnswer.shapeID + ' == ' + correctAnswer.shapeID + ' \n colour: ' + userAnswer.colourID + ' == ' + correctAnswer.colourID + '\n scale: ' + userAnswer.scale + ' == ' + correctAnswer.scale + '\n angle: ' + userAnswer.angle + ' == ' + correctAnswer.angle);
                    }
                }
            }
            this.showFeedback(userPoints == this._dropSlots.length);
        };

        GameView.prototype.showFeedback = function (isGood) {
            var textFrame = 0;
            var audioFeed = 1;
            this._door.animations.play('anim', 12, false);
            this.onGameReset();
            this.hideAnswers(true);

            if (isGood) {
                this._textGoodCounter = Math.floor(Math.random() * 3) + 1;
                textFrame = this._textGoodCounter - 1;
                audioFeed = this._textGoodCounter;
                DesignADoor.GameControler.getInstance().addLevelPoint();
            } else {
                this._textWrongCounter = Math.floor(Math.random() * 5) + 1;
                if (this._textWrongCounter <= 3) {
                    this._textWrongCounter = 4;
                }
                audioFeed = this._textWrongCounter;
                textFrame = this._textWrongCounter - 1;
                DesignADoor.GameControler.getInstance().removeLevelPoint();
                this._lives.removeLive();
            }
            var textPosition = this._textPositions[this._doorID - 1];
            this._textFeedback.frame = textFrame;
            this._textFeedback.position.setTo(textPosition.x, textPosition.y);
            this._textFeedback.scale.setTo(0, 0);
            this._textFeedback.visible = true;
            var toScale = new Phaser.Point(2, 2);
            var textTween = this.game.add.tween(this._textFeedback).to({ width: 150, height: 116 }, 1000, Phaser.Easing.Bounce.Out, true, 300);
            this._audioFeedback = this.game.add.audio('audioFeedback' + audioFeed);
            this._audioFeedback.play();

            DesignADoor.GameControler.getInstance().userAnswerIsGood = isGood;
            this.checkLevelComplete(isGood);
        };

        GameView.prototype.checkLevelComplete = function (isGood) {
            this._coins.updateCoin();

            //console.log('pojnts: '+GameControler.getInstance().getLevelPoint());
            if (DesignADoor.GameControler.getInstance().getLevelPoint() == 3) {
                this.goToNextLevel(isGood);
            } else {
                console.log('user lifes: ' + DesignADoor.GameControler.getInstance().userLifes);
                if (DesignADoor.GameControler.getInstance().userLifes < 1) {
                    var s = new DesignADoor.LevelCompleteView(this.game, this, false);
                    this._buttons.visible = false;
                    DesignADoor.GameControler.getInstance().setLevel(this._level);
                }
            }
        };

        GameView.prototype.goToNextLevel = function (isGood) {
            var nextLevel;
            if (isGood) {
                if (this._level < 6) {
                    nextLevel = this._level + 1;
                } else {
                    nextLevel = 1;
                }
                DesignADoor.GameControler.getInstance().setLevel(nextLevel);

                //this._game.onMainMenuPlaySelect();
                var s = new DesignADoor.LevelCompleteView(this.game, this, true);
                this._buttons.visible = false;
            }
        };

        //LevelComplete popup
        GameView.prototype.onGameClick = function () {
            this._overSound.play();
        };
        GameView.prototype.onNextClick = function () {
            //this._overSound.play();
            //this.game.state.start('LevelInfoView');
            this._game.onMainMenuPlaySelect();
        };

        /*
        render() {
        if (this._audioFeedback != undefined) {
        console.log(this._audioFeedback.currentTime + '  ' + this._audioFeedback.totalDuration);
        }
        
        if (this._audioFeedback != undefined && this._audioFeedback.isPlaying) {
        if (this._audioFeedback.currentTime >= this._audioFeedback.totalDuration) {
        this.onGameReset();
        }
        }
        }
        */
        //Scale&Rotate
        GameView.prototype.onRotateSelect = function () {
            this._overSound.play();
            if (this._gameState != 'rotate') {
                this.deselectScale(true);
                this.deselectRotate(false);
            } else {
                this.deselectRotate(true);
            }
        };

        GameView.prototype.deselectRotate = function (deselect) {
            if (!deselect) {
                this._gameState = 'rotate';
                this._rotateButton.setFrames(1, 1);
                this.removeDragEvent(true);
            } else {
                this._gameState = 'drag';
                this._rotateButton.setFrames(0, 0);
                this.removeDragEvent(false);
            }
        };

        GameView.prototype.onScaleSelected = function () {
            this._overSound.play();
            if (this._gameState != 'scale') {
                this.deselectRotate(true);
                this.deselectScale(false);
            } else {
                this.deselectScale(true);
            }
        };

        GameView.prototype.deselectScale = function (deselect) {
            if (!deselect) {
                this._gameState = 'scale';
                this._scaleButton.setFrames(1, 1);
                this.removeDragEvent(true);
            } else {
                this._gameState = 'drag';
                this._scaleButton.setFrames(0, 0);
                this.removeDragEvent(false);
            }
        };

        GameView.prototype.removeDragEvent = function (remove) {
            for (var i = 0; i < this._userAnswers.length; i++) {
                var s = this._userAnswers[i];
                if (remove) {
                    s.input.disableDrag();
                } else {
                    s.input.enableDrag();
                }
            }
        };

        //Colours Drag&Drop
        GameView.prototype.onColourDragStart = function (colour) {
            if (this._gameState == 'scale') {
                this.deselectScale(true);
            } else if (this._gameState == 'rotate') {
                this.deselectRotate(true);
            }
            colour.bringToTop();
        };

        GameView.prototype.onColourDragStop = function (colour) {
            for (var i = this._userAnswers.length - 1; i >= 0; i--) {
                var answer = this._userAnswers[i];
                if (colour.overlap(answer)) {
                    answer.colourID = colour.colourID;
                    answer.frame = colour.colourID - 1;
                    break;
                }
            }
        };

        //Shapes Drag&Drop methods
        GameView.prototype.onShapeDown = function (shape) {
            if (this._gameState == 'scale') {
                if (shape.inShapeSlotID >= 0) {
                    var p = new Phaser.Point(1, 1);
                    if (shape.scale.x == 1) {
                        shape.scale.setTo(1.5, 1.5);
                    } else {
                        shape.scale = p;
                    }
                }
            } else if (this._gameState == 'rotate') {
                if (shape.inShapeSlotID >= 0) {
                    shape.angle += 90;
                }
            }
        };
        GameView.prototype.onShapeDragStart = function (shape) {
            if (this._gameState == 'scale') {
                this.deselectScale(true);
            } else if (this._gameState == 'rotate') {
                this.deselectRotate(true);
            }
            var copyShape = new DesignADoor.DShape(this.game, this, shape.startX, shape.startY, shape.shapeID, shape.colourID);
            this._selectedShape = shape;
            this._selectedShape.bringToTop();
            if (shape.inShapeSlotID >= 0) {
                this.removeShapeFromAnswer(shape);
                this.selectSlot(shape.inShapeSlotID, shape.shapeID, false);
            }
        };
        GameView.prototype.onShapeDragStop = function (shape) {
            this.checkShapeDropPosition(shape);
        };

        GameView.prototype.checkShapeDropPosition = function (shape) {
            var isHit = false;
            for (var i = 0; i < this._dropSlots.length; i++) {
                var slot = this._dropSlots[i];
                if (shape.overlap(slot)) {
                    if (slot.inSlotShapeID <= 0) {
                        isHit = true;
                        shape.x = slot.x;
                        shape.y = slot.y;
                        shape.inShapeSlotID = slot.slotID;
                        this.selectSlot(slot.slotID, shape.shapeID, true);
                        this._userAnswers.push(shape);
                        break;
                    }
                }
            }

            if (!isHit) {
                this.removeShapeFromAnswer(shape);
                shape.destroy();
                this._selectedShape = undefined;
            }
        };

        GameView.prototype.removeShapeFromAnswer = function (shape) {
            for (var i = this._userAnswers.length - 1; i >= 0; i--) {
                var answer = this._userAnswers[i];
                if (answer == shape) {
                    this._userAnswers.splice(i, 1);
                }
            }
        };

        GameView.prototype.selectSlot = function (slotID, dropID, select) {
            var slot = this._dropSlots[slotID];
            if (select) {
                slot.inSlotShapeID = dropID;
            } else {
                slot.inSlotShapeID = -1;
            }
        };
        return GameView;
    })(Phaser.State);
    DesignADoor.GameView = GameView;
})(DesignADoor || (DesignADoor = {}));
var DesignADoor;
(function (DesignADoor) {
    var LevelCompleteView = (function (_super) {
        __extends(LevelCompleteView, _super);
        function LevelCompleteView(game, state, isOk) {
            _super.call(this, game);
            this._game = state;
            this._isOk = isOk;
            game.add.existing(this);
            this.createBackground();
        }
        LevelCompleteView.prototype.createBackground = function () {
            this._overSound = this.game.add.sound('overAudio');
            var bg = this.game.add.sprite(0, 0, 'levelComplete');
            this.addChild(bg);
            var popupText;
            if (this._isOk) {
                popupText = 'Congratulations!  You completed the level!';
            } else {
                popupText = 'You got too many wrong answers.  Try again!';
            }
            var style = { font: "32px vag_rounded_bold", fill: "#ffffff", align: "justify", stroke: "#000000", strokeThickness: 4 };
            var text = this.game.add.text(160, 250, popupText, style);
            text.wordWrap = true;
            text.wordWrapWidth = 500;
            text.align = 'center';
            text.fill = '#ffffff';
            text.position.setTo(this.stage.width / 2 - text.width / 2, 250);
            this.addChild(text);
            this._buttons = this.game.add.group();
            this.addChild(this._buttons);

            var buttonsBg = this.game.add.sprite(0, 0, 'buttonsBg');
            this._buttons.addChild(buttonsBg);

            var gameButton = this.game.add.button(10, 0, 'buttons', this.onGame, this, 1, 0);
            gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            this._buttons.addChild(gameButton);

            var playButton = this.game.add.button(0, gameButton.y, 'buttons', this.onNext, this, 5, 4);
            playButton.x = this._buttons.width - playButton.width - 10;
            this._buttons.addChild(playButton);

            this._buttons.x = this.stage.width / 2 - this._buttons.width / 2;
            this._buttons.y = 600 - this._buttons.height - 100;
        };

        LevelCompleteView.prototype.onGame = function () {
            this._overSound.play();
            this._game.onGameClick();
        };

        LevelCompleteView.prototype.onNext = function () {
            this._overSound.play();
            this._game.onNextClick();
        };
        return LevelCompleteView;
    })(Phaser.Group);
    DesignADoor.LevelCompleteView = LevelCompleteView;
})(DesignADoor || (DesignADoor = {}));
var DesignADoor;
(function (DesignADoor) {
    var LevelInfoView = (function (_super) {
        __extends(LevelInfoView, _super);
        function LevelInfoView(game) {
            _super.call(this);
            this._game = game;
        }
        LevelInfoView.prototype.create = function () {
            this._overSound = this.add.sound('overAudio');
            this._level = DesignADoor.GameControler.getInstance().getLevel();
            console.log('level info: ' + this._level);
            var background = this.add.sprite(0, 0, 'mainMenuBg');
            var bg = this.add.sprite(0, 0, 'levelInfo' + this._level);

            var style = { font: "32px vag_rounded_bold", fill: "#ffffff", align: "center", stroke: "#000000", strokeThickness: 4 };
            var text = this.add.text(160, 250, this.levelText(), style);
            text.wordWrap = true;
            text.wordWrapWidth = 500;
            text.align = 'center';
            text.fill = '#ffffff';

            this._buttons = this.add.group();

            var buttonsBg = this.add.sprite(0, 0, 'buttonsBg');
            this._buttons.addChild(buttonsBg);

            var gameButton = this.add.button(10, 0, 'buttons', this.onGameClick, this, 1, 0);
            gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            this._buttons.addChild(gameButton);

            var playButton = this.add.button(0, gameButton.y, 'buttons', this.onStartClick, this, 5, 4);
            playButton.x = this._buttons.width - playButton.width - 10;
            this._buttons.addChild(playButton);

            this._buttons.x = this.stage.width / 2 - this._buttons.width / 2;
            this._buttons.y = 600 - this._buttons.height - 100;
        };

        LevelInfoView.prototype.levelText = function () {
            var s;
            switch (this._level) {
                case 1:
                    s = 'Learn how to play Design A Door.';
                    break;
                case 2:
                    s = 'Make the designs with the shapes!';
                    break;
                case 3:
                    s = 'Make designs with colors!';
                    break;
                case 4:
                    s = 'Make the designs with big and small shapes!';
                    break;
                case 5:
                    s = 'Make the designs and spin the shapes!';
                    break;
                case 6:
                    s = 'Make the designs with all the tools!';
                    break;
            }
            return s;
        };

        LevelInfoView.prototype.onGameClick = function () {
            this._overSound.play();
        };

        LevelInfoView.prototype.onStartClick = function () {
            this._overSound.play();
            this._game.onLevelStartSelect();
        };
        return LevelInfoView;
    })(Phaser.State);
    DesignADoor.LevelInfoView = LevelInfoView;
})(DesignADoor || (DesignADoor = {}));
var DesignADoor;
(function (DesignADoor) {
    var LivesView = (function (_super) {
        __extends(LivesView, _super);
        function LivesView(game, state) {
            _super.call(this, game);
            this._numOfLives = 3;
            this._state = state;
            this._maxLives = DesignADoor.GameControler.getInstance().userLifes;
            this.game.add.existing(this);

            this.createLives();
        }
        LivesView.prototype.createLives = function () {
            var ypos = 0;
            this._lives = new Array();
            for (var i = 0; i < this._maxLives; i++) {
                var s = this.game.add.sprite(0, 0, 'life');
                s.position.setTo(0, ypos);
                ypos += s.height + 10;
                this.addChild(s);
                this._lives.push(s);
            }
        };

        LivesView.prototype.removeLive = function () {
            console.log('remove lif');
            if (DesignADoor.GameControler.getInstance().userLifes <= this._maxLives) {
                var s = this._lives[DesignADoor.GameControler.getInstance().userLifes];
                this.removeChild(s);
                this._numOfLives--;
            }
        };
        return LivesView;
    })(Phaser.Group);
    DesignADoor.LivesView = LivesView;
})(DesignADoor || (DesignADoor = {}));
var DesignADoor;
(function (DesignADoor) {
    var MenuView = (function (_super) {
        __extends(MenuView, _super);
        function MenuView(game) {
            _super.call(this);
            this._game = game;
        }
        MenuView.prototype.preload = function () {
        };

        MenuView.prototype.create = function () {
            this._overSound = this.add.sound('overAudio');
            var background = this.add.sprite(0, 0, 'mainMenuBg');
            this._buttons = this.add.group();

            var buttonsBg = this.add.sprite(0, 0, 'buttonsBg');
            this._buttons.addChild(buttonsBg);

            var gameButton = this.add.button(10, 0, 'buttons', this.onGameClick, this, 1, 0);
            gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            this._buttons.addChild(gameButton);

            var playButton = this.add.button(0, gameButton.y, 'buttons', this.onPlayClick, this, 3, 2);
            playButton.x = this._buttons.width - playButton.width - 10;
            this._buttons.addChild(playButton);

            this._buttons.x = 20;
            this._buttons.y = this.game.stage.height - this._buttons.height - 20;

            DesignADoor.GameControler.getInstance().playGameMusic(this.game);
        };

        MenuView.prototype.onGameClick = function () {
            this._overSound.play();
            this._game.onMainMenuGamesSelect();
        };

        MenuView.prototype.onPlayClick = function () {
            this._overSound.play();
            this._game.onMainMenuPlaySelect();
        };
        return MenuView;
    })(Phaser.State);
    DesignADoor.MenuView = MenuView;
})(DesignADoor || (DesignADoor = {}));
var DesignADoor;
(function (DesignADoor) {
    var TutorialView = (function (_super) {
        __extends(TutorialView, _super);
        function TutorialView(game) {
            _super.call(this);
            this._game = game;
        }
        TutorialView.prototype.preload = function () {
        };

        TutorialView.prototype.create = function () {
            this._overSound = this.add.sound('overAudio');
            this._level = DesignADoor.GameControler.getInstance().getLevel();
            var level = this._level;
            if (level == 1 || level == 2) {
                level = 1;
            }
            console.log(this._level);
            var background = this.add.sprite(0, 0, 'tutorialBg');
            var instructions = this.add.sprite(0, 0, 'tutorialTexts', this._level - 1);
            var door = this.add.sprite(340, 220, 'tutorialDoor');

            var animation = this.add.sprite(door.x, door.y, 'tutorialAnimation' + level);
            animation.animations.add('anim');
            animation.animations.play('anim', 12, true, false);

            this._buttons = this.add.group();

            var buttonsBg = this.add.sprite(0, 0, 'buttonsBg');
            this._buttons.addChild(buttonsBg);

            var gameButton = this.add.button(10, 0, 'buttons', this.onGameClick, this, 1, 0);
            gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            this._buttons.addChild(gameButton);

            var playButton = this.add.button(0, gameButton.y, 'buttons', this.onStartClick, this, 5, 4);
            playButton.x = this._buttons.width - playButton.width - 10;
            this._buttons.addChild(playButton);

            this._buttons.x = 20;
            this._buttons.y = 600 - this._buttons.height - 20;

            this._audioInstructions = this.game.add.audio('audioTutorial' + this._level);
            this._audioInstructions.play();
        };

        TutorialView.prototype.onGameClick = function () {
            this._overSound.play();
            this._audioInstructions.stop();
            this._game.onMainMenuGamesSelect();
        };

        TutorialView.prototype.onStartClick = function () {
            this._overSound.play();
            this._audioInstructions.stop();
            this._game.onMainMenuStartSelect();
        };
        return TutorialView;
    })(Phaser.State);
    DesignADoor.TutorialView = TutorialView;
})(DesignADoor || (DesignADoor = {}));
