﻿var SimpleGame = (function () {
    function SimpleGame() {
        this.game = new Phaser.Game(800, 600, Phaser.AUTO, 'content', { preload: this.preload, create: this.create });
    }
    SimpleGame.prototype.preload = function () {
        // this.game.load.atlasJSONArray('bot', 'assets/tutorial1.png', 'assets/tutorial1.json');
    };

    SimpleGame.prototype.create = function () {
        //var logo = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'logo');
        //logo.anchor.setTo(0.5, 0.5);
        var frameData = this.game.cache.getFrameData('bot');
        console.log("dupa", frameData);

        //  This sprite is using a texture atlas for all of its animation data
        var bot = this.game.add.sprite(280, 263, 'bot');

        //  Here we add a new animation called 'run'
        //  We haven't specified any frames because it's using every frame in the texture atlas
        //bot.animations.add('run');
        bot.animations.add('botAnim');

        //  And this starts the animation playing by using its key ("run")
        //  15 is the frame rate (15fps)
        //  true means it will loop when it finishes
        bot.animations.play('botAnim', 12, true, false);
    };
    return SimpleGame;
})();

window.onload = function () {
    var game = new SimpleGame();
};
//# sourceMappingURL=app.js.map
