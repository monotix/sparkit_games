﻿module DesignADoor {
    export class LevelData {
        public positions: Array<any>;
        public colours: Array<number>;
        public shapes: Array<number>;
        public rotations: Array<number>;
        public scales: Array<Phaser.Point>;
        public canRotate: boolean;
        public canScale: boolean;
        public canColour: boolean;

        constructor(positions:Array<any>,colours:Array<number>, shapes:Array<number>, rotations:Array<number>, scales:Array<Phaser.Point>) {
            this.positions = positions;
            this.colours = colours;
            this.shapes = shapes;
            this.rotations = rotations;
            this.scales = scales;
        }

        public getCorrectColourByShapeID(shapeID:number): number {
            for (var i: number = 0; i < this.shapes.length; i++) {
                var sID:number = this.shapes[i];
                if (shapeID == sID) {
                    return this.colours[i];
                }
            }
        }
    }
}