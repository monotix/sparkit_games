﻿module DesignADoor {
    export class MenuView extends Phaser.State {

        private _buttons: Phaser.Group;
        private _game: Game;
        private _overSound: Phaser.Sound;

        constructor(game:Game) {
            super();
            this._game = game;
        }

        preload() {
           
        }

        create() {
            this._overSound = this.add.sound('overAudio');
            var background: Phaser.Sprite = this.add.sprite(0, 0, 'mainMenuBg');
            this._buttons = this.add.group();

            var buttonsBg: Phaser.Sprite = this.add.sprite(0, 0, 'buttonsBg');
            this._buttons.addChild(buttonsBg);

            var gameButton = this.add.button(10, 0, 'buttons', this.onGameClick, this, 1, 0);
            gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            this._buttons.addChild(gameButton);

            var playButton = this.add.button(0, gameButton.y, 'buttons', this.onPlayClick, this, 3, 2);
            playButton.x = this._buttons.width - playButton.width - 10;
            this._buttons.addChild(playButton);

            this._buttons.x = 20;
            this._buttons.y = this.game.stage.height - this._buttons.height - 20;

            GameControler.getInstance().playGameMusic(this.game);
        }

        private onGameClick() {
            this._overSound.play();
            this._game.onMainMenuGamesSelect();
        }

        private onPlayClick() {
            this._overSound.play();
            this._game.onMainMenuPlaySelect();
        }
    }
} 