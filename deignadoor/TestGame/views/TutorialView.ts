﻿module DesignADoor {

    export class TutorialView extends Phaser.State {
        private _level: number;
        private _game: Game;
        private _buttons: Phaser.Group;
        private _audioInstructions: Phaser.Sound;
        private _overSound: Phaser.Sound;
        constructor(game:Game) {
            super();
            this._game = game;
        }

        preload() {
        }

        create() {
            this._overSound = this.add.sound('overAudio');
            this._level = GameControler.getInstance().getLevel();
            var level: number = this._level;
            if (level == 1 || level == 2) {
                level = 1;
            }
            console.log(this._level);
            var background: Phaser.Sprite = this.add.sprite(0, 0, 'tutorialBg');
            var instructions: Phaser.Sprite = this.add.sprite(0, 0, 'tutorialTexts', this._level-1);
            var door: Phaser.Sprite = this.add.sprite(340, 220, 'tutorialDoor');
            
            var animation: Phaser.Sprite = this.add.sprite(door.x, door.y, 'tutorialAnimation'+level);
            animation.animations.add('anim');
            animation.animations.play('anim', 12, true, false);

            

            this._buttons = this.add.group();

            var buttonsBg: Phaser.Sprite = this.add.sprite(0, 0, 'buttonsBg');
            this._buttons.addChild(buttonsBg);

            var gameButton = this.add.button(10, 0, 'buttons', this.onGameClick, this, 1, 0);
            gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            this._buttons.addChild(gameButton);

            var playButton = this.add.button(0, gameButton.y, 'buttons', this.onStartClick, this, 5, 4);
            playButton.x = this._buttons.width - playButton.width - 10;
            this._buttons.addChild(playButton);

            this._buttons.x = 20;
            this._buttons.y = 600 - this._buttons.height - 20;

            this._audioInstructions = this.game.add.audio('audioTutorial' + this._level);
            this._audioInstructions.play();
        }

        private onGameClick() {
            this._overSound.play();
            this._audioInstructions.stop();
            this._game.onMainMenuGamesSelect();
        }

        private onStartClick() {
            this._overSound.play();
            this._audioInstructions.stop();
            this._game.onMainMenuStartSelect();
        }
    }

}