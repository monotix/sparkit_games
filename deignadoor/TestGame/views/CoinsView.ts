﻿module DesignADoor {
    export class CoinsView extends Phaser.Group {
        private _state: GameView;
        private _text: Phaser.Text;
        constructor(game: Phaser.Game, state: GameView) {
            super(game);
            this._state = state;
            this.game.add.existing(this);

            this.createBackground();
        }


        private createBackground() {
            var xpos: number = 250;
            var bg: Phaser.Sprite = this._state.add.sprite(xpos+40, 0, 'coinsBg');
            var coin: Phaser.Sprite = this._state.add.sprite(0, 0, 'coin');
            coin.scale = new Phaser.Point(0.5, 0.5);
            coin.position.setTo(xpos+50, bg.height / 2 - coin.height / 2);

            var numOfCoins: string;
            if (GameControler.getInstance().getCoins() < 10) {
                numOfCoins = '  ' + GameControler.getInstance().getCoins();
            } else if (GameControler.getInstance().getCoins() < 100) {
                numOfCoins = ' ' + GameControler.getInstance().getCoins();
            } else {
                numOfCoins = '' + GameControler.getInstance().getCoins();
            }

            var style = { font: "32px vag_rounded_bold", fill: "#ffffff", align: "center", stroke: "#000000", strokeThickness: 4 };
            this._text = this._state.add.text(0, 0, numOfCoins, style);
            this._text.position.setTo(xpos+67, bg.height / 2 - this._text.height / 2 + 2);

            var animCoin: Phaser.Sprite = this._state.add.sprite(0, 0, 'coinsSpinning');
            animCoin.position.setTo(xpos, bg.height / 2 - animCoin.height / 2);
            animCoin.animations.add('anim');
            animCoin.animations.play('anim', 12, true);
        }

        public updateCoin(): void {
            var numOfCoins: string;
            if (GameControler.getInstance().getCoins() < 10) {
                numOfCoins = '  ' + GameControler.getInstance().getCoins();
            } else if (GameControler.getInstance().getCoins() < 100) {
                numOfCoins = ' ' + GameControler.getInstance().getCoins();
            } else {
                numOfCoins = '' + GameControler.getInstance().getCoins();
            }
            this._text.setText('' + numOfCoins);
        }
    }
} 