﻿module DesignADoor {
    export class LevelCompleteView extends Phaser.Group {
        private _game: GameView;
        private _buttons: Phaser.Group;
        private _isOk: boolean;
        private _overSound: Phaser.Sound;
        constructor(game: Phaser.Game, state: GameView, isOk:boolean) {
            super(game);
            this._game = state;
            this._isOk = isOk;
            game.add.existing(this);
            this.createBackground();
        }

        createBackground() {
            this._overSound = this.game.add.sound('overAudio');
            var bg: Phaser.Sprite = this.game.add.sprite(0, 0, 'levelComplete');
            this.addChild(bg);
            var popupText: string;
            if (this._isOk) {
                popupText = 'Congratulations!  You completed the level!';
            } else {
                popupText = 'You got too many wrong answers.  Try again!'
            }
            var style = { font: "32px vag_rounded_bold", fill: "#ffffff", align: "justify", stroke: "#000000", strokeThickness: 4 };
            var text: Phaser.Text = this.game.add.text(160, 250, popupText, style);
            text.wordWrap = true;
            text.wordWrapWidth = 500;
            text.align = 'center';
            text.fill = '#ffffff';
            text.position.setTo(this.stage.width / 2 - text.width / 2,250);
            this.addChild(text);
            this._buttons = this.game.add.group();
            this.addChild(this._buttons);

            var buttonsBg: Phaser.Sprite = this.game.add.sprite(0, 0, 'buttonsBg');
            this._buttons.addChild(buttonsBg);
            
            var gameButton = this.game.add.button(10, 0, 'buttons', this.onGame, this, 1, 0);
            gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            this._buttons.addChild(gameButton);

            var playButton = this.game.add.button(0, gameButton.y, 'buttons', this.onNext, this, 5, 4);
            playButton.x = this._buttons.width - playButton.width - 10;
            this._buttons.addChild(playButton);

            this._buttons.x = this.stage.width / 2 - this._buttons.width / 2;
            this._buttons.y = 600 - this._buttons.height - 100;
        }

        private onGame() {
            this._overSound.play();
            this._game.onGameClick();
        }

        private onNext() {
            this._overSound.play();
            this._game.onNextClick();
        }
    }
} 