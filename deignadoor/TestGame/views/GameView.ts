﻿module DesignADoor {
    export class GameView extends Phaser.State {
        private _level: number;
        private _game: Game;
        private _buttons: Phaser.Group;
        private _selectedShape: DShape;
        private _levelData: LevelData;
        private _dropSlots: Array<DShape>;
        private _userAnswers: Array<DShape>;
        private _gameState: string;
        private _scaleButton: Phaser.Button;
        private _rotateButton: Phaser.Button;
        private _clock: Phaser.Group;
        private _blocker: Phaser.Sprite;
        private _door: Phaser.Sprite;
        private _textPositions: Array<Phaser.Point>;
        private _textFeedback: Phaser.Sprite;
        private _textWrongCounter: number = 3;
        private _textGoodCounter: number = 0;
        private _doorID: number;
        private _coins: CoinsView;
        private _overSound: Phaser.Sound;
        private _audioFeedback: Phaser.Sound;
        private _lives: LivesView;

        constructor(game: Game) {
            super();
            this._game = game;
            
        }

        preload() {
            
        }

        create() {
            if (GameControler.getInstance().userAnswerIsGood) {
                GameControler.getInstance().refresh();
            }
            this._level = GameControler.getInstance().getLevel();
            this._gameState = GameControler.getInstance().getGameState();
            this._levelData = GameControler.getInstance().getAnswers();
            this._overSound = this.add.sound('overAudio');

            var backgound: Phaser.Sprite = this.add.sprite(0, 0, 'bg');

            this._buttons = this.add.group();

            var buttonsBg: Phaser.Sprite = this.add.sprite(0, 0, 'buttonsBg');
            this._buttons.addChild(buttonsBg);


            var gameButton = this.add.button(10, 0, 'buttons', this.onHelpClick, this, 1, 0);
            gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            this._buttons.addChild(gameButton);

            var playButton = this.add.button(0, gameButton.y, 'buttons', this.onGamesClick, this, 5, 4);
            playButton.x = this._buttons.width - playButton.width - 10;
            this._buttons.addChild(playButton);

            this._buttons.x = 10;
            this._buttons.y = this.game.stage.height - this._buttons.height - 10;

            this.createShapesPanel();
            this.createColourPanel();
            this.createScalePanel();
            this.createRightPanel();
            this.createAnswers();
            this.createCoins();
            this._blocker = this.add.sprite(0, 0, 'blocker');
            this._blocker.inputEnabled = true;
            this.createClock();
       
        }

        private createCoins() {
            this._coins = new CoinsView(this.game, this);
            this._coins.position.setTo(140, 0);
        }

        private createAnswers() {
            this._dropSlots = new Array();
            this._userAnswers = new Array();
            for (var i: number = 0; i < this._levelData.positions.length; i++) {
                var p: DShapePoint = this._levelData.positions[i];
                var sAngle: number = this._levelData.rotations[i]
                var sScale: Phaser.Point = this._levelData.scales[i]
                var shape: DShape = new DShape(this.game, this, p.getX(), p.getY(), this._levelData.shapes[i], this._levelData.colours[i]);
                shape.angle = sAngle;
                shape.scale = sScale;
                shape.inputEnabled = false;
                shape.slotID = i;
                this._dropSlots.push(shape);
            }

        }

        private createClock() {
            this._clock = new ClockView(this.game, this);
            this._clock.position.setTo(15, 150);
            this._clock.bringToTop(this._clock.children);
        }

        public onClockTimerComplete() {
            this.blockUI(false);
            this._clock.visible = false;
            this.hideAnswers(true);
        }

        private createRightPanel() {
            var resetButton: Phaser.Button = this.add.button(730, 293, 'reset', this.onGameReset, this, 1, 0);
            resetButton.anchor.setTo(0.5, 0.5);

            var checkButton: Phaser.Button = this.add.button(730, 420, 'check', this.onGameCheck, this, 1, 0);
            checkButton.anchor.setTo(0.5, 0.5);

            this._lives = new LivesView(this.game, this);
            this._lives.position.setTo(this.stage.width - this._lives.width - 50, 10);
        }

        private createShapesPanel() {
            var rnd: number;
            if (GameControler.getInstance().userAnswerIsGood) {
                rnd = Math.floor(Math.random() * 5) + 1
            } else {
                rnd = GameControler.getInstance().userDoorID; 
            }
            
            var bg: Phaser.Sprite = this.add.sprite(10, 13, 'shapesBg');
            this._door = this.add.sprite(226, 0, 'door'+rnd, 0);
            this._door.animations.add('anim');
            this._doorID = rnd;
            GameControler.getInstance().userDoorID = this._doorID;

            this._textPositions = new Array();
            this._textPositions.push(new Phaser.Point(this._door.x + 235, 291));
            this._textPositions.push(new Phaser.Point(this._door.x + 235, 262));
            this._textPositions.push(new Phaser.Point(this._door.x + 223, 203));
            this._textPositions.push(new Phaser.Point(this._door.x + 233, 267));
            this._textPositions.push(new Phaser.Point(this._door.x + 230, 317));
            this._textPositions.push(new Phaser.Point(this._door.x + 230, 277));

            this._textFeedback = this.add.sprite(0, 0, 'feedbackText', 0);
            this._textFeedback.anchor.setTo(0.5, 0.5);
            this._textFeedback.visible = false;

            var xpos: number = 63;
            var ypos: number = 55;
            for (var i: number = 1; i <= 6; i++) {
                var shapeID: number = i;
                var shapeKey: string = 'shape' + shapeID;
                var colourID: number
                if (!this._levelData.canColour) {
                    colourID = this._levelData.getCorrectColourByShapeID(i);
                } else {
                    colourID = Math.floor(Math.random() * 6) + 1;
                }
                
                var shape: DShape = new DShape(this.game,this, xpos, ypos, shapeID, colourID);

                xpos += 88;
                if (i % 2 == 0) {
                    ypos += 63;
                    xpos = 63;
                }
                //console.log('shape: '+ shapeID + 'colour: ' + colourID + ' xpos: ' + xpos + ' ypos: ' + ypos);
            }
        }

        private createColourPanel() {
            var bg: Phaser.Sprite = this.add.sprite(10, 230, 'colorBg');
            var xpos: number = 32;
            for (var i: number = 1; i <= 6; i++) {
                var colour: DColour = new DColour(this.game, this, xpos, 290, i);
                xpos += 30;

                if (!this._levelData.canColour) {
                    colour.alpha = 0.5;
                    colour.inputEnabled = false;
                }
            }
        }

        private createScalePanel() {
            var bg: Phaser.Sprite = this.add.sprite(10, 360, 'scaleBg');

            this._scaleButton = this.add.button(25, 380, 'scale', this.onScaleSelected, this, 0, 0);
            if (!this._levelData.canScale) {
                this._scaleButton.alpha = 0.5;
                this._scaleButton.inputEnabled = false;
            }
            this._rotateButton = this.add.button(114, 380, 'rotate', this.onRotateSelect, this, 0, 0);
            if (!this._levelData.canRotate) {
                this._rotateButton.alpha = 0.5;
                this._rotateButton.inputEnabled = false;
            }
        }

        private onHelpClick() {
            this._overSound.play();
        }

        private onGamesClick() {
            this._overSound.play();
            this.game.state.start('GameView', true, false);
        }

        private blockUI(block: boolean) {
            this._blocker.visible = block;
            this._blocker.bringToTop();
        }

        private hideAnswers(hide: boolean) {
            for (var i: number = 0; i < this._dropSlots.length; i++) {
                var slot: DShape = this._dropSlots[i];
                slot.visible = !hide;
            }
        }
        //Check answers&reset
        private onGameReset() {
            this._overSound.play();
            for (var i: number = 0; i < this._userAnswers.length; i++) {
                var s: DShape = this._userAnswers[i];
                this.selectSlot(s.inShapeSlotID, s.shapeID, false);
                s.destroy();
            }
            this._userAnswers = new Array();

            for (var j: number = 0; j < this._dropSlots.length; j++) {
                var correctAnswer: DShape = this._dropSlots[j];
                correctAnswer.checked = false;
            }
        }

        private onGameCheck() {
            this._overSound.play();
            this.blockUI(true);
            var userPoints: number = 0;
            for (var i: number = 0; i < this._userAnswers.length; i++) {
                var userAnswer: DShape = this._userAnswers[i];
                for (var j: number = 0; j < this._dropSlots.length; j++) {
                    var correctAnswer: DShape = this._dropSlots[j];
                    
                    if (userAnswer.shapeID == correctAnswer.shapeID &&
                        userAnswer.colourID == correctAnswer.colourID &&
                        userAnswer.angle == correctAnswer.angle &&
                        userAnswer.scale.x == correctAnswer.scale.x&&
                        !correctAnswer.checked) {
                        userPoints++;
                        correctAnswer.checked = true;
                        //console.log('user shape: ' + userAnswer.shapeID + ' == ' + correctAnswer.shapeID + ' \n colour: ' + userAnswer.colourID + ' == ' + correctAnswer.colourID + '\n scale: ' + userAnswer.scale + ' == ' + correctAnswer.scale + '\n angle: ' + userAnswer.angle + ' == ' + correctAnswer.angle);
                    }
                }
            }
            this.showFeedback(userPoints == this._dropSlots.length);
        }

        private showFeedback(isGood: boolean) {
            var textFrame: number = 0;
            var audioFeed: number = 1;
            this._door.animations.play('anim', 12, false);
            this.onGameReset();
            this.hideAnswers(true);

            if (isGood) {
                this._textGoodCounter = Math.floor(Math.random() * 3) + 1
                textFrame = this._textGoodCounter-1;
                audioFeed = this._textGoodCounter;
                GameControler.getInstance().addLevelPoint();
            } else {
                this._textWrongCounter = Math.floor(Math.random() * 5) + 1
                if (this._textWrongCounter <= 3) {
                    this._textWrongCounter = 4;
                }
                audioFeed = this._textWrongCounter;
                textFrame = this._textWrongCounter-1;
                GameControler.getInstance().removeLevelPoint();
                this._lives.removeLive();
            }
            var textPosition: Phaser.Point = this._textPositions[this._doorID - 1];
            this._textFeedback.frame = textFrame;
            this._textFeedback.position.setTo(textPosition.x, textPosition.y);
            this._textFeedback.scale.setTo(0, 0);
            this._textFeedback.visible = true;
            var toScale = new Phaser.Point(2, 2);
            var textTween = this.game.add.tween(this._textFeedback).to({ width:150, height:116 }, 1000, Phaser.Easing.Bounce.Out, true,300);
            this._audioFeedback = this.game.add.audio('audioFeedback' + audioFeed);
            this._audioFeedback.play();

            

            GameControler.getInstance().userAnswerIsGood = isGood;
            this.checkLevelComplete(isGood);
        }

        private checkLevelComplete(isGood:boolean) {
            this._coins.updateCoin();
            //console.log('pojnts: '+GameControler.getInstance().getLevelPoint());
            if (GameControler.getInstance().getLevelPoint() == 3) {
                this.goToNextLevel(isGood);
            } else {
                console.log('user lifes: ' + GameControler.getInstance().userLifes);
             if (GameControler.getInstance().userLifes < 1) {
                var s: LevelCompleteView = new LevelCompleteView(this.game, this, false);
                 this._buttons.visible = false;
                 GameControler.getInstance().setLevel(this._level);
             }
            }
            
        }

        private goToNextLevel(isGood:boolean): void {
            var nextLevel: number;
            if (isGood) {
                if (this._level < 6) {
                    nextLevel = this._level + 1;
                } else {
                    nextLevel = 1;
                }
                GameControler.getInstance().setLevel(nextLevel);
                //this._game.onMainMenuPlaySelect();
                var s: LevelCompleteView = new LevelCompleteView(this.game, this, true);
                this._buttons.visible = false;
            } 
        }

        //LevelComplete popup
        public onGameClick() {
            this._overSound.play();
        }
        public onNextClick() {
            //this._overSound.play();
            //this.game.state.start('LevelInfoView');
            this._game.onMainMenuPlaySelect();
        }
        
        /*
        render() {
            if (this._audioFeedback != undefined) {
                console.log(this._audioFeedback.currentTime + '  ' + this._audioFeedback.totalDuration);
            }
            
            if (this._audioFeedback != undefined && this._audioFeedback.isPlaying) {
                if (this._audioFeedback.currentTime >= this._audioFeedback.totalDuration) {
                    this.onGameReset();
                }
            }
        }
*/
        //Scale&Rotate
        private onRotateSelect() {
            this._overSound.play();
            if (this._gameState != 'rotate') {
                this.deselectScale(true);
                this.deselectRotate(false);
            } else {
                this.deselectRotate(true);
            }
        }

        private deselectRotate(deselect:boolean) {
            if (!deselect) {
                this._gameState = 'rotate'
                this._rotateButton.setFrames(1, 1);
                this.removeDragEvent(true);
            } else {
                this._gameState = 'drag';
                this._rotateButton.setFrames(0, 0);
                this.removeDragEvent(false);
            }
        }

        private onScaleSelected() {
            this._overSound.play();
            if (this._gameState != 'scale') {
                this.deselectRotate(true);
                this.deselectScale(false);
            } else {
                this.deselectScale(true);
            }
        }

        private deselectScale(deselect: boolean) {
            if (!deselect) {
                this._gameState = 'scale'
                this._scaleButton.setFrames(1, 1);
                this.removeDragEvent(true);
            } else {
                this._gameState = 'drag';
                this._scaleButton.setFrames(0, 0);
                this.removeDragEvent(false);
            }
        }

        private removeDragEvent(remove: boolean) {
            for (var i: number = 0; i < this._userAnswers.length; i++) {
                var s: DShape = this._userAnswers[i];
                if (remove) {
                    s.input.disableDrag();
                } else {
                    s.input.enableDrag();
                }
            }
        }

        //Colours Drag&Drop
        public onColourDragStart(colour: DColour) {
            if (this._gameState == 'scale') {
                this.deselectScale(true);
            } else if (this._gameState == 'rotate') {
                this.deselectRotate(true);
            }
            colour.bringToTop();
        }

        public onColourDragStop(colour: DColour) {
            for (var i: number = this._userAnswers.length - 1; i >= 0; i--) {
                var answer: DShape = this._userAnswers[i];
                if (colour.overlap(answer)) {
                    answer.colourID = colour.colourID;
                    answer.frame = colour.colourID - 1;
                    break;
                }
            }
        }

        //Shapes Drag&Drop methods
        public onShapeDown(shape: DShape) {
            if (this._gameState == 'scale') {
                if (shape.inShapeSlotID >= 0) {
                    var p: Phaser.Point = new Phaser.Point(1, 1);
                    if (shape.scale.x == 1) {
                        shape.scale.setTo(1.5, 1.5);
                    } else {
                        shape.scale = p;
                    }
                }
            } else if (this._gameState == 'rotate') {
                if (shape.inShapeSlotID >= 0) {
                    shape.angle += 90;
                }
            }
        }
        public onShapeDragStart(shape: DShape) {
            if (this._gameState == 'scale') {
                this.deselectScale(true);
            } else if (this._gameState == 'rotate') {
                this.deselectRotate(true);
            }
            var copyShape: DShape = new DShape(this.game, this, shape.startX, shape.startY, shape.shapeID, shape.colourID);
            this._selectedShape = shape;
            this._selectedShape.bringToTop();
            if (shape.inShapeSlotID >= 0) {
                this.removeShapeFromAnswer(shape);
                this.selectSlot(shape.inShapeSlotID, shape.shapeID, false);
            } 
        }
        public onShapeDragStop(shape: DShape) {
            this.checkShapeDropPosition(shape);
        }

        private checkShapeDropPosition(shape:DShape) {
            var isHit: boolean = false;
            for (var i: number = 0; i < this._dropSlots.length; i++) {
                var slot: DShape = this._dropSlots[i];
                if (shape.overlap(slot)) {
                    if (slot.inSlotShapeID <= 0) {
                        isHit = true;
                        shape.x = slot.x;
                        shape.y = slot.y;
                        shape.inShapeSlotID = slot.slotID;
                        this.selectSlot(slot.slotID, shape.shapeID, true);
                        this._userAnswers.push(shape);
                        break;
                    }
                }
            }

            if (!isHit) {
                this.removeShapeFromAnswer(shape);
                shape.destroy();
                this._selectedShape = undefined;
            } 
        }

        private removeShapeFromAnswer(shape: DShape) {
            for (var i: number = this._userAnswers.length - 1; i >= 0; i--) {
                var answer: DShape = this._userAnswers[i];
                if (answer == shape) {
                    this._userAnswers.splice(i, 1);
                }
            }
        }

        private selectSlot(slotID: number, dropID:number, select: boolean) {
            var slot: DShape = this._dropSlots[slotID];
            if (select) {
                slot.inSlotShapeID = dropID;                
            } else {
                slot.inSlotShapeID = -1;
            }
        }
    }
} 