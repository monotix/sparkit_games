﻿module DesignADoor {
    export class DColour extends Phaser.Sprite {
        public colourID: number;
        public startX: number;
        public startY: number;
        private _game: GameView;
        constructor(game: Phaser.Game, state:GameView, x: number, y: number, colourID: number) {
            var colourFrame: number = (colourID - 1) * 2;
            super(game, x, y, 'colours', colourFrame);
            this.colourID = colourID;
            this.startX = x;
            this.startY = y;
            this.anchor.setTo(0.5, 0.5);
            this.inputEnabled = true;
            this._game = state;
            this.anchor.setTo(0.5, 0.5);
            this.hitArea = new Phaser.Rectangle(-25, -70, 50, 140);
            this.inputEnabled = true;
            this.input.enableDrag();
            this.events.onDragStart.add(this.onStartDrag, this);
            this.events.onDragStop.add(this.onStopDrag, this);
            game.add.existing(this);
        }

        private onStartDrag() {
            var keyFrame: number = (this.colourID - 1) + 12;
            this.frame = keyFrame;
            this._game.onColourDragStart(this);
        }

        private onStopDrag() {
            this.goBack();
            this._game.onColourDragStop(this);
        }

        public goBack() {
            var tween = this._game.add.tween(this).to({ x: this.startX, y: this.startY }, 500, Phaser.Easing.Bounce.Out, true,250);
            tween.onComplete.add(this.onBackComplete, this);
        }

        private onBackComplete() {
            var keyFrame: number = (this.colourID - 1) * 2;
            this.frame = keyFrame;
        }
    }
} 