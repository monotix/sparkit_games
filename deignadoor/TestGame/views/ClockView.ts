﻿module DesignADoor {
    export class ClockView extends Phaser.Group {

        private _arrow: Phaser.Sprite;
        private _sign: Phaser.Sprite;
        private _spring: Phaser.Sprite;
        private _game: GameView;
        private _background: Phaser.Sprite;
        private _tiktak: Phaser.Sound;

        constructor(game:Phaser.Game, state:GameView) {
            super(game);
            this._game = state;
            game.add.existing(this);
 
            this.createBackground();
            this.createClockArrow();
            this.createSign();

            this.startTimer();
        }

        createSign() {
            this._spring = this.game.add.sprite(this.width/2, this.height/2+20, 'clockSpring');
            this._spring.anchor.setTo(0.5, 1);
            this._spring.visible = false;
            this.addChild(this._spring);
           

            this._sign = this.game.add.sprite(this._spring.x, this.height/2+20, 'clockSign');
            this._sign.anchor.setTo(0.5, 1);
            this._sign.visible = false;
            this.addChild(this._sign);

            this.sendToBack(this._sign);
            this.sendToBack(this._spring);
        }

        createBackground() {
            this._background = this.game.add.sprite(0, 0, 'clockBackground');
            this.addChild(this._background);
        }

        createClockArrow() {
            this._arrow = this.game.add.sprite(100, 87, 'clockArrow');
            this._arrow.anchor.setTo(0.5, 1);
            this.addChild(this._arrow);

            var centerClock: Phaser.Sprite = this.game.add.sprite(100, 87, 'clockCenter');
            centerClock.anchor.setTo(0.5, 0.5);
            this.addChild(centerClock);
        }

        public startTimer() {
            GameControler.getInstance().minumumVolumeGameMusic(true);
            this._tiktak = this._game.add.audio('tiktakAudio', 1, true);
            this._tiktak.play();

            var tween = this.game.add.tween(this._arrow).to({ angle: -360 }, 10000, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.onTimerComplete, this);
        }

        private onTimerComplete() {
            this._tiktak.stop();
            var ring: Phaser.Sound = this._game.add.audio('ringAudio', 1, false);
            ring.play();
            this._sign.visible = true;
            this._spring.visible = true;

            var springScale = new Phaser.Point(1, 1.2);
            var springTween = this.game.add.tween(this._spring).to({ scale: springScale, y: this.height / 2 - 20}, 1000, Phaser.Easing.Bounce.Out, true,1500);
            var signTween = this.game.add.tween(this._sign).to({ y: -20 }, 1000, Phaser.Easing.Bounce.Out, true,1500);
            var clockTween = this.game.add.tween(this._background).to({ x: -5 }, 100, Phaser.Easing.Linear.None, true, 0, 10);
            var arrowTween = this.game.add.tween(this._arrow).to({ x: this._arrow.x -5 }, 100, Phaser.Easing.Linear.None, true, 0, 10);

            signTween.onComplete.add(this.onSignComplete, this);
        }

        private onSignComplete() {
            GameControler.getInstance().minumumVolumeGameMusic(false);
            this._game.onClockTimerComplete();
        }
    }
} 