﻿module DesignADoor {
    export class LevelInfoView extends Phaser.State {
        private _level: number;
        private _game: Game;
        private _buttons: Phaser.Group;
        private _overSound: Phaser.Sound;
        constructor(game: Game) {
            super();
            this._game = game;
        }

        create() {
            this._overSound = this.add.sound('overAudio');
            this._level = GameControler.getInstance().getLevel();
            console.log('level info: ' + this._level);
            var background: Phaser.Sprite = this.add.sprite(0, 0, 'mainMenuBg');
            var bg: Phaser.Sprite = this.add.sprite(0, 0, 'levelInfo' + this._level);

            var style = { font: "32px vag_rounded_bold", fill: "#ffffff", align: "center", stroke: "#000000", strokeThickness: 4 };
            var text: Phaser.Text = this.add.text(160, 250, this.levelText(), style);
            text.wordWrap = true;
            text.wordWrapWidth = 500;
            text.align = 'center';
            text.fill = '#ffffff';

            this._buttons = this.add.group();

            var buttonsBg: Phaser.Sprite = this.add.sprite(0, 0, 'buttonsBg');
            this._buttons.addChild(buttonsBg);

            var gameButton = this.add.button(10, 0, 'buttons', this.onGameClick, this, 1, 0);
            gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            this._buttons.addChild(gameButton);

            var playButton = this.add.button(0, gameButton.y, 'buttons', this.onStartClick, this, 5, 4);
            playButton.x = this._buttons.width - playButton.width - 10;
            this._buttons.addChild(playButton);

            this._buttons.x = this.stage.width/2 - this._buttons.width/2;
            this._buttons.y = 600 - this._buttons.height - 100;
        }

        private levelText(): string {
            var s: string;
            switch (this._level) {
                case 1:
                    s = 'Learn how to play Design A Door.'
                    break;
                case 2:
                    s = 'Make the designs with the shapes!'
                    break;
                case 3:
                    s = 'Make designs with colors!'
                    break;
                case 4:
                    s = 'Make the designs with big and small shapes!'
                    break;
                case 5:
                    s = 'Make the designs and spin the shapes!'
                    break;
                case 6:
                    s = 'Make the designs with all the tools!'
                    break;

            }
            return s;
        }

        private onGameClick() {
            this._overSound.play();
        }

        private onStartClick() {
            this._overSound.play();
            this._game.onLevelStartSelect();
        }
    }
} 