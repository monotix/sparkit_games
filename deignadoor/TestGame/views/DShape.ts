﻿module DesignADoor {
    export class DShape extends Phaser.Sprite {
        public shapeID: number;
        public colourID: number;
        public startX: number;
        public startY: number;
        public slotID: number;
        public inSlotShapeID: number;
        public inShapeSlotID: number;
        public checked: boolean;

        private _game: GameView;

        constructor(game: Phaser.Game, state:GameView, x: number, y: number, shapeID: number, colourID: number) {
            var shapeKey: String = 'shape' + shapeID;
            super(game, x, y, shapeKey, colourID - 1);
            this._game = state;
            this.shapeID = shapeID;
            this.colourID = colourID;
            this.startX = x;
            this.startY = y;
            this.slotID = -1;
            this.inSlotShapeID = -1;
            this.inShapeSlotID = -1;
            this.anchor.setTo(0.5, 0.5);
            this.inputEnabled = true;
            this.input.enableDrag();
            this.events.onDragStart.add(this.onStartDrag, this);
            this.events.onDragStop.add(this.onStopDrag, this);
            this.events.onInputDown.add(this.onDown, this);
          
            game.add.existing(this);
        }

        preload() {
           
        }

        create() {
        }

        private onDown() {
            this._game.onShapeDown(this);
        }

        private onStartDrag() {
            this._game.onShapeDragStart(this);
        }

        private onStopDrag() {
            this._game.onShapeDragStop(this);
        }
    }
} 