﻿module DesignADoor {
    export class LivesView extends Phaser.Group {
        private _state: GameView;
        private _maxLives: Number;
        private _lives: Array<Phaser.Sprite>;
        private _numOfLives = 3;
        constructor(game: Phaser.Game, state: GameView) {
            super(game);
            this._state = state;
            this._maxLives = GameControler.getInstance().userLifes;
            this.game.add.existing(this);

            this.createLives();
        }

        public createLives() {
            var ypos: number = 0;
            this._lives = new Array();
            for (var i: number = 0; i < this._maxLives; i++) {
                var s: Phaser.Sprite = this.game.add.sprite(0,0,'life');
                s.position.setTo(0, ypos);
                ypos += s.height + 10;
                this.addChild(s);
                this._lives.push(s);
            }
        }

        public removeLive() {
            console.log('remove lif');
            if (GameControler.getInstance().userLifes <= this._maxLives) {
                var s: Phaser.Sprite = this._lives[GameControler.getInstance().userLifes];
                this.removeChild(s);
                this._numOfLives--;
            }
            
        }

    }
} 