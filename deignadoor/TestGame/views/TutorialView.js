﻿var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var DesignADoor;
(function (DesignADoor) {
    var TutorialView = (function (_super) {
        __extends(TutorialView, _super);
        function TutorialView(currentLevel) {
            _super.call(this);
            this._level = currentLevel;
        }
        TutorialView.prototype.preload = function () {
            var atlasURL = 'assets/tutorial' + this._level;
            this.load.atlasJSONArray('tutorialAnimation', atlasURL + '.png', atlasURL + 'json');
        };

        TutorialView.prototype.create = function () {
            var animation = this.add.sprite(280, 263, 'tutorialAnimation');
            animation.animations.add('anim');
            animation.animations.play('anim', 12, true, false);
        };
        return TutorialView;
    })(Phaser.State);
    DesignADoor.TutorialView = TutorialView;
})(DesignADoor || (DesignADoor = {}));
//# sourceMappingURL=TutorialView.js.map
