﻿module DesignADoor {

    export class Preloader extends Phaser.State {

        private preloadBar: Phaser.Sprite;
        private preloadBarBg: Phaser.Sprite;

        preload() {
            // preloader sprite
            var bg: Phaser.Sprite = this.game.add.sprite(0, 0, 'mainMenuBg');
            this.preloadBarBg = this.game.add.sprite(0, 0, 'progressBar');
            this.preloadBarBg.position.setTo(this.stage.width / 2 - this.preloadBarBg.width / 2, this.stage.height - this.preloadBarBg.height - 10);

            this.preloadBar = this.add.sprite(0, 0, 'progressBarBlue');
            this.preloadBar.position = this.preloadBarBg.position;
            this.load.setPreloadSprite(this.preloadBar);
            //  assets
            //
            this.load.image('coinsBg', 'assets/ui/coinProgressTab.png');
            this.load.image('coin', 'assets/ui/coin.png');
            this.load.image('life', 'assets/ui/lifeicon.png');
            this.load.spritesheet('coinsSpinning', 'assets/ui/spinning_coin_gold.png', 32, 32, 8);
            this.load.audio('gameAudio', 'assets/audio/designADoorTheme.mp3');
            this.load.audio('tiktakAudio', 'assets/audio/tiktaker.mp3');
            this.load.audio('ringAudio', 'assets/audio/rings.mp3');
            this.load.audio('overAudio', 'assets/audio/onOver.mp3');
            //TutorialView
            this.load.image('tutorialDoor', 'assets/tutorialDoor.png');
            this.load.image('tutorialBg', 'assets/tutorialBackground.png');
            this.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
            this.load.atlasJSONArray('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');
            this.load.atlasJSONArray('tutorialTexts', 'assets/tutorialTexts.png', 'assets/tutorialTexts.json');

            //MenuView
            this.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
            this.load.atlasJSONArray('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');

            //GameView
            this.load.image('bg', 'assets/ui/UIBackground.png');
            this.load.image('shapesBg', 'assets/ui/UIShapeBackgund.png');
            this.load.image('colorBg', 'assets/ui/UIColorBackground.png');
            this.load.image('scaleBg', 'assets/ui/UIScaleBackground.png');
            this.load.image('clockArrow', 'assets/ui/clockArrow.png');
            this.load.image('clockBackground', 'assets/ui/clockBackground.png');
            this.load.image('clockCenter', 'assets/ui/clockCenter.png');
            this.load.image('clockSpring', 'assets/ui/clockSpring.png');
            this.load.image('clockSign', 'assets/ui/clockSign.png');
            this.load.image('blocker', 'assets/ui/UIBlocker.png');

            this.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
            this.load.atlasJSONArray('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');
            this.load.atlasJSONArray('colours', 'assets/ui/colours.png', 'assets/ui/colours.json');
            this.load.atlasJSONArray('paints', 'assets/ui/paints.png', 'assets/ui/paints.json');
            this.load.atlasJSONArray('scale', 'assets/ui/UIScaleButton.png', 'assets/ui/UIScaleButton.json');
            this.load.atlasJSONArray('rotate', 'assets/ui/UIRotateButton.png', 'assets/ui/UIRotateButton.json');
            this.load.atlasJSONArray('reset', 'assets/ui/UIResetButton.png', 'assets/ui/UIResetButton.json');
            this.load.atlasJSONArray('check', 'assets/ui/UICheckButton.png', 'assets/ui/UICheckButton.json');
            this.load.atlasJSONArray('feedbackText', 'assets/ui/feedbackTexts.png', 'assets/ui/feedbackTexts.json');

            for (var i: number = 1; i <= 6; i++) {
                var shapeID: number = i;
                var shapeKey: string = 'shape' + shapeID;
                this.load.atlasJSONArray(shapeKey, 'assets/ui/' + shapeKey + '.png', 'assets/ui/' + shapeKey + '.json');

                var atlasURL: String = 'assets/tutorial' + i;
                this.load.atlasJSONArray('tutorialAnimation'+i, atlasURL + '.png', atlasURL + '.json');
                //
                this.load.atlasJSONArray('door' + i, 'assets/ui/door' + i + '.jpg', 'assets/ui/door' + i + '.json');
                this.game.load.audio('audioTutorial' + i, 'assets/audio/tutorial' + i + '.mp3');

                this.load.image('levelInfo' + i, 'assets/level' + i + '.png');

            }
            this.load.image('levelComplete', 'assets/levelComplete.png');
            for (var i: number = 1; i <= 5; i++) {
                this.game.load.audio('audioFeedback' + i, 'assets/audio/feedback' + i + '.mp3');
            }
        }

        create() {
            var tween = this.add.tween(this.preloadBar).to({ alpha: 0 }, 500, Phaser.Easing.Linear.None, true);
            var tween2 = this.add.tween(this.preloadBarBg).to({ alpha: 0 }, 500, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.startMainMenu, this);
            
        }

        startMainMenu() {
            this.game.state.start('MenuView', true, false);
        }

    }

} 