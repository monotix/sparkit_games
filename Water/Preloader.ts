﻿module Water {

    export class Preloader extends Phaser.State {


        preload() {
          
            var bg = this.add.image(0, 0, "bg", 0);

            var blackProgress = this.add.image(0, 0, "progressBarBlack", 0);
            var blueProgress = this.add.sprite(0, 0, "progressBarBlue", 0);


            blackProgress.position.setTo(this.stage.width / 2 - blackProgress.width / 2, this.stage.height - blackProgress.height - 10);

            blueProgress.position = blackProgress.position;
            this.load.setPreloadSprite(blueProgress);


            this.load.image("mainMenuBg", "assets/mainMenu/splashScreen.png");
            this.load.atlasJSONArray('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');
            this.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
            this.load.audio("MainMenuSn", "assets/audio/ScubaDudeTheme.mp3");
            this.load.audio("overButtonA", "assets/audio/overButton.mp3");
            this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");
            this.load.atlasJSONArray('helpBtn', 'assets/popup/help.png', 'assets/popup/help.json');

            var mobile: boolean;
            mobile = false
            if (!this.game.device.desktop) {
                mobile = true;
            }


            if (mobile) {
                this.load.image("firstInstruction", "assets/03_wake_trash.png");
                this.load.audio("instructionSn", "assets/audio/wake_thrash_02.mp3");

            }
            else {
                this.load.image("firstInstruction", "assets/02_wake_trash.png");
                this.load.audio("instructionSn", "assets/audio/wake_thrash_01.mp3");
            }




            this.load.image("popup", "assets/popup/popup.png");
            this.load.image("logoGame", "assets/popup/logo.png");
            this.load.atlasJSONArray('games', 'assets/popup/games.png', 'assets/popup/games.json');
            this.load.atlasJSONArray('playGame', 'assets/popup/play.png', 'assets/popup/play.json');
    


            this.load.image("bgGame", "assets/water/gameBackground.png");

            this.load.atlasJSONArray('boatsmedium', 'assets/water/boat/boatsmedium.png', 'assets/water/boat/boatsmedium.json');
            this.load.atlasJSONArray('boatssmall', 'assets/water/boat/boatssmall.png', 'assets/water/boat/boatssmall.json');

            /* instructions */
            this.load.atlasJSONArray('prompt', 'assets/water/prompt/prompt.png', 'assets/water/prompt/prompt.json');
            this.load.audio("WakeIntro", "assets/audio/instruction/WakeIntro.mp3");
            this.load.audio("WakeSuccess0", "assets/audio/instruction/WakeSuccess0.mp3");
            this.load.audio("WakeWrong0", "assets/audio/instruction/WakeWrong0.mp3");
            this.load.audio("music", "assets/audio/ThemeMusic.mp3");
            
            this.load.image("firstInstruction", "assets/water/instructionsScreen.png");
            this.load.image("sequence", "assets/water/sequence.png");
            /* player */

            this.load.atlasJSONArray('player', 'assets/water/player/froward.png', 'assets/water/player/forward.json');
            this.load.atlasJSONArray('jump', 'assets/water/player/jump.png', 'assets/water/player/jump.json');
            this.load.atlasJSONArray('forwardSpeed', 'assets/water/player/charging.png', 'assets/water/player/charging.json');

            this.load.atlasJSONArray('miganie', 'assets/water/player/miganie.png', 'assets/water/player/miganie.json');
            this.load.atlasJSONArray('crashforward', 'assets/water/player/crashforward.png', 'assets/water/player/crashforward.json');



            for (var i: number = 1; i < 7; i++) {
                this.load.atlasJSONArray('tricki'+i, 'assets/water/player/trick'+i+'.png', 'assets/water/player/trick'+i+'.json');
            }

            this.load.atlasJSONArray('special', 'assets/water/special.png', 'assets/water/special.json');
            this.load.atlasJSONArray('jumpbtn', 'assets/water/jumpBtn.png', 'assets/water/jumpBtn.json');


            this.load.image('bgletter', 'assets/water/letter/bgletter.png');
            
            this.load.image("bgClock", "assets/water/interface.png");


            /* obstacle */

            this.load.image("piasek", "assets/water/obstacle/piasek.png");
            this.load.image("wood", "assets/water/obstacle/wood.png");
            this.load.image("hammer", "assets/water/obstacle/hammer.png");
            this.load.image("turtle", "assets/water/obstacle/turtle.png");
            this.load.image("zielone", "assets/water/obstacle/zielone.png");
            this.load.image("ball", "assets/water/obstacle/ball.png");
            this.load.image("ball2", "assets/water/obstacle/ball2.png");
            this.load.image("walen", "assets/water/obstacle/walen.png");
            this.load.atlasJSONArray('popletter', 'assets/water/letter/popletter.png', 'assets/water/letter/popletter.json');
           


            this.load.atlasJSONArray('shark1', 'assets/water/obstacle/shark.png', 'assets/water/obstacle/shark.json');
            this.load.atlasJSONArray('shark2', 'assets/water/obstacle/shark2.png', 'assets/water/obstacle/shark2.json');
            this.load.atlasJSONArray('shark3', 'assets/water/obstacle/shark3.png', 'assets/water/obstacle/shark3.json');
            this.load.atlasJSONArray('shark4', 'assets/water/obstacle/shark4.png', 'assets/water/obstacle/shark4.json');

            this.load.atlasJSONArray('osmiornica', 'assets/water/obstacle/osmiornica.png', 'assets/water/obstacle/osmiornica.json');


          //  this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");

         

            for (var i: number = 1; i < 5; i++) {

                this.load.audio("fail" + i, "assets/audio/instruction/fail" + i + ".mp3");


            }

            for (var i: number = 1; i < 5; i++) {

                this.load.audio("success" + i, "assets/audio/instruction/success" + i + ".mp3");


            }

       
        /*    for (var i: number = 1; i < 4; i++) {
                this.load.image("item"+i, "assets/robo/item"+i+".PNG");
            }

            for (var i: number = 1; i < 4; i++) {
                this.load.image("robot" + i, "assets/robo/robot" + i + ".PNG");
            }
            */
         


            /*end skrzynie"*/


          

            /*  end roboty */

          


         

            this.load.atlasJSONArray("scoreScreen", "assets/scoreScreen/scoreScreen.png", "assets/scoreScreen/scoreScreen.json");
            this.load.atlasJSONArray("playScoreScreen", "assets/scoreScreen/scoreScreenButtons.png", "assets/scoreScreen/scoreScreenButtons.json");


            for (var i: number = 1; i < 6; i++) {

                this.load.atlasJSONArray("fail" + i, "assets/feedbacks/fail" + i + ".png", "assets/feedbacks/fail" + i + ".json");



            }


            for (var i: number = 1; i < 5; i++) {

                this.load.atlasJSONArray("success" + i, "assets/feedbacks/success" + i + ".png", "assets/feedbacks/success" + i + ".json");



            }

            for (var i: number = 1; i < 5; i++) {
                this.load.audio("congrats" + i, "assets/feedbacks/congrats" + i + ".mp3");



            }

            this.load.audio("failure1", "assets/feedbacks/failure1.mp3");
            this.load.audio("failure2", "assets/feedbacks/failure2.mp3");


        }


        create() {
           
            this.game.state.start("MainMenu", true, false);

        }

    }


} 

var mobile;