﻿module Water {

    export class Player extends Phaser.Sprite {


        private _gameControl: GameControler = GameControler.getInstance();


        private _crashForward: Phaser.Sprite;
        private _forward: Phaser.Sprite;
        private _jump: Phaser.Sprite;
        private _forwardSpeed: Phaser.Sprite;
        private _miganie: Phaser.Sprite;

        private electroAudio: Phaser.Sound;
        private electroAudio2: Phaser.Sound;
        private crashAudio: Phaser.Sound;
        private crashAudio2: Phaser.Sound;
        private jumpAudio: Phaser.Sound;
        private holeAudio: Phaser.Sound;
        private _currentAnim: Phaser.Sprite;
        private _currentTrickiCount: number = 0;
        private _tricki: Phaser.Sprite;
        constructor(game: Phaser.Game, x: number, y: number, key: string) {

            super(game, x, y, key);

          //  this.anchor.setTo(0.5, 0.5);
          
            game.physics.enable(this, Phaser.Physics.ARCADE);


            this._crashForward = new Phaser.Sprite(game, 0, 0, "crashforward");
            this._crashForward.animations.add("play");

            this._jump = new Phaser.Sprite(game, 0, 0, "jump");
            this._jump.animations.add("play");

            this._forward = new Phaser.Sprite(game, 0, 0, "player");
            this._forward.animations.add("play");


            this._forwardSpeed = new Phaser.Sprite(game, 0, 0, "forwardSpeed");
            this._forwardSpeed.animations.add("play");

            this._miganie = new Phaser.Sprite(game, 0, 0, "miganie");
            this._miganie.animations.add("play");
            

          /*  this.animations.add("forward", this.countFrames(1, 19), 24, true);
            this.animations.add("fastforward", this.countFrames(20, 34), 24, false);

            this.animations.add("jump", this.countFrames(35, 88), 24, false);
            this.animations.add("jumptrick", this.countFrames(89, 136), 24, false);
        */

            this.electroAudio = this.game.sound.add("electro", 1, false, true);
            this.electroAudio2 = this.game.sound.add("electro1", 1, false, true);

            this.crashAudio = this.game.sound.add("crash", 1, false, true);
            this.crashAudio2 = this.game.sound.add("crash2", 1, false, true);
            this.jumpAudio = this.game.sound.add("jumpAudio", 1, false, true);
            this.holeAudio = this.game.sound.add("holeAudio", 1, false, true);

        }


        jump() {

            this.addAnim(this._jump);
          
            this._currentAnim.animations.play("play", 24, false, false);
         
          
            this._currentAnim.animations.getAnimation("play").onComplete.add(function () { this.endJump();}, this);


        }

        endJump() {
            this.addAnim(this._forward);
            this._currentAnim.animations.play("play", 24, true, false);
            this._gameControl.collisionPlayer = false;
        }

        jumptrick() {
            if (this._currentTrickiCount == 6) {
                this._currentTrickiCount = 0;
            }
                this._gameControl.level.addCredits();
                this._currentTrickiCount++;
                console.log("trocki cout", this._currentTrickiCount);
                this._tricki = new Phaser.Sprite(this.game, 0, 0, "tricki" + this._currentTrickiCount, 0);
                this._tricki.animations.add("play");

                this.addAnim(this._tricki);

                this._currentAnim.animations.play("play", 24, false, false);


                this._currentAnim.animations.getAnimation("play").onComplete.add(function () { this.jumptrickiend() }, this);
            

        }

        jumptrickiend() {
           
            this.addAnim(this._forward);
            this._tricki.kill();
            this._tricki = null;
            this._currentAnim.animations.play("play", 24, true, false);
            this._gameControl.collisionPlayer = false;


        }

        crashForward() {

            this.addAnim(this._crashForward);
          
            this._currentAnim.animations.play("play",24, false);
            this._currentAnim.animations.getAnimation("play").onComplete.add(function () { this.crashForwardOver(); }, this);

        }

        crashForwardOver() {
            this.addAnim(this._miganie);
            this._currentAnim.animations.play("play", 24, false);
            this._currentAnim.animations.getAnimation("play").onComplete.add(function () { this.activePlayer(); }, this);
           // this.game.add.tween(this._currentAnim).to({ alpha: 0 }, 0.5, Phaser.Easing.Linear.None, false, 0.5, 5).start();
            //var tweenAn =  this.game.add.tween(this._currentAnim).to({ alpha: 1 }, 5, Phaser.Easing.Linear.None,false).start();
          //  this.game.time.events.add(Phaser.Timer.SECOND * 4, this.activePlayer, this)
          //  tweenAn.onComplete.add(this.activePlayer,this);

        }

        activePlayer() {
            console.log("aktywne");
            this._gameControl.collisionPlayer = false;

        }

        forward() {

            this.addAnim(this._forward);
            this._currentAnim.animations.play("play", 24, true);
            


        }

        forwardSpeed() {
           
            this.addAnim(this._forwardSpeed);
            this._currentAnim.animations.play("play", 24, true);

        }


        addAnim(anim:Phaser.Sprite) {

            if (this._currentAnim != null) {

                this.removeChild(this._currentAnim);

            }

            this._currentAnim = anim;
          //  this._currentAnim.scale.setTo(0.5, 0.5);
            this._currentAnim.anchor.setTo(0.5, 0.5);
            this._currentAnim.animations.stop("play", true);
            this.addChild(this._currentAnim);

        }

        fastforward() {

            this.animations.play("fastforward", 24, false, false);
         

        }
       
       

        playAgain() {


            this._gameControl.collisionPlayer = false;


        }

    


        countFrames(start: number, num: number): Array<number> {
            var countArr: Array<number> = new Array();
            for (var i: number = start; i < num + 1; i++) {

                countArr.push(i);


            }

            return countArr;


        }



    }


} 