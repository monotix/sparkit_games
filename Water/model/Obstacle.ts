﻿module Water {

    export class Obstacle extends Phaser.Sprite{

        private _haveAnim:boolean
        constructor(game:Phaser.Game,x:number,y:number,key:string) {

            super(game, x, y, key);
            this.name = key;

            if (this.name == "osmiornica" || this.name == "shark" || this.name == "shark2" || this.name == "shark3" || this.name == "shark4") {
                this._haveAnim = true;
                this.animations.add("anim");
                this.animations.play("anim", 24, true);
                this.y += 305;
            }
            else if (this.name == "boatsmedium") {

                this.frame = this.getRandom(this.animations.frameTotal);
                this.y += 50;

            }
            else if (this.name == "boatssmall") {
                this.y += 100;

            }
            else {

                this.y += 305;
            }
            
        }

        getRandom(max: number): number {

            return Math.floor(Math.random() * max);

        }


        stopAnim() {

            if (this._haveAnim) {

                this.animations.stop("anim");

            }
            else {
                console.log("nie ma animacji", this.name);
            }

        }
    }

} 