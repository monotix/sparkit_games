﻿module Water {
    export class ShowLetter extends Phaser.Sprite {


     
      //  private _letters: Letters = Letters.getInstance();
        private _letterShow: Phaser.Text;
        private _gameControler: GameControler = GameControler.getInstance();
        private _letterControler: LetterControler;
        private _letterD: Phaser.Sound;
        private _letterB: Phaser.Sound;
        private _letterP: Phaser.Sound;
        private failArr: Array<Phaser.Sound>;
    
        private _currentMax: number;
        private _currentCount: number;
        constructor(game: Phaser.Game, x: number, y: number, key: string) {

            super(game, x, y, key);
         
            this._letterControler = LetterControler.getInstance();

            this.y = this.game.height / 2 - this.height / 2;
            this.x = this.game.width / 2 - this.width / 2;
          

        }

       

       

        showLetters() {
            if (this._currentMax == this._currentCount) {

                console.log("hideMe");
                this.visible = false;
                this._gameControler.collisionPlayer = false;
                this._gameControler.level.stopGame(false);
                this._gameControler.level.blokada(false);
            }
            else {
                this._gameControler.level.blokada(true);
                this.showLetter(this._letterControler.currentLetters[this._currentCount]);
                this.game.time.events.add(Phaser.Timer.SECOND * 2, function () {

                    this.removeChild(this._letterShow);
                  



                }, this)
                this.game.time.events.add(Phaser.Timer.SECOND * 2.5, function () {

                  
                    this.showLetters();



                }, this)
                this._currentCount++;
            }


        }


        public addBgToDisplayLetter() {

            this._currentMax = this._letterControler.currentLetters.length;
            this._currentCount = 0;
            this.showLetters();

       
          
        }

       
        private showLetter(str: string) {

           

                var style = { font: "120px Arial", fill: "#000000", align: "center" };
                if (this._letterShow) this._letterShow.destroy();
                this._letterShow = new Phaser.Text(this.game, 0, 0, str, style);

                this._letterShow.x = this.width / 2 - this._letterShow.width / 2;
                this._letterShow.y = this.height / 2 - this._letterShow.height / 2+20;
             
                this.addChild(this._letterShow);

            

         

        }



      
        public playAgain() {
           
            // this._gameControler.hideLetterOnGame = true;
           // this._gameControler.hideLetterOnGame = true;

          //  this.failArr[this._gameControler._badLetters].onStop.add(this.playAgainAfterSound, this);
          //  this.failArr[this._gameControler._badLetters].play();



        }

    }

}  