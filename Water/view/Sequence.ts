﻿module Water {

    export class Sequence extends Phaser.Sprite {

        private _collectLetters: Array<string> = new Array();
        private _contenerLetter: Phaser.Group;
        constructor(game: Phaser.Game, x: number, y: number, key: string) {

            super(game, x, y, key);

            this._contenerLetter = new Phaser.Group(game);
            this._contenerLetter.y = 15;
            this._contenerLetter.x = 0;
            this.addChild(this._contenerLetter);


        }


        public addLetter(letter: string) {

            var style = { font: "40px Arial", fill: "#000000", align: "center" };

            var letterShow = new Phaser.Text(this.game, 0, 0, letter, style);

            letterShow.x = this._contenerLetter.width + 20;
            this._contenerLetter.addChild(letterShow);

            this._collectLetters.push(letter);



        }

        public removeLetters() {

            this._contenerLetter.removeAll();
            this._collectLetters.splice(0, this._collectLetters.length);

        }







    }


}  