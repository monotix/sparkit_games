﻿module Water {

    export class ObstacleView extends Phaser.Group {

     

        private _longerObstacle: Array<any>;
        private _game: Phaser.Game;
        private _obstacleArr: Array<any>;
        private _whenAddNewScreen: Obstacle;
        private _gameControler: GameControler;
        private _obstacleControl: ObstacleControler;
        private _letterControler: LetterControler;
        constructor(game: Phaser.Game) {

            super(game);
            this._game = game;
            this._gameControler = GameControler.getInstance();
            this._letterControler = LetterControler.getInstance();
          


            this.enableBody = true;

         
            this._obstacleControl = ObstacleControler.getInstance();

        }






        randomBoats(): Phaser.Sprite {

       //     var rand = this.getRandom(this.boatsStrArr.length)
       //     var str: string = this.boatsStrArr[rand];

         //   var y_ = (rand == 0) ? 100 : 50;

        //    var boats: Phaser.Sprite = new Phaser.Sprite(this._game, 0, y_, str, 0);
        //    boats.frame = this.getRandom(boats.animations.frameTotal);

            return ;

        }

        getObstacle() {
            this._obstacleArr = new Array();
            this._longerObstacle = new Array();

            var obstcaleArr: Array<any> = this._obstacleControl.level1();
            var max: number = obstcaleArr.length;
            for (var i: number = 0; i < max; i++) {

                var obj = obstcaleArr[i];

                if (obj.name == "popletter") {

                    var popletter: PopLetter = new PopLetter(this._game, obj.x + 600, obj.y, obj.name);
                    popletter.addLetter(this._letterControler.getPopLetter());
                    this.getTheLonger(popletter.x, popletter.width, i);
                    this.add(popletter);
                    popletter.body.velocity.x = this._gameControler.SPEEDBOAT;
                    this._obstacleArr.push(popletter);

                }
                else {

                  
                    var obstacle: Obstacle = new Obstacle(this._game, obj.x + 600, obj.y, obj.name);
                    this.getTheLonger(obstacle.x, obstacle.width, i);
                    
                    this.add(obstacle);
                    obstacle.body.velocity.x = this._gameControler.SPEEDBOAT;
                    if (obj.name == "boatssmall" || obj.name == "boatsmedium") {

                        obstacle.body.setSize(0, 0, 0, 0);
                        obstacle.body.velocity.x = -70;
                    }

                    if (obj.name == "wood") {

                        obstacle.body.setSize(25,200, 50, 0);

                    }
                   
                    this._obstacleArr.push(obstacle);


                }
               


            }

            var obstacleFarAway = this._obstacleArr[this._longerObstacle[0][1]];
            //obstacleFarAway.checkWorldBounds = true
            this._whenAddNewScreen = obstacleFarAway;
            console.log(obstacleFarAway);
          //  obstacleFarAway.events.onOutOfBounds.add(this.obstacleOutOfScreen, this);


        }

        getTheLonger(posx:number,width_:number, i:number) {

            if (this._longerObstacle.length > 0) {

                if (this._longerObstacle[0][0] < posx + width_) {

                    this._longerObstacle[0] = new Array(posx + width_, i);

                }
            }
            else {

                this._longerObstacle.push(new Array(posx + width_,i));

            }

        }
      

       
        

       obstacleOutOfScreen() {
            console.log("boats out of screen");
            this.delObstacle();
            this.getObstacle();
        }

        delObstacle() {


            for (var i: number=0; i < this._obstacleArr.length; i++) {

                if (this._obstacleArr[i].world.x + this._obstacleArr[i].width < 0) {
                    this._obstacleArr[i].destroy(true);
                    this._obstacleArr.splice(i, 1);

                    --i;


                }

            }

        }

        changeSpeed(stop:boolean=false) {
            var speed = (stop) ? 0 : this._gameControler.SPEEDBOAT;
          
            this.forEach(function (item) {
                item.body.velocity.x = speed;
                if (!stop && (item.name == "boatsmedium" || item.name == "boatssmall")) item.body.velocity.x = -70;
              
            }, this);


        }

        fastForward(fast: boolean = false) {
            if (!this._gameControler.collisionPlayer) {
                var speed = -130;
                if (!fast) speed = -100;
                this._gameControler.SPEEDBOAT = speed;
                this.forEach(function (item) {
                    item.body.velocity.x = speed;

                }, this);
            }

        }

        removeObstacleLetters() {


            this.forEach(function (item) {
                if(item != undefined && item.name == "popletter") item.destroy(true);

            }, this);


        }

        removeObstacle() {
          

            this.forEach(function (item) {
                if (item != undefined )  item.destroy(true);

            }, this);


        }



        getRandom(max: number):number {

            return Math.floor(Math.random() * max);

        }

        
        update() {
          

            if (this._whenAddNewScreen != null && this._whenAddNewScreen.world.x < 300) {

                this.obstacleOutOfScreen();

            }

        }

    }


} 