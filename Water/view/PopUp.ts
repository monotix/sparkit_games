﻿module Water {

    export class PopUp extends Phaser.Sprite {

        private logo;
        private _game: Phaser.Game;
        private gamesBtn: Phaser.Button;
        private playBtn: Phaser.Button;
        private level1: any;
        private overButtonA: Phaser.Sound;
        constructor(game: Phaser.Game, level: any, x: number, y: number) {
            super(game, x, y);
            this.level1 = level;
            this._game = game;

            this.overButtonA = this.game.add.audio("overButtonA", 1, false);

            var bgPopup: Phaser.Sprite = new Phaser.Sprite(this.game, 0, 0, "popup");
            this.addChild(bgPopup);
            this.logo = new Phaser.Sprite(this.game, 0, 0, "logoGame");
            this.addChild(this.logo);

            this.logo.x = (bgPopup.width / 2 - this.logo.width / 2);
            this.logo.y = this.logo.height - 20;



            var text = "Good try.\n Keep playing to win credits";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };

            var textDescription = new Phaser.Text(this.game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;

            textDescription.x = 400 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 100 + (140 / 2 - textDescription.height / 2);

            this.addChild(textDescription);

            this.textLevel();
            //this.beatText();
       

            this.addButtons();

            this.visible = false;
        }
        addButtons() {
            this.gamesBtn = new Phaser.Button(this._game, 0, 0, "games", this.gamesBtnClick, this, 1, 0);
            this.playBtn = new Phaser.Button(this._game, 100, 0, "playGame", this.playBtnClick, this, 1, 0);

            this.gamesBtn.inputEnabled = true;
            this.gamesBtn.input.priorityID = 10;
            this.playBtn.inputEnabled = true;
            this.playBtn.input.priorityID = 10;
            var buttonsGr = new Phaser.Group(this._game);
            this.addChild(buttonsGr);

            buttonsGr.add(this.gamesBtn);
            buttonsGr.add(this.playBtn);


            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;


        }

        public activeButton() {

            this.gamesBtn.inputEnabled = true;

            this.playBtn.inputEnabled = true;

        }

        gamesBtnClick() {
            this.overButtonA.play();

        }

        playBtnClick() {

            this.overButtonA.play();

            this.visible = false;
            this.gamesBtn.inputEnabled = false;
         //   this.gamesBtn.input.priorityID = 3;
            this.playBtn.inputEnabled = false;
           // this.playBtn.input.priorityID = 3;

            this.level1.hideScreenAfterFinishPlaye();
            //  this.game.state.start("PopupStart", true, false);

        }



        textLevel() {

            var text = "GAME OVER";
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };

            var textDescription = new Phaser.Text(this._game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
          

            textDescription.x = this.game.width / 2 - textDescription.width / 2
            textDescription.y = this.logo.y + this.logo.height + 25;
            this.addChild(textDescription);

        }




    }


} 