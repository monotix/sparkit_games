﻿module Water {

    export class PromptView extends Phaser.Group {

        private _textView: Phaser.Text;
        private _kayak: Phaser.Sprite;
        private _audio: Phaser.Sound;
        private _parent: Level;
        constructor(game: Phaser.Game, parent:Level) {
            super(game);
            game.add.existing(this);

          
            this._parent = parent;
            this._kayak = this.game.add.sprite(0, 0, 'prompt', 0);
            this._kayak.animations.add('anim');
            this.addChild(this._kayak);

            var style = { font: "18px Arial", fill: "#000000", align: "left" };
            this._textView = this.game.add.text(17, 155, "",style);
            this.addChild(this._textView);
            this.visible = false;

            this.y = this.game.height - this.height;
            this.x = this.game.width - this.width;
           
        }


       
        
        public playIntro() {
            this._parent.blokada(true);
            this.visible = true;
            this._kayak.animations.play('anim', 31, true, false);
            this._textView.setText("Collect the letters in the order that\nthey are shown. Are you Ready? Let's\ngive it a go!!");
            this._audio = this.game.add.audio('WakeIntro', 1, false);
            this._audio.play();
            this._audio.onStop.addOnce(this.onAudioComplete, this);
        }

        public playFeedbackGood(nextLevel: boolean = false) {
            if (!nextLevel) {

            this._parent.blokada(true);
            this.visible = true;
            this._kayak.animations.play('anim', 31, true, false);
            this._textView.setText("Holy Moley!  I'm Impressed!\nThe Next Sequence is...");
            this._audio = this.game.add.audio('WakeSuccess0', 1, false);
            this._audio.play();
            this._audio.onStop.addOnce(this.onAudioComplete, this);
              
            }
            else {
                this._parent.showFinishLevel();
            }
        }

        public playFeedbackWrong(end: boolean = false) {
            this._parent.blokada(true);
            this.visible = true;
            this._kayak.animations.play('anim', 31, true, false);
            this._textView.setText("That was a fair go, but let's try again.\nHave you found a way to remember\nthe sounds yet?");
            this._audio = this.game.add.audio('WakeWrong0', 1, false);
            this._audio.play();
            this._audio.onStop.addOnce(this.onAudioComplete, this);

            if (end) {
                this._audio.onStop.addOnce(this.onAudioCompleteEnd, this);
            }
            else {
                this._audio.onStop.addOnce(this.onAudioComplete, this);
            }
        }

        private onAudioComplete() {
            this.visible = false;
            this._parent.onPromptAudioComplete();
        }

        private onAudioCompleteNextLevel() {
            this.visible = false;
            this._parent.showFinishLevel();
        }

        private onAudioCompleteEnd() {
            this.visible = false;
            this._parent.showFinish();
        }
    }
} 