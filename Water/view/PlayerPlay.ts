﻿module Water {
    export class PlayerPlay extends Phaser.Group {

        private _gameControler: GameControler;
        private _obstacleControl: ObstacleControler;
        private _game: Phaser.Game;
        private _player: Player;
        private _tween: Phaser.Tween = null;
        private cursors;
        constructor(game: Phaser.Game) {

            super(game);
            this._game = game;
            this._gameControler = GameControler.getInstance();




            this.enableBody = true;


            this._obstacleControl = ObstacleControler.getInstance();
            if (mobile) {
               
                this.game.input.onDown.add(this.movePlayer, this);
            }
            else {
                this.cursors = this.game.input.keyboard.createCursorKeys();
              

            }
            this.addPlayer();
        }

        addPlayer() {

            this._player = new Player(this._game, 0, 0, "");

            this._player.x = 400;// this._player.width - 400
            this._player.y =300;
            this.add(this._player);

            this._player.body.setSize(100, 20, -320, 80);
           
            this._player.forward();

        }


        movePlayer(pointer) {

            if (!this._gameControler.collisionPlayer) {


             





                var posy: number = pointer.y
                if (posy < 300) posy = 300;
                else if (posy > 500) posy = 500;
                if (pointer.x > 630) return;
               this.stopTween();

                if (!this._gameControler.collisionPlayer) {
                    var duration = (this.game.physics.arcade.distanceToPointer(this._player, pointer) / 200) * 1000;


                    this._tween = this.game.add.tween(this._player).to({ y: posy }, duration, Phaser.Easing.Linear.None).start();
                   this._tween.onComplete.add(this.endMove, this);

                }

            }


        }


        endMove() {
          //  this._player.playerPlay();

        }

        tweenStop() {

            if (this._tween != null) {

                this._tween.stop();

            }

        }



        stopTween() {

            if (this._tween != null && this._tween.isRunning) {
                this.game.tweens.remove(this._tween);

            }

           

        }

        public get player(): Player {

            return this._player;
        }


        update() {
            if (!mobile) {
                if (this.cursors.up.isDown) {
                    console.log("nciska w gore");
                    this._player.y -= 2;
                }
                else if (this.cursors.down.isDown) {
                    console.log("nciska w dol");
                    this._player.y += 2;

                }
            }

            if (this._player.y < 300) {
                this._player.y = 299;
            }
            else if (this._player.y > 500) {
           
               this._player.y = 499;
            }


        }

    }

}