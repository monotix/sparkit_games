﻿module Water {

    export class GameControler {


        public static _instance: GameControler = null;

        public SPEEDBOAT: number = -100;
        public SPEEDROBOT: number = 100;

        public finishGame: boolean = false;
        private _currenLevel: number = 0;
        private _level: Level;
        public collisionPlayer: boolean = false;
        public gameOver: boolean = true;
        public maxLevel: number = 3;
        public currentLevel: number = 0;
        construct() {

            if (GameControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            GameControler._instance = this;

        }




        public static getInstance(): GameControler {

            if (GameControler._instance === null) {

                GameControler._instance = new GameControler();


            }


            return GameControler._instance;



        }

        public restart() {
            this._currenLevel = 0;
            this.level.playAgain();
        }

        public nextLevel() {

            this.currentLevel++;

            this.level.showPrompt(true, true);

        }

        setLevel(level: Level) {

            this._level = level;

        }

        public get level(): Level {

            return this._level;
        }

     

    }


}  