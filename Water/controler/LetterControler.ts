﻿module Water {

    export class LetterControler {


        public static _instance: LetterControler = null;
        private _currentLetters: Array<string>;
        private _currentWorld: string;
        private _maxLetter: number;
        private _corretAnswer: number = 0;
        private _maxBadAnswer: number = 3;
        private _badAnswer: number = 0;
        private _gameControler: GameControler = GameControler.getInstance();
        private _currentSequence: number = 0;
        private _maxSequence: number = 2;
        construct() {

            if (LetterControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            LetterControler._instance = this;
          
        }





        public static getInstance(): LetterControler {

            if (LetterControler._instance === null) {

                LetterControler._instance = new LetterControler();


            }


            return LetterControler._instance;



        }


        public phonemes: Array<string> = new Array(
            "a", "ai", "ay", "a", "aigh", "al", "au", "aw", "augh", "ar",
            "b", "bb", "bt", "c", "ch", "ck", "ce", "d", "ed", "dd", "dge",
            "ey", "eigh", "e", "ea", "ee", "ei", "el", "ew", "ear", "er",
            "f", "ff", "ph", "g", "gg", "gh", "ge", "gn", "h", "i", "ie", "il", "ir",
            "ie", "igh", "j", "k", "kn", "l", "ll", "le", "m", "mm", "mb", "mn",
            "n", "nn", "o", "ough", "oa", "oe", "ow", "ou", "oo", "or",
            "p", "pp", "ph", "pn", "pt", "qu", "r", "rr", "rh", "re", "s", "ss", "se",
            "st", "sc", "sh", "s", "se", "t", "tt", "th", "tch", "u", "ue", "ui", "ur",
            "v", "ve", "w", "wh", "wr", "x", "y", "yr", "z", "zz", "ze"
            );


        public phonemeSounds: Array<string> = new Array(
            "a", "a_e", "aw", "b", "c", "ch", "d", "e", "ee", "er", "f", "g", "h",
            "i", "i_e", "j", "k", "l", "m", "n", "o", "o_e", "oo1", "oo2",
            "ow", "oy", "p", "qu", "r", "s", "sh", "t", "th", "th2", "u", "u_e", "v",
            "w", "x", "y", "z"
            );



        public randomPhoneme(): string {
            var i: number = Math.floor(Math.random() * this.phonemes.length);
            return this.phonemes[i];
        }


        setRandomLetters(howmany:number=1) {
            howmany = this._gameControler.currentLevel + 1;
            this._currentLetters = new Array();
            console.log("ile liter do zapamietania", howmany, this._gameControler.currentLevel);
            for (var i: number = 0; i < howmany; i++) {
                var num = this.getRandomNumber(this.phonemes.length-1);
                console.log("phenomes", num);
                this._currentLetters.push(this.phonemes[num]);


            }
            console.log("current", this._currentLetters);
            this._currentWorld = this._currentLetters[0];
            this._maxLetter = this._currentLetters.length;


        }

        checkWord(str:string) {

            if (str == this._currentLetters[this._corretAnswer]) {
                console.log("dobra liltera");
                this._corretAnswer++;
                this.checkWinOrDie(false);
            }
            else {
                this._badAnswer++;
                this.checkWinOrDie(true);
                console.log("zla liltera");
            }

        }

        checkWinOrDie(die:boolean) {
            console.log("die", this._badAnswer, "goog", this._corretAnswer);
            if (this._badAnswer == this._maxBadAnswer) {
                this._gameControler.collisionPlayer = true;
                console.log("die you");
                this._gameControler.level.showPrompt(false,true);
                this._corretAnswer = 0;
                this._badAnswer = 0;
                return;

            }
            else if(this._corretAnswer == this._maxLetter) {

                this._currentSequence++;
                this._badAnswer = 0;
                if (this._currentSequence != this._maxSequence) {
                    this._corretAnswer = 0;
                    this.setRandomLetters(this._gameControler.currentLevel + 1);
                    this._gameControler.level.showPrompt(true);
                    this._gameControler.level.sequenceLevel(this._currentSequence + 1 + "/" + 2);
                   
                }
                else {
                    this._currentSequence = 0;
                    this._gameControler.collisionPlayer = true;
                    this._gameControler.nextLevel();
                    this.setRandomLetters();
                    console.log("you win Level");
                }
               
                return;
            }

            if (die) {
                this._corretAnswer = 0;
                this._gameControler.level.showPrompt(false);
                this._gameControler.level.removeSequence();
            }
            else {
               


            }



        }



        getRandomNumber(max: number, min:number=0): number {
            //--max;
            return Math.floor(Math.random() * (max - min) + min);

        }
    
        getPopLetter(): string {

            var randNum: number = this.getRandomNumber(2);
         
            console.log("random number letter", randNum);
            var str: string;
            if (randNum == 1) {

                str = this.phonemes[this.getRandomNumber(this.phonemes.length)];

            }
            else {

                var num: number = this.getRandomNumber(this._currentLetters.length);
             
                str = this._currentLetters[num];
               
            }

           
          
            


            return str;

        }

        public get currentLetters():Array<string> {

            return this._currentLetters;

        }


         public static  words: Array<any> = new Array(
        {
            // level 1 english words
            english: new Array
                (
                { word: ["ai", "r"], sounds: ["a_e", "r"] },
                { word: ["a", "l", "e"], sounds: ["a_e", "l", "split"] },
                { word: ["a", "m"], sounds: ["a", "m"] },
                { word: ["a", "n"], sounds: ["a", "n"] },
                { word: ["a", "n", "d"], sounds: ["a", "n", "d"] },
                { word: ["a", "n", "t"], sounds: ["a", "n", "t"] },
                { word: ["a", "r", "m"], sounds: ["o", "r", "m"] },
                { word: ["a", "r", "t"], sounds: ["o", "r", "t"] },
                { word: ["a", "s"], sounds: ["a", "z"] },
                { word: ["a", "sh"], sounds: ["a", "sh"] },
                { word: ["a", "s", "k"], sounds: ["a", "s", "k"] },
                { word: ["a", "t"], sounds: ["a", "t"] },
                { word: ["b", "a", "d"], sounds: ["b", "a", "d"] },
                { word: ["b", "a", "g"], sounds: ["b", "a", "g"] },
                { word: ["b", "a", "r"], sounds: ["b", "o", "r"] }
                )
        },


        {
            // level 2 english words
            english: new Array
                (
                { word: ["a", "b", "j", "e", "c", "t"], sounds: ["a", "b", "j", "e", "k", "t"] },
                { word: ["a", "b", "le"], sounds: ["a_e", "b", "l"] },
                { word: ["a", "b", "oa", "r", "d"], sounds: ["u", "b", "o_e", "r", "d"] },
                { word: ["a", "b", "o", "r", "t"], sounds: ["u", "b", "o_e", "r", "t"] },
                { word: ["a", "b", "ou", "n", "d"], sounds: ["u", "b", "ow", "n", "d"] },
                { word: ["a", "b", "o", "v", "e"], sounds: ["u", "b", "u", "v", "split"] },
                { word: ["a", "b", "r", "ea", "s", "t"], sounds: ["u", "b", "r", "e", "s", "t"] },
                { word: ["a", "b", "s", "e", "n", "ce"], sounds: ["a", "b", "s", "e", "n", "s"] },
                { word: ["a", "b", "s", "o", "l", "ve"], sounds: ["u", "b", "z", "o", "l", "v"] },
                {
                    word: ["a", "b", "s", "t", "r", "a", "c", "t"],
                    sounds: ["a", "b", "s", "t", "r", "a", "k", "t"]
                },
                { word: ["a", "b", "s", "ur", "d"], sounds: ["u", "b", "s", "er", "d"] },
                { word: ["a", "b", "u", "se"], sounds: ["u", "b", "u_e", "s"] },
                { word: ["a", "b", "y", "ss"], sounds: ["u", "b", "i", "s"] },
                { word: ["a", "c", "c", "e", "p", "t"], sounds: ["a", "c", "s", "e", "p", "t"] },
                { word: ["a", "c", "c", "l", "ai", "m"], sounds: ["u", "c", "c", "l", "a_e", "m"] },
                { word: ["a", "ce"], sounds: ["a_e", "s"] }
                )
        },


        {
            // level 3 english words
            english: new Array
                (
                {
                    word: ["a", "b", "d", "o", "m", "i", "n", "al"],
                    sounds: ["u", "b", "d", "o", "m", "i", "n", "l"]
                },
                { word: ["a", "b", "l", "y"], sounds: ["a_e", "b", "l", "ee"] },
                {
                    word: ["a", "b", "o", "l", "i", "sh", "ed"],
                    sounds: ["u", "b", "o", "l", "i", "sh", "d"]
                },
                {
                    word: ["a", "b", "ou", "t"],
                    sounds: ["a", "b", "ow", "t"]
                },
                {
                    word: ["a", "b", "r", "a", "s", "i", "ve"],
                    sounds: ["u", "b", "r", "a_e", "s", "i", "v"]
                },
                { word: ["a", "b", "s", "e", "n", "t"], sounds: ["a", "b", "s", "e", "n", "t"] },
                {
                    word: ["a", "b", "u", "n", "d", "a", "n", "ce"],
                    sounds: ["u", "b", "u", "n", "d", "u", "n", "s"]
                },
                {
                    word: ["a", "b", "u", "n", "d", "a", "n", "t"],
                    sounds: ["u", "b", "u", "n", "d", "u", "n", "t"]
                },
                {
                    word: ["a", "b", "u", "s", "i", "ve"],
                    sounds: ["u", "b", "u_e", "s", "i", "v"]
                },
                {
                    word: ["a", "c", "qu", "i", "tt", "ed"],
                    sounds: ["u", "c", "qu", "i", "t", "d"]
                },
                {
                    word: ["a", "c", "r", "o", "n", "y", "m"],
                    sounds: ["a", "c", "r", "o", "n", "i", "m"]
                },
                { word: ["a", "c", "r", "o", "ss"], sounds: ["o", "c", "r", "o", "s"] },
                {
                    word: ["a", "c", "r", "y", "l", "i", "c"],
                    sounds: ["u", "c", "r", "i", "l", "i", "c"]
                },
                {
                    word: ["a", "c", "t", "i", "v", "i", "s", "m"],
                    sounds: ["a", "c", "t", "i", "v", "i", "z", "m"]
                }
                )
        }
        );
        



    }


}  