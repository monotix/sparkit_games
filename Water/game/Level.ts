﻿module Water {

    export class Level extends Phaser.State {


        private _bg: Phaser.Group;
        private _obstcaleGroup: ObstacleView;
        private _gameControler: GameControler;

        private _letterControler: LetterControler;
        private _prompt: PromptView;

        private _sequenceGr: Phaser.Group;
        private _sequence: Sequence;
        private _playerPlay: PlayerPlay;


        private _showLetter: ShowLetter;
        private _showLetterGr: Phaser.Group;

        private _clock: ClockView;

        private _finishScreenLevel: FinishLevel;
        private _finishScreen: FinishScreen;
        private _special: Phaser.Sprite;
        private _jumpBtn: Phaser.Button;
        private _timer: number = 0;
        private _timerCounter;
        private rightKey;
        private _blokada: Phaser.Graphics; 
        private jumpPressed: boolean = false;
        private _blokadaGr: Phaser.Group;
        private cursors;
   
        private helpGr: Phaser.Group;
        private _helpBtn: Phaser.Button;
        private _finishGr: Phaser.Group;
        private _instructionShow;
        create() {

            this._gameControler = GameControler.getInstance();

            this._gameControler.setLevel(this);

            this._letterControler = LetterControler.getInstance();
            this._letterControler.setRandomLetters();


            this._blokadaGr = this.add.group();

            this._bg = this.game.add.group();
            this.game.add.image(0, 0, "bgGame", 0, this._bg);

            this._obstcaleGroup = new ObstacleView(this.game);

            this.stage.addChild(this._obstcaleGroup);
            this._obstcaleGroup.getObstacle();


            this._playerPlay = new PlayerPlay(this.game);
            this.stage.addChild(this._playerPlay);
            /*
            to jest gadajacy koles. 
            playIntro() - mowi intro
            playFeedbackGood() - mowi jak wykonamy poprawnie sekwencje
            playFeedbackWrong() - mowi jak zrobimy blad
            */



            this._sequenceGr = this.add.group();
            this._sequence = new Sequence(this.game, 0, 0, "sequence");
            this._sequenceGr.add(this._sequence);


            this.stage.addChild(this._blokadaGr);
           

            /*
            to jest zegar
            startTimer() start timera
            stopTimer() stop timera
            setSequenceText(text) dodajemy text w sequence
            setCreditsText(text) dodajemy text w credits. $(dolar) jest dodawany w klasie
            */
            this._clock = new ClockView(this.game, this);
            this._clock.position.setTo(this.game.width - this._clock.width, 0);
            this.stage.addChild(this._clock);

            this._clock.startTimer();
            this._clock.setSequenceText("1/2");
            this._clock.setCreditText("0");
            this.stopGame(true);


            this._showLetterGr = this.game.add.group();
            this.stage.addChild(this._showLetterGr);

            if (mobile) {
                this._special = this.add.sprite(0, 0, "special");
                this._special.y = this.game.height - this._special.height - 30;
                this._special.x = 550;
                this._special.animations.add("play");


                this._jumpBtn = this.add.button(0, 0, "jumpbtn", this.jummBtnDown, this, 1, 0, 1, 0);
                this._jumpBtn.y = this._special.y + 15;
                this._jumpBtn.x = 630;

                this.stage.addChild(this._special);
                this.stage.addChild(this._jumpBtn);
                this._jumpBtn.onInputDown.add(this.jummBtnDown, this);
                this._jumpBtn.onInputUp.add(this.jummBtnUp, this);
            }
       /*     this.game.input.keyboard.onUpCallback = function (e) {
                if (e.keyCode == Phaser.Keyboard.UP) {
                    this.jummBtnUp();
                }
            };*/

            this._prompt = new PromptView(this.game, this);
            this._prompt.position.setTo(this.game.width - this._prompt.width, this.game.height - this._prompt.height);
            this.stage.addChild(this._prompt);
            this._prompt.playIntro();

            if (!mobile) {
                this.cursors = this.game.input.keyboard.createCursorKeys();
            }
          

            this.helpGr = this.add.group();
            this.stage.addChild(this.helpGr);
            this._helpBtn = this.add.button(0, 0, "helpBtn", this.helpBtnClick, this, 1, 0, 0, 0, this.helpGr);
            this.helpGr.x = 0;//this.game.stage.width - this.helpGr.width;
            this.helpGr.y = 600 - this.helpGr.height;

            this._helpBtn.buttonMode = true;
            this._helpBtn.inputEnabled = true;
            this._helpBtn.input.priorityID = 11;



            this.game.input.onDown.add(function () { if (this.game.paused) { this.game.paused = false; } if (this._instructionShow) { this.stage.removeChild(this._instructionShow); this._instructionShow.kill(); this._instructionShow = null; } }, this);
            
        }

        private helpBtnClick() {

            console.log("pokaz instrukcje");
            this.game.paused = !this.game.paused;

            if (this._instructionShow == null) this._instructionShow = this.add.image(0, 0, "firstInstruction", 0);
            

            // this.add.image(0, 0, "firstInstruction", 0);
            this._instructionShow.width = this.game.world.width;//this.stage.width;
            this._instructionShow.height = this.game.world.height;
            this.stage.addChild(this._instructionShow);

        }

        public addCredits() {
             
            this._clock.addCredits();

        }


       public blokada(show:boolean = true) {

            if (this._blokada == null) {
                this._blokada = new Phaser.Graphics(this.game, 0, 0);
                this._blokada.beginFill(0x000000, 0.6);
                this._blokada.drawRect(0, 0, this.game.width, this.game.height);
                this._blokada.endFill();
                this._blokadaGr.add(this._blokada);
                this._blokada.visible = false;
            }
           
            this._blokada.visible = show;
        }

        jummBtnDown() {
            if (!this._gameControler.collisionPlayer) {

                this.jumpPressed = true;
                this._special.animations.play("play");
            }
            
       
            
        }


        jummBtnUp() {
            this.jumpPressed = false;
            console.log("timer tricki", this._timer );

            if (!this._gameControler.collisionPlayer) {
                if (this._timer > 60) {
                    this._gameControler.collisionPlayer = true;
                    this._playerPlay.player.jumptrick();
                }

                else {
                    this._gameControler.collisionPlayer = true;
                    this._playerPlay.player.jump();
                }
                this._timer = 0;

                this._special.animations.stop("play", true);

            }
        }

        

        playAgain(win:boolean = false) {

            this._obstcaleGroup.delObstacle();
            this._obstcaleGroup.removeObstacle();
            this._obstcaleGroup.getObstacle()
            this.stopGame(true);
            this._clock.setCreditText("0");
            this._clock.setSequenceText("1/2");
            if (win) this._prompt.playIntro();
            else this._prompt.playFeedbackWrong();


        }

        /*
        Ta funkcja jest odpalana jak koles skonczy audio
        */
        public onPromptAudioComplete() {
            console.log("Koles skonczyl gadac");
            this.removeSequence();
            this._gameControler.collisionPlayer = true;
            this._showLetter = new ShowLetter(this.game, 0, 0, "bgletter");
            this._showLetterGr.add(this._showLetter);
            
            this._showLetter.addBgToDisplayLetter();
        }

        public sequenceLevel(str:string){
            this._clock.setSequenceText(str);

        }

        public showFinishLevel() {
            console.log("FInish Level:");
            this._finishScreenLevel = new FinishLevel(this.game);
        //    this._finishScreenLevel.x = this.game.width / 2 - this._finishScreenLevel.width / 2;
        //    this._finishScreenLevel.y = this.game.height / 2 - this._finishScreenLevel.height / 2;
            this.stage.addChild(this._finishScreenLevel);
            this.stopGame(true);
        }

        public showFinish() {
          
            this._finishScreen = new FinishScreen(this.game);
       //     this._finishScreen.x = this.game.width / 2 - this._finishScreen.width / 2;
       //     this._finishScreen.y = this.game.height / 2 - this._finishScreen.height / 2;
            this.stage.addChild(this._finishScreen);
            this.stopGame(true);
        }

        public showPrompt(correct: boolean, end: boolean = false) {
            this._gameControler.collisionPlayer = true;
            if (correct) {
                this.stopGame(true);
                this._obstcaleGroup.removeObstacleLetters();
                this._prompt.playFeedbackGood(end);
            }
            else {
                this.stopGame(true);
                this._gameControler.collisionPlayer = true;
                this._prompt.playFeedbackWrong(end);
               
            }
        }

        public removeSequence() {

            this._sequence.removeLetters();
        }
        stopGame(stop:boolean) {

            this._obstcaleGroup.changeSpeed(stop);

        }


        private prawaClick: boolean = false;

        update() {

            if (!this._gameControler.collisionPlayer) {
                this.game.physics.arcade.overlap(this._playerPlay, this._obstcaleGroup, this.collision, null, this);


             

                if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
                    this._gameControler.collisionPlayer = true;
                    this._playerPlay.player.jump();
                    console.log("Jump====>");
                }
                

                if (this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {
                    console.log("PRAWO====>");
                    if (!this.prawaClick) {
                        this.prawaClick = true;
                       // this._gameControler.collisionPlayer = false;
                        this._playerPlay.player.fastforward();
                        this._obstcaleGroup.fastForward(true);
                    }
                }
                else if (!this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {
                    if (this.prawaClick) {
                        this.prawaClick = false;
                      //  this._gameControler.collisionPlayer = false;
                        this._playerPlay.player.forward();
                        this._obstcaleGroup.fastForward(false);
                        this._obstcaleGroup.changeSpeed(false);
                    }
                   

                }

                if (this.jumpPressed) this._timer++;
                
            }
        }


        collision(object, object2) {

            console.log("kolizja",object2.name);
            if (!this._gameControler.collisionPlayer) {

               
                if (object2.name == "popletter") {

                    this._sequence.addLetter(object2.letter);
                    this._letterControler.checkWord(object2.letter);
                    object2.destroy(true);


                }
                else {

                    this._playerPlay.player.crashForward();
                    this._gameControler.collisionPlayer = true;
                }

              
                this._timer = 0;
               if(mobile) this._special.animations.stop("play",true);
            }

        }

        render() {
        /*    
             this.game.debug.quadTree(this.game.physics.arcade.quadTree);
       
             this._obstcaleGroup.forEach(function (item) {
                 // item.body.velocity = 0;
              
                 this.game.debug.body(item);
             }, this)

             this._playerPlay.forEach(function (item) {
                 // item.body.velocity = 0;
              
                 this.game.debug.body(item);
             }, this)

        */

        }

    }
}
