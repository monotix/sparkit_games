var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Water;
(function (Water) {
    var Preloader = (function (_super) {
        __extends(Preloader, _super);
        function Preloader() {
            _super.apply(this, arguments);
        }
        Preloader.prototype.preload = function () {
            var bg = this.add.image(0, 0, "bg", 0);
            var blackProgress = this.add.image(0, 0, "progressBarBlack", 0);
            var blueProgress = this.add.sprite(0, 0, "progressBarBlue", 0);
            blackProgress.position.setTo(this.stage.width / 2 - blackProgress.width / 2, this.stage.height - blackProgress.height - 10);
            blueProgress.position = blackProgress.position;
            this.load.setPreloadSprite(blueProgress);
            this.load.image("mainMenuBg", "assets/mainMenu/SplashScreen.png");
            this.load.atlasJSONArray('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');
            this.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
            this.load.audio("MainMenuSn", "assets/audio/ScubaDudeTheme.mp3");
            this.load.audio("overButtonA", "assets/audio/overButton.mp3");
            this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");
            for (var i = 1; i < 5; i++) {
                this.load.audio("fail" + i, "assets/audio/instruction/fail" + i + ".mp3");
            }
            for (var i = 1; i < 5; i++) {
                this.load.audio("success" + i, "assets/audio/instruction/success" + i + ".mp3");
            }
            this.load.image("item11", "assets/robo/item11.PNG");
            /*    for (var i: number = 1; i < 4; i++) {
                    this.load.image("item"+i, "assets/robo/item"+i+".PNG");
                }
    
                for (var i: number = 1; i < 4; i++) {
                    this.load.image("robot" + i, "assets/robo/robot" + i + ".PNG");
                }
                */
            this.load.image("bg1", "assets/robo/stage/bg_level0.png");
            this.load.image("bg2", "assets/robo/stage/bg_level1.png");
            this.load.image("bg3", "assets/robo/stage/bg_level2.png");
            for (var i = 0; i < 13; i++) {
                this.load.image("itemDrag" + i, "assets/robo/item/s" + i + ".PNG");
            }
            this.load.image("skrzynka", "assets/robo/item/skrzynka.png");
            this.load.atlasJSONArray("skrzynkaWyglad", "assets/robo/item/shapes.png", "assets/robo/item/shapes.json");
            for (var i = 0; i < 12; i++) {
                this.load.image("robot" + i, "assets/robo/robots/robot" + i + ".png");
            }
            /*  end roboty */
            this.load.atlasJSONArray("tasmaskrzynia", "assets/robo/stage/tasma_skrzynki.png", "assets/robo/stage/tasma_skrzynki.json");
            this.load.atlasJSONArray("tasmaroboty", "assets/robo/stage/tasma.png", "assets/robo/stage/tasma.json");
            this.load.atlasJSONArray("kolozebate", "assets/robo/stage/koloZebate.png", "assets/robo/stage/koloZebate.json");
            this.load.atlasJSONArray("gwizdek", "assets/robo/stage/gwizdek.png", "assets/robo/stage/gwizdek.json");
            this.load.atlasJSONArray("robobo", "assets/robo/stage/robobobo.png", "assets/robo/stage/robobobo.json");
            this.load.atlasJSONArray("scoreScreen", "assets/scoreScreen/scoreScreen.png", "assets/scoreScreen/scoreScreen.json");
            this.load.atlasJSONArray("playScoreScreen", "assets/scoreScreen/scoreScreenButtons.png", "assets/scoreScreen/scoreScreenButtons.json");
            for (var i = 1; i < 6; i++) {
                this.load.atlasJSONArray("fail" + i, "assets/feedbacks/fail" + i + ".png", "assets/feedbacks/fail" + i + ".json");
            }
            for (var i = 1; i < 5; i++) {
                this.load.atlasJSONArray("success" + i, "assets/feedbacks/success" + i + ".png", "assets/feedbacks/success" + i + ".json");
            }
            for (var i = 1; i < 5; i++) {
                this.load.audio("congrats" + i, "assets/feedbacks/congrats" + i + ".mp3");
            }
            this.load.audio("failure1", "assets/feedbacks/failure1.mp3");
            this.load.audio("failure2", "assets/feedbacks/failure2.mp3");
        };
        Preloader.prototype.create = function () {
            this.game.state.start("MainMenu", true, false);
        };
        return Preloader;
    })(Phaser.State);
    Water.Preloader = Preloader;
})(Water || (Water = {}));
//# sourceMappingURL=Preloader.js.map