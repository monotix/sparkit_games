var Greeter = (function () {
    function Greeter(element) {
        this.element = element;
        this.element.innerHTML += "The time is: ";
        this.span = document.createElement('span');
        this.element.appendChild(this.span);
        this.span.innerText = new Date().toUTCString();
    }
    Greeter.prototype.start = function () {
        var _this = this;
        this.timerToken = setInterval(function () { return _this.span.innerHTML = new Date().toUTCString(); }, 500);
    };
    Greeter.prototype.stop = function () {
        clearTimeout(this.timerToken);
    };
    return Greeter;
})();
window.onload = function () {
    var el = document.getElementById('content');
    var greeter = new Greeter(el);
    greeter.start();
};
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Water;
(function (Water) {
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            _super.apply(this, arguments);
        }
        Boot.prototype.preload = function () {
            this.load.image("bg", "assets/SplashScreen.png");
            this.load.image("progressBarBlack", "assets/preloader/ProgressBar.png");
            this.load.image("progressBarBlue", "assets/preloader/ProgressBarBlue.png");
        };
        Boot.prototype.create = function () {
            this.game.state.start("Preloader", true, false);
        };
        return Boot;
    })(Phaser.State);
    Water.Boot = Boot;
})(Water || (Water = {}));
var Water;
(function (Water) {
    var GameControler = (function () {
        function GameControler() {
            this.SPEEDBOAT = -100;
            this.SPEEDROBOT = 100;
            this.finishGame = false;
            this._currenLevel = 0;
            this.collisionPlayer = false;
            this.gameOver = true;
            this.maxLevel = 3;
            this.currentLevel = 0;
        }
        GameControler.prototype.construct = function () {
            if (GameControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            GameControler._instance = this;
        };
        GameControler.getInstance = function () {
            if (GameControler._instance === null) {
                GameControler._instance = new GameControler();
            }
            return GameControler._instance;
        };
        GameControler.prototype.restart = function () {
            this._currenLevel = 0;
            this.level.playAgain();
        };
        GameControler.prototype.nextLevel = function () {
            this.currentLevel++;
            this.level.showPrompt(true, true);
        };
        GameControler.prototype.setLevel = function (level) {
            this._level = level;
        };
        Object.defineProperty(GameControler.prototype, "level", {
            get: function () {
                return this._level;
            },
            enumerable: true,
            configurable: true
        });
        GameControler.prototype.setSound = function (sound_) {
            this.mainSound = sound_;
        };
        GameControler._instance = null;
        return GameControler;
    })();
    Water.GameControler = GameControler;
})(Water || (Water = {}));
var Water;
(function (Water) {
    var LetterControler = (function () {
        function LetterControler() {
            this._corretAnswer = 0;
            this._maxBadAnswer = 3;
            this._badAnswer = 0;
            this._gameControler = Water.GameControler.getInstance();
            this._currentSequence = 0;
            this._maxSequence = 2;
            this.phonemes = new Array("a", "ai", "ay", "a", "aigh", "al", "au", "aw", "augh", "ar", "b", "bb", "bt", "c", "ch", "ck", "ce", "d", "ed", "dd", "dge", "ey", "eigh", "e", "ea", "ee", "ei", "el", "ew", "ear", "er", "f", "ff", "ph", "g", "gg", "gh", "ge", "gn", "h", "i", "ie", "il", "ir", "ie", "igh", "j", "k", "kn", "l", "ll", "le", "m", "mm", "mb", "mn", "n", "nn", "o", "ough", "oa", "oe", "ow", "ou", "oo", "or", "p", "pp", "ph", "pn", "pt", "qu", "r", "rr", "rh", "re", "s", "ss", "se", "st", "sc", "sh", "s", "se", "t", "tt", "th", "tch", "u", "ue", "ui", "ur", "v", "ve", "w", "wh", "wr", "x", "y", "yr", "z", "zz", "ze");
            this.phonemeSounds = new Array("a", "a_e", "aw", "b", "c", "ch", "d", "e", "ee", "er", "f", "g", "h", "i", "i_e", "j", "k", "l", "m", "n", "o", "o_e", "oo1", "oo2", "ow", "oy", "p", "qu", "r", "s", "sh", "t", "th", "th2", "u", "u_e", "v", "w", "x", "y", "z");
        }
        LetterControler.prototype.construct = function () {
            if (LetterControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            LetterControler._instance = this;
        };
        LetterControler.getInstance = function () {
            if (LetterControler._instance === null) {
                LetterControler._instance = new LetterControler();
            }
            return LetterControler._instance;
        };
        LetterControler.prototype.randomPhoneme = function () {
            var i = Math.floor(Math.random() * this.phonemes.length);
            return this.phonemes[i];
        };
        LetterControler.prototype.setRandomLetters = function (howmany) {
            if (howmany === void 0) { howmany = 1; }
            howmany = this._gameControler.currentLevel + 1;
            this._currentLetters = new Array();
            console.log("ile liter do zapamietania", howmany, this._gameControler.currentLevel);
            for (var i = 0; i < howmany; i++) {
                var num = this.getRandomNumber(this.phonemes.length - 1);
                console.log("phenomes", num);
                this._currentLetters.push(this.phonemes[num]);
            }
            console.log("current", this._currentLetters);
            this._currentWorld = this._currentLetters[0];
            this._maxLetter = this._currentLetters.length;
        };
        LetterControler.prototype.checkWord = function (str) {
            if (str == this._currentLetters[this._corretAnswer]) {
                console.log("dobra liltera");
                this._corretAnswer++;
                this.checkWinOrDie(false);
            }
            else {
                this._badAnswer++;
                this.checkWinOrDie(true);
                console.log("zla liltera");
            }
        };
        LetterControler.prototype.checkWinOrDie = function (die) {
            console.log("die", this._badAnswer, "goog", this._corretAnswer);
            if (this._badAnswer == this._maxBadAnswer) {
                this._gameControler.collisionPlayer = true;
                console.log("die you");
                this._gameControler.level.showPrompt(false, true);
                this._corretAnswer = 0;
                this._badAnswer = 0;
                return;
            }
            else if (this._corretAnswer == this._maxLetter) {
                this._currentSequence++;
                this._badAnswer = 0;
                if (this._currentSequence != this._maxSequence) {
                    this._corretAnswer = 0;
                    this.setRandomLetters(this._gameControler.currentLevel + 1);
                    this._gameControler.level.showPrompt(true);
                    this._gameControler.level.sequenceLevel(this._currentSequence + 1 + "/" + 2);
                }
                else {
                    this._currentSequence = 0;
                    this._gameControler.collisionPlayer = true;
                    this._gameControler.nextLevel();
                    this.setRandomLetters();
                    console.log("you win Level");
                }
                return;
            }
            if (die) {
                this._corretAnswer = 0;
                this._gameControler.level.showPrompt(false);
                this._gameControler.level.removeSequence();
            }
            else {
            }
        };
        LetterControler.prototype.getRandomNumber = function (max, min) {
            if (min === void 0) { min = 0; }
            //--max;
            return Math.floor(Math.random() * (max - min) + min);
        };
        LetterControler.prototype.getPopLetter = function () {
            var randNum = this.getRandomNumber(2);
            console.log("random number letter", randNum);
            var str;
            if (randNum == 1) {
                str = this.phonemes[this.getRandomNumber(this.phonemes.length)];
            }
            else {
                var num = this.getRandomNumber(this._currentLetters.length);
                str = this._currentLetters[num];
            }
            return str;
        };
        Object.defineProperty(LetterControler.prototype, "currentLetters", {
            get: function () {
                return this._currentLetters;
            },
            enumerable: true,
            configurable: true
        });
        LetterControler._instance = null;
        LetterControler.words = new Array({
            // level 1 english words
            english: new Array({ word: ["ai", "r"], sounds: ["a_e", "r"] }, { word: ["a", "l", "e"], sounds: ["a_e", "l", "split"] }, { word: ["a", "m"], sounds: ["a", "m"] }, { word: ["a", "n"], sounds: ["a", "n"] }, { word: ["a", "n", "d"], sounds: ["a", "n", "d"] }, { word: ["a", "n", "t"], sounds: ["a", "n", "t"] }, { word: ["a", "r", "m"], sounds: ["o", "r", "m"] }, { word: ["a", "r", "t"], sounds: ["o", "r", "t"] }, { word: ["a", "s"], sounds: ["a", "z"] }, { word: ["a", "sh"], sounds: ["a", "sh"] }, { word: ["a", "s", "k"], sounds: ["a", "s", "k"] }, { word: ["a", "t"], sounds: ["a", "t"] }, { word: ["b", "a", "d"], sounds: ["b", "a", "d"] }, { word: ["b", "a", "g"], sounds: ["b", "a", "g"] }, { word: ["b", "a", "r"], sounds: ["b", "o", "r"] })
        }, {
            // level 2 english words
            english: new Array({ word: ["a", "b", "j", "e", "c", "t"], sounds: ["a", "b", "j", "e", "k", "t"] }, { word: ["a", "b", "le"], sounds: ["a_e", "b", "l"] }, { word: ["a", "b", "oa", "r", "d"], sounds: ["u", "b", "o_e", "r", "d"] }, { word: ["a", "b", "o", "r", "t"], sounds: ["u", "b", "o_e", "r", "t"] }, { word: ["a", "b", "ou", "n", "d"], sounds: ["u", "b", "ow", "n", "d"] }, { word: ["a", "b", "o", "v", "e"], sounds: ["u", "b", "u", "v", "split"] }, { word: ["a", "b", "r", "ea", "s", "t"], sounds: ["u", "b", "r", "e", "s", "t"] }, { word: ["a", "b", "s", "e", "n", "ce"], sounds: ["a", "b", "s", "e", "n", "s"] }, { word: ["a", "b", "s", "o", "l", "ve"], sounds: ["u", "b", "z", "o", "l", "v"] }, {
                word: ["a", "b", "s", "t", "r", "a", "c", "t"],
                sounds: ["a", "b", "s", "t", "r", "a", "k", "t"]
            }, { word: ["a", "b", "s", "ur", "d"], sounds: ["u", "b", "s", "er", "d"] }, { word: ["a", "b", "u", "se"], sounds: ["u", "b", "u_e", "s"] }, { word: ["a", "b", "y", "ss"], sounds: ["u", "b", "i", "s"] }, { word: ["a", "c", "c", "e", "p", "t"], sounds: ["a", "c", "s", "e", "p", "t"] }, { word: ["a", "c", "c", "l", "ai", "m"], sounds: ["u", "c", "c", "l", "a_e", "m"] }, { word: ["a", "ce"], sounds: ["a_e", "s"] })
        }, {
            // level 3 english words
            english: new Array({
                word: ["a", "b", "d", "o", "m", "i", "n", "al"],
                sounds: ["u", "b", "d", "o", "m", "i", "n", "l"]
            }, { word: ["a", "b", "l", "y"], sounds: ["a_e", "b", "l", "ee"] }, {
                word: ["a", "b", "o", "l", "i", "sh", "ed"],
                sounds: ["u", "b", "o", "l", "i", "sh", "d"]
            }, {
                word: ["a", "b", "ou", "t"],
                sounds: ["a", "b", "ow", "t"]
            }, {
                word: ["a", "b", "r", "a", "s", "i", "ve"],
                sounds: ["u", "b", "r", "a_e", "s", "i", "v"]
            }, { word: ["a", "b", "s", "e", "n", "t"], sounds: ["a", "b", "s", "e", "n", "t"] }, {
                word: ["a", "b", "u", "n", "d", "a", "n", "ce"],
                sounds: ["u", "b", "u", "n", "d", "u", "n", "s"]
            }, {
                word: ["a", "b", "u", "n", "d", "a", "n", "t"],
                sounds: ["u", "b", "u", "n", "d", "u", "n", "t"]
            }, {
                word: ["a", "b", "u", "s", "i", "ve"],
                sounds: ["u", "b", "u_e", "s", "i", "v"]
            }, {
                word: ["a", "c", "qu", "i", "tt", "ed"],
                sounds: ["u", "c", "qu", "i", "t", "d"]
            }, {
                word: ["a", "c", "r", "o", "n", "y", "m"],
                sounds: ["a", "c", "r", "o", "n", "i", "m"]
            }, { word: ["a", "c", "r", "o", "ss"], sounds: ["o", "c", "r", "o", "s"] }, {
                word: ["a", "c", "r", "y", "l", "i", "c"],
                sounds: ["u", "c", "r", "i", "l", "i", "c"]
            }, {
                word: ["a", "c", "t", "i", "v", "i", "s", "m"],
                sounds: ["a", "c", "t", "i", "v", "i", "z", "m"]
            })
        });
        return LetterControler;
    })();
    Water.LetterControler = LetterControler;
})(Water || (Water = {}));
var Water;
(function (Water) {
    var ObstacleControler = (function () {
        function ObstacleControler() {
        }
        ObstacleControler.prototype.construct = function () {
            if (ObstacleControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            ObstacleControler._instance = this;
        };
        ObstacleControler.getInstance = function () {
            if (ObstacleControler._instance === null) {
                ObstacleControler._instance = new ObstacleControler();
            }
            return ObstacleControler._instance;
        };
        ObstacleControler.prototype.level1 = function () {
            var arr = new Array(new Array({ x: 216, y: 94, name: "ball" }, { x: 515, y: 98, name: "popletter" }, { x: 874, y: 93, name: "wood" }), new Array({ x: 256, y: 96, name: "ball" }, { x: 546, y: 105, name: "popletter" }, { x: 863, y: 93, name: "ball" }), new Array({ x: 256, y: 96, name: "ball" }, { x: 546, y: 105, name: "popletter" }, { x: 863, y: 93, name: "ball" }, { x: 902, y: 96, name: "boatssmall" }), new Array({ x: 209, y: 96, name: "ball" }, { x: 487, y: 102, name: "popletter" }, { x: 802, y: 102, name: "popletter" }, { x: 902, y: 96, name: "boatssmall" }, { x: 414, y: 96, name: "boatsmedium" }), new Array({ x: 209, y: 96, name: "ball" }, { x: 465, y: 93, name: "ball" }, { x: 875, y: 93, name: "ball" }, { x: 414, y: 96, name: "boatsmedium" }), new Array({ x: 209, y: 96, name: "ball" }, { x: 465, y: 93, name: "ball" }, { x: 875, y: 93, name: "popletter" }, { x: 414, y: 96, name: "boatsmedium" }), new Array({ x: 270, y: 99, name: "wood" }, { x: 843, y: 99, name: "wood" }, { x: 554, y: 93, name: "popletter" }, { x: 697, y: 96, name: "boatsmedium" }), new Array({ x: 129, y: 96, name: "ball" }, { x: 425, y: 93, name: "ball" }, { x: 657, y: 93, name: "ball" }, { x: 407, y: 96, name: "boatsmedium" }), new Array({ x: 490, y: 101, name: "popletter" }, { x: 155, y: 98, name: "popletter" }, { x: 895, y: 100, name: "popletter" }, { x: 902, y: 96, name: "boatssmall" }), new Array({ x: 209, y: 99, name: "popletter" }, { x: 644, y: 92, name: "popletter" }, { x: 954, y: 95, name: "popletter" }, { x: 334, y: 96, name: "boatsmedium" }, { x: 860, y: 96, name: "boatsmedium" }));
            return arr[this.getRandom(arr.length)];
        };
        ObstacleControler.prototype.level2 = function () {
            var arr = new Array(new Array({ x: 144, y: 179, name: "walen" }, { x: 430, y: 185, name: "walen" }, { x: 804, y: 101, name: "popletter" }, { x: 1204, y: 101, name: "osmiornica" }, { x: 860, y: 96, name: "boatssmedium" }), new Array({ x: 335, y: 90, name: "ball" }, { x: 504, y: 96, name: "popletter" }, { x: 336, y: 202, name: "piasek" }, { x: 765, y: 181, name: "walen" }, { x: 1131, y: 96, name: "ball" }, { x: 411, y: 96, name: "boatssmedium" }, { x: 1015, y: 96, name: "boatssmall" }), new Array({ x: 164, y: 180, name: "walen" }, { x: 494, y: 102, name: "osmiornica" }, { x: 539, y: 101, name: "piasek" }, { x: 733, y: 98, name: "popletter" }, { x: 968, y: 180, name: "walen" }, { x: 491, y: 96, name: "boatssmall" }), new Array({ x: 236, y: 183, name: "walen" }, { x: 547, y: 183, name: "walen" }, { x: 939, y: 183, name: "walen" }), new Array({ x: 212, y: 98, name: "ball" }, { x: 506, y: 103, name: "osmiornica" }, { x: 755, y: 96, name: "popletter" }), new Array({ x: 137, y: 98, name: "ball" }, { x: 337, y: 98, name: "ball" }, { x: 516, y: 99, name: "popletter" }, { x: 922, y: 108, name: "osmiornica" }, { x: 1000, y: 199, name: "piasek" }, { x: 409, y: 199, name: "boatsmedium" }), new Array({ x: 216, y: 98, name: "ball" }, { x: 991, y: 98, name: "popletter" }, { x: 460, y: 99, name: "popletter" }, { x: 666, y: 180, name: "walen" }, { x: 769, y: 199, name: "piasek" }), new Array({ x: 209, y: 98, name: "ball" }, { x: 392, y: 98, name: "popletter" }, { x: 989, y: 99, name: "popletter" }, { x: 676, y: 180, name: "walen" }, { x: 330, y: 199, name: "piasek" }, { x: 1021, y: 193, name: "piasek" }, { x: 1106, y: 199, name: "boatsmedium" }, { x: 306, y: 199, name: "boatssmall" }), new Array({ x: 1030, y: 98, name: "ball" }, { x: 121, y: 98, name: "popletter" }, { x: 459, y: 99, name: "popletter" }, { x: 748, y: 102, name: "osmiornica" }, { x: 339, y: 199, name: "boatsmedium" }), new Array({ x: 170, y: 98, name: "ball" }, { x: 423, y: 98, name: "popletter" }, { x: 707, y: 99, name: "popletter" }, { x: 475, y: 180, name: "piasek" }, { x: 858, y: 199, name: "boatsmedium" }, { x: 897, y: 185, name: "walen" }));
            return arr[this.getRandom(arr.length)];
        };
        ObstacleControler.prototype.getRandom = function (max) {
            return Math.floor(Math.random() * max);
        };
        ObstacleControler._instance = null;
        return ObstacleControler;
    })();
    Water.ObstacleControler = ObstacleControler;
})(Water || (Water = {}));
var Water;
(function (Water) {
    var Instruction = (function (_super) {
        __extends(Instruction, _super);
        function Instruction() {
            _super.apply(this, arguments);
        }
        Instruction.prototype.create = function () {
            var instruction = this.add.image(0, 0, "firstInstruction", 0);
            instruction.width = this.game.world.width; //this.stage.width;
            instruction.height = this.game.world.height; //this.stage.height;
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            this._mainMenuBg = this.game.add.audio('instructionSn', 1, true);
            this._mainMenuBg.play("", 0, 1, false);
            var gameControler = Water.GameControler.getInstance();
            gameControler.mainSound.volume = 0.05;
            // var instructionAudio = this.game.add.audio("chuja", 1, false);
            //  instructionAudio.play();
            var _buttons = this.add.group();
            var gameButton = this.add.button(0, 0, "games", this.onGameClick, this, 1, 0);
            //gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            _buttons.addChild(gameButton);
            var playButton = this.add.button(100, 0, "playGame", this.onPlayClick, this, 1, 0);
            playButton.onInputDown.add(this.overPlay2, this);
            _buttons.addChild(playButton);
            _buttons.x = 0;
            _buttons.y = this.game.stage.height - _buttons.height;
        };
        Instruction.prototype.onGameClick = function () {
            this.overButtonA.play();
        };
        Instruction.prototype.overPlay2 = function () {
            this.overButtonA.play();
        };
        Instruction.prototype.onPlayClick = function () {
            this._mainMenuBg.stop();
            this.game.state.start("Level", true, false);
        };
        return Instruction;
    })(Phaser.State);
    Water.Instruction = Instruction;
})(Water || (Water = {}));
var Water;
(function (Water) {
    var Level = (function (_super) {
        __extends(Level, _super);
        function Level() {
            _super.apply(this, arguments);
            this._timer = 0;
            this.jumpPressed = false;
            this.prawaClick = false;
        }
        Level.prototype.create = function () {
            this._gameControler = Water.GameControler.getInstance();
            this._gameControler.setLevel(this);
            this._letterControler = Water.LetterControler.getInstance();
            this._letterControler.setRandomLetters();
            this._blokadaGr = this.add.group();
            this._bg = this.game.add.group();
            this.game.add.image(0, 0, "bgGame", 0, this._bg);
            this._obstcaleGroup = new Water.ObstacleView(this.game);
            this.stage.addChild(this._obstcaleGroup);
            this._obstcaleGroup.getObstacle();
            this._playerPlay = new Water.PlayerPlay(this.game);
            this.stage.addChild(this._playerPlay);
            /*
            to jest gadajacy koles.
            playIntro() - mowi intro
            playFeedbackGood() - mowi jak wykonamy poprawnie sekwencje
            playFeedbackWrong() - mowi jak zrobimy blad
            */
            this._sequenceGr = this.add.group();
            this._sequence = new Water.Sequence(this.game, 0, 0, "sequence");
            this._sequenceGr.add(this._sequence);
            this.stage.addChild(this._blokadaGr);
            /*
            to jest zegar
            startTimer() start timera
            stopTimer() stop timera
            setSequenceText(text) dodajemy text w sequence
            setCreditsText(text) dodajemy text w credits. $(dolar) jest dodawany w klasie
            */
            this._clock = new Water.ClockView(this.game, this);
            this._clock.position.setTo(this.game.width - this._clock.width, 0);
            this.stage.addChild(this._clock);
            this._clock.startTimer();
            this._clock.setSequenceText("1/2");
            this._clock.setCreditText("0");
            this.stopGame(true);
            this._showLetterGr = this.game.add.group();
            this.stage.addChild(this._showLetterGr);
            if (mobile) {
                this._special = this.add.sprite(0, 0, "special");
                this._special.y = this.game.height - this._special.height - 30;
                this._special.x = 550;
                this._special.animations.add("play");
                this._jumpBtn = this.add.button(0, 0, "jumpbtn", this.jummBtnDown, this, 1, 0, 1, 0);
                this._jumpBtn.y = this._special.y + 15;
                this._jumpBtn.x = 630;
                this.stage.addChild(this._special);
                this.stage.addChild(this._jumpBtn);
                this._jumpBtn.onInputDown.add(this.jummBtnDown, this);
                this._jumpBtn.onInputUp.add(this.jummBtnUp, this);
            }
            /*     this.game.input.keyboard.onUpCallback = function (e) {
                     if (e.keyCode == Phaser.Keyboard.UP) {
                         this.jummBtnUp();
                     }
                 };*/
            this._prompt = new Water.PromptView(this.game, this);
            this._prompt.position.setTo(this.game.width - this._prompt.width, this.game.height - this._prompt.height);
            this.stage.addChild(this._prompt);
            this._prompt.playIntro();
            if (!mobile) {
                this.cursors = this.game.input.keyboard.createCursorKeys();
            }
            this.helpGr = this.add.group();
            this.stage.addChild(this.helpGr);
            this._helpBtn = this.add.button(0, 0, "helpBtn", this.helpBtnClick, this, 1, 0, 0, 0, this.helpGr);
            this.helpGr.x = 0; //this.game.stage.width - this.helpGr.width;
            this.helpGr.y = 600 - this.helpGr.height;
            this._helpBtn.buttonMode = true;
            this._helpBtn.inputEnabled = true;
            this._helpBtn.input.priorityID = 11;
            this.game.input.onDown.add(function () {
                if (this.game.paused) {
                    this.game.paused = false;
                }
                if (this._instructionShow) {
                    this.stage.removeChild(this._instructionShow);
                    this._instructionShow.kill();
                    this._instructionShow = null;
                }
            }, this);
        };
        Level.prototype.helpBtnClick = function () {
            console.log("pokaz instrukcje");
            this.game.paused = !this.game.paused;
            if (this._instructionShow == null)
                this._instructionShow = this.add.image(0, 0, "firstInstruction", 0);
            // this.add.image(0, 0, "firstInstruction", 0);
            this._instructionShow.width = this.game.world.width; //this.stage.width;
            this._instructionShow.height = this.game.world.height;
            this.stage.addChild(this._instructionShow);
        };
        Level.prototype.addCredits = function () {
            this._clock.addCredits();
        };
        Level.prototype.blokada = function (show) {
            if (show === void 0) { show = true; }
            if (this._blokada == null) {
                this._blokada = new Phaser.Graphics(this.game, 0, 0);
                this._blokada.beginFill(0x000000, 0.6);
                this._blokada.drawRect(0, 0, this.game.width, this.game.height);
                this._blokada.endFill();
                this._blokadaGr.add(this._blokada);
                this._blokada.visible = false;
            }
            this._blokada.visible = show;
        };
        Level.prototype.jummBtnDown = function () {
            if (!this._gameControler.collisionPlayer) {
                this.jumpPressed = true;
                this._special.animations.play("play");
            }
        };
        Level.prototype.jummBtnUp = function () {
            this.jumpPressed = false;
            console.log("timer tricki", this._timer);
            if (!this._gameControler.collisionPlayer) {
                if (this._timer > 60) {
                    this._gameControler.collisionPlayer = true;
                    this._playerPlay.player.jumptrick();
                }
                else {
                    this._gameControler.collisionPlayer = true;
                    this._playerPlay.player.jump();
                }
                this._timer = 0;
                this._special.animations.stop("play", true);
            }
        };
        Level.prototype.playAgain = function (win) {
            if (win === void 0) { win = false; }
            this._obstcaleGroup.delObstacle();
            this._obstcaleGroup.removeObstacle();
            this._obstcaleGroup.getObstacle();
            this.stopGame(true);
            this._clock.setCreditText("0");
            this._clock.setSequenceText("1/2");
            if (win)
                this._prompt.playIntro();
            else
                this._prompt.playFeedbackWrong();
        };
        /*
        Ta funkcja jest odpalana jak koles skonczy audio
        */
        Level.prototype.onPromptAudioComplete = function () {
            console.log("Koles skonczyl gadac");
            this.removeSequence();
            this._gameControler.collisionPlayer = true;
            this._showLetter = new Water.ShowLetter(this.game, 0, 0, "bgletter");
            this._showLetterGr.add(this._showLetter);
            this._showLetter.addBgToDisplayLetter();
        };
        Level.prototype.sequenceLevel = function (str) {
            this._clock.setSequenceText(str);
        };
        Level.prototype.showFinishLevel = function () {
            console.log("FInish Level:");
            this._finishScreenLevel = new Water.FinishLevel(this.game);
            //    this._finishScreenLevel.x = this.game.width / 2 - this._finishScreenLevel.width / 2;
            //    this._finishScreenLevel.y = this.game.height / 2 - this._finishScreenLevel.height / 2;
            this.stage.addChild(this._finishScreenLevel);
            this.stopGame(true);
        };
        Level.prototype.showFinish = function () {
            this._finishScreen = new Water.FinishScreen(this.game);
            //     this._finishScreen.x = this.game.width / 2 - this._finishScreen.width / 2;
            //     this._finishScreen.y = this.game.height / 2 - this._finishScreen.height / 2;
            this.stage.addChild(this._finishScreen);
            this.stopGame(true);
        };
        Level.prototype.showPrompt = function (correct, end) {
            if (end === void 0) { end = false; }
            this._gameControler.collisionPlayer = true;
            if (correct) {
                this.stopGame(true);
                this._obstcaleGroup.removeObstacleLetters();
                this._prompt.playFeedbackGood(end);
            }
            else {
                this.stopGame(true);
                this._gameControler.collisionPlayer = true;
                this._prompt.playFeedbackWrong(end);
            }
        };
        Level.prototype.removeSequence = function () {
            this._sequence.removeLetters();
        };
        Level.prototype.stopGame = function (stop) {
            this._obstcaleGroup.changeSpeed(stop);
        };
        Level.prototype.update = function () {
            if (!this._gameControler.collisionPlayer) {
                this.game.physics.arcade.overlap(this._playerPlay, this._obstcaleGroup, this.collision, null, this);
                if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
                    this._gameControler.collisionPlayer = true;
                    this._playerPlay.player.jump();
                    console.log("Jump====>");
                }
                if (this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {
                    console.log("PRAWO====>");
                    if (!this.prawaClick) {
                        this.prawaClick = true;
                        // this._gameControler.collisionPlayer = false;
                        this._playerPlay.player.fastforward();
                        this._obstcaleGroup.fastForward(true);
                    }
                }
                else if (!this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {
                    if (this.prawaClick) {
                        this.prawaClick = false;
                        //  this._gameControler.collisionPlayer = false;
                        this._playerPlay.player.forward();
                        this._obstcaleGroup.fastForward(false);
                        this._obstcaleGroup.changeSpeed(false);
                    }
                }
                if (this.jumpPressed)
                    this._timer++;
            }
        };
        Level.prototype.collision = function (object, object2) {
            console.log("kolizja", object2.name);
            if (!this._gameControler.collisionPlayer) {
                if (object2.name == "popletter") {
                    this._sequence.addLetter(object2.letter);
                    this._letterControler.checkWord(object2.letter);
                    object2.destroy(true);
                }
                else {
                    this._playerPlay.player.crashForward();
                    this._gameControler.collisionPlayer = true;
                }
                this._timer = 0;
                if (mobile)
                    this._special.animations.stop("play", true);
            }
        };
        Level.prototype.render = function () {
            /*
                 this.game.debug.quadTree(this.game.physics.arcade.quadTree);
           
                 this._obstcaleGroup.forEach(function (item) {
                     // item.body.velocity = 0;
                  
                     this.game.debug.body(item);
                 }, this)
    
                 this._playerPlay.forEach(function (item) {
                     // item.body.velocity = 0;
                  
                     this.game.debug.body(item);
                 }, this)
    
            */
        };
        return Level;
    })(Phaser.State);
    Water.Level = Level;
})(Water || (Water = {}));
var Water;
(function (Water) {
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            console.log("start");
            _super.call(this, 800, 600, Phaser.AUTO, "content", null);
            this.state.add("Boot", Water.Boot, false);
            this.state.add("Preloader", Water.Preloader, false);
            this.state.add("MainMenu", Water.MainMenu, false);
            this.state.add("Instruction", Water.Instruction, false);
            this.state.add("PopupStart", Water.PopupStart, false);
            this.state.add("Level", Water.Level, false);
            this.state.start("Boot");
        }
        return Game;
    })(Phaser.Game);
    Water.Game = Game;
})(Water || (Water = {}));
window.onload = function () {
    var game = new Water.Game;
};
var userLevel = 0;
var Water;
(function (Water) {
    var MainMenu = (function (_super) {
        __extends(MainMenu, _super);
        function MainMenu() {
            _super.apply(this, arguments);
        }
        MainMenu.prototype.create = function () {
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var _mainMenuBg = this.game.add.audio('MainMenuSn', 1, true);
            _mainMenuBg.volume = 0.1;
            _mainMenuBg.play();
            var gameControler = Water.GameControler.getInstance();
            gameControler.setSound(_mainMenuBg);
            var background = this.add.sprite(0, 0, 'mainMenuBg');
            var _buttons = this.add.group();
            var gameButton = this.add.button(0, 0, "games", this.onGameClick, this, 1, 0);
            //gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            _buttons.addChild(gameButton);
            var playButton = this.add.button(100, 0, "playGame", this.onPlayClick, this, 1, 0);
            playButton.onInputDown.add(this.overPlay2, this);
            _buttons.addChild(playButton);
            _buttons.x = 0;
            _buttons.y = this.game.stage.height - _buttons.height;
        };
        MainMenu.prototype.overPlay2 = function () {
            this.overButtonA.play();
        };
        MainMenu.prototype.onGameClick = function () {
            this.overButtonA.play();
        };
        MainMenu.prototype.onPlayClick = function () {
            this.game.state.start("PopupStart", true, false);
        };
        return MainMenu;
    })(Phaser.State);
    Water.MainMenu = MainMenu;
})(Water || (Water = {}));
var Water;
(function (Water) {
    var Obstacle = (function (_super) {
        __extends(Obstacle, _super);
        function Obstacle(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this.name = key;
            if (this.name == "osmiornica" || this.name == "shark" || this.name == "shark2" || this.name == "shark3" || this.name == "shark4") {
                this._haveAnim = true;
                this.animations.add("anim");
                this.animations.play("anim", 24, true);
                this.y += 305;
            }
            else if (this.name == "boatsmedium") {
                this.frame = this.getRandom(this.animations.frameTotal);
                this.y += 50;
            }
            else if (this.name == "boatssmall") {
                this.y += 100;
            }
            else {
                this.y += 305;
            }
        }
        Obstacle.prototype.getRandom = function (max) {
            return Math.floor(Math.random() * max);
        };
        Obstacle.prototype.stopAnim = function () {
            if (this._haveAnim) {
                this.animations.stop("anim");
            }
            else {
                console.log("nie ma animacji", this.name);
            }
        };
        return Obstacle;
    })(Phaser.Sprite);
    Water.Obstacle = Obstacle;
})(Water || (Water = {}));
var Water;
(function (Water) {
    var Player = (function (_super) {
        __extends(Player, _super);
        function Player(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this._gameControl = Water.GameControler.getInstance();
            this._currentTrickiCount = 0;
            //  this.anchor.setTo(0.5, 0.5);
            game.physics.enable(this, Phaser.Physics.ARCADE);
            this._crashForward = new Phaser.Sprite(game, 0, 0, "crashforward");
            this._crashForward.animations.add("play");
            this._jump = new Phaser.Sprite(game, 0, 0, "jump");
            this._jump.animations.add("play");
            this._forward = new Phaser.Sprite(game, 0, 0, "player");
            this._forward.animations.add("play");
            this._forwardSpeed = new Phaser.Sprite(game, 0, 0, "forwardSpeed");
            this._forwardSpeed.animations.add("play");
            this._miganie = new Phaser.Sprite(game, 0, 0, "miganie");
            this._miganie.animations.add("play");
            /*  this.animations.add("forward", this.countFrames(1, 19), 24, true);
              this.animations.add("fastforward", this.countFrames(20, 34), 24, false);
  
              this.animations.add("jump", this.countFrames(35, 88), 24, false);
              this.animations.add("jumptrick", this.countFrames(89, 136), 24, false);
          */
            this.electroAudio = this.game.sound.add("electro", 1, false, true);
            this.electroAudio2 = this.game.sound.add("electro1", 1, false, true);
            this.crashAudio = this.game.sound.add("crash", 1, false, true);
            this.crashAudio2 = this.game.sound.add("crash2", 1, false, true);
            this.jumpAudio = this.game.sound.add("jumpAudio", 1, false, true);
            this.holeAudio = this.game.sound.add("holeAudio", 1, false, true);
        }
        Player.prototype.jump = function () {
            this.addAnim(this._jump);
            this._currentAnim.animations.play("play", 24, false, false);
            this._currentAnim.animations.getAnimation("play").onComplete.add(function () {
                this.endJump();
            }, this);
        };
        Player.prototype.endJump = function () {
            this.addAnim(this._forward);
            this._currentAnim.animations.play("play", 24, true, false);
            this._gameControl.collisionPlayer = false;
        };
        Player.prototype.jumptrick = function () {
            if (this._currentTrickiCount == 6) {
                this._currentTrickiCount = 0;
            }
            this._gameControl.level.addCredits();
            this._currentTrickiCount++;
            console.log("trocki cout", this._currentTrickiCount);
            this._tricki = new Phaser.Sprite(this.game, 0, 0, "tricki" + this._currentTrickiCount, 0);
            this._tricki.animations.add("play");
            this.addAnim(this._tricki);
            this._currentAnim.animations.play("play", 24, false, false);
            this._currentAnim.animations.getAnimation("play").onComplete.add(function () {
                this.jumptrickiend();
            }, this);
        };
        Player.prototype.jumptrickiend = function () {
            this.addAnim(this._forward);
            this._tricki.kill();
            this._tricki = null;
            this._currentAnim.animations.play("play", 24, true, false);
            this._gameControl.collisionPlayer = false;
        };
        Player.prototype.crashForward = function () {
            this.addAnim(this._crashForward);
            this._currentAnim.animations.play("play", 24, false);
            this._currentAnim.animations.getAnimation("play").onComplete.add(function () {
                this.crashForwardOver();
            }, this);
        };
        Player.prototype.crashForwardOver = function () {
            this.addAnim(this._miganie);
            this._currentAnim.animations.play("play", 24, false);
            this._currentAnim.animations.getAnimation("play").onComplete.add(function () {
                this.activePlayer();
            }, this);
            // this.game.add.tween(this._currentAnim).to({ alpha: 0 }, 0.5, Phaser.Easing.Linear.None, false, 0.5, 5).start();
            //var tweenAn =  this.game.add.tween(this._currentAnim).to({ alpha: 1 }, 5, Phaser.Easing.Linear.None,false).start();
            //  this.game.time.events.add(Phaser.Timer.SECOND * 4, this.activePlayer, this)
            //  tweenAn.onComplete.add(this.activePlayer,this);
        };
        Player.prototype.activePlayer = function () {
            console.log("aktywne");
            this._gameControl.collisionPlayer = false;
        };
        Player.prototype.forward = function () {
            this.addAnim(this._forward);
            this._currentAnim.animations.play("play", 24, true);
        };
        Player.prototype.forwardSpeed = function () {
            this.addAnim(this._forwardSpeed);
            this._currentAnim.animations.play("play", 24, true);
        };
        Player.prototype.addAnim = function (anim) {
            if (this._currentAnim != null) {
                this.removeChild(this._currentAnim);
            }
            this._currentAnim = anim;
            //  this._currentAnim.scale.setTo(0.5, 0.5);
            this._currentAnim.anchor.setTo(0.5, 0.5);
            this._currentAnim.animations.stop("play", true);
            this.addChild(this._currentAnim);
        };
        Player.prototype.fastforward = function () {
            this.animations.play("fastforward", 24, false, false);
        };
        Player.prototype.playAgain = function () {
            this._gameControl.collisionPlayer = false;
        };
        Player.prototype.countFrames = function (start, num) {
            var countArr = new Array();
            for (var i = start; i < num + 1; i++) {
                countArr.push(i);
            }
            return countArr;
        };
        return Player;
    })(Phaser.Sprite);
    Water.Player = Player;
})(Water || (Water = {}));
var Water;
(function (Water) {
    var PopLetter = (function (_super) {
        __extends(PopLetter, _super);
        function PopLetter(game, x, y, key, posx, posy, bounceWidth, bounceHeight) {
            if (posx === void 0) { posx = 0; }
            if (posy === void 0) { posy = 0; }
            if (bounceWidth === void 0) { bounceWidth = 0; }
            if (bounceHeight === void 0) { bounceHeight = 0; }
            _super.call(this, game, x, y, key);
            this.letter = "b";
            this.name = "popletter";
            //  this.animations.add("pop");
            this.y += 300;
            this.posx = (posx == 0) ? 0 : posx;
            this.posy = (posy == 0) ? 0 : posy;
            this.bounceWidth = (bounceWidth == 0) ? this.width : bounceWidth;
            this.bounceHeight = (bounceHeight == 0) ? this.height : bounceHeight;
        }
        PopLetter.prototype.addLetter = function (str) {
            this.frame = str.length - 1;
            var style = { font: "40px Arial", fill: "#000000", align: "center" };
            if (this._letterShow)
                this._letterShow.destroy();
            this._letterShow = new Phaser.Text(this.game, 0, 0, str, style);
            this._letterShow.x = this.width / 2 - this._letterShow.width / 2;
            this._letterShow.y = this.height / 2 - this._letterShow.height / 2;
            // this._letterShow.anchor.set(0.5, 0.5);
            //   this._letterShow.x = 40;
            //  this._letterShow.y = 25;
            this.addChild(this._letterShow);
            this.letter = str;
        };
        PopLetter.prototype.getBounce = function () {
            var rect = new Phaser.Rectangle(this.posx, this.posy, this.bounceWidth, this.bounceHeight);
            return rect;
        };
        PopLetter.prototype.collisonPlayAnim = function () {
            this.removeChild(this._letterShow);
            this.animations.getAnimation("pop").onComplete.add(function () {
                this.kill();
            }, this);
            this.play("pop", 24, false, true);
        };
        return PopLetter;
    })(Phaser.Sprite);
    Water.PopLetter = PopLetter;
})(Water || (Water = {}));
var Water;
(function (Water) {
    var Preloader = (function (_super) {
        __extends(Preloader, _super);
        function Preloader() {
            _super.apply(this, arguments);
        }
        Preloader.prototype.preload = function () {
            var bg = this.add.image(0, 0, "bg", 0);
            var blackProgress = this.add.image(0, 0, "progressBarBlack", 0);
            var blueProgress = this.add.sprite(0, 0, "progressBarBlue", 0);
            blackProgress.position.setTo(this.stage.width / 2 - blackProgress.width / 2, this.stage.height - blackProgress.height - 10);
            blueProgress.position = blackProgress.position;
            this.load.setPreloadSprite(blueProgress);
            this.load.image("mainMenuBg", "assets/mainMenu/splashScreen.png");
            this.load.atlasJSONArray('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');
            this.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
            this.load.audio("MainMenuSn", "assets/audio/ScubaDudeTheme.mp3");
            this.load.audio("overButtonA", "assets/audio/overButton.mp3");
            this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");
            this.load.atlasJSONArray('helpBtn', 'assets/popup/help.png', 'assets/popup/help.json');
            var mobile;
            mobile = false;
            if (!this.game.device.desktop) {
                mobile = true;
            }
            if (mobile) {
                this.load.image("firstInstruction", "assets/03_wake_trash.png");
                this.load.audio("instructionSn", "assets/audio/wake_thrash_02.mp3");
            }
            else {
                this.load.image("firstInstruction", "assets/02_wake_trash.png");
                this.load.audio("instructionSn", "assets/audio/wake_thrash_01.mp3");
            }
            this.load.image("popup", "assets/popup/popup.png");
            this.load.image("logoGame", "assets/popup/logo.png");
            this.load.atlasJSONArray('games', 'assets/popup/games.png', 'assets/popup/games.json');
            this.load.atlasJSONArray('playGame', 'assets/popup/play.png', 'assets/popup/play.json');
            this.load.image("bgGame", "assets/water/gameBackground.png");
            this.load.atlasJSONArray('boatsmedium', 'assets/water/boat/boatsmedium.png', 'assets/water/boat/boatsmedium.json');
            this.load.atlasJSONArray('boatssmall', 'assets/water/boat/boatssmall.png', 'assets/water/boat/boatssmall.json');
            /* instructions */
            this.load.atlasJSONArray('prompt', 'assets/water/prompt/prompt.png', 'assets/water/prompt/prompt.json');
            this.load.audio("WakeIntro", "assets/audio/instruction/WakeIntro.mp3");
            this.load.audio("WakeSuccess0", "assets/audio/instruction/WakeSuccess0.mp3");
            this.load.audio("WakeWrong0", "assets/audio/instruction/WakeWrong0.mp3");
            this.load.audio("music", "assets/audio/ThemeMusic.mp3");
            this.load.image("firstInstruction", "assets/water/instructionsScreen.png");
            this.load.image("sequence", "assets/water/sequence.png");
            /* player */
            this.load.atlasJSONArray('player', 'assets/water/player/froward.png', 'assets/water/player/forward.json');
            this.load.atlasJSONArray('jump', 'assets/water/player/jump.png', 'assets/water/player/jump.json');
            this.load.atlasJSONArray('forwardSpeed', 'assets/water/player/charging.png', 'assets/water/player/charging.json');
            this.load.atlasJSONArray('miganie', 'assets/water/player/miganie.png', 'assets/water/player/miganie.json');
            this.load.atlasJSONArray('crashforward', 'assets/water/player/crashforward.png', 'assets/water/player/crashforward.json');
            for (var i = 1; i < 7; i++) {
                this.load.atlasJSONArray('tricki' + i, 'assets/water/player/trick' + i + '.png', 'assets/water/player/trick' + i + '.json');
            }
            this.load.atlasJSONArray('special', 'assets/water/special.png', 'assets/water/special.json');
            this.load.atlasJSONArray('jumpbtn', 'assets/water/jumpBtn.png', 'assets/water/jumpBtn.json');
            this.load.image('bgletter', 'assets/water/letter/bgletter.png');
            this.load.image("bgClock", "assets/water/interface.png");
            /* obstacle */
            this.load.image("piasek", "assets/water/obstacle/piasek.png");
            this.load.image("wood", "assets/water/obstacle/wood.png");
            this.load.image("hammer", "assets/water/obstacle/hammer.png");
            this.load.image("turtle", "assets/water/obstacle/turtle.png");
            this.load.image("zielone", "assets/water/obstacle/zielone.png");
            this.load.image("ball", "assets/water/obstacle/ball.png");
            this.load.image("ball2", "assets/water/obstacle/ball2.png");
            this.load.image("walen", "assets/water/obstacle/walen.png");
            this.load.atlasJSONArray('popletter', 'assets/water/letter/popletter.png', 'assets/water/letter/popletter.json');
            this.load.atlasJSONArray('shark1', 'assets/water/obstacle/shark.png', 'assets/water/obstacle/shark.json');
            this.load.atlasJSONArray('shark2', 'assets/water/obstacle/shark2.png', 'assets/water/obstacle/shark2.json');
            this.load.atlasJSONArray('shark3', 'assets/water/obstacle/shark3.png', 'assets/water/obstacle/shark3.json');
            this.load.atlasJSONArray('shark4', 'assets/water/obstacle/shark4.png', 'assets/water/obstacle/shark4.json');
            this.load.atlasJSONArray('osmiornica', 'assets/water/obstacle/osmiornica.png', 'assets/water/obstacle/osmiornica.json');
            for (var i = 1; i < 5; i++) {
                this.load.audio("fail" + i, "assets/audio/instruction/fail" + i + ".mp3");
            }
            for (var i = 1; i < 5; i++) {
                this.load.audio("success" + i, "assets/audio/instruction/success" + i + ".mp3");
            }
            /*    for (var i: number = 1; i < 4; i++) {
                    this.load.image("item"+i, "assets/robo/item"+i+".PNG");
                }
    
                for (var i: number = 1; i < 4; i++) {
                    this.load.image("robot" + i, "assets/robo/robot" + i + ".PNG");
                }
                */
            /*end skrzynie"*/
            /*  end roboty */
            this.load.atlasJSONArray("scoreScreen", "assets/scoreScreen/scoreScreen.png", "assets/scoreScreen/scoreScreen.json");
            this.load.atlasJSONArray("playScoreScreen", "assets/scoreScreen/scoreScreenButtons.png", "assets/scoreScreen/scoreScreenButtons.json");
            for (var i = 1; i < 6; i++) {
                this.load.atlasJSONArray("fail" + i, "assets/feedbacks/fail" + i + ".png", "assets/feedbacks/fail" + i + ".json");
            }
            for (var i = 1; i < 5; i++) {
                this.load.atlasJSONArray("success" + i, "assets/feedbacks/success" + i + ".png", "assets/feedbacks/success" + i + ".json");
            }
            for (var i = 1; i < 5; i++) {
                this.load.audio("congrats" + i, "assets/feedbacks/congrats" + i + ".mp3");
            }
            this.load.audio("failure1", "assets/feedbacks/failure1.mp3");
            this.load.audio("failure2", "assets/feedbacks/failure2.mp3");
        };
        Preloader.prototype.create = function () {
            this.game.state.start("MainMenu", true, false);
        };
        return Preloader;
    })(Phaser.State);
    Water.Preloader = Preloader;
})(Water || (Water = {}));
var mobile;
var Water;
(function (Water) {
    var ClockView = (function (_super) {
        __extends(ClockView, _super);
        function ClockView(game, parent) {
            _super.call(this, game);
            this._creditsCount = 0;
            game.add.existing(this);
            this._parent = parent;
            var bg = this.game.add.sprite(0, 0, 'bgClock');
            this.addChild(bg);
            var style = { font: "18px Arial", fill: "#000000", align: "left" };
            this._time = this.game.add.text(70, 80, "00:00", style);
            this.addChild(this._time);
            this._sequence = this.game.add.text(35, 13, "", style);
            this.addChild(this._sequence);
            this._credits = this.game.add.text(40, 47, "", style);
            this.addChild(this._credits);
        }
        /*
        Public
        */
        ClockView.prototype.startTimer = function () {
            console.log("START CZAS");
            this.resetTimer();
            this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);
        };
        ClockView.prototype.stopTimer = function () {
            console.log("STOP CZAS");
            this.game.time.events.removeAll();
        };
        ClockView.prototype.addCredits = function () {
            this._creditsCount++;
            this.setCreditText("" + this._creditsCount);
        };
        ClockView.prototype.setSequenceText = function (text) {
            this._sequence.setText(text);
        };
        ClockView.prototype.setCreditText = function (text) {
            this._credits.setText("$" + text);
        };
        /*
        Private
        */
        ClockView.prototype.resetTimer = function () {
            console.log("RESET CZAS");
            this._time.setText("00:00");
            this._seconds = 0;
            this._minutes = 0;
        };
        ClockView.prototype.updateTimer = function () {
            this._seconds++;
            if (this._seconds >= 60) {
                this._minutes++;
                this._seconds = 0;
            }
            var sec;
            var min;
            if (this._seconds < 10) {
                sec = "0" + this._seconds;
            }
            else {
                sec = "" + this._seconds;
            }
            if (this._minutes < 10) {
                min = "0" + this._minutes;
            }
            else {
                min = "" + this._minutes;
            }
            this._time.setText(min + ":" + sec);
        };
        return ClockView;
    })(Phaser.Group);
    Water.ClockView = ClockView;
})(Water || (Water = {}));
var Water;
(function (Water) {
    var FinishLevel = (function (_super) {
        __extends(FinishLevel, _super);
        function FinishLevel(game) {
            _super.call(this, game);
            this._gameControl = Water.GameControler.getInstance();
            this._game = game;
            this.create();
        }
        FinishLevel.prototype.create = function () {
            this._finishScreen = new Water.PopUpWin(this.game, this, 0, 0);
            this._finishScreen.visible = true;
            //   this._finishScreen.animations.add("play");
            // this._finishScreen.play("play", 24, true);
            this.addChild(this._finishScreen);
            /*  this._playButton = new Phaser.Button(this._game, 0, 0, "playScoreScreen", this.playAgain, this, 1, 0, 1, 0);
  
              this._quit = new Phaser.Button(this._game, 300, 10, "playScoreScreen", this.quitClick, this, 3, 2, 3, 2);
              this._playButton.x = 257;
              this._playButton.y = 358;
              this._quit.x = 427;
              this._quit.y = 358;
              this.addChild(this._playButton);
              this.addChild(this._quit);*/
        };
        FinishLevel.prototype.hideScreenAfterFinishPlaye = function () {
            this.playAgain();
        };
        FinishLevel.prototype.playAgain = function () {
            this._gameControl.level.playAgain(true);
            this.removeAll();
            this.destroy(true);
            console.log("play again");
        };
        FinishLevel.prototype.quitClick = function () {
            console.log("quit");
        };
        return FinishLevel;
    })(Phaser.Group);
    Water.FinishLevel = FinishLevel;
})(Water || (Water = {}));
var Water;
(function (Water) {
    var FinishScreen = (function (_super) {
        __extends(FinishScreen, _super);
        function FinishScreen(game) {
            _super.call(this, game);
            this._gameControl = Water.GameControler.getInstance();
            this._game = game;
            this.create();
        }
        FinishScreen.prototype.create = function () {
            this._finishScreen = new Water.PopUp(this.game, this, 0, 0);
            this._finishScreen.visible = true;
            //new Phaser.Sprite(this._game, 0, 0, "scoreScreen");
            //    this._finishScreen.animations.add("play");
            //   this._finishScreen.play("play", 24, true);
            this.addChild(this._finishScreen);
            //   this._playButton = new Phaser.Button(this._game, 0, 0, "playScoreScreen", this.playAgain, this, 1, 0, 1, 0);
            //    this._quit = new Phaser.Button(this._game, 300, 10, "playScoreScreen", this.quitClick, this, 3, 2, 3, 2);
            //   this._playButton.x = 257;
            //   this._playButton.y = 358;
            //   this._quit.x = 427;
            //   this._quit.y = 358;
            //   this.addChild(this._playButton);
            //   this.addChild(this._quit);
        };
        FinishScreen.prototype.playAgain = function () {
            this._gameControl.restart();
            this.removeAll();
            this.destroy(true);
            console.log("play again");
        };
        FinishScreen.prototype.quitClick = function () {
            console.log("quit");
        };
        FinishScreen.prototype.hideScreenAfterFinishPlaye = function () {
            this.playAgain();
        };
        return FinishScreen;
    })(Phaser.Group);
    Water.FinishScreen = FinishScreen;
})(Water || (Water = {}));
var Water;
(function (Water) {
    var ObstacleView = (function (_super) {
        __extends(ObstacleView, _super);
        function ObstacleView(game) {
            _super.call(this, game);
            this._game = game;
            this._gameControler = Water.GameControler.getInstance();
            this._letterControler = Water.LetterControler.getInstance();
            this.enableBody = true;
            this._obstacleControl = Water.ObstacleControler.getInstance();
        }
        ObstacleView.prototype.randomBoats = function () {
            //     var rand = this.getRandom(this.boatsStrArr.length)
            //     var str: string = this.boatsStrArr[rand];
            //   var y_ = (rand == 0) ? 100 : 50;
            //    var boats: Phaser.Sprite = new Phaser.Sprite(this._game, 0, y_, str, 0);
            //    boats.frame = this.getRandom(boats.animations.frameTotal);
            return;
        };
        ObstacleView.prototype.getObstacle = function () {
            this._obstacleArr = new Array();
            this._longerObstacle = new Array();
            var obstcaleArr = this._obstacleControl.level1();
            var max = obstcaleArr.length;
            for (var i = 0; i < max; i++) {
                var obj = obstcaleArr[i];
                if (obj.name == "popletter") {
                    var popletter = new Water.PopLetter(this._game, obj.x + 600, obj.y, obj.name);
                    popletter.addLetter(this._letterControler.getPopLetter());
                    this.getTheLonger(popletter.x, popletter.width, i);
                    this.add(popletter);
                    popletter.body.velocity.x = this._gameControler.SPEEDBOAT;
                    this._obstacleArr.push(popletter);
                }
                else {
                    var obstacle = new Water.Obstacle(this._game, obj.x + 600, obj.y, obj.name);
                    this.getTheLonger(obstacle.x, obstacle.width, i);
                    this.add(obstacle);
                    obstacle.body.velocity.x = this._gameControler.SPEEDBOAT;
                    if (obj.name == "boatssmall" || obj.name == "boatsmedium") {
                        obstacle.body.setSize(0, 0, 0, 0);
                        obstacle.body.velocity.x = -70;
                    }
                    if (obj.name == "wood") {
                        obstacle.body.setSize(25, 200, 50, 0);
                    }
                    this._obstacleArr.push(obstacle);
                }
            }
            var obstacleFarAway = this._obstacleArr[this._longerObstacle[0][1]];
            //obstacleFarAway.checkWorldBounds = true
            this._whenAddNewScreen = obstacleFarAway;
            console.log(obstacleFarAway);
            //  obstacleFarAway.events.onOutOfBounds.add(this.obstacleOutOfScreen, this);
        };
        ObstacleView.prototype.getTheLonger = function (posx, width_, i) {
            if (this._longerObstacle.length > 0) {
                if (this._longerObstacle[0][0] < posx + width_) {
                    this._longerObstacle[0] = new Array(posx + width_, i);
                }
            }
            else {
                this._longerObstacle.push(new Array(posx + width_, i));
            }
        };
        ObstacleView.prototype.obstacleOutOfScreen = function () {
            console.log("boats out of screen");
            this.delObstacle();
            this.getObstacle();
        };
        ObstacleView.prototype.delObstacle = function () {
            for (var i = 0; i < this._obstacleArr.length; i++) {
                if (this._obstacleArr[i].world.x + this._obstacleArr[i].width < 0) {
                    this._obstacleArr[i].destroy(true);
                    this._obstacleArr.splice(i, 1);
                    --i;
                }
            }
        };
        ObstacleView.prototype.changeSpeed = function (stop) {
            if (stop === void 0) { stop = false; }
            var speed = (stop) ? 0 : this._gameControler.SPEEDBOAT;
            this.forEach(function (item) {
                item.body.velocity.x = speed;
                if (!stop && (item.name == "boatsmedium" || item.name == "boatssmall"))
                    item.body.velocity.x = -70;
            }, this);
        };
        ObstacleView.prototype.fastForward = function (fast) {
            if (fast === void 0) { fast = false; }
            if (!this._gameControler.collisionPlayer) {
                var speed = -130;
                if (!fast)
                    speed = -100;
                this._gameControler.SPEEDBOAT = speed;
                this.forEach(function (item) {
                    item.body.velocity.x = speed;
                }, this);
            }
        };
        ObstacleView.prototype.removeObstacleLetters = function () {
            this.forEach(function (item) {
                if (item != undefined && item.name == "popletter")
                    item.destroy(true);
            }, this);
        };
        ObstacleView.prototype.removeObstacle = function () {
            this.forEach(function (item) {
                if (item != undefined)
                    item.destroy(true);
            }, this);
        };
        ObstacleView.prototype.getRandom = function (max) {
            return Math.floor(Math.random() * max);
        };
        ObstacleView.prototype.update = function () {
            if (this._whenAddNewScreen != null && this._whenAddNewScreen.world.x < 300) {
                this.obstacleOutOfScreen();
            }
        };
        return ObstacleView;
    })(Phaser.Group);
    Water.ObstacleView = ObstacleView;
})(Water || (Water = {}));
var Water;
(function (Water) {
    var PlayerPlay = (function (_super) {
        __extends(PlayerPlay, _super);
        function PlayerPlay(game) {
            _super.call(this, game);
            this._tween = null;
            this._game = game;
            this._gameControler = Water.GameControler.getInstance();
            this.enableBody = true;
            this._obstacleControl = Water.ObstacleControler.getInstance();
            if (mobile) {
                this.game.input.onDown.add(this.movePlayer, this);
            }
            else {
                this.cursors = this.game.input.keyboard.createCursorKeys();
            }
            this.addPlayer();
        }
        PlayerPlay.prototype.addPlayer = function () {
            this._player = new Water.Player(this._game, 0, 0, "");
            this._player.x = 400; // this._player.width - 400
            this._player.y = 300;
            this.add(this._player);
            this._player.body.setSize(100, 20, -320, 80);
            this._player.forward();
        };
        PlayerPlay.prototype.movePlayer = function (pointer) {
            if (!this._gameControler.collisionPlayer) {
                var posy = pointer.y;
                if (posy < 300)
                    posy = 300;
                else if (posy > 500)
                    posy = 500;
                if (pointer.x > 630)
                    return;
                this.stopTween();
                if (!this._gameControler.collisionPlayer) {
                    var duration = (this.game.physics.arcade.distanceToPointer(this._player, pointer) / 200) * 1000;
                    this._tween = this.game.add.tween(this._player).to({ y: posy }, duration, Phaser.Easing.Linear.None).start();
                    this._tween.onComplete.add(this.endMove, this);
                }
            }
        };
        PlayerPlay.prototype.endMove = function () {
            //  this._player.playerPlay();
        };
        PlayerPlay.prototype.tweenStop = function () {
            if (this._tween != null) {
                this._tween.stop();
            }
        };
        PlayerPlay.prototype.stopTween = function () {
            if (this._tween != null && this._tween.isRunning) {
                this.game.tweens.remove(this._tween);
            }
        };
        Object.defineProperty(PlayerPlay.prototype, "player", {
            get: function () {
                return this._player;
            },
            enumerable: true,
            configurable: true
        });
        PlayerPlay.prototype.update = function () {
            if (!mobile) {
                if (this.cursors.up.isDown) {
                    console.log("nciska w gore");
                    this._player.y -= 2;
                }
                else if (this.cursors.down.isDown) {
                    console.log("nciska w dol");
                    this._player.y += 2;
                }
            }
            if (this._player.y < 300) {
                this._player.y = 299;
            }
            else if (this._player.y > 500) {
                this._player.y = 499;
            }
        };
        return PlayerPlay;
    })(Phaser.Group);
    Water.PlayerPlay = PlayerPlay;
})(Water || (Water = {}));
var Water;
(function (Water) {
    var PopUp = (function (_super) {
        __extends(PopUp, _super);
        function PopUp(game, level, x, y) {
            _super.call(this, game, x, y);
            this.level1 = level;
            this._game = game;
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var bgPopup = new Phaser.Sprite(this.game, 0, 0, "popup");
            this.addChild(bgPopup);
            this.logo = new Phaser.Sprite(this.game, 0, 0, "logoGame");
            this.addChild(this.logo);
            this.logo.x = (bgPopup.width / 2 - this.logo.width / 2);
            this.logo.y = this.logo.height - 20;
            var text = "Good try.\n Keep playing to win credits";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this.game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;
            textDescription.x = 400 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 100 + (140 / 2 - textDescription.height / 2);
            this.addChild(textDescription);
            this.textLevel();
            //this.beatText();
            this.addButtons();
            this.visible = false;
        }
        PopUp.prototype.addButtons = function () {
            this.gamesBtn = new Phaser.Button(this._game, 0, 0, "games", this.gamesBtnClick, this, 1, 0);
            this.playBtn = new Phaser.Button(this._game, 100, 0, "playGame", this.playBtnClick, this, 1, 0);
            this.gamesBtn.inputEnabled = true;
            this.gamesBtn.input.priorityID = 10;
            this.playBtn.inputEnabled = true;
            this.playBtn.input.priorityID = 10;
            var buttonsGr = new Phaser.Group(this._game);
            this.addChild(buttonsGr);
            buttonsGr.add(this.gamesBtn);
            buttonsGr.add(this.playBtn);
            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;
        };
        PopUp.prototype.activeButton = function () {
            this.gamesBtn.inputEnabled = true;
            this.playBtn.inputEnabled = true;
        };
        PopUp.prototype.gamesBtnClick = function () {
            this.overButtonA.play();
        };
        PopUp.prototype.playBtnClick = function () {
            this.overButtonA.play();
            this.visible = false;
            this.gamesBtn.inputEnabled = false;
            //   this.gamesBtn.input.priorityID = 3;
            this.playBtn.inputEnabled = false;
            // this.playBtn.input.priorityID = 3;
            this.level1.hideScreenAfterFinishPlaye();
            //  this.game.state.start("PopupStart", true, false);
        };
        PopUp.prototype.textLevel = function () {
            var text = "GAME OVER";
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this._game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = this.game.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 25;
            this.addChild(textDescription);
        };
        return PopUp;
    })(Phaser.Sprite);
    Water.PopUp = PopUp;
})(Water || (Water = {}));
function launchGame(result) {
    userLevel = result.level;
    console.log("user.level", result, result.level);
}
//declare function launchGame(result);
var Water;
(function (Water) {
    var PopupStart = (function (_super) {
        __extends(PopupStart, _super);
        function PopupStart() {
            _super.apply(this, arguments);
        }
        PopupStart.prototype.create = function () {
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var background = this.add.sprite(0, 0, 'mainMenuBg');
            var bgPopup = this.add.sprite(0, 0, "popup");
            this.logo = this.add.sprite(0, 0, "logoGame");
            this.logo.x = (bgPopup.width / 2 - this.logo.width / 2);
            this.logo.y = this.logo.height - 20;
            var text = "Collect gems in the right order.\n Get 14 gem points to win!\n Remember the order of the gems you see.";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;
            textDescription.x = this.game.stage.width / 2 - textDescription.wordWrapWidth / 2;
            textDescription.y = this.logo.y + this.logo.height + 100 + (140 / 2 - textDescription.height / 2);
            this.textLevel();
            //this.beatText();
            this.addButtons();
        };
        PopupStart.prototype.addButtons = function () {
            var gamesBtn = this.add.button(0, 0, "games", this.gamesBtnClick, this, 1, 0);
            var playBtn = this.add.button(100, 0, "playGame", this.playBtnClick, this, 1, 0);
            var buttonsGr = this.add.group();
            buttonsGr.add(gamesBtn);
            buttonsGr.add(playBtn);
            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;
        };
        PopupStart.prototype.gamesBtnClick = function () {
            this.overButtonA.play();
        };
        PopupStart.prototype.playBtnClick = function () {
            this.overButtonA.play();
            this.game.state.start("Instruction", true, false);
        };
        PopupStart.prototype.beatText = function () {
            var text = "BEAT THIS LEVEL";
            //" 6";
            var style = { font: "bold 13px Arial", fill: "#660000", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = (this.game.width / 2) + (140) - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 100;
            var numberText = "6";
            var styleNumber = { font: "bold 17px Arial", fill: "#990000", align: "center" };
            var numberShow = this.add.text(0, 0, numberText, styleNumber);
            numberShow.x = textDescription.x + textDescription.width / 2 - numberShow.width / 2;
            numberShow.y = textDescription.y + textDescription.height;
            var tunesText = "MORE TUNES TO WIN";
            var tunesShow = this.add.text(0, 0, tunesText, style);
            tunesShow.wordWrap = true;
            tunesShow.wordWrapWidth = 100;
            tunesShow.x = textDescription.x + textDescription.width / 2 - tunesShow.width / 2;
            tunesShow.y = numberShow.y + numberShow.height;
            var creditsNmText = "25";
            var styleCredits = { font: "bold 17px Arial", fill: "#ff0000", align: "center" };
            var credistNmShow = this.add.text(0, 0, creditsNmText, styleCredits);
            credistNmShow.x = tunesShow.x + tunesShow.width / 2 - credistNmShow.width / 2;
            credistNmShow.y = tunesShow.y + tunesShow.height;
            var creditsText = "CREDITS";
            var credistShow = this.add.text(0, 0, creditsText, style);
            credistShow.x = tunesShow.x + tunesShow.width / 2 - credistShow.width / 2;
            credistShow.y = credistNmShow.y + credistNmShow.height;
        };
        PopupStart.prototype.textCredits = function () {
            var text = "Beat this level";
            //" 6";
            var style = { font: "bold 14px Arial", fill: "#660000", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = (this.game.width / 2) + 257 / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 100;
            var numberText = "6";
            var styleNumber = { font: "bold 18px Arial", fill: "#990000", align: "center" };
            var numberShow = this.add.text(0, 0, numberText, styleNumber);
            numberShow.x = textDescription.x + textDescription.width / 2 - numberShow.width / 2;
            numberShow.y = textDescription.y + textDescription.height;
        };
        PopupStart.prototype.textLevel = function () {
            //APIgetLevel(userID, gameID);
            var text = "LEVEL " + userLevel;
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = this.game.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 25;
        };
        return PopupStart;
    })(Phaser.State);
    Water.PopupStart = PopupStart;
})(Water || (Water = {}));
var Water;
(function (Water) {
    var PopUpWin = (function (_super) {
        __extends(PopUpWin, _super);
        function PopUpWin(game, level, x, y) {
            _super.call(this, game, x, y);
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            this._game = game;
            this.level1 = level;
            var bgPopup = new Phaser.Sprite(this.game, 0, 0, "popup");
            this.addChild(bgPopup);
            this.logo = new Phaser.Sprite(this.game, 0, 0, "logoGame");
            this.addChild(this.logo);
            this.logo.x = (bgPopup.width / 2 - this.logo.width / 2);
            this.logo.y = this.logo.height - 20;
            var text = "You finish the level.\n Keep Playing to win credits!";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this.game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;
            textDescription.x = 400 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 100 + (140 / 2 - textDescription.height / 2);
            this.addChild(textDescription);
            this.textLevel();
            //this.beatText();
            this.addButtons();
            this.visible = false;
        }
        PopUpWin.prototype.addButtons = function () {
            this.gamesBtn = new Phaser.Button(this._game, 0, 0, "games", this.gamesBtnClick, this, 1, 0);
            this.playBtn = new Phaser.Button(this._game, 100, 0, "playGame", this.playBtnClick, this, 1, 0);
            this.gamesBtn.inputEnabled = true;
            this.gamesBtn.input.priorityID = 10;
            this.playBtn.inputEnabled = true;
            this.playBtn.input.priorityID = 10;
            var buttonsGr = new Phaser.Group(this._game);
            this.addChild(buttonsGr);
            buttonsGr.add(this.gamesBtn);
            buttonsGr.add(this.playBtn);
            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;
        };
        PopUpWin.prototype.activeButton = function () {
            this.gamesBtn.inputEnabled = true;
            this.playBtn.inputEnabled = true;
        };
        PopUpWin.prototype.gamesBtnClick = function () {
            this.overButtonA.play();
        };
        PopUpWin.prototype.playBtnClick = function () {
            this.overButtonA.play();
            this.visible = false;
            this.gamesBtn.inputEnabled = false;
            this.gamesBtn.input.priorityID = 3;
            this.playBtn.inputEnabled = false;
            this.playBtn.input.priorityID = 3;
            this.level1.hideScreenAfterFinishPlaye();
            //  this.game.state.start("PopupStart", true, false);
        };
        PopUpWin.prototype.textLevel = function () {
            var text = "GOOD JOB";
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this._game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = this.game.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 25;
            this.addChild(textDescription);
        };
        return PopUpWin;
    })(Phaser.Sprite);
    Water.PopUpWin = PopUpWin;
})(Water || (Water = {}));
var Water;
(function (Water) {
    var PromptView = (function (_super) {
        __extends(PromptView, _super);
        function PromptView(game, parent) {
            _super.call(this, game);
            game.add.existing(this);
            this._parent = parent;
            this._kayak = this.game.add.sprite(0, 0, 'prompt', 0);
            this._kayak.animations.add('anim');
            this.addChild(this._kayak);
            var style = { font: "18px Arial", fill: "#000000", align: "left" };
            this._textView = this.game.add.text(17, 155, "", style);
            this.addChild(this._textView);
            this.visible = false;
            this.y = this.game.height - this.height;
            this.x = this.game.width - this.width;
        }
        PromptView.prototype.playIntro = function () {
            this._parent.blokada(true);
            this.visible = true;
            this._kayak.animations.play('anim', 31, true, false);
            this._textView.setText("Collect the letters in the order that\nthey are shown. Are you Ready? Let's\ngive it a go!!");
            this._audio = this.game.add.audio('WakeIntro', 1, false);
            this._audio.play();
            this._audio.onStop.addOnce(this.onAudioComplete, this);
        };
        PromptView.prototype.playFeedbackGood = function (nextLevel) {
            if (nextLevel === void 0) { nextLevel = false; }
            if (!nextLevel) {
                this._parent.blokada(true);
                this.visible = true;
                this._kayak.animations.play('anim', 31, true, false);
                this._textView.setText("Holy Moley!  I'm Impressed!\nThe Next Sequence is...");
                this._audio = this.game.add.audio('WakeSuccess0', 1, false);
                this._audio.play();
                this._audio.onStop.addOnce(this.onAudioComplete, this);
            }
            else {
                this._parent.showFinishLevel();
            }
        };
        PromptView.prototype.playFeedbackWrong = function (end) {
            if (end === void 0) { end = false; }
            this._parent.blokada(true);
            this.visible = true;
            this._kayak.animations.play('anim', 31, true, false);
            this._textView.setText("That was a fair go, but let's try again.\nHave you found a way to remember\nthe sounds yet?");
            this._audio = this.game.add.audio('WakeWrong0', 1, false);
            this._audio.play();
            this._audio.onStop.addOnce(this.onAudioComplete, this);
            if (end) {
                this._audio.onStop.addOnce(this.onAudioCompleteEnd, this);
            }
            else {
                this._audio.onStop.addOnce(this.onAudioComplete, this);
            }
        };
        PromptView.prototype.onAudioComplete = function () {
            this.visible = false;
            this._parent.onPromptAudioComplete();
        };
        PromptView.prototype.onAudioCompleteNextLevel = function () {
            this.visible = false;
            this._parent.showFinishLevel();
        };
        PromptView.prototype.onAudioCompleteEnd = function () {
            this.visible = false;
            this._parent.showFinish();
        };
        return PromptView;
    })(Phaser.Group);
    Water.PromptView = PromptView;
})(Water || (Water = {}));
var Water;
(function (Water) {
    var Sequence = (function (_super) {
        __extends(Sequence, _super);
        function Sequence(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this._collectLetters = new Array();
            this._contenerLetter = new Phaser.Group(game);
            this._contenerLetter.y = 15;
            this._contenerLetter.x = 0;
            this.addChild(this._contenerLetter);
        }
        Sequence.prototype.addLetter = function (letter) {
            var style = { font: "40px Arial", fill: "#000000", align: "center" };
            var letterShow = new Phaser.Text(this.game, 0, 0, letter, style);
            letterShow.x = this._contenerLetter.width + 20;
            this._contenerLetter.addChild(letterShow);
            this._collectLetters.push(letter);
        };
        Sequence.prototype.removeLetters = function () {
            this._contenerLetter.removeAll();
            this._collectLetters.splice(0, this._collectLetters.length);
        };
        return Sequence;
    })(Phaser.Sprite);
    Water.Sequence = Sequence;
})(Water || (Water = {}));
var Water;
(function (Water) {
    var ShowLetter = (function (_super) {
        __extends(ShowLetter, _super);
        function ShowLetter(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this._gameControler = Water.GameControler.getInstance();
            this._letterControler = Water.LetterControler.getInstance();
            this.y = this.game.height / 2 - this.height / 2;
            this.x = this.game.width / 2 - this.width / 2;
        }
        ShowLetter.prototype.showLetters = function () {
            if (this._currentMax == this._currentCount) {
                console.log("hideMe");
                this.visible = false;
                this._gameControler.collisionPlayer = false;
                this._gameControler.level.stopGame(false);
                this._gameControler.level.blokada(false);
            }
            else {
                this._gameControler.level.blokada(true);
                this.showLetter(this._letterControler.currentLetters[this._currentCount]);
                this.game.time.events.add(Phaser.Timer.SECOND * 2, function () {
                    this.removeChild(this._letterShow);
                }, this);
                this.game.time.events.add(Phaser.Timer.SECOND * 2.5, function () {
                    this.showLetters();
                }, this);
                this._currentCount++;
            }
        };
        ShowLetter.prototype.addBgToDisplayLetter = function () {
            this._currentMax = this._letterControler.currentLetters.length;
            this._currentCount = 0;
            this.showLetters();
        };
        ShowLetter.prototype.showLetter = function (str) {
            var style = { font: "120px Arial", fill: "#000000", align: "center" };
            if (this._letterShow)
                this._letterShow.destroy();
            this._letterShow = new Phaser.Text(this.game, 0, 0, str, style);
            this._letterShow.x = this.width / 2 - this._letterShow.width / 2;
            this._letterShow.y = this.height / 2 - this._letterShow.height / 2 + 20;
            this.addChild(this._letterShow);
        };
        ShowLetter.prototype.playAgain = function () {
            // this._gameControler.hideLetterOnGame = true;
            // this._gameControler.hideLetterOnGame = true;
            //  this.failArr[this._gameControler._badLetters].onStop.add(this.playAgainAfterSound, this);
            //  this.failArr[this._gameControler._badLetters].play();
        };
        return ShowLetter;
    })(Phaser.Sprite);
    Water.ShowLetter = ShowLetter;
})(Water || (Water = {}));
//# sourceMappingURL=game.js.map