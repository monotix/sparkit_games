﻿module Snow {

    export class Preloader extends Phaser.State {


        preload() {
          
            var bg = this.add.image(0, 0, "bg", 0);

            var blackProgress = this.add.image(0, 0, "progressBarBlack", 0);
            var blueProgress = this.add.sprite(0, 0, "progressBarBlue", 0);


            blackProgress.position.setTo(this.stage.width / 2 - blackProgress.width / 2, this.stage.height - blackProgress.height - 10);

            blueProgress.position = blackProgress.position;
            this.load.setPreloadSprite(blueProgress);


            this.load.image("mainMenuBg", "assets/mainMenu/splashScreen.png");
            this.load.atlasJSONArray('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');
            this.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
            this.load.audio("MainMenuSn", "assets/snow/audio/ThemeMusic.mp3");
            this.load.audio("overButtonA", "assets/audio/overButton.mp3");
            this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");

         
            mobile = false;
            if (!this.game.device.desktop) {
                mobile = true;
            }


            if (mobile) {
                this.load.image("firstInstruction", "assets/03_scate_create.png");
                this.load.audio("instructionSn", "assets/audio/skate_create_02.mp3");

            }
            else {
                this.load.image("firstInstruction", "assets/02_scate_create.png");
                this.load.audio("instructionSn", "assets/audio/skate_create_01.mp3");
            }




            this.load.image("popup", "assets/popup/popup.png");
            this.load.image("logoGame", "assets/popup/logo.png");
            this.load.atlasJSONArray('games', 'assets/popup/games.png', 'assets/popup/games.json');
            this.load.atlasJSONArray('playGame', 'assets/popup/play.png', 'assets/popup/play.json');
            this.load.atlasJSONArray('helpBtn', 'assets/popup/help.png', 'assets/popup/help.json');

            this.load.image("bgGame", "assets/water/gameBackground.png");

            this.load.atlasJSONArray('boatsmedium', 'assets/water/boat/boatsmedium.png', 'assets/water/boat/boatsmedium.json');
            this.load.atlasJSONArray('boatssmall', 'assets/water/boat/boatssmall.png', 'assets/water/boat/boatssmall.json');

            /* instructions */
            this.load.image('prompt', 'assets/snow/prompt/instruction1.png');
            this.load.audio("WakeIntro", "assets/snow/prompt/IntroPrompt.mp3");
            this.load.audio("WakeSuccess0", "assets/snow/prompt/SuccessPrompt4.mp3");
            this.load.audio("WakeWrong0", "assets/snow/prompt/FailurePrompt1_0.mp3");

            /* 


            */

            this.load.audio("jumpAd", "assets/snow/player/jump.wav");
            this.load.audio("psssAd", "assets/snow/player/psss.mp3");
            this.load.audio("monetaAd", "assets/snow/player/moneta.mp3");
            this.load.audio("crashAd", "assets/snow/player/crash.mp3");



            
            this.load.image("firstInstruction", "assets/water/instructionsScreen.png");
            this.load.image("sequence", "assets/snow/letterClip.png");
            /* player */

            this.load.atlasJSONArray('player', 'assets/snow/player/paddle.png', 'assets/snow/player/paddle.json');
            this.load.atlasJSONArray('jump', 'assets/snow/player/grind.png', 'assets/snow/player/grind.json');
            this.load.atlasJSONArray('forwardSpeed', 'assets/water/player/charging.png', 'assets/water/player/charging.json');
            this.load.atlasJSONArray('left', 'assets/snow/player/left.png', 'assets/snow/player/left.json');
            this.load.atlasJSONArray('right', 'assets/snow/player/right.png', 'assets/snow/player/right.json');
            this.load.atlasJSONArray('miganie', 'assets/water/player/miganie.png', 'assets/water/player/miganie.json');
            this.load.atlasJSONArray('crashforward', 'assets/snow/player/crashForward.png', 'assets/snow/player/crashForward.json');



            for (var i: number = 1; i < 7; i++) {
                this.load.atlasJSONArray('tricki'+i, 'assets/water/player/trick'+i+'.png', 'assets/water/player/trick'+i+'.json');
            }

            this.load.atlasJSONArray('special', 'assets/water/special.png', 'assets/water/special.json');
            this.load.atlasJSONArray('jumpbtn', 'assets/water/jumpBtn.png', 'assets/water/jumpBtn.json');


            this.load.image('bgletter', 'assets/water/letter/bgletter.png');
            
            this.load.image("bgClock", "assets/water/interface.png");


            /* obstacle */

        /*    this.load.image("piasek", "assets/water/obstacle/wood2.png");
            this.load.image("wood", "assets/water/obstacle/wood.png");
            this.load.image("hammer", "assets/water/obstacle/hammer.png");
            this.load.image("turtle", "assets/water/obstacle/turtle.png");
            this.load.image("zielone", "assets/water/obstacle/zielone.png");
            this.load.image("ball", "assets/water/obstacle/ball.png");
            this.load.image("ball2", "assets/water/obstacle/ball2.png");
            this.load.image("walen", "assets/water/obstacle/walen.png");
            */
            this.load.image("wood2", "assets/snow/obstacle/wood2.png");
            this.load.image("wood", "assets/snow/obstacle/wood.png");
            this.load.image("jumper", "assets/snow/obstacle/jumper.png");
            this.load.image("jumper2", "assets/snow/obstacle/jumper2.png");
            this.load.image("choinka", "assets/snow/obstacle/choinka.png");
            this.load.image("balwan", "assets/snow/obstacle/balwan.png");
            this.load.image("pien", "assets/snow/obstacle/pien.png");
            this.load.image("rocks", "assets/snow/obstacle/rocks.png");
            this.load.image("rocks2", "assets/snow/obstacle/rocks2.png");

            this.load.atlasJSONArray('popletter', 'assets/snow/popletter/popletter1.png', 'assets/snow/popletter/popletter1.json');
           


            this.load.atlasJSONArray('shark1', 'assets/water/obstacle/shark.png', 'assets/water/obstacle/shark.json');
            this.load.atlasJSONArray('shark2', 'assets/water/obstacle/shark2.png', 'assets/water/obstacle/shark2.json');
            this.load.atlasJSONArray('shark3', 'assets/water/obstacle/shark3.png', 'assets/water/obstacle/shark3.json');
            this.load.atlasJSONArray('shark4', 'assets/water/obstacle/shark4.png', 'assets/water/obstacle/shark4.json');

            this.load.atlasJSONArray('osmiornica', 'assets/water/obstacle/osmiornica.png', 'assets/water/obstacle/osmiornica.json');


          //  this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");

         

            for (var i: number = 1; i < 5; i++) {

                this.load.audio("fail" + i, "assets/audio/instruction/fail" + i + ".mp3");


            }

            for (var i: number = 1; i < 5; i++) {

                this.load.audio("success" + i, "assets/audio/instruction/success" + i + ".mp3");


            }

            /*skrzynie*/

         

         

            this.load.atlasJSONArray("scoreScreen", "assets/scoreScreen/scoreScreen.png", "assets/scoreScreen/scoreScreen.json");
            this.load.atlasJSONArray("playScoreScreen", "assets/scoreScreen/scoreScreenButtons.png", "assets/scoreScreen/scoreScreenButtons.json");


            for (var i: number = 1; i < 6; i++) {

                this.load.atlasJSONArray("fail" + i, "assets/feedbacks/fail" + i + ".png", "assets/feedbacks/fail" + i + ".json");



            }


            for (var i: number = 1; i < 5; i++) {

                this.load.atlasJSONArray("success" + i, "assets/feedbacks/success" + i + ".png", "assets/feedbacks/success" + i + ".json");



            }

            for (var i: number = 1; i < 5; i++) {
                this.load.audio("congrats" + i, "assets/feedbacks/congrats" + i + ".mp3");



            }

            this.load.audio("failure1", "assets/feedbacks/failure1.mp3");
            this.load.audio("failure2", "assets/feedbacks/failure2.mp3");


            this.load.image("szare", "letters/szare.png");
            this.load.image("szared", "letters/szared.png");
            this.load.image("szared2", "letters/szereb2.png");
            this.load.image("orange", "letters/poma.png");
            this.load.image("green", "letters/ziel.png");
            this.load.image("bg", "letters/bg.png");
            this.load.image("bgDrop", "letters/bgDrop.png");
            this.load.atlasJSONArray('doneBtn', 'letters/done.png', 'letters/done.json');
            this.load.atlasJSONArray('clearBtn', 'letters/clearbtn.png', 'letters/clearbtn.json');

           

            this.load.atlasJSONArray("niceTry", "letters/fail1.png", "letters/fail1.json");
            this.load.atlasJSONArray("letterSuccess1", "letters/perfect.png", "letters/perfect.json");
            this.load.atlasJSONArray("letterSuccess2", "letters/awesome.png", "letters/awesome.json");
            this.load.atlasJSONArray("letterSuccess3", "letters/waytogo.png", "letters/waytogo.json");
            this.load.atlasJSONArray("letterSuccess4", "letters/yougotit.png", "letters/yougotit.json");

        }


        create() {
           
            this.game.state.start("MainMenu", true, false);

        }

    }


} 

var mobile;