﻿module Snow {

    export class Level extends Phaser.State {


        private _bg: Phaser.Group;
        private _obstcaleGroup: ObstacleView;
        private _gameControler: GameControler;

        private _letterControler: LetterControler;
        private _prompt: PromptView;

        private _sequenceGr: Phaser.Group;
        private _sequence: Sequence;
        private _playerPlay: PlayerPlay;


        private _showLetter: ShowLetter;
        private _showLetterGr: Phaser.Group;

        private _clock: ClockView;

        private _finishScreenLevel: FinishLevel;
        private _finishScreen: FinishScreen;
        private _special: Phaser.Sprite;
        private _jumpBtn: Phaser.Button;
        private _timer: number = 0;
        private _timerCounter;
        private rightKey;
        private _blokada: Phaser.Graphics; 
        private jumpPressed: boolean = false;
        private _blokadaGr: Phaser.Group;
        private _gameGr: Phaser.Group;
        public _isShowEndScrren: boolean = false;
        private letters: Letters;
        private cursors;
        private _speedPlayer: number = 5;
        private helpGr;
        private _helpBtn;
        private _instructionShow;
        create() {

            this._gameControler = GameControler.getInstance();

            this._gameControler.setLevel(this);
            this._gameControler.currentLevel = userLevel;

            this._letterControler = LetterControler.getInstance();
            this._letterControler.setRandomLetters();


            this._blokadaGr = this.add.group();

            this._bg = this.game.add.group();
            this.game.add.image(0, 0, "bgGame", 0, this._bg);

            this._obstcaleGroup = new ObstacleView(this.game);

         
            this._obstcaleGroup.getObstacle();


            this._playerPlay = new PlayerPlay(this.game);
            this.stage.addChild(this._playerPlay);

            this.stage.addChild(this._obstcaleGroup);
            /*
            to jest gadajacy koles. 
            playIntro() - mowi intro
            playFeedbackGood() - mowi jak wykonamy poprawnie sekwencje
            playFeedbackWrong() - mowi jak zrobimy blad
            */

            this._gameGr = this.add.group();




            this._sequenceGr = this.add.group();
            this._sequence = new Sequence(this.game, 15, 30, "sequence");
            this._sequenceGr.add(this._sequence);

            this.stage.addChild(this._sequenceGr);

            this.stage.addChild(this._blokadaGr);
            this._prompt = new PromptView(this.game, this);
            this._prompt.position.setTo(this.game.width - this._prompt.width, this.game.height - this._prompt.height);
         
            this._prompt.playIntro();

           

          

       
           

            

            /*
            to jest zegar
            startTimer() start timera
            stopTimer() stop timera
            setSequenceText(text) dodajemy text w sequence
            setCreditsText(text) dodajemy text w credits. $(dolar) jest dodawany w klasie
            */
            this._clock = new ClockView(this.game, this);
            this._clock.position.setTo(this.game.width - this._clock.width, 0);
            this.stage.addChild(this._clock);

            this._clock.startTimer();
            this._clock.setSequenceText("1/2");
            this._clock.setCreditText("0");
            this.stopGame(true);

            this._gameControler.level.stopGame(false);

            this._gameGr.add(this._obstcaleGroup);
            this._gameGr.add(this._playerPlay);

            
      //      this._showLetterGr = this.game.add.group();
           // this.stage.addChild(this._showLetterGr);

       //     this._special = this.add.sprite(0, 0, "special");
       //     this._special.y = this.game.height - this._special.height  - 30;
        //    this._special.x = 550;
           // this._special.animations.add("play");


          //  this._jumpBtn = this.add.button(0, 0, "jumpbtn", this.jummBtnDown, this, 1, 0, 1, 0);
          //  this._jumpBtn.y = this._special.y + 15;
          //  this._jumpBtn.x = 630;
             
          ///  this.stage.addChild(this._special);
         //   this.stage.addChild(this._jumpBtn);
         //   this._jumpBtn.onInputDown.add(this.jummBtnDown, this);
         //   this._jumpBtn.onInputUp.add(this.jummBtnUp, this);

       /*     this.game.input.keyboard.onUpCallback = function (e) {
                if (e.keyCode == Phaser.Keyboard.UP) {
                    this.jummBtnUp();
                }
            };*/

            if (!mobile) {

                this.cursors = this.game.input.keyboard.createCursorKeys();
        
                this.game.input.keyboard.addKeyCapture([
                    Phaser.Keyboard.LEFT,
                    Phaser.Keyboard.RIGHT,
                    Phaser.Keyboard.UP,
                    Phaser.Keyboard.DOWN,
                    Phaser.Keyboard.SPACEBAR
                ]);
        

            }

            this.helpGr = this.add.group();
            this.stage.addChild(this.helpGr);



            this.stage.addChild(this._prompt);

            this.stage.addChild(this._sequence);


            this._helpBtn = this.add.button(0, 0, "helpBtn", this.helpBtnClick, this, 1, 0, 0, 0, this.helpGr);
            this.helpGr.x = 0;//800 - this.helpGr.width;
            this.helpGr.y = 600 - this.helpGr.height;

            this._helpBtn.buttonMode = true;
            this._helpBtn.inputEnabled = true;
            this._helpBtn.input.priorityID = 110;
            //this.showLetterMenu(new Array("a"));
           
            this.game.input.onDown.add(function () { if (this.game.paused) { this.game.paused = false; } if (this._instructionShow) { this.stage.removeChild(this._instructionShow); this._instructionShow.kill(); this._instructionShow = null; } }, this);
           
          
        }

        private helpBtnClick() {

            console.log("pokaz instrukcje");
            this.game.paused = !this.game.paused;

            if (this._instructionShow == null) this._instructionShow = this.add.image(0, 0, "firstInstruction", 0);
            

            // this.add.image(0, 0, "firstInstruction", 0);
            this._instructionShow.width = this.game.world.width;//this.stage.width;
            this._instructionShow.height = this.game.world.height;
            this.stage.addChild(this._instructionShow);

        }


        youwinSequence() {

          //  this.stopGame(false);
            this._clock.setSequenceText("1/2");
        }

        youLose() {
         //   console.log("przegrales chuju:");
            this._gameControler.checkBadAnswer();
        }


        public showLetterMenu(_collectLetters: Array<string>) {
           
           

            this.stopGame(true);
            this.letters = new Letters(this.game, 0, 0, "", _collectLetters, this);
            console.log("jaka szerokosc",this.game.world.width / 2, this.letters.widthScreen / 2);
            this.letters.x = this.game.world.width / 2 - this.letters.widthScreen / 2;
            this.letters.y = this.game.world.height / 2 - this.letters.heightScreen / 2;
            this.stage.addChild(this.letters);

        }

        public addCredits() {
             
            this._clock.addCredits();

        }

        addPlayerToTop(top: boolean) {

            if (top) {
                this._gameGr.addChild(this._obstcaleGroup);
                this._gameGr.addChild(this._playerPlay);

              

            }
            else {
                this._gameGr.addChild(this._playerPlay);
                this._gameGr.addChild(this._obstcaleGroup);
              

            }
           
            this.stage.addChild(this._prompt);
        }


       public blokada(show:boolean = true) {

       /*     if (this._blokada == null) {
                this._blokada = new Phaser.Graphics(this.game, 0, 0);
                this._blokada.beginFill(0x000000, 0.6);
                this._blokada.drawRect(0, 0, this.game.width, this.game.height);
                this._blokada.endFill();
                this._blokadaGr.add(this._blokada);
                this._blokada.visible = false;
            }
           
            this._blokada.visible = show;*/
        }

      

        

        playAgain(win:boolean = false) {
            this._isShowEndScrren = false;
            this._obstcaleGroup.delObstacle();
            this._obstcaleGroup.removeObstacle();
            this._obstcaleGroup.getObstacle()
            this.stopGame(true);
            this._clock.setCreditText("0");

            if (win) {
            //    this._clock.setSequenceText("1/2");
                this.stopGame(false);
            }
            else {
                this._prompt.playFeedbackWrong();
            }
         


        }

        /*
        Ta funkcja jest odpalana jak koles skonczy audio
        */
        public onPromptAudioComplete() {
            console.log("Koles skonczyl gadac");
            this.removeSequence();
           // this._gameControler.collisionPlayer = false;
           // this._gameControler.level.stopGame(false);
           // this._showLetter = new ShowLetter(this.game, 0, 0, "bgletter");
           // this._showLetterGr.add(this._showLetter);
            
           // this._showLetter.addBgToDisplayLetter();
            this.stopGame(false);
        }

        public sequenceLevel(str:string){
            this._clock.setSequenceText(str);

        }

        public showFinishLevel(win: boolean = false) {
            console.log("FInish Level:");

            this.stopGame(true);
            this._isShowEndScrren = true;
            this._finishScreenLevel = new FinishLevel(this.game);
            if (win) this._finishScreenLevel.youwin();
            else this._finishScreenLevel.youLose();
            this._finishScreenLevel.x = 0;//this.game.width / 2 - this._finishScreenLevel.width / 2;
            this._finishScreenLevel.y = 0;//this.game.height / 2 - this._finishScreenLevel.height / 2;
            this.stage.addChild(this._finishScreenLevel);
            this.stopGame(true);

           


        }

        public showFinish() {
          
            this._finishScreen = new FinishScreen(this.game);
            this._finishScreen.x = this.game.width / 2 - this._finishScreen.width / 2;
            this._finishScreen.y = this.game.height / 2 - this._finishScreen.height / 2;
            this.stage.addChild(this._finishScreen);
            this.stopGame(true);
        }

        public showPrompt(correct: boolean, end: boolean = false) {

            if (correct) {
                this.stopGame(true);
                this._obstcaleGroup.removeObstacleLetters();
                this._prompt.playFeedbackGood(end);
            }
            else {
                this.stopGame(true);
                this._gameControler.collisionPlayer = true;
                this._prompt.playFeedbackWrong(end);
               
            }
        }

        public removeSequence() {

            this._sequence.removeLetters();
        }

        public stopGame(stop:boolean,changePos:boolean = false) {
            if (!this._isShowEndScrren) {
                this._obstcaleGroup.changeSpeed(stop, changePos);
                this._playerPlay._stop = stop;
            }
        }


        update() {

            if (!this._gameControler.collisionPlayer) {
              //  console.log("position player", this._playerPlay.toGlobal(this._playerPlay.player.position));
                if (!mobile) {
                    if (this.cursors.right.isDown) {
                        this._playerPlay.getPlayer().x += this._speedPlayer;
                        this._playerPlay.getPlayer().right();
                    }

                    else if (this.cursors.left.isDown) {
                        this._playerPlay.getPlayer().x -= this._speedPlayer;
                        this._playerPlay.getPlayer().left();
                    }

                    if (this.cursors.up.isDown) {
                        this._playerPlay.getPlayer().y -= this._speedPlayer;

                    }

                    else if (this.cursors.down.isDown) {
                        this._playerPlay.getPlayer().y += this._speedPlayer;

                    }
                }
                this.game.physics.arcade.overlap(this._playerPlay, this._obstcaleGroup, this.collision, null, this);


              //  console.log(this._playerPlay.player.x);
    /*            if (this._playerPlay.x < 100) this._playerPlay.x = 200;
                else if (this._playerPlay.x > 600) this._playerPlay.x = 550;
                if (this._playerPlay.y < 100) this._playerPlay.y = 200;
                else if (this._playerPlay.y > 800) this._playerPlay.y = 750;*/
/*  
             
                
                if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
                    this._gameControler.collisionPlayer = true;
                    this._playerPlay.player.jump();
                    console.log("Jump====>");
                }
                

                if (this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {
                    console.log("PRAWO====>");
                    // this._gameControler.collisionPlayer = true;
                    // this._playerPlay.player.fastforward();
                    // this._obstcaleGroup.fastForward();
                }
                else {


                }

                if (this.jumpPressed) this._timer++;
             */   
            }
        }


        collision(object, object2) {

            console.log("kolizja", object2.name, this._gameControler.collisionPlayer);
            if (!this._gameControler.collisionPlayer) {

               
                if (object2.name == "popletter") {
                    
                    object2.body.setSize(-1, -1, -1200, -1600);
                    // this._sequence.addLetter(object2.letter);
                    //  this._letterControler.checkWord(object2.letter);
                    object2.collisonPlayAnim();


                }
                else if (object2.name == "correctLetter") {
                    
                 
                    object2.body.setSize(-1, -1, -1200, -1600);
                   // object2.name = "die";
                    this._sequence.addLetter(object2.letter);
                    // this._letterControler.checkWord(object2.letter);
                    this._obstcaleGroup.changeLetterToCoin();
                    this._letterControler.addCorrectLetters(object2.letter);
                    object2.collisonPlayAnim();


                }
                else if (object2.name == "coin") {
                    object2.body.setSize(-1, -1, -1200, -1600);
                 //   this._sequence.addLetter(object2.letter);
                  //  this._letterControler.checkWord(object2.letter);
                    object2.coinAd();
                    this.addCredits();


                    object2.kill();


                }
                else if (object2.name == "jumper2") {
                    console.log("chujjjjaaja", this._obstcaleGroup.toGlobal(object2.position));
                    var pos = this._obstcaleGroup.toGlobal(object2.position);
                    this._playerPlay.changePos(new Phaser.Point(pos.x+80, pos.y+20));
                  //  this._playerPlay.y = 0;object2.game.world.y + 30;
                  //  this._playerPlay.stopTween();
                    this.addPlayerToTop(true);
                    this._gameControler.collisionPlayer = true;
                    this._playerPlay.player.jump();
                   
                }

                else {
                    console.log("kolizja z ",object2.name);
                    this._gameControler.collisionPlayer = true;
                    this._playerPlay.player.crashForward();
                   
                }

              
                this._timer = 0;
                //this._special.animations.stop("play",true);
            }

        }

        render() {
           /*
             this.game.debug.quadTree(this.game.physics.arcade.quadTree);
       
             this._obstcaleGroup.forEach(function (item) {
                 // item.body.velocity = 0;
              
                 this.game.debug.body(item);
             }, this)

             this._playerPlay.forEach(function (item) {
                 // item.body.velocity = 0;
              
                 this.game.debug.body(item);
             }, this)
            */
        

        }

    }
}
