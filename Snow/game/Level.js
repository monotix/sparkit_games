var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Snow;
(function (Snow) {
    var Level = (function (_super) {
        __extends(Level, _super);
        function Level() {
            _super.apply(this, arguments);
            this._timer = 0;
            this.jumpPressed = false;
        }
        Level.prototype.create = function () {
            this._gameControler = Snow.GameControler.getInstance();
            this._gameControler.setLevel(this);
            this._letterControler = Snow.LetterControler.getInstance();
            this._letterControler.setRandomLetters();
            this._blokadaGr = this.add.group();
            this._bg = this.game.add.group();
            this.game.add.image(0, 0, "bgGame", 0, this._bg);
            this._obstcaleGroup = new Snow.ObstacleView(this.game);
            this.stage.addChild(this._obstcaleGroup);
            this._obstcaleGroup.getObstacle();
            this._playerPlay = new Snow.PlayerPlay(this.game);
            this.stage.addChild(this._playerPlay);
            /*
            to jest gadajacy koles.
            playIntro() - mowi intro
            playFeedbackGood() - mowi jak wykonamy poprawnie sekwencje
            playFeedbackWrong() - mowi jak zrobimy blad
            */
            this._sequenceGr = this.add.group();
            this._sequence = new Snow.Sequence(this.game, 0, 0, "sequence");
            this._sequenceGr.add(this._sequence);
            this.stage.addChild(this._blokadaGr);
            this._prompt = new Snow.PromptView(this.game, this);
            this._prompt.position.setTo(this.game.width - this._prompt.width, this.game.height - this._prompt.height);
            this.stage.addChild(this._prompt);
            this._prompt.playFeedbackWrong();
            /*
            to jest zegar
            startTimer() start timera
            stopTimer() stop timera
            setSequenceText(text) dodajemy text w sequence
            setCreditsText(text) dodajemy text w credits. $(dolar) jest dodawany w klasie
            */
            this._clock = new Snow.ClockView(this.game, this);
            this._clock.position.setTo(this.game.width - this._clock.width, 0);
            this.stage.addChild(this._clock);
            this._clock.startTimer();
            this._clock.setSequenceText("1/2");
            this._clock.setCreditText("0");
            this.stopGame(true);
            this._showLetterGr = this.game.add.group();
            this.stage.addChild(this._showLetterGr);
            this._special = this.add.sprite(0, 0, "special");
            this._special.y = this.game.height - this._special.height - 30;
            this._special.x = 550;
            this._special.animations.add("play");
            this._jumpBtn = this.add.button(0, 0, "jumpbtn", this.jummBtnDown, this, 1, 0, 1, 0);
            this._jumpBtn.y = this._special.y + 15;
            this._jumpBtn.x = 630;
            this.stage.addChild(this._special);
            this.stage.addChild(this._jumpBtn);
            this._jumpBtn.onInputDown.add(this.jummBtnDown, this);
            this._jumpBtn.onInputUp.add(this.jummBtnUp, this);
            /*     this.game.input.keyboard.onUpCallback = function (e) {
                     if (e.keyCode == Phaser.Keyboard.UP) {
                         this.jummBtnUp();
                     }
                 };*/
        };
        Level.prototype.addCredits = function () {
            this._clock.addCredits();
        };
        Level.prototype.blokada = function (show) {
            if (show === void 0) { show = true; }
            if (this._blokada == null) {
                this._blokada = new Phaser.Graphics(this.game, 0, 0);
                this._blokada.beginFill(0x000000, 0.6);
                this._blokada.drawRect(0, 0, this.game.width, this.game.height);
                this._blokada.endFill();
                this._blokadaGr.add(this._blokada);
                this._blokada.visible = false;
            }
            this._blokada.visible = show;
        };
        Level.prototype.jummBtnDown = function () {
            if (!this._gameControler.collisionPlayer) {
                this.jumpPressed = true;
                this._special.animations.play("play");
            }
        };
        Level.prototype.jummBtnUp = function () {
            this.jumpPressed = false;
            console.log("timer tricki", this._timer);
            if (!this._gameControler.collisionPlayer) {
                if (this._timer > 60) {
                    this._gameControler.collisionPlayer = true;
                    this._playerPlay.player.jumptrick();
                }
                else {
                    this._gameControler.collisionPlayer = true;
                    this._playerPlay.player.jump();
                }
                this._timer = 0;
                this._special.animations.stop("play", true);
            }
        };
        Level.prototype.playAgain = function () {
            this._obstcaleGroup.delObstacle();
            this._obstcaleGroup.removeObstacle();
            this._obstcaleGroup.getObstacle();
            this.stopGame(true);
            this._clock.setCreditText("0");
            this._clock.setSequenceText("1/2");
            this._prompt.playFeedbackWrong();
        };
        /*
        Ta funkcja jest odpalana jak koles skonczy audio
        */
        Level.prototype.onPromptAudioComplete = function () {
            console.log("Koles skonczyl gadac");
            this.removeSequence();
            this._showLetter = new Snow.ShowLetter(this.game, 0, 0, "bgletter");
            this._showLetterGr.add(this._showLetter);
            this._showLetter.addBgToDisplayLetter();
        };
        Level.prototype.sequenceLevel = function (str) {
            this._clock.setSequenceText(str);
        };
        Level.prototype.showFinishLevel = function () {
            console.log("FInish Level:");
            this._finishScreenLevel = new Snow.FinishLevel(this.game);
            this._finishScreenLevel.x = this.game.width / 2 - this._finishScreenLevel.width / 2;
            this._finishScreenLevel.y = this.game.height / 2 - this._finishScreenLevel.height / 2;
            this.stage.addChild(this._finishScreenLevel);
            this.stopGame(true);
        };
        Level.prototype.showFinish = function () {
            this._finishScreen = new Snow.FinishScreen(this.game);
            this._finishScreen.x = this.game.width / 2 - this._finishScreen.width / 2;
            this._finishScreen.y = this.game.height / 2 - this._finishScreen.height / 2;
            this.stage.addChild(this._finishScreen);
            this.stopGame(true);
        };
        Level.prototype.showPrompt = function (correct, end) {
            if (end === void 0) { end = false; }
            if (correct) {
                this.stopGame(true);
                this._obstcaleGroup.removeObstacleLetters();
                this._prompt.playFeedbackGood(end);
            }
            else {
                this.stopGame(true);
                this._gameControler.collisionPlayer = true;
                this._prompt.playFeedbackWrong(end);
            }
        };
        Level.prototype.removeSequence = function () {
            this._sequence.removeLetters();
        };
        Level.prototype.stopGame = function (stop) {
            this._obstcaleGroup.changeSpeed(stop);
        };
        Level.prototype.update = function () {
            if (!this._gameControler.collisionPlayer) {
                this.game.physics.arcade.overlap(this._playerPlay, this._obstcaleGroup, this.collision, null, this);
                if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
                    this._gameControler.collisionPlayer = true;
                    this._playerPlay.player.jump();
                    console.log("Jump====>");
                }
                if (this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {
                    console.log("PRAWO====>");
                }
                else {
                }
                if (this.jumpPressed)
                    this._timer++;
            }
        };
        Level.prototype.collision = function (object, object2) {
            console.log("kolizja", object2.name);
            if (!this._gameControler.collisionPlayer) {
                if (object2.name == "popletter") {
                    this._sequence.addLetter(object2.letter);
                    this._letterControler.checkWord(object2.letter);
                    object2.destroy(true);
                }
                else {
                    this._playerPlay.player.crashForward();
                    this._gameControler.collisionPlayer = true;
                }
                this._timer = 0;
                this._special.animations.stop("play", true);
            }
        };
        Level.prototype.render = function () {
            /*
                 this.game.debug.quadTree(this.game.physics.arcade.quadTree);
           
                 this._obstcaleGroup.forEach(function (item) {
                     // item.body.velocity = 0;
                  
                     this.game.debug.body(item);
                 }, this)
    
                 this._playerPlay.forEach(function (item) {
                     // item.body.velocity = 0;
                  
                     this.game.debug.body(item);
                 }, this)
    
            */
        };
        return Level;
    })(Phaser.State);
    Snow.Level = Level;
})(Snow || (Snow = {}));
//# sourceMappingURL=Level.js.map