﻿module Snow {

    export class Game extends Phaser.Game {

        constructor() {

            console.log("start");
            super(800, 600, Phaser.AUTO, "content", null,true);
          
            this.state.add("Boot", Boot, false);
            this.state.add("Preloader", Preloader, false);
            this.state.add("MainMenu", MainMenu, false);
            this.state.add("PopupStart", PopupStart, false);
            this.state.add("Instruction", Instruction, false);
            this.state.add("Level", Level, false);
            this.state.start("Boot");

        }



    }

}

window.onload = () => {

    var game = new Snow.Game;

}

var userLevel = 1;