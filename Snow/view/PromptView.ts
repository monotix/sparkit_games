﻿module Snow {

    export class PromptView extends Phaser.Group {

        private _textView: Phaser.Text;
        private _kayak: Phaser.Sprite;
        private _audio: Phaser.Sound;
        private _parent: Level;
        constructor(game: Phaser.Game, parent:Level) {
            super(game);
            game.add.existing(this);

          
            this._parent = parent;
            this._kayak = this.game.add.sprite(0, 0, 'prompt', 0);
           // this._kayak.animations.add('anim');
            this._kayak.scale.setTo(0.6, 0.6);
            this.addChild(this._kayak);

            var style = { font: "18px Arial", fill: "#FFFFFF", align: "left" };
            this._textView = this.game.add.text(17, 75, "",style);
            this.addChild(this._textView);
            this.visible = false;

            this.y = this.game.height - this.height;
            this.x = this.game.width - this.width;
           
        }


       
        
        public playIntro() {
            this._parent.blokada(true);
            this.visible = true;
           // this._kayak.animations.play('anim', 31, true, false);
            this._textView.setText("Get ready to shred! Collect the red letters only.\n remember the order and sounds of the letters, \nand then combine them to make a word.\n be careful don't wipe out on anything!!");
            this._audio = this.game.add.audio('WakeIntro', 1, false);
            this._audio.play();
            this._audio.onStop.addOnce(this.onAudioComplete, this);
        }

        public playFeedbackGood(nextLevel: boolean = false) {
            if (!nextLevel) {

            this._parent.blokada(true);
            this.visible = true;
            this._kayak.animations.play('anim', 31, true, false);
            this._textView.setText("Holy Moley!  I'm Impressed!\nThe Next Sequence is...");
            this._audio = this.game.add.audio('WakeSuccess0', 1, false);
            this._audio.play();
            this._audio.onStop.addOnce(this.onAudioComplete, this);
              
            }
            else {
                this._parent.showFinishLevel();
            }
        }

        public playFeedbackWrong(end: boolean = false) {
            this._parent.blokada(true);
            this.visible = true;
            this._kayak.animations.play('anim', 31, true, false);
            this._textView.setText("Good effort, that was close!\n Let's take another listen as you collect the letters.");
            this._audio = this.game.add.audio('WakeWrong0', 1, false);
            this._audio.play();
            this._audio.onStop.addOnce(this.onAudioComplete, this);

            if (end) {
                this._audio.onStop.addOnce(this.onAudioCompleteEnd, this);
            }
            else {
                this._audio.onStop.addOnce(this.onAudioComplete, this);
            }
        }

        private onAudioComplete() {
            this.visible = false;
            this._parent.onPromptAudioComplete();
        }

        private onAudioCompleteNextLevel() {
            this.visible = false;
            this._parent.showFinishLevel(true);
        }

        private onAudioCompleteEnd() {
            this.visible = false;
            this._parent.showFinish();
        }
    }
} 