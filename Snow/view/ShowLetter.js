var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Snow;
(function (Snow) {
    var ShowLetter = (function (_super) {
        __extends(ShowLetter, _super);
        function ShowLetter(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this._gameControler = Snow.GameControler.getInstance();
            this._letterControler = Snow.LetterControler.getInstance();
            this.y = this.game.height / 2 - this.height / 2;
            this.x = this.game.width / 2 - this.width / 2;
        }
        ShowLetter.prototype.showLetters = function () {
            if (this._currentMax == this._currentCount) {
                console.log("hideMe");
                this.visible = false;
                this._gameControler.collisionPlayer = false;
                this._gameControler.level.stopGame(false);
                this._gameControler.level.blokada(false);
            }
            else {
                this._gameControler.level.blokada(true);
                this.showLetter(this._letterControler.currentLetters[this._currentCount]);
                this.game.time.events.add(Phaser.Timer.SECOND * 2, function () {
                    this.removeChild(this._letterShow);
                }, this);
                this.game.time.events.add(Phaser.Timer.SECOND * 2.5, function () {
                    this.showLetters();
                }, this);
                this._currentCount++;
            }
        };
        ShowLetter.prototype.addBgToDisplayLetter = function () {
            this._currentMax = this._letterControler.currentLetters.length;
            this._currentCount = 0;
            this.showLetters();
        };
        ShowLetter.prototype.showLetter = function (str) {
            var style = { font: "120px Arial", fill: "#000000", align: "center" };
            if (this._letterShow)
                this._letterShow.destroy();
            this._letterShow = new Phaser.Text(this.game, 0, 0, str, style);
            this._letterShow.x = this.width / 2 - this._letterShow.width / 2;
            this._letterShow.y = this.height / 2 - this._letterShow.height / 2;
            this.addChild(this._letterShow);
        };
        ShowLetter.prototype.playAgain = function () {
            // this._gameControler.hideLetterOnGame = true;
            // this._gameControler.hideLetterOnGame = true;
            //  this.failArr[this._gameControler._badLetters].onStop.add(this.playAgainAfterSound, this);
            //  this.failArr[this._gameControler._badLetters].play();
        };
        return ShowLetter;
    })(Phaser.Sprite);
    Snow.ShowLetter = ShowLetter;
})(Snow || (Snow = {}));
//# sourceMappingURL=ShowLetter.js.map