﻿module Snow {
    export class PlayerPlay extends Phaser.Group {

        private _gameControler: GameControler;
        private _obstacleControl: ObstacleControler;
        private _game: Phaser.Game;
        private _player: Player;
        private _tween: Phaser.Tween = null;
        private _leftRight: number;
        public _stop: boolean = false;
        private _stopTween: boolean = false;
        constructor(game: Phaser.Game) {

            super(game);
            this._game = game;
            this._gameControler = GameControler.getInstance();




            this.enableBody = true;


            this._obstacleControl = ObstacleControler.getInstance();

           if(mobile) this.game.input.onDown.add(this.movePlayer, this);
          
            this.addPlayer();
        }

        addPlayer() {

            this._player = new Player(this._game, 0, 0, "");

            this._player.x = 400;// this._player.width - 400
            this._player.y =300;
            this.add(this._player);
            this.lastPos = new Phaser.Point(400, 300);

            this._leftRight = this.player.x;

            this._player.body.setSize(60, 20, -100,0);

            this._player.forward();

        }

        public getPlayer(): Player{
            return this._player;
        }

        private lastPos: Phaser.Point;
        movePlayer(pointer) {

            if (!this._gameControler.collisionPlayer && !this._stop && !this._stopTween) {


             
              //  this._stopTween = true;

                if (this._leftRight < pointer.x) {

                    this._player.right();
                }

                else {

                    this._player.left();

                }

                this._leftRight = pointer.x;

                var posy: number = pointer.y
               // if (posy < 300) posy = 300;
               if (posy > 500) posy = 500;
               // if (pointer.x > 630) return;
               this.tweenStop();
               posy = Math.floor(posy);
               var posx = Math.floor(pointer.x);
               if (posx > 890) posx = 800;
               else if (posx < 100) posx = 100;
           //    if (posx < 0) posx = 0;
            //   if (posy < 0) posy = 0;
               var ok: boolean = false;
               if (this.lastPos != null && this.grannicaKlikania(new Phaser.Point(posx, posy))){
                   this.lastPos = new Phaser.Point(posx, posy);
                   ok = true;
               }

              

               console.log("pozycja",posx, posy,this._tween,this._player.x,this._player.y);
                if (!this._gameControler.collisionPlayer && ok) {
                    var duration = (this.game.physics.arcade.distanceToPointer(this._player, pointer) / 200) * 1000;


                    this._tween = this.game.add.tween(this._player).to({ y: Math.floor(posy), x: Math.floor(posx) }, duration, Phaser.Easing.Linear.None).start();
                   this._tween.onComplete.add(this.endMove, this);

                }

            }


        }

        grannicaKlikania(pos: Phaser.Point): boolean {

            if (pos.x != this.lastPos.x ) {

                if (pos.y != this.lastPos.y ) {

                    return true;
                }

            }

            return false;
        }


        endMove() {
           // this._player.forward();
            this._stopTween = false;
        }

        changePos(pointer: Phaser.Point) {

            console.log("zmiana pozycji", pointer);
            this.tweenStop();

            if (!this._gameControler.collisionPlayer) {
                // var duration = (this.game.physics.arcade.distanceToPointer(this._player, pointer) / 200) * 1000;


                this._tween = this.game.add.tween(this._player).to({ y: Math.floor(pointer.y), x: Math.floor(pointer.x) }, 2, Phaser.Easing.Linear.None).start();
                this._tween.onComplete.add(this.endMove, this);
            }

        }

        tweenStop() {
         
            if (this._tween != null) {
                console.log("tween player stop");
                this._tween.stop();

            }

        }



        stopTween() {
            console.log("usuwam tween", this._tween != null, this._tween.isRunning);
            if (this._tween != null && this._tween.isRunning) {
                this.game.tweens.remove(this._tween);

            }

           

        }

        public get player(): Player {

            return this._player;
        }

        upadte() {

            console.log("czy grupa ma update??");

        }


    }

}