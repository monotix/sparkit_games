﻿module Snow {

    export class ObstacleView extends Phaser.Group {

     

        private _longerObstacle: Array<any>;
        private _game: Phaser.Game;
        private _obstacleArr: Array<any>;
        private _whenAddNewScreen: Obstacle;
        private _gameControler: GameControler;
        private _obstacleControl: ObstacleControler;
        private _letterControler: LetterControler;
        private _boundsOut: Phaser.Sprite;

        constructor(game: Phaser.Game) {

            super(game);
            this._game = game;
            this._gameControler = GameControler.getInstance();
            this._letterControler = LetterControler.getInstance();
          


            this.enableBody = true;

         
            this._obstacleControl = ObstacleControler.getInstance();

            this._boundsOut = new Phaser.Sprite(this._game, 0, 0, "");
            var grfx: Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);

            grfx.beginFill(0x000000, 1);
            grfx.drawRect(0, 0, 100, 100);
            grfx.endFill();

            this._boundsOut.addChild(grfx);
            this._boundsOut.y = this.game.height - 100;
            this._boundsOut.x = this.game.width;
           
         //   this.add(this._boundsOut);
            
     /*       this._boundsOut.body.velocity.x = -270;//this._gameControler.SPEEDX;
            this._boundsOut.body.velocity.y = 0;
            this._boundsOut.body.immovable = true;
            this._boundsOut.body.checkCollision.none = true;
            this._boundsOut.checkWorldBounds = true;*/
           // this._boundsOut.events.onOutOfBounds.add(this.getObstacle, this);
        }






        randomBoats(): Phaser.Sprite {

       //     var rand = this.getRandom(this.boatsStrArr.length)
       //     var str: string = this.boatsStrArr[rand];

         //   var y_ = (rand == 0) ? 100 : 50;

        //    var boats: Phaser.Sprite = new Phaser.Sprite(this._game, 0, y_, str, 0);
        //    boats.frame = this.getRandom(boats.animations.frameTotal);

            return ;

        }

        getObstacle() {
            console.log("outofScreen", this._boundsOut.x, this._boundsOut.y);
            this.delObstacles();
           // this._boundsOut.y = this.game.height - 100;
           // this._boundsOut.x = this.game.width;
            this._obstacleArr = new Array();
            this._longerObstacle = new Array();
         //   if (!this._gameControler.collisionPlayer && this._gameControler.SPEEDX != 0) {
                var obstcaleArr: Array<any> = this._obstacleControl.level1();
                var max: number = obstcaleArr.length;

                for (var i: number = 0; i < max; i++) {

                    var obj = obstcaleArr[i];

                    if (obj.name == "popletter") {

                        var letter = this._letterControler.getPopLetter();
                        var objectName = obj.name;

                        var popletter: PopLetter = new PopLetter(this._game, obj.x + 800, obj.y + 320, obj.name);
                       
                        popletter.isRed(letter[0].correct);
                        popletter.addLetter(letter[0].str);

                       // this.getTheLonger(popletter.x, popletter.width, i);
                        this.add(popletter);
                        popletter.body.velocity = new Phaser.Point(this._gameControler.SPEEDX, this._gameControler.SPEEDY);
                        this._obstacleArr.push(popletter);
                        popletter.body.setSize(60, 50, 20, 0);


                    }
                    else {


                        var obstacle: Obstacle = new Obstacle(this._game, obj.x +800, obj.y+320, obj.name);
                        this.getTheLonger(obstacle.x, obstacle.width, i);

                        this.add(obstacle);
                        obstacle.body.velocity = new Phaser.Point(this._gameControler.SPEEDX, this._gameControler.SPEEDY);
                        // obstacle.body.velocity.y = this._gameControler.SPEEDBOAT;
                       

                        if (obj.name == "wood") {

                            obstacle.body.setSize(100, 50, 0, 0);

                        }
                        else if (obj.name == "jumper2") {

                            obstacle.body.setSize(100, 50, 15, 25);

                        }
                        else if (obj.name == "choinka") {

                            obstacle.body.setSize(100, 50, 0, 225);

                        }
                        else if (obj.name == "balwan") {

                            obstacle.body.setSize(70, 50, 0, 50);

                        }

                        this._obstacleArr.push(obstacle);


                    }




                }

         //   }
                console.log(this._longerObstacle == null,  this._longerObstacle.length);
            if (this._longerObstacle == null || this._longerObstacle.length == 0) {

                var obstacle: Obstacle = new Obstacle(this._game, obj.x + 800, obj.y + 320, "wood");
                this.getTheLonger(obstacle.x, obstacle.width, i);
                obstacle.visible = false;
                this.add(obstacle);
                obstacle.body.velocity = new Phaser.Point(this._gameControler.SPEEDX, this._gameControler.SPEEDY);
                obstacle.body.setSize(0, 0, 0, 0);

                this._obstacleArr.push(obstacle);
                this.getTheLonger(536, 423, 0);


            }
            console.log(this._longerObstacle);
            var obstacleFarAway = this._obstacleArr[this._longerObstacle[0][1]];
            obstacleFarAway.checkWorldBounds = true
            this._whenAddNewScreen = obstacleFarAway;
          //  console.log("obstacle ile oboiektow wylosowano", max, obstacleFarAway);
         //  obstacleFarAway.events.onOutOfBounds.add(this.obstacleOutOfScreen, this);
            
          
         //   this.game.time.events.add(Phaser.Timer.SECOND * 4, this.getObstacle, this)

        }

        addAgain() {


        }

        getTheLonger(posx:number,width_:number, i:number) {
            console.log("test", posx, width_, i, this._longerObstacle.length);
            if (this._longerObstacle.length > 0) {

                if (this._longerObstacle[0][0] < posx + width_) {

                    this._longerObstacle[0] = new Array(posx + width_, i);

                }
            }
            else {

                this._longerObstacle.push(new Array(posx + width_,i));

            }

        }
      

       
        changeLetterToCoin() {

            this.forEach(function (item) {
                if (item.name == "popletter" || item.name == "correctLetter") {

                    item.isCoin();
                }
               

            }, this);


        }
        

       obstacleOutOfScreen() {
            console.log("boats out of screen");
           // this.delObstacle();
           // this.getObstacle();
        }

        delObstacle() {


            for (var i: number=0; i < this._obstacleArr.length; i++) {

                if (this._obstacleArr[i].world.x + this._obstacleArr[i].width < 0) {
                    this._obstacleArr[i].destroy(true);
                    this._obstacleArr.splice(i, 1);

                    --i;


                }

            }

       }

        delObstacles() {
        
           
            this.forEach(function (item) {
              
                if (item != null && item != undefined && this.toGlobal(item.position).x < -1000) {
                 //   console.log("niszcze obiekt", item.name, this.toGlobal(item.position).x);
                    item.destroy(true) 

                }
                //  if (!stop && (item.name == "boatsmedium" || item.name == "boatssmall")) item.body.velocity.x = -70;
              
            }, this);
           

        }

        changeSpeed(stop:boolean=false,changePos:boolean=false) {
            var speed = (stop) ? 0 : this._gameControler.SPEEDX;
            var speedy = (stop) ? 0 : this._gameControler.SPEEDY;
            console.log("stop", stop,speed);
            this.forEach(function (item) {
                item.body.velocity = new Phaser.Point(speed, speedy);
                if (changePos) {
                    item.x += 150;
                    item.y += 100;

                }
              //  if (!stop && (item.name == "boatsmedium" || item.name == "boatssmall")) item.body.velocity.x = -70;
              
            }, this);
         //   this._boundsOut.body.velocity.x = -270;
         //   this._boundsOut.body.velocity.y = 0;

        }


        removeObstacleLetters() {


            this.forEach(function (item) {
                if(item != undefined && item.name == "popletter") item.destroy(true);

            }, this);


        }

        removeObstacle() {
          

            this.forEach(function (item) {
                if (item != undefined )  item.destroy(true);

            }, this);


        }



        getRandom(max: number):number {

            return Math.floor(Math.random() * max);

        }

        
        update() {
          
           // console.log("osttatni",this.toGlobal(this._whenAddNewScreen.position).x);
            if (this._whenAddNewScreen != null && this.toGlobal(this._whenAddNewScreen.position).x < 500) {

                this.getObstacle();

            }

        }

    }


} 