var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Snow;
(function (Snow) {
    var PromptView = (function (_super) {
        __extends(PromptView, _super);
        function PromptView(game, parent) {
            _super.call(this, game);
            game.add.existing(this);
            this._parent = parent;
            this._kayak = this.game.add.sprite(0, 0, 'prompt', 0);
            this._kayak.animations.add('anim');
            this.addChild(this._kayak);
            var style = { font: "18px Arial", fill: "#000000", align: "left" };
            this._textView = this.game.add.text(17, 155, "", style);
            this.addChild(this._textView);
            this.visible = false;
            this.y = this.game.height - this.height;
            this.x = this.game.width - this.width;
        }
        PromptView.prototype.playIntro = function () {
            this._parent.blokada(true);
            this.visible = true;
            this._kayak.animations.play('anim', 31, true, false);
            this._textView.setText("Collect the letters in the order that\nthey are shown. Are you Ready? Let's\ngive it a go!!");
            this._audio = this.game.add.audio('WakeIntro', 1, false);
            this._audio.play();
            this._audio.onStop.addOnce(this.onAudioComplete, this);
        };
        PromptView.prototype.playFeedbackGood = function (nextLevel) {
            if (nextLevel === void 0) { nextLevel = false; }
            if (!nextLevel) {
                this._parent.blokada(true);
                this.visible = true;
                this._kayak.animations.play('anim', 31, true, false);
                this._textView.setText("Holy Moley!  I'm Impressed!\nThe Next Sequence is...");
                this._audio = this.game.add.audio('WakeSuccess0', 1, false);
                this._audio.play();
                this._audio.onStop.addOnce(this.onAudioComplete, this);
            }
            else {
                this._parent.showFinishLevel();
            }
        };
        PromptView.prototype.playFeedbackWrong = function (end) {
            if (end === void 0) { end = false; }
            this._parent.blokada(true);
            this.visible = true;
            this._kayak.animations.play('anim', 31, true, false);
            this._textView.setText("That was a fair go, but let's try again.\nHave you found a way to remember\nthe sounds yet?");
            this._audio = this.game.add.audio('WakeWrong0', 1, false);
            this._audio.play();
            this._audio.onStop.addOnce(this.onAudioComplete, this);
            if (end) {
                this._audio.onStop.addOnce(this.onAudioCompleteEnd, this);
            }
            else {
                this._audio.onStop.addOnce(this.onAudioComplete, this);
            }
        };
        PromptView.prototype.onAudioComplete = function () {
            this.visible = false;
            this._parent.onPromptAudioComplete();
        };
        PromptView.prototype.onAudioCompleteNextLevel = function () {
            this.visible = false;
            this._parent.showFinishLevel();
        };
        PromptView.prototype.onAudioCompleteEnd = function () {
            this.visible = false;
            this._parent.showFinish();
        };
        return PromptView;
    })(Phaser.Group);
    Snow.PromptView = PromptView;
})(Snow || (Snow = {}));
//# sourceMappingURL=PromptView.js.map