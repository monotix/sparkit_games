var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Snow;
(function (Snow) {
    var ObstacleView = (function (_super) {
        __extends(ObstacleView, _super);
        function ObstacleView(game) {
            _super.call(this, game);
            this._game = game;
            this._gameControler = Snow.GameControler.getInstance();
            this._letterControler = Snow.LetterControler.getInstance();
            this.enableBody = true;
            this._obstacleControl = Snow.ObstacleControler.getInstance();
        }
        ObstacleView.prototype.randomBoats = function () {
            //     var rand = this.getRandom(this.boatsStrArr.length)
            //     var str: string = this.boatsStrArr[rand];
            //   var y_ = (rand == 0) ? 100 : 50;
            //    var boats: Phaser.Sprite = new Phaser.Sprite(this._game, 0, y_, str, 0);
            //    boats.frame = this.getRandom(boats.animations.frameTotal);
            return;
        };
        ObstacleView.prototype.getObstacle = function () {
            this._obstacleArr = new Array();
            this._longerObstacle = new Array();
            var obstcaleArr = this._obstacleControl.level1();
            var max = obstcaleArr.length;
            for (var i = 0; i < max; i++) {
                var obj = obstcaleArr[i];
                if (obj.name == "popletter") {
                    var popletter = new Snow.PopLetter(this._game, obj.x + 600, obj.y, obj.name);
                    popletter.addLetter(this._letterControler.getPopLetter());
                    this.getTheLonger(popletter.x, popletter.width, i);
                    this.add(popletter);
                    popletter.body.velocity.x = this._gameControler.SPEEDBOAT;
                    this._obstacleArr.push(popletter);
                }
                else {
                    var obstacle = new Snow.Obstacle(this._game, obj.x + 600, obj.y, obj.name);
                    this.getTheLonger(obstacle.x, obstacle.width, i);
                    this.add(obstacle);
                    obstacle.body.velocity.x = this._gameControler.SPEEDBOAT;
                    if (obj.name == "boatssmall" || obj.name == "boatsmedium") {
                        obstacle.body.setSize(0, 0, 0, 0);
                        obstacle.body.velocity.x = -70;
                    }
                    if (obj.name == "wood") {
                        obstacle.body.setSize(25, 200, 50, 0);
                    }
                    this._obstacleArr.push(obstacle);
                }
            }
            var obstacleFarAway = this._obstacleArr[this._longerObstacle[0][1]];
            //obstacleFarAway.checkWorldBounds = true
            this._whenAddNewScreen = obstacleFarAway;
            console.log(obstacleFarAway);
            //  obstacleFarAway.events.onOutOfBounds.add(this.obstacleOutOfScreen, this);
        };
        ObstacleView.prototype.getTheLonger = function (posx, width_, i) {
            if (this._longerObstacle.length > 0) {
                if (this._longerObstacle[0][0] < posx + width_) {
                    this._longerObstacle[0] = new Array(posx + width_, i);
                }
            }
            else {
                this._longerObstacle.push(new Array(posx + width_, i));
            }
        };
        ObstacleView.prototype.obstacleOutOfScreen = function () {
            console.log("boats out of screen");
            this.delObstacle();
            this.getObstacle();
        };
        ObstacleView.prototype.delObstacle = function () {
            for (var i = 0; i < this._obstacleArr.length; i++) {
                if (this._obstacleArr[i].world.x + this._obstacleArr[i].width < 0) {
                    this._obstacleArr[i].destroy(true);
                    this._obstacleArr.splice(i, 1);
                    --i;
                }
            }
        };
        ObstacleView.prototype.changeSpeed = function (stop) {
            if (stop === void 0) { stop = false; }
            var speed = (stop) ? 0 : this._gameControler.SPEEDBOAT;
            this.forEach(function (item) {
                item.body.velocity.x = speed;
                if (!stop && (item.name == "boatsmedium" || item.name == "boatssmall"))
                    item.body.velocity.x = -70;
            }, this);
        };
        ObstacleView.prototype.fastForward = function (stop) {
            if (stop === void 0) { stop = false; }
            var speed = -130;
            this.forEach(function (item) {
                item.body.velocity.x = speed;
            }, this);
        };
        ObstacleView.prototype.removeObstacleLetters = function () {
            this.forEach(function (item) {
                if (item != undefined && item.name == "popletter")
                    item.destroy(true);
            }, this);
        };
        ObstacleView.prototype.removeObstacle = function () {
            this.forEach(function (item) {
                if (item != undefined)
                    item.destroy(true);
            }, this);
        };
        ObstacleView.prototype.getRandom = function (max) {
            return Math.floor(Math.random() * max);
        };
        ObstacleView.prototype.update = function () {
            if (this._whenAddNewScreen != null && this._whenAddNewScreen.world.x < 300) {
                this.obstacleOutOfScreen();
            }
        };
        return ObstacleView;
    })(Phaser.Group);
    Snow.ObstacleView = ObstacleView;
})(Snow || (Snow = {}));
//# sourceMappingURL=ObstacleView.js.map