﻿declare function APIsetLevel(userID, gameID, userLevel);
declare function APIgetLevel(userID, gameID);
declare var userID: any;
declare var gameID: any;

module Snow {

    export class ClockView extends Phaser.Group {

        private _sequence: Phaser.Text;
        private _credits: Phaser.Text;
        private _time: Phaser.Text;
        private _parent: Level;
        private _seconds: number;
        private _minutes: number;
        private _creditsCount: number = 0;
        private _sequencCount: number = 0;
        private _gameControler: GameControler = GameControler.getInstance();
        constructor(game: Phaser.Game, parent: Level) {
            super(game);
            game.add.existing(this);

            this._parent = parent;

            var bg: Phaser.Sprite = this.game.add.sprite(0, 0, 'bgClock');
            this.addChild(bg);

            var style = { font: "18px Arial", fill: "#000000", align: "left" };
            this._time = this.game.add.text(70, 80, "00:00", style);
            this.addChild(this._time);

            this._sequence = this.game.add.text(35, 13, "", style);
            this.addChild(this._sequence);

            this._credits = this.game.add.text(40, 47, "", style);
            this.addChild(this._credits);
        }
        /*
        Public
        */
        public startTimer() {
            console.log("START CZAS");
            this.resetTimer();
            this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);
        }

        public stopTimer() {

            console.log("STOP CZAS");
            this.game.time.events.removeAll();
        }


        public addCredits() {
            this._creditsCount++;
            this.setCreditText(""+this._creditsCount);

        }
        public setSequenceText(text: string) {
            this._sequencCount++;
            if (this._sequencCount > 2) {
                console.log("next Level");

                this._sequencCount = 1;
                this._parent.stopGame(true);
                this._parent.showFinishLevel(true);
                this._sequence.setText("" + this._sequencCount + "/2");
                
                if (userLevel < 5) {
                    APIsetLevel(userID, gameID, userLevel + 1);
                    userLevel++;
                    this._gameControler.currentLevel = userLevel;
                }
            }
            else {

                this._sequence.setText("" + this._sequencCount + "/2");
                this._parent._isShowEndScrren = false;
                this._parent.stopGame(false);

            }

           
        }

        public setCreditText(text: string) {
            this._credits.setText("$" + text);
        }
        /*
        Private
        */
        private resetTimer() {
            console.log("RESET CZAS");
            this._time.setText("00:00");
            this._seconds = 0;
            this._minutes = 0;
        }

        private updateTimer() {
            this._seconds++;
            if (this._seconds >= 60) {
                this._minutes++;
                this._seconds = 0;
            }

            var sec: String;
            var min: String;
            if (this._seconds < 10) {
                sec = "0" + this._seconds;
            } else {
                sec = ""+this._seconds;
            } 

            if (this._minutes < 10) {
                min = "0" + this._minutes;
            } else {
                min = "" + this._minutes;
            }

            this._time.setText(min + ":" + sec);
        }
        
    }
} 