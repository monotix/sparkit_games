var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Snow;
(function (Snow) {
    var FinishScreen = (function (_super) {
        __extends(FinishScreen, _super);
        function FinishScreen(game) {
            _super.call(this, game);
            this._gameControl = Snow.GameControler.getInstance();
            this._game = game;
            this.create();
        }
        FinishScreen.prototype.create = function () {
            this._finishScreen = new Phaser.Sprite(this._game, 0, 0, "scoreScreen");
            this._finishScreen.animations.add("play");
            this._finishScreen.play("play", 24, true);
            this.addChild(this._finishScreen);
            this._playButton = new Phaser.Button(this._game, 0, 0, "playScoreScreen", this.playAgain, this, 1, 0, 1, 0);
            this._quit = new Phaser.Button(this._game, 300, 10, "playScoreScreen", this.quitClick, this, 3, 2, 3, 2);
            this._playButton.x = 257;
            this._playButton.y = 358;
            this._quit.x = 427;
            this._quit.y = 358;
            this.addChild(this._playButton);
            this.addChild(this._quit);
        };
        FinishScreen.prototype.playAgain = function () {
            this._gameControl.restart();
            this.removeAll();
            this.destroy(true);
            console.log("play again");
        };
        FinishScreen.prototype.quitClick = function () {
            console.log("quit");
        };
        return FinishScreen;
    })(Phaser.Group);
    Snow.FinishScreen = FinishScreen;
})(Snow || (Snow = {}));
//# sourceMappingURL=FinishScreen.js.map