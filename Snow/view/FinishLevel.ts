﻿module Snow {


    export class FinishLevel extends Phaser.Group {
        private _finishScreen: Phaser.Sprite;
        private _playButton: Phaser.Button;
        private _quit: Phaser.Button;
        private _game: Phaser.Game;
        private _gameControl: GameControler = GameControler.getInstance();
        private gameOverSpr: Phaser.Sprite;
        private youWinSpr: Phaser.Sprite;
        private screenWinOrGameOver: any;
        private _bgBlack: Phaser.Graphics;
        private _bgSpr: Phaser.Sprite;
        private _win: boolean = false;
        constructor(game: Phaser.Game) {
            super(game);
            this._game = game;
            this.create();
        }

        create() {


            this._bgBlack = new Phaser.Graphics(this.game, 0, 0);
            this._bgBlack.beginFill(0x000000, 0.6);
            this._bgBlack.drawRect(0, 0, this.game.width, this.game.height);
            this._bgBlack.endFill();

            this._bgSpr = new Phaser.Sprite(this.game, 0, 0, "");
            this._bgSpr.addChild(this._bgBlack);
            this.add(this._bgSpr);


            this._bgSpr.buttonMode = true;
            this._bgSpr.inputEnabled = true;
            this._bgSpr.input.priorityID = 10;

            this._bgSpr.events.onInputDown.add(this.onDown, this);

        //    this.gameOverSpr = new PopUp(this.game, this, 0, 0);
         //   this.youWinSpr = new PopUpWin(this.game, this, 0, 0);



            // this.visible = 



           // this.add(this.gameOverSpr);

//            this.addChild(this.youWinSpr);
          
           
        }


        youwin() {
            this._win = true;
            this.screenWinOrGameOver = new PopUpWin(this.game, this, 0, 0);
            this.addTostage();
        }

        youLose() {
            this._win = false;
            this.screenWinOrGameOver = new PopUp(this.game, this, 0, 0);
            this.addTostage();
        }

        addTostage() {
            this.addChild(this.screenWinOrGameOver);
            this.screenWinOrGameOver.activeButton();
            this.screenWinOrGameOver.visible = true;
        }

        public hideScreenAfterFinishPlaye() {

            this.playAgain();
        }

        onDown() {
            console.log("ondown finish");
        }

        playAgain() {


            if (this.screenWinOrGameOver != null) {
                this.screenWinOrGameOver.kill();
            }


          //  this._gameControl.level.clearBtnClick();
            this.visible = false;
            this._gameControl.level.playAgain(this._win);
            this.removeAll();
            this.destroy(true);
            console.log("play again");

        }

        quitClick() {
            console.log("quit");

        }

    }

}   