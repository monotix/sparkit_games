var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Snow;
(function (Snow) {
    var Sequence = (function (_super) {
        __extends(Sequence, _super);
        function Sequence(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this._collectLetters = new Array();
            this._contenerLetter = new Phaser.Group(game);
            this._contenerLetter.y = 15;
            this._contenerLetter.x = 0;
            this.addChild(this._contenerLetter);
        }
        Sequence.prototype.addLetter = function (letter) {
            var style = { font: "40px Arial", fill: "#000000", align: "center" };
            var letterShow = new Phaser.Text(this.game, 0, 0, letter, style);
            letterShow.x = this._contenerLetter.width + 20;
            this._contenerLetter.addChild(letterShow);
            this._collectLetters.push(letter);
        };
        Sequence.prototype.removeLetters = function () {
            this._contenerLetter.removeAll();
            this._collectLetters.splice(0, this._collectLetters.length);
        };
        return Sequence;
    })(Phaser.Sprite);
    Snow.Sequence = Sequence;
})(Snow || (Snow = {}));
//# sourceMappingURL=Sequence.js.map