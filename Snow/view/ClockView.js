var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Snow;
(function (Snow) {
    var ClockView = (function (_super) {
        __extends(ClockView, _super);
        function ClockView(game, parent) {
            _super.call(this, game);
            this._creditsCount = 0;
            game.add.existing(this);
            this._parent = parent;
            var bg = this.game.add.sprite(0, 0, 'bgClock');
            this.addChild(bg);
            var style = { font: "18px Arial", fill: "#000000", align: "left" };
            this._time = this.game.add.text(70, 80, "00:00", style);
            this.addChild(this._time);
            this._sequence = this.game.add.text(35, 13, "", style);
            this.addChild(this._sequence);
            this._credits = this.game.add.text(40, 47, "", style);
            this.addChild(this._credits);
        }
        /*
        Public
        */
        ClockView.prototype.startTimer = function () {
            console.log("START CZAS");
            this.resetTimer();
            this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);
        };
        ClockView.prototype.stopTimer = function () {
            console.log("STOP CZAS");
            this.game.time.events.removeAll();
        };
        ClockView.prototype.addCredits = function () {
            this._creditsCount++;
            this.setCreditText("" + this._creditsCount);
        };
        ClockView.prototype.setSequenceText = function (text) {
            this._sequence.setText(text);
        };
        ClockView.prototype.setCreditText = function (text) {
            this._credits.setText("$" + text);
        };
        /*
        Private
        */
        ClockView.prototype.resetTimer = function () {
            console.log("RESET CZAS");
            this._time.setText("00:00");
            this._seconds = 0;
            this._minutes = 0;
        };
        ClockView.prototype.updateTimer = function () {
            this._seconds++;
            if (this._seconds >= 60) {
                this._minutes++;
                this._seconds = 0;
            }
            var sec;
            var min;
            if (this._seconds < 10) {
                sec = "0" + this._seconds;
            }
            else {
                sec = "" + this._seconds;
            }
            if (this._minutes < 10) {
                min = "0" + this._minutes;
            }
            else {
                min = "" + this._minutes;
            }
            this._time.setText(min + ":" + sec);
        };
        return ClockView;
    })(Phaser.Group);
    Snow.ClockView = ClockView;
})(Snow || (Snow = {}));
//# sourceMappingURL=ClockView.js.map