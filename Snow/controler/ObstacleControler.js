var Snow;
(function (Snow) {
    var ObstacleControler = (function () {
        function ObstacleControler() {
        }
        ObstacleControler.prototype.construct = function () {
            if (ObstacleControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            ObstacleControler._instance = this;
        };
        ObstacleControler.getInstance = function () {
            if (ObstacleControler._instance === null) {
                ObstacleControler._instance = new ObstacleControler();
            }
            return ObstacleControler._instance;
        };
        ObstacleControler.prototype.level1 = function () {
            var arr = new Array(new Array({ x: 216, y: 94, name: "ball" }, { x: 515, y: 98, name: "popletter" }, { x: 874, y: 93, name: "wood" }), new Array({ x: 256, y: 96, name: "ball" }, { x: 546, y: 105, name: "popletter" }, { x: 863, y: 93, name: "ball" }), new Array({ x: 256, y: 96, name: "ball" }, { x: 546, y: 105, name: "popletter" }, { x: 863, y: 93, name: "ball" }, { x: 902, y: 96, name: "boatssmall" }), new Array({ x: 209, y: 96, name: "ball" }, { x: 487, y: 102, name: "popletter" }, { x: 802, y: 102, name: "popletter" }, { x: 902, y: 96, name: "boatssmall" }, { x: 414, y: 96, name: "boatsmedium" }), new Array({ x: 209, y: 96, name: "ball" }, { x: 465, y: 93, name: "ball" }, { x: 875, y: 93, name: "ball" }, { x: 414, y: 96, name: "boatsmedium" }), new Array({ x: 209, y: 96, name: "ball" }, { x: 465, y: 93, name: "ball" }, { x: 875, y: 93, name: "popletter" }, { x: 414, y: 96, name: "boatsmedium" }), new Array({ x: 270, y: 99, name: "wood" }, { x: 843, y: 99, name: "wood" }, { x: 554, y: 93, name: "popletter" }, { x: 697, y: 96, name: "boatsmedium" }), new Array({ x: 129, y: 96, name: "ball" }, { x: 425, y: 93, name: "ball" }, { x: 657, y: 93, name: "ball" }, { x: 407, y: 96, name: "boatsmedium" }), new Array({ x: 490, y: 101, name: "popletter" }, { x: 155, y: 98, name: "popletter" }, { x: 895, y: 100, name: "popletter" }, { x: 902, y: 96, name: "boatssmall" }), new Array({ x: 209, y: 99, name: "popletter" }, { x: 644, y: 92, name: "popletter" }, { x: 954, y: 95, name: "popletter" }, { x: 334, y: 96, name: "boatsmedium" }, { x: 860, y: 96, name: "boatsmedium" }));
            return arr[this.getRandom(arr.length)];
        };
        ObstacleControler.prototype.level2 = function () {
            var arr = new Array(new Array({ x: 144, y: 179, name: "walen" }, { x: 430, y: 185, name: "walen" }, { x: 804, y: 101, name: "popletter" }, { x: 1204, y: 101, name: "osmiornica" }, { x: 860, y: 96, name: "boatssmedium" }), new Array({ x: 335, y: 90, name: "ball" }, { x: 504, y: 96, name: "popletter" }, { x: 336, y: 202, name: "piasek" }, { x: 765, y: 181, name: "walen" }, { x: 1131, y: 96, name: "ball" }, { x: 411, y: 96, name: "boatssmedium" }, { x: 1015, y: 96, name: "boatssmall" }), new Array({ x: 164, y: 180, name: "walen" }, { x: 494, y: 102, name: "osmiornica" }, { x: 539, y: 101, name: "piasek" }, { x: 733, y: 98, name: "popletter" }, { x: 968, y: 180, name: "walen" }, { x: 491, y: 96, name: "boatssmall" }), new Array({ x: 236, y: 183, name: "walen" }, { x: 547, y: 183, name: "walen" }, { x: 939, y: 183, name: "walen" }), new Array({ x: 212, y: 98, name: "ball" }, { x: 506, y: 103, name: "osmiornica" }, { x: 755, y: 96, name: "popletter" }), new Array({ x: 137, y: 98, name: "ball" }, { x: 337, y: 98, name: "ball" }, { x: 516, y: 99, name: "popletter" }, { x: 922, y: 108, name: "osmiornica" }, { x: 1000, y: 199, name: "piasek" }, { x: 409, y: 199, name: "boatsmedium" }), new Array({ x: 216, y: 98, name: "ball" }, { x: 991, y: 98, name: "popletter" }, { x: 460, y: 99, name: "popletter" }, { x: 666, y: 180, name: "walen" }, { x: 769, y: 199, name: "piasek" }), new Array({ x: 209, y: 98, name: "ball" }, { x: 392, y: 98, name: "popletter" }, { x: 989, y: 99, name: "popletter" }, { x: 676, y: 180, name: "walen" }, { x: 330, y: 199, name: "piasek" }, { x: 1021, y: 193, name: "piasek" }, { x: 1106, y: 199, name: "boatsmedium" }, { x: 306, y: 199, name: "boatssmall" }), new Array({ x: 1030, y: 98, name: "ball" }, { x: 121, y: 98, name: "popletter" }, { x: 459, y: 99, name: "popletter" }, { x: 748, y: 102, name: "osmiornica" }, { x: 339, y: 199, name: "boatsmedium" }), new Array({ x: 170, y: 98, name: "ball" }, { x: 423, y: 98, name: "popletter" }, { x: 707, y: 99, name: "popletter" }, { x: 475, y: 180, name: "piasek" }, { x: 858, y: 199, name: "boatsmedium" }, { x: 897, y: 185, name: "walen" }));
            return arr[this.getRandom(arr.length)];
        };
        ObstacleControler.prototype.getRandom = function (max) {
            return Math.floor(Math.random() * max);
        };
        ObstacleControler._instance = null;
        return ObstacleControler;
    })();
    Snow.ObstacleControler = ObstacleControler;
})(Snow || (Snow = {}));
//# sourceMappingURL=ObstacleControler.js.map