﻿module Snow {

    export class ObstacleControler {


        public static _instance: ObstacleControler = null;

        
        construct() {

            if (ObstacleControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            ObstacleControler._instance = this;

        }




        public static getInstance(): ObstacleControler {

            if (ObstacleControler._instance === null) {

                ObstacleControler._instance = new ObstacleControler();


            }


            return ObstacleControler._instance;



        }

        level1():Array<any> {

            var arr: Array<any> = new Array(
                new Array(
                    
                    { x: 192, y: 21, name: "popletter" },
                    { x: 27, y: 183, name: "popletter" }
                 

                    ),

                new Array(
                  
                    { x: 31, y: 33, name: "popletter" },
                    { x: 311, y: 293, name: "popletter" }
                    ),
                new Array(
                    { x: 63, y: 32, name: "rocks" },
                    { x: 346, y: 370, name: "popletter" },
                    { x: 459, y: 265, name: "popletter" },
                    { x: 536, y: 423, name: "wood" }
                 

                    ),
                new Array(
                    { x: 209, y: 96, name: "rocks" },
                    { x: 30, y: 34, name: "popletter" },
                    { x: 178, y: 451, name: "popletter" },
                   
                    { x: 364, y: 287, name: "jumper2" }
                

                    ),
                new Array(
                    { x: 24, y: 35, name: "popletter" },
                    { x: 265, y: 172, name: "choinka" },
                    { x: 434, y: 406, name: "rocks" }

                    ),
                new Array(
                    { x: -72, y: 195, name: "rocks" },
                    { x: 228, y: 72, name: "rocks2" },
                    { x: 385, y: 281, name: "choinka" },
                    { x: 559, y: 233, name: "popletter" },

                    { x: 276, y: 503, name: "popletter" }

                    ),
                new Array(
                    { x: 81, y: 531, name: "balwan" },
                    { x: 374, y: 282, name: "balwan" },
                    { x: 574, y: 68, name: "balwan" },

                    { x: 225, y: 446, name: "popletter" },
                    { x: 473, y: 217, name: "popletter" }

                    ),
                new Array(
                    { x: -82, y: 308, name: "rocks2" },
                    { x: 154, y: 101, name: "rocks2" },
                    { x: 233, y: 384, name: "popletter" },
                    { x: 438, y: 193, name: "popletter" },
                    { x: 509, y: 557, name: "jumper2" },
                    { x: 702, y: 366, name: "jumper2" }

                    ),
                new Array(
                    { x: 65, y: 31, name: "rocks" },
                    { x: 176, y: 331, name: "rocks" },
                    { x: 421, y: 578, name: "rocks" },
                    { x: 672, y: 170, name: "rocks" },
                    { x: 376, y: 130, name: "wood" },
                     { x: 404, y: 360, name: "popletter" }
                    ),
                new Array(
                    { x: 69, y: 56, name: "balwan" },
                    { x: 510, y: 66, name: "rocks" },
                    { x: 50, y: 465, name: "rocks" },
                    { x: 372, y: 660, name: "popletter" },
                    { x: 652, y: 393, name: "popletter" },
                    { x: 263, y: 852, name: "choinka" },
                    { x: 870, y: 270, name: "choinka" },
                    { x: 859, y: 561, name: "wood" }

                    )


                
                );
        

            return arr[this.getRandom(arr.length)];
        }

        level2():Array<any> {


            var arr: Array<any> = new Array(
                new Array(
                    { x: 144, y: 179, name: "walen" },
                    { x: 430, y: 185, name: "walen" },
                    { x: 804, y: 101, name: "popletter" },
                    { x: 1204, y: 101, name: "osmiornica" },
                    { x: 860, y: 96, name: "boatssmedium" }

                    ),
                new Array(
                    { x: 335, y: 90, name: "ball" },
                    { x: 504, y: 96, name: "popletter" },
                    { x: 336, y: 202, name: "piasek" },
                    { x: 765, y: 181, name: "walen" },
                    { x: 1131, y: 96, name: "ball" },
                    { x: 411, y: 96, name: "boatssmedium" },
                    { x: 1015, y: 96, name: "boatssmall" }
                    ),
                new Array(
                    { x: 164, y: 180, name: "walen" },
                    { x: 494, y: 102, name: "osmiornica" },
                    { x: 539, y: 101, name: "piasek" },
                    { x: 733, y: 98, name: "popletter" },
                    { x: 968, y: 180, name: "walen" },
                    { x: 491, y: 96, name: "boatssmall" }
                    ),
                new Array(
                    { x: 236, y: 183, name: "walen" },
                    { x: 547, y: 183, name: "walen" },
                    { x: 939, y: 183, name: "walen" }
                   
                    ),
                new Array(
                    { x: 212, y: 98, name: "ball" },
                    { x: 506, y: 103, name: "osmiornica" },
                    { x: 755, y: 96, name: "popletter" }

                    ),
                new Array(
                    { x: 137, y: 98, name: "ball" },
                    { x: 337, y: 98, name: "ball" },
                    { x: 516, y: 99, name: "popletter" },
                    { x: 922, y: 108, name: "osmiornica" },
                    { x: 1000, y: 199, name: "piasek" },
                    { x: 409, y: 199, name: "boatsmedium" }
                    ),
                new Array(
                    { x: 216, y: 98, name: "ball" },
                    { x: 991, y: 98, name: "popletter" },
                    { x: 460, y: 99, name: "popletter" },
                    { x: 666, y: 180, name: "walen" },
                    { x: 769, y: 199, name: "piasek" }
                    ),
                new Array(
                    { x: 209, y: 98, name: "ball" },
                    { x: 392, y: 98, name: "popletter" },
                    { x: 989, y: 99, name: "popletter" },
                    { x: 676, y: 180, name: "walen" },
                    { x: 330, y: 199, name: "piasek" },
                    { x: 1021, y: 193, name: "piasek" },
                    { x: 1106, y: 199, name: "boatsmedium" },
                    { x: 306, y: 199, name: "boatssmall" }
                    ),
                new Array(
                    { x: 1030, y: 98, name: "ball" },
                    { x: 121, y: 98, name: "popletter" },
                    { x: 459, y: 99, name: "popletter" },
                    { x: 748, y: 102, name: "osmiornica" },
                    { x: 339, y: 199, name: "boatsmedium" }

                    ),
                new Array(
                    { x: 170, y: 98, name: "ball" },
                    { x: 423, y: 98, name: "popletter" },
                    { x: 707, y: 99, name: "popletter" },
                    { x: 475, y: 180, name: "piasek" },
                    { x: 858, y: 199, name: "boatsmedium" },
                    { x: 897, y: 185, name: "walen" }

                    )                         
                );

            return arr[this.getRandom(arr.length)];
        }

        getRandom(max: number): number {

            return Math.floor(Math.random() * max);

        }


        /*  

            
            this.load.image("piasek", "assets/water/obstacle/piasek.png");
            this.load.image("wood", "assets/water/obstacle/wood.png");
            this.load.image("hammer", "assets/water/obstacle/hammer.png");
            this.load.image("turtle", "assets/water/obstacle/turtle.png");
            this.load.image("zielone", "assets/water/obstacle/zielone.png");
            this.load.image("ball", "assets/water/obstacle/wood.png");
            this.load.image("ball", "assets/water/obstacle/wood.png");


            this.load.atlasJSONArray('shark1', 'assets/water/obstacle/shark.png', 'assets/water/obstacle/shark.json');
            this.load.atlasJSONArray('shark2', 'assets/water/obstacle/shark1.png', 'assets/water/obstacle/shark1.json');
            this.load.atlasJSONArray('shark3', 'assets/water/obstacle/shark2.png', 'assets/water/obstacle/shark2.json');
            this.load.atlasJSONArray('shark4', 'assets/water/obstacle/shark3.png', 'assets/water/obstacle/shark3.json');

            this.load.atlasJSONArray('osmiornica', 'assets/water/obstacle/osmiornica.png', 'assets/water/obstacle/osmiornica.json');



        */


    }


}  