﻿module Snow {

    export class GameControler {


        public static _instance: GameControler = null;

        public SPEEDBOAT: number = -150;
        public SPEEDROBOT: number = 100;

        public SPEEDX: number = -150;
        public SPEEDY: number = -100;

        public finishGame: boolean = false;
        private _currenLevel: number = 0;
        private _level: Level;
        public _collisionPlayer: boolean = false;
        public gameOver: boolean = true;
        public maxLevel: number = 3;
        public currentLevel: number = 0;
        public mainSound: Phaser.Sound;
        private _countBadAnswer: number = 0;
        construct() {

            if (GameControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            GameControler._instance = this;

        }


        public get collisionPlayer(): boolean {

            return this._collisionPlayer;

        }

        public set collisionPlayer(isColliding: boolean) {

            this._collisionPlayer = isColliding;

        }


        public static getInstance(): GameControler {

            if (GameControler._instance === null) {

                GameControler._instance = new GameControler();


            }


            return GameControler._instance;



        }

        public restart() {
            this._currenLevel = 0;
            this.level.playAgain();
        }

        public nextLevel() {

            this.currentLevel++;

            this.level.showPrompt(true, true);

        }

        setLevel(level: Level) {

            this._level = level;

        }

        public get level(): Level {

            return this._level;
        }


        public setSound(sound_: Phaser.Sound) {

            this.mainSound = sound_;
        }

        public checkBadAnswer() {

            this._countBadAnswer++;
            if (this._countBadAnswer > 2) {

                this.level.showFinishLevel(false);

                this._countBadAnswer = 0;
                this.level.stopGame(true);
                this.level._isShowEndScrren = true;

            }
            else {
                this.level._isShowEndScrren = false;
                this.level.stopGame(false);
            }

        }
     

    }


}  