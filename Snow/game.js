var Greeter = (function () {
    function Greeter(element) {
        this.element = element;
        this.element.innerHTML += "The time is: ";
        this.span = document.createElement('span');
        this.element.appendChild(this.span);
        this.span.innerText = new Date().toUTCString();
    }
    Greeter.prototype.start = function () {
        var _this = this;
        this.timerToken = setInterval(function () { return _this.span.innerHTML = new Date().toUTCString(); }, 500);
    };
    Greeter.prototype.stop = function () {
        clearTimeout(this.timerToken);
    };
    return Greeter;
})();
window.onload = function () {
    var el = document.getElementById('content');
    var greeter = new Greeter(el);
    greeter.start();
};
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Snow;
(function (Snow) {
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            _super.apply(this, arguments);
        }
        Boot.prototype.preload = function () {
            this.load.image("bg", "assets/SplashScreen.png");
            this.load.image("progressBarBlack", "assets/preloader/ProgressBar.png");
            this.load.image("progressBarBlue", "assets/preloader/ProgressBarBlue.png");
        };
        Boot.prototype.create = function () {
            this.game.state.start("Preloader", true, false);
        };
        return Boot;
    })(Phaser.State);
    Snow.Boot = Boot;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var GameControler = (function () {
        function GameControler() {
            this.SPEEDBOAT = -150;
            this.SPEEDROBOT = 100;
            this.SPEEDX = -150;
            this.SPEEDY = -100;
            this.finishGame = false;
            this._currenLevel = 0;
            this._collisionPlayer = false;
            this.gameOver = true;
            this.maxLevel = 3;
            this.currentLevel = 0;
            this._countBadAnswer = 0;
        }
        GameControler.prototype.construct = function () {
            if (GameControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            GameControler._instance = this;
        };
        Object.defineProperty(GameControler.prototype, "collisionPlayer", {
            get: function () {
                return this._collisionPlayer;
            },
            set: function (isColliding) {
                this._collisionPlayer = isColliding;
            },
            enumerable: true,
            configurable: true
        });
        GameControler.getInstance = function () {
            if (GameControler._instance === null) {
                GameControler._instance = new GameControler();
            }
            return GameControler._instance;
        };
        GameControler.prototype.restart = function () {
            this._currenLevel = 0;
            this.level.playAgain();
        };
        GameControler.prototype.nextLevel = function () {
            this.currentLevel++;
            this.level.showPrompt(true, true);
        };
        GameControler.prototype.setLevel = function (level) {
            this._level = level;
        };
        Object.defineProperty(GameControler.prototype, "level", {
            get: function () {
                return this._level;
            },
            enumerable: true,
            configurable: true
        });
        GameControler.prototype.setSound = function (sound_) {
            this.mainSound = sound_;
        };
        GameControler.prototype.checkBadAnswer = function () {
            this._countBadAnswer++;
            if (this._countBadAnswer > 2) {
                this.level.showFinishLevel(false);
                this._countBadAnswer = 0;
                this.level.stopGame(true);
                this.level._isShowEndScrren = true;
            }
            else {
                this.level._isShowEndScrren = false;
                this.level.stopGame(false);
            }
        };
        GameControler._instance = null;
        return GameControler;
    })();
    Snow.GameControler = GameControler;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var LetterControler = (function () {
        function LetterControler() {
            this._corretAnswer = 0;
            this._maxBadAnswer = 3;
            this._badAnswer = 0;
            this._gameControler = Snow.GameControler.getInstance();
            this._currentSequence = 0;
            this._maxSequence = 2;
            this.correctLeters = new Array();
            this.phonemes = new Array("a", "ai", "ay", "a", "aigh", "al", "au", "aw", "ar", "b", "bb", "bt", "c", "ch", "ck", "ce", "d", "ed", "dd", "ey", "e", "ea", "ee", "ei", "el", "ew", "er", "f", "ff", "ph", "g", "gg", "gh", "ge", "gn", "h", "i", "ie", "il", "ir", "ie", "igh", "j", "k", "kn", "l", "ll", "le", "m", "mm", "mb", "mn", "n", "nn", "o", "oa", "oe", "ow", "ou", "oo", "or", "p", "pp", "ph", "pn", "pt", "qu", "r", "rr", "rh", "re", "s", "ss", "se", "st", "sc", "sh", "s", "se", "t", "tt", "th", "tch", "u", "ue", "ui", "ur", "v", "ve", "w", "wh", "wr", "x", "y", "yr", "z", "zz", "ze");
            /*
            public phonemes: Array<string> = new Array(
                "a", "ai", "ay", "a", "aigh", "al", "au", "aw", "augh", "ar",
                "b", "bb", "bt", "c", "ch", "ck", "ce", "d", "ed", "dd", "dge",
                "ey", "eigh", "e", "ea", "ee", "ei", "el", "ew", "ear", "er",
                "f", "ff", "ph", "g", "gg", "gh", "ge", "gn", "h", "i", "ie", "il", "ir",
                "ie", "igh", "j", "k", "kn", "l", "ll", "le", "m", "mm", "mb", "mn",
                "n", "nn", "o", "ough", "oa", "oe", "ow", "ou", "oo", "or",
                "p", "pp", "ph", "pn", "pt", "qu", "r", "rr", "rh", "re", "s", "ss", "se",
                "st", "sc", "sh", "s", "se", "t", "tt", "th", "tch", "u", "ue", "ui", "ur",
                "v", "ve", "w", "wh", "wr", "x", "y", "yr", "z", "zz", "ze"
                );
            */
            this.phonemeSounds = new Array("a", "a_e", "aw", "b", "c", "ch", "d", "e", "ee", "er", "f", "g", "h", "i", "i_e", "j", "k", "l", "m", "n", "o", "o_e", "oo1", "oo2", "ow", "oy", "p", "qu", "r", "s", "sh", "t", "th", "th2", "u", "u_e", "v", "w", "x", "y", "z");
            this.correctPhonemes = new Array("a", "b", "c", "ch", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s", "sh", "t", "u", "v", "w", "x", "y", "z");
        }
        LetterControler.prototype.construct = function () {
            if (LetterControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            LetterControler._instance = this;
        };
        LetterControler.getInstance = function () {
            if (LetterControler._instance === null) {
                LetterControler._instance = new LetterControler();
            }
            return LetterControler._instance;
        };
        LetterControler.prototype.randomPhoneme = function () {
            var i = Math.floor(Math.random() * this.phonemes.length);
            return this.phonemes[i];
        };
        LetterControler.prototype.addCorrectLetters = function (str) {
            this.correctLeters.push(str);
            if (this.correctLeters.length > 3) {
            }
        };
        LetterControler.prototype.setRandomLetters = function (howmany) {
            if (howmany === void 0) { howmany = 1; }
            howmany = this._gameControler.currentLevel + 1;
            this._currentLetters = new Array();
            console.log("ile liter do zapamietania", howmany, this._gameControler.currentLevel);
            for (var i = 0; i < howmany; i++) {
                var num = this.getRandomNumber(this.phonemes.length - 1);
                console.log("phenomes", num);
                this._currentLetters.push(this.phonemes[num]);
            }
            console.log("current", this._currentLetters);
            this._currentWorld = this._currentLetters[0];
            this._maxLetter = this._currentLetters.length;
        };
        LetterControler.prototype.checkWord = function (str) {
            if (str == this._currentLetters[this._corretAnswer]) {
                console.log("dobra liltera");
                this._corretAnswer++;
                this.checkWinOrDie(false);
            }
            else {
                this._badAnswer++;
                this.checkWinOrDie(true);
                console.log("zla liltera");
            }
        };
        LetterControler.prototype.checkWinOrDie = function (die) {
            console.log("die", this._badAnswer, "goog", this._corretAnswer);
            if (this._badAnswer == this._maxBadAnswer) {
                console.log("die you");
                this._gameControler.level.showPrompt(false, true);
                this._corretAnswer = 0;
                this._badAnswer = 0;
                return;
            }
            else if (this._corretAnswer == this._maxLetter) {
                this._currentSequence++;
                this._badAnswer = 0;
                if (this._currentSequence != this._maxSequence) {
                    this._corretAnswer = 0;
                    this.setRandomLetters(this._gameControler.currentLevel + 1);
                    this._gameControler.level.showPrompt(true);
                    this._gameControler.level.sequenceLevel(this._currentSequence + 1 + "/" + 2);
                }
                else {
                    this._currentSequence = 0;
                    this._gameControler.nextLevel();
                    this.setRandomLetters();
                    console.log("you win Level");
                }
                return;
            }
            if (die) {
                this._corretAnswer = 0;
                this._gameControler.level.showPrompt(false);
                this._gameControler.level.removeSequence();
            }
            else {
            }
        };
        LetterControler.prototype.getRandomNumber = function (max, min) {
            if (min === void 0) { min = 0; }
            //--max;
            return Math.floor(Math.random() * (max - min) + min);
        };
        LetterControler.prototype.isCorrectLetter = function (str) {
            for (var i = 0; i < this.correctPhonemes.length; i++) {
                if (str == this.correctPhonemes[i])
                    return true;
            }
            return false;
        };
        LetterControler.prototype.getPopLetter = function () {
            var randNum = this.getRandomNumber(2);
            console.log("random number letter", randNum);
            var str;
            if (randNum == 1) {
                str = this.phonemes[this.getRandomNumber(this.phonemes.length)];
            }
            else {
                var num = this.getRandomNumber(this._currentLetters.length);
                str = this._currentLetters[num];
            }
            var letter = this.isCorrectLetter(str);
            var arr = new Array({ str: str, correct: letter });
            return arr;
        };
        Object.defineProperty(LetterControler.prototype, "currentLetters", {
            get: function () {
                return this._currentLetters;
            },
            enumerable: true,
            configurable: true
        });
        LetterControler._instance = null;
        LetterControler.words = new Array({
            // level 1 english words
            english: new Array({ word: ["ai", "r"], sounds: ["a_e", "r"] }, { word: ["a", "l", "e"], sounds: ["a_e", "l", "split"] }, { word: ["a", "m"], sounds: ["a", "m"] }, { word: ["a", "n"], sounds: ["a", "n"] }, { word: ["a", "n", "d"], sounds: ["a", "n", "d"] }, { word: ["a", "n", "t"], sounds: ["a", "n", "t"] }, { word: ["a", "r", "m"], sounds: ["o", "r", "m"] }, { word: ["a", "r", "t"], sounds: ["o", "r", "t"] }, { word: ["a", "s"], sounds: ["a", "z"] }, { word: ["a", "sh"], sounds: ["a", "sh"] }, { word: ["a", "s", "k"], sounds: ["a", "s", "k"] }, { word: ["a", "t"], sounds: ["a", "t"] }, { word: ["b", "a", "d"], sounds: ["b", "a", "d"] }, { word: ["b", "a", "g"], sounds: ["b", "a", "g"] }, { word: ["b", "a", "r"], sounds: ["b", "o", "r"] })
        }, {
            // level 2 english words
            english: new Array({ word: ["a", "b", "j", "e", "c", "t"], sounds: ["a", "b", "j", "e", "k", "t"] }, { word: ["a", "b", "le"], sounds: ["a_e", "b", "l"] }, { word: ["a", "b", "oa", "r", "d"], sounds: ["u", "b", "o_e", "r", "d"] }, { word: ["a", "b", "o", "r", "t"], sounds: ["u", "b", "o_e", "r", "t"] }, { word: ["a", "b", "ou", "n", "d"], sounds: ["u", "b", "ow", "n", "d"] }, { word: ["a", "b", "o", "v", "e"], sounds: ["u", "b", "u", "v", "split"] }, { word: ["a", "b", "r", "ea", "s", "t"], sounds: ["u", "b", "r", "e", "s", "t"] }, { word: ["a", "b", "s", "e", "n", "ce"], sounds: ["a", "b", "s", "e", "n", "s"] }, { word: ["a", "b", "s", "o", "l", "ve"], sounds: ["u", "b", "z", "o", "l", "v"] }, {
                word: ["a", "b", "s", "t", "r", "a", "c", "t"],
                sounds: ["a", "b", "s", "t", "r", "a", "k", "t"]
            }, { word: ["a", "b", "s", "ur", "d"], sounds: ["u", "b", "s", "er", "d"] }, { word: ["a", "b", "u", "se"], sounds: ["u", "b", "u_e", "s"] }, { word: ["a", "b", "y", "ss"], sounds: ["u", "b", "i", "s"] }, { word: ["a", "c", "c", "e", "p", "t"], sounds: ["a", "c", "s", "e", "p", "t"] }, { word: ["a", "c", "c", "l", "ai", "m"], sounds: ["u", "c", "c", "l", "a_e", "m"] }, { word: ["a", "ce"], sounds: ["a_e", "s"] })
        }, {
            // level 3 english words
            english: new Array({
                word: ["a", "b", "d", "o", "m", "i", "n", "al"],
                sounds: ["u", "b", "d", "o", "m", "i", "n", "l"]
            }, { word: ["a", "b", "l", "y"], sounds: ["a_e", "b", "l", "ee"] }, {
                word: ["a", "b", "o", "l", "i", "sh", "ed"],
                sounds: ["u", "b", "o", "l", "i", "sh", "d"]
            }, {
                word: ["a", "b", "ou", "t"],
                sounds: ["a", "b", "ow", "t"]
            }, {
                word: ["a", "b", "r", "a", "s", "i", "ve"],
                sounds: ["u", "b", "r", "a_e", "s", "i", "v"]
            }, { word: ["a", "b", "s", "e", "n", "t"], sounds: ["a", "b", "s", "e", "n", "t"] }, {
                word: ["a", "b", "u", "n", "d", "a", "n", "ce"],
                sounds: ["u", "b", "u", "n", "d", "u", "n", "s"]
            }, {
                word: ["a", "b", "u", "n", "d", "a", "n", "t"],
                sounds: ["u", "b", "u", "n", "d", "u", "n", "t"]
            }, {
                word: ["a", "b", "u", "s", "i", "ve"],
                sounds: ["u", "b", "u_e", "s", "i", "v"]
            }, {
                word: ["a", "c", "qu", "i", "tt", "ed"],
                sounds: ["u", "c", "qu", "i", "t", "d"]
            }, {
                word: ["a", "c", "r", "o", "n", "y", "m"],
                sounds: ["a", "c", "r", "o", "n", "i", "m"]
            }, { word: ["a", "c", "r", "o", "ss"], sounds: ["o", "c", "r", "o", "s"] }, {
                word: ["a", "c", "r", "y", "l", "i", "c"],
                sounds: ["u", "c", "r", "i", "l", "i", "c"]
            }, {
                word: ["a", "c", "t", "i", "v", "i", "s", "m"],
                sounds: ["a", "c", "t", "i", "v", "i", "z", "m"]
            })
        });
        return LetterControler;
    })();
    Snow.LetterControler = LetterControler;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var ObstacleControler = (function () {
        function ObstacleControler() {
        }
        ObstacleControler.prototype.construct = function () {
            if (ObstacleControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            ObstacleControler._instance = this;
        };
        ObstacleControler.getInstance = function () {
            if (ObstacleControler._instance === null) {
                ObstacleControler._instance = new ObstacleControler();
            }
            return ObstacleControler._instance;
        };
        ObstacleControler.prototype.level1 = function () {
            var arr = new Array(new Array({ x: 192, y: 21, name: "popletter" }, { x: 27, y: 183, name: "popletter" }), new Array({ x: 31, y: 33, name: "popletter" }, { x: 311, y: 293, name: "popletter" }), new Array({ x: 63, y: 32, name: "rocks" }, { x: 346, y: 370, name: "popletter" }, { x: 459, y: 265, name: "popletter" }, { x: 536, y: 423, name: "wood" }), new Array({ x: 209, y: 96, name: "rocks" }, { x: 30, y: 34, name: "popletter" }, { x: 178, y: 451, name: "popletter" }, { x: 364, y: 287, name: "jumper2" }), new Array({ x: 24, y: 35, name: "popletter" }, { x: 265, y: 172, name: "choinka" }, { x: 434, y: 406, name: "rocks" }), new Array({ x: -72, y: 195, name: "rocks" }, { x: 228, y: 72, name: "rocks2" }, { x: 385, y: 281, name: "choinka" }, { x: 559, y: 233, name: "popletter" }, { x: 276, y: 503, name: "popletter" }), new Array({ x: 81, y: 531, name: "balwan" }, { x: 374, y: 282, name: "balwan" }, { x: 574, y: 68, name: "balwan" }, { x: 225, y: 446, name: "popletter" }, { x: 473, y: 217, name: "popletter" }), new Array({ x: -82, y: 308, name: "rocks2" }, { x: 154, y: 101, name: "rocks2" }, { x: 233, y: 384, name: "popletter" }, { x: 438, y: 193, name: "popletter" }, { x: 509, y: 557, name: "jumper2" }, { x: 702, y: 366, name: "jumper2" }), new Array({ x: 65, y: 31, name: "rocks" }, { x: 176, y: 331, name: "rocks" }, { x: 421, y: 578, name: "rocks" }, { x: 672, y: 170, name: "rocks" }, { x: 376, y: 130, name: "wood" }, { x: 404, y: 360, name: "popletter" }), new Array({ x: 69, y: 56, name: "balwan" }, { x: 510, y: 66, name: "rocks" }, { x: 50, y: 465, name: "rocks" }, { x: 372, y: 660, name: "popletter" }, { x: 652, y: 393, name: "popletter" }, { x: 263, y: 852, name: "choinka" }, { x: 870, y: 270, name: "choinka" }, { x: 859, y: 561, name: "wood" }));
            return arr[this.getRandom(arr.length)];
        };
        ObstacleControler.prototype.level2 = function () {
            var arr = new Array(new Array({ x: 144, y: 179, name: "walen" }, { x: 430, y: 185, name: "walen" }, { x: 804, y: 101, name: "popletter" }, { x: 1204, y: 101, name: "osmiornica" }, { x: 860, y: 96, name: "boatssmedium" }), new Array({ x: 335, y: 90, name: "ball" }, { x: 504, y: 96, name: "popletter" }, { x: 336, y: 202, name: "piasek" }, { x: 765, y: 181, name: "walen" }, { x: 1131, y: 96, name: "ball" }, { x: 411, y: 96, name: "boatssmedium" }, { x: 1015, y: 96, name: "boatssmall" }), new Array({ x: 164, y: 180, name: "walen" }, { x: 494, y: 102, name: "osmiornica" }, { x: 539, y: 101, name: "piasek" }, { x: 733, y: 98, name: "popletter" }, { x: 968, y: 180, name: "walen" }, { x: 491, y: 96, name: "boatssmall" }), new Array({ x: 236, y: 183, name: "walen" }, { x: 547, y: 183, name: "walen" }, { x: 939, y: 183, name: "walen" }), new Array({ x: 212, y: 98, name: "ball" }, { x: 506, y: 103, name: "osmiornica" }, { x: 755, y: 96, name: "popletter" }), new Array({ x: 137, y: 98, name: "ball" }, { x: 337, y: 98, name: "ball" }, { x: 516, y: 99, name: "popletter" }, { x: 922, y: 108, name: "osmiornica" }, { x: 1000, y: 199, name: "piasek" }, { x: 409, y: 199, name: "boatsmedium" }), new Array({ x: 216, y: 98, name: "ball" }, { x: 991, y: 98, name: "popletter" }, { x: 460, y: 99, name: "popletter" }, { x: 666, y: 180, name: "walen" }, { x: 769, y: 199, name: "piasek" }), new Array({ x: 209, y: 98, name: "ball" }, { x: 392, y: 98, name: "popletter" }, { x: 989, y: 99, name: "popletter" }, { x: 676, y: 180, name: "walen" }, { x: 330, y: 199, name: "piasek" }, { x: 1021, y: 193, name: "piasek" }, { x: 1106, y: 199, name: "boatsmedium" }, { x: 306, y: 199, name: "boatssmall" }), new Array({ x: 1030, y: 98, name: "ball" }, { x: 121, y: 98, name: "popletter" }, { x: 459, y: 99, name: "popletter" }, { x: 748, y: 102, name: "osmiornica" }, { x: 339, y: 199, name: "boatsmedium" }), new Array({ x: 170, y: 98, name: "ball" }, { x: 423, y: 98, name: "popletter" }, { x: 707, y: 99, name: "popletter" }, { x: 475, y: 180, name: "piasek" }, { x: 858, y: 199, name: "boatsmedium" }, { x: 897, y: 185, name: "walen" }));
            return arr[this.getRandom(arr.length)];
        };
        ObstacleControler.prototype.getRandom = function (max) {
            return Math.floor(Math.random() * max);
        };
        ObstacleControler._instance = null;
        return ObstacleControler;
    })();
    Snow.ObstacleControler = ObstacleControler;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var Instruction = (function (_super) {
        __extends(Instruction, _super);
        function Instruction() {
            _super.apply(this, arguments);
        }
        Instruction.prototype.create = function () {
            var instruction = this.add.image(0, 0, "firstInstruction", 0);
            instruction.width = this.game.world.width; //this.stage.width;
            instruction.height = this.game.world.height; //this.stage.height;
            this._mainMenuBg = this.game.add.audio('instructionSn', 1, true);
            this._mainMenuBg.play("", 0, 1, false);
            var gameControler = Snow.GameControler.getInstance();
            gameControler.mainSound.volume = 0.05;
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            // var instructionAudio = this.game.add.audio("chuja", 1, false);
            //  instructionAudio.play();
            var _buttons = this.add.group();
            var gameButton = this.add.button(0, 0, "games", this.onGameClick, this, 1, 0);
            //gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            _buttons.addChild(gameButton);
            var playButton = this.add.button(100, 0, "playGame", this.onPlayClick, this, 1, 0);
            playButton.onInputDown.add(this.overPlay2, this);
            _buttons.addChild(playButton);
            _buttons.x = 0;
            _buttons.y = this.game.stage.height - _buttons.height;
        };
        Instruction.prototype.onGameClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        Instruction.prototype.overPlay2 = function () {
            this.overButtonA.play();
        };
        Instruction.prototype.onPlayClick = function () {
            this._mainMenuBg.stop();
            this.game.state.start("Level", true, false);
        };
        return Instruction;
    })(Phaser.State);
    Snow.Instruction = Instruction;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var Level = (function (_super) {
        __extends(Level, _super);
        function Level() {
            _super.apply(this, arguments);
            this._timer = 0;
            this.jumpPressed = false;
            this._isShowEndScrren = false;
            this._speedPlayer = 5;
        }
        Level.prototype.create = function () {
            this._gameControler = Snow.GameControler.getInstance();
            this._gameControler.setLevel(this);
            this._gameControler.currentLevel = userLevel;
            this._letterControler = Snow.LetterControler.getInstance();
            this._letterControler.setRandomLetters();
            this._blokadaGr = this.add.group();
            this._bg = this.game.add.group();
            this.game.add.image(0, 0, "bgGame", 0, this._bg);
            this._obstcaleGroup = new Snow.ObstacleView(this.game);
            this._obstcaleGroup.getObstacle();
            this._playerPlay = new Snow.PlayerPlay(this.game);
            this.stage.addChild(this._playerPlay);
            this.stage.addChild(this._obstcaleGroup);
            /*
            to jest gadajacy koles.
            playIntro() - mowi intro
            playFeedbackGood() - mowi jak wykonamy poprawnie sekwencje
            playFeedbackWrong() - mowi jak zrobimy blad
            */
            this._gameGr = this.add.group();
            this._sequenceGr = this.add.group();
            this._sequence = new Snow.Sequence(this.game, 15, 30, "sequence");
            this._sequenceGr.add(this._sequence);
            this.stage.addChild(this._sequenceGr);
            this.stage.addChild(this._blokadaGr);
            this._prompt = new Snow.PromptView(this.game, this);
            this._prompt.position.setTo(this.game.width - this._prompt.width, this.game.height - this._prompt.height);
            this._prompt.playIntro();
            /*
            to jest zegar
            startTimer() start timera
            stopTimer() stop timera
            setSequenceText(text) dodajemy text w sequence
            setCreditsText(text) dodajemy text w credits. $(dolar) jest dodawany w klasie
            */
            this._clock = new Snow.ClockView(this.game, this);
            this._clock.position.setTo(this.game.width - this._clock.width, 0);
            this.stage.addChild(this._clock);
            this._clock.startTimer();
            this._clock.setSequenceText("1/2");
            this._clock.setCreditText("0");
            this.stopGame(true);
            this._gameControler.level.stopGame(false);
            this._gameGr.add(this._obstcaleGroup);
            this._gameGr.add(this._playerPlay);
            //      this._showLetterGr = this.game.add.group();
            // this.stage.addChild(this._showLetterGr);
            //     this._special = this.add.sprite(0, 0, "special");
            //     this._special.y = this.game.height - this._special.height  - 30;
            //    this._special.x = 550;
            // this._special.animations.add("play");
            //  this._jumpBtn = this.add.button(0, 0, "jumpbtn", this.jummBtnDown, this, 1, 0, 1, 0);
            //  this._jumpBtn.y = this._special.y + 15;
            //  this._jumpBtn.x = 630;
            ///  this.stage.addChild(this._special);
            //   this.stage.addChild(this._jumpBtn);
            //   this._jumpBtn.onInputDown.add(this.jummBtnDown, this);
            //   this._jumpBtn.onInputUp.add(this.jummBtnUp, this);
            /*     this.game.input.keyboard.onUpCallback = function (e) {
                     if (e.keyCode == Phaser.Keyboard.UP) {
                         this.jummBtnUp();
                     }
                 };*/
            if (!mobile) {
                this.cursors = this.game.input.keyboard.createCursorKeys();
                this.game.input.keyboard.addKeyCapture([
                    Phaser.Keyboard.LEFT,
                    Phaser.Keyboard.RIGHT,
                    Phaser.Keyboard.UP,
                    Phaser.Keyboard.DOWN,
                    Phaser.Keyboard.SPACEBAR
                ]);
            }
            this.helpGr = this.add.group();
            this.stage.addChild(this.helpGr);
            this.stage.addChild(this._prompt);
            this.stage.addChild(this._sequence);
            this._helpBtn = this.add.button(0, 0, "helpBtn", this.helpBtnClick, this, 1, 0, 0, 0, this.helpGr);
            this.helpGr.x = 0; //800 - this.helpGr.width;
            this.helpGr.y = 600 - this.helpGr.height;
            this._helpBtn.buttonMode = true;
            this._helpBtn.inputEnabled = true;
            this._helpBtn.input.priorityID = 110;
            //this.showLetterMenu(new Array("a"));
            this.game.input.onDown.add(function () {
                if (this.game.paused) {
                    this.game.paused = false;
                }
                if (this._instructionShow) {
                    this.stage.removeChild(this._instructionShow);
                    this._instructionShow.kill();
                    this._instructionShow = null;
                }
            }, this);
        };
        Level.prototype.helpBtnClick = function () {
            console.log("pokaz instrukcje");
            this.game.paused = !this.game.paused;
            if (this._instructionShow == null)
                this._instructionShow = this.add.image(0, 0, "firstInstruction", 0);
            // this.add.image(0, 0, "firstInstruction", 0);
            this._instructionShow.width = this.game.world.width; //this.stage.width;
            this._instructionShow.height = this.game.world.height;
            this.stage.addChild(this._instructionShow);
        };
        Level.prototype.youwinSequence = function () {
            //  this.stopGame(false);
            this._clock.setSequenceText("1/2");
        };
        Level.prototype.youLose = function () {
            //   console.log("przegrales chuju:");
            this._gameControler.checkBadAnswer();
        };
        Level.prototype.showLetterMenu = function (_collectLetters) {
            this.stopGame(true);
            this.letters = new Snow.Letters(this.game, 0, 0, "", _collectLetters, this);
            console.log("jaka szerokosc", this.game.world.width / 2, this.letters.widthScreen / 2);
            this.letters.x = this.game.world.width / 2 - this.letters.widthScreen / 2;
            this.letters.y = this.game.world.height / 2 - this.letters.heightScreen / 2;
            this.stage.addChild(this.letters);
        };
        Level.prototype.addCredits = function () {
            this._clock.addCredits();
        };
        Level.prototype.addPlayerToTop = function (top) {
            if (top) {
                this._gameGr.addChild(this._obstcaleGroup);
                this._gameGr.addChild(this._playerPlay);
            }
            else {
                this._gameGr.addChild(this._playerPlay);
                this._gameGr.addChild(this._obstcaleGroup);
            }
            this.stage.addChild(this._prompt);
        };
        Level.prototype.blokada = function (show) {
            if (show === void 0) { show = true; }
            /*     if (this._blokada == null) {
                     this._blokada = new Phaser.Graphics(this.game, 0, 0);
                     this._blokada.beginFill(0x000000, 0.6);
                     this._blokada.drawRect(0, 0, this.game.width, this.game.height);
                     this._blokada.endFill();
                     this._blokadaGr.add(this._blokada);
                     this._blokada.visible = false;
                 }
                
                 this._blokada.visible = show;*/
        };
        Level.prototype.playAgain = function (win) {
            if (win === void 0) { win = false; }
            this._isShowEndScrren = false;
            this._obstcaleGroup.delObstacle();
            this._obstcaleGroup.removeObstacle();
            this._obstcaleGroup.getObstacle();
            this.stopGame(true);
            this._clock.setCreditText("0");
            if (win) {
                //    this._clock.setSequenceText("1/2");
                this.stopGame(false);
            }
            else {
                this._prompt.playFeedbackWrong();
            }
        };
        /*
        Ta funkcja jest odpalana jak koles skonczy audio
        */
        Level.prototype.onPromptAudioComplete = function () {
            console.log("Koles skonczyl gadac");
            this.removeSequence();
            // this._gameControler.collisionPlayer = false;
            // this._gameControler.level.stopGame(false);
            // this._showLetter = new ShowLetter(this.game, 0, 0, "bgletter");
            // this._showLetterGr.add(this._showLetter);
            // this._showLetter.addBgToDisplayLetter();
            this.stopGame(false);
        };
        Level.prototype.sequenceLevel = function (str) {
            this._clock.setSequenceText(str);
        };
        Level.prototype.showFinishLevel = function (win) {
            if (win === void 0) { win = false; }
            console.log("FInish Level:");
            this.stopGame(true);
            this._isShowEndScrren = true;
            this._finishScreenLevel = new Snow.FinishLevel(this.game);
            if (win)
                this._finishScreenLevel.youwin();
            else
                this._finishScreenLevel.youLose();
            this._finishScreenLevel.x = 0; //this.game.width / 2 - this._finishScreenLevel.width / 2;
            this._finishScreenLevel.y = 0; //this.game.height / 2 - this._finishScreenLevel.height / 2;
            this.stage.addChild(this._finishScreenLevel);
            this.stopGame(true);
        };
        Level.prototype.showFinish = function () {
            this._finishScreen = new Snow.FinishScreen(this.game);
            this._finishScreen.x = this.game.width / 2 - this._finishScreen.width / 2;
            this._finishScreen.y = this.game.height / 2 - this._finishScreen.height / 2;
            this.stage.addChild(this._finishScreen);
            this.stopGame(true);
        };
        Level.prototype.showPrompt = function (correct, end) {
            if (end === void 0) { end = false; }
            if (correct) {
                this.stopGame(true);
                this._obstcaleGroup.removeObstacleLetters();
                this._prompt.playFeedbackGood(end);
            }
            else {
                this.stopGame(true);
                this._gameControler.collisionPlayer = true;
                this._prompt.playFeedbackWrong(end);
            }
        };
        Level.prototype.removeSequence = function () {
            this._sequence.removeLetters();
        };
        Level.prototype.stopGame = function (stop, changePos) {
            if (changePos === void 0) { changePos = false; }
            if (!this._isShowEndScrren) {
                this._obstcaleGroup.changeSpeed(stop, changePos);
                this._playerPlay._stop = stop;
            }
        };
        Level.prototype.update = function () {
            if (!this._gameControler.collisionPlayer) {
                //  console.log("position player", this._playerPlay.toGlobal(this._playerPlay.player.position));
                if (!mobile) {
                    if (this.cursors.right.isDown) {
                        this._playerPlay.getPlayer().x += this._speedPlayer;
                        this._playerPlay.getPlayer().right();
                    }
                    else if (this.cursors.left.isDown) {
                        this._playerPlay.getPlayer().x -= this._speedPlayer;
                        this._playerPlay.getPlayer().left();
                    }
                    if (this.cursors.up.isDown) {
                        this._playerPlay.getPlayer().y -= this._speedPlayer;
                    }
                    else if (this.cursors.down.isDown) {
                        this._playerPlay.getPlayer().y += this._speedPlayer;
                    }
                }
                this.game.physics.arcade.overlap(this._playerPlay, this._obstcaleGroup, this.collision, null, this);
            }
        };
        Level.prototype.collision = function (object, object2) {
            console.log("kolizja", object2.name, this._gameControler.collisionPlayer);
            if (!this._gameControler.collisionPlayer) {
                if (object2.name == "popletter") {
                    object2.body.setSize(-1, -1, -1200, -1600);
                    // this._sequence.addLetter(object2.letter);
                    //  this._letterControler.checkWord(object2.letter);
                    object2.collisonPlayAnim();
                }
                else if (object2.name == "correctLetter") {
                    object2.body.setSize(-1, -1, -1200, -1600);
                    // object2.name = "die";
                    this._sequence.addLetter(object2.letter);
                    // this._letterControler.checkWord(object2.letter);
                    this._obstcaleGroup.changeLetterToCoin();
                    this._letterControler.addCorrectLetters(object2.letter);
                    object2.collisonPlayAnim();
                }
                else if (object2.name == "coin") {
                    object2.body.setSize(-1, -1, -1200, -1600);
                    //   this._sequence.addLetter(object2.letter);
                    //  this._letterControler.checkWord(object2.letter);
                    object2.coinAd();
                    this.addCredits();
                    object2.kill();
                }
                else if (object2.name == "jumper2") {
                    console.log("chujjjjaaja", this._obstcaleGroup.toGlobal(object2.position));
                    var pos = this._obstcaleGroup.toGlobal(object2.position);
                    this._playerPlay.changePos(new Phaser.Point(pos.x + 80, pos.y + 20));
                    //  this._playerPlay.y = 0;object2.game.world.y + 30;
                    //  this._playerPlay.stopTween();
                    this.addPlayerToTop(true);
                    this._gameControler.collisionPlayer = true;
                    this._playerPlay.player.jump();
                }
                else {
                    console.log("kolizja z ", object2.name);
                    this._gameControler.collisionPlayer = true;
                    this._playerPlay.player.crashForward();
                }
                this._timer = 0;
            }
        };
        Level.prototype.render = function () {
            /*
              this.game.debug.quadTree(this.game.physics.arcade.quadTree);
        
              this._obstcaleGroup.forEach(function (item) {
                  // item.body.velocity = 0;
               
                  this.game.debug.body(item);
              }, this)
 
              this._playerPlay.forEach(function (item) {
                  // item.body.velocity = 0;
               
                  this.game.debug.body(item);
              }, this)
             */
        };
        return Level;
    })(Phaser.State);
    Snow.Level = Level;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            console.log("start");
            _super.call(this, 800, 600, Phaser.AUTO, "content", null, true);
            this.state.add("Boot", Snow.Boot, false);
            this.state.add("Preloader", Snow.Preloader, false);
            this.state.add("MainMenu", Snow.MainMenu, false);
            this.state.add("PopupStart", Snow.PopupStart, false);
            this.state.add("Instruction", Snow.Instruction, false);
            this.state.add("Level", Snow.Level, false);
            this.state.start("Boot");
        }
        return Game;
    })(Phaser.Game);
    Snow.Game = Game;
})(Snow || (Snow = {}));
window.onload = function () {
    var game = new Snow.Game;
};
var userLevel = 1;
var Snow;
(function (Snow) {
    var DropLetter = (function (_super) {
        __extends(DropLetter, _super);
        function DropLetter(game, parent, numerOfSlots) {
            _super.call(this, game, parent, "drop", true, true);
            this.widthLetter = 0;
            this._parent = parent;
            this.bgDrop = new Phaser.Sprite(game, 0, 0, "bgDrop");
            this.addChild(this.bgDrop);
            this._arr = new Array();
            var posx = numerOfSlots * 50;
            posx = 400 - posx / 2;
            for (var i = 0; i < numerOfSlots; i++) {
                var big = (this._parent._arrAnswear[i].length > 2) ? true : false;
                var let1 = new Snow.PlaceToDrop(game, big);
                this._arr.push(let1);
                let1.x = posx + i * 50;
                let1.y = 10;
                this.add(let1);
            }
            var let1 = new Snow.PlaceToDrop(game);
            //  this.add(let1);
            let1.x = 350;
            let1.y = 10;
            var let2 = new Snow.PlaceToDrop(game);
            let2.x = let1.x + let1.width + 15;
            let2.y = let1.y;
            // this.add(let2);
            var let3 = new Snow.PlaceToDrop(game);
            let3.x = let2.x + let2.width + 15;
            // this.add(let3);
            var let4 = new Snow.PlaceToDrop(game);
            let4.x = let3.x + let3.width + 15;
            // this.add(let4);
            //  this._arr = new Array(let1);
        }
        DropLetter.prototype.clear = function () {
            for (var i = 0; i < this._arr.length; i++) {
                this._arr[i].clear();
            }
        };
        DropLetter.prototype.checkLetter = function () {
            var licz = 0;
            for (var i = 0; i < this._arr.length; i++) {
                if (this._arr[i].isAdded) {
                    //console.log( this._arr[i]._letter.letterString());
                    console.log(this._arr[i]._letter.letterString(), this._parent._arrAnswear[i]);
                    if (this._arr[i]._letter.letterString() == this._parent._arrAnswear[i]) {
                        licz++;
                    }
                    else {
                    }
                }
            }
            if (licz == this._parent._arrAnswear.length) {
                this._parent.youWin();
                console.log("you win");
            }
            else {
                console.log("you lose");
                this._parent.youLose();
            }
        };
        return DropLetter;
    })(Phaser.Group);
    Snow.DropLetter = DropLetter;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var Letter = (function (_super) {
        __extends(Letter, _super);
        function Letter(game, letter, bgColor) {
            if (bgColor === void 0) { bgColor = 1; }
            _super.call(this, game, 0, 0, "");
            this.widthLetter = 0;
            this.name = letter;
            var bg;
            if (bgColor == 1) {
                bg = new Phaser.Sprite(this.game, 0, 0, "szare");
            }
            else if (bgColor == 2) {
                bg = new Phaser.Sprite(this.game, 0, 0, "green");
            }
            else if (bgColor == 3) {
                bg = new Phaser.Sprite(this.game, 0, 0, "orange");
            }
            this._letter = new Phaser.Sprite(game, 0, 0, "");
            this._letter.addChild(bg);
            this.addChild(this._letter);
            var color = "#00FFFF";
            var style = { font: "40px Arial", fill: color, align: "left" };
            console.log("log", letter);
            var _letterShow = new Phaser.Text(this.game, 0, 0, letter, style);
            console.log(_letterShow.width, bg.width);
            while (_letterShow.width > bg.width - 5 && _letterShow.fontSize > 0) {
                _letterShow.fontSize--;
            }
            while (_letterShow.height > bg.height - 5 && _letterShow.fontSize > 0) {
                _letterShow.fontSize--;
            }
            _letterShow.x = bg.width / 2 - _letterShow.width / 2 - 2;
            _letterShow.y = bg.height / 2 - _letterShow.height / 2;
            this._letter.addChild(_letterShow);
            this.widthLetter = bg.width;
        }
        return Letter;
    })(Phaser.Sprite);
    Snow.Letter = Letter;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var LetterWord = (function (_super) {
        __extends(LetterWord, _super);
        function LetterWord(game, letter) {
            _super.call(this, game, 0, 0, "");
            this.isAdded = false;
            this.widthLetter = 0;
            this.name = letter;
            var bg = new Phaser.Sprite(this.game, 0, 0, "szare");
            if (letter.length > 2) {
                bg = new Phaser.Sprite(this.game, 0, 0, "szared");
            }
            if (letter.length > 3) {
                bg = new Phaser.Sprite(this.game, 0, 0, "szared2");
            }
            this._letter = new Phaser.Sprite(game, 0, 0, "");
            this._letter.addChild(bg);
            this.addChild(this._letter);
            var color = "#000000";
            var style = { font: "25px Arial", fill: color, align: "left" };
            console.log("log", bg.width);
            var _letterShow = new Phaser.Text(this.game, 0, 0, letter.toUpperCase(), style);
            //    _letterShow.x = this._letter.width / 2 - _letterShow.width / 2;
            //    _letterShow.y = this._letter.height / 2 - _letterShow.height / 2;
            this._letterStr = letter;
            while (_letterShow.width > bg.width - 5 && _letterShow.fontSize > 0) {
                _letterShow.fontSize--;
            }
            while (_letterShow.height > bg.height - 5 && _letterShow.fontSize > 0) {
                _letterShow.fontSize--;
            }
            _letterShow.x = bg.width / 2 - _letterShow.width / 2 - 2;
            _letterShow.y = bg.height / 2 - _letterShow.height / 2 + 2;
            this._letter.addChild(_letterShow);
            this.inputEnabled = true;
            //  this.input.enableDrag();
            this.widthLetter = bg.width;
        }
        LetterWord.prototype.addObjectToDrag = function () {
            var spr = new LetterWord(this.game, this._letterStr);
            spr.anchor.setTo(0.5, 0.5);
            return spr;
        };
        LetterWord.prototype.letterString = function () {
            return this._letterStr;
        };
        return LetterWord;
    })(Phaser.Sprite);
    Snow.LetterWord = LetterWord;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var Letters = (function (_super) {
        __extends(Letters, _super);
        function Letters(game, x, y, key, lettersArr, parent_) {
            _super.call(this, game, 0, 0, "");
            this.alphabet = new Array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'che', 'er', 'oo', 'ow', 'oy', 'qu', 'sh', 'th', 'aw', 'th');
            this.words = [
                { a: new Array({ a: "s<a>t" }) },
                { b: [{ b: "<b>ust" }, { bb: "bu<bb>le" }] },
                { c: [{ c: "<c>at" }, { k: "<k>ink" }, { ck: "lu<ck>" }, { ch: "<ch>rome" }] },
                { d: [{ d: "<d>id" }, { dd: "la<dd>er" }, { ed: "wast<ed>" }] },
                { e: [{ e: "t<e>n" }, { ea: "h<ea>d" }, { ai: "s<ai>d" }, { ie: "fr<ie>nd" }] },
                { f: [{ f: "<f>an" }, { ff: "flu<ff>" }, { ph: "<ph>one" }, { gh: "rou<gh>" }] },
                { g: [{ g: "ba<g>" }, { gg: "gi<gg>le" }, { ph: "<gh>ost" }] },
                { h: [{ h: "<h>am" }, { wh: "<wh>o" }] },
                { i: [{ i: "<i>t" }, { y: "cr<y>pt" }] },
                { j: [{ j: "<j>am" }, { g: "<g>em" }, { ge: "lar<ge>" }, { dge: "bri<dge>" }] },
                { k: [{ k: "<k>itty" }, { c: "<c>ut" }, { ck: "lu<ck>" }, { ch: "<ch>ristmas" }] },
                { l: [{ l: "<l>it" }, { ll: "pi<ll>" }, { le: "tab<le>" }, { el: "lab<el>" }, { il: "pup<il>" }, { al: "sab<al>" }] },
                { m: [{ m: "<m>utt" }, { mm: "su<mm>er" }, { mb: "du<mb>" }, { mn: "autu<mn>" }] },
                { n: [{ n: "<n>ot" }, { nn: "wi<nn>er" }, { kn: "<kn>ot" }, { gn: "<gn>aw" }, { pn: "<pn>eumonia" }] },
                { o: [{ o: "p<o>t" }, { a: "f<a>ther" }] },
                { p: [{ p: "po<p>" }, { pp: "pa<pp>y" }] },
                { r: [{ r: "<r>un" }, { wr: "<wr>ote" }, { rr: "ma<rr>y" }, { rh: "<rh>ino" }, { re: "the<re>" }] },
                { s: [{ s: "<s>at" }, { se: "mou<se>" }, { ss: "bra<ss>" }, { c: "<c>ider" }, { ce: "choi<ce>" }, { st: "whi<st>le" }, { sc: "<sc>ience" }] },
                { t: [{ t: "<t>an" }, { tt: "mi<tt>" }, { bt: "dou<bt>" }, { pt: "<pt>eradactyle" }] },
                { u: [{ u: "r<u>b" }, { ou: "t<ou>gh" }, { a: "<a>mount" }] },
                { v: [{ v: "<v>an" }, { ve: "lea<ve>" }] },
                { w: [{ w: "<w>inter" }, { wh: "<wh>ere" }] },
                { x: [{ x: "mi<x>" }] },
                { y: [{ y: "<y>ellow" }] },
                { z: [{ z: "<z>ipper" }, { zz: "bu<zz>er" }, { s: "hi<s>" }, { l: "lo<se>" }, { ze: "oo<ze>" }, { x: "<x>ylophone" }] },
                { ch: [{ w: "<ch>imp" }, { tch: "ca<tch>" }] },
                { ee: [{ ee: "tr<ee>" }, { ea: "<ea>t" }, { ie: "br<ie>f" }, { y: "sunn<y>" }, { e: "h<e>" }, { ie: "pet<i>t<e>" }, { i: "ind<i>an" }, { ei: "rec<ei>ve" }, { ee: "p<e>t<e>" }, { ey: "monk<ey>" }] },
                { er: [{ er: "h<er>" }, { ur: "b<ur>n" }, { ir: "b<ir>d" }, { w: "w<or>m" }, { ear: "<ear>n" }, { yr: "s<yr>up" }, { ar: "collar" }] },
                { ee: [{ ee: "tr<ee>" }, { ea: "<ea>t" }, { ie: "br<ie>f" }, { y: "sunn<y>" }, { e: "h<e>" }, { ie: "pet<i>t<e>" }, { i: "ind<i>an" }, { ei: "rec<ei>ve" }, { ee: "p<e>t<e>" }, { ey: "monk<ey>" }] },
                { oo: [{ oo: "l<oo>t" }, { ue: "gl<ue>" }, { ew: "fl<ew>" }, { u: "s<u>per" }, { ui: "fr<ui>t" }, { ue: "t<u>n<e>" }, { ou: "gr<ou>p" }, { sh: "sh<oe>" }, { o: "t<o>" }] },
                { oo: [{ oo: "l<oo>k" }, { uol: "c<oul>d" }, { u: "p<u>t" }] },
                { ow: [{ ow: "t<ow>n" }, { ou: "<ou>t" }] },
                { oy: [{ oy: "t<oy>" }, { oi: "<oi>l" }] },
                { qu: [{ qu: "<qu>iet" }] },
                { sh: [{ sh: "<sh>ip" }, { ch: "mi<ch>gan" }, { s: "<s>ure" }] },
                { ue: [{ ue: "c<u>t<e>" }, { u: "p<u>pil" }, { ew: "kn<ew>" }, { ue: "f<ue>l" }] },
                { ie: [{ ie: "b<i>t<e>" }, { ie: "tr<ie>d" }, { i: "ch<i>ld" }, { igh: "f<igh>t" }, { y: "m<y>" }, { eigh: "h<eigh>t" }] },
                { ae: [{ ae: "<a>t<e>" }, { ai: "tr<ai>n" }, { ay: "st<ay>" }, { ea: "br<ea>k" }, { ey: "th<ey>" }, { eigh: "sl<eigh>" }, { a: "t<a>ble" }, { ei: "r<ei>gn" }, { ei: "str<aigh>t" }] },
                { aw: [{ al: "w<al>k" }, { aw: "w<al>k" }, { au: "<au>gust" }, { o: "fr<o>g" }] }
            ];
            this._arrAnswear = new Array("a", "b", "c", "d");
            this.widthScreen = 0;
            this.heightScreen = 0;
            this._parent = parent_;
            this._parent._isShowEndScrren = true;
            this._successArr = new Array(new Phaser.Sprite(this.game, 0, 0, "letterSuccess1", 0), new Phaser.Sprite(this.game, 0, 0, "letterSuccess2", 0), new Phaser.Sprite(this.game, 0, 0, "letterSuccess3", 0), new Phaser.Sprite(this.game, 0, 0, "letterSuccess4", 0));
            this._arrAnswear = lettersArr;
            console.log("zebrane litery to ", this._arrAnswear);
            //this.game.add.sprite(0, 0, "bg");
            var bg = new Phaser.Sprite(this.game, 0, 0, "bg");
            this.addChild(bg);
            this.widthScreen = bg.width;
            this.heightScreen = bg.height;
            //      this._dropLetter = new DropLetter(this.game);
            //    this.addChild(this._dropLetter);
            this._objectsToDropGr = new Snow.DropLetter(this.game, this, this._arrAnswear.length);
            this._objectsToDropGr.y = 0;
            this._objectsToDropGr.enableBody = true;
            this.addChild(this._objectsToDropGr);
            this._wordsGr = this.game.add.group();
            this._wordsGr.y = 100;
            this._wordsGr.x = 330;
            this.addChild(this._wordsGr);
            this.makeLetter();
            this.game.input.onUp.add(this.stageUp, this);
            var gameButton = new Phaser.Button(this.game, 0, 0, "clearBtn", this.onGameClear, this, 1, 0);
            gameButton.x = 300;
            gameButton.y = 500;
            this.addChild(gameButton);
            var gameButton = new Phaser.Button(this.game, 0, 0, "doneBtn", this.onDoneClick, this, 1, 0);
            gameButton.inputEnabled;
            gameButton.x = 500;
            gameButton.y = 500;
            this.addChild(gameButton);
        }
        Letters.prototype.onDoneClick = function (item) {
            item.inputEnabled = false;
            this._wordsGr.removeAll();
            this._objectsToDropGr.checkLetter();
        };
        Letters.prototype.onGameClear = function () {
            this._wordsGr.removeAll();
            this._objectsToDropGr.clear();
        };
        Letters.prototype.playAnim = function (animSpr) {
            this.addChild(animSpr);
            animSpr.x = 400;
            animSpr.y = 100;
            var anim = animSpr.animations.add("anim");
            anim.onComplete.add(function () {
                this.game.time.events.add(Phaser.Timer.SECOND * 2, this.animationStopped, this);
            }, this);
            animSpr.animations.play("anim", 24, false, false);
        };
        Letters.prototype.playAnim2 = function (animSpr) {
            this.addChild(animSpr);
            animSpr.x = 400;
            animSpr.y = 100;
            var anim = animSpr.animations.add("anim");
            anim.onComplete.add(function () {
                this.game.time.events.add(Phaser.Timer.SECOND * 2, this.animationStopped2, this);
            }, this);
            animSpr.animations.play("anim", 24, false, false);
        };
        Letters.prototype.animationStopped = function () {
            this._parent.youwinSequence();
            //   this._parent.stopGame(false);
            this.kill();
            this.visible = false;
        };
        Letters.prototype.animationStopped2 = function () {
            this._parent.youLose();
            this.kill();
            this.visible = false;
        };
        Letters.prototype.youWin = function () {
            this.playAnim(this._successArr[this.randomNum(this._successArr.length, 0)]);
        };
        Letters.prototype.youLose = function () {
            this.playAnim2(new Phaser.Sprite(this.game, 0, 0, "fail1", 0));
        };
        Letters.prototype.makeLetter = function () {
            var stalaY = 100;
            var posx = 0;
            var posy = 0;
            var licz = 1;
            for (var i = 0; i < this.alphabet.length; i++) {
                var letter = new Snow.Letter(this.game, this.alphabet[i], 1);
                letter.inputEnabled = true;
                letter.input.useHandCursor = true;
                letter.events.onInputDown.add(this.letterOnDown, this);
                letter.x = posx;
                letter.y = posy + stalaY;
                this.addChild(letter);
                posx += 50 + 10;
                console.log(posx);
                if (licz % 5 == 0 && licz != 0) {
                    posy += 60;
                    posx = 0;
                }
                licz++;
            }
            console.log("ile", i);
            var letter1 = new Snow.PlaceToDrop(this.game);
            //  letter.inputEnabled = true;
            //   this._objectsToDropGr.add(letter);
            //var letter2: Letter = new Letter(this.game, "");
            //  letter2.x = 100;
            //    this._objectsToDropGr.add(letter2);
        };
        Letters.prototype.letterOnDown = function (letter) {
            for (var i = 0; i < this.words.length; i++) {
                for (var name in this.words[i]) {
                    if (name == letter.name) {
                        this.drawWords(this.words[i][name]);
                    }
                }
            }
        };
        Letters.prototype.drawWords = function (arr) {
            this._wordsGr.forEach(function (item) {
                item.kill();
            }, this);
            var posX = 0;
            var posY = 0;
            for (var i = 0; i < arr.length; i++) {
                for (var name in arr[i]) {
                    //      console.log("nazwa ? ", name);
                    //    console.log(arr[i][name]);
                    var letter = new Snow.LetterWord(this.game, name);
                    letter.events.onInputDown.add(this.check, this);
                    letter.y = posY;
                    letter.x = posX;
                    this._wordsGr.add(letter);
                    var word = new Snow.Word(this.game, arr[i][name]);
                    word.x = letter.widthLetter + 15 + posX;
                    word.y = posY;
                    posY += word.height + 30;
                    this._wordsGr.add(word);
                    // this.drawWords(this.words[i][name]);
                    if (i == 5) {
                        posY = 0;
                        posX = 200;
                    }
                }
            }
        };
        Letters.prototype.check = function (item) {
            var spr = item.addObjectToDrag();
            spr.anchor.set(0.5, 0.5);
            this.stage.addChild(spr);
            this._objectToDrag = spr;
            this.game.physics.enable(spr, Phaser.Physics.ARCADE);
        };
        Letters.prototype.checkSecondTime = function (item) {
            item.isAdded = false;
            item.placeToDrop.isAdded = false;
            var spr = item;
            this.stage.addChild(spr);
            this._objectToDrag = spr;
            this.game.physics.enable(spr, Phaser.Physics.ARCADE);
        };
        Letters.prototype.stageUp = function () {
            this.game.physics.arcade.overlap(this._objectToDrag, this._objectsToDropGr, this.collision, null, this);
            // if (this._objectToDrag != null) this.checkIfNotAdded(this._objectToDrag);
            this.checkIfNotAdded(this._objectToDrag);
        };
        Letters.prototype.checkIfNotAdded = function (object) {
            if (object === void 0) { object = null; }
            if (object != null && !object.isAdded) {
                console.log("object niedodany ");
                object.kill();
            }
            else {
                console.log("object dodany ");
            }
        };
        Letters.prototype.collision = function (object, object2) {
            console.log("kolizja");
            // this._objectsToDropGr.add(object);
            //object.x = object2.x;
            // object.y = object2.y;
            if (!object2.isAdded) {
                object2.addLetter(object);
                //   object.x = 0;
                //  object.y = 0;
                object.events.onInputDown.add(this.checkSecondTime, this);
                //  object2.addChild(object);
                object.isAdded = true;
            }
            else {
                object.kill();
            }
            this._objectToDrag = null;
        };
        Letters.prototype.update = function () {
            if (this._objectToDrag != null) {
                // var pos = this.stage.position.x
                this._objectToDrag.position.setTo(this.game.input.x, this.game.input.y);
            }
        };
        Letters.prototype.randomNum = function (max, min) {
            if (min === void 0) { min = 0; }
            return Math.floor(Math.random() * (max - min) + min);
        };
        return Letters;
    })(Phaser.Sprite);
    Snow.Letters = Letters;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var PlaceToDrop = (function (_super) {
        __extends(PlaceToDrop, _super);
        function PlaceToDrop(game, big) {
            if (big === void 0) { big = false; }
            _super.call(this, game, 0, 0, "");
            this.isAdded = false;
            this._bg = new Phaser.Sprite(game, 0, 0, "szare");
            if (big)
                this._bg = new Phaser.Sprite(game, 0, 0, "szared");
            this.addChild(this._bg);
        }
        PlaceToDrop.prototype.addLetter = function (letter) {
            if (!this.isAdded) {
                this._letter = letter;
                this._letter.x = 0;
                this._letter.y = 0;
                this.addChild(this._letter);
                this._letter.placeToDrop = this;
                this.isAdded = true;
            }
        };
        PlaceToDrop.prototype.clear = function () {
            if (this._letter != null) {
                this.removeChild(this._letter);
                this.isAdded = false;
                this._letter.isAdded = false;
                this._letter.kill();
            }
        };
        return PlaceToDrop;
    })(Phaser.Sprite);
    Snow.PlaceToDrop = PlaceToDrop;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var Test = (function (_super) {
        __extends(Test, _super);
        function Test() {
            _super.apply(this, arguments);
        }
        return Test;
    })(Phaser.Group);
    Snow.Test = Test;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var Word = (function (_super) {
        __extends(Word, _super);
        function Word(game, letter) {
            _super.call(this, game, 0, 0, "");
            this.name = letter;
            this._letter = new Phaser.Sprite(game, 0, 0, "");
            // this._letter.addChild(gra);
            this.addChild(this._letter);
            var first = letter.indexOf("<");
            var last;
            var firstText;
            var lastText;
            var middle;
            //   console.log("index", first);
            if (first != -1) {
                last = letter.indexOf(">");
                firstText = letter.slice(0, first);
                lastText = letter.slice(last + 1, letter.length);
                middle = letter.slice(first + 1, last);
                var color2 = "#FF0000";
                var style2 = { font: "35px Arial", fill: color2, align: "left" };
                var color = "#000000";
                var style = { font: "35px Arial", fill: color, align: "left" };
                var _letterShow = new Phaser.Text(this.game, 0, 0, firstText, style);
                var _letterMiddle = new Phaser.Text(this.game, 0, 0, middle, style2);
                var _letterEnd = new Phaser.Text(this.game, 0, 0, lastText, style);
                this._letter.addChild(_letterShow);
                _letterMiddle.x = _letterShow.x + _letterShow.width;
                _letterEnd.x = _letterMiddle.x + _letterMiddle.width;
                this._letter.addChild(_letterMiddle);
                this._letter.addChild(_letterEnd);
                this._letter.x = 50 - this._letter.width / 2;
            }
        }
        return Word;
    })(Phaser.Sprite);
    Snow.Word = Word;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var MainMenu = (function (_super) {
        __extends(MainMenu, _super);
        function MainMenu() {
            _super.apply(this, arguments);
        }
        MainMenu.prototype.create = function () {
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var _mainMenuBg = this.game.add.audio('MainMenuSn', 1, true);
            _mainMenuBg.volume = 0.1;
            _mainMenuBg.play();
            var gameControler = Snow.GameControler.getInstance();
            gameControler.setSound(_mainMenuBg);
            var background = this.add.sprite(0, 0, 'mainMenuBg');
            var _buttons = this.add.group();
            var gameButton = this.add.button(0, 0, "games", this.onGameClick, this, 1, 0);
            //gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            _buttons.addChild(gameButton);
            var playButton = this.add.button(100, 0, "playGame", this.onPlayClick, this, 1, 0);
            playButton.onInputDown.add(this.overPlay2, this);
            _buttons.addChild(playButton);
            _buttons.x = 0;
            _buttons.y = this.game.stage.height - _buttons.height;
        };
        MainMenu.prototype.overPlay2 = function () {
            this.overButtonA.play();
        };
        MainMenu.prototype.onGameClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        MainMenu.prototype.onPlayClick = function () {
            this.game.state.start("PopupStart", true, false);
        };
        return MainMenu;
    })(Phaser.State);
    Snow.MainMenu = MainMenu;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var Obstacle = (function (_super) {
        __extends(Obstacle, _super);
        function Obstacle(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this.name = key;
            if (this.name == "osmiornica" || this.name == "shark" || this.name == "shark2" || this.name == "shark3" || this.name == "shark4") {
                this._haveAnim = true;
                this.animations.add("anim");
                this.animations.play("anim", 24, true);
                this.y += 305;
            }
            else if (this.name == "boatsmedium") {
                this.frame = this.getRandom(this.animations.frameTotal);
                this.y += 50;
            }
            else if (this.name == "boatssmall") {
                this.y += 100;
            }
            else {
                this.y += 305;
            }
        }
        Obstacle.prototype.getRandom = function (max) {
            return Math.floor(Math.random() * max);
        };
        Obstacle.prototype.stopAnim = function () {
            if (this._haveAnim) {
                this.animations.stop("anim");
            }
            else {
                console.log("nie ma animacji", this.name);
            }
        };
        return Obstacle;
    })(Phaser.Sprite);
    Snow.Obstacle = Obstacle;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var Player = (function (_super) {
        __extends(Player, _super);
        function Player(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this._gameControl = Snow.GameControler.getInstance();
            this._currentTrickiCount = 0;
            this.blokadaAnimLeft = false;
            this.blokadaAnimRight = false;
            //  this.anchor.setTo(0.5, 0.5);
            game.physics.enable(this, Phaser.Physics.ARCADE);
            this._crashForward = new Phaser.Sprite(game, 0, 0, "crashforward");
            this._crashForward.animations.add("play");
            this._left = new Phaser.Sprite(game, 0, 0, "left");
            this._left.animations.add("play");
            this._right = new Phaser.Sprite(game, 0, 0, "right");
            this._right.animations.add("play");
            this._jump = new Phaser.Sprite(game, 0, 0, "jump");
            this._jump.animations.add("play");
            this._forward = new Phaser.Sprite(game, 0, 0, "player");
            this._forward.animations.add("play");
            this._forwardSpeed = new Phaser.Sprite(game, 0, 0, "forwardSpeed");
            this._forwardSpeed.animations.add("play");
            this._jumpAd = this.game.sound.add("jumpAd", 1, false);
            this._psssAd = this.game.sound.add("psssAd", 1, false);
            this._crashAd = this.game.sound.add("crashAd", 1, false);
            // this._miganie = new Phaser.Sprite(game, 0, 0, "miganie");
            // this._miganie.animations.add("play");
            /*  this.animations.add("forward", this.countFrames(1, 19), 24, true);
              this.animations.add("fastforward", this.countFrames(20, 34), 24, false);
  
              this.animations.add("jump", this.countFrames(35, 88), 24, false);
              this.animations.add("jumptrick", this.countFrames(89, 136), 24, false);
          */
            this.electroAudio = this.game.sound.add("electro", 1, false, true);
            this.electroAudio2 = this.game.sound.add("electro1", 1, false, true);
            this.crashAudio = this.game.sound.add("crash", 1, false, true);
            this.crashAudio2 = this.game.sound.add("crash2", 1, false, true);
            this.jumpAudio = this.game.sound.add("jumpAudio", 1, false, true);
            this.holeAudio = this.game.sound.add("holeAudio", 1, false, true);
        }
        Player.prototype.jump = function () {
            this.addAnim(this._jump);
            this._currentAnim.animations.play("play", 24, false, false);
            this._jumpAd.play();
            this._currentAnim.animations.getAnimation("play").onComplete.add(function () {
                this.endJump();
            }, this);
        };
        Player.prototype.endJump = function () {
            this.addAnim(this._forward);
            this._currentAnim.animations.play("play", 24, true, false);
            this._gameControl.collisionPlayer = false;
            this._gameControl.level.addPlayerToTop(false);
            this.blokadaAnimLeft = false;
            this.blokadaAnimRight = false;
        };
        Player.prototype.right = function () {
            if (!this.blokadaAnimRight) {
                this.blokadaAnimLeft = false;
                this.blokadaAnimRight = true;
                this.addAnim(this._right);
                this._psssAd.play();
                this._currentAnim.animations.play("play", 24, false, false);
                this._currentAnim.animations.getAnimation("play").onComplete.add(function () {
                    this.endJump();
                }, this);
            }
        };
        Player.prototype.left = function () {
            if (!this.blokadaAnimLeft) {
                this.blokadaAnimLeft = true;
                this.blokadaAnimRight = false;
                this.addAnim(this._left);
                this._psssAd.play();
                this._currentAnim.animations.play("play", 24, false, false);
                this._currentAnim.animations.getAnimation("play").onComplete.add(function () {
                    this.endJump();
                }, this);
            }
        };
        Player.prototype.crashForward = function () {
            console.log("gram kolizje");
            this.addAnim(this._crashForward);
            this._crashAd.play();
            this._gameControl.level.stopGame(true);
            this._currentAnim.animations.play("play", 24, false);
            this._currentAnim.animations.getAnimation("play").onComplete.add(function () {
                this.crashForwardOver();
            }, this);
        };
        Player.prototype.crashForwardOver = function () {
            console.log("koncze grac kolizje");
            this._gameControl.level.stopGame(false, true);
            this.addAnim(this._forward);
            this._currentAnim.animations.play("play", 24, false);
            this.game.time.events.add(Phaser.Timer.SECOND * 1, this.activePlayer, this);
            // this.activePlayer();
            // this.game.add.tween(this._currentAnim).to({ alpha: 0 }, 0.5, Phaser.Easing.Linear.None, false, 0.5, 5).start();
            //var tweenAn =  this.game.add.tween(this._currentAnim).to({ alpha: 1 }, 5, Phaser.Easing.Linear.None,false).start();
            //  this.game.time.events.add(Phaser.Timer.SECOND * 4, this.activePlayer, this)
            //  tweenAn.onComplete.add(this.activePlayer,this);
        };
        Player.prototype.activePlayer = function () {
            console.log("aktywne");
            this._gameControl.collisionPlayer = false;
        };
        Player.prototype.forward = function () {
            this.addAnim(this._forward);
            this._currentAnim.animations.play("play", 24, true);
        };
        Player.prototype.addAnim = function (anim) {
            if (this._currentAnim != null) {
                this.removeChild(this._currentAnim);
            }
            this._currentAnim = anim;
            //  this._currentAnim.scale.setTo(0.5, 0.5);
            this._currentAnim.anchor.setTo(0.5, 0.5);
            this._currentAnim.animations.stop("play", true);
            this.addChild(this._currentAnim);
        };
        Player.prototype.playAgain = function () {
            if (this.stage.x < 0)
                this.stage.x = 300;
            else if (this.stage.x > 800)
                this.stage.x = 300;
            if (this.stage.y < 0)
                this.stage.y = 500;
            else if (this.stage.y > 600)
                this.stage.y = 500;
            console.log("sprawdzam", this.stage.x, this.stage.y);
            if (isNaN(this.x)) {
                this.x = 400;
                console.log("x is nan");
            }
            if (isNaN(this.y)) {
                console.log("y is nan");
                this.x = 300;
            }
            this._gameControl.collisionPlayer = false;
        };
        Player.prototype.countFrames = function (start, num) {
            var countArr = new Array();
            for (var i = start; i < num + 1; i++) {
                countArr.push(i);
            }
            return countArr;
        };
        return Player;
    })(Phaser.Sprite);
    Snow.Player = Player;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var PopLetter = (function (_super) {
        __extends(PopLetter, _super);
        function PopLetter(game, x, y, key, posx, posy, bounceWidth, bounceHeight) {
            if (posx === void 0) { posx = 0; }
            if (posy === void 0) { posy = 0; }
            if (bounceWidth === void 0) { bounceWidth = 0; }
            if (bounceHeight === void 0) { bounceHeight = 0; }
            _super.call(this, game, x, y, key);
            this.letter = "b";
            this.color = "#000000";
            this.name = "popletter";
            //  this.animations.add("pop");
            this.animations.add("pop", this.countFrames(7, 19), 24, false);
            this.y += 300;
            this.posx = (posx == 0) ? 0 : posx;
            this.posy = (posy == 0) ? 0 : posy;
            this.bounceWidth = (bounceWidth == 0) ? this.width : bounceWidth;
            this.bounceHeight = (bounceHeight == 0) ? this.height : bounceHeight;
            this._coinAd = this.game.sound.add("monetaAd", 1, false);
        }
        PopLetter.prototype.isCoin = function () {
            this.removeChild(this._letterShow);
            this.name = "coin";
            this.frame = 4;
        };
        PopLetter.prototype.isRed = function (red) {
            if (red) {
                this.color = "#FF0000";
                this.name = "correctLetter";
            }
        };
        PopLetter.prototype.coinAd = function () {
            this._coinAd.play();
        };
        PopLetter.prototype.addLetter = function (str) {
            this.frame = str.length - 1;
            var style = { font: "40px Arial", fill: this.color, align: "center" };
            if (this._letterShow)
                this._letterShow.destroy();
            this._letterShow = new Phaser.Text(this.game, 0, 0, str, style);
            this._letterShow.x = 50 - this._letterShow.width / 2;
            this._letterShow.y = 25 - this._letterShow.height / 2;
            // this._letterShow.anchor.set(0.5, 0.5);
            //   this._letterShow.x = 40;
            //  this._letterShow.y = 25;
            this.addChild(this._letterShow);
            this.letter = str;
        };
        PopLetter.prototype.getBounce = function () {
            var rect = new Phaser.Rectangle(this.posx, this.posy, this.bounceWidth, this.bounceHeight);
            return rect;
        };
        PopLetter.prototype.collisonPlayAnim = function () {
            this.removeChild(this._letterShow);
            // this.animations.getAnimation("pop").onComplete.add(function () { this.kill(); }, this);
            this.play("pop", 24, false, true);
        };
        PopLetter.prototype.countFrames = function (start, num) {
            var countArr = new Array();
            for (var i = start; i < num + 1; i++) {
                countArr.push(i);
            }
            return countArr;
        };
        return PopLetter;
    })(Phaser.Sprite);
    Snow.PopLetter = PopLetter;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var Preloader = (function (_super) {
        __extends(Preloader, _super);
        function Preloader() {
            _super.apply(this, arguments);
        }
        Preloader.prototype.preload = function () {
            var bg = this.add.image(0, 0, "bg", 0);
            var blackProgress = this.add.image(0, 0, "progressBarBlack", 0);
            var blueProgress = this.add.sprite(0, 0, "progressBarBlue", 0);
            blackProgress.position.setTo(this.stage.width / 2 - blackProgress.width / 2, this.stage.height - blackProgress.height - 10);
            blueProgress.position = blackProgress.position;
            this.load.setPreloadSprite(blueProgress);
            this.load.image("mainMenuBg", "assets/mainMenu/splashScreen.png");
            this.load.atlasJSONArray('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');
            this.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
            this.load.audio("MainMenuSn", "assets/snow/audio/ThemeMusic.mp3");
            this.load.audio("overButtonA", "assets/audio/overButton.mp3");
            this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");
            mobile = false;
            if (!this.game.device.desktop) {
                mobile = true;
            }
            if (mobile) {
                this.load.image("firstInstruction", "assets/03_scate_create.png");
                this.load.audio("instructionSn", "assets/audio/skate_create_02.mp3");
            }
            else {
                this.load.image("firstInstruction", "assets/02_scate_create.png");
                this.load.audio("instructionSn", "assets/audio/skate_create_01.mp3");
            }
            this.load.image("popup", "assets/popup/popup.png");
            this.load.image("logoGame", "assets/popup/logo.png");
            this.load.atlasJSONArray('games', 'assets/popup/games.png', 'assets/popup/games.json');
            this.load.atlasJSONArray('playGame', 'assets/popup/play.png', 'assets/popup/play.json');
            this.load.atlasJSONArray('helpBtn', 'assets/popup/help.png', 'assets/popup/help.json');
            this.load.image("bgGame", "assets/water/gameBackground.png");
            this.load.atlasJSONArray('boatsmedium', 'assets/water/boat/boatsmedium.png', 'assets/water/boat/boatsmedium.json');
            this.load.atlasJSONArray('boatssmall', 'assets/water/boat/boatssmall.png', 'assets/water/boat/boatssmall.json');
            /* instructions */
            this.load.image('prompt', 'assets/snow/prompt/instruction1.png');
            this.load.audio("WakeIntro", "assets/snow/prompt/IntroPrompt.mp3");
            this.load.audio("WakeSuccess0", "assets/snow/prompt/SuccessPrompt4.mp3");
            this.load.audio("WakeWrong0", "assets/snow/prompt/FailurePrompt1_0.mp3");
            /*


            */
            this.load.audio("jumpAd", "assets/snow/player/jump.wav");
            this.load.audio("psssAd", "assets/snow/player/psss.mp3");
            this.load.audio("monetaAd", "assets/snow/player/moneta.mp3");
            this.load.audio("crashAd", "assets/snow/player/crash.mp3");
            this.load.image("firstInstruction", "assets/water/instructionsScreen.png");
            this.load.image("sequence", "assets/snow/letterClip.png");
            /* player */
            this.load.atlasJSONArray('player', 'assets/snow/player/paddle.png', 'assets/snow/player/paddle.json');
            this.load.atlasJSONArray('jump', 'assets/snow/player/grind.png', 'assets/snow/player/grind.json');
            this.load.atlasJSONArray('forwardSpeed', 'assets/water/player/charging.png', 'assets/water/player/charging.json');
            this.load.atlasJSONArray('left', 'assets/snow/player/left.png', 'assets/snow/player/left.json');
            this.load.atlasJSONArray('right', 'assets/snow/player/right.png', 'assets/snow/player/right.json');
            this.load.atlasJSONArray('miganie', 'assets/water/player/miganie.png', 'assets/water/player/miganie.json');
            this.load.atlasJSONArray('crashforward', 'assets/snow/player/crashForward.png', 'assets/snow/player/crashForward.json');
            for (var i = 1; i < 7; i++) {
                this.load.atlasJSONArray('tricki' + i, 'assets/water/player/trick' + i + '.png', 'assets/water/player/trick' + i + '.json');
            }
            this.load.atlasJSONArray('special', 'assets/water/special.png', 'assets/water/special.json');
            this.load.atlasJSONArray('jumpbtn', 'assets/water/jumpBtn.png', 'assets/water/jumpBtn.json');
            this.load.image('bgletter', 'assets/water/letter/bgletter.png');
            this.load.image("bgClock", "assets/water/interface.png");
            /* obstacle */
            /*    this.load.image("piasek", "assets/water/obstacle/wood2.png");
                this.load.image("wood", "assets/water/obstacle/wood.png");
                this.load.image("hammer", "assets/water/obstacle/hammer.png");
                this.load.image("turtle", "assets/water/obstacle/turtle.png");
                this.load.image("zielone", "assets/water/obstacle/zielone.png");
                this.load.image("ball", "assets/water/obstacle/ball.png");
                this.load.image("ball2", "assets/water/obstacle/ball2.png");
                this.load.image("walen", "assets/water/obstacle/walen.png");
                */
            this.load.image("wood2", "assets/snow/obstacle/wood2.png");
            this.load.image("wood", "assets/snow/obstacle/wood.png");
            this.load.image("jumper", "assets/snow/obstacle/jumper.png");
            this.load.image("jumper2", "assets/snow/obstacle/jumper2.png");
            this.load.image("choinka", "assets/snow/obstacle/choinka.png");
            this.load.image("balwan", "assets/snow/obstacle/balwan.png");
            this.load.image("pien", "assets/snow/obstacle/pien.png");
            this.load.image("rocks", "assets/snow/obstacle/rocks.png");
            this.load.image("rocks2", "assets/snow/obstacle/rocks2.png");
            this.load.atlasJSONArray('popletter', 'assets/snow/popletter/popletter1.png', 'assets/snow/popletter/popletter1.json');
            this.load.atlasJSONArray('shark1', 'assets/water/obstacle/shark.png', 'assets/water/obstacle/shark.json');
            this.load.atlasJSONArray('shark2', 'assets/water/obstacle/shark2.png', 'assets/water/obstacle/shark2.json');
            this.load.atlasJSONArray('shark3', 'assets/water/obstacle/shark3.png', 'assets/water/obstacle/shark3.json');
            this.load.atlasJSONArray('shark4', 'assets/water/obstacle/shark4.png', 'assets/water/obstacle/shark4.json');
            this.load.atlasJSONArray('osmiornica', 'assets/water/obstacle/osmiornica.png', 'assets/water/obstacle/osmiornica.json');
            for (var i = 1; i < 5; i++) {
                this.load.audio("fail" + i, "assets/audio/instruction/fail" + i + ".mp3");
            }
            for (var i = 1; i < 5; i++) {
                this.load.audio("success" + i, "assets/audio/instruction/success" + i + ".mp3");
            }
            /*skrzynie*/
            this.load.atlasJSONArray("scoreScreen", "assets/scoreScreen/scoreScreen.png", "assets/scoreScreen/scoreScreen.json");
            this.load.atlasJSONArray("playScoreScreen", "assets/scoreScreen/scoreScreenButtons.png", "assets/scoreScreen/scoreScreenButtons.json");
            for (var i = 1; i < 6; i++) {
                this.load.atlasJSONArray("fail" + i, "assets/feedbacks/fail" + i + ".png", "assets/feedbacks/fail" + i + ".json");
            }
            for (var i = 1; i < 5; i++) {
                this.load.atlasJSONArray("success" + i, "assets/feedbacks/success" + i + ".png", "assets/feedbacks/success" + i + ".json");
            }
            for (var i = 1; i < 5; i++) {
                this.load.audio("congrats" + i, "assets/feedbacks/congrats" + i + ".mp3");
            }
            this.load.audio("failure1", "assets/feedbacks/failure1.mp3");
            this.load.audio("failure2", "assets/feedbacks/failure2.mp3");
            this.load.image("szare", "letters/szare.png");
            this.load.image("szared", "letters/szared.png");
            this.load.image("szared2", "letters/szereb2.png");
            this.load.image("orange", "letters/poma.png");
            this.load.image("green", "letters/ziel.png");
            this.load.image("bg", "letters/bg.png");
            this.load.image("bgDrop", "letters/bgDrop.png");
            this.load.atlasJSONArray('doneBtn', 'letters/done.png', 'letters/done.json');
            this.load.atlasJSONArray('clearBtn', 'letters/clearbtn.png', 'letters/clearbtn.json');
            this.load.atlasJSONArray("niceTry", "letters/fail1.png", "letters/fail1.json");
            this.load.atlasJSONArray("letterSuccess1", "letters/perfect.png", "letters/perfect.json");
            this.load.atlasJSONArray("letterSuccess2", "letters/awesome.png", "letters/awesome.json");
            this.load.atlasJSONArray("letterSuccess3", "letters/waytogo.png", "letters/waytogo.json");
            this.load.atlasJSONArray("letterSuccess4", "letters/yougotit.png", "letters/yougotit.json");
        };
        Preloader.prototype.create = function () {
            this.game.state.start("MainMenu", true, false);
        };
        return Preloader;
    })(Phaser.State);
    Snow.Preloader = Preloader;
})(Snow || (Snow = {}));
var mobile;
var Snow;
(function (Snow) {
    var ClockView = (function (_super) {
        __extends(ClockView, _super);
        function ClockView(game, parent) {
            _super.call(this, game);
            this._creditsCount = 0;
            this._sequencCount = 0;
            this._gameControler = Snow.GameControler.getInstance();
            game.add.existing(this);
            this._parent = parent;
            var bg = this.game.add.sprite(0, 0, 'bgClock');
            this.addChild(bg);
            var style = { font: "18px Arial", fill: "#000000", align: "left" };
            this._time = this.game.add.text(70, 80, "00:00", style);
            this.addChild(this._time);
            this._sequence = this.game.add.text(35, 13, "", style);
            this.addChild(this._sequence);
            this._credits = this.game.add.text(40, 47, "", style);
            this.addChild(this._credits);
        }
        /*
        Public
        */
        ClockView.prototype.startTimer = function () {
            console.log("START CZAS");
            this.resetTimer();
            this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);
        };
        ClockView.prototype.stopTimer = function () {
            console.log("STOP CZAS");
            this.game.time.events.removeAll();
        };
        ClockView.prototype.addCredits = function () {
            this._creditsCount++;
            this.setCreditText("" + this._creditsCount);
        };
        ClockView.prototype.setSequenceText = function (text) {
            this._sequencCount++;
            if (this._sequencCount > 2) {
                console.log("next Level");
                this._sequencCount = 1;
                this._parent.stopGame(true);
                this._parent.showFinishLevel(true);
                this._sequence.setText("" + this._sequencCount + "/2");
                if (userLevel < 5) {
                    APIsetLevel(userID, gameID, userLevel + 1);
                    userLevel++;
                    this._gameControler.currentLevel = userLevel;
                }
            }
            else {
                this._sequence.setText("" + this._sequencCount + "/2");
                this._parent._isShowEndScrren = false;
                this._parent.stopGame(false);
            }
        };
        ClockView.prototype.setCreditText = function (text) {
            this._credits.setText("$" + text);
        };
        /*
        Private
        */
        ClockView.prototype.resetTimer = function () {
            console.log("RESET CZAS");
            this._time.setText("00:00");
            this._seconds = 0;
            this._minutes = 0;
        };
        ClockView.prototype.updateTimer = function () {
            this._seconds++;
            if (this._seconds >= 60) {
                this._minutes++;
                this._seconds = 0;
            }
            var sec;
            var min;
            if (this._seconds < 10) {
                sec = "0" + this._seconds;
            }
            else {
                sec = "" + this._seconds;
            }
            if (this._minutes < 10) {
                min = "0" + this._minutes;
            }
            else {
                min = "" + this._minutes;
            }
            this._time.setText(min + ":" + sec);
        };
        return ClockView;
    })(Phaser.Group);
    Snow.ClockView = ClockView;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var FinishLevel = (function (_super) {
        __extends(FinishLevel, _super);
        function FinishLevel(game) {
            _super.call(this, game);
            this._gameControl = Snow.GameControler.getInstance();
            this._win = false;
            this._game = game;
            this.create();
        }
        FinishLevel.prototype.create = function () {
            this._bgBlack = new Phaser.Graphics(this.game, 0, 0);
            this._bgBlack.beginFill(0x000000, 0.6);
            this._bgBlack.drawRect(0, 0, this.game.width, this.game.height);
            this._bgBlack.endFill();
            this._bgSpr = new Phaser.Sprite(this.game, 0, 0, "");
            this._bgSpr.addChild(this._bgBlack);
            this.add(this._bgSpr);
            this._bgSpr.buttonMode = true;
            this._bgSpr.inputEnabled = true;
            this._bgSpr.input.priorityID = 10;
            this._bgSpr.events.onInputDown.add(this.onDown, this);
            //    this.gameOverSpr = new PopUp(this.game, this, 0, 0);
            //   this.youWinSpr = new PopUpWin(this.game, this, 0, 0);
            // this.visible = 
            // this.add(this.gameOverSpr);
            //            this.addChild(this.youWinSpr);
        };
        FinishLevel.prototype.youwin = function () {
            this._win = true;
            this.screenWinOrGameOver = new Snow.PopUpWin(this.game, this, 0, 0);
            this.addTostage();
        };
        FinishLevel.prototype.youLose = function () {
            this._win = false;
            this.screenWinOrGameOver = new Snow.PopUp(this.game, this, 0, 0);
            this.addTostage();
        };
        FinishLevel.prototype.addTostage = function () {
            this.addChild(this.screenWinOrGameOver);
            this.screenWinOrGameOver.activeButton();
            this.screenWinOrGameOver.visible = true;
        };
        FinishLevel.prototype.hideScreenAfterFinishPlaye = function () {
            this.playAgain();
        };
        FinishLevel.prototype.onDown = function () {
            console.log("ondown finish");
        };
        FinishLevel.prototype.playAgain = function () {
            if (this.screenWinOrGameOver != null) {
                this.screenWinOrGameOver.kill();
            }
            //  this._gameControl.level.clearBtnClick();
            this.visible = false;
            this._gameControl.level.playAgain(this._win);
            this.removeAll();
            this.destroy(true);
            console.log("play again");
        };
        FinishLevel.prototype.quitClick = function () {
            console.log("quit");
        };
        return FinishLevel;
    })(Phaser.Group);
    Snow.FinishLevel = FinishLevel;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var FinishScreen = (function (_super) {
        __extends(FinishScreen, _super);
        function FinishScreen(game) {
            _super.call(this, game);
            this._gameControl = Snow.GameControler.getInstance();
            this._game = game;
            this.create();
        }
        FinishScreen.prototype.create = function () {
            this._finishScreen = new Phaser.Sprite(this._game, 0, 0, "scoreScreen");
            this._finishScreen.animations.add("play");
            this._finishScreen.play("play", 24, true);
            this.addChild(this._finishScreen);
            this._playButton = new Phaser.Button(this._game, 0, 0, "playScoreScreen", this.playAgain, this, 1, 0, 1, 0);
            this._quit = new Phaser.Button(this._game, 300, 10, "playScoreScreen", this.quitClick, this, 3, 2, 3, 2);
            this._playButton.x = 257;
            this._playButton.y = 358;
            this._quit.x = 427;
            this._quit.y = 358;
            this.addChild(this._playButton);
            this.addChild(this._quit);
        };
        FinishScreen.prototype.playAgain = function () {
            this._gameControl.restart();
            this.removeAll();
            this.destroy(true);
            console.log("play again");
        };
        FinishScreen.prototype.quitClick = function () {
            console.log("quit");
        };
        return FinishScreen;
    })(Phaser.Group);
    Snow.FinishScreen = FinishScreen;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var ObstacleView = (function (_super) {
        __extends(ObstacleView, _super);
        function ObstacleView(game) {
            _super.call(this, game);
            this._game = game;
            this._gameControler = Snow.GameControler.getInstance();
            this._letterControler = Snow.LetterControler.getInstance();
            this.enableBody = true;
            this._obstacleControl = Snow.ObstacleControler.getInstance();
            this._boundsOut = new Phaser.Sprite(this._game, 0, 0, "");
            var grfx = new Phaser.Graphics(this.game, 0, 0);
            grfx.beginFill(0x000000, 1);
            grfx.drawRect(0, 0, 100, 100);
            grfx.endFill();
            this._boundsOut.addChild(grfx);
            this._boundsOut.y = this.game.height - 100;
            this._boundsOut.x = this.game.width;
            //   this.add(this._boundsOut);
            /*       this._boundsOut.body.velocity.x = -270;//this._gameControler.SPEEDX;
                   this._boundsOut.body.velocity.y = 0;
                   this._boundsOut.body.immovable = true;
                   this._boundsOut.body.checkCollision.none = true;
                   this._boundsOut.checkWorldBounds = true;*/
            // this._boundsOut.events.onOutOfBounds.add(this.getObstacle, this);
        }
        ObstacleView.prototype.randomBoats = function () {
            //     var rand = this.getRandom(this.boatsStrArr.length)
            //     var str: string = this.boatsStrArr[rand];
            //   var y_ = (rand == 0) ? 100 : 50;
            //    var boats: Phaser.Sprite = new Phaser.Sprite(this._game, 0, y_, str, 0);
            //    boats.frame = this.getRandom(boats.animations.frameTotal);
            return;
        };
        ObstacleView.prototype.getObstacle = function () {
            console.log("outofScreen", this._boundsOut.x, this._boundsOut.y);
            this.delObstacles();
            // this._boundsOut.y = this.game.height - 100;
            // this._boundsOut.x = this.game.width;
            this._obstacleArr = new Array();
            this._longerObstacle = new Array();
            //   if (!this._gameControler.collisionPlayer && this._gameControler.SPEEDX != 0) {
            var obstcaleArr = this._obstacleControl.level1();
            var max = obstcaleArr.length;
            for (var i = 0; i < max; i++) {
                var obj = obstcaleArr[i];
                if (obj.name == "popletter") {
                    var letter = this._letterControler.getPopLetter();
                    var objectName = obj.name;
                    var popletter = new Snow.PopLetter(this._game, obj.x + 800, obj.y + 320, obj.name);
                    popletter.isRed(letter[0].correct);
                    popletter.addLetter(letter[0].str);
                    // this.getTheLonger(popletter.x, popletter.width, i);
                    this.add(popletter);
                    popletter.body.velocity = new Phaser.Point(this._gameControler.SPEEDX, this._gameControler.SPEEDY);
                    this._obstacleArr.push(popletter);
                    popletter.body.setSize(60, 50, 20, 0);
                }
                else {
                    var obstacle = new Snow.Obstacle(this._game, obj.x + 800, obj.y + 320, obj.name);
                    this.getTheLonger(obstacle.x, obstacle.width, i);
                    this.add(obstacle);
                    obstacle.body.velocity = new Phaser.Point(this._gameControler.SPEEDX, this._gameControler.SPEEDY);
                    // obstacle.body.velocity.y = this._gameControler.SPEEDBOAT;
                    if (obj.name == "wood") {
                        obstacle.body.setSize(100, 50, 0, 0);
                    }
                    else if (obj.name == "jumper2") {
                        obstacle.body.setSize(100, 50, 15, 25);
                    }
                    else if (obj.name == "choinka") {
                        obstacle.body.setSize(100, 50, 0, 225);
                    }
                    else if (obj.name == "balwan") {
                        obstacle.body.setSize(70, 50, 0, 50);
                    }
                    this._obstacleArr.push(obstacle);
                }
            }
            //   }
            console.log(this._longerObstacle == null, this._longerObstacle.length);
            if (this._longerObstacle == null || this._longerObstacle.length == 0) {
                var obstacle = new Snow.Obstacle(this._game, obj.x + 800, obj.y + 320, "wood");
                this.getTheLonger(obstacle.x, obstacle.width, i);
                obstacle.visible = false;
                this.add(obstacle);
                obstacle.body.velocity = new Phaser.Point(this._gameControler.SPEEDX, this._gameControler.SPEEDY);
                obstacle.body.setSize(0, 0, 0, 0);
                this._obstacleArr.push(obstacle);
                this.getTheLonger(536, 423, 0);
            }
            console.log(this._longerObstacle);
            var obstacleFarAway = this._obstacleArr[this._longerObstacle[0][1]];
            obstacleFarAway.checkWorldBounds = true;
            this._whenAddNewScreen = obstacleFarAway;
            //  console.log("obstacle ile oboiektow wylosowano", max, obstacleFarAway);
            //  obstacleFarAway.events.onOutOfBounds.add(this.obstacleOutOfScreen, this);
            //   this.game.time.events.add(Phaser.Timer.SECOND * 4, this.getObstacle, this)
        };
        ObstacleView.prototype.addAgain = function () {
        };
        ObstacleView.prototype.getTheLonger = function (posx, width_, i) {
            console.log("test", posx, width_, i, this._longerObstacle.length);
            if (this._longerObstacle.length > 0) {
                if (this._longerObstacle[0][0] < posx + width_) {
                    this._longerObstacle[0] = new Array(posx + width_, i);
                }
            }
            else {
                this._longerObstacle.push(new Array(posx + width_, i));
            }
        };
        ObstacleView.prototype.changeLetterToCoin = function () {
            this.forEach(function (item) {
                if (item.name == "popletter" || item.name == "correctLetter") {
                    item.isCoin();
                }
            }, this);
        };
        ObstacleView.prototype.obstacleOutOfScreen = function () {
            console.log("boats out of screen");
            // this.delObstacle();
            // this.getObstacle();
        };
        ObstacleView.prototype.delObstacle = function () {
            for (var i = 0; i < this._obstacleArr.length; i++) {
                if (this._obstacleArr[i].world.x + this._obstacleArr[i].width < 0) {
                    this._obstacleArr[i].destroy(true);
                    this._obstacleArr.splice(i, 1);
                    --i;
                }
            }
        };
        ObstacleView.prototype.delObstacles = function () {
            this.forEach(function (item) {
                if (item != null && item != undefined && this.toGlobal(item.position).x < -1000) {
                    //   console.log("niszcze obiekt", item.name, this.toGlobal(item.position).x);
                    item.destroy(true);
                }
                //  if (!stop && (item.name == "boatsmedium" || item.name == "boatssmall")) item.body.velocity.x = -70;
            }, this);
        };
        ObstacleView.prototype.changeSpeed = function (stop, changePos) {
            if (stop === void 0) { stop = false; }
            if (changePos === void 0) { changePos = false; }
            var speed = (stop) ? 0 : this._gameControler.SPEEDX;
            var speedy = (stop) ? 0 : this._gameControler.SPEEDY;
            console.log("stop", stop, speed);
            this.forEach(function (item) {
                item.body.velocity = new Phaser.Point(speed, speedy);
                if (changePos) {
                    item.x += 150;
                    item.y += 100;
                }
                //  if (!stop && (item.name == "boatsmedium" || item.name == "boatssmall")) item.body.velocity.x = -70;
            }, this);
            //   this._boundsOut.body.velocity.x = -270;
            //   this._boundsOut.body.velocity.y = 0;
        };
        ObstacleView.prototype.removeObstacleLetters = function () {
            this.forEach(function (item) {
                if (item != undefined && item.name == "popletter")
                    item.destroy(true);
            }, this);
        };
        ObstacleView.prototype.removeObstacle = function () {
            this.forEach(function (item) {
                if (item != undefined)
                    item.destroy(true);
            }, this);
        };
        ObstacleView.prototype.getRandom = function (max) {
            return Math.floor(Math.random() * max);
        };
        ObstacleView.prototype.update = function () {
            // console.log("osttatni",this.toGlobal(this._whenAddNewScreen.position).x);
            if (this._whenAddNewScreen != null && this.toGlobal(this._whenAddNewScreen.position).x < 500) {
                this.getObstacle();
            }
        };
        return ObstacleView;
    })(Phaser.Group);
    Snow.ObstacleView = ObstacleView;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var PlayerPlay = (function (_super) {
        __extends(PlayerPlay, _super);
        function PlayerPlay(game) {
            _super.call(this, game);
            this._tween = null;
            this._stop = false;
            this._stopTween = false;
            this._game = game;
            this._gameControler = Snow.GameControler.getInstance();
            this.enableBody = true;
            this._obstacleControl = Snow.ObstacleControler.getInstance();
            if (mobile)
                this.game.input.onDown.add(this.movePlayer, this);
            this.addPlayer();
        }
        PlayerPlay.prototype.addPlayer = function () {
            this._player = new Snow.Player(this._game, 0, 0, "");
            this._player.x = 400; // this._player.width - 400
            this._player.y = 300;
            this.add(this._player);
            this.lastPos = new Phaser.Point(400, 300);
            this._leftRight = this.player.x;
            this._player.body.setSize(60, 20, -100, 0);
            this._player.forward();
        };
        PlayerPlay.prototype.getPlayer = function () {
            return this._player;
        };
        PlayerPlay.prototype.movePlayer = function (pointer) {
            if (!this._gameControler.collisionPlayer && !this._stop && !this._stopTween) {
                //  this._stopTween = true;
                if (this._leftRight < pointer.x) {
                    this._player.right();
                }
                else {
                    this._player.left();
                }
                this._leftRight = pointer.x;
                var posy = pointer.y;
                // if (posy < 300) posy = 300;
                if (posy > 500)
                    posy = 500;
                // if (pointer.x > 630) return;
                this.tweenStop();
                posy = Math.floor(posy);
                var posx = Math.floor(pointer.x);
                if (posx > 890)
                    posx = 800;
                else if (posx < 100)
                    posx = 100;
                //    if (posx < 0) posx = 0;
                //   if (posy < 0) posy = 0;
                var ok = false;
                if (this.lastPos != null && this.grannicaKlikania(new Phaser.Point(posx, posy))) {
                    this.lastPos = new Phaser.Point(posx, posy);
                    ok = true;
                }
                console.log("pozycja", posx, posy, this._tween, this._player.x, this._player.y);
                if (!this._gameControler.collisionPlayer && ok) {
                    var duration = (this.game.physics.arcade.distanceToPointer(this._player, pointer) / 200) * 1000;
                    this._tween = this.game.add.tween(this._player).to({ y: Math.floor(posy), x: Math.floor(posx) }, duration, Phaser.Easing.Linear.None).start();
                    this._tween.onComplete.add(this.endMove, this);
                }
            }
        };
        PlayerPlay.prototype.grannicaKlikania = function (pos) {
            if (pos.x != this.lastPos.x) {
                if (pos.y != this.lastPos.y) {
                    return true;
                }
            }
            return false;
        };
        PlayerPlay.prototype.endMove = function () {
            // this._player.forward();
            this._stopTween = false;
        };
        PlayerPlay.prototype.changePos = function (pointer) {
            console.log("zmiana pozycji", pointer);
            this.tweenStop();
            if (!this._gameControler.collisionPlayer) {
                // var duration = (this.game.physics.arcade.distanceToPointer(this._player, pointer) / 200) * 1000;
                this._tween = this.game.add.tween(this._player).to({ y: Math.floor(pointer.y), x: Math.floor(pointer.x) }, 2, Phaser.Easing.Linear.None).start();
                this._tween.onComplete.add(this.endMove, this);
            }
        };
        PlayerPlay.prototype.tweenStop = function () {
            if (this._tween != null) {
                console.log("tween player stop");
                this._tween.stop();
            }
        };
        PlayerPlay.prototype.stopTween = function () {
            console.log("usuwam tween", this._tween != null, this._tween.isRunning);
            if (this._tween != null && this._tween.isRunning) {
                this.game.tweens.remove(this._tween);
            }
        };
        Object.defineProperty(PlayerPlay.prototype, "player", {
            get: function () {
                return this._player;
            },
            enumerable: true,
            configurable: true
        });
        PlayerPlay.prototype.upadte = function () {
            console.log("czy grupa ma update??");
        };
        return PlayerPlay;
    })(Phaser.Group);
    Snow.PlayerPlay = PlayerPlay;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var PopUp = (function (_super) {
        __extends(PopUp, _super);
        function PopUp(game, level, x, y) {
            _super.call(this, game, x, y);
            this.level1 = level;
            this._game = game;
            var bgPopup = new Phaser.Sprite(this.game, 0, 0, "popup");
            this.addChild(bgPopup);
            this.logo = new Phaser.Sprite(this.game, 0, 0, "logoGame");
            this.addChild(this.logo);
            this.logo.x = (bgPopup.width / 2 - this.logo.width / 2);
            this.logo.y = 70;
            var text = "Good try.\n Keep playing to win credits";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this.game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;
            textDescription.x = 400 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 140;
            this.addChild(textDescription);
            this.textLevel();
            //this.beatText();
            this.addButtons();
            this.visible = false;
        }
        PopUp.prototype.addButtons = function () {
            this.gamesBtn = new Phaser.Button(this._game, 0, 0, "games", this.gamesBtnClick, this, 1, 0);
            this.playBtn = new Phaser.Button(this._game, 100, 0, "playGame", this.playBtnClick, this, 1, 0);
            this.gamesBtn.inputEnabled = true;
            this.gamesBtn.input.priorityID = 10;
            this.playBtn.inputEnabled = true;
            this.playBtn.input.priorityID = 10;
            var buttonsGr = new Phaser.Group(this._game);
            this.addChild(buttonsGr);
            buttonsGr.add(this.gamesBtn);
            buttonsGr.add(this.playBtn);
            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;
        };
        PopUp.prototype.activeButton = function () {
            this.gamesBtn.inputEnabled = true;
            this.playBtn.inputEnabled = true;
        };
        PopUp.prototype.gamesBtnClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        PopUp.prototype.playBtnClick = function () {
            this.visible = false;
            this.gamesBtn.inputEnabled = false;
            //   this.gamesBtn.input.priorityID = 3;
            this.playBtn.inputEnabled = false;
            // this.playBtn.input.priorityID = 3;
            this.level1.hideScreenAfterFinishPlaye();
            //  this.game.state.start("PopupStart", true, false);
        };
        PopUp.prototype.textLevel = function () {
            var text = "GAME OVER";
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this._game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = 400 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 25;
            this.addChild(textDescription);
        };
        return PopUp;
    })(Phaser.Sprite);
    Snow.PopUp = PopUp;
})(Snow || (Snow = {}));
function launchGame(result) {
    userLevel = result.level;
    if (userLevel >= 5) {
        userLevel = 5;
        APIsetLevel(userID, gameID, userLevel);
    }
}
//declare function launchGame(result);
var Snow;
(function (Snow) {
    var PopupStart = (function (_super) {
        __extends(PopupStart, _super);
        function PopupStart() {
            _super.apply(this, arguments);
        }
        PopupStart.prototype.create = function () {
            var background = this.add.sprite(0, 0, 'mainMenuBg');
            var bgPopup = this.add.sprite(0, 0, "popup");
            this.logo = this.add.sprite(0, 0, "logoGame");
            this.logo.x = (bgPopup.width / 2 - this.logo.width / 2);
            this.logo.y = 70;
            var text = "Pay close attention to the design on the skateboard.\n";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;
            textDescription.x = 400 - textDescription.wordWrapWidth / 2 + 30;
            textDescription.y = this.logo.y + this.logo.height + 145;
            this.textLevel();
            //this.beatText();
            this.addButtons();
        };
        PopupStart.prototype.addButtons = function () {
            var gamesBtn = this.add.button(0, 0, "games", this.gamesBtnClick, this, 1, 0);
            var playBtn = this.add.button(100, 0, "playGame", this.playBtnClick, this, 1, 0);
            var buttonsGr = this.add.group();
            buttonsGr.add(gamesBtn);
            buttonsGr.add(playBtn);
            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;
        };
        PopupStart.prototype.gamesBtnClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        PopupStart.prototype.playBtnClick = function () {
            this.game.state.start("Instruction", true, false);
        };
        PopupStart.prototype.beatText = function () {
            var text = "BEAT THIS LEVEL";
            //" 6";
            var style = { font: "bold 13px Arial", fill: "#660000", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = (this.game.width / 2) + (140) - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 100;
            var numberText = "6";
            var styleNumber = { font: "bold 17px Arial", fill: "#990000", align: "center" };
            var numberShow = this.add.text(0, 0, numberText, styleNumber);
            numberShow.x = textDescription.x + textDescription.width / 2 - numberShow.width / 2;
            numberShow.y = textDescription.y + textDescription.height;
            var tunesText = "MORE TUNES TO WIN";
            var tunesShow = this.add.text(0, 0, tunesText, style);
            tunesShow.wordWrap = true;
            tunesShow.wordWrapWidth = 100;
            tunesShow.x = textDescription.x + textDescription.width / 2 - tunesShow.width / 2;
            tunesShow.y = numberShow.y + numberShow.height;
            var creditsNmText = "25";
            var styleCredits = { font: "bold 17px Arial", fill: "#ff0000", align: "center" };
            var credistNmShow = this.add.text(0, 0, creditsNmText, styleCredits);
            credistNmShow.x = tunesShow.x + tunesShow.width / 2 - credistNmShow.width / 2;
            credistNmShow.y = tunesShow.y + tunesShow.height;
            var creditsText = "CREDITS";
            var credistShow = this.add.text(0, 0, creditsText, style);
            credistShow.x = tunesShow.x + tunesShow.width / 2 - credistShow.width / 2;
            credistShow.y = credistNmShow.y + credistNmShow.height;
        };
        PopupStart.prototype.textCredits = function () {
            var text = "Beat this level";
            //" 6";
            var style = { font: "bold 14px Arial", fill: "#660000", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = (this.game.width / 2) + 257 / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 100;
            var numberText = "6";
            var styleNumber = { font: "bold 18px Arial", fill: "#990000", align: "center" };
            var numberShow = this.add.text(0, 0, numberText, styleNumber);
            numberShow.x = textDescription.x + textDescription.width / 2 - numberShow.width / 2;
            numberShow.y = textDescription.y + textDescription.height;
        };
        PopupStart.prototype.textLevel = function () {
            //APIgetLevel(userID, gameID);
            var text = "LEVEL " + userLevel;
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = this.game.stage.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 25;
        };
        return PopupStart;
    })(Phaser.State);
    Snow.PopupStart = PopupStart;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var PopUpWin = (function (_super) {
        __extends(PopUpWin, _super);
        function PopUpWin(game, level, x, y) {
            _super.call(this, game, x, y);
            this._game = game;
            this.level1 = level;
            var bgPopup = new Phaser.Sprite(this.game, 0, 0, "popup");
            this.addChild(bgPopup);
            this.logo = new Phaser.Sprite(this.game, 0, 0, "logoGame");
            this.addChild(this.logo);
            this.logo.x = (400 - this.logo.width / 2);
            this.logo.y = 70;
            var text = "You finish the level.\n Keep Playing to win credits!";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this.game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;
            textDescription.x = 400 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 140;
            this.addChild(textDescription);
            this.textLevel();
            //this.beatText();
            this.addButtons();
            this.visible = false;
        }
        PopUpWin.prototype.addButtons = function () {
            this.gamesBtn = new Phaser.Button(this._game, 0, 0, "games", this.gamesBtnClick, this, 1, 0);
            this.playBtn = new Phaser.Button(this._game, 100, 0, "playGame", this.playBtnClick, this, 1, 0);
            this.gamesBtn.inputEnabled = true;
            this.gamesBtn.input.priorityID = 10;
            this.playBtn.inputEnabled = true;
            this.playBtn.input.priorityID = 10;
            var buttonsGr = new Phaser.Group(this._game);
            this.addChild(buttonsGr);
            buttonsGr.add(this.gamesBtn);
            buttonsGr.add(this.playBtn);
            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;
        };
        PopUpWin.prototype.activeButton = function () {
            this.gamesBtn.inputEnabled = true;
            this.playBtn.inputEnabled = true;
        };
        PopUpWin.prototype.gamesBtnClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        PopUpWin.prototype.playBtnClick = function () {
            this.visible = false;
            this.gamesBtn.inputEnabled = false;
            this.gamesBtn.input.priorityID = 3;
            this.playBtn.inputEnabled = false;
            this.playBtn.input.priorityID = 3;
            this.level1.hideScreenAfterFinishPlaye();
            //  this.game.state.start("PopupStart", true, false);
        };
        PopUpWin.prototype.textLevel = function () {
            var text = "GOOD JOB";
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this._game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = 400 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 25;
            this.addChild(textDescription);
        };
        return PopUpWin;
    })(Phaser.Sprite);
    Snow.PopUpWin = PopUpWin;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var PromptView = (function (_super) {
        __extends(PromptView, _super);
        function PromptView(game, parent) {
            _super.call(this, game);
            game.add.existing(this);
            this._parent = parent;
            this._kayak = this.game.add.sprite(0, 0, 'prompt', 0);
            // this._kayak.animations.add('anim');
            this._kayak.scale.setTo(0.6, 0.6);
            this.addChild(this._kayak);
            var style = { font: "18px Arial", fill: "#FFFFFF", align: "left" };
            this._textView = this.game.add.text(17, 75, "", style);
            this.addChild(this._textView);
            this.visible = false;
            this.y = this.game.height - this.height;
            this.x = this.game.width - this.width;
        }
        PromptView.prototype.playIntro = function () {
            this._parent.blokada(true);
            this.visible = true;
            // this._kayak.animations.play('anim', 31, true, false);
            this._textView.setText("Get ready to shred! Collect the red letters only.\n remember the order and sounds of the letters, \nand then combine them to make a word.\n be careful don't wipe out on anything!!");
            this._audio = this.game.add.audio('WakeIntro', 1, false);
            this._audio.play();
            this._audio.onStop.addOnce(this.onAudioComplete, this);
        };
        PromptView.prototype.playFeedbackGood = function (nextLevel) {
            if (nextLevel === void 0) { nextLevel = false; }
            if (!nextLevel) {
                this._parent.blokada(true);
                this.visible = true;
                this._kayak.animations.play('anim', 31, true, false);
                this._textView.setText("Holy Moley!  I'm Impressed!\nThe Next Sequence is...");
                this._audio = this.game.add.audio('WakeSuccess0', 1, false);
                this._audio.play();
                this._audio.onStop.addOnce(this.onAudioComplete, this);
            }
            else {
                this._parent.showFinishLevel();
            }
        };
        PromptView.prototype.playFeedbackWrong = function (end) {
            if (end === void 0) { end = false; }
            this._parent.blokada(true);
            this.visible = true;
            this._kayak.animations.play('anim', 31, true, false);
            this._textView.setText("Good effort, that was close!\n Let's take another listen as you collect the letters.");
            this._audio = this.game.add.audio('WakeWrong0', 1, false);
            this._audio.play();
            this._audio.onStop.addOnce(this.onAudioComplete, this);
            if (end) {
                this._audio.onStop.addOnce(this.onAudioCompleteEnd, this);
            }
            else {
                this._audio.onStop.addOnce(this.onAudioComplete, this);
            }
        };
        PromptView.prototype.onAudioComplete = function () {
            this.visible = false;
            this._parent.onPromptAudioComplete();
        };
        PromptView.prototype.onAudioCompleteNextLevel = function () {
            this.visible = false;
            this._parent.showFinishLevel(true);
        };
        PromptView.prototype.onAudioCompleteEnd = function () {
            this.visible = false;
            this._parent.showFinish();
        };
        return PromptView;
    })(Phaser.Group);
    Snow.PromptView = PromptView;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var Sequence = (function (_super) {
        __extends(Sequence, _super);
        function Sequence(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this._collectLetters = new Array();
            this._gameControler = Snow.GameControler.getInstance();
            this._contenerLetter = new Phaser.Group(game);
            this._contenerLetter.y = 15;
            this._contenerLetter.x = 0;
            this.addChild(this._contenerLetter);
        }
        Sequence.prototype.addLetter = function (letter) {
            var style = { font: "40px Arial", fill: "#000000", align: "center" };
            var letterShow = new Phaser.Text(this.game, 0, 0, letter, style);
            this._contenerLetter.removeAll();
            letterShow.x = this._contenerLetter.width + 20;
            this._contenerLetter.addChild(letterShow);
            this._collectLetters.push(letter);
            console.log("ile liter posiadam", this._collectLetters.length, this._gameControler.currentLevel);
            if (this._collectLetters.length >= this._gameControler.currentLevel) {
                var arr = this._collectLetters.slice(0, this._collectLetters.length);
                console.log("collect letters", arr);
                this._gameControler.level.showLetterMenu(arr);
                this.removeLetters();
            }
        };
        Sequence.prototype.removeLetters = function () {
            this._contenerLetter.removeAll();
            this._collectLetters.splice(0, this._collectLetters.length);
        };
        return Sequence;
    })(Phaser.Sprite);
    Snow.Sequence = Sequence;
})(Snow || (Snow = {}));
var Snow;
(function (Snow) {
    var ShowLetter = (function (_super) {
        __extends(ShowLetter, _super);
        function ShowLetter(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this._gameControler = Snow.GameControler.getInstance();
            this._letterControler = Snow.LetterControler.getInstance();
            this.y = this.game.height / 2 - this.height / 2;
            this.x = this.game.width / 2 - this.width / 2;
        }
        ShowLetter.prototype.showLetters = function () {
            if (this._currentMax == this._currentCount) {
                console.log("hideMe");
                this.visible = false;
                this._gameControler.collisionPlayer = false;
                this._gameControler.level.stopGame(false);
                this._gameControler.level.blokada(false);
            }
            else {
                this._gameControler.level.blokada(true);
                this.showLetter(this._letterControler.currentLetters[this._currentCount]);
                this.game.time.events.add(Phaser.Timer.SECOND * 2, function () {
                    this.removeChild(this._letterShow);
                }, this);
                this.game.time.events.add(Phaser.Timer.SECOND * 2.5, function () {
                    this.showLetters();
                }, this);
                this._currentCount++;
            }
        };
        ShowLetter.prototype.addBgToDisplayLetter = function () {
            this._currentMax = this._letterControler.currentLetters.length;
            this._currentCount = 0;
            this.showLetters();
        };
        ShowLetter.prototype.showLetter = function (str) {
            var style = { font: "120px Arial", fill: "#000000", align: "center" };
            if (this._letterShow)
                this._letterShow.destroy();
            this._letterShow = new Phaser.Text(this.game, 0, 0, str, style);
            this._letterShow.x = this.width / 2 - this._letterShow.width / 2;
            this._letterShow.y = this.height / 2 - this._letterShow.height / 2;
            this.addChild(this._letterShow);
        };
        ShowLetter.prototype.playAgain = function () {
            // this._gameControler.hideLetterOnGame = true;
            // this._gameControler.hideLetterOnGame = true;
            //  this.failArr[this._gameControler._badLetters].onStop.add(this.playAgainAfterSound, this);
            //  this.failArr[this._gameControler._badLetters].play();
        };
        return ShowLetter;
    })(Phaser.Sprite);
    Snow.ShowLetter = ShowLetter;
})(Snow || (Snow = {}));
//# sourceMappingURL=game.js.map