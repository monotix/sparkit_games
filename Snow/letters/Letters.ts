﻿module Snow {

    export class Letters extends Phaser.Sprite {

        private alphabet: Array<string> = new Array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'che', 'er', 'oo', 'ow', 'oy', 'qu', 'sh', 'th', 'aw', 'th');


        private words: Array<any> = [
            { a: new Array({ a: "s<a>t" }) },
            { b: [{ b: "<b>ust" }, { bb: "bu<bb>le" }] },
            { c: [{ c: "<c>at" }, { k: "<k>ink" }, { ck: "lu<ck>" }, { ch: "<ch>rome" }] },
            { d: [{ d: "<d>id" }, { dd: "la<dd>er" }, { ed: "wast<ed>" }] },
            { e: [{ e: "t<e>n" }, { ea: "h<ea>d" }, { ai: "s<ai>d" }, { ie: "fr<ie>nd" }] },
            { f: [{ f: "<f>an" }, { ff: "flu<ff>" }, { ph: "<ph>one" }, { gh: "rou<gh>" }] },
            { g: [{ g: "ba<g>" }, { gg: "gi<gg>le" }, { ph: "<gh>ost" }]},
            { h: [{ h: "<h>am" }, { wh: "<wh>o" }] },
            { i: [{ i: "<i>t" }, { y: "cr<y>pt" }]},
            { j: [{ j: "<j>am" }, { g: "<g>em" }, { ge: "lar<ge>" }, { dge: "bri<dge>" }] },
            { k: [{ k: "<k>itty" }, { c: "<c>ut" }, { ck: "lu<ck>" }, { ch: "<ch>ristmas" }] },
            { l: [{ l: "<l>it" }, { ll: "pi<ll>" }, { le: "tab<le>" }, { el: "lab<el>" }, { il: "pup<il>" }, { al: "sab<al>" }] },
            { m: [{ m: "<m>utt" }, { mm: "su<mm>er" }, { mb: "du<mb>" }, { mn: "autu<mn>" }] },
            { n: [{ n: "<n>ot" }, { nn: "wi<nn>er" }, { kn: "<kn>ot" }, { gn: "<gn>aw" }, { pn: "<pn>eumonia" }] },
            { o: [{ o: "p<o>t" }, { a: "f<a>ther" }] },
            { p: [{ p: "po<p>" }, { pp: "pa<pp>y" }] },
            { r: [{ r: "<r>un" }, { wr: "<wr>ote" }, { rr: "ma<rr>y" }, { rh: "<rh>ino" }, { re: "the<re>" }] },
            { s: [{ s: "<s>at" }, { se: "mou<se>" }, { ss: "bra<ss>" }, { c: "<c>ider" }, { ce: "choi<ce>" }, { st: "whi<st>le" }, { sc: "<sc>ience" }] },
            { t: [{ t: "<t>an" }, { tt: "mi<tt>" }, { bt: "dou<bt>" }, { pt: "<pt>eradactyle" }] },
            { u: [{ u: "r<u>b" }, { ou: "t<ou>gh" }, { a: "<a>mount" }] },
            { v: [{ v: "<v>an" }, { ve: "lea<ve>" }] },
            { w: [{ w: "<w>inter" }, { wh: "<wh>ere" }] },
            { x: [{ x: "mi<x>" }] },
            { y: [{ y: "<y>ellow" }] },
            { z: [{ z: "<z>ipper" }, { zz: "bu<zz>er" }, { s: "hi<s>" }, { l: "lo<se>" }, { ze: "oo<ze>" }, { x: "<x>ylophone" }] },
            { ch: [{ w: "<ch>imp" }, { tch: "ca<tch>" }] },
            { ee: [{ ee: "tr<ee>" }, { ea: "<ea>t" }, { ie: "br<ie>f" }, { y: "sunn<y>" }, { e: "h<e>" }, { ie: "pet<i>t<e>" }, { i: "ind<i>an" }, { ei: "rec<ei>ve" }, { ee: "p<e>t<e>" }, { ey: "monk<ey>" }] },
            { er: [{ er: "h<er>" }, { ur: "b<ur>n" }, { ir: "b<ir>d" }, { w: "w<or>m" }, { ear: "<ear>n" }, { yr: "s<yr>up" }, { ar: "collar" }] },
            { ee: [{ ee: "tr<ee>" }, { ea: "<ea>t" }, { ie: "br<ie>f" }, { y: "sunn<y>" }, { e: "h<e>" }, { ie: "pet<i>t<e>" }, { i: "ind<i>an" }, { ei: "rec<ei>ve" }, { ee: "p<e>t<e>" }, { ey: "monk<ey>" }] },
            { oo: [{ oo: "l<oo>t" }, { ue: "gl<ue>" }, { ew: "fl<ew>" }, { u: "s<u>per" }, { ui: "fr<ui>t" }, { ue: "t<u>n<e>" }, { ou: "gr<ou>p" }, { sh: "sh<oe>" }, { o: "t<o>" }] },
            { oo: [{ oo: "l<oo>k" }, { uol: "c<oul>d" }, { u: "p<u>t" }] },
            { ow: [{ ow: "t<ow>n" }, { ou: "<ou>t" } ] },
            { oy: [{ oy: "t<oy>" }, { oi: "<oi>l" }] },
            { qu: [{ qu: "<qu>iet" }] },
            { sh: [{ sh: "<sh>ip" }, { ch: "mi<ch>gan" }, { s: "<s>ure" }] },
            { ue: [{ ue: "c<u>t<e>" }, { u: "p<u>pil" }, { ew: "kn<ew>" }, { ue: "f<ue>l" }] },
         
            { ie: [{ ie: "b<i>t<e>" }, { ie: "tr<ie>d" }, { i: "ch<i>ld" }, { igh: "f<igh>t" }, { y: "m<y>" }, { eigh: "h<eigh>t" }] },
            { ae: [{ ae: "<a>t<e>" }, { ai: "tr<ai>n" }, { ay: "st<ay>" }, { ea: "br<ea>k" }, { ey: "th<ey>" }, { eigh: "sl<eigh>" }, { a: "t<a>ble" }, { ei: "r<ei>gn" }, { ei: "str<aigh>t" }] },
            { aw: [{ al: "w<al>k" }, { aw: "w<al>k" }, { au: "<au>gust" }, { o: "fr<o>g" }] }
            

          
          



            ];

        private _wordsGr: Phaser.Group;

        private _objectToDrag;

        private _objectsToDropGr: DropLetter;


        private _dropLetter:DropLetter

        public _arrAnswear: Array<string> = new Array("a", "b", "c", "d");
        private _successArr: Array<Phaser.Sprite>;
        private _parent: Level;
        public widthScreen: number = 0;
        public heightScreen:number = 0 
        constructor(game: Phaser.Game, x: number, y: number, key: string, lettersArr:Array<string>,parent_:Level) {

            super(game, 0, 0, "");
            this._parent = parent_;
            this._parent._isShowEndScrren = true;
            this._successArr = new Array(new Phaser.Sprite(this.game, 0, 0, "letterSuccess1", 0), new Phaser.Sprite(this.game, 0, 0, "letterSuccess2", 0), new Phaser.Sprite(this.game, 0, 0, "letterSuccess3", 0), new Phaser.Sprite(this.game, 0, 0, "letterSuccess4", 0));
            this._arrAnswear = lettersArr;
            console.log("zebrane litery to ", this._arrAnswear);
            //this.game.add.sprite(0, 0, "bg");
            var bg = new Phaser.Sprite(this.game, 0, 0, "bg");
            this.addChild(bg);
            this.widthScreen = bg.width;
            this.heightScreen = bg.height;
      //      this._dropLetter = new DropLetter(this.game);
        //    this.addChild(this._dropLetter);

            this._objectsToDropGr = new DropLetter(this.game, this, this._arrAnswear.length);
            this._objectsToDropGr.y = 0;
            this._objectsToDropGr.enableBody = true;
            this.addChild(this._objectsToDropGr);

            this._wordsGr = this.game.add.group();
            this._wordsGr.y = 100;
            this._wordsGr.x = 330;
            this.addChild(this._wordsGr); 
            this.makeLetter();

            this.game.input.onUp.add(this.stageUp, this);

            var gameButton = new Phaser.Button(this.game, 0, 0, "clearBtn", this.onGameClear, this, 1, 0);
          
            gameButton.x = 300;
            gameButton.y = 500;
            this.addChild(gameButton);


            var gameButton = new Phaser.Button(this.game, 0, 0, "doneBtn", this.onDoneClick, this, 1, 0);
            gameButton.inputEnabled
            gameButton.x = 500;
            gameButton.y = 500;
            this.addChild(gameButton);
            
        }


        onDoneClick(item:Phaser.Button) {
            item.inputEnabled = false;
            this._wordsGr.removeAll();
            this._objectsToDropGr.checkLetter();
        }

        onGameClear() {
            this._wordsGr.removeAll();
            this._objectsToDropGr.clear();

        }

        playAnim(animSpr:Phaser.Sprite) {

            this.addChild(animSpr);
            animSpr.x = 400;
            animSpr.y = 100;
            var anim = animSpr.animations.add("anim");
            anim.onComplete.add(function () { this.game.time.events.add(Phaser.Timer.SECOND * 2, this.animationStopped, this) }, this);
            animSpr.animations.play("anim", 24, false, false);

        }

        playAnim2(animSpr: Phaser.Sprite) {

            this.addChild(animSpr);
            animSpr.x = 400;
            animSpr.y = 100;
            var anim = animSpr.animations.add("anim");
            anim.onComplete.add(function () { this.game.time.events.add(Phaser.Timer.SECOND * 2, this.animationStopped2, this) }, this);
            animSpr.animations.play("anim", 24, false, false);

        }

        animationStopped() {

            this._parent.youwinSequence();
         //   this._parent.stopGame(false);
            this.kill();
            this.visible = false;

        }

        animationStopped2() {

            this._parent.youLose();
         
            this.kill();
            this.visible = false;

        }

        youWin() {
            this.playAnim(this._successArr[this.randomNum(this._successArr.length,0)]);


          
           
          
          
        }

        youLose() {
         
            this.playAnim2(new Phaser.Sprite(this.game, 0, 0, "fail1", 0));
        }

        makeLetter() {
            var stalaY: number = 100;
            var posx: number = 0;
            var posy: number = 0;
            var licz: number = 1;
            for (var i: number = 0; i < this.alphabet.length; i++) {

                var letter: Letter = new Letter(this.game, this.alphabet[i],1);
                letter.inputEnabled = true;
                letter.input.useHandCursor = true;
                letter.events.onInputDown.add(this.letterOnDown, this);
                letter.x = posx;
                letter.y = posy + stalaY;
                this.addChild(letter);
                posx += 50 + 10;
                console.log(posx);
                if (licz % 5 == 0 && licz != 0) {
                    posy += 60;
                    posx = 0;
                }
                licz++;
            }
            console.log("ile", i);

            var letter1:PlaceToDrop = new PlaceToDrop(this.game);
          //  letter.inputEnabled = true;
         //   this._objectsToDropGr.add(letter);
            
            //var letter2: Letter = new Letter(this.game, "");
          //  letter2.x = 100;
        //    this._objectsToDropGr.add(letter2);

            
        }


        letterOnDown(letter) {

          
            for (var i: number = 0; i < this.words.length; i++) {

                for (var name in this.words[i]) {
                   
                    if (name == letter.name) {
                      
                       
                        this.drawWords(this.words[i][name]);

                    }
                   
                }
            }
        }


        drawWords(arr: Array<any>) {


            this._wordsGr.forEach(function (item) {

                item.kill();

            }, this);

            var posX:number = 0;
            var posY: number = 0;
            for (var i: number = 0; i < arr.length; i++) {

             //   console.log("gra", arr[i]);

                for (var name in arr[i]) {

                  
                  //      console.log("nazwa ? ", name);
                    //    console.log(arr[i][name]);
                        var letter = new LetterWord(this.game, name);
                        letter.events.onInputDown.add(this.check, this);
                        
                        
                        letter.y = posY;
                        letter.x = posX;
                        this._wordsGr.add(letter);
                        var word: Word = new Word(this.game, arr[i][name]);
                        word.x = letter.widthLetter + 15 + posX;
                        word.y = posY;
                        posY += word.height + 30;
                        this._wordsGr.add(word);
                       // this.drawWords(this.words[i][name]);
                        if (i == 5) {
                            posY = 0;
                            posX = 200;
                        }

                }

            }


        }

        check(item: LetterWord) {
           
            var spr = item.addObjectToDrag();
            spr.anchor.set(0.5, 0.5);
       
            this.stage.addChild(spr);
          
            this._objectToDrag = spr;
          
            this.game.physics.enable(spr, Phaser.Physics.ARCADE);


        }

        checkSecondTime(item: LetterWord) {

            item.isAdded = false;
            item.placeToDrop.isAdded = false;
            var spr = item;
            this.stage.addChild(spr);

            this._objectToDrag = spr;

            this.game.physics.enable(spr, Phaser.Physics.ARCADE);


        }


        stageUp() {
          
            this.game.physics.arcade.overlap(this._objectToDrag, this._objectsToDropGr, this.collision,null, this);
         
            // if (this._objectToDrag != null) this.checkIfNotAdded(this._objectToDrag);
            this.checkIfNotAdded(this._objectToDrag);
        }

        checkIfNotAdded(object = null) {


            if (object != null && !object.isAdded) {

                console.log("object niedodany ");
                object.kill();

            }
            else {
                console.log("object dodany ");
               
            }

        }

        collision(object,object2) {

            console.log("kolizja");
           // this._objectsToDropGr.add(object);
            //object.x = object2.x;
           // object.y = object2.y;
            if (!object2.isAdded) {
                object2.addLetter(object);
                //   object.x = 0;
                //  object.y = 0;
                object.events.onInputDown.add(this.checkSecondTime, this);
                //  object2.addChild(object);
                object.isAdded = true;

            }
            else {
                object.kill();

            }
           
            this._objectToDrag = null;

        }

        update() {


            if (this._objectToDrag != null) {
                // var pos = this.stage.position.x
               
                this._objectToDrag.position.setTo(this.game.input.x, this.game.input.y);

            }

        }

        public randomNum(max: number, min: number = 0): number {

            return Math.floor(Math.random() * (max - min) + min);
        }
    }

}
     