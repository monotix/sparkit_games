﻿module Snow {

    export class DropLetter extends Phaser.Group {

        private bgDrop: Phaser.Sprite;
        private _dropGr: Phaser.Group;
        private _arr: Array<PlaceToDrop>;
        private _parent: Letters;
        public widthLetter: number = 0;
        constructor(game: Phaser.Game, parent:Letters, numerOfSlots:number) {

            super(game,parent,"drop",true,true);
            this._parent = parent;
            this.bgDrop = new Phaser.Sprite(game, 0, 0, "bgDrop");
            this.addChild(this.bgDrop);
            this._arr = new Array();

            var posx = numerOfSlots * 50;

            posx = 400 - posx / 2;


            for (var i: number = 0; i < numerOfSlots; i++) {
                var big: boolean = (this._parent._arrAnswear[i].length > 2) ? true : false;
                var let1: PlaceToDrop = new PlaceToDrop(game,big);
                this._arr.push(let1);

                let1.x = posx + i * 50;
                let1.y = 10;
                this.add(let1);
            }
           

            var let1: PlaceToDrop = new PlaceToDrop(game);
            
          //  this.add(let1);
            let1.x = 350;
            let1.y = 10;
            var let2: PlaceToDrop = new PlaceToDrop(game);
            let2.x = let1.x + let1.width + 15;
            let2.y = let1.y;
           // this.add(let2);

            var let3: PlaceToDrop = new PlaceToDrop(game);
            let3.x = let2.x + let2.width + 15;
           // this.add(let3);

            var let4: PlaceToDrop = new PlaceToDrop(game);
            let4.x = let3.x + let3.width + 15;

           // this.add(let4);

          
          //  this._arr = new Array(let1);


        }


        public clear() {


            for (var i: number = 0; i < this._arr.length; i++) {

                this._arr[i].clear();

            }


        }


        public checkLetter() {
            var licz: number = 0;
            for (var i: number = 0; i < this._arr.length; i++) {

                if (this._arr[i].isAdded) {
                   //console.log( this._arr[i]._letter.letterString());
                   console.log(this._arr[i]._letter.letterString(), this._parent._arrAnswear[i]);
                   if (this._arr[i]._letter.letterString() == this._parent._arrAnswear[i]) {

                     
                       licz++;

                   }
                   else {

                   }
                }

                
            }

            if (licz == this._parent._arrAnswear.length) {
                this._parent.youWin();
                console.log("you win");
            }
            else {
                console.log("you lose");
                this._parent.youLose();
            }

        }

    }


}
   
   
    