﻿module Snow {

    export class LetterWord extends Phaser.Sprite {


        private _letter: Phaser.Sprite;
        private _letterStr: string;
        public isAdded: boolean = false;
        public placeToDrop: PlaceToDrop;
        public widthLetter: number = 0;
        constructor(game: Phaser.Game, letter: string) {

            super(game, 0, 0, "");

            this.name = letter;
            var bg = new Phaser.Sprite(this.game, 0, 0, "szare");
            if (letter.length > 2) {
                bg = new Phaser.Sprite(this.game, 0, 0, "szared");
            }
            if (letter.length > 3) {
                bg = new Phaser.Sprite(this.game, 0, 0, "szared2");
            }
            this._letter = new Phaser.Sprite(game, 0, 0, "");
            this._letter.addChild(bg);
            this.addChild(this._letter);

            var color: string = "#000000";
            var style = { font: "25px Arial", fill: color, align: "left" };
            console.log("log", bg.width);
            var _letterShow = new Phaser.Text(this.game, 0, 0, letter.toUpperCase(), style);
            //    _letterShow.x = this._letter.width / 2 - _letterShow.width / 2;
            //    _letterShow.y = this._letter.height / 2 - _letterShow.height / 2;

            this._letterStr = letter;
            while (_letterShow.width > bg.width - 5 && _letterShow.fontSize > 0) {
                _letterShow.fontSize--;

            }
            while (_letterShow.height > bg.height - 5 && _letterShow.fontSize > 0) {
                _letterShow.fontSize--;

            }
            _letterShow.x = bg.width / 2 - _letterShow.width / 2 - 2;
            _letterShow.y = bg.height / 2 - _letterShow.height / 2 + 2 ;
            this._letter.addChild(_letterShow);
            this.inputEnabled = true;
          //  this.input.enableDrag();
            this.widthLetter = bg.width;

        }

        addObjectToDrag(): LetterWord {



            var spr = new LetterWord(this.game, this._letterStr);
            spr.anchor.setTo(0.5, 0.5);

            return spr;

        }

        public letterString(): string {

            return this._letterStr;
        }

    }


}  