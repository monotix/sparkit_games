﻿module Snow {

    export class PlaceToDrop extends Phaser.Sprite {


        private _bg: Phaser.Sprite;
        public isAdded: boolean = false;
        public _letter: LetterWord;
        
        constructor(game: Phaser.Game,big:boolean=false) {

            super(game, 0, 0, "");

            this._bg = new Phaser.Sprite(game, 0, 0, "szare");
            if (big) this._bg = new Phaser.Sprite(game, 0, 0, "szared");
            this.addChild(this._bg);
            

        }


        public addLetter(letter:LetterWord) {

            if (!this.isAdded) {
                this._letter = letter;
                this._letter.x = 0;
                this._letter.y = 0;
                this.addChild(this._letter);
                this._letter.placeToDrop = this;
                this.isAdded = true;
            }
           
        }

        public clear() {
            if (this._letter != null) {

                this.removeChild(this._letter);
                this.isAdded = false;
                this._letter.isAdded = false;
                this._letter.kill();

            }

        }



    }

} 