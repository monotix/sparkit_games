﻿module Snow {

    export class Boot extends Phaser.State {

        preload() {


            this.load.image("bg", "assets/SplashScreen.png");
            this.load.image("progressBarBlack", "assets/preloader/ProgressBar.png");
            this.load.image("progressBarBlue", "assets/preloader/ProgressBarBlue.png");

        }

        create() {

            
            this.game.state.start("Preloader", true, false);

        }

    }



} 