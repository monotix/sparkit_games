var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Snow;
(function (Snow) {
    var MainMenu = (function (_super) {
        __extends(MainMenu, _super);
        function MainMenu() {
            _super.apply(this, arguments);
        }
        MainMenu.prototype.create = function () {
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var _mainMenuBg = this.game.add.audio('MainMenuSn', 1, true);
            _mainMenuBg.stop();
            var background = this.add.sprite(0, 0, 'mainMenuBg');
            var _buttons = this.add.group();
            var buttonsBg = this.add.sprite(0, 0, 'buttonsBg');
            _buttons.addChild(buttonsBg);
            var gameButton = this.add.button(14, 36, 'buttons', null, this, 0, 0, 1);
            _buttons.addChild(gameButton);
            var playButton = this.add.button(61, gameButton.y, 'buttons', this.onPlayClick, this, 3, 2);
            playButton.onInputDown.add(this.overPlay2, this);
            _buttons.addChild(playButton);
            _buttons.x = 0;
            _buttons.y = this.game.stage.height - _buttons.height;
        };
        MainMenu.prototype.overPlay2 = function () {
            this.overButtonA.play();
        };
        MainMenu.prototype.onPlayClick = function () {
            this.game.state.start("Instruction", true, false);
        };
        return MainMenu;
    })(Phaser.State);
    Snow.MainMenu = MainMenu;
})(Snow || (Snow = {}));
//# sourceMappingURL=MainMenu.js.map