var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Snow;
(function (Snow) {
    var Preloader = (function (_super) {
        __extends(Preloader, _super);
        function Preloader() {
            _super.apply(this, arguments);
        }
        Preloader.prototype.preload = function () {
            var bg = this.add.image(0, 0, "bg", 0);
            var blackProgress = this.add.image(0, 0, "progressBarBlack", 0);
            var blueProgress = this.add.sprite(0, 0, "progressBarBlue", 0);
            blackProgress.position.setTo(this.stage.width / 2 - blackProgress.width / 2, this.stage.height - blackProgress.height - 10);
            blueProgress.position = blackProgress.position;
            this.load.setPreloadSprite(blueProgress);
            this.load.image("mainMenuBg", "assets/mainMenu/SplashScreen.png");
            this.load.atlasJSONArray('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');
            this.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
            this.load.audio("MainMenuSn", "assets/audio/ScubaDudeTheme.mp3");
            this.load.audio("overButtonA", "assets/audio/overButton.mp3");
            this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");
            this.load.image("bgGame", "assets/water/gameBackground.png");
            this.load.atlasJSONArray('boatsmedium', 'assets/water/boat/boatsmedium.png', 'assets/water/boat/boatsmedium.json');
            this.load.atlasJSONArray('boatssmall', 'assets/water/boat/boatssmall.png', 'assets/water/boat/boatssmall.json');
            /* instructions */
            this.load.atlasJSONArray('prompt', 'assets/water/prompt/prompt.png', 'assets/water/prompt/prompt.json');
            this.load.audio("WakeIntro", "assets/audio/instruction/WakeIntro.mp3");
            this.load.audio("WakeSuccess0", "assets/audio/instruction/WakeSuccess0.mp3");
            this.load.audio("WakeWrong0", "assets/audio/instruction/WakeWrong0.mp3");
            this.load.image("firstInstruction", "assets/water/instructionsScreen.png");
            this.load.image("sequence", "assets/water/sequence.png");
            /* player */
            this.load.atlasJSONArray('player', 'assets/water/player/froward.png', 'assets/water/player/forward.json');
            this.load.atlasJSONArray('jump', 'assets/water/player/jump.png', 'assets/water/player/jump.json');
            this.load.atlasJSONArray('forwardSpeed', 'assets/water/player/charging.png', 'assets/water/player/charging.json');
            this.load.atlasJSONArray('miganie', 'assets/water/player/miganie.png', 'assets/water/player/miganie.json');
            this.load.atlasJSONArray('crashforward', 'assets/water/player/crashforward.png', 'assets/water/player/crashforward.json');
            for (var i = 1; i < 7; i++) {
                this.load.atlasJSONArray('tricki' + i, 'assets/water/player/trick' + i + '.png', 'assets/water/player/trick' + i + '.json');
            }
            this.load.atlasJSONArray('special', 'assets/water/special.png', 'assets/water/special.json');
            this.load.atlasJSONArray('jumpbtn', 'assets/water/jumpBtn.png', 'assets/water/jumpBtn.json');
            this.load.image('bgletter', 'assets/water/letter/bgletter.png');
            this.load.image("bgClock", "assets/water/interface.png");
            /* obstacle */
            this.load.image("piasek", "assets/water/obstacle/piasek.png");
            this.load.image("wood", "assets/water/obstacle/wood.png");
            this.load.image("hammer", "assets/water/obstacle/hammer.png");
            this.load.image("turtle", "assets/water/obstacle/turtle.png");
            this.load.image("zielone", "assets/water/obstacle/zielone.png");
            this.load.image("ball", "assets/water/obstacle/ball.png");
            this.load.image("ball2", "assets/water/obstacle/ball2.png");
            this.load.image("walen", "assets/water/obstacle/walen.png");
            this.load.atlasJSONArray('popletter', 'assets/water/letter/popletter.png', 'assets/water/letter/popletter.json');
            this.load.atlasJSONArray('shark1', 'assets/water/obstacle/shark.png', 'assets/water/obstacle/shark.json');
            this.load.atlasJSONArray('shark2', 'assets/water/obstacle/shark2.png', 'assets/water/obstacle/shark2.json');
            this.load.atlasJSONArray('shark3', 'assets/water/obstacle/shark3.png', 'assets/water/obstacle/shark3.json');
            this.load.atlasJSONArray('shark4', 'assets/water/obstacle/shark4.png', 'assets/water/obstacle/shark4.json');
            this.load.atlasJSONArray('osmiornica', 'assets/water/obstacle/osmiornica.png', 'assets/water/obstacle/osmiornica.json');
            for (var i = 1; i < 5; i++) {
                this.load.audio("fail" + i, "assets/audio/instruction/fail" + i + ".mp3");
            }
            for (var i = 1; i < 5; i++) {
                this.load.audio("success" + i, "assets/audio/instruction/success" + i + ".mp3");
            }
            this.load.image("item11", "assets/robo/item11.PNG");
            /*    for (var i: number = 1; i < 4; i++) {
                    this.load.image("item"+i, "assets/robo/item"+i+".PNG");
                }
    
                for (var i: number = 1; i < 4; i++) {
                    this.load.image("robot" + i, "assets/robo/robot" + i + ".PNG");
                }
                */
            this.load.image("bg1", "assets/robo/stage/bg_level0.png");
            this.load.image("bg2", "assets/robo/stage/bg_level1.png");
            this.load.image("bg3", "assets/robo/stage/bg_level2.png");
            for (var i = 0; i < 13; i++) {
                this.load.image("itemDrag" + i, "assets/robo/item/s" + i + ".PNG");
            }
            this.load.image("skrzynka", "assets/robo/item/skrzynka.png");
            this.load.atlasJSONArray("skrzynkaWyglad", "assets/robo/item/shapes.png", "assets/robo/item/shapes.json");
            for (var i = 0; i < 12; i++) {
                this.load.image("robot" + i, "assets/robo/robots/robot" + i + ".png");
            }
            /*  end roboty */
            this.load.atlasJSONArray("tasmaskrzynia", "assets/robo/stage/tasma_skrzynki.png", "assets/robo/stage/tasma_skrzynki.json");
            this.load.atlasJSONArray("tasmaroboty", "assets/robo/stage/tasma.png", "assets/robo/stage/tasma.json");
            this.load.atlasJSONArray("kolozebate", "assets/robo/stage/koloZebate.png", "assets/robo/stage/koloZebate.json");
            this.load.atlasJSONArray("gwizdek", "assets/robo/stage/gwizdek.png", "assets/robo/stage/gwizdek.json");
            this.load.atlasJSONArray("robobo", "assets/robo/stage/robobobo.png", "assets/robo/stage/robobobo.json");
            this.load.atlasJSONArray("scoreScreen", "assets/scoreScreen/scoreScreen.png", "assets/scoreScreen/scoreScreen.json");
            this.load.atlasJSONArray("playScoreScreen", "assets/scoreScreen/scoreScreenButtons.png", "assets/scoreScreen/scoreScreenButtons.json");
            for (var i = 1; i < 6; i++) {
                this.load.atlasJSONArray("fail" + i, "assets/feedbacks/fail" + i + ".png", "assets/feedbacks/fail" + i + ".json");
            }
            for (var i = 1; i < 5; i++) {
                this.load.atlasJSONArray("success" + i, "assets/feedbacks/success" + i + ".png", "assets/feedbacks/success" + i + ".json");
            }
            for (var i = 1; i < 5; i++) {
                this.load.audio("congrats" + i, "assets/feedbacks/congrats" + i + ".mp3");
            }
            this.load.audio("failure1", "assets/feedbacks/failure1.mp3");
            this.load.audio("failure2", "assets/feedbacks/failure2.mp3");
        };
        Preloader.prototype.create = function () {
            this.game.state.start("MainMenu", true, false);
        };
        return Preloader;
    })(Phaser.State);
    Snow.Preloader = Preloader;
})(Snow || (Snow = {}));
//# sourceMappingURL=Preloader.js.map