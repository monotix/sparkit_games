declare class Greeter {
    element: HTMLElement;
    span: HTMLElement;
    timerToken: number;
    constructor(element: HTMLElement);
    start(): void;
    stop(): void;
}
declare module Snow {
    class Boot extends Phaser.State {
        preload(): void;
        create(): void;
    }
}
declare module Snow {
    class GameControler {
        static _instance: GameControler;
        SPEEDBOAT: number;
        SPEEDROBOT: number;
        SPEEDX: number;
        SPEEDY: number;
        finishGame: boolean;
        private _currenLevel;
        private _level;
        _collisionPlayer: boolean;
        gameOver: boolean;
        maxLevel: number;
        currentLevel: number;
        mainSound: Phaser.Sound;
        private _countBadAnswer;
        construct(): void;
        collisionPlayer: boolean;
        static getInstance(): GameControler;
        restart(): void;
        nextLevel(): void;
        setLevel(level: Level): void;
        level: Level;
        setSound(sound_: Phaser.Sound): void;
        checkBadAnswer(): void;
    }
}
declare module Snow {
    class LetterControler {
        static _instance: LetterControler;
        private _currentLetters;
        private _currentWorld;
        private _maxLetter;
        private _corretAnswer;
        private _maxBadAnswer;
        private _badAnswer;
        private _gameControler;
        private _currentSequence;
        private _maxSequence;
        correctLeters: Array<string>;
        construct(): void;
        static getInstance(): LetterControler;
        phonemes: Array<string>;
        phonemeSounds: Array<string>;
        correctPhonemes: Array<string>;
        randomPhoneme(): string;
        addCorrectLetters(str: string): void;
        setRandomLetters(howmany?: number): void;
        checkWord(str: string): void;
        checkWinOrDie(die: boolean): void;
        getRandomNumber(max: number, min?: number): number;
        isCorrectLetter(str: string): boolean;
        getPopLetter(): Array<any>;
        currentLetters: Array<string>;
        static words: Array<any>;
    }
}
declare module Snow {
    class ObstacleControler {
        static _instance: ObstacleControler;
        construct(): void;
        static getInstance(): ObstacleControler;
        level1(): Array<any>;
        level2(): Array<any>;
        getRandom(max: number): number;
    }
}
declare module Snow {
    class Instruction extends Phaser.State {
        private overButtonA;
        private _mainMenuBg;
        create(): void;
        onGameClick(): void;
        private overPlay2();
        onPlayClick(): void;
    }
}
declare module Snow {
    class Level extends Phaser.State {
        private _bg;
        private _obstcaleGroup;
        private _gameControler;
        private _letterControler;
        private _prompt;
        private _sequenceGr;
        private _sequence;
        private _playerPlay;
        private _showLetter;
        private _showLetterGr;
        private _clock;
        private _finishScreenLevel;
        private _finishScreen;
        private _special;
        private _jumpBtn;
        private _timer;
        private _timerCounter;
        private rightKey;
        private _blokada;
        private jumpPressed;
        private _blokadaGr;
        private _gameGr;
        _isShowEndScrren: boolean;
        private letters;
        private cursors;
        private _speedPlayer;
        private helpGr;
        private _helpBtn;
        private _instructionShow;
        create(): void;
        private helpBtnClick();
        youwinSequence(): void;
        youLose(): void;
        showLetterMenu(_collectLetters: Array<string>): void;
        addCredits(): void;
        addPlayerToTop(top: boolean): void;
        blokada(show?: boolean): void;
        playAgain(win?: boolean): void;
        onPromptAudioComplete(): void;
        sequenceLevel(str: string): void;
        showFinishLevel(win?: boolean): void;
        showFinish(): void;
        showPrompt(correct: boolean, end?: boolean): void;
        removeSequence(): void;
        stopGame(stop: boolean, changePos?: boolean): void;
        update(): void;
        collision(object: any, object2: any): void;
        render(): void;
    }
}
declare module Snow {
    class Game extends Phaser.Game {
        constructor();
    }
}
declare var userLevel: number;
declare module Snow {
    class DropLetter extends Phaser.Group {
        private bgDrop;
        private _dropGr;
        private _arr;
        private _parent;
        widthLetter: number;
        constructor(game: Phaser.Game, parent: Letters, numerOfSlots: number);
        clear(): void;
        checkLetter(): void;
    }
}
declare module Snow {
    class Letter extends Phaser.Sprite {
        private _letter;
        widthLetter: number;
        constructor(game: Phaser.Game, letter: string, bgColor?: number);
    }
}
declare module Snow {
    class LetterWord extends Phaser.Sprite {
        private _letter;
        private _letterStr;
        isAdded: boolean;
        placeToDrop: PlaceToDrop;
        widthLetter: number;
        constructor(game: Phaser.Game, letter: string);
        addObjectToDrag(): LetterWord;
        letterString(): string;
    }
}
declare module Snow {
    class Letters extends Phaser.Sprite {
        private alphabet;
        private words;
        private _wordsGr;
        private _objectToDrag;
        private _objectsToDropGr;
        private _dropLetter;
        _arrAnswear: Array<string>;
        private _successArr;
        private _parent;
        widthScreen: number;
        heightScreen: number;
        constructor(game: Phaser.Game, x: number, y: number, key: string, lettersArr: Array<string>, parent_: Level);
        onDoneClick(item: Phaser.Button): void;
        onGameClear(): void;
        playAnim(animSpr: Phaser.Sprite): void;
        playAnim2(animSpr: Phaser.Sprite): void;
        animationStopped(): void;
        animationStopped2(): void;
        youWin(): void;
        youLose(): void;
        makeLetter(): void;
        letterOnDown(letter: any): void;
        drawWords(arr: Array<any>): void;
        check(item: LetterWord): void;
        checkSecondTime(item: LetterWord): void;
        stageUp(): void;
        checkIfNotAdded(object?: any): void;
        collision(object: any, object2: any): void;
        update(): void;
        randomNum(max: number, min?: number): number;
    }
}
declare module Snow {
    class PlaceToDrop extends Phaser.Sprite {
        private _bg;
        isAdded: boolean;
        _letter: LetterWord;
        constructor(game: Phaser.Game, big?: boolean);
        addLetter(letter: LetterWord): void;
        clear(): void;
    }
}
declare module Snow {
    class Test extends Phaser.Group {
    }
}
declare module Snow {
    class Word extends Phaser.Sprite {
        private _letter;
        constructor(game: Phaser.Game, letter: string);
    }
}
declare module Snow {
    class MainMenu extends Phaser.State {
        private overButtonA;
        create(): void;
        private overPlay2();
        onGameClick(): void;
        onPlayClick(): void;
    }
}
declare module Snow {
    class Obstacle extends Phaser.Sprite {
        private _haveAnim;
        constructor(game: Phaser.Game, x: number, y: number, key: string);
        getRandom(max: number): number;
        stopAnim(): void;
    }
}
declare module Snow {
    class Player extends Phaser.Sprite {
        private _gameControl;
        private _crashForward;
        private _forward;
        private _jump;
        private _forwardSpeed;
        private _miganie;
        private _left;
        private _right;
        private electroAudio;
        private electroAudio2;
        private crashAudio;
        private crashAudio2;
        private jumpAudio;
        private holeAudio;
        private _currentAnim;
        private _currentTrickiCount;
        private _tricki;
        private _jumpAd;
        private _psssAd;
        private _crashAd;
        private blokadaAnimLeft;
        private blokadaAnimRight;
        constructor(game: Phaser.Game, x: number, y: number, key: string);
        jump(): void;
        endJump(): void;
        right(): void;
        left(): void;
        crashForward(): void;
        crashForwardOver(): void;
        activePlayer(): void;
        forward(): void;
        addAnim(anim: Phaser.Sprite): void;
        playAgain(): void;
        countFrames(start: number, num: number): Array<number>;
    }
}
declare module Snow {
    class PopLetter extends Phaser.Sprite {
        private _letterShow;
        letter: string;
        private posx;
        private posy;
        private bounceWidth;
        private bounceHeight;
        private color;
        private _coinAd;
        constructor(game: Phaser.Game, x: number, y: number, key: string, posx?: number, posy?: number, bounceWidth?: number, bounceHeight?: number);
        isCoin(): void;
        isRed(red: boolean): void;
        coinAd(): void;
        addLetter(str: string): void;
        getBounce(): Phaser.Rectangle;
        collisonPlayAnim(): void;
        countFrames(start: number, num: number): Array<number>;
    }
}
declare module Snow {
    class Preloader extends Phaser.State {
        preload(): void;
        create(): void;
    }
}
declare var mobile: any;
declare function APIsetLevel(userID: any, gameID: any, userLevel: any): any;
declare function APIgetLevel(userID: any, gameID: any): any;
declare var userID: any;
declare var gameID: any;
declare module Snow {
    class ClockView extends Phaser.Group {
        private _sequence;
        private _credits;
        private _time;
        private _parent;
        private _seconds;
        private _minutes;
        private _creditsCount;
        private _sequencCount;
        private _gameControler;
        constructor(game: Phaser.Game, parent: Level);
        startTimer(): void;
        stopTimer(): void;
        addCredits(): void;
        setSequenceText(text: string): void;
        setCreditText(text: string): void;
        private resetTimer();
        private updateTimer();
    }
}
declare module Snow {
    class FinishLevel extends Phaser.Group {
        private _finishScreen;
        private _playButton;
        private _quit;
        private _game;
        private _gameControl;
        private gameOverSpr;
        private youWinSpr;
        private screenWinOrGameOver;
        private _bgBlack;
        private _bgSpr;
        private _win;
        constructor(game: Phaser.Game);
        create(): void;
        youwin(): void;
        youLose(): void;
        addTostage(): void;
        hideScreenAfterFinishPlaye(): void;
        onDown(): void;
        playAgain(): void;
        quitClick(): void;
    }
}
declare module Snow {
    class FinishScreen extends Phaser.Group {
        private _finishScreen;
        private _playButton;
        private _quit;
        private _game;
        private _gameControl;
        constructor(game: Phaser.Game);
        create(): void;
        playAgain(): void;
        quitClick(): void;
    }
}
declare module Snow {
    class ObstacleView extends Phaser.Group {
        private _longerObstacle;
        private _game;
        private _obstacleArr;
        private _whenAddNewScreen;
        private _gameControler;
        private _obstacleControl;
        private _letterControler;
        private _boundsOut;
        constructor(game: Phaser.Game);
        randomBoats(): Phaser.Sprite;
        getObstacle(): void;
        addAgain(): void;
        getTheLonger(posx: number, width_: number, i: number): void;
        changeLetterToCoin(): void;
        obstacleOutOfScreen(): void;
        delObstacle(): void;
        delObstacles(): void;
        changeSpeed(stop?: boolean, changePos?: boolean): void;
        removeObstacleLetters(): void;
        removeObstacle(): void;
        getRandom(max: number): number;
        update(): void;
    }
}
declare module Snow {
    class PlayerPlay extends Phaser.Group {
        private _gameControler;
        private _obstacleControl;
        private _game;
        private _player;
        private _tween;
        private _leftRight;
        _stop: boolean;
        private _stopTween;
        constructor(game: Phaser.Game);
        addPlayer(): void;
        getPlayer(): Player;
        private lastPos;
        movePlayer(pointer: any): void;
        grannicaKlikania(pos: Phaser.Point): boolean;
        endMove(): void;
        changePos(pointer: Phaser.Point): void;
        tweenStop(): void;
        stopTween(): void;
        player: Player;
        upadte(): void;
    }
}
declare module Snow {
    class PopUp extends Phaser.Sprite {
        private logo;
        private _game;
        private gamesBtn;
        private playBtn;
        private level1;
        private overButtonA;
        constructor(game: Phaser.Game, level: any, x: number, y: number);
        addButtons(): void;
        activeButton(): void;
        gamesBtnClick(): void;
        playBtnClick(): void;
        textLevel(): void;
    }
}
declare function APIsetLevel(userID: any, gameID: any, userLevel: any): any;
declare function APIgetLevel(userID: any, gameID: any): any;
declare var userID: any;
declare var gameID: any;
declare function launchGame(result: any): void;
declare module Snow {
    class PopupStart extends Phaser.State {
        private logo;
        private overButtonA;
        create(): void;
        addButtons(): void;
        gamesBtnClick(): void;
        playBtnClick(): void;
        beatText(): void;
        textCredits(): void;
        textLevel(): void;
    }
}
declare module Snow {
    class PopUpWin extends Phaser.Sprite {
        private logo;
        private _game;
        private gamesBtn;
        private playBtn;
        private level1;
        private overButtonA;
        constructor(game: Phaser.Game, level: any, x: number, y: number);
        addButtons(): void;
        activeButton(): void;
        gamesBtnClick(): void;
        playBtnClick(): void;
        textLevel(): void;
    }
}
declare module Snow {
    class PromptView extends Phaser.Group {
        private _textView;
        private _kayak;
        private _audio;
        private _parent;
        constructor(game: Phaser.Game, parent: Level);
        playIntro(): void;
        playFeedbackGood(nextLevel?: boolean): void;
        playFeedbackWrong(end?: boolean): void;
        private onAudioComplete();
        private onAudioCompleteNextLevel();
        private onAudioCompleteEnd();
    }
}
declare module Snow {
    class Sequence extends Phaser.Sprite {
        private _collectLetters;
        private _contenerLetter;
        private _gameControler;
        constructor(game: Phaser.Game, x: number, y: number, key: string);
        addLetter(letter: string): void;
        removeLetters(): void;
    }
}
declare module Snow {
    class ShowLetter extends Phaser.Sprite {
        private _letterShow;
        private _gameControler;
        private _letterControler;
        private _letterD;
        private _letterB;
        private _letterP;
        private failArr;
        private _currentMax;
        private _currentCount;
        constructor(game: Phaser.Game, x: number, y: number, key: string);
        showLetters(): void;
        addBgToDisplayLetter(): void;
        private showLetter(str);
        playAgain(): void;
    }
}
