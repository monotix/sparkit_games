var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Snow;
(function (Snow) {
    var Obstacle = (function (_super) {
        __extends(Obstacle, _super);
        function Obstacle(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this.name = key;
            if (this.name == "osmiornica" || this.name == "shark" || this.name == "shark2" || this.name == "shark3" || this.name == "shark4") {
                this._haveAnim = true;
                this.animations.add("anim");
                this.animations.play("anim", 24, true);
                this.y += 305;
            }
            else if (this.name == "boatsmedium") {
                this.frame = this.getRandom(this.animations.frameTotal);
                this.y += 50;
            }
            else if (this.name == "boatssmall") {
                this.y += 100;
            }
            else {
                this.y += 305;
            }
        }
        Obstacle.prototype.getRandom = function (max) {
            return Math.floor(Math.random() * max);
        };
        Obstacle.prototype.stopAnim = function () {
            if (this._haveAnim) {
                this.animations.stop("anim");
            }
            else {
                console.log("nie ma animacji", this.name);
            }
        };
        return Obstacle;
    })(Phaser.Sprite);
    Snow.Obstacle = Obstacle;
})(Snow || (Snow = {}));
//# sourceMappingURL=Obstacle.js.map