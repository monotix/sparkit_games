﻿module Snow {
    export class PopLetter extends Phaser.Sprite {

        private _letterShow: Phaser.Text;
        public letter= "b";
        private posx: number;
        private posy: number;
        private bounceWidth: number;
        private bounceHeight: number;
        private color: string = "#000000";
        private _coinAd: Phaser.Sound;
        constructor(game: Phaser.Game, x: number, y: number, key: string, posx: number = 0, posy: number = 0, bounceWidth: number = 0, bounceHeight: number = 0) {

            super(game, x, y, key);
         
            this.name = "popletter";
          //  this.animations.add("pop");
            this.animations.add("pop", this.countFrames(7, 19),24,false);
            this.y += 300;
            this.posx = (posx == 0) ? 0 : posx;
            this.posy = (posy == 0) ? 0 : posy;
            this.bounceWidth = (bounceWidth == 0) ? this.width : bounceWidth;
            this.bounceHeight = (bounceHeight == 0) ? this.height : bounceHeight;
            this._coinAd = this.game.sound.add("monetaAd", 1, false);
           
        }


        isCoin() {
            
            this.removeChild(this._letterShow);
            this.name = "coin";
            this.frame = 4;


        }

        isRed(red:boolean) {

            if (red) {
                this.color = "#FF0000";
                this.name = "correctLetter";
            }
        }

        coinAd() {

            this._coinAd.play();
        }

        addLetter(str: string) {

            this.frame = str.length - 1;
            var style = { font: "40px Arial", fill: this.color, align: "center" };
            if (this._letterShow) this._letterShow.destroy();
            this._letterShow = new Phaser.Text(this.game, 0, 0, str, style);
            this._letterShow.x = 50 - this._letterShow.width / 2;
            this._letterShow.y = 25 - this._letterShow.height / 2;
           // this._letterShow.anchor.set(0.5, 0.5);
            //   this._letterShow.x = 40;
            //  this._letterShow.y = 25;
            this.addChild(this._letterShow);
            this.letter = str;

        }

        public getBounce(): Phaser.Rectangle {

            var rect: Phaser.Rectangle = new Phaser.Rectangle(this.posx, this.posy, this.bounceWidth, this.bounceHeight);

            return rect;

        }

        public collisonPlayAnim() {

            this.removeChild(this._letterShow);
           // this.animations.getAnimation("pop").onComplete.add(function () { this.kill(); }, this);

            this.play("pop", 24, false, true);



        }

        countFrames(start: number, num: number): Array<number> {
            var countArr: Array<number> = new Array();
            for (var i: number = start; i < num + 1; i++) {

                countArr.push(i);


            }

            return countArr;


        }

    }


} 