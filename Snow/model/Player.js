var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Snow;
(function (Snow) {
    var Player = (function (_super) {
        __extends(Player, _super);
        function Player(game, x, y, key) {
            _super.call(this, game, x, y, key);
            this._gameControl = Snow.GameControler.getInstance();
            this._currentTrickiCount = 0;
            //  this.anchor.setTo(0.5, 0.5);
            game.physics.enable(this, Phaser.Physics.ARCADE);
            this._crashForward = new Phaser.Sprite(game, 0, 0, "crashforward");
            this._crashForward.animations.add("play");
            this._jump = new Phaser.Sprite(game, 0, 0, "jump");
            this._jump.animations.add("play");
            this._forward = new Phaser.Sprite(game, 0, 0, "player");
            this._forward.animations.add("play");
            this._forwardSpeed = new Phaser.Sprite(game, 0, 0, "forwardSpeed");
            this._forwardSpeed.animations.add("play");
            this._miganie = new Phaser.Sprite(game, 0, 0, "miganie");
            this._miganie.animations.add("play");
            /*  this.animations.add("forward", this.countFrames(1, 19), 24, true);
              this.animations.add("fastforward", this.countFrames(20, 34), 24, false);
  
              this.animations.add("jump", this.countFrames(35, 88), 24, false);
              this.animations.add("jumptrick", this.countFrames(89, 136), 24, false);
          */
            this.electroAudio = this.game.sound.add("electro", 1, false, true);
            this.electroAudio2 = this.game.sound.add("electro1", 1, false, true);
            this.crashAudio = this.game.sound.add("crash", 1, false, true);
            this.crashAudio2 = this.game.sound.add("crash2", 1, false, true);
            this.jumpAudio = this.game.sound.add("jumpAudio", 1, false, true);
            this.holeAudio = this.game.sound.add("holeAudio", 1, false, true);
        }
        Player.prototype.jump = function () {
            this.addAnim(this._jump);
            this._currentAnim.animations.play("play", 24, false, false);
            this._currentAnim.animations.getAnimation("play").onComplete.add(function () {
                this.endJump();
            }, this);
        };
        Player.prototype.endJump = function () {
            this.addAnim(this._forward);
            this._currentAnim.animations.play("play", 24, true, false);
            this._gameControl.collisionPlayer = false;
        };
        Player.prototype.jumptrick = function () {
            if (this._currentTrickiCount == 6) {
                this._currentTrickiCount = 0;
            }
            this._gameControl.level.addCredits();
            this._currentTrickiCount++;
            console.log("trocki cout", this._currentTrickiCount);
            this._tricki = new Phaser.Sprite(this.game, 0, 0, "tricki" + this._currentTrickiCount, 0);
            this._tricki.animations.add("play");
            this.addAnim(this._tricki);
            this._currentAnim.animations.play("play", 24, false, false);
            this._currentAnim.animations.getAnimation("play").onComplete.add(function () {
                this.jumptrickiend();
            }, this);
        };
        Player.prototype.jumptrickiend = function () {
            this.addAnim(this._forward);
            this._tricki.kill();
            this._tricki = null;
            this._currentAnim.animations.play("play", 24, true, false);
            this._gameControl.collisionPlayer = false;
        };
        Player.prototype.crashForward = function () {
            this.addAnim(this._crashForward);
            this._currentAnim.animations.play("play", 24, false);
            this._currentAnim.animations.getAnimation("play").onComplete.add(function () {
                this.crashForwardOver();
            }, this);
        };
        Player.prototype.crashForwardOver = function () {
            this.addAnim(this._miganie);
            this._currentAnim.animations.play("play", 24, false);
            this._currentAnim.animations.getAnimation("play").onComplete.add(function () {
                this.activePlayer();
            }, this);
            // this.game.add.tween(this._currentAnim).to({ alpha: 0 }, 0.5, Phaser.Easing.Linear.None, false, 0.5, 5).start();
            //var tweenAn =  this.game.add.tween(this._currentAnim).to({ alpha: 1 }, 5, Phaser.Easing.Linear.None,false).start();
            //  this.game.time.events.add(Phaser.Timer.SECOND * 4, this.activePlayer, this)
            //  tweenAn.onComplete.add(this.activePlayer,this);
        };
        Player.prototype.activePlayer = function () {
            console.log("aktywne");
            this._gameControl.collisionPlayer = false;
        };
        Player.prototype.forward = function () {
            this.addAnim(this._forward);
            this._currentAnim.animations.play("play", 24, true);
        };
        Player.prototype.forwardSpeed = function () {
            this.addAnim(this._forwardSpeed);
            this._currentAnim.animations.play("play", 24, true);
        };
        Player.prototype.addAnim = function (anim) {
            if (this._currentAnim != null) {
                this.removeChild(this._currentAnim);
            }
            this._currentAnim = anim;
            //  this._currentAnim.scale.setTo(0.5, 0.5);
            this._currentAnim.anchor.setTo(0.5, 0.5);
            this._currentAnim.animations.stop("play", true);
            this.addChild(this._currentAnim);
        };
        Player.prototype.fastforward = function () {
            this.animations.play("fastforward", 24, false, false);
        };
        Player.prototype.playAgain = function () {
            this._gameControl.collisionPlayer = false;
        };
        Player.prototype.countFrames = function (start, num) {
            var countArr = new Array();
            for (var i = start; i < num + 1; i++) {
                countArr.push(i);
            }
            return countArr;
        };
        return Player;
    })(Phaser.Sprite);
    Snow.Player = Player;
})(Snow || (Snow = {}));
//# sourceMappingURL=Player.js.map