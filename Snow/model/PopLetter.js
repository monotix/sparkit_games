var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Snow;
(function (Snow) {
    var PopLetter = (function (_super) {
        __extends(PopLetter, _super);
        function PopLetter(game, x, y, key, posx, posy, bounceWidth, bounceHeight) {
            if (posx === void 0) { posx = 0; }
            if (posy === void 0) { posy = 0; }
            if (bounceWidth === void 0) { bounceWidth = 0; }
            if (bounceHeight === void 0) { bounceHeight = 0; }
            _super.call(this, game, x, y, key);
            this.letter = "b";
            this.name = "popletter";
            //  this.animations.add("pop");
            this.y += 300;
            this.posx = (posx == 0) ? 0 : posx;
            this.posy = (posy == 0) ? 0 : posy;
            this.bounceWidth = (bounceWidth == 0) ? this.width : bounceWidth;
            this.bounceHeight = (bounceHeight == 0) ? this.height : bounceHeight;
        }
        PopLetter.prototype.addLetter = function (str) {
            this.frame = str.length - 1;
            var style = { font: "40px Arial", fill: "#000000", align: "center" };
            if (this._letterShow)
                this._letterShow.destroy();
            this._letterShow = new Phaser.Text(this.game, 0, 0, str, style);
            this._letterShow.x = this.width / 2 - this._letterShow.width / 2;
            this._letterShow.y = this.height / 2 - this._letterShow.height / 2;
            // this._letterShow.anchor.set(0.5, 0.5);
            //   this._letterShow.x = 40;
            //  this._letterShow.y = 25;
            this.addChild(this._letterShow);
            this.letter = str;
        };
        PopLetter.prototype.getBounce = function () {
            var rect = new Phaser.Rectangle(this.posx, this.posy, this.bounceWidth, this.bounceHeight);
            return rect;
        };
        PopLetter.prototype.collisonPlayAnim = function () {
            this.removeChild(this._letterShow);
            this.animations.getAnimation("pop").onComplete.add(function () {
                this.kill();
            }, this);
            this.play("pop", 24, false, true);
        };
        return PopLetter;
    })(Phaser.Sprite);
    Snow.PopLetter = PopLetter;
})(Snow || (Snow = {}));
//# sourceMappingURL=PopLetter.js.map