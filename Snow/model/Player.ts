﻿module Snow {

    export class Player extends Phaser.Sprite {


        private _gameControl: GameControler = GameControler.getInstance();


        private _crashForward: Phaser.Sprite;
        private _forward: Phaser.Sprite;
        private _jump: Phaser.Sprite;
        private _forwardSpeed: Phaser.Sprite;
        private _miganie: Phaser.Sprite;
        private _left: Phaser.Sprite;
        private _right: Phaser.Sprite;
        private electroAudio: Phaser.Sound;
        private electroAudio2: Phaser.Sound;
        private crashAudio: Phaser.Sound;
        private crashAudio2: Phaser.Sound;
        private jumpAudio: Phaser.Sound;
        private holeAudio: Phaser.Sound;
        private _currentAnim: Phaser.Sprite;
        private _currentTrickiCount: number = 0;
        private _tricki: Phaser.Sprite;
        private _jumpAd: Phaser.Sound;
        private _psssAd: Phaser.Sound;
        private _crashAd: Phaser.Sound;
        private blokadaAnimLeft: boolean = false;
        private blokadaAnimRight: boolean = false;
        constructor(game: Phaser.Game, x: number, y: number, key: string) {

            super(game, x, y, key);

          //  this.anchor.setTo(0.5, 0.5);
          
            game.physics.enable(this, Phaser.Physics.ARCADE);


            this._crashForward = new Phaser.Sprite(game, 0, 0, "crashforward");
            this._crashForward.animations.add("play");

            this._left = new Phaser.Sprite(game, 0, 0, "left");
            this._left.animations.add("play");

            this._right = new Phaser.Sprite(game, 0, 0, "right");
            this._right.animations.add("play");


            this._jump = new Phaser.Sprite(game, 0, 0, "jump");
            this._jump.animations.add("play");

            this._forward = new Phaser.Sprite(game, 0, 0, "player");
            this._forward.animations.add("play");


            this._forwardSpeed = new Phaser.Sprite(game, 0, 0, "forwardSpeed");
            this._forwardSpeed.animations.add("play");

            this._jumpAd = this.game.sound.add("jumpAd", 1, false);
            this._psssAd = this.game.sound.add("psssAd", 1, false);
            this._crashAd = this.game.sound.add("crashAd", 1, false);
           // this._miganie = new Phaser.Sprite(game, 0, 0, "miganie");
           // this._miganie.animations.add("play");
            

          /*  this.animations.add("forward", this.countFrames(1, 19), 24, true);
            this.animations.add("fastforward", this.countFrames(20, 34), 24, false);

            this.animations.add("jump", this.countFrames(35, 88), 24, false);
            this.animations.add("jumptrick", this.countFrames(89, 136), 24, false);
        */

            this.electroAudio = this.game.sound.add("electro", 1, false, true);
            this.electroAudio2 = this.game.sound.add("electro1", 1, false, true);

            this.crashAudio = this.game.sound.add("crash", 1, false, true);
            this.crashAudio2 = this.game.sound.add("crash2", 1, false, true);
            this.jumpAudio = this.game.sound.add("jumpAudio", 1, false, true);
            this.holeAudio = this.game.sound.add("holeAudio", 1, false, true);

        }


        jump() {

            this.addAnim(this._jump);
          
            this._currentAnim.animations.play("play", 24, false, false);
            this._jumpAd.play();
          
            this._currentAnim.animations.getAnimation("play").onComplete.add(function () { this.endJump();}, this);


        }

        endJump() {
            this.addAnim(this._forward);
            this._currentAnim.animations.play("play", 24, true, false);
            this._gameControl.collisionPlayer = false;
            this._gameControl.level.addPlayerToTop(false);

            this.blokadaAnimLeft = false;
            this.blokadaAnimRight = false;
        }

        right() {

            if (!this.blokadaAnimRight) {
                this.blokadaAnimLeft = false;
                this.blokadaAnimRight = true;
                this.addAnim(this._right);
                this._psssAd.play();
                this._currentAnim.animations.play("play", 24, false, false);


                this._currentAnim.animations.getAnimation("play").onComplete.add(function () { this.endJump(); }, this);
            }
        
        }

        left() {

            if (!this.blokadaAnimLeft) {
                this.blokadaAnimLeft = true;
                this.blokadaAnimRight = false;
                this.addAnim(this._left);
                this._psssAd.play();
                this._currentAnim.animations.play("play", 24, false, false);


                this._currentAnim.animations.getAnimation("play").onComplete.add(function () { this.endJump(); }, this);
            }
        }

      

       

        crashForward() {
            console.log("gram kolizje");
            this.addAnim(this._crashForward);
            this._crashAd.play();
            this._gameControl.level.stopGame(true);
            this._currentAnim.animations.play("play",24, false);
            this._currentAnim.animations.getAnimation("play").onComplete.add(function () { this.crashForwardOver(); }, this);

        }

        crashForwardOver() {
            console.log("koncze grac kolizje");
            this._gameControl.level.stopGame(false, true);
            this.addAnim(this._forward);
            this._currentAnim.animations.play("play", 24, false);
            
            this.game.time.events.add(Phaser.Timer.SECOND * 1, this.activePlayer, this);
           // this.activePlayer();
           // this.game.add.tween(this._currentAnim).to({ alpha: 0 }, 0.5, Phaser.Easing.Linear.None, false, 0.5, 5).start();
            //var tweenAn =  this.game.add.tween(this._currentAnim).to({ alpha: 1 }, 5, Phaser.Easing.Linear.None,false).start();
          //  this.game.time.events.add(Phaser.Timer.SECOND * 4, this.activePlayer, this)
          //  tweenAn.onComplete.add(this.activePlayer,this);

        }

        activePlayer() {
            console.log("aktywne");
            this._gameControl.collisionPlayer = false;

        }

        forward() {

            this.addAnim(this._forward);
            this._currentAnim.animations.play("play", 24, true);
            


        }


        addAnim(anim:Phaser.Sprite) {

            if (this._currentAnim != null) {

                this.removeChild(this._currentAnim);

            }

            this._currentAnim = anim;
          //  this._currentAnim.scale.setTo(0.5, 0.5);
            this._currentAnim.anchor.setTo(0.5, 0.5);
            this._currentAnim.animations.stop("play", true);
            this.addChild(this._currentAnim);

        }

      
       

        playAgain() {

            if (this.stage.x < 0) this.stage.x = 300;
            else if (this.stage.x > 800) this.stage.x = 300;

            if (this.stage.y < 0) this.stage.y = 500;
            else if (this.stage.y > 600) this.stage.y = 500;
            console.log("sprawdzam", this.stage.x, this.stage.y);
            if (isNaN(this.x)){
                this.x = 400;
                console.log("x is nan");
            }
            if (isNaN(this.y)) {
                console.log("y is nan");
                this.x = 300;
            }
            this._gameControl.collisionPlayer = false;


        }

    


        countFrames(start: number, num: number): Array<number> {
            var countArr: Array<number> = new Array();
            for (var i: number = start; i < num + 1; i++) {

                countArr.push(i);


            }

            return countArr;


        }



    }


} 