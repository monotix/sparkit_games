var Main = {};


Main.frameRange = function (posstart, posend) {
    hitFrames = [];
    for (a = posstart; a < posend + 1; a++) {
        hitFrames.push(a);
    }
    return hitFrames;
}

Main.currentWord = false;
Main.currentChunks = [];

Main.indexLetters = [
    {
    "a": [],
    "b": [],
    "c": [],
    "d": [],
    "e": [],
    "f": [],
    "g": [],
    "h": [],
    "i": [],
    "j": [],
    "k": [],
    "l": [],
    "m": [],
    "n": [],
    "o": [],
    "p": [],
    "q": [],
    "r": [],
    "s": [],
    "t": [],
    "u": [],
    "v": [],
    "w": [],
    "x": [],
    "y": [],
    "z": []
    }
];

Main.Vocabulary = [
    {"word": "bike", "chunks": ["b","i","k","e"]},
    {"word": "swan", "chunks": ["s","w","a","n"]},
    {"word": "nail", "chunks": ["n","a","i","l"]},
    {"word": "bone", "chunks": ["b","o","n","e"]},
    {"word": "wake", "chunks": ["w","a","k","e"]},
    {"word": "fish", "chunks": ["f","i","s","h"]}
]

Main.Boot = function (game) {
    this.game = game;

};

Main.Boot.prototype = {

    preload: function () {
        this.game.load.image('splash', 'assets/splashScreen.png');
        this.load.image('loaderFull', 'assets/ProgressBarBlue.png');
        this.load.image('loaderEmpty', 'assets/ProgressBar.png');
    },

    create: function () {
        // this.game.stage.enableOrientationCheck(true, false, 'orientation');
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.state.start('preloader', Main.Preloader);
    },


}