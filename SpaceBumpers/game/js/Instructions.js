Main.Instructions = function (game) {
    this.game = game;
};

Main.Instructions.prototype = {
    preload: function () {},
    create: function () {
        this.createBackground();
        var _buttons = this.game.add.group();
        var buttonsBg = this.game.add.sprite(0, 0, 'buttonsBg');
        _buttons.addChild(buttonsBg);
        var gameButton = this.game.add.button(14, 36, 'buttons', this.onGameClick, this, 0, 0, 1);
        _buttons.addChild(gameButton);
        var playButton = this.game.add.button(61, 36, 'buttons', this.onPlayClick, this, 3, 2);
        playButton.onInputDown.add(this.overPlay2, this);
        _buttons.addChild(playButton);
        _buttons.x = 20;
        _buttons.y = 517;
    },
    onGameClick: function () {},
    onPlayClick: function () {
        this.game.state.start("welcome", Main.Welcome);
    },
    overPlay2: function () {
        // this.overButtonA.play();
    },
    createBackground: function () {
        //  A simple background for our game
        this.game.add.sprite(0, 0, 'instructionScreen');

    },
    launchTheGame: function () {
        this.game.state.start("thegame", Main.Thegame);
    }
};