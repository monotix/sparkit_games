Main.GuessGame = function (game) {
    this.game = game;
};

Main.GuessGame.prototype = {
    preload: function () {},
    create: function () {
        var background = this.add.sprite(0, 0, 'space');
        var style = {
            font: "40px Arial",
            fill: "#ffff00",
            align: "center"
        };
        this.add.text(10,10, "Drag letters to type: "+Main.currentWord, style);
    }

}