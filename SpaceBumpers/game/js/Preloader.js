Main.Preloader = function (game) {
    this.game = game;
};

Main.Preloader.prototype = {
    loaderFull: Phaser.Sprite,
    loaderEmpty: Phaser.Sprite,
    test: Phaser.Sprite,
    preload: function () {
        this.game.add.sprite(0, 0, 'splash');

        this.loaderEmpty = this.add.sprite(35, 485, 'loaderEmpty');
        this.loaderEmpty.crop = new Phaser.Rectangle(0, 0, this.loaderEmpty.width, this.loaderEmpty.height);
        this.loaderEmpty.name = 'loaderEmpty';
        this.loaderFull = this.add.sprite(35, 485, 'loaderFull');
        this.loaderFull.crop = new Phaser.Rectangle(0, 0, 0, this.loaderFull.height);
        this.loaderFull.name = 'loaderFull';

        this.game.load.atlas('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');
        this.game.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
        this.game.load.atlas('countdown', 'assets/countdown.png', 'assets/countdown.json');
        this.game.load.atlas('asteroid', 'assets/asteroid.png', 'assets/asteroid.json');
        this.game.load.atlas('bumper', 'assets/srodek.png', 'assets/srodek.json');
        this.game.load.atlas('player', 'assets/player.png', 'assets/player.json');
        this.game.load.atlas('monster', 'assets/monster.png', 'assets/monster.json');


        this.load.audio("overButtonA", "assets/audio/overButton.wav");
        this.load.audio("clearButtonA", "assets/audio/clearButton.wav");
        this.load.atlas("scoreScreen", "assets/scoreScreen/scoreScreen.png", "assets/scoreScreen/scoreScreen.json");
        this.load.atlas("playScoreScreen", "assets/scoreScreen/scoreScreenButtons.png", "assets/scoreScreen/scoreScreenButtons.json");

        this.game.load.image('instructionScreen', 'assets/instructionScreen.png');
        this.game.load.image('space', 'assets/gameBg.png');
        this.game.load.image('gui', 'assets/gui.png');
        this.game.load.image('bubble', 'assets/buubleBg.png');
        this.game.load.image('target', 'assets/target.png');
        
        
        this.game.load.image('borders', 'assets/borders.png');
        this.game.load.physics('bordersPhysics', 'assets/borders.json');
        this.game.load.physics('playerPhysics', 'assets/playerPhysics.json');

  

        //icons
        this.game.load.image('icon1_a', 'assets/icon1_a.png');
        this.game.load.image('icon1_b', 'assets/icon1_b.png');
        this.game.load.image('icon2_a', 'assets/icon2_a.png');
        this.game.load.image('icon2_b', 'assets/icon2_b.png');
        this.game.load.image('icon3_a', 'assets/icon3_a.png');
        this.game.load.image('icon3_b', 'assets/icon3_b.png');


        this.load.onFileComplete.add(this.fileLoaded, this);
    },
    create: function () {
        this.game.state.start('mainmenu', Main.MainMenu);
        //this.game.state.start('thegame', Main.Thegame);
    },
    fileLoaded: function (progress) {
        // console.log(this.loaderFull.crop.width);
        this.loaderEmpty.crop.left = (135 / 100) * progress;
        this.loaderFull.crop.width = (135 / 100) * progress;
    }
}