Main.Thegame = function (game) {
    this.game = game;

    //settings
    this.score = 0;
    this.lives = 3;
    this.frame = 0;
    this.creditsAll = 0;
    this.currentLevel = 1;
    this.baddiesCount = 0;
    this.hitByRock = 0;
    this.mouseIsDown = false;
    var platforms, scoreText, livesText;
    this.blockedScreen = false;
    Main.currentWord = false;
    Main.currentChunks = [];
    this.currentLetter = false;
    this.playerGuessed = 0;
    this.monsterGuessed = 0;
    this.flashSpeed = 80;
    this.dblClickDelay = 0;
};

Main.Thegame.prototype = {

    preload: function () {},
    render: function () {
        //this.game.debug.spriteInfo(player, 32, 32);
        //this.game.debug.body(player);
    },
    create: function () {

        this.timeDelay = 0;
        this.monsterMoveDelay = 0;
        this.asteroidMoveDelay = 0;
        this.game.world.setBounds(0, 0, 800, 600);
        this.game.physics.startSystem(Phaser.Physics.P2JS);
        this.createBackground();
        this.createPlayer();
        this.createMonster();
        this.createAsteroid();
        this.createBumper();
        this.game.physics.p2.createContactMaterial(playerMaterial, bordersPolygonSpriteMaterial, {
            friction: 1,
            restitution: 1.5
        });
        this.game.physics.p2.createContactMaterial(monsterMaterial, bordersPolygonSpriteMaterial, {
            friction: 1,
            restitution: 1.5
        });
        this.game.physics.p2.createContactMaterial(playerMaterial, bumperMaterial, {
            friction: 1,
            restitution: 1.5
        });
        this.game.physics.p2.createContactMaterial(monsterMaterial, bumperMaterial, {
            friction: 1,
            restitution: 1.5
        });

        this.game.physics.p2.createContactMaterial(playerMaterial, asteroidMaterial, {
            friction: 1,
            restitution: 0.5
        });

        gui.bringToTop();

        if (Main.currentWord == false) {
            this.pickNewWord();
        }
        this.game.physics.p2.setPostBroadphaseCallback(this.checkOverlap, this);
    },

    update: function () {

        if (!this.blockedScreen) {
            spaceKey = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        }

        if (this.game.input.activePointer.isDown && this.mouseIsDown == false) {
            this.mouseIsDown = true;
            
            if (this.game.time.now < this.dblClickDelay) {
                //player.body.velocity.x *= 3;
                //player.body.velocity.y *= 3;
                this.flashSpeed += 20;
                console.log(player.body.velocity.x);
                this.game.physics.arcade.moveToPointer(player, this.flashSpeed);
            } else {
                this.flashSpeed = 80;
                this.game.physics.arcade.moveToPointer(player, this.flashSpeed);
            }
            this.dblClickDelay = this.game.time.now + 500
        }

        //Obróć playera do myszki
        deltaMouseRad = player.rotation - this.game.physics.arcade.angleToPointer(player) - Math.PI / 2;
        deltaMouseRad = deltaMouseRad - Math.PI / 2
        mod = Math.PI * 2
        deltaMouseRad = deltaMouseRad % mod;
        if (deltaMouseRad != deltaMouseRad % (mod / 2)) {
            deltaMouseRad = (deltaMouseRad < 0) ? deltaMouseRad + mod : deltaMouseRad - mod;
        }
        speed = 150
        player.body.rotateLeft(speed * deltaMouseRad);

        //Obróć monstera do litery
        if (this.currentLetter != false) {
            var monsterAngle = Math.atan2(monster.y - bubble.y, monster.x - bubble.x) * (180 / Math.PI);
            deltaMouseRad = monster.rotation - monsterAngle - Math.PI / 2;
            deltaMouseRad = deltaMouseRad - Math.PI / 2
            mod = Math.PI * 2
            deltaMouseRad = deltaMouseRad % mod;
            if (deltaMouseRad != deltaMouseRad % (mod / 2)) {
                deltaMouseRad = (deltaMouseRad < 0) ? deltaMouseRad + mod : deltaMouseRad - mod;
            }
            speed = 50;
            monster.body.rotateLeft(speed * deltaMouseRad);
        }


        if (this.game.input.activePointer.isUp) {
            this.mouseIsDown = false;
        }

        if (this.currentLetter != false && this.game.time.now > this.monsterMoveDelay) {
            this.game.physics.arcade.moveToObject(monster, bubble, 80);
            this.monsterMoveDelay = this.game.time.now + 500
        }
        if (this.currentLetter != false && this.game.time.now > this.asteroidMoveDelay) {
            this.game.physics.arcade.moveToObject(asteroid, bubble, 30);
            this.asteroidMoveDelay = this.game.time.now + 500
        }

        // nie wszystkie litery zebrane, wyciągamy pierwszą niezebraną literę słowa        
        if (this.currentLetter == false && Main.currentChunks.length > this.playerGuessed) {
            this.currentLetter = Main.currentChunks[this.playerGuessed];
            console.log(this.currentLetter);
            this.createBubble();
        }
        // wszystko zebrane przez playera
        if (Main.currentChunks.length == this.playerGuessed && this.playerGuessed > 0) {
            this.youWin();
        }
    },

    checkOverlap: function (body1, body2) {
        //console.log(body1.sprite);
        if ((body1.sprite.name === 'bubble' && body2.sprite.name === 'player') || (body2.sprite.name === 'bubble' && body1.sprite.name === 'player')) {
            this.playerCollectsLetter(); //whatever you need on overlap
            return false;
        }
        return true;
    },

    pickNewWord: function () {
        var wordsObject = Main.Vocabulary;
        var randomWordIndex = this.randomNum(wordsObject.length, 0);
        var currentWordObject = wordsObject[randomWordIndex];

        Main.currentWord = currentWordObject.word;
        Main.currentChunks = currentWordObject.chunks;

        console.log(Main.currentWord);
        this.playerGuessed = 0;
        this.monsterGuessed = 0;
    },

    createBubble: function () {
        var newx = this.randomNum(700, 100);
        var newy = this.randomNum(500, 100);
        bubble = this.game.add.sprite(newx, newy, 'bubble');
        this.game.physics.p2.enableBody(bubble);
        bubble.body.clearShapes();
        var circleShape = new p2.Circle(0.1);
        bubble.body.addShape(circleShape)
        bubble.body.x = newx;
        bubble.body.y = newy;
        bubble.anchor.setTo(0.5, 0.5);
        bubble.body.static = true;

        var style = {
            font: "40px Arial",
            fill: "#ff0000",
            align: "center"
        };
        bubbletxt = this.game.add.text(bubble.body.x, bubble.body.y, this.currentLetter, style);
        bubbletxt.anchor.set(0.5);
        bubble.body.onBeginContact.add(this.letterCollected, this);
        player.bringToTop();
        monster.bringToTop();
        asteroid.bringToTop();
    },
    quit: function () {},
    playAgain: function () {
        console.log("playagain");
        this.onBlockScreen(false);
        this.stage.removeChild(this.scoreScreenGr);
        this.defaultScore();
    },
    defaultScore: function () {
        this.fixedSatellites = 0;
        this.hitByRock = 0;
        satellite.animations.play('walk', 20, true);

    },
    youWin: function (finishLevel) {
        this.game.state.start("instructions", Main.Instructions);
        //this.game.state.start("guessgame", Main.GuessGame);
    },
    restart: function () {
        this.satellitesToFix = 3;
        this.fixedSatellites = 0;
        this.baddiesCount = 0;
        this.hitByRock = 0;
        this.create();
    },
    createBackground: function () {
        //  A simple background for our game        
        this.game.add.sprite(0, 0, 'space');
        gui = this.game.add.sprite(0, 500, 'gui');

        bordersPolygonSprite = this.game.add.sprite(0, 0, 'borders');
        this.game.physics.p2.enableBody(bordersPolygonSprite);
        bordersPolygonSprite.body.clearShapes();
        bordersPolygonSprite.body.loadPolygon('bordersPhysics', 'borders');
        bordersPolygonSprite.body.static = true;
        bordersPolygonSprite.body.x = 400;
        bordersPolygonSprite.body.y = 270;
        bordersPolygonSpriteMaterial = this.game.physics.p2.createMaterial(); //create material
        bordersPolygonSprite.body.setMaterial(bordersPolygonSpriteMaterial);

    },

    createBumper: function () {
        //  A simple background for our game        
        bumper = this.game.add.sprite(400, 200, 'bumper');
        this.game.physics.p2.enableBody(bumper);
        ///bumper.body.clearShapes();
        bumper.body.static = true;
        bumper.body.x = 400;
        bumper.body.y = 270;
        bumperMaterial = this.game.physics.p2.createMaterial(); //create material
        bumper.body.setMaterial(bumperMaterial);

    },

    createPlayer: function () {
        player = this.game.add.sprite(100, 350, 'player');
        player.name = "player";
        player.anchor.set(0.5, 0.1);
        this.game.physics.p2.enableBody(player);
        player.body.clearShapes();
        //player.body.loadPolygon('playerPhysics', 'playerPhysics'); 
        var circleShape = new p2.Circle(1.5);
        player.body.addShape(circleShape)
        player.animations.add('walk', Main.frameRange(1, 9));
        player.animations.play('walk', 30, true);
        player.scale.set(0.4);
        player.body.x = 300;
        player.body.y = 270;
        playerMaterial = this.game.physics.p2.createMaterial();
        player.body.setMaterial(playerMaterial);

    },

    createMonster: function () {
        monster = this.game.add.sprite(400, 350, 'monster');
        monster.name = "monster";
        monster.anchor.set(0.5, 1);
        this.game.physics.p2.enableBody(monster);
        monster.body.clearShapes();
        var circleShape = new p2.Circle(1.5);
        monster.body.addShape(circleShape)
        monster.animations.add('walk', Main.frameRange(1, 9));
        monster.animations.play('walk', 20, true);
        monster.scale.set(0.4);
        monster.body.x = 500;
        monster.body.y = 350;
        monsterMaterial = this.game.physics.p2.createMaterial();
        monster.body.setMaterial(monsterMaterial);
    },

    createAsteroid: function () {
        asteroid = this.game.add.sprite(400, 350, 'asteroid');
        this.game.physics.p2.enableBody(asteroid)
        asteroid.body.clearShapes();
        var circleShape = new p2.Circle(1.9);
        asteroid.body.addShape(circleShape)
        asteroid.body.x = 200;
        asteroid.body.y = 350;
        asteroid.anchor.setTo(0.5, 0.5);
        //asteroid.body.static = true;
        asteroid.animations.add('walk', Main.frameRange(1, 120));
        asteroid.animations.play('walk', 30, true);
        asteroidMaterial = this.game.physics.p2.createMaterial();
        asteroid.body.setMaterial(asteroidMaterial);

    },

    letterCollected: function (target) {
        //        console.log(target);
        if (target.sprite.name == "player") {
            console.log("player hits");
            this.playerGuessed = this.playerGuessed + 1;
            bubble.kill();
            if (player.scale.x < 1.1) {
                player.scale.set(player.scale.x + 0.1);
            }
            if (monster.scale.x > 0.2) {
                monster.scale.set(monster.scale.x - 0.1);
            }
            this.game.world.remove(bubbletxt);
            this.currentLetter = false;
        }
        if (target.sprite.name == "monster") {
            console.log("monster hits");
            this.monsterGuessed = this.monsterGuessed + 1;
            if (monster.scale.x < 1.1) {
                monster.scale.set(monster.scale.x + 0.1);
            }
            if (player.scale.x > 0.2) {
                player.scale.set(player.scale.x - 0.1);
            }
            this.currentLetter = false;
            bubble.kill();
            this.game.world.remove(bubbletxt);
        }
        console.log("monster: " + this.monsterGuessed);
        console.log("player: " + this.playerGuessed);
    },

    randomNum: function (max, min) {
        if (min === void 0) {
            min = 0;
        }
        return Math.floor(Math.random() * (max - min) + min);
    },

    onBlockScreen: function (show) {
        if (show) {
            console.log('blok');
            this.blockedScreen = true;
            this.blockScreen.inputEnabled = true;
            this.blockScreen.input.priorityID = 1;
            this.stage.addChild(this.blockScreen);
        } else {
            this.blockedScreen = false;
            this.blockScreen.inputEnabled = false;
            this.blockScreen.input.priorityID = 0;
            this.blockScreen.kill();
        }
    }
}