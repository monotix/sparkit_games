Main.MainMenu = function (game) {
    this.game = game;
};

Main.MainMenu.prototype = {
    preload: function () {},
    create: function () {
        //this.overButtonA = this.game.add.audio('overButtonA', 1, false);
        //this._mainMenuBg = this.game.add.audio('MainMenuSn', 1, true);
        //this._mainMenuBg.stop();
        var background = this.add.sprite(0, 0, 'splash');
        var _buttons = this.game.add.group();
        var buttonsBg = this.game.add.sprite(0, 0, 'buttonsBg');
        _buttons.addChild(buttonsBg);
        var gameButton = this.game.add.button(14, 36, 'buttons', this.onGameClick, this, 0, 0, 1);
        _buttons.addChild(gameButton);
        var playButton = this.game.add.button(61, 36, 'buttons', this.onPlayClick, this, 3, 2);
        playButton.onInputDown.add(this.overPlay2, this);
        _buttons.addChild(playButton);
        _buttons.x = 20;
        _buttons.y = 517;
    },
    overPlay2: function () {
        // this.overButtonA.play();
    },
    onGameClick: function () {},
    onPlayClick: function () {
        this.game.state.start("instructions", Main.Instructions);
        //this.game.state.start("welcome", Main.Thegame);
    },
    levels: function () {
     this.game.levels = {};
    }

}