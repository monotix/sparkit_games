var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Path;
(function (Path) {
    var Level = (function (_super) {
        __extends(Level, _super);
        function Level() {
            _super.apply(this, arguments);
            this.sprite = null;
            this.graphic = null;
            this.wasDown = false;
            this.path = null;
            this.pathIndex = -1;
            this.pathSpriteIndex = -1;
        }
        Level.prototype.blackoutGraphic = function () {
            this.graphic.beginFill(0x000000);
            this.graphic.lineStyle(4, 0x000000, 1);
            this.graphic.drawRect(0, 0, this.game.width, this.game.height);
            this.graphic.endFill();
            this.graphic.lineStyle(4, 0xFF0000, 1);
        };
        Level.prototype.preload = function () {
            this.game.load.image('star', 'star.png');
        };
        Level.prototype.create = function () {
            this.graphic = this.game.add.graphics(0, 0);
            this.blackoutGraphic();
            this.sprite = this.game.add.sprite(100, 100, 'star');
            this.sprite.anchor.setTo(0.5, 0.5);
        };
        Level.prototype.update = function () {
            if (this.game.input.mousePointer.isDown) {
                if (!this.wasDown) {
                    this.graphic.moveTo(this.game.input.x, this.game.input.y);
                    this.blackoutGraphic();
                    this.pathIndex = 0;
                    this.pathSpriteIndex = 0;
                    this.path = [];
                    this.wasDown = true;
                }
                if (this.pathIndex == 0 || (this.path[this.pathIndex - 1].x != this.game.input.x || this.path[this.pathIndex - 1].y != this.game.input.y)) {
                    this.graphic.lineTo(this.game.input.x, this.game.input.y);
                    this.path[this.pathIndex] = new Phaser.Point(this.game.input.x, this.game.input.y);
                    this.pathIndex++;
                }
            }
            else {
                this.wasDown = false;
            }
            if (this.path != null && this.path.length > 0 && this.pathSpriteIndex < this.pathIndex) {
                this.pathSpriteIndex = Math.min(this.pathSpriteIndex, this.path.length - 1);
                this.game.physics.arcade.moveToXY(this.sprite, this.path[this.pathSpriteIndex].x, this.path[this.pathSpriteIndex].y, 250);
                if (this.game.physics.arcade.distanceToXY(this.sprite, this.path[this.pathSpriteIndex].x, this.path[this.pathSpriteIndex].y) < 20) {
                    this.pathSpriteIndex++;
                    if (this.pathSpriteIndex >= this.pathIndex) {
                        this.sprite.body.velocity.setTo(0, 0);
                    }
                }
            }
        };
        return Level;
    })(Phaser.State);
    Path.Level = Level;
})(Path || (Path = {}));
//# sourceMappingURL=Level.js.map