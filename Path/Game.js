var Greeter = (function () {
    function Greeter(element) {
        this.element = element;
        this.element.innerHTML += "The time is: ";
        this.span = document.createElement('span');
        this.element.appendChild(this.span);
        this.span.innerText = new Date().toUTCString();
    }
    Greeter.prototype.start = function () {
        var _this = this;
        this.timerToken = setInterval(function () { return _this.span.innerHTML = new Date().toUTCString(); }, 500);
    };
    Greeter.prototype.stop = function () {
        clearTimeout(this.timerToken);
    };
    return Greeter;
})();
window.onload = function () {
    var el = document.getElementById('content');
    var greeter = new Greeter(el);
    greeter.start();
};
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Path;
(function (Path) {
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            console.log("start");
            _super.call(this, 800, 600, Phaser.AUTO, "content", null, true);
            this.state.add("Level", Path.Level, false);
            this.state.start("Level");
        }
        return Game;
    })(Phaser.Game);
    Path.Game = Game;
})(Path || (Path = {}));
window.onload = function () {
    var game = new Path.Game;
};
var Path;
(function (Path) {
    var Level = (function (_super) {
        __extends(Level, _super);
        function Level() {
            _super.apply(this, arguments);
            this.sprite = null;
            this.graphic = null;
            this.wasDown = false;
            this.sprite2 = null;
            this.bmd = null;
            this.path = null;
            this.pathIndex = -1;
            this.pathSpriteIndex = -1;
        }
        Level.prototype.blackoutGraphic = function () {
            this.graphic.clear();
            this.graphic.beginFill(0x000000);
            //  this.graphic.lineStyle(4, 0x000000, 1);
            //    this.graphic.drawRect(0, 0, this.game.width, this.game.height);
            this.graphic.endFill();
            this.graphic.lineStyle(4, 0xFF0000, 1);
            //  this.graphic.moveTo(100,100);
            //  this.graphic.lineTo(100, 100);
            //\  this.graphic.lineTo(200, 200);
        };
        Level.prototype.preload = function () {
            this.game.load.image('star', 'star.png');
        };
        Level.prototype.create = function () {
            this.bmd = this.game.add.bitmapData(800, 600);
            var color = 'red';
            this.bmd.ctx.beginPath();
            this.bmd.ctx.lineWidth = "4";
            this.bmd.ctx.strokeStyle = color;
            this.bmd.ctx.stroke();
            this.sprite2 = this.game.add.sprite(0, 0, this.bmd);
            // this.graphic = this.game.add.graphics(0, 0);
            //this.blackoutGraphic();
            this.sprite = this.game.add.sprite(100, 100, 'star');
            this.sprite.anchor.setTo(0.5, 0.5);
        };
        Level.prototype.showPoints = function () {
            var str = "new Array(";
            for (var i = 0; i < this.path.length; i++) {
                var line = "{ x : " + this.path[i].x + ", y : " + this.path[i].y + "}";
                var przecinek = ",\n";
                if (i == this.path.length - 1)
                    przecinek = "\n);";
                str += (line + przecinek);
            }
            console.log(str);
            /*

            new Array({x : 20,y:20});

            */
        };
        Level.prototype.update = function () {
            if (this.game.input.mousePointer.isDown) {
                if (!this.wasDown) {
                    // this.graphic.moveTo(this.game.input.x, this.game.input.y);
                    //this.blackoutGraphic();
                    if (this.path != null)
                        this.showPoints();
                    this.bmd.clear();
                    this.bmd.ctx.beginPath();
                    this.bmd.ctx.moveTo(this.game.input.x, this.game.input.y);
                    this.bmd.ctx.lineWidth = 2;
                    this.bmd.ctx.stroke();
                    this.bmd.dirty = true;
                    this.pathIndex = 0;
                    this.pathSpriteIndex = 0;
                    this.path = [];
                    this.wasDown = true;
                }
                console.log("klikniete1");
                if (this.pathIndex == 0 || (this.path[this.pathIndex - 1].x != this.game.input.x || this.path[this.pathIndex - 1].y != this.game.input.y)) {
                    //   this.graphic.moveTo(this.game.input.x, this.game.input.y);
                    console.log(this.game.input.x, this.game.input.y);
                    this.bmd.ctx.lineTo(Math.floor(this.game.input.x), Math.floor(this.game.input.y));
                    this.bmd.ctx.stroke();
                    this.bmd.render();
                    this.bmd.dirty = true;
                    //   this.bmd.refreshBuffer();
                    // this.graphic.drawRect(0, 0, this.game.width, this.game.height);
                    this.path[this.pathIndex] = new Phaser.Point(this.game.input.x, this.game.input.y);
                    this.pathIndex++;
                }
            }
            else {
                this.wasDown = false;
            }
            /*   if (this.path != null && this.path.length > 0 && this.pathSpriteIndex < this.pathIndex) {
                   this.pathSpriteIndex = Math.min(this.pathSpriteIndex, this.path.length - 1);
                   this.game.physics.arcade.moveToXY(this.sprite, this.path[this.pathSpriteIndex].x, this.path[this.pathSpriteIndex].y, 250);
                   if (this.game.physics.arcade.distanceToXY(this.sprite, this.path[this.pathSpriteIndex].x, this.path[this.pathSpriteIndex].y) < 20) {
                       this.pathSpriteIndex++;
                       if (this.pathSpriteIndex >= this.pathIndex) {
                           this.sprite.body.velocity.setTo(0, 0);
                       }
                   }*/
        };
        return Level;
    })(Phaser.State);
    Path.Level = Level;
})(Path || (Path = {}));
//# sourceMappingURL=game.js.map