﻿module Path {

    export class Level extends Phaser.State {

        private sprite = null;
        private graphic = null;
        private wasDown = false;

        private sprite2 = null;
        private bmd = null;
        private path = null;
        private pathIndex = -1;
        private pathSpriteIndex = -1;

        blackoutGraphic() {
            this.graphic.clear();
            this.graphic.beginFill(0x000000);
            //  this.graphic.lineStyle(4, 0x000000, 1);
            //    this.graphic.drawRect(0, 0, this.game.width, this.game.height);
            this.graphic.endFill();
            this.graphic.lineStyle(4, 0xFF0000, 1);

            //  this.graphic.moveTo(100,100);
            //  this.graphic.lineTo(100, 100);
            //\  this.graphic.lineTo(200, 200);
        }

        preload() {
            this.game.load.image('star', 'star.png');
        }


        create() {

            this.bmd = this.game.add.bitmapData(800, 600);
            var color = 'red';

            this.bmd.ctx.beginPath();
            this.bmd.ctx.lineWidth = "4";
            this.bmd.ctx.strokeStyle = color;
            this.bmd.ctx.stroke();
            this.sprite2 = this.game.add.sprite(0, 0, this.bmd); 
            // this.graphic = this.game.add.graphics(0, 0);
            //this.blackoutGraphic();
            this.sprite = this.game.add.sprite(100, 100, 'star');
            this.sprite.anchor.setTo(0.5, 0.5);
        }

        showPoints() {

            var str: string = "new Array(";
            for (var i: number = 0; i < this.path.length; i++) {

                var line: string = "{ x : " + this.path[i].x + ", y : " + this.path[i].y + "}";
                var przecinek = ",\n";
                if (i == this.path.length - 1) przecinek = "\n);";

                str += (line + przecinek);
            }
            console.log(str);
            /*

            new Array({x : 20,y:20});

            */

        }

        update() {
          
        if (this.game.input.mousePointer.isDown) {
            if (!this.wasDown) {
               
               // this.graphic.moveTo(this.game.input.x, this.game.input.y);
              
                //this.blackoutGraphic();
                if (this.path != null) this.showPoints();
                this.bmd.clear();
                this.bmd.ctx.beginPath();
                this.bmd.ctx.moveTo(this.game.input.x, this.game.input.y);
                this.bmd.ctx.lineWidth = 2;
                this.bmd.ctx.stroke();
                this.bmd.dirty = true;
               
                this. pathIndex = 0;
                this.pathSpriteIndex = 0;
                this.path = [];
                this.wasDown = true;
            }

            console.log("klikniete1");
           

            if (this.pathIndex == 0 || (this.path[this.pathIndex - 1].x != this.game.input.x || this.path[this.pathIndex - 1].y != this.game.input.y)) {
             //   this.graphic.moveTo(this.game.input.x, this.game.input.y);
                console.log(this.game.input.x, this.game.input.y);
                this.bmd.ctx.lineTo(Math.floor(this.game.input.x), Math.floor(this.game.input.y));
                this.bmd.ctx.stroke();
                this.bmd.render();
                this.bmd.dirty = true;  
             //   this.bmd.refreshBuffer();
               // this.graphic.drawRect(0, 0, this.game.width, this.game.height);
                this.path[this.pathIndex] = new Phaser.Point(this.game.input.x, this.game.input.y)
                this.pathIndex++;
            }
        } else {
            this.wasDown = false;
        }
     /*   if (this.path != null && this.path.length > 0 && this.pathSpriteIndex < this.pathIndex) {
            this.pathSpriteIndex = Math.min(this.pathSpriteIndex, this.path.length - 1);
            this.game.physics.arcade.moveToXY(this.sprite, this.path[this.pathSpriteIndex].x, this.path[this.pathSpriteIndex].y, 250);
            if (this.game.physics.arcade.distanceToXY(this.sprite, this.path[this.pathSpriteIndex].x, this.path[this.pathSpriteIndex].y) < 20) {
                this.pathSpriteIndex++;
                if (this.pathSpriteIndex >= this.pathIndex) {
                    this.sprite.body.velocity.setTo(0, 0);
                }
            }*/
        

    }

    }

} 