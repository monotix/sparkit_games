﻿module BeachBuilder {
    export class Level1 extends Phaser.State {

        private _level1Gr: Phaser.Group;
        private _dragPeople: PeopleToDrag;
        private _dragItems: ItemsToDrag;
        private _collisonObject: ObjectToDrop;
        private itemsToDropArray: Array<ObjectToDrop> = new Array();

        private audioArray: Array<Phaser.Sound> = new Array();
        private _actualAudioPlay: Phaser.Sound;
        private poepleArray: Array<PlayerAnim> = new Array();
        private _dayNight: DayNight;
        private _timeOfDayGr: Phaser.Group;
        private _maxDropObject: number;
        private _buttonsInGame: Phaser.Group;
        private _solutions: Solutions;
        private _checkCorrect: CheckCorect;
        private _gameControler: GameControler;
        private textLevel: Phaser.Sprite;
        private textLevelGr: Phaser.Group;
        private firtsTime: boolean = true;
        public scoreText: Phaser.Text;
        private blockScreen: Phaser.Sprite;
        private _overButtonA;
        private _clearButtonA;
        private audoBadAnswerArray: Array<Phaser.Sound>;
        private failArray: Array<Phaser.Sprite>
        private congratsAudioArray: Array<Phaser.Sound> = new Array();
        private successAnimArray: Array<Phaser.Sprite> = new Array();
        private scoreScreenSpr: Phaser.Sprite;
        private scoreScreenGr: Phaser.Group;
        private fail: Phaser.Sprite;
        private _xml: XmlControl;
        private textDescription: Phaser.Text;
        private textDescdoneBtn: Phaser.Button;
        private succesAnimatioScreen: Phaser.Sprite;
        private wspolne: Array<any> = new Array();
        private helpGr: Phaser.Group;
        private _helpBtn: Phaser.Button;
        private _finishGr: Phaser.Group;
        private _instructionShow;
        preload() {


        }



        create() {


            /*   audio set lessons */
            /*
                        for (var i: number = 0; i < 20; i++) {
            
                            this.audioArray.push(this.game.add.audio("lessonsA" + i, 1, false));
            
            
                        }
                        */
            this.audoBadAnswerArray = new Array();
            this.audoBadAnswerArray.push(this.game.add.audio("failure1", 1, false));
            this.audoBadAnswerArray.push(this.game.add.audio("failure2", 1, false));


            for (var i: number = 1; i < 5; i++) {

                this.successAnimArray.push(new Phaser.Sprite(this.game, 0, 0, "success" + i, 0));




            }

            for (var i: number = 1; i < 5; i++) {

                this.congratsAudioArray.push(this.game.add.audio("congrats" + i, 1, false));


            }

            /* end audio set lessons */



            this.failArray = new Array();
            for (var i: number = 1; i < 6; i++) {

                this.failArray.push(new Phaser.Sprite(this.game, 0, 0, "fail" + i, 0));



            }

            /* audio */

            this._overButtonA = this.game.add.audio("overButtonA", 1, false);
         
          
            // this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            this._clearButtonA = this.game.add.audio("clearButtonA", 1, false);
          

            /*end audio*/


            this._solutions = Solutions.getInstance();
            this._checkCorrect = new CheckCorect();
            this._checkCorrect.incomon = "table";
            this._gameControler = GameControler.getInstance();

            this._gameControler.setGameState(this);


            this._xml = XmlControl.getInstance();
            console.log(this._xml);

            var parser = new DOMParser();
            var parsedXml: XMLDocument;
            var xmlLoaded = this.cache.getText('xml');

            parsedXml = parser.parseFromString(xmlLoaded, "application/xml");

            this._xml.xml = parsedXml;
            this._gameControler.maxLevel = this._xml.getMaxLevel();
            this._gameControler.setMaxScene(this._xml.getMaxRandomScenes());
            this._gameControler.randomGame();


            this._xml.getScene(this._gameControler.actualRandomTextId);
            //   this._actualAudioPlay = new Phaser.Sound(;
         
            //this._actualAudioPlay = this.audioArray[this._gameControler.actualRandomTextId];
            
            //  this._actualAudioPlay.play();

            this.textLevelGr = this.add.group();
            var bgTextLevel = new Phaser.Graphics(this.game, 0, 0);
            bgTextLevel.beginFill(0xFFFFFF, 1);
            bgTextLevel.drawRect(0, 0, this.game.width, this.game.height);
            bgTextLevel.endFill();





            this.textLevelGr.addChild(bgTextLevel);
            this.textLevel = this.add.sprite(293, 165, 'TextLevel1', 0, this.textLevelGr);
            //  this.textLevel.anchor.setTo(0.5, 0.5);

            this.textLevel.x = this.game.width / 2 - this.textLevel.width / 2;
            this.textLevel.y = this.game.height / 2 - this.textLevel.height / 2;





            this.textDescdoneBtn = this.game.add.button(323, 275, "doneBtn", this.doneBtnClick, this, 1, 0, 0, 0, this.textLevelGr);
            this.textDescdoneBtn.onInputDown.add(this.doneBtnOver, this);
            this.textDescdoneBtn.inputEnabled = true;
            this.textDescdoneBtn.input.priorityID = 2;

            this.setTextDescription();
            //s  doneBtn.x += textLevel.x;
          

            //interctive object set x,y 
          
            //  this.blockScreen.addChild(blokScreenSprite);
            
            //this.stage.removeChild(this.blockScreen);
            this.scoreScreenGr = this.add.group();

            this.scoreScreenSpr = this.game.add.sprite(0, 0, "scoreScreen", 0, this.scoreScreenGr);
            this.scoreScreenSpr.animations.add("score");
            this.scoreScreenSpr.animations.play("score", 48, true);
            var scorePlay = this.game.add.button(0, 0, "playScoreScreen", this.playAgain, this, 1, 0, 1, 0, this.scoreScreenGr);
            var quit = this.game.add.button(300, 10, "playScoreScreen", this.quit, this, 3, 2, 3, 2, this.scoreScreenGr);
            scorePlay.inputEnabled = true;
            scorePlay.input.priorityID = 3;
            quit.inputEnabled = true;
            quit.input.priorityID = 3;
            this.scoreScreenSpr.addChild(scorePlay);
            this.scoreScreenSpr.addChild(quit);
            scorePlay.x = 257;
            scorePlay.y = 358;
            quit.x = 427;
            quit.y = 358;

            this.scoreScreenGr.x = - this.scoreScreenGr.width;
            this.scoreScreenGr.y = - this.scoreScreenGr.height;
            this.scoreScreenGr.visible = false;
            this.stage.removeChild(this.scoreScreenGr);

            var blokScreenSprite: Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
            blokScreenSprite.beginFill(0x000000, 0);
            blokScreenSprite.drawRect(0, 0, 800, 600);
            blokScreenSprite.endFill();
            this.blockScreen = new Phaser.Sprite(this.game, 0, 0);

            this.blockScreen.addChild(blokScreenSprite);

            this.blockScreen.addChild(this.blockScreen);
            this.stage.addChild(this.blockScreen);
            

            //spr.buttonMode = true;
            //  spr.cacheAsBitmap = true;
           

            this.helpGr = this.add.group();
            this.stage.addChild(this.helpGr);
            this._helpBtn = this.add.button(0, 0, "helpBtn", this.helpBtnClick, this, 1, 0, 0, 0, this.helpGr);
            this.helpGr.x =this.game.stage.width - this.helpGr.width;
            this.helpGr.y = 0;

            this._helpBtn.buttonMode = true;
            this._helpBtn.inputEnabled = true;
            this._helpBtn.input.priorityID = 11;



            this.game.input.onDown.add(function () { if (this.game.paused) { this.game.paused = false; } if (this._instructionShow) { this.stage.removeChild(this._instructionShow); this._instructionShow.kill(); this._instructionShow = null; } }, this);
            

        }

        private helpBtnClick() {

            console.log("pokaz instrukcje");
            this.game.paused = !this.game.paused;

            if (this._instructionShow == null) this._instructionShow = this.add.image(0, 0, "InstructionsScreen", 0);
            

            // this.add.image(0, 0, "firstInstruction", 0);
            this._instructionShow.width = this.game.world.width;//this.stage.width;
            this._instructionShow.height = this.game.world.height;
            this.stage.addChild(this._instructionShow);

        }


        private setTextDescription() {

            var text = this._xml.getDescription(this._gameControler.actualRandomTextId);
            var style = { font: "35px Arial", fill: "#ffffff", align: "center" };
            if (this.textDescription) this.textDescription.destroy();
            this.textDescription = new Phaser.Text(this.game, 0, 0, text, style);
            this.textDescription.wordWrap = true;
            this.textDescription.wordWrapWidth = 600;

            this.textDescription.x = this.game.width / 2 - this.textDescription.width / 2;
            this.textDescription.y = this.game.height / 2 - this.textDescription.height / 2;

            this.textLevelGr.addChild(this.textDescription);

            this.textDescdoneBtn.y = this.textDescription.y + this.textDescription.height + 20;


        }

        playAgain() {
            console.log("playagain");
            this.stage.removeChild(this.succesAnimatioScreen);
            this.onBlockScreen(false);

            this.scoreScreenSpr.inputEnabled = false;
            //  this.scoreScreenSpr.input.priorityID = 0;
            this.scoreScreenGr.visible = false;
            this.stage.removeChild(this.scoreScreenGr);


            this._gameControler.defaultScore();
            this.restart();
            this.restartNextLevel();


        }



        quit() {
            /*  this.onBlockScreen(false);
              this.scoreScreenGr.x = - this.scoreScreenGr.width;
              this.scoreScreenGr.y = - this.scoreScreenGr.height;
              this.scoreScreenSpr.inputEnabled = false;
              this.scoreScreenSpr.input.priorityID = 0;
              this.scoreScreenGr.visible = false;
              this.stage.removeChild(this.scoreScreenGr);
              this.game.state.start("MainMenu", true, false);*/
        }

        private doneBtnOver() {

            this._overButtonA.play();
        }

        clearOver() {
            this._clearButtonA.play();

        }

        doneBtnClick() {


            if (this.firtsTime) {
                this.playFirstTime();
                this.firtsTime = false;
            }
            else {

                this.textLevelGr.x = - this.textLevelGr.width;
                this.textLevelGr.y = - this.textLevelGr.height;
                this.textLevelGr.visible = false;

            }

            // this._actualAudioPlay.stop();




        }


        public repeatLesson() {

            this.onBlockScreen(true);

            this.audoBadAnswerArray[this._gameControler.actualBadAnswer - 1].onStop.add(this.finishAudioBadAnswer, this);
            this.audoBadAnswerArray[this._gameControler.actualBadAnswer - 1].play();
            //  this.game.time.events.loop(this.audoBadAnswerArray[0].totalDuration, this.finishAudioBadAnswer, this);
            //  this.finishAudioBadAnswer();  

        }

        onBlockScreen(show: boolean) {
            if (show) {
                this.blockScreen.inputEnabled = true;
                this.blockScreen.input.priorityID = 1;
                this.stage.addChild(this.blockScreen);
            }
            else {
                this.blockScreen.inputEnabled = false;
                this.blockScreen.input.priorityID = 0;
                this.stage.removeChild(this.blockScreen);
            }

        }


        finishAudioBadAnswer() {

            this.restart();

            // this.textLevel.frame = this._gameControler.actualRandomTextId;
           
            this._actualAudioPlay = this.audioArray[0];
          
            //this._actualAudioPlay.play();
            this.textLevelGr.visible = true;

            this.textLevelGr.x = 0;
            this.textLevelGr.y = 0;
            this.stage.addChild(this.textLevelGr);
            this.onBlockScreen(false);

        }


        playFirstTime() {

          



            /* buttony play clear */
            this._buttonsInGame = this.add.group();
            var doneInGame: Phaser.Button = this.add.button(0, 0, "doneInGameBtn", this.doneInGameClick, this, 1, 0);
            doneInGame.onInputDown.add(this.clearOver, this);
            var clearBtn: Phaser.Button = this.add.button(0, 0, "clearBtn", this.clearBtnClick, this, 1, 0);
            clearBtn.onInputDown.add(this.doneBtnOver, this);
            clearBtn.y = doneInGame.y + doneInGame.height - 10;
            this._buttonsInGame.addChild(doneInGame);

            this._buttonsInGame.addChild(clearBtn);
            //this.stage.addChild(this._buttonsInGame);
            this._buttonsInGame.x = 0;
            this._buttonsInGame.y = 420;


            /* end buttony play clear */

            this._dayNight = new DayNight(this.game, 0, 0, "sunBg", "moonBg", "sunsetBg")

            this._level1Gr = this.add.group();
            //this.stage.addChild(this._level1Gr);
          
            this._level1Gr.addChild(this._dayNight);
            this._level1Gr.addChild(this._buttonsInGame);
            var style = { font: "16px Arial", fill: "#000000", align: "left" };
            this.scoreText = new Phaser.Text(this.game, 0, 0, "Credits:0      Scene: 1/5  ", style);
            this._level1Gr.addChild(this.scoreText);
            var animNameArray: Array<string> = ArrayStatic.animNameArray;
            var itemsDragNameArray: Array<string> = ArrayStatic.itemsDragNameArray;
            var itemsDragPositionArray: Array<Phaser.Point> = new Array(new Phaser.Point(564, 466), new Phaser.Point(559, 511), new Phaser.Point(565, 570), new Phaser.Point(639, 469), new Phaser.Point(635, 516), new Phaser.Point(638, 566));

            var itemsDragArray: Array<ItemsToDrag> = new Array();


            

            var maxLengthItems: number = itemsDragNameArray.length;

            for (var i: number = 0; i < maxLengthItems; i++) {
                var p: Phaser.Point = new Phaser.Point(itemsDragPositionArray[i].x, itemsDragPositionArray[i].y);
                var itemToDrag: ItemsToDrag = new ItemsToDrag(this.game, this, p, new Phaser.Point(56, 45), itemsDragNameArray[i], i, itemsDragNameArray[i]);
                this._level1Gr.addChild(itemToDrag);
            }



            var peopleNameArray: Array<string> = new Array("ridl", "erad", "tan", "kaz");
            var scaleArray: Array<number> = new Array(0.85, 0.81, 0.84, 0.95);
            var peoplePositionArray: Array<Phaser.Point> = new Array(new Phaser.Point(155, 490), new Phaser.Point(247, 493), new Phaser.Point(341, 496), new Phaser.Point(425, 495));

            var animNameArray: Array<string> = ArrayStatic.animNameArray;
            /* tworzenie ludzi plus dodanie animacji */
            var maxPeopleLength: number = peopleNameArray.length;
            for (var j: number = 0; j < maxPeopleLength; j++) {
                var p: Phaser.Point = new Phaser.Point(peoplePositionArray[j].x, peoplePositionArray[j].y);
                var peopleToDrag: PlayerAnim = new PlayerAnim(this.game, this, p, new Phaser.Point(76, 147), peopleNameArray[j] + animNameArray[0], j, peopleNameArray[j], scaleArray[j]);
                this.poepleArray.push(peopleToDrag);
                this._level1Gr.addChild(peopleToDrag);
                /*dodanie animacji kolejnych, po default */
                for (var l: number = 1; l < 7; l++) {

                    peopleToDrag.addAnimation(new Phaser.Sprite(this.game, 0, 0, peopleNameArray[j] + animNameArray[l], 0), animNameArray[l]);

                }

            }

            /* tworzenie desek jako ludzi*/

            var skateBoard: PlayerAnim = new PlayerAnim(this.game, this, new Phaser.Point(740, 480), new Phaser.Point(76, 147), "skateboard1", 4, "skateboard1", 1, true);
            var skateBoard1: PlayerAnim = new PlayerAnim(this.game, this, new Phaser.Point(740, 509), new Phaser.Point(76, 147), "skateboard2", 5, "skateboard2", 1, true);
            var skateBoard2: PlayerAnim = new PlayerAnim(this.game, this, new Phaser.Point(740, 536), new Phaser.Point(76, 147), "skateboard3", 6, "skateboard3", 1, true);
            var skateBoard3: PlayerAnim = new PlayerAnim(this.game, this, new Phaser.Point(740, 563), new Phaser.Point(76, 147), "skateboard4", 7, "skateboard4", 1, true);


            this._level1Gr.addChild(skateBoard);
            this._level1Gr.addChild(skateBoard1);
            this._level1Gr.addChild(skateBoard2);
            this._level1Gr.addChild(skateBoard3);

            this.poepleArray.push(skateBoard);
            this.poepleArray.push(skateBoard1);
            this.poepleArray.push(skateBoard2);
            this.poepleArray.push(skateBoard3);

            var itemsToDropNameArray: Array<string> = ArrayStatic.itemsToDropNameArray;
            var itemsToDropPositionArray: Array<Phaser.Point> = new Array(new Phaser.Point(82,205), new Phaser.Point(176, 183), new Phaser.Point(84, 302), new Phaser.Point(622, 188), new Phaser.Point(752, 196), new Phaser.Point(732,307), new Phaser.Point(303, 293), new Phaser.Point(496, 295));
            var itemsToDropPositionPeopleArray: Array<Phaser.Point> = new Array(new Phaser.Point(60, 142), new Phaser.Point(174, 120), new Phaser.Point(75, 282), new Phaser.Point(592, 140), new Phaser.Point(713, 150), new Phaser.Point(692, 270), new Phaser.Point(323, 268), new Phaser.Point(496, 263));

            new Array("redTree", "orangeTree", "benchLeft", "blueTree", "greenTree", "rightBench", "fountainHighlight", "fountainHighlight2");

            var maxLengthItemsDrop: number = itemsToDropNameArray.length;
            this._maxDropObject = maxLengthItemsDrop;
            for (var i: number = 0; i < maxLengthItemsDrop; i++) {
                var p: Phaser.Point = new Phaser.Point(itemsToDropPositionArray[i].x, itemsToDropPositionArray[i].y);
                var objectToDrop: ObjectToDrop = new ObjectToDrop(this.game, this, p, itemsToDropNameArray[i], 0, new Phaser.Point(139, 128), i, 1, itemsToDropPositionPeopleArray[i], itemsToDropNameArray[i]);
                this._level1Gr.addChild(objectToDrop);
                this.itemsToDropArray.push(objectToDrop);
            }
          //  this.itemsToDropArray[6].name = "table";
          //  this.itemsToDropArray[7].name = "table";
            this.itemsToDropArray[6].isAlpha = true;
            this.itemsToDropArray[7].isAlpha = true;
            this.wspolne.push(this.itemsToDropArray[6]);
            this.wspolne.push(this.itemsToDropArray[7]);


           

            this._timeOfDayGr = this.add.group();
            // this.add.sprite(50, 0, "bgLevel1", this._timeOfDayGr);

            var sunBtn: TimeOfDayBtn = new TimeOfDayBtn(this.game, this, 0, 0, "day", 0);
            var sunsetBtn: TimeOfDayBtn = new TimeOfDayBtn(this.game, this, 0, 0, "sunset", 1);
            var nightBtn: TimeOfDayBtn = new TimeOfDayBtn(this.game, this, 0, 0, "moon", 2);
            this._timeOfDayGr.addChild(sunBtn);
            var margin: number = 5;
            sunsetBtn.x = sunBtn.x + sunBtn.width + margin;
            this._timeOfDayGr.addChild(sunsetBtn);

            nightBtn.x = sunsetBtn.x + sunsetBtn.width + margin;
            this._timeOfDayGr.addChild(nightBtn);
            this._timeOfDayGr.x = 720;
            this._timeOfDayGr.y = 442;
            this._level1Gr.addChild(this._timeOfDayGr);
            

            


            /* var _redChair: Phaser.Sprite = this.add.sprite(13, 87, "redChair", 0, this._level1Gr);
             var _blueChair: Phaser.Sprite = this.add.sprite(326, 253, "blueChair", 0, this._level1Gr);
             var _greenChair: Phaser.Sprite = this.add.sprite(568, 120, "greenChair", 0, this._level1Gr);
            // _redChair.anchor.setTo(0.5, 0.5);
             _blueChair.anchor.setTo(0.5, 0.5);
             _greenChair.anchor.setTo(0.5, 0.5);
             _redChair.events.onDragStart.add(this.redChairDragStart, this);
             _redChair.inputEnabled = true;
             _redChair.input.enableDrag();
            /* this._redChair.onDragStart.add(this.onStartDrag, this);
             this._redChair.onDragStop.add(this.onStopDrag, this);*/
            // this._level1Gr.addChild(_redChair);*/
           
          



        }

        public nextLevel() {

            console.log("next level");
            this.onBlockScreen(true);
            var losowanieAnim = this._gameControler.randomNum(this.successAnimArray.length);

            this.succesAnimatioScreen = this.successAnimArray[losowanieAnim];
            this.succesAnimatioScreen.x = this.game.width / 2 - this.succesAnimatioScreen.width / 2;
            this.succesAnimatioScreen.y = this.game.height / 2 - this.succesAnimatioScreen.height / 2;
            this.stage.addChild(this.succesAnimatioScreen);
            var anim = this.succesAnimatioScreen.animations.add("animAgain");
            anim.onComplete.add(function () { this.game.time.events.add(Phaser.Timer.SECOND * 2, this.restartNextLevel, this) }, this);
            this.succesAnimatioScreen.animations.play("animAgain", 24, false, false);
          
            //  this.congratsAudioArray[this._gameControler.randomNum(this.congratsAudioArray.length)].onStop.add(this.finishAudioBadAnswer, this);
            var jakaLiczba = this._gameControler.randomNum(this.congratsAudioArray.length);
            console.log("jaka   liczba dla audio gratulacji", jakaLiczba);
            this.congratsAudioArray[jakaLiczba].play();





        }

        restartNextLevel() {
            console.log("restartNextLevel");
            this.stage.removeChild(this.succesAnimatioScreen);
            this.succesAnimatioScreen = null;
            this.onBlockScreen(false);
            this.restart();

            console.log("ktory level", this._gameControler.countLevel, this._xml.levelNum);
            this._xml.levelNum = this._gameControler.countLevel;
            this._gameControler.maxLevel = this._xml.getMaxLevel();
            this._gameControler.setMaxScene(this._xml.getMaxRandomScenes());
            this._gameControler.randomGame();


            this._xml.getScene(this._gameControler.actualRandomTextId);

            this.setTextDescription();
            //  console.log("audio play", this.audioArray[this._gameControler.actualRandomTextId], this._gameControler.actualRandomTextId);
            //  this._actualAudioPlay = this.audioArray[0];
            //   this._actualAudioPlay.play();
            this.textLevelGr.visible = true;
            this.textLevelGr.x = 0;
            this.textLevelGr.y = 0;
            this.stage.addChild(this.textLevelGr);

        }

        doneInGameClick() {
            var prawda = this._checkCorrect.checkAnswers(this.itemsToDropArray, this.poepleArray, this._dayNight.answer, this.wspolne);
            console.log("prawidlowa odpowiedz?", prawda);
            if (prawda) {

                this._gameControler.corectAnswer();

            }
            else {
                this._gameControler.badAnswer();

            }

        }


        clearBtnClick() {

            this.restart();

        }

        private restart() {


            for (var i: number = 0; i < this._maxDropObject; i++) {

                this.itemsToDropArray[i].restart();

            }

            for (var i: number = 0; i < this.poepleArray.length; i++) {

                this.poepleArray[i].restart();

            }

            this._dayNight.changeTimeOfDay(0);

        }


        public changeTimeOfDay(id: number) {

            console.log("change time of day", id);
            this._dayNight.changeTimeOfDay(id);

        }


        public itemsOnDragStop(items: ItemsToDrag) {

            var isPass: boolean = false;

            var dragPoint: Phaser.Point = new Phaser.Point(this._dragItems.x, this._dragItems.y);
            for (var i: number = 0; i < 4; i++) {

                if (this.poepleArray[i].isDroped && Phaser.Rectangle.contains(this.poepleArray[i]._rectangle, dragPoint.x, dragPoint.y)) {

                    this.poepleArray[i].playAnimation(items.id);

                }



            }


            this._dragItems.x = this._dragItems.startX;
            this._dragItems.y = this._dragItems.startY;

            this._dragItems = null;

        }

        public itemsOnDragStart(items: ItemsToDrag) {

            this._dragItems = items;
            items.bringToTop();

        }

        public peopleOnDragStart(items: PlayerAnim) {
            this._dragPeople = items;

            items.bringToTop();

        }

        public peopleOnDragStop(items: PlayerAnim) {


            var isPass: boolean = false;
            for (var i: number = 0; i < this._maxDropObject; i++) {


                if (this.itemsToDropArray[i].isEmpty && Phaser.Rectangle.contains(this.itemsToDropArray[i]._rectangle, this._dragPeople.x, this._dragPeople.y)) {
                    this.itemsToDropArray[i].changeFrame(0);
                    this._dragPeople.startX = this._dragPeople.x = this.itemsToDropArray[i].positionPeople.x;
                    this._dragPeople.startY = this._dragPeople.y = this.itemsToDropArray[i].positionPeople.y
                    if (this._dragPeople.isSkateBoard) {
                        this._dragPeople.startY = this._dragPeople.y = this._dragPeople.y+ 30;
                    }
                    this._dragPeople.checkLastObjectToDrop(this.itemsToDropArray[i]);
                    isPass = true;
                    this.itemsToDropArray[i].isEmpty = false;
                    this.itemsToDropArray[i].answer = this._dragPeople.name;
                    this.itemsToDropArray[i].dropedObjectName = this.itemsToDropArray[i].ownName;
                    items.playAnim();
                }
                else {

                    this.itemsToDropArray[i].changeFrame(0);
                }

            }

            if (!isPass) {
                this._dragPeople.x = this._dragPeople.startX;
                this._dragPeople.y = this._dragPeople.startY;
            }
            this._dragPeople = null;
        }

        update() {

            if (this._dragPeople != null) {
                var dragPoint: Phaser.Point = new Phaser.Point(this._dragPeople.x, this._dragPeople.y);
                for (var i: number = 0; i < this._maxDropObject; i++) {

                    if (this.itemsToDropArray[i].isEmpty && Phaser.Rectangle.contains(this.itemsToDropArray[i]._rectangle, dragPoint.x, dragPoint.y)) {

                        this.itemsToDropArray[i].changeFrame(1)
                    }
                    else {
                        this.itemsToDropArray[i].changeFrame(0);

                    }


                }

            }



        }

        public finishLevelAnim() {





        }


        public youWin(finishLevel: boolean = false) {

            //this.add.image(0, 0, "youWin");
            console.log("you win");
            this.onBlockScreen(true);
            this.succesAnimatioScreen = this.successAnimArray[this._gameControler.randomNum(this.successAnimArray.length)];
            this.succesAnimatioScreen.x = this.game.width / 2 - this.succesAnimatioScreen.width / 2;
            this.succesAnimatioScreen.y = this.game.height / 2 - this.succesAnimatioScreen.height / 2;
            this.stage.addChild(this.succesAnimatioScreen);
            var anim = this.succesAnimatioScreen.animations.add("animAgain");
            if (finishLevel) {
                anim.onComplete.add(function () { this.game.time.events.add(Phaser.Timer.SECOND * 2, this.restartNextLevel, this) }, this);
            }
            else {
                anim.onComplete.add(function () { this.game.time.events.add(Phaser.Timer.SECOND * 2, this.playAgain, this) }, this);

            }
            this.succesAnimatioScreen.animations.play("animAgain", 24, false, false);
          
            //  this.congratsAudioArray[this._gameControler.randomNum(this.congratsAudioArray.length)].onStop.add(this.finishAudioBadAnswer, this);
            this.congratsAudioArray[this._gameControler.randomNum(this.congratsAudioArray.length)].play();

        }

        public finishGame() {

            this.onBlockScreen(true);
            this.succesAnimatioScreen = this.successAnimArray[this._gameControler.randomNum(this.successAnimArray.length)];
            this.succesAnimatioScreen.x = this.game.width / 2 - this.succesAnimatioScreen.width / 2;
            this.succesAnimatioScreen.y = this.game.height / 2 - this.succesAnimatioScreen.height / 2;
            this.stage.addChild(this.succesAnimatioScreen);
            var anim = this.succesAnimatioScreen.animations.add("animAgain");

            anim.onComplete.add(function () { this.game.time.events.add(Phaser.Timer.SECOND * 2, this.animationStopped, this) }, this);

            this.succesAnimatioScreen.animations.play("animAgain", 24, false, false);
          
            //  this.congratsAudioArray[this._gameControler.randomNum(this.congratsAudioArray.length)].onStop.add(this.finishAudioBadAnswer, this);
            this.congratsAudioArray[this._gameControler.randomNum(this.congratsAudioArray.length)].play();


        }



        public youLose() {

            this.onBlockScreen(true);
            this.fail = this.failArray[this._gameControler.randomNum(this.failArray.length)];
            this.fail.x = this.game.width / 2 - this.fail.width / 2;
            this.fail.y = this.game.height / 2 - this.fail.height / 2;
            this.stage.addChild(this.fail);
            var anim = this.fail.animations.add("anim");
            anim.onComplete.add(function () { this.game.time.events.add(Phaser.Timer.SECOND * 2, this.animationStopped, this) }, this);
            this.fail.animations.play("anim", 24, false, false);
            //this.game.time.events.add(Phaser.Timer.SECOND * 6, this.animationStopped, this);
           
            //this.add.image(0, 0, "youLose");
        }

        animationStopped() {
            this.stage.removeChild(this.fail);
            this.scoreScreenGr.x = this.world.width / 2 - this.scoreScreenGr.width / 2;
            this.scoreScreenGr.y = this.world.height / 2 - this.scoreScreenGr.height / 2;
            this.scoreScreenSpr.inputEnabled = true;
            this.scoreScreenSpr.input.priorityID = 2;
            this.scoreScreenGr.visible = true;
            this.stage.addChild(this.scoreScreenGr);
        }

        public scoreScreen() {



        }




    }

}