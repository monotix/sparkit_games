var Greeter = (function () {
    function Greeter(element) {
        this.element = element;
        this.element.innerHTML += "The time is: ";
        this.span = document.createElement('span');
        this.element.appendChild(this.span);
        this.span.innerText = new Date().toUTCString();
    }
    Greeter.prototype.start = function () {
        var _this = this;
        this.timerToken = setInterval(function () { return _this.span.innerHTML = new Date().toUTCString(); }, 500);
    };
    Greeter.prototype.stop = function () {
        clearTimeout(this.timerToken);
    };
    return Greeter;
})();
window.onload = function () {
    var el = document.getElementById('content');
    var greeter = new Greeter(el);
    greeter.start();
};
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var BeachBuilder;
(function (BeachBuilder) {
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            _super.apply(this, arguments);
        }
        Boot.prototype.preload = function () {
            this.load.image("bg", "assets/SplashScreen2.png");
            this.load.image("progressBar", "assets/preloader/ProgressBar.png");
            this.load.image("progressBarBlue", "assets/preloader/ProgressBarBlue.png");
            //splashScreen
        };
        Boot.prototype.create = function () {
            this.game.state.start("Preloader", true, false);
        };
        return Boot;
    })(Phaser.State);
    BeachBuilder.Boot = Boot;
})(BeachBuilder || (BeachBuilder = {}));
var BeachBuilder;
(function (BeachBuilder) {
    var GameControler = (function () {
        function GameControler() {
            this.maxBadRepaets = 3;
            this.actualBadAnswer = 0;
            this.maxCorrectReapets = 5;
            this.actualCorectAnswe = 0;
            this.creditsAll = 0;
            this.maxCreditsForRound = 3;
            this.curentScene = 1;
            this.countLevel = 0;
            if (GameControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            GameControler._instance = this;
            this._solutions = BeachBuilder.Solutions.getInstance();
        }
        GameControler.getInstance = function () {
            if (GameControler._instance === null) {
                GameControler._instance = new GameControler();
            }
            return GameControler._instance;
        };
        GameControler.prototype.randomGame = function () {
            console.log("max scenes", this.maxRandomScene);
            var maxScene = this.maxRandomScene - 1;
            this.actualRandomTextId = this.randomScene();
            // this.drawedNumbers.push(this.actualRandomTextId);
            console.log("wylosowano scene", this.actualRandomTextId, this.drawedNumbers);
        };
        GameControler.prototype.setMaxScene = function (num) {
            this.maxRandomScene = num;
            this.drawedNumbers = new Array();
            for (var i = 0; i < num; i++) {
                this.drawedNumbers.push(i);
            }
        };
        GameControler.prototype.randomScene = function () {
            var losowanie = this.randomNum(this.drawedNumbers.length);
            var wylosowanaLiczba = this.drawedNumbers[losowanie];
            this.drawedNumbers.splice(losowanie, 1);
            console.log("losowanie sceny", wylosowanaLiczba, losowanie, this.drawedNumbers);
            return wylosowanaLiczba;
        };
        GameControler.prototype.sprawdzam = function () {
        };
        GameControler.prototype.corectAnswer = function () {
            this.actualCorectAnswe++;
            if (this.actualCorectAnswe > 4) {
                //this.maxLevel
                this.countLevel++;
                APIsetLevel(userID, gameID, this.countLevel + 2);
                this.defaultScore();
                console.log("Kolejny level", this.countLevel, this.curentScene);
                this._gameState.youWin(true);
            }
            else {
                this.creditsAll += this.maxCreditsForRound;
                this.curentScene++;
                this.changeScore();
                /* set max credit to default */
                this.maxCreditsForRound = 3;
                console.log("nextScene");
                this._gameState.nextLevel();
            }
        };
        GameControler.prototype.defaultScore = function () {
            this.actualBadAnswer = 0;
            //  this.creditsAll = 0;
            this.actualCorectAnswe = 0;
            this.curentScene = 1;
            this.changeScore();
        };
        GameControler.prototype.changeScore = function () {
            this._gameState.scoreText.text = "Credits " + this.creditsAll + "     Scenes: " + this.curentScene + "/5   Level: " + (this.countLevel + 1);
        };
        GameControler.prototype.badAnswer = function () {
            this.actualBadAnswer++;
            if (this.actualBadAnswer > 2) {
                //GAme Over
                this._gameState.youLose();
                this.actualBadAnswer = 0;
            }
            else {
                //play audio
                this._gameState.repeatLesson();
                this.maxCreditsForRound--;
            }
        };
        GameControler.prototype.setLevel = function (level_) {
            this.countLevel = level_;
        };
        GameControler.prototype.randomNum = function (max, min) {
            if (min === void 0) { min = 0; }
            return Math.floor(Math.random() * (max - min) + min);
        };
        GameControler.prototype.setGameState = function (value) {
            this._gameState = value;
        };
        GameControler.prototype.getGameState = function () {
            return this._gameState;
        };
        GameControler.prototype.setSound = function (sound_) {
            this.mainSound = sound_;
        };
        GameControler._instance = null;
        return GameControler;
    })();
    BeachBuilder.GameControler = GameControler;
})(BeachBuilder || (BeachBuilder = {}));
var BeachBuilder;
(function (BeachBuilder) {
    var XmlControl = (function () {
        function XmlControl() {
            this.levelNum = 0;
            if (XmlControl._instance) {
                throw new Error("Error: Instantiation failed: Use XmlControl.getInstance() instead of new.");
            }
            XmlControl._instance = this;
            /*  var parser = new DOMParser();
              var loadedXml: XMLDocument;
              var xml = this.cache.getText('xml');
              
              loadedXml = parser.parseFromString(xml, "application/xml");
  
              console.log("xml", loadedXml.querySelector("scene"));*/
        }
        Object.defineProperty(XmlControl.prototype, "xml", {
            get: function () {
                return this._xml;
            },
            set: function (xmlLoaded) {
                console.log(xmlLoaded);
                this._xml = xmlLoaded;
            },
            enumerable: true,
            configurable: true
        });
        XmlControl.prototype.getMaxLevel = function () {
            return this._xml.getElementsByTagName("level").length;
        };
        XmlControl.prototype.getDescription = function (id) {
            var maxItems = this._xml.getElementsByTagName("level")[this.levelNum].childNodes[id].childNodes.length;
            for (var i = 0; i < maxItems; i++) {
                if (this._xml.getElementsByTagName("level")[this.levelNum].childNodes[id].childNodes[i].nodeName == "desc") {
                    console.log(i, this._xml.getElementsByTagName("level")[this.levelNum].childNodes[id].childNodes[i].textContent);
                    return this._xml.getElementsByTagName("level")[this.levelNum].childNodes[id].childNodes[i].textContent;
                }
            }
            return "";
        };
        XmlControl.prototype.getScene = function (id) {
            var levelXml = this._xml.getElementsByTagName("level");
            console.log("w xml", this.levelNum);
            var childsceneList = levelXml.item(this.levelNum).childNodes;
            var maxItems = childsceneList[id].childNodes.length;
            var itemArray = new Array();
            console.log(maxItems, levelXml.item(this.levelNum).childNodes, childsceneList[id].childNodes, childsceneList[id].childNodes);
            for (var i = 0; i < maxItems; i++) {
                if (childsceneList[id].childNodes[i].nodeName == "item") {
                    var desc = new Array();
                    console.log("szukam bledu id", id, "child nodes id", i);
                    desc.push(childsceneList[id].childNodes[i].attributes.getNamedItem("location").nodeValue);
                    desc.push(childsceneList[id].childNodes[i].attributes.getNamedItem("people").nodeValue);
                    desc.push(childsceneList[id].childNodes[i].attributes.getNamedItem("action").nodeValue);
                    itemArray.push(desc);
                }
            }
            return itemArray;
        };
        XmlControl.prototype.getMaxRandomScenes = function () {
            console.log("ile plansz w levelu nr ", this.levelNum, this._xml.getElementsByTagName("level")[this.levelNum].childNodes.length);
            return this._xml.getElementsByTagName("level")[this.levelNum].childNodes.length;
        };
        XmlControl.prototype.timeOfDay = function (id) {
            var maxItems = this._xml.getElementsByTagName("scene")[id].childNodes.length;
            var itemArray = new Array();
            for (var i = 0; i < maxItems; i++) {
                if (this._xml.getElementsByTagName("scene")[id].childNodes[i].nodeName == "time") {
                    return this._xml.getElementsByTagName("scene")[id].childNodes[i].attributes.getNamedItem("id").nodeValue;
                }
            }
            return "";
        };
        XmlControl.getInstance = function () {
            if (XmlControl._instance === null) {
                XmlControl._instance = new XmlControl();
            }
            return XmlControl._instance;
        };
        XmlControl._instance = null;
        return XmlControl;
    })();
    BeachBuilder.XmlControl = XmlControl;
})(BeachBuilder || (BeachBuilder = {}));
var BeachBuilder;
(function (BeachBuilder) {
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            console.log("start");
            _super.call(this, 800, 600, Phaser.AUTO, 'content', null);
            this.state.add("Boot", BeachBuilder.Boot, false);
            this.state.add("Preloader", BeachBuilder.Preloader, false);
            this.state.add("MainMenu", BeachBuilder.MainMenu, false);
            this.state.add("PopupStart", BeachBuilder.PopupStart, false);
            this.state.add("Instruction", BeachBuilder.Instruction, false);
            this.state.add("Level1", BeachBuilder.Level1, false);
            this.state.start("Boot");
        }
        return Game;
    })(Phaser.Game);
    BeachBuilder.Game = Game;
})(BeachBuilder || (BeachBuilder = {}));
window.onload = function () {
    var game = new BeachBuilder.Game;
};
var userLevel = 1;
var BeachBuilder;
(function (BeachBuilder) {
    var ArrayStatic = (function () {
        function ArrayStatic() {
        }
        ArrayStatic.animNameArray = new Array("_default", "_player", "_phone", "_comic", "_gum", "_laptop", "_ice");
        ArrayStatic.itemsDragNameArray = new Array("cdPlayer", "cellPhone", "book", "gum", "laptop", "ice");
        ArrayStatic.itemsToDropNameArray = new Array("redTree", "orangeTree", "benchLeft", "blueTree", "greenTree", "rightBench", "fountainHighlight", "fountainHighlight2");
        return ArrayStatic;
    })();
    BeachBuilder.ArrayStatic = ArrayStatic;
})(BeachBuilder || (BeachBuilder = {}));
var BeachBuilder;
(function (BeachBuilder) {
    var CheckCorect = (function () {
        function CheckCorect() {
            this.randomLevel = 11;
            this._xml = null;
            this.solution = BeachBuilder.Solutions.getInstance();
            this._gameControl = BeachBuilder.GameControler.getInstance();
        }
        /*
            dupa
            podajelokacje
               sprawdzam czy ma odpowiedz czlowieka
                i czy czlowiek ma animacje

        */
        CheckCorect.prototype.checkPeopleAnim = function (who, action, peopleAction) {
            var max = peopleAction.length;
            for (var i = 0; i < max; i++) {
                //console.log(peopleAction[i].name, peopleAction[i].answer);
                if (peopleAction[i].name == who && peopleAction[i].answer == action) {
                    return true;
                }
            }
            return false;
        };
        CheckCorect.prototype.actionOfPeople = function (who, peopleAction) {
            var max = peopleAction.length;
            for (var i = 0; i < max; i++) {
                //console.log(peopleAction[i].name, peopleAction[i].answer);
                if (peopleAction[i].name == who) {
                    return peopleAction[i].answer;
                }
            }
            return "";
        };
        CheckCorect.prototype.zwrocAkcjeCzlowieka = function (czlek, peopleAction) {
            for (var i = 0; i < peopleAction.length; i++) {
                if (czlek == peopleAction[i].name) {
                    return peopleAction[i].answer;
                }
            }
            return "";
        };
        CheckCorect.prototype.eliminacjaDuplikatu = function (arr) {
            var results = [];
            for (var i = 0, len = arr.length - 1; i < len; i++) {
                if ((results.indexOf(arr[i]) == -1) && (arr.indexOf(arr[i], i + 1) != -1)) {
                    results.push(arr[i]);
                }
            }
            return results;
            /* var i;
             var  len = arr.length,
                 out = [],
                 obj = {};
 
             for (i = 0; i < len; i++) {
                 obj[arr[i]] = 0;
             }
             for (i in obj) {
                 out.push(i);
             }
             return out;
            console.log( "eliminator", out);*/
        };
        CheckCorect.prototype.checkAnswers = function (dropObject, peopleAction, timeOfDay, wspolne) {
            if (this._xml === null)
                this._xml = BeachBuilder.XmlControl.getInstance();
            var arr = this._xml.getScene(this._gameControl.actualRandomTextId);
            console.log(arr);
            var pass = false;
            var ileznalezionoStolow = 0;
            var tableWzorzec = new Array();
            var tablePowtorki = arr.slice(0, arr.length);
            var sameloakcje = new Array();
            for (var i = 0; i < tablePowtorki.length; i++) {
                sameloakcje.push(tablePowtorki[i][0]);
            }
            var powieloneLokacje = this.eliminacjaDuplikatu(sameloakcje);
            console.log("powieloneLokacje", powieloneLokacje);
            for (var j = 0; j < powieloneLokacje.length; j++) {
                for (var i = 0; i < tablePowtorki.length; i++) {
                    if (powieloneLokacje[j] == tablePowtorki[i][0]) {
                        tableWzorzec.push(tablePowtorki[i]);
                        console.log(j, i, powieloneLokacje[j], tablePowtorki[i][0], tablePowtorki[i]);
                        //  tablePowtorki.splice(i, 1);
                        //    arr.splice(i, 1);
                        ileznalezionoStolow++;
                    }
                }
            }
            /*
             for (var i: number = 0; i < tableWzorzec.length; i++) {
 
                 console.log("sprawdzam co jest", tableWzorzec[i][0], tableWzorzec[i][1]);
             }
             console.log("tabela wzorzec", tableWzorzec, tableWzorzec.length, ileznalezionoStolow);
            */
            /*
           
            for (var i: number = 0; i < arr.length; i++) {
                for (var j: number = 0; j < dropObject.length; j++) {
                    //znajudjemy stol sprawdzamy czy kto siedzi,,
                    console.log("wspolne check ", dropObject[j].name, this._incommon, dropObject[j].name, arr[i][0])
                    

                    if (dropObject[j].name == this._incommon && dropObject[j].name == arr[i][0]) {
                        
                        if (arr[i][1] == dropObject[j].answer) {

                            prawidloweOdpowiedzi++;
                           

                        }



                    }

                    else if (dropObject[j].name == this._incommon && dropObject[j].answer.length > 0) {
                        return false;

                    }


                }

            }

            if (prawidloweOdpowiedzi == ileznalezionoStolow) pass = true;

            if (!pass) return pass;*/
            var odpowiedziZGryArray = new Array();
            var odpowiedziArray = new Array();
            for (var j = 0; j < dropObject.length; j++) {
                // console.log(dropObject[j].name, "umbrellaYellow");
                odpowiedziZGryArray.push(new Array(dropObject[j].name, dropObject[j].answer, this.zwrocAkcjeCzlowieka(dropObject[j].answer, peopleAction)));
                odpowiedziArray.push(new Array(dropObject[j].name, "", ""));
            }
            var countTable = 0;
            var idOfTableArray = new Array();
            for (var k = 0; k < arr.length; k++) {
                for (var j = 0; j < odpowiedziArray.length; j++) {
                    if (dropObject[j].name == arr[k][0] && dropObject[j].name) {
                        odpowiedziArray[j][0] = arr[k][0];
                        odpowiedziArray[j][1] = arr[k][1];
                        odpowiedziArray[j][2] = arr[k][2];
                        console.log("odpowiedzi z xml", odpowiedziArray[j][0], odpowiedziArray[j][1]);
                    }
                }
            }
            /*
                        for (var i: number = 0; i < tableWzorzec.length; i++) {
            
            
                            odpowiedziArray[idOfTableArray[i]] = tableWzorzec[i];
            
            
                        }
                        */
            console.log("odpowiedzi wzorzec", odpowiedziArray);
            console.log("odpowiedzi z gry", odpowiedziZGryArray);
            var prawidloweOdpowiedziStol = 0;
            var ileZnaleziono = 0;
            var tableArray = new Array();
            console.log("i co kurwa", powieloneLokacje.length, odpowiedziZGryArray.length);
            for (var i = 0; i < powieloneLokacje.length; i++) {
                for (var j = 0; j < odpowiedziZGryArray.length; j++) {
                    if (odpowiedziZGryArray[j][0] == powieloneLokacje[i] && odpowiedziZGryArray[j][1].length > 0) {
                        tableArray.push(odpowiedziZGryArray[j]);
                        ileZnaleziono++;
                    }
                }
            }
            var porownaODpowiedzi = new Array();
            for (var i = 0; i < powieloneLokacje.length; i++) {
                for (var j = 0; j < odpowiedziArray.length; j++) {
                    if (odpowiedziArray[j][0] == powieloneLokacje[i]) {
                        porownaODpowiedzi.push(odpowiedziArray[j]);
                    }
                }
            }
            console.log("kurwa mac", porownaODpowiedzi, tableArray);
            var maxOdp = tableArray.length;
            var liczOdp = 0;
            for (var i = 0; i < tableArray.length; i++) {
                for (var j = 0; j < porownaODpowiedzi.length; j++) {
                    if (tableArray[j][1] == porownaODpowiedzi[i][1] && tableArray[j][2] == porownaODpowiedzi[i][2]) {
                        liczOdp++;
                    }
                }
            }
            if (liczOdp < maxOdp)
                return false;
            console.log("co tu kurwa mam", tableArray, odpowiedziArray);
            console.log("Dupppaa", tableArray.length, odpowiedziZGryArray.length);
            for (var i = 0; i < powieloneLokacje.length; i++) {
                for (var j = 0; j < odpowiedziZGryArray.length; j++) {
                    if (odpowiedziZGryArray[j][0] == powieloneLokacje[i]) {
                        odpowiedziZGryArray[j] = new Array("", "", "");
                        odpowiedziArray[j] = new Array("", "", "");
                    }
                }
            }
            //  console.log(tableArray[0], tableArray[1], tableWzorzec[0], tableWzorzec[1]);
            //if (tableArray.length > tableWzorzec.length) return;
            /*
                        for (var i: number = 0; i < tableArray.length; i) {
            
                            console.log(i, tableArray.length, tableArray[i][1], tableWzorzec[tableWzorzec.length - 1][1], tableArray[i][2], tableWzorzec[tableWzorzec.length - 1][2]);
                            if (tableArray[i][1] == tableWzorzec[tableWzorzec.length - 1][1] && tableArray[i][2] == tableWzorzec[tableWzorzec.length - 1][2]) {
                                prawidloweOdpowiedziStol++;
                                tableArray.splice(i, 1);
                                tableWzorzec.splice(tableWzorzec.length - 1, 1);
                                i = 0;
                                console.log("wchodzi", tableArray, tableWzorzec);
                            }
                            else {
                                i++;
                            }
            
                            console.log("ppo spradzwa", i, tableArray.length);
            
            
            
                        }
                        console.log(prawidloweOdpowiedziStol, ileznalezionoStolow);
                        if (prawidloweOdpowiedziStol == ileznalezionoStolow) pass = true;
            
                        if (!pass) return pass;*/
            console.log(odpowiedziZGryArray);
            console.log(odpowiedziArray);
            for (var j = 0; j < odpowiedziZGryArray.length; j++) {
                if (odpowiedziZGryArray[j][0] == odpowiedziArray[j][0] && odpowiedziZGryArray[j][1] == odpowiedziArray[j][1] && odpowiedziZGryArray[j][2] == odpowiedziArray[j][2]) {
                    console.log(odpowiedziZGryArray[j][0], odpowiedziArray[j][0], odpowiedziZGryArray[j][1], odpowiedziArray[j][1], odpowiedziZGryArray[j][2], odpowiedziArray[j][2]);
                    pass = true;
                }
                else {
                    return false;
                }
            }
            if (this._xml.timeOfDay(0).length > 0) {
                pass = false;
                console.log("sprawdzam dzien", this._xml.timeOfDay(0), timeOfDay);
                if (this._xml.timeOfDay(0) == timeOfDay) {
                    pass = true;
                }
            }
            return pass;
        };
        /* this.randomLevel = GameControler.getInstance().actualRandomTextId;
         console.log("aktualny lewel to", this.randomLevel);
         for (var i: number = 0; i < dropObject.length; i++){
           //  console.log("ludzie", this.solution.levels[this.randomLevel].placeAnswer[i], dropObject[i].answer, i);
             if (this.solution.levels[this.randomLevel].placeAnswer[i] != dropObject[i].answer) {
                 return false;

             }

         }

         for (var i: number = 0; i < peopleAction.length; i++) {
             //console.log("ludzie", this.solution.levels[this.randomLevel].peopleAnswer[i] , peopleAction[i].answer, i);
             if (this.solution.levels[this.randomLevel].peopleAnswer[i] != peopleAction[i].answer) {
                 return false;

             }

         }

         if (this.solution.levels[this.randomLevel].timeOfDay != -1) {

             if (this.solution.levels[this.randomLevel].timeOfDay != timeOfDay) {
                 return false;
             }

         }*/
        CheckCorect.prototype.usunPowtarzajaceOdpowiedzi = function (arr, location) {
            for (var i = 0; i < arr.length; i++) {
                if (arr[i][0] == location) {
                    console.log("usuwam??", i, arr[i][0], location);
                    arr.splice(i, 1);
                    --i;
                }
            }
            console.log("po usunieciu", arr);
        };
        Object.defineProperty(CheckCorect.prototype, "incomon", {
            set: function (incomonObject) {
                this._incommon = incomonObject;
            },
            enumerable: true,
            configurable: true
        });
        return CheckCorect;
    })();
    BeachBuilder.CheckCorect = CheckCorect;
})(BeachBuilder || (BeachBuilder = {}));
var BeachBuilder;
(function (BeachBuilder) {
    var DayNight = (function (_super) {
        __extends(DayNight, _super);
        function DayNight(game, x, y, day, night, sunset) {
            _super.call(this, game, x, y);
            this.answer = "day";
            this._day = new Phaser.Sprite(game, 0, 0, day);
            this._night = new Phaser.Sprite(game, 0, 0, night);
            this._sunset = new Phaser.Sprite(game, 0, 0, sunset);
            this._timeOfDay = this._day;
            this.addChild(this._timeOfDay);
            this.fountain = new Phaser.Sprite(game, 0, 0, "fountain", 0);
            // this.fountain.scale.setTo(1.38, 1.38);
            this.fountain.animations.add("playfountain");
            this.fountain.animations.play("playfountain", 24, true);
            this.fountain.x = this.game.width / 2 - this.fountain.width / 2;
            this.fountain.y = this.game.height / 2 - this.fountain.height / 2 - 60;
            this.addChild(this.fountain);
        }
        DayNight.prototype.changeTimeOfDay = function (id) {
            this._timeOfDay = null;
            this.removeChild(this._timeOfDay);
            var nazwaPory = "";
            if (id == 0) {
                nazwaPory = "day";
                this._timeOfDay = this._day;
            }
            else if (id == 1) {
                this._timeOfDay = this._night;
                nazwaPory = "sunset";
            }
            else if (id == 2) {
                this._timeOfDay = this._sunset;
                nazwaPory = "night";
            }
            this.answer = nazwaPory;
            this.addChild(this._timeOfDay);
            this.addChild(this.fountain);
        };
        return DayNight;
    })(Phaser.Sprite);
    BeachBuilder.DayNight = DayNight;
})(BeachBuilder || (BeachBuilder = {}));
var BeachBuilder;
(function (BeachBuilder) {
    var ItemsObject = (function (_super) {
        __extends(ItemsObject, _super);
        function ItemsObject(game, x, y, widthHeight) {
            _super.call(this, game, x, y);
            this.initialX = x;
            this.initialY = y;
            this._rectangle = new Phaser.Rectangle(0, 0, widthHeight.x, widthHeight.y);
        }
        return ItemsObject;
    })(Phaser.Sprite);
    BeachBuilder.ItemsObject = ItemsObject;
})(BeachBuilder || (BeachBuilder = {}));
var BeachBuilder;
(function (BeachBuilder) {
    var ItemsToDrag = (function (_super) {
        __extends(ItemsToDrag, _super);
        function ItemsToDrag(game, state, point, widthHeight, key, id, name, scale, isSkateBoard) {
            if (key === void 0) { key = ""; }
            if (id === void 0) { id = 0; }
            if (name === void 0) { name = ""; }
            if (scale === void 0) { scale = 1; }
            if (isSkateBoard === void 0) { isSkateBoard = false; }
            _super.call(this, game, point.x, point.y, widthHeight);
            this.name = name;
            this.startX = this.x;
            this.startY = this.y;
            this.isSkateBoard = isSkateBoard;
            this._game = state;
            this.initialScale = this.scale = new Phaser.Point(scale, scale);
            this._rectangle.offset(-this._rectangle.halfWidth, -this._rectangle.halfHeight);
            if (key.length > 0) {
                this._items = new Phaser.Sprite(game, 0, 0, key, 0);
                this._items.anchor.setTo(0.5, 0.5);
                this.addChild(this._items);
            }
            this.id = id;
            this.inputEnabled = true;
            this.input.enableDrag();
            this.events.onDragStart.add(this.onStartDrag, this);
            this.events.onDragStop.add(this.onStopDrag, this);
            this.answer = "";
            game.add.existing(this);
            //  console.log("constructor people");
        }
        ItemsToDrag.prototype.onStopDrag = function () {
            this._game.itemsOnDragStop(this);
        };
        ItemsToDrag.prototype.onStartDrag = function () {
            this._game.itemsOnDragStart(this);
        };
        return ItemsToDrag;
    })(BeachBuilder.ItemsObject);
    BeachBuilder.ItemsToDrag = ItemsToDrag;
})(BeachBuilder || (BeachBuilder = {}));
var BeachBuilder;
(function (BeachBuilder) {
    var ObjectToDrop = (function (_super) {
        __extends(ObjectToDrop, _super);
        function ObjectToDrop(game, state, point, key, frame, widthHeight, id, scale, positionPeople_, name) {
            if (id === void 0) { id = 0; }
            if (scale === void 0) { scale = 1; }
            if (positionPeople_ === void 0) { positionPeople_ = new Phaser.Point(0, 0); }
            if (name === void 0) { name = ""; }
            _super.call(this, game, point.x, point.y, widthHeight);
            this.isEmpty = true;
            this._isAlpha = false;
            this.key = key;
            this.name = name;
            if (positionPeople_.x == 0 && positionPeople_.y == 0) {
                this.positionPeople = new Phaser.Point(point.x, point.y);
            }
            else {
                this.positionPeople = positionPeople_;
            }
            this.frame = frame;
            this._game = state;
            this.ownName = key;
            this.anchor.setTo(0.5, 0.5);
            this.scale = new Phaser.Point(scale, scale);
            // this._rectangle.offset(-this._rectangle.halfWidth, -this._rectangle.halfHeight);
            this._rectangle = new Phaser.Rectangle(this.x - this._rectangle.width / 2, this.y - (this._rectangle.height * 1.2), this._rectangle.width, this._rectangle.height);
            /* var grap: Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
          
             grap.beginFill(0x000000, 0.5);
             grap.drawRect(- this._rectangle.halfWidth, - this._rectangle.halfHeight, this._rectangle.width, this._rectangle.height);
             grap.endFill();
             this.addChild(grap);*/
            this._object = new Phaser.Sprite(game, 0, 0, key, 0);
            this._object.anchor.setTo(0.5, 0.5);
            this.addChild(this._object);
            this.answer = "";
            game.add.existing(this);
        }
        ObjectToDrop.prototype.create = function () {
        };
        ObjectToDrop.prototype.changeFrame = function (id, alpha) {
            if (alpha === void 0) { alpha = false; }
            this._object.frame = id;
            if (this._isAlpha && this._object.frame == 0) {
                this._object.alpha = 0;
            }
            else {
                this._object.alpha = 1;
            }
        };
        ObjectToDrop.prototype.restart = function () {
            this.isEmpty = true;
            this.answer = "";
        };
        Object.defineProperty(ObjectToDrop.prototype, "isAlpha", {
            set: function (show) {
                this._isAlpha = true;
                this._object.alpha = 0;
            },
            enumerable: true,
            configurable: true
        });
        return ObjectToDrop;
    })(BeachBuilder.ItemsObject);
    BeachBuilder.ObjectToDrop = ObjectToDrop;
})(BeachBuilder || (BeachBuilder = {}));
var BeachBuilder;
(function (BeachBuilder) {
    var PeopleToDrag = (function (_super) {
        __extends(PeopleToDrag, _super);
        function PeopleToDrag() {
            _super.apply(this, arguments);
            this.isDroped = false;
        }
        PeopleToDrag.prototype.create = function () {
            // this._rectangle.offset(-this._rectangle.halfWidth, -this._rectangle.halfHeight);
            // this.changeFrame(3);
        };
        PeopleToDrag.prototype.checkLastObjectToDrop = function (actualObjectToDrop) {
            if (this.lastobjecToDrop) {
                this.lastobjecToDrop.isEmpty = true;
                this.lastobjecToDrop.answer = "";
            }
            this.lastobjecToDrop = actualObjectToDrop;
            this.isDroped = true;
        };
        return PeopleToDrag;
    })(BeachBuilder.ItemsToDrag);
    BeachBuilder.PeopleToDrag = PeopleToDrag;
})(BeachBuilder || (BeachBuilder = {}));
var BeachBuilder;
(function (BeachBuilder) {
    var PlayerAnim = (function (_super) {
        __extends(PlayerAnim, _super);
        function PlayerAnim() {
            _super.apply(this, arguments);
            this.animationArray = new Array();
            this.nameAnimationArray = new Array();
        }
        PlayerAnim.prototype.addAnimation = function (anim, nameAnim) {
            //this.scale.add(0.072, 0.072);
            anim.anchor.setTo(0.5, 0.5);
            this.animationArray.push(anim);
            this.nameAnimationArray.push(nameAnim);
        };
        PlayerAnim.prototype.playAnimation = function (id) {
            this.removeChild(this._actualAnim);
            this._actualAnim = null;
            this.answer = this.nameAnimationArray[id];
            this._actualAnim = this.animationArray[id];
            this.addChild(this._actualAnim);
            this._actualAnim.animations.add(this.nameAnimationArray[id]);
            this._actualAnim.animations.play(this.nameAnimationArray[id], 24, true, false);
        };
        PlayerAnim.prototype.playAnim = function () {
            if (!this._actualAnim) {
                this._actualAnim = this._items;
            }
            this._actualAnim.animations.add("");
            this._actualAnim.animations.play("", 24, true, false);
        };
        PlayerAnim.prototype.restart = function () {
            this.startX = this.x = this.initialX;
            this.startY = this.y = this.initialY;
            this.scale = this.initialScale;
            this.isDroped = false;
            this.lastobjecToDrop = null;
            this.removeChild(this._actualAnim);
            this._actualAnim = null;
            console.log("nazwa animacji", this.nameAnimationArray[0]);
            //   this._actualAnim = this.animationArray[0];
            this.addChild(this._items);
            this._items.frame = 0;
            this._items.animations.stop();
            this.answer = "";
        };
        PlayerAnim.prototype.onStopDrag = function () {
            this._game.peopleOnDragStop(this);
            this._rectangle = new Phaser.Rectangle(this.x - this._rectangle.halfWidth, this.y - this._rectangle.halfHeight, this._rectangle.width, this._rectangle.height);
            if (this.x == this.initialX && this.y == this.initialY)
                this.scale = this.initialScale;
        };
        PlayerAnim.prototype.onStartDrag = function () {
            // this.scale = new Phaser.Point(1.2, 1.2);
            this.scale = new Phaser.Point(1, 1);
            this._game.peopleOnDragStart(this);
        };
        return PlayerAnim;
    })(BeachBuilder.PeopleToDrag);
    BeachBuilder.PlayerAnim = PlayerAnim;
})(BeachBuilder || (BeachBuilder = {}));
var BeachBuilder;
(function (BeachBuilder) {
    var Places = (function () {
        // -1 nie wazne 0 dzien, 1wieczor, noc
        function Places() {
            this.itemsToDropNameArray = BeachBuilder.ArrayStatic.itemsToDropNameArray;
            this.peopleNameArray = new Array("ridl", "erad", "tan", "kaz", "skateboard1", "skateboard2", "skateboard3", "skateboard4");
            this.animNameArray = BeachBuilder.ArrayStatic.animNameArray;
            this.timeOfDay = -1;
            this.placeAnswer = new Array();
            for (var i = 0; i < this.itemsToDropNameArray.length; i++) {
                this.placeAnswer.push(-1);
            }
            this.peopleAnswer = new Array();
            for (var i = 0; i < this.peopleNameArray.length; i++) {
                this.peopleAnswer.push(-1);
            }
        }
        Places.prototype.setSolution = function (namePlace, scoreNamePeople, namePeople, scoreNameObject, timeOfDay_) {
            if (namePlace === void 0) { namePlace = ""; }
            if (scoreNamePeople === void 0) { scoreNamePeople = ""; }
            if (namePeople === void 0) { namePeople = ""; }
            if (scoreNameObject === void 0) { scoreNameObject = ""; }
            if (timeOfDay_ === void 0) { timeOfDay_ = -1; }
            if (timeOfDay_ != -1) {
                this.timeOfDay = timeOfDay_;
            }
            if (namePlace.length > 0) {
                var whichDrop;
                for (var i = 0; i < this.itemsToDropNameArray.length; i++) {
                    if (this.itemsToDropNameArray[i] == namePlace) {
                        whichDrop = i;
                    }
                }
                for (var i = 0; i < this.peopleNameArray.length; i++) {
                    if (this.peopleNameArray[i] == scoreNamePeople) {
                        //  console.log("znaleziono czlowieka", scoreNamePeople, "jego id",i);
                        this.placeAnswer[whichDrop] = i;
                    }
                }
            }
            if (namePeople.length > 0) {
                var whichPeople;
                for (var i = 0; i < this.peopleNameArray.length; i++) {
                    if (this.peopleNameArray[i] == namePeople) {
                        whichPeople = i;
                    }
                }
                for (var i = 0; i < this.animNameArray.length; i++) {
                    if (this.animNameArray[i] == scoreNameObject) {
                        this.peopleAnswer[whichPeople] = i;
                    }
                }
            }
        };
        return Places;
    })();
    BeachBuilder.Places = Places;
})(BeachBuilder || (BeachBuilder = {}));
var BeachBuilder;
(function (BeachBuilder) {
    var Solutions = (function () {
        function Solutions() {
            this.itemsToDropNameArray = new Array("redChair", "blueChair", "greenChair", "umbrellaYellow", "towelGreen", "towelOrange", "highLightLeft", "highLightRight");
            this.animNameArray = new Array("_default", "_wind", "_drum", "_towel", "_drink", "_gum", "_zzzz");
            this.peopleNameArray = new Array("ridl", "mo", "gab", "kaz");
            if (Solutions._instance) {
                throw new Error("Error: Instantiation failed: Use Solutions.getInstance() instead of new.");
            }
            Solutions._instance = this;
            this.level1Desc();
        }
        Solutions.getInstance = function () {
            if (Solutions._instance === null) {
                Solutions._instance = new Solutions();
            }
            return Solutions._instance;
        };
        Solutions.prototype.level1 = function (peopleInPlace, peopleAction) {
        };
        Solutions.prototype.level1Desc = function () {
            console.log("Level1 Ustawiony");
            this.levels = new Array();
            var level1 = new BeachBuilder.Places();
            //Kaz plays ball on the red chair.
            level1.setSolution("redChair", "kaz", "kaz", "_gum");
            this.levels.push(level1);
            var level2 = new BeachBuilder.Places();
            //It is dark.  Mo sits on the green towel.
            level2.setSolution("towelGreen", "mo", "", "", 2);
            this.levels.push(level2);
            //Ridl sleeps on the picnic table.
            var level3 = new BeachBuilder.Places();
            level3.setSolution("highLightLeft", "ridl", "ridl", "_zzzz");
            this.levels.push(level3);
            //Kaz sits on the red chair.  Mo sits next to her.  The sun is setting.  It is getting late.
            var level4 = new BeachBuilder.Places();
            level4.setSolution("redChair", "kaz", "", "", 1);
            level4.setSolution("blueChair", "mo", "", "", 1);
            this.levels.push(level4);
            //Ridl and Gabe love drums. They play them well.  Ridl is on the red chair.  Gabe is on the green chair.
            var level5 = new BeachBuilder.Places();
            level5.setSolution("redChair", "ridl", "ridl", "_drum");
            level5.setSolution("greenChair", "gab", "gab", "_drum");
            this.levels.push(level5);
            //Kaz sleeps under the shade.  Mo has a drink at the table.
            var level6 = new BeachBuilder.Places();
            level6.setSolution("umbrellaYellow", "kaz", "kaz", "_zzzz");
            level6.setSolution("highLightLeft", "mo", "mo", "_drink");
            this.levels.push(level6);
            //It is dark.  Mo and Kaz sit at the table. 
            var level7 = new BeachBuilder.Places();
            level7.setSolution("highLightLeft", "kaz");
            level7.setSolution("highLightRight", "mo");
            this.levels.push(level7);
            //Mo sits on the red chair.  Kaz sits on the blue chair.  It is late.  Kaz is happy.  Mo is happy.
            var level8 = new BeachBuilder.Places();
            level8.setSolution("redChair", "mo", "", "", 1);
            level8.setSolution("blueChair", "kaz", "", "", 1);
            this.levels.push(level8);
            //Mo sleeps on the red chair.  The sun is going down.  Ridl is drying off on the green chair.
            var level9 = new BeachBuilder.Places();
            level9.setSolution("redChair", "mo", "mo", "_zzzz", 1);
            level9.setSolution("greenChair", "ridl", "ridl", "_towel", 1);
            this.levels.push(level9);
            //Kaz likes her drink.  She sits on the red chair.
            var level10 = new BeachBuilder.Places();
            level10.setSolution("redChair", "kaz", "kaz", "_drink");
            //level10.setSolution("greenChair", "ridl", "ridl", "_towel", 1);
            this.levels.push(level10);
            // Mo plays ball on the red chair
            var level11 = new BeachBuilder.Places();
            level11.setSolution("redChair", "mo", "mo", "_gum");
            this.levels.push(level11);
            // It is dark.  Kaz sits on the green towel.
            var level12 = new BeachBuilder.Places();
            level12.setSolution("towelGreen", "kaz", "", "", 0);
            this.levels.push(level12);
            //Gabe sleeps on the picnic table.
            var level13 = new BeachBuilder.Places();
            level13.setSolution("highLightLeft", "gab", "gab", "_zzzz");
            this.levels.push(level13);
            //Mo sits on the red chair.  Kaz sits next to her.  The sun is setting.  It is getting late.
            var level14 = new BeachBuilder.Places();
            level14.setSolution("redChair", "mo", "", "", 1);
            level14.setSolution("blueChair", "kaz", "", "", 1);
            this.levels.push(level14);
            //Ridl and Gabe love drums.  They play them well.  Gabe is on the red chair. Ridl is on the green chair.
            var level15 = new BeachBuilder.Places();
            level15.setSolution("redChair", "gab", "gab", "_drum");
            level15.setSolution("greenChair", "ridl", "ridl", "_drum");
            this.levels.push(level15);
            //Mo sleeps under the shade. Kaz has a drink at the table.
            var level16 = new BeachBuilder.Places();
            level16.setSolution("umbrellaYellow", "mo", "mo", "_zzzz");
            level16.setSolution("highLightLeft", "kaz", "kaz", "_drink");
            this.levels.push(level16);
            //It is dark.  Ridl and Gabe sit at the table. 
            var level17 = new BeachBuilder.Places();
            level17.setSolution("highLightLeft", "ridl", "", "", 2);
            level17.setSolution("highLightRight", "gab", "", "", 2);
            this.levels.push(level17);
            //Kaz sits on the red chair.  Mo sits on the blue chair.  It is late.  Kaz is happy.  Mo is happy.
            var level18 = new BeachBuilder.Places();
            level18.setSolution("redChair", "kaz", "", "", 1);
            level18.setSolution("blueChair", "mo", "", "", 1);
            this.levels.push(level18);
            //Kaz sleeps on the red chair.  The sun is going down.  Gabe is drying off on the green chair.
            var level19 = new BeachBuilder.Places();
            level19.setSolution("redChair", "kaz", "kaz", "_zzzz", 1);
            level19.setSolution("greenChair", "gab", "gab", "_towel", 1);
            this.levels.push(level19);
            // Mo likes her drink.She sits on the red chair.
            var level20 = new BeachBuilder.Places();
            level20.setSolution("redChair", "mo", "mo", "_drink");
            this.levels.push(level20);
            //Kaz and Brad read books.  They sit on benches.
            var level21 = new BeachBuilder.Places();
            level21.setSolution(" ", "gab", "gab", "_zzzz");
            this.levels.push(level21);
        };
        Solutions._instance = null;
        return Solutions;
    })();
    BeachBuilder.Solutions = Solutions;
})(BeachBuilder || (BeachBuilder = {}));
var BeachBuilder;
(function (BeachBuilder) {
    var TimeOfDayBtn = (function (_super) {
        __extends(TimeOfDayBtn, _super);
        function TimeOfDayBtn(game, state, x, y, key, id) {
            _super.call(this, game, x, y, key, this.timeOfDayBtnClick, this);
            this.anchor.setTo(0.5, 0.5);
            this._game = state;
            this._id = id;
        }
        TimeOfDayBtn.prototype.timeOfDayBtnClick = function () {
            this._game.changeTimeOfDay(this._id);
        };
        return TimeOfDayBtn;
    })(Phaser.Button);
    BeachBuilder.TimeOfDayBtn = TimeOfDayBtn;
})(BeachBuilder || (BeachBuilder = {}));
var BeachBuilder;
(function (BeachBuilder) {
    var Preloader = (function (_super) {
        __extends(Preloader, _super);
        function Preloader() {
            _super.apply(this, arguments);
        }
        Preloader.prototype.reolad = function () {
            var tween = this.add.tween(this.progressBarBlue).to({ alpha: 1 }, 1000, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.reolad2, this);
        };
        Preloader.prototype.reolad2 = function () {
            var tween = this.add.tween(this.progressBarBlue).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.reolad, this);
        };
        Preloader.prototype.preload = function () {
            var mobile;
            mobile = false;
            if (!this.game.device.desktop) {
                mobile = true;
            }
            var bg = this.game.add.sprite(0, 0, "bg");
            this.progresBar = this.game.add.sprite(0, 0, "progressBar");
            //  this.progresBar.x = this.game.width / 2 - this.progresBar.width / 2;
            //  this.progresBar.y = this.game.height - this.progresBar.height - 20;
            this.progresBar.position.setTo(this.stage.width / 2 - this.progresBar.width / 2, this.stage.height - this.progresBar.height - 10);
            this.progressBarBlue = this.add.sprite(0, 0, 'progressBarBlue');
            this.progressBarBlue.position = this.progresBar.position;
            this.load.setPreloadSprite(this.progressBarBlue);
            if (mobile) {
                this.load.image("InstructionsScreen", "assets/03_gallop_park.png");
                this.load.audio("instructionSn", "assets/audio/gallop_park_02.mp3");
            }
            else {
                this.load.image("InstructionsScreen", "assets/02_gallop_park.png");
                this.load.audio("instructionSn", "assets/audio/gallop_park_01.mp3");
            }
            this.load.audio("MainMenuSn", "assets/mainMenu/BBThemeMusic.mp3");
            this.load.image("popup", "assets/popup/popup.png");
            this.load.image("logoGame", "assets/popup/logo.png");
            this.load.atlasJSONArray('games', 'assets/popup/games.png', 'assets/popup/games.json');
            this.load.atlasJSONArray('helpBtn', 'assets/popup/help.png', 'assets/popup/help.json');
            this.load.atlasJSONArray('playGame', 'assets/popup/play.png', 'assets/popup/play.json');
            this.load.image("mainMenuBg", "assets/mainMenu/SplashScreen2.png");
            this.load.atlasJSONArray('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');
            this.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
            this.load.audio("overButtonA", "assets/audio/overButton.mp3");
            this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");
            this.load.atlasJSONArray("doneInGameBtn", "assets/Level1/buttons/doneInGame.png", "assets/Level1/buttons/doneInGame.json");
            this.load.atlasJSONArray("clearBtn", "assets/Level1/buttons/clear.png", "assets/Level1/buttons/clear.json");
            this.load.image("TextLevel1", "assets/Level1/TextLevel1.png");
            this.load.atlasJSONArray("doneBtn", "assets/Level1/buttons/textDoneBtn.png", "assets/Level1/buttons/textDoneBtn.json");
            this.load.image("youWin", "assets/Level1/youWin.png");
            this.load.image("youLose", "assets/Level1/youLose.png");
            this.load.image("bgLevel1", "assets/Level1/bgLevel1.png");
            this.load.image("sunBg", "assets/Level1/sun.png");
            this.load.image("sunsetBg", "assets/Level1/sunset.png");
            this.load.image("moonBg", "assets/Level1/moon.png");
            this.load.atlasJSONArray("fountain", "assets/Level1/objects/fountain.png", "assets/Level1/objects/fountain.json");
            /* items to drop */
            this.load.atlasJSONArray("benchLeft", "assets/Level1/objects/bench.png", "assets/Level1/objects/bench.json");
            this.load.atlasJSONArray("redTree", "assets/Level1/objects/leftTree.png", "assets/Level1/objects/leftTree.json");
            this.load.atlasJSONArray("orangeTree", "assets/Level1/objects/orangeTree.png", "assets/Level1/objects/orangeTree.json");
            this.load.atlasJSONArray("blueTree", "assets/Level1/objects/blueTree.png", "assets/Level1/objects/blueTree.json");
            this.load.atlasJSONArray("greenTree", "assets/Level1/objects/greenTree.png", "assets/Level1/objects/greenTree.json");
            this.load.atlasJSONArray("rightBench", "assets/Level1/objects/rightBench.png", "assets/Level1/objects/rightBench.json");
            this.load.atlasJSONArray("fountainHighlight", "assets/Level1/objects/fountainHighlight.png", "assets/Level1/objects/fountainHighlight.json");
            this.load.atlasJSONArray("fountainHighlight2", "assets/Level1/objects/fountainHighlight2.png", "assets/Level1/objects/fountainHighlight2.json");
            /*end item to drop */
            /*items to drag*/
            this.load.image("cdPlayer", "assets/Level1/downBarForObjects/items/CdPlayer.png");
            this.load.image("cellPhone", "assets/Level1/downBarForObjects/items/CellPhone.png");
            this.load.image("book", "assets/Level1/downBarForObjects/items/Comic.png");
            this.load.image("gum", "assets/Level1/downBarForObjects/items/BubbleGum.png");
            this.load.image("laptop", "assets/Level1/downBarForObjects/items/Laptop.png");
            this.load.image("ice", "assets/Level1/downBarForObjects/items/ice.png");
            /*end items to drag*/
            /* time of day */
            this.load.image("day", "assets/Level1/objects/SunBtn.png");
            this.load.image("moon", "assets/Level1/objects/MoonBtn.png");
            this.load.image("sunset", "assets/Level1/objects/sunsetBtn.png");
            /*end time of day */
            /* people to drag */
            /*  this.load.atlasJSONArray("gab", "assets/Level1/downBarForObjects/gab.png", "assets/Level1/downBarForObjects/gab.json");
              this.load.atlasJSONArray("kaz", "assets/Level1/downBarForObjects/kaz.png", "assets/Level1/downBarForObjects/kaz.json");
              this.load.atlasJSONArray("mo", "assets/Level1/downBarForObjects/mo.png", "assets/Level1/downBarForObjects/mo.json");
              this.load.atlasJSONArray("ridl", "assets/Level1/downBarForObjects/ridl.png", "assets/Level1/downBarForObjects/ridl.json");*/
            /* animations */
            var animNameArray = new Array("_default", "_player", "_phone", "_comic", "_gum", "_laptop", "_ice");
            for (var i = 0; i < 7; i++) {
                this.load.atlasJSONArray("kaz" + animNameArray[i], "assets/Level1/downBarForObjects/kaz/kaz" + animNameArray[i] + ".png", "assets/Level1/downBarForObjects/kaz/kaz" + animNameArray[i] + ".json");
                this.load.atlasJSONArray("tan" + animNameArray[i], "assets/Level1/downBarForObjects/tan/tan" + animNameArray[i] + ".png", "assets/Level1/downBarForObjects/tan/tan" + animNameArray[i] + ".json");
                this.load.atlasJSONArray("erad" + animNameArray[i], "assets/Level1/downBarForObjects/brad/brad" + animNameArray[i] + ".png", "assets/Level1/downBarForObjects/brad/brad" + animNameArray[i] + ".json");
                this.load.atlasJSONArray("ridl" + animNameArray[i], "assets/Level1/downBarForObjects/ridl/ridl" + animNameArray[i] + ".png", "assets/Level1/downBarForObjects/ridl/ridl" + animNameArray[i] + ".json");
            }
            for (var i = 0; i < 20; i++) {
                this.load.audio("lessonsA" + (i), "assets/audio/level/prompt1A_" + (i + 1) + ".mp3");
            }
            for (var i = 1; i < 6; i++) {
                this.load.atlasJSONArray("fail" + i, "assets/feedbacks/fail" + i + ".png", "assets/feedbacks/fail" + i + ".json");
            }
            for (var i = 1; i < 5; i++) {
                this.load.atlasJSONArray("success" + i, "assets/feedbacks/success" + i + ".png", "assets/feedbacks/success" + i + ".json");
            }
            for (var i = 4; i < 8; i++) {
                this.load.audio("congrats" + i, "assets/audio/gallop_park_0" + i + ".mp3");
            }
            this.load.audio("failure1", "assets/audio/gallop_park_08.mp3");
            this.load.audio("failure2", "assets/audio/gallop_park_09.mp3");
            /* end animations */
            /* skateboard as people */
            this.load.image("skateboard1", "assets/Level1/objects/skateboard1.png");
            this.load.image("skateboard2", "assets/Level1/objects/skateboard2.png");
            this.load.image("skateboard3", "assets/Level1/objects/skateboard3.png");
            this.load.image("skateboard4", "assets/Level1/objects/skateboard4.png");
            /* end skateboard as people */
            this.load.audio("overButtonA", "assets/audio/overButton.wav");
            this.load.audio("clearButtonA", "assets/audio/clearButton.wav");
            this.load.atlasJSONArray("scoreScreen", "assets/scoreScreen/scoreScreen.png", "assets/scoreScreen/scoreScreen.json");
            this.load.atlasJSONArray("playScoreScreen", "assets/scoreScreen/scoreScreenButtons.png", "assets/scoreScreen/scoreScreenButtons.json");
            this.load.text('xml', 'assets/level1.xml');
        };
        Preloader.prototype.update = function () {
            //this.progressBarBlue.x = this.game.load.progress;
            console.log(this.progressBarBlue.x);
        };
        Preloader.prototype.mainMenu = function () {
        };
        Preloader.prototype.create = function () {
            this.game.state.start("MainMenu");
        };
        return Preloader;
    })(Phaser.State);
    BeachBuilder.Preloader = Preloader;
})(BeachBuilder || (BeachBuilder = {}));
var BeachBuilder;
(function (BeachBuilder) {
    var Instruction = (function (_super) {
        __extends(Instruction, _super);
        function Instruction() {
            _super.apply(this, arguments);
        }
        Instruction.prototype.create = function () {
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            this._mainMenuBg = this.game.add.audio('instructionSn', 1, true);
            this._mainMenuBg.play("", 0, 1, false);
            var gameControler = BeachBuilder.GameControler.getInstance();
            gameControler.mainSound.volume = 0.05;
            var background = this.add.sprite(0, 0, 'InstructionsScreen');
            background.width = this.game.world.width; //this.stage.width;
            background.height = this.game.world.height; //this.stage.height;
            this._buttons = this.add.group();
            //var buttonsBg: Phaser.Sprite = this.add.sprite(0, 0, 'buttonsBg');
            // this._buttons.addChild(buttonsBg);
            var gameButton = this.add.button(0, 0, "games", this.onGameClick, this, 1, 0);
            //gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            this._buttons.addChild(gameButton);
            var playButton = this.add.button(100, 0, "playGame", this.onPlayClick, this, 1, 0);
            playButton.onInputDown.add(this.overPlay2, this);
            //playButton.x = this._buttons.width - playButton.width - 10;
            this._buttons.addChild(playButton);
            this._buttons.x = 0;
            this._buttons.y = this.game.stage.height - this._buttons.height;
        };
        Instruction.prototype.onGameClick = function () {
            window.history.back();
            this.overButtonA.play();
        };
        Instruction.prototype.overPlay2 = function () {
            this.overButtonA.play();
        };
        Instruction.prototype.onPlayClick = function () {
            // this._mainMenuBg.stop();
            this._mainMenuBg.stop();
            this.game.state.start("Level1", true, false);
        };
        return Instruction;
    })(Phaser.State);
    BeachBuilder.Instruction = Instruction;
})(BeachBuilder || (BeachBuilder = {}));
var BeachBuilder;
(function (BeachBuilder) {
    var Level1 = (function (_super) {
        __extends(Level1, _super);
        function Level1() {
            _super.apply(this, arguments);
            this.itemsToDropArray = new Array();
            this.audioArray = new Array();
            this.poepleArray = new Array();
            this.firtsTime = true;
            this.congratsAudioArray = new Array();
            this.successAnimArray = new Array();
            this.wspolne = new Array();
        }
        Level1.prototype.preload = function () {
        };
        Level1.prototype.create = function () {
            /*   audio set lessons */
            /*
                        for (var i: number = 0; i < 20; i++) {
            
                            this.audioArray.push(this.game.add.audio("lessonsA" + i, 1, false));
            
            
                        }
                        */
            this.audoBadAnswerArray = new Array();
            this.audoBadAnswerArray.push(this.game.add.audio("failure1", 1, false));
            this.audoBadAnswerArray.push(this.game.add.audio("failure2", 1, false));
            for (var i = 1; i < 5; i++) {
                this.successAnimArray.push(new Phaser.Sprite(this.game, 0, 0, "success" + i, 0));
            }
            for (var i = 1; i < 5; i++) {
                this.congratsAudioArray.push(this.game.add.audio("congrats" + i, 1, false));
            }
            /* end audio set lessons */
            this.failArray = new Array();
            for (var i = 1; i < 6; i++) {
                this.failArray.push(new Phaser.Sprite(this.game, 0, 0, "fail" + i, 0));
            }
            /* audio */
            this._overButtonA = this.game.add.audio("overButtonA", 1, false);
            // this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            this._clearButtonA = this.game.add.audio("clearButtonA", 1, false);
            /*end audio*/
            this._solutions = BeachBuilder.Solutions.getInstance();
            this._checkCorrect = new BeachBuilder.CheckCorect();
            this._checkCorrect.incomon = "table";
            this._gameControler = BeachBuilder.GameControler.getInstance();
            this._gameControler.mainSound.volume = 0.1;
            this._gameControler.setGameState(this);
            this._gameControler.setLevel(userLevel - 1);
            this._xml = BeachBuilder.XmlControl.getInstance();
            console.log(this._xml);
            var parser = new DOMParser();
            var parsedXml;
            var xmlLoaded = this.cache.getText('xml');
            parsedXml = parser.parseFromString(xmlLoaded, "application/xml");
            this._xml.xml = parsedXml;
            this._gameControler.maxLevel = this._xml.getMaxLevel();
            this._gameControler.setMaxScene(this._xml.getMaxRandomScenes());
            this._gameControler.randomGame();
            this._xml.getScene(this._gameControler.actualRandomTextId);
            //   this._actualAudioPlay = new Phaser.Sound(;
            //this._actualAudioPlay = this.audioArray[this._gameControler.actualRandomTextId];
            //  this._actualAudioPlay.play();
            this.textLevelGr = this.add.group();
            var bgTextLevel = new Phaser.Graphics(this.game, 0, 0);
            bgTextLevel.beginFill(0xFFFFFF, 1);
            bgTextLevel.drawRect(0, 0, this.game.width, this.game.height);
            bgTextLevel.endFill();
            this.textLevelGr.addChild(bgTextLevel);
            this.textLevel = this.add.sprite(293, 165, 'TextLevel1', 0, this.textLevelGr);
            //  this.textLevel.anchor.setTo(0.5, 0.5);
            this.textLevel.x = this.game.width / 2 - this.textLevel.width / 2;
            this.textLevel.y = this.game.height / 2 - this.textLevel.height / 2;
            this.textDescdoneBtn = this.game.add.button(323, 275, "doneBtn", this.doneBtnClick, this, 1, 0, 0, 0, this.textLevelGr);
            this.textDescdoneBtn.onInputDown.add(this.doneBtnOver, this);
            this.textDescdoneBtn.inputEnabled = true;
            this.textDescdoneBtn.input.priorityID = 2;
            this.setTextDescription();
            //s  doneBtn.x += textLevel.x;
            //interctive object set x,y 
            //  this.blockScreen.addChild(blokScreenSprite);
            //this.stage.removeChild(this.blockScreen);
            this.scoreScreenGr = this.add.group();
            //  this.scoreScreenGr.add(this.gameOverSpr);
            //this.game.add.sprite(0, 0, "scoreScreen", 0, this.scoreScreenGr);
            //  this.gameOverSpr.animations.add("score");
            //  this.gameOverSpr.animations.play("score", 48, true);
            //var scorePlay = this.game.add.button(0, 0, "playScoreScreen", this.playAgain, this, 1, 0, 1, 0, this.scoreScreenGr);
            //var quit = this.game.add.button(300, 10, "playScoreScreen", this.quit, this, 3, 2, 3, 2, this.scoreScreenGr);
            // scorePlay.inputEnabled = true;
            //scorePlay.input.priorityID = 3;
            //quit.inputEnabled = true;
            //quit.input.priorityID = 3;
            //this.gameOverSpr.addChild(scorePlay);
            //this.gameOverSpr.addChild(quit);
            //  scorePlay.x = 257;
            //  scorePlay.y = 358;
            //  quit.x = 427;
            // quit.y = 358;
            this.scoreScreenGr.x = 0;
            this.scoreScreenGr.y = 0;
            this.scoreScreenGr.visible = false;
            this.stage.removeChild(this.scoreScreenGr);
            var blokScreenSprite = new Phaser.Graphics(this.game, 0, 0);
            blokScreenSprite.beginFill(0x000000, 0);
            blokScreenSprite.drawRect(0, 0, 800, 600);
            blokScreenSprite.endFill();
            this.blockScreen = new Phaser.Sprite(this.game, 0, 0);
            this.blockScreen.addChild(blokScreenSprite);
            this.blockScreen.addChild(this.blockScreen);
            this.stage.addChild(this.blockScreen);
            //spr.buttonMode = true;
            //  spr.cacheAsBitmap = true;
            this.helpGr = this.add.group();
            this.stage.addChild(this.helpGr);
            this._helpBtn = this.add.button(0, 0, "helpBtn", this.helpBtnClick, this, 1, 0, 0, 0, this.helpGr);
            this.helpGr.x = this.game.stage.width - this.helpGr.width;
            this.helpGr.y = 0;
            this._helpBtn.buttonMode = true;
            this._helpBtn.inputEnabled = true;
            this._helpBtn.input.priorityID = 11;
            this.game.input.onDown.add(function () {
                if (this.game.paused) {
                    this.game.paused = false;
                }
                if (this._instructionShow) {
                    this.stage.removeChild(this._instructionShow);
                    this._instructionShow.kill();
                    this._instructionShow = null;
                }
            }, this);
        };
        Level1.prototype.helpBtnClick = function () {
            console.log("pokaz instrukcje");
            this.game.paused = !this.game.paused;
            if (this._instructionShow == null)
                this._instructionShow = this.add.image(0, 0, "InstructionsScreen", 0);
            // this.add.image(0, 0, "firstInstruction", 0);
            this._instructionShow.width = this.game.world.width; //this.stage.width;
            this._instructionShow.height = this.game.world.height;
            this.stage.addChild(this._instructionShow);
        };
        Level1.prototype.setTextDescription = function () {
            var text = this._xml.getDescription(this._gameControler.actualRandomTextId);
            var style = { font: "35px Arial", fill: "#ffffff", align: "center" };
            if (this.textDescription)
                this.textDescription.destroy();
            this.textDescription = new Phaser.Text(this.game, 0, 0, text, style);
            this.textDescription.wordWrap = true;
            this.textDescription.wordWrapWidth = 600;
            this.textDescription.x = this.game.width / 2 - this.textDescription.width / 2;
            this.textDescription.y = this.game.height / 2 - this.textDescription.height / 2;
            this.textLevelGr.addChild(this.textDescription);
            this.textDescdoneBtn.y = this.textDescription.y + this.textDescription.height + 20;
        };
        Level1.prototype.playAgain = function () {
            console.log("playagain");
            this.stage.removeChild(this.succesAnimatioScreen);
            this.onBlockScreen(false);
            this.gameOverSpr.inputEnabled = false;
            //  this.gameOverSpr.input.priorityID = 0;
            this.scoreScreenGr.visible = false;
            this.stage.removeChild(this.scoreScreenGr);
            this._gameControler.defaultScore();
            this.restart();
            this.restartNextLevel();
        };
        Level1.prototype.quit = function () {
            /*  this.onBlockScreen(false);
              this.scoreScreenGr.x = - this.scoreScreenGr.width;
              this.scoreScreenGr.y = - this.scoreScreenGr.height;
              this.gameOverSpr.inputEnabled = false;
              this.gameOverSpr.input.priorityID = 0;
              this.scoreScreenGr.visible = false;
              this.stage.removeChild(this.scoreScreenGr);
              this.game.state.start("MainMenu", true, false);*/
        };
        Level1.prototype.doneBtnOver = function () {
            this._overButtonA.play();
        };
        Level1.prototype.clearOver = function () {
            this._clearButtonA.play();
        };
        Level1.prototype.doneBtnClick = function () {
            if (this.firtsTime) {
                this.playFirstTime();
                this.firtsTime = false;
            }
            else {
                this.textLevelGr.x = -this.textLevelGr.width;
                this.textLevelGr.y = -this.textLevelGr.height;
                this.textLevelGr.visible = false;
            }
            // this._actualAudioPlay.stop();
        };
        Level1.prototype.repeatLesson = function () {
            this.onBlockScreen(true);
            this.audoBadAnswerArray[this._gameControler.actualBadAnswer - 1].onStop.add(this.finishAudioBadAnswer, this);
            this.audoBadAnswerArray[this._gameControler.actualBadAnswer - 1].volume = 1;
            this.audoBadAnswerArray[this._gameControler.actualBadAnswer - 1].play();
            //  this.game.time.events.loop(this.audoBadAnswerArray[0].totalDuration, this.finishAudioBadAnswer, this);
            //  this.finishAudioBadAnswer();  
        };
        Level1.prototype.onBlockScreen = function (show) {
            if (show) {
                this.blockScreen.inputEnabled = true;
                this.blockScreen.input.priorityID = 1;
                this.stage.addChild(this.blockScreen);
            }
            else {
                this.blockScreen.inputEnabled = false;
                this.blockScreen.input.priorityID = 0;
                this.stage.removeChild(this.blockScreen);
            }
        };
        Level1.prototype.finishAudioBadAnswer = function () {
            this.restart();
            // this.textLevel.frame = this._gameControler.actualRandomTextId;
            this._actualAudioPlay = this.audioArray[0];
            //this._actualAudioPlay.play();
            this.textLevelGr.visible = true;
            this.textLevelGr.x = 0;
            this.textLevelGr.y = 0;
            this.stage.addChild(this.textLevelGr);
            this.onBlockScreen(false);
        };
        Level1.prototype.playFirstTime = function () {
            /* buttony play clear */
            this._buttonsInGame = this.add.group();
            var doneInGame = this.add.button(0, 0, "doneInGameBtn", this.doneInGameClick, this, 1, 0);
            doneInGame.onInputDown.add(this.clearOver, this);
            var clearBtn = this.add.button(0, 0, "clearBtn", this.clearBtnClick, this, 1, 0);
            clearBtn.onInputDown.add(this.doneBtnOver, this);
            clearBtn.y = doneInGame.y + doneInGame.height - 10;
            this._buttonsInGame.addChild(doneInGame);
            this._buttonsInGame.addChild(clearBtn);
            //this.stage.addChild(this._buttonsInGame);
            this._buttonsInGame.x = 0;
            this._buttonsInGame.y = 420;
            /* end buttony play clear */
            this._dayNight = new BeachBuilder.DayNight(this.game, 0, 0, "sunBg", "moonBg", "sunsetBg");
            this._level1Gr = this.add.group();
            //this.stage.addChild(this._level1Gr);
            this._level1Gr.addChild(this._dayNight);
            this._level1Gr.addChild(this._buttonsInGame);
            var style = { font: "16px Arial", fill: "#000000", align: "left" };
            this.scoreText = new Phaser.Text(this.game, 0, 0, "Credits:0      Scene: 1/5  ", style);
            this._level1Gr.addChild(this.scoreText);
            var animNameArray = BeachBuilder.ArrayStatic.animNameArray;
            var itemsDragNameArray = BeachBuilder.ArrayStatic.itemsDragNameArray;
            var itemsDragPositionArray = new Array(new Phaser.Point(564, 466), new Phaser.Point(559, 511), new Phaser.Point(565, 570), new Phaser.Point(639, 469), new Phaser.Point(635, 516), new Phaser.Point(638, 566));
            var itemsDragArray = new Array();
            var maxLengthItems = itemsDragNameArray.length;
            for (var i = 0; i < maxLengthItems; i++) {
                var p = new Phaser.Point(itemsDragPositionArray[i].x, itemsDragPositionArray[i].y);
                var itemToDrag = new BeachBuilder.ItemsToDrag(this.game, this, p, new Phaser.Point(56, 45), itemsDragNameArray[i], i, itemsDragNameArray[i]);
                this._level1Gr.addChild(itemToDrag);
            }
            var peopleNameArray = new Array("ridl", "erad", "tan", "kaz");
            var scaleArray = new Array(0.85, 0.81, 0.84, 0.95);
            var peoplePositionArray = new Array(new Phaser.Point(155, 490), new Phaser.Point(247, 493), new Phaser.Point(341, 496), new Phaser.Point(425, 495));
            var animNameArray = BeachBuilder.ArrayStatic.animNameArray;
            /* tworzenie ludzi plus dodanie animacji */
            var maxPeopleLength = peopleNameArray.length;
            for (var j = 0; j < maxPeopleLength; j++) {
                var p = new Phaser.Point(peoplePositionArray[j].x, peoplePositionArray[j].y);
                var peopleToDrag = new BeachBuilder.PlayerAnim(this.game, this, p, new Phaser.Point(76, 147), peopleNameArray[j] + animNameArray[0], j, peopleNameArray[j], scaleArray[j]);
                this.poepleArray.push(peopleToDrag);
                this._level1Gr.addChild(peopleToDrag);
                for (var l = 1; l < 7; l++) {
                    peopleToDrag.addAnimation(new Phaser.Sprite(this.game, 0, 0, peopleNameArray[j] + animNameArray[l], 0), animNameArray[l]);
                }
            }
            /* tworzenie desek jako ludzi*/
            var skateBoard = new BeachBuilder.PlayerAnim(this.game, this, new Phaser.Point(745, 490), new Phaser.Point(76, 147), "skateboard1", 4, "skateboard1", 1, true);
            var skateBoard1 = new BeachBuilder.PlayerAnim(this.game, this, new Phaser.Point(745, 519), new Phaser.Point(76, 147), "skateboard2", 5, "skateboard2", 1, true);
            var skateBoard2 = new BeachBuilder.PlayerAnim(this.game, this, new Phaser.Point(745, 546), new Phaser.Point(76, 147), "skateboard3", 6, "skateboard3", 1, true);
            var skateBoard3 = new BeachBuilder.PlayerAnim(this.game, this, new Phaser.Point(745, 573), new Phaser.Point(76, 147), "skateboard4", 7, "skateboard4", 1, true);
            this._level1Gr.addChild(skateBoard);
            this._level1Gr.addChild(skateBoard1);
            this._level1Gr.addChild(skateBoard2);
            this._level1Gr.addChild(skateBoard3);
            this.poepleArray.push(skateBoard);
            this.poepleArray.push(skateBoard1);
            this.poepleArray.push(skateBoard2);
            this.poepleArray.push(skateBoard3);
            var itemsToDropNameArray = BeachBuilder.ArrayStatic.itemsToDropNameArray;
            var itemsToDropPositionArray = new Array(new Phaser.Point(82, 205), new Phaser.Point(176, 183), new Phaser.Point(84, 302), new Phaser.Point(622, 188), new Phaser.Point(752, 196), new Phaser.Point(732, 307), new Phaser.Point(303, 293), new Phaser.Point(496, 295));
            var itemsToDropPositionPeopleArray = new Array(new Phaser.Point(60, 142), new Phaser.Point(174, 120), new Phaser.Point(75, 282), new Phaser.Point(592, 140), new Phaser.Point(713, 150), new Phaser.Point(692, 270), new Phaser.Point(323, 268), new Phaser.Point(496, 263));
            new Array("redTree", "orangeTree", "benchLeft", "blueTree", "greenTree", "rightBench", "fountainHighlight", "fountainHighlight2");
            var maxLengthItemsDrop = itemsToDropNameArray.length;
            this._maxDropObject = maxLengthItemsDrop;
            for (var i = 0; i < maxLengthItemsDrop; i++) {
                var p = new Phaser.Point(itemsToDropPositionArray[i].x, itemsToDropPositionArray[i].y);
                var objectToDrop = new BeachBuilder.ObjectToDrop(this.game, this, p, itemsToDropNameArray[i], 0, new Phaser.Point(139, 128), i, 1, itemsToDropPositionPeopleArray[i], itemsToDropNameArray[i]);
                this._level1Gr.addChild(objectToDrop);
                this.itemsToDropArray.push(objectToDrop);
            }
            //  this.itemsToDropArray[6].name = "table";
            //  this.itemsToDropArray[7].name = "table";
            this.itemsToDropArray[6].isAlpha = true;
            this.itemsToDropArray[7].isAlpha = true;
            this.wspolne.push(this.itemsToDropArray[6]);
            this.wspolne.push(this.itemsToDropArray[7]);
            this._timeOfDayGr = this.add.group();
            // this.add.sprite(50, 0, "bgLevel1", this._timeOfDayGr);
            var sunBtn = new BeachBuilder.TimeOfDayBtn(this.game, this, 0, 0, "day", 0);
            var sunsetBtn = new BeachBuilder.TimeOfDayBtn(this.game, this, 0, 0, "sunset", 1);
            var nightBtn = new BeachBuilder.TimeOfDayBtn(this.game, this, 0, 0, "moon", 2);
            this._timeOfDayGr.addChild(sunBtn);
            var margin = 5;
            sunsetBtn.x = sunBtn.x + sunBtn.width + margin;
            this._timeOfDayGr.addChild(sunsetBtn);
            nightBtn.x = sunsetBtn.x + sunsetBtn.width + margin;
            this._timeOfDayGr.addChild(nightBtn);
            this._timeOfDayGr.x = 720;
            this._timeOfDayGr.y = 442;
            this._level1Gr.addChild(this._timeOfDayGr);
            /* var _redChair: Phaser.Sprite = this.add.sprite(13, 87, "redChair", 0, this._level1Gr);
             var _blueChair: Phaser.Sprite = this.add.sprite(326, 253, "blueChair", 0, this._level1Gr);
             var _greenChair: Phaser.Sprite = this.add.sprite(568, 120, "greenChair", 0, this._level1Gr);
            // _redChair.anchor.setTo(0.5, 0.5);
             _blueChair.anchor.setTo(0.5, 0.5);
             _greenChair.anchor.setTo(0.5, 0.5);
             _redChair.events.onDragStart.add(this.redChairDragStart, this);
             _redChair.inputEnabled = true;
             _redChair.input.enableDrag();
            /* this._redChair.onDragStart.add(this.onStartDrag, this);
             this._redChair.onDragStop.add(this.onStopDrag, this);*/
            // this._level1Gr.addChild(_redChair);*/
        };
        Level1.prototype.nextLevel = function () {
            console.log("next level");
            this.onBlockScreen(true);
            var losowanieAnim = this._gameControler.randomNum(this.successAnimArray.length);
            this.succesAnimatioScreen = this.successAnimArray[losowanieAnim];
            this.succesAnimatioScreen.x = this.game.width / 2 - this.succesAnimatioScreen.width / 2;
            this.succesAnimatioScreen.y = this.game.height / 2 - this.succesAnimatioScreen.height / 2;
            this.stage.addChild(this.succesAnimatioScreen);
            var anim = this.succesAnimatioScreen.animations.add("animAgain");
            anim.onComplete.add(function () {
                this.game.time.events.add(Phaser.Timer.SECOND * 2, this.restartNextLevel, this);
            }, this);
            this.succesAnimatioScreen.animations.play("animAgain", 24, false, false);
            //  this.congratsAudioArray[this._gameControler.randomNum(this.congratsAudioArray.length)].onStop.add(this.finishAudioBadAnswer, this);
            var jakaLiczba = this._gameControler.randomNum(this.congratsAudioArray.length);
            console.log("jaka   liczba dla audio gratulacji", jakaLiczba);
            this.congratsAudioArray[jakaLiczba].play();
        };
        Level1.prototype.restartNextLevel = function () {
            this.stage.removeChild(this.succesAnimatioScreen);
            this.succesAnimatioScreen = null;
            this.onBlockScreen(false);
            this.restart();
            //  console.log("level num:", APIgetLevel(userID, gameID), userID, gameID);
            this._xml.levelNum = this._gameControler.countLevel; //APIgetLevel(userID, gameID);
            //this._gameControler.countLevel;
            this._gameControler.maxLevel = this._xml.getMaxLevel();
            this._gameControler.setMaxScene(this._xml.getMaxRandomScenes());
            this._gameControler.randomGame();
            this._xml.getScene(this._gameControler.actualRandomTextId);
            this.setTextDescription();
            //  console.log("audio play", this.audioArray[this._gameControler.actualRandomTextId], this._gameControler.actualRandomTextId);
            //  this._actualAudioPlay = this.audioArray[0];
            //   this._actualAudioPlay.play();
            this.textLevelGr.visible = true;
            this.textLevelGr.x = 0;
            this.textLevelGr.y = 0;
            this.stage.addChild(this.textLevelGr);
        };
        Level1.prototype.doneInGameClick = function () {
            var prawda = this._checkCorrect.checkAnswers(this.itemsToDropArray, this.poepleArray, this._dayNight.answer, this.wspolne);
            console.log("prawidlowa odpowiedz?", prawda);
            if (prawda) {
                this._gameControler.corectAnswer();
            }
            else {
                this._gameControler.badAnswer();
            }
        };
        Level1.prototype.clearBtnClick = function () {
            this.restart();
        };
        Level1.prototype.restart = function () {
            for (var i = 0; i < this._maxDropObject; i++) {
                this.itemsToDropArray[i].restart();
            }
            for (var i = 0; i < this.poepleArray.length; i++) {
                this.poepleArray[i].restart();
            }
            this._dayNight.changeTimeOfDay(0);
        };
        Level1.prototype.changeTimeOfDay = function (id) {
            console.log("change time of day", id);
            this._dayNight.changeTimeOfDay(id);
        };
        Level1.prototype.itemsOnDragStop = function (items) {
            var isPass = false;
            var dragPoint = new Phaser.Point(this._dragItems.x, this._dragItems.y);
            for (var i = 0; i < 4; i++) {
                if (this.poepleArray[i].isDroped && Phaser.Rectangle.contains(this.poepleArray[i]._rectangle, dragPoint.x, dragPoint.y)) {
                    this.poepleArray[i].playAnimation(items.id);
                }
            }
            this._dragItems.x = this._dragItems.startX;
            this._dragItems.y = this._dragItems.startY;
            this._dragItems = null;
        };
        Level1.prototype.itemsOnDragStart = function (items) {
            this._dragItems = items;
            items.bringToTop();
        };
        Level1.prototype.peopleOnDragStart = function (items) {
            this._dragPeople = items;
            items.bringToTop();
        };
        Level1.prototype.peopleOnDragStop = function (items) {
            var isPass = false;
            for (var i = 0; i < this._maxDropObject; i++) {
                if (this.itemsToDropArray[i].isEmpty && Phaser.Rectangle.contains(this.itemsToDropArray[i]._rectangle, this._dragPeople.x, this._dragPeople.y)) {
                    this.itemsToDropArray[i].changeFrame(0);
                    this._dragPeople.startX = this._dragPeople.x = this.itemsToDropArray[i].positionPeople.x;
                    this._dragPeople.startY = this._dragPeople.y = this.itemsToDropArray[i].positionPeople.y;
                    if (this._dragPeople.isSkateBoard) {
                        this._dragPeople.startY = this._dragPeople.y = this._dragPeople.y + 30;
                    }
                    this._dragPeople.checkLastObjectToDrop(this.itemsToDropArray[i]);
                    isPass = true;
                    this.itemsToDropArray[i].isEmpty = false;
                    this.itemsToDropArray[i].answer = this._dragPeople.name;
                    this.itemsToDropArray[i].dropedObjectName = this.itemsToDropArray[i].ownName;
                    items.playAnim();
                }
                else {
                    this.itemsToDropArray[i].changeFrame(0);
                }
            }
            if (!isPass) {
                this._dragPeople.x = this._dragPeople.startX;
                this._dragPeople.y = this._dragPeople.startY;
            }
            this._dragPeople = null;
        };
        Level1.prototype.update = function () {
            if (this._dragPeople != null) {
                var dragPoint = new Phaser.Point(this._dragPeople.x, this._dragPeople.y);
                for (var i = 0; i < this._maxDropObject; i++) {
                    if (this.itemsToDropArray[i].isEmpty && Phaser.Rectangle.contains(this.itemsToDropArray[i]._rectangle, dragPoint.x, dragPoint.y)) {
                        this.itemsToDropArray[i].changeFrame(1);
                    }
                    else {
                        this.itemsToDropArray[i].changeFrame(0);
                    }
                }
            }
        };
        Level1.prototype.finishLevelAnim = function () {
        };
        Level1.prototype.youWin = function (finishLevel) {
            if (finishLevel === void 0) { finishLevel = false; }
            //this.add.image(0, 0, "youWin");
            console.log("you win");
            this.onBlockScreen(true);
            this.succesAnimatioScreen = this.successAnimArray[this._gameControler.randomNum(this.successAnimArray.length)];
            this.succesAnimatioScreen.x = this.game.width / 2 - this.succesAnimatioScreen.width / 2;
            this.succesAnimatioScreen.y = this.game.height / 2 - this.succesAnimatioScreen.height / 2;
            //  this.stage.addChild(this.succesAnimatioScreen);
            var anim = this.succesAnimatioScreen.animations.add("animAgain");
            //  APIsetLevel(userID, gameID, parseInt(userLevel) + 1);
            // APIsetLevel(userID, gameID, userLevel + 1);
            this.finishGame();
            //   anim.onComplete.add(function () { this.game.time.events.add(Phaser.Timer.SECOND * 2, this.finishGame, this) }, this);
            if (finishLevel) {
            }
            else {
            }
            // this.succesAnimatioScreen.animations.play("animAgain", 24, false, false);
            //  this.congratsAudioArray[this._gameControler.randomNum(this.congratsAudioArray.length)].onStop.add(this.finishAudioBadAnswer, this);
            this.congratsAudioArray[this._gameControler.randomNum(this.congratsAudioArray.length)].play();
        };
        Level1.prototype.finishGame = function () {
            this.onBlockScreen(true);
            this.succesAnimatioScreen = this.successAnimArray[this._gameControler.randomNum(this.successAnimArray.length)];
            this.succesAnimatioScreen.x = this.game.width / 2 - this.succesAnimatioScreen.width / 2;
            this.succesAnimatioScreen.y = this.game.height / 2 - this.succesAnimatioScreen.height / 2;
            this.stage.addChild(this.succesAnimatioScreen);
            var anim = this.succesAnimatioScreen.animations.add("animAgain");
            this.screenWinOrGameOver = new BeachBuilder.PopUpWin(this.game, this, 0, 0);
            anim.onComplete.add(function () {
                this.game.time.events.add(Phaser.Timer.SECOND * 2, this.animationStopped, this);
            }, this);
            this.succesAnimatioScreen.animations.play("animAgain", 24, false, false);
            //  this.congratsAudioArray[this._gameControler.randomNum(this.congratsAudioArray.length)].onStop.add(this.finishAudioBadAnswer, this);
            this.congratsAudioArray[this._gameControler.randomNum(this.congratsAudioArray.length)].play();
        };
        Level1.prototype.youLose = function () {
            this.onBlockScreen(true);
            this.fail = this.failArray[this._gameControler.randomNum(this.failArray.length)];
            this.fail.x = this.game.width / 2 - this.fail.width / 2;
            this.fail.y = this.game.height / 2 - this.fail.height / 2;
            this.stage.addChild(this.fail);
            this.screenWinOrGameOver = new BeachBuilder.PopUp(this.game, this, 0, 0);
            var anim = this.fail.animations.add("anim");
            anim.onComplete.add(function () {
                this.game.time.events.add(Phaser.Timer.SECOND * 2, this.animationStopped, this);
            }, this);
            this.fail.animations.play("anim", 24, false, false);
            //this.game.time.events.add(Phaser.Timer.SECOND * 6, this.animationStopped, this);
            //this.add.image(0, 0, "youLose");
        };
        Level1.prototype.animationStopped = function () {
            this.stage.removeChild(this.fail);
            this.scoreScreenGr.x = 0; // this.world.width / 2 - this.scoreScreenGr.width / 2;
            this.scoreScreenGr.y = 0; // this.world.height / 2 - this.scoreScreenGr.height / 2;
            this.screenWinOrGameOver.visible = true;
            this.screenWinOrGameOver.inputEnabled = true;
            this.screenWinOrGameOver.input.priorityID = 2;
            this.scoreScreenGr.addChild(this.screenWinOrGameOver);
            //  this.youWinSpr.inputEnabled = true;
            // this.youWinSpr.input.priorityID = 2;
            this.scoreScreenGr.visible = true;
            this.stage.addChild(this.scoreScreenGr);
        };
        Level1.prototype.hideScreenAfterFinishPlaye = function () {
            // this.stage.removeChild(this.succesAnimatioScreen);
            //  this.succesAnimatioScreen = null;
            //  this.onBlockScreen(false);
            this.restart();
            this.restartNextLevel();
        };
        return Level1;
    })(Phaser.State);
    BeachBuilder.Level1 = Level1;
})(BeachBuilder || (BeachBuilder = {}));
var BeachBuilder;
(function (BeachBuilder) {
    var MainMenu = (function (_super) {
        __extends(MainMenu, _super);
        function MainMenu() {
            _super.apply(this, arguments);
        }
        MainMenu.prototype.preload = function () {
        };
        MainMenu.prototype.create = function () {
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            this.mainSound = this.game.add.audio('MainMenuSn', 1, true);
            this.mainSound.volume = 0.1;
            this.mainSound.play();
            var gameControler = BeachBuilder.GameControler.getInstance();
            gameControler.setSound(this.mainSound);
            var background = this.add.sprite(0, 0, 'mainMenuBg');
            this._buttons = this.add.group();
            // var buttonsBg: Phaser.Sprite = this.add.sprite(0, 0, 'buttonsBg');
            //  this._buttons.addChild(buttonsBg);
            var gameButton = this.add.button(0, 0, "games", this.onGameClick, this, 1, 0);
            //gameButton.y = this._buttons.height / 2 - gameButton.height / 2;
            this._buttons.addChild(gameButton);
            var playButton = this.add.button(100, 0, "playGame", this.onPlayClick, this, 1, 0);
            playButton.onInputDown.add(this.overPlay2, this);
            //playButton.x = this._buttons.width - playButton.width - 10;
            this._buttons.addChild(playButton);
            this._buttons.x = 0;
            this._buttons.y = this.game.stage.height - this._buttons.height;
        };
        MainMenu.prototype.overPlay2 = function () {
            this.overButtonA.play();
        };
        MainMenu.prototype.onGameClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        MainMenu.prototype.onPlayClick = function () {
            // this.mainSound.stop();
            this.game.state.start("PopupStart", true, false);
        };
        return MainMenu;
    })(Phaser.State);
    BeachBuilder.MainMenu = MainMenu;
})(BeachBuilder || (BeachBuilder = {}));
var BeachBuilder;
(function (BeachBuilder) {
    var PopUp = (function (_super) {
        __extends(PopUp, _super);
        function PopUp(game, level, x, y) {
            _super.call(this, game, x, y);
            this.level1 = level;
            this._game = game;
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var bgPopup = new Phaser.Sprite(this.game, 0, 0, "popup");
            this.addChild(bgPopup);
            this.logo = new Phaser.Sprite(this.game, 0, 0, "logoGame");
            this.addChild(this.logo);
            this.logo.x = (bgPopup.width / 2 - this.logo.width / 2);
            this.logo.y = this.logo.height - 20;
            var text = "You lose. Good try.\n Keep playing to win credits";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this.game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;
            textDescription.x = this.game.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 100 + (140 / 2 - textDescription.height / 2);
            this.addChild(textDescription);
            this.textLevel();
            //this.beatText();
            this.addButtons();
            this.visible = false;
        }
        PopUp.prototype.addButtons = function () {
            this.gamesBtn = new Phaser.Button(this._game, 0, 0, "games", this.gamesBtnClick, this, 1, 0);
            this.playBtn = new Phaser.Button(this._game, 100, 0, "playGame", this.playBtnClick, this, 1, 0);
            this.gamesBtn.inputEnabled = true;
            this.gamesBtn.input.priorityID = 3;
            this.playBtn.inputEnabled = true;
            this.playBtn.input.priorityID = 3;
            var buttonsGr = new Phaser.Group(this._game);
            this.addChild(buttonsGr);
            buttonsGr.add(this.gamesBtn);
            buttonsGr.add(this.playBtn);
            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;
        };
        PopUp.prototype.gamesBtnClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        PopUp.prototype.playBtnClick = function () {
            this.overButtonA.play();
            this.visible = false;
            this.gamesBtn.inputEnabled = false;
            this.gamesBtn.input.priorityID = 3;
            this.playBtn.inputEnabled = false;
            this.playBtn.input.priorityID = 3;
            this.level1.hideScreenAfterFinishPlaye();
            this.kill();
            //  this.game.state.start("PopupStart", true, false);
        };
        PopUp.prototype.textLevel = function () {
            var text = "GAME OVER";
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this._game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = this.game.stage.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 25;
            this.addChild(textDescription);
        };
        return PopUp;
    })(Phaser.Sprite);
    BeachBuilder.PopUp = PopUp;
})(BeachBuilder || (BeachBuilder = {}));
function launchGame(result) {
    userLevel = result.level;
}
//declare function launchGame(result);
var BeachBuilder;
(function (BeachBuilder) {
    var PopupStart = (function (_super) {
        __extends(PopupStart, _super);
        function PopupStart() {
            _super.apply(this, arguments);
        }
        PopupStart.prototype.create = function () {
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            var background = this.add.sprite(0, 0, 'mainMenuBg');
            var bgPopup = this.add.sprite(0, 0, "popup");
            this.logo = this.add.sprite(0, 0, "logoGame");
            this.logo.x = (bgPopup.width / 2 - this.logo.width / 2);
            this.logo.y = 70;
            var text = "Build silly scenes to match\n the story you read.";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;
            textDescription.x = this.game.stage.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 105 + (140 / 2 - textDescription.height / 2);
            this.textLevel();
            //this.beatText();
            this.addButtons();
        };
        PopupStart.prototype.addButtons = function () {
            var gamesBtn = this.add.button(0, 0, "games", this.gamesBtnClick, this, 1, 0);
            var playBtn = this.add.button(100, 0, "playGame", this.playBtnClick, this, 1, 0);
            var buttonsGr = this.add.group();
            buttonsGr.add(gamesBtn);
            buttonsGr.add(playBtn);
            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;
        };
        PopupStart.prototype.gamesBtnClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        PopupStart.prototype.playBtnClick = function () {
            this.overButtonA.play();
            this.game.state.start("Instruction", true, false);
        };
        PopupStart.prototype.beatText = function () {
            var text = "BEAT THIS LEVEL";
            //" 6";
            var style = { font: "bold 13px Arial", fill: "#660000", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = (this.game.width / 2) + (140) - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 100;
            var numberText = "6";
            var styleNumber = { font: "bold 17px Arial", fill: "#990000", align: "center" };
            var numberShow = this.add.text(0, 0, numberText, styleNumber);
            numberShow.x = textDescription.x + textDescription.width / 2 - numberShow.width / 2;
            numberShow.y = textDescription.y + textDescription.height;
            var tunesText = "MORE TUNES TO WIN";
            var tunesShow = this.add.text(0, 0, tunesText, style);
            tunesShow.wordWrap = true;
            tunesShow.wordWrapWidth = 100;
            tunesShow.x = textDescription.x + textDescription.width / 2 - tunesShow.width / 2;
            tunesShow.y = numberShow.y + numberShow.height;
            var creditsNmText = "25";
            var styleCredits = { font: "bold 17px Arial", fill: "#ff0000", align: "center" };
            var credistNmShow = this.add.text(0, 0, creditsNmText, styleCredits);
            credistNmShow.x = tunesShow.x + tunesShow.width / 2 - credistNmShow.width / 2;
            credistNmShow.y = tunesShow.y + tunesShow.height;
            var creditsText = "CREDITS";
            var credistShow = this.add.text(0, 0, creditsText, style);
            credistShow.x = tunesShow.x + tunesShow.width / 2 - credistShow.width / 2;
            credistShow.y = credistNmShow.y + credistNmShow.height;
        };
        PopupStart.prototype.textCredits = function () {
            var text = "Beat this level";
            //" 6";
            var style = { font: "bold 14px Arial", fill: "#660000", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = (this.game.width / 2) + 257 / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 100;
            var numberText = "6";
            var styleNumber = { font: "bold 18px Arial", fill: "#990000", align: "center" };
            var numberShow = this.add.text(0, 0, numberText, styleNumber);
            numberShow.x = textDescription.x + textDescription.width / 2 - numberShow.width / 2;
            numberShow.y = textDescription.y + textDescription.height;
        };
        PopupStart.prototype.textLevel = function () {
            //APIgetLevel(userID, gameID);
            var text = "LEVEL " + userLevel;
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };
            var textDescription = this.add.text(0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = this.game.stage.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 25;
        };
        return PopupStart;
    })(Phaser.State);
    BeachBuilder.PopupStart = PopupStart;
})(BeachBuilder || (BeachBuilder = {}));
var BeachBuilder;
(function (BeachBuilder) {
    var PopUpWin = (function (_super) {
        __extends(PopUpWin, _super);
        function PopUpWin(game, level, x, y) {
            _super.call(this, game, x, y);
            this.overButtonA = this.game.add.audio("overButtonA", 1, false);
            this._game = game;
            this.level1 = level;
            var bgPopup = new Phaser.Sprite(this.game, 0, 0, "popup");
            this.addChild(bgPopup);
            this.logo = new Phaser.Sprite(this.game, 0, 0, "logoGame");
            this.addChild(this.logo);
            this.logo.x = (bgPopup.width / 2 - this.logo.width / 2);
            this.logo.y = this.logo.height - 20;
            var text = "You finish the level.\n Keep Playing to win credits!";
            var style = { font: "bold 22px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this.game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.wordWrap = true;
            textDescription.wordWrapWidth = 470;
            textDescription.x = this.game.stage.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 105 + (140 / 2 - textDescription.height / 2);
            this.addChild(textDescription);
            this.textLevel();
            //this.beatText();
            this.addButtons();
            this.visible = false;
        }
        PopUpWin.prototype.addButtons = function () {
            this.gamesBtn = new Phaser.Button(this._game, 0, 0, "games", this.gamesBtnClick, this, 1, 0);
            this.playBtn = new Phaser.Button(this._game, 100, 0, "playGame", this.playBtnClick, this, 1, 0);
            this.gamesBtn.inputEnabled = true;
            this.gamesBtn.input.priorityID = 3;
            this.playBtn.inputEnabled = true;
            this.playBtn.input.priorityID = 3;
            var buttonsGr = new Phaser.Group(this._game);
            this.addChild(buttonsGr);
            buttonsGr.add(this.gamesBtn);
            buttonsGr.add(this.playBtn);
            buttonsGr.x = this.game.width / 2 - buttonsGr.width / 2;
            buttonsGr.y = this.game.height - 100;
        };
        PopUpWin.prototype.gamesBtnClick = function () {
            this.overButtonA.play();
            window.history.back();
        };
        PopUpWin.prototype.playBtnClick = function () {
            this.overButtonA.play();
            this.visible = false;
            this.gamesBtn.inputEnabled = false;
            this.gamesBtn.input.priorityID = 3;
            this.playBtn.inputEnabled = false;
            this.playBtn.input.priorityID = 3;
            this.level1.hideScreenAfterFinishPlaye();
            //  this.game.state.start("PopupStart", true, false);
            this.kill();
        };
        PopUpWin.prototype.textLevel = function () {
            var text = "GOOD JOB";
            var style = { font: "bold 34px Arial", fill: "#993300", align: "center" };
            var textDescription = new Phaser.Text(this._game, 0, 0, text, style);
            //   new Phaser.Text(this.game, 0, 0, text, style);
            textDescription.x = this.game.stage.width / 2 - textDescription.width / 2;
            textDescription.y = this.logo.y + this.logo.height + 25;
            this.addChild(textDescription);
        };
        return PopUpWin;
    })(Phaser.Sprite);
    BeachBuilder.PopUpWin = PopUpWin;
})(BeachBuilder || (BeachBuilder = {}));
//# sourceMappingURL=game.js.map