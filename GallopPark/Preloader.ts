﻿module BeachBuilder {
    export class Preloader extends Phaser.State {
        private progresBar: Phaser.Sprite;
        private progressBarBlue: Phaser.Sprite;



        private reolad() {

            var tween = this.add.tween(this.progressBarBlue).to({ alpha: 1 }, 1000, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.reolad2, this);
        }

        private reolad2() {
            var tween = this.add.tween(this.progressBarBlue).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.reolad, this);

        }
        preload() {

            var mobile: boolean;
            mobile = false
            if (!this.game.device.desktop) {
                mobile = true;
            }

            var bg: Phaser.Sprite = this.game.add.sprite(0, 0, "bg");

            this.progresBar = this.game.add.sprite(0, 0, "progressBar");
         
            
            //  this.progresBar.x = this.game.width / 2 - this.progresBar.width / 2;
            //  this.progresBar.y = this.game.height - this.progresBar.height - 20;
         
           
            this.progresBar.position.setTo(this.stage.width / 2 - this.progresBar.width / 2, this.stage.height - this.progresBar.height - 10);

            this.progressBarBlue = this.add.sprite(0, 0, 'progressBarBlue');
            this.progressBarBlue.position = this.progresBar.position;
            this.load.setPreloadSprite(this.progressBarBlue);

            if (mobile) {
                this.load.image("InstructionsScreen", "assets/03_gallop_park.png");
                this.load.audio("instructionSn", "assets/audio/gallop_park_02.mp3");

            }
            else {
                this.load.image("InstructionsScreen", "assets/02_gallop_park.png");
                this.load.audio("instructionSn", "assets/audio/gallop_park_01.mp3");
            }

            this.load.audio("MainMenuSn", "assets/mainMenu/BBThemeMusic.mp3");

            this.load.image("popup", "assets/popup/popup.png");
            this.load.image("logoGame", "assets/popup/logo.png");
            this.load.atlasJSONArray('games', 'assets/popup/games.png', 'assets/popup/games.json');
            this.load.atlasJSONArray('helpBtn', 'assets/popup/help.png', 'assets/popup/help.json');
        
            this.load.atlasJSONArray('playGame', 'assets/popup/play.png', 'assets/popup/play.json');
         
        
            this.load.image("mainMenuBg", "assets/mainMenu/SplashScreen2.png");
            this.load.atlasJSONArray('buttons', 'assets/mainMenu/mainMenuButtons.png', 'assets/mainMenu/mainMenuButtons.json');
            this.load.image('buttonsBg', 'assets/mainMenu/naviMenuBackground.png');
           
            this.load.audio("overButtonA", "assets/audio/overButton.mp3");
            this.load.audio("clearButtonA", "assets/audio/clearButton.mp3");


            this.load.atlasJSONArray("doneInGameBtn", "assets/Level1/buttons/doneInGame.png", "assets/Level1/buttons/doneInGame.json");
            this.load.atlasJSONArray("clearBtn", "assets/Level1/buttons/clear.png", "assets/Level1/buttons/clear.json");


            this.load.image("TextLevel1", "assets/Level1/TextLevel1.png");
            this.load.atlasJSONArray("doneBtn", "assets/Level1/buttons/textDoneBtn.png", "assets/Level1/buttons/textDoneBtn.json");

            this.load.image("youWin", "assets/Level1/youWin.png");
            this.load.image("youLose", "assets/Level1/youLose.png");

            this.load.image("bgLevel1", "assets/Level1/bgLevel1.png");
            this.load.image("sunBg", "assets/Level1/sun.png");
            this.load.image("sunsetBg", "assets/Level1/sunset.png");
            this.load.image("moonBg", "assets/Level1/moon.png");


            this.load.atlasJSONArray("fountain", "assets/Level1/objects/fountain.png", "assets/Level1/objects/fountain.json");
            /* items to drop */
            this.load.atlasJSONArray("benchLeft", "assets/Level1/objects/bench.png", "assets/Level1/objects/bench.json");
            this.load.atlasJSONArray("redTree", "assets/Level1/objects/leftTree.png", "assets/Level1/objects/leftTree.json");
            this.load.atlasJSONArray("orangeTree", "assets/Level1/objects/orangeTree.png", "assets/Level1/objects/orangeTree.json");
            this.load.atlasJSONArray("blueTree", "assets/Level1/objects/blueTree.png", "assets/Level1/objects/blueTree.json");
            this.load.atlasJSONArray("greenTree", "assets/Level1/objects/greenTree.png", "assets/Level1/objects/greenTree.json");
            this.load.atlasJSONArray("rightBench", "assets/Level1/objects/rightBench.png", "assets/Level1/objects/rightBench.json");
            this.load.atlasJSONArray("fountainHighlight", "assets/Level1/objects/fountainHighlight.png", "assets/Level1/objects/fountainHighlight.json");
            this.load.atlasJSONArray("fountainHighlight2", "assets/Level1/objects/fountainHighlight2.png", "assets/Level1/objects/fountainHighlight2.json");
            /*end item to drop */

            /*items to drag*/

            this.load.image("cdPlayer", "assets/Level1/downBarForObjects/items/CdPlayer.png");
            this.load.image("cellPhone", "assets/Level1/downBarForObjects/items/CellPhone.png");
            this.load.image("book", "assets/Level1/downBarForObjects/items/Comic.png");

            this.load.image("gum", "assets/Level1/downBarForObjects/items/BubbleGum.png");
            this.load.image("laptop", "assets/Level1/downBarForObjects/items/Laptop.png");
            this.load.image("ice", "assets/Level1/downBarForObjects/items/ice.png");

          
            /*end items to drag*/

            /* time of day */

            this.load.image("day", "assets/Level1/objects/SunBtn.png");
            this.load.image("moon", "assets/Level1/objects/MoonBtn.png");
            this.load.image("sunset", "assets/Level1/objects/sunsetBtn.png");

            /*end time of day */

            /* people to drag */
            /*  this.load.atlasJSONArray("gab", "assets/Level1/downBarForObjects/gab.png", "assets/Level1/downBarForObjects/gab.json");
              this.load.atlasJSONArray("kaz", "assets/Level1/downBarForObjects/kaz.png", "assets/Level1/downBarForObjects/kaz.json");
              this.load.atlasJSONArray("mo", "assets/Level1/downBarForObjects/mo.png", "assets/Level1/downBarForObjects/mo.json");
              this.load.atlasJSONArray("ridl", "assets/Level1/downBarForObjects/ridl.png", "assets/Level1/downBarForObjects/ridl.json");*/

            
            /* animations */
            var animNameArray: Array<string> = new Array("_default", "_player", "_phone", "_comic", "_gum", "_laptop", "_ice");
            for (var i: number = 0; i < 7; i++) {

                this.load.atlasJSONArray("kaz" + animNameArray[i], "assets/Level1/downBarForObjects/kaz/kaz" + animNameArray[i] + ".png", "assets/Level1/downBarForObjects/kaz/kaz" + animNameArray[i] + ".json");
                this.load.atlasJSONArray("tan" + animNameArray[i], "assets/Level1/downBarForObjects/tan/tan" + animNameArray[i] + ".png", "assets/Level1/downBarForObjects/tan/tan" + animNameArray[i] + ".json");
                this.load.atlasJSONArray("erad" + animNameArray[i], "assets/Level1/downBarForObjects/brad/brad" + animNameArray[i] + ".png", "assets/Level1/downBarForObjects/brad/brad" + animNameArray[i] + ".json");
                this.load.atlasJSONArray("ridl" + animNameArray[i], "assets/Level1/downBarForObjects/ridl/ridl" + animNameArray[i] + ".png", "assets/Level1/downBarForObjects/ridl/ridl" + animNameArray[i] + ".json");


            }

            for (var i: number = 0; i < 20; i++) {

                this.load.audio("lessonsA" + (i), "assets/audio/level/prompt1A_" + (i + 1) + ".mp3");


            }


            for (var i: number = 1; i < 6; i++) {

                this.load.atlasJSONArray("fail" + i, "assets/feedbacks/fail" + i + ".png", "assets/feedbacks/fail" + i + ".json");



            }


            for (var i: number = 1; i < 5; i++) {

                this.load.atlasJSONArray("success" + i, "assets/feedbacks/success" + i + ".png", "assets/feedbacks/success" + i + ".json");



            }

            for (var i: number = 4; i < 8; i++) {
                this.load.audio("congrats" + i, "assets/audio/gallop_park_0" + i + ".mp3");



            }

            this.load.audio("failure1", "assets/audio/gallop_park_08.mp3");
            this.load.audio("failure2", "assets/audio/gallop_park_09.mp3");
            /* end animations */

            /* skateboard as people */

            this.load.image("skateboard1", "assets/Level1/objects/skateboard1.png");
            this.load.image("skateboard2", "assets/Level1/objects/skateboard2.png");
            this.load.image("skateboard3", "assets/Level1/objects/skateboard3.png");
            this.load.image("skateboard4", "assets/Level1/objects/skateboard4.png");

            /* end skateboard as people */

            this.load.audio("overButtonA", "assets/audio/overButton.wav");
            this.load.audio("clearButtonA", "assets/audio/clearButton.wav");



            this.load.atlasJSONArray("scoreScreen", "assets/scoreScreen/scoreScreen.png", "assets/scoreScreen/scoreScreen.json");
            this.load.atlasJSONArray("playScoreScreen", "assets/scoreScreen/scoreScreenButtons.png", "assets/scoreScreen/scoreScreenButtons.json");

            this.load.text('xml', 'assets/level1.xml');

        }

        update() {
            //this.progressBarBlue.x = this.game.load.progress;
            console.log(this.progressBarBlue.x);
        }


        mainMenu() {


        }

        create() {




            this.game.state.start("MainMenu");


        }

    }

}