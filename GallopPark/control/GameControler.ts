﻿module BeachBuilder {

    export class GameControler {

        public static _instance: GameControler = null;
        private _solutions: Solutions;
        public actualRandomTextId: number;
        public maxBadRepaets: number = 3;
        public actualBadAnswer: number = 0;
        public maxCorrectReapets = 5;
        private actualCorectAnswe: number = 0;
        private _gameState: Level1;
        public creditsAll: number = 0;
        private maxCreditsForRound: number = 3;
        public curentScene: number = 1;
        private maxRandomScene: number;
        public countLevel: number = 0;
        public maxLevel: number;
        private drawedNumbers: Array<number>;
        constructor() {

            if (GameControler._instance) {
                throw new Error("Error: Instantiation failed: Use GameControler.getInstance() instead of new.");
            }
            GameControler._instance = this;
            this._solutions = Solutions.getInstance();

        }


        public static getInstance(): GameControler {

            if (GameControler._instance === null) {

                GameControler._instance = new GameControler();
            }

            return GameControler._instance;
        }

        public randomGame() {
            console.log("max scenes", this.maxRandomScene);

            var maxScene: number = this.maxRandomScene  - 1;

            this.actualRandomTextId = this.randomScene();
            // this.drawedNumbers.push(this.actualRandomTextId);

            console.log("wylosowano scene", this.actualRandomTextId, this.drawedNumbers);


        }


        public setMaxScene(num: number) {

            this.maxRandomScene = num;

            this.drawedNumbers = new Array();
            for (var i: number = 0; i < num; i++) {

                this.drawedNumbers.push(i);


            }





        }
        private randomScene(): number {
            var losowanie = this.randomNum(this.drawedNumbers.length);


            var wylosowanaLiczba = this.drawedNumbers[losowanie];
            this.drawedNumbers.splice(losowanie, 1);
            console.log("losowanie sceny", wylosowanaLiczba, losowanie, this.drawedNumbers);
            return wylosowanaLiczba;


        }

        private sprawdzam() {



        }

        public corectAnswer() {

            this.actualCorectAnswe++;
            if (this.actualCorectAnswe > 4) {

                //this.maxLevel
                this.countLevel++;
                if (this.countLevel < this.maxLevel) {
                    this.defaultScore();
                    console.log("Kolejny level", this.countLevel, this.curentScene);
                    this._gameState.youWin(true);
                }
                else {
                    console.log("finishGame");
                    this._gameState.finishGame();
                }
                
                // you win
            }
            else {

                this.creditsAll += this.maxCreditsForRound;
                this.curentScene++;
                this.changeScore();
                /* set max credit to default */
                this.maxCreditsForRound = 3;
                console.log("nextScene");
                this._gameState.nextLevel();

            }


        }

        public defaultScore() {
            this.actualBadAnswer = 0;
            //  this.creditsAll = 0;
            this.actualCorectAnswe = 0;
            this.curentScene = 1;
            this.changeScore();

        }

        private changeScore() {

            this._gameState.scoreText.text = "Credits " + this.creditsAll + "     Scenes: " + this.curentScene + "/5   Level: " + (this.countLevel + 1);

        }

        public badAnswer() {
            this.actualBadAnswer++;
            if (this.actualBadAnswer > 2) {
                //GAme Over
                this._gameState.youLose();
            }
            else {
                //play audio
                this._gameState.repeatLesson();
                this.maxCreditsForRound--;
            }



        }

        public randomNum(max: number, min: number = 0): number {

            return Math.floor(Math.random() * (max - min) + min);
        }
        public setGameState(value: Level1) {
            this._gameState = value;
        }

        public getGameState(): Level1 {
            return this._gameState;
        }

    }


} 