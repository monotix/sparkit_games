﻿module BeachBuilder {

    export class Places {
        private itemsToDropNameArray: Array<String> = ArrayStatic.itemsToDropNameArray;
        private peopleNameArray: Array<string> = new Array("ridl", "erad", "tan", "kaz", "skateboard1", "skateboard2", "skateboard3", "skateboard4");
        private animNameArray: Array<string> = ArrayStatic.animNameArray;
        //animNameArray  bez "_default"
        public placeAnswer: Array<number>;
        public peopleAnswer: Array<number>;
        public timeOfDay: number = -1;
        // -1 nie wazne 0 dzien, 1wieczor, noc
        constructor() {

            this.placeAnswer = new Array();
            for (var i: number = 0; i < this.itemsToDropNameArray.length; i++) {

                this.placeAnswer.push(-1);

            }
            this.peopleAnswer = new Array();
            for (var i: number = 0; i < this.peopleNameArray.length; i++) {

                this.peopleAnswer.push(-1);

            }


        }

        public setSolution(namePlace: string = "", scoreNamePeople: string ="", namePeople:string ="", scoreNameObject:string="",timeOfDay_:number=-1) {

            if (timeOfDay_ != -1) {

                this.timeOfDay = timeOfDay_;

            }
                if (namePlace.length > 0) {
                    var whichDrop: number;
                  //  console.log("nazwa miejsca", namePlace, "nazwa ludzia", scoreNamePeople);
                    for (var i: number = 0; i < this.itemsToDropNameArray.length; i++) {

                        if (this.itemsToDropNameArray[i] == namePlace) {

                            whichDrop = i;
                            //console.log("znaleziono nazwa miejsca", namePlace, i);

                        }
                    }

                    for (var i: number = 0; i < this.peopleNameArray.length; i++) {

                        if (this.peopleNameArray[i] == scoreNamePeople) {
                          //  console.log("znaleziono czlowieka", scoreNamePeople, "jego id",i);
                            this.placeAnswer[whichDrop] = i;


                        }

                    }

                }
                if (namePeople.length > 0) {
                        var whichPeople: number;
                        for (var i: number = 0; i < this.peopleNameArray.length; i++) {

                            if (this.peopleNameArray[i] == namePeople) {


                                whichPeople = i;


                            }

                        }

                        for (var i: number = 0; i < this.animNameArray.length; i++) {

                            if (this.animNameArray[i] == scoreNameObject) {


                                this.peopleAnswer[whichPeople] = i;


                            }

                        }

                }



            

        }



    }



} 